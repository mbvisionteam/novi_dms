﻿#include "stdafx.h"
#include "Hikrobot.h"



Hikrobot*	Hikrobot::glob;

Hikrobot::Hikrobot()
{
	//glob = this;
	cameraHandle = NULL;
	availableDevices = { 0 };
}



Hikrobot::~Hikrobot()
{
	int retVal = -1;

	StopGrabber();
	//Zapri povezavo s Hikrobot kamero
	retVal = MV_CC_CloseDevice(cameraHandle);

	if (retVal != MV_OK)
		qDebug() << "Nesupesno zapiranje povezave s Hikrobot kamero";

	//Uniči objekt za povezavo s kamero in sprosti pomnilnik
	retVal = MV_CC_DestroyHandle(cameraHandle);

	if (retVal != MV_OK)
		qDebug() << "Nesupesno unicenje objekta za povezavo s Hikrobot kamero";

}

int Hikrobot::AvailableDevices()
{
	int retVal = -1;
	int cameraType = 0;
	retVal = MV_CC_EnumDevices(MV_GIGE_DEVICE | MV_USB_DEVICE, &availableDevices);

	if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno enumeriranje Hikrobot kamer";
		return retVal;
	}

	return availableDevices.nDeviceNum;
}


void  __stdcall Hikrobot::OnFrameReady(unsigned char * pData, MV_FRAME_OUT_INFO_EX * pFrameInfo, void * pUser)
{
	glob = (Hikrobot*)pUser;

	if (glob->frameReadyFreq.timeCounter >= glob->FPS)
	{
		glob->frameReadyFreq.SetStop();

		glob->frameReadyFreq.CalcFrequency(glob->frameReadyFreq.timeCounter);
		glob->frameReadyFreq.timeCounter = 0;
		glob->frameReadyFreq.SetStart();
	}

	unsigned char * grabbed = pData;

	Mat mat(glob->height, glob->width, CV_8UC1);
	int bufferSize = glob->height * glob->width;

	if (glob->isLive)
	{
		if (glob->enableLive) //kadar imamo ziv prikaz, je ziva slika na sliki 0
		{
			std::copy(&grabbed[0], &grabbed[0] + bufferSize, mat.data);

			if (glob->flipCode < 2)
			{
				flip(mat, mat, glob->flipCode);
			}
			cvtColor(mat, glob->image[0].buffer[0], COLOR_GRAY2RGB);

			
			glob->imageReady[glob->imageIndex] = ImageStages::ImageReceived;

			if (glob->realTimeProcessing)
				emit glob->frameReadySignal(glob->id, glob->imageIndex);
			glob->imageIndex ++;
		}
	

		else if (glob->trigger)
		{
			std::copy(&grabbed[0], &grabbed[0] + bufferSize, mat.data);
			if (glob->flipCode < 2)
			{
				flip(mat, mat, glob->flipCode);
			}
			cvtColor(mat, glob->image[0].buffer[0], COLOR_GRAY2RGB);
			glob->imageReady[glob->imageIndex] = ImageStages::ImageReceived;

			if (glob->realTimeProcessing)
				emit glob->frameReadySignal(glob->id, glob->imageIndex);

			glob->imageIndex++;
		}


		glob->isFrameReady = true;


		if ((glob->FPS > 200) && (glob->FPS < 2000))
		{
			if (glob->frameReadyCounter % 30 == 0)
				emit glob->showImageSignal(0);
		}
		else if (glob->FPS >= 2000)
		{
			if (glob->frameReadyCounter % 50 == 0)
				emit glob->showImageSignal(0);
		}
		else
			emit glob->showImageSignal(0);
		//if (!enableLive)

		//else imageIndex = 0;
		//ce hocemo sliko zakleniti postavimo zastavico lockImage
		if (glob->imageIndex >= glob->image.size())
			glob->imageIndex = 0;

		glob->frameReadyCounter++;

		glob->grabSucceeded = true;

		glob->frameCounter++;
		glob->frameReadyFreq.timeCounter++;

	}


}

int Hikrobot::Init()
{
	int numOfDevices = -1;
	int retVal = -1;
	this->isLive = false;
	cameraHandle = NULL;
	availableDevices = { 0 };

	trigger = true;//vedno pustimo na true. Deluje softwarski in hardwarski trigger
	flipCode = 0;
	//Priklopljen
	numOfDevices = AvailableDevices();

	if (numOfDevices > 0)
	{
		retVal = ConnectDevice(this->serialNumber);

		if (retVal != MV_OK)
			return retVal;
	}
	else
		return -1
		;
	InitImages(width, height, depth);

	if (depth == 1)
		videoFormat = (QString("MONO 8"));

	int nRet = MV_CC_SetBoolValue(cameraHandle, "AcquisitionFrameRateEnable", true);

	if (MV_OK != nRet)
	{
		//return nRet;
	}

	MV_CC_SetFloatValue(cameraHandle, "AcquisitionFrameRate", (float)FPS);

	if (MV_OK != nRet)
	{
		//return nRet;
	}

	return 1;
}

int Hikrobot::Start()
{
	int retVal = -1;
	MVCC_INTVALUE_EX struIntValue = { 0 };

	MVCC_INTVALUE_EX struIntValueHeight = { 0 };

	retVal = MV_CC_RegisterImageCallBackEx(cameraHandle, OnFrameReady, this);

	if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno izklapljanje samodejnega nastavljanja izpostave na Hikrobot kameri";
	}

	retVal = MV_CC_SetIntValueEx(cameraHandle, "Width", width);

	if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno izklapljanje samodejnega nastavljanja izpostave na Hikrobot kameri";
	}

	retVal = MV_CC_SetIntValueEx(cameraHandle, "Height", height);

	if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno izklapljanje samodejnega nastavljanja izpostave na Hikrobot kameri";
	}
	//retVal = MV_CC_GetIntValueEx(cameraHandle, "Width", &struIntValue);
	//retVal = MV_CC_GetIntValueEx(cameraHandle, "Height", &struIntValueHeight);

	//Rezerviramo spomin za slike
	GetGainAbsolute();
	GetExposureAbsolute();
	//SetGainAbsolute(currentGain);
	//SetExposureAbsolute(20000);*/

	//Zaženemo zajemanje slik
	//nastavimo se trigger hardware


	if (trigger)
	{
		retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerMode", MV_TRIGGER_MODE_ON);
		if (MV_OK != retVal)
		{
			qDebug() << "Neuspesno nastavljenja strojenga proženja";
		}
		retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerSource", MV_TRIGGER_SOURCE_LINE0);

		if (MV_OK != retVal)
		{
			qDebug() << "Neuspesno nastavljenja strojenga proženja";
		}
	}
	else
	{
		retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerMode", MV_TRIGGER_MODE_OFF);
	}

	//OPENCV
	/*flipcode = 0 : flip vertically.
		flipcode > 0 : flip horizontally.
		flipcode < 0 : flip vertically and horizontally.*/

	if ((rotatedVertical == false) && (rotadedHorizontal == false))
		flipCode = 2;//nastavim svojo kodo v primeru brez rotacije
	else if ((rotatedVertical == true) && (rotadedHorizontal == false))
		flipCode = 0;
	else if ((rotatedVertical == false) && (rotadedHorizontal == true))
		flipCode = 1;
	else if ((rotatedVertical == true) && (rotadedHorizontal == true))
		flipCode = -1;



	GetPartialOffsetX();
	GetPartialOffsetY();

	if (StartGrabber() == MV_OK)
		isLive = true;
	else
		isLive = false;

	return 1;
}

int Hikrobot::StartGrabber()
{
	int retVal = -1;

	//Začnemo z zajemom slik
	retVal = MV_CC_StartGrabbing(cameraHandle);

	if (retVal != MV_OK)
	{
		qDebug() << "Neuspesna inicializacija zajema slik";
		return retVal;
	}


	return retVal;
}

int Hikrobot::StopGrabber()
{
	int retVal = -1;

	//Končamo z zajemom slik
	retVal = MV_CC_StopGrabbing(cameraHandle);

	if (retVal != MV_OK)
	{
		qDebug() << "Neuspesna prekinitev zajema slik";
		//return retVal;
	}

	return 0;
}

int Hikrobot::GrabImage(int index)
{
	//StartGrabber();
	int retVal = -1;
	MV_FRAME_OUT grabbedFrame;
	bufferSize = height * width; //* 1 + 100000;
	//StartGrabber();
	imageIndex = index;

	retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerSource", MV_TRIGGER_SOURCE_SOFTWARE);


	retVal = MV_CC_SetCommandValue(cameraHandle, "TriggerSoftware");


	retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerSource", MV_TRIGGER_SOURCE_LINE0);
	/*retVal = MV_CC_GetImageBuffer(cameraHandle, &grabbedFrame, 1000);

	if (retVal != MV_OK)
	{
		qDebug() << "Neuspesen zajem slik iz Hikrobot kamere";
	}

	Mat mat(height, width, CV_8UC1);

	std::copy(&grabbedFrame.pBufAddr[0], &grabbedFrame.pBufAddr[0] + bufferSize, mat.data);
	cvtColor(mat, image[imageIndex].buffer[0], COLOR_GRAY2RGB);


	retVal = MV_CC_FreeImageBuffer(cameraHandle, &grabbedFrame);*/

	/*if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno sproscanje pomnilnika zajete slike iz Hikrobot kamere";
		return retVal;
	}*/

	//StopGrabber();
	return 0;
}

bool Hikrobot::SetGainAbsolute(double gain)
{
	int retVal = -1;
	uint8_t autoGainSetting = MV_GAIN_MODE_OFF;
	//Če je željeno ojačanje izven možnega območja, omejimo vrednost
	gain = (gain > maxGain) ? maxGain : gain;
	gain = (gain < minGain) ? minGain : gain;

	//Poskrbimo da je samodejna nastavitev ojačanja izklopljena
	retVal = MV_CC_SetEnumValue(cameraHandle, "GainAuto", autoGainSetting);

	/*if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno izklapljanje samodenjenga ojačanja na Hikrobot kameri";
		return false;
	}*/

	retVal = MV_CC_SetFloatValue(cameraHandle, "Gain", gain);

	/*if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno nastavljanje ojačanja za Hikrobot kamero";
		return false;
	}*/

	//Posodobimo trenutno vrednost ojačanja
	GetGainAbsolute();

	return true;
}

int Hikrobot::GetGainAbsolute()
{
	int retVal = -1;
	MVCC_FLOATVALUE gainInfo = { 0 };

	//Trenutno vrednost ojačanja (Gain)
	retVal = MV_CC_GetFloatValue(cameraHandle, "Gain", &gainInfo);

	/*if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno pridobivanj informacij o ojačanju Hikrobot kamere";
		return retVal;
	}*/

	maxGain = gainInfo.fMax;
	minGain = gainInfo.fMin;
	currentGain = gainInfo.fCurValue;

	return 0;
}

void Hikrobot::AutoGainOnce()
{
	int retVal = -1;
	uint8_t autoGainSetting = MV_GAIN_MODE_ONCE;


	retVal = MV_CC_SetEnumValue(cameraHandle, "GainAuto", autoGainSetting);

	/*if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno nastavljanje samodejenega ojačanja na Hikrobot kameri";
		return;
	}*/
}

void Hikrobot::AutoGainContinuous()
{
	int retVal = -1;
	uint8_t autoGainSetting = MV_GAIN_MODE_CONTINUOUS;

	retVal = MV_CC_SetEnumValue(cameraHandle, "GainAuto", autoGainSetting);

	if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno nastavljanje samodejenega ojačanja na Hikrobot kameri";
		return;
	}
}

bool Hikrobot::SetExposureAbsolute(double exposureTime)
{
	int retVal = -1;
	//MV_EXPOSURE_AUTO_MODE_OFF
	//Če je željen čas izpostave izven možnega območja, omejimo vrednost
	exposureTime = (exposureTime > maxExpo) ? maxExpo : exposureTime;
	exposureTime = (exposureTime < minExpo) ? minExpo : exposureTime;

	retVal = MV_CC_SetEnumValue(cameraHandle, "ExposureAuto", MV_EXPOSURE_AUTO_MODE_OFF);

	/*if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno izklapljanje samodejnega nastavljanja izpostave na Hikrobot kameri";
		return false;
	}*/

	//Nastavljanje časa izpostave v µs
	retVal = MV_CC_SetFloatValue(cameraHandle, "ExposureTime", exposureTime);

	/*if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno nastavljanje časa izpostave na Hikrobot kameri";
		return false;
	}*/

	//Posodobimo trenutno vrednost časa izpostave
	GetExposureAbsolute();

	return true;
}

double Hikrobot::GetExposureAbsolute()
{
	int retVal = -1;
	MVCC_FLOATVALUE exposureTimeInfo = { 0 };

	//Trenutna vrednost časa izpostave (exposure time)
	retVal = MV_CC_GetFloatValue(cameraHandle, "ExposureTime", &exposureTimeInfo);

	/*if (retVal != MV_OK)
	{
		qDebug() << "Neuspesno branje vrednosti časa izpostave";
		return -1.0;
	}*/

	maxExpo = exposureTimeInfo.fMax;
	minExpo = exposureTimeInfo.fMin;
	currentExposure = exposureTimeInfo.fCurValue;

	return currentExposure;
}

void Hikrobot::AutoExposureOnce()
{
}

void Hikrobot::AutoExposureContinuous()
{
}

bool Hikrobot::EnableLive()
{
	int retVal = 0;
	if (!enableLive) //vklopimo samo ce je enable live ni aktiven
	{
		if (trigger)
		{
			retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerMode", MV_TRIGGER_MODE_OFF);
			/*if (MV_OK != retVal)
			{
				//Ɖrŝntf(Η^Ğƚ TriggerMode fail!nRet[0x%x]\n", nRet);
				return retVal;
			}*/
			retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerSource", MV_TRIGGER_SOURCE_LINE0);
		}


		enableLive = true;


	}
	return true;
}

bool Hikrobot::DisableLive()
{
	int retVal = 0;
	if (enableLive) //izklopimo samo ce je enable live aktiven
	{
		StopGrabber();
		retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerMode", MV_TRIGGER_MODE_ON);

			retVal = MV_CC_SetEnumValue(cameraHandle, "TriggerSource", MV_TRIGGER_SOURCE_LINE0);
		


		enableLive = false;
		StartGrabber();

	}
	return true;
}

void Hikrobot::SnapOne()
{
	int retVal = 0;
	GrabImage(0);
}

int Hikrobot::ConnectDevice(QString serialIn)
{
	int retVal = -1;
	unsigned int nAccessMode = MV_ACCESS_Exclusive;
	unsigned short nSwitchoverKey = 0;
	char* serialOut;
	QString serialString;

	MV_CC_DEVICE_INFO *deviceInfo;

	//Povežemo se s Hikrobot kamero ki ima serijsko številko serialIn
	for (int i = 0; i < availableDevices.nDeviceNum; ++i)
	{
		retVal = MV_CC_CreateHandle(&cameraHandle, availableDevices.pDeviceInfo[i]);

		if (retVal != MV_OK)
		{
			qDebug() << "Neuspesno ustvarjanje objekta za povezavo na Hikrobot kamero";
			return retVal;
		}

		MVCC_INTVALUE heart;

		MV_CC_SetHeartBeatTimeout(cameraHandle, 5000);
		//Poveži se s kamero
		retVal = MV_CC_OpenDevice(cameraHandle, nAccessMode, nSwitchoverKey);

		if (retVal != MV_OK)
		{
			qDebug() << "Neuspesen poskus povezave s Hikrobot kamero";
			StopGrabber();

			retVal = MV_CC_CloseDevice(cameraHandle);

			if (retVal != MV_OK)
				qDebug() << "Nesupesno zapiranje povezave s Hikrobot kamero";

			//Uniči objekt za povezavo s kamero in sprosti pomnilnik
			retVal = MV_CC_DestroyHandle(cameraHandle);

			if (retVal != MV_OK)
				qDebug() << "Nesupesno unicenje objekta za povezavo s Hikrobot kamero";
			//return retVal;
		}
		//retVal = MV_CC_CreateHandle(&cameraHandle, availableDevices.pDeviceInfo[i]);
		//retVal = MV_CC_OpenDevice(cameraHandle, nAccessMode, nSwitchoverKey);

		//Podatki o povezani Hikrobot kameri
		//retVal = MV_CC_GetDeviceInfo(cameraHandle, deviceInfo);
		//Serijska številka kamere

		MV_CC_SetHeartBeatTimeout(cameraHandle, 5000);

		serialOut = (char*)availableDevices.pDeviceInfo[i]->SpecialInfo.stGigEInfo.chSerialNumber;
		//serialOut = (char*)availableDevices.pDeviceInfo[i].SpecialInfo.stGigEInfo.chSerialNumber;
		serialString = "";

		for (int j = 0; j < strlen(serialOut); ++j)
		{
			serialString.append(serialOut[j]);
		}

		//Preverimo če gre za pravo kamero
		if (serialString.compare(serialIn, Qt::CaseInsensitive) == 0)
		{
			vendorName = (char*)availableDevices.pDeviceInfo[i]->SpecialInfo.stGigEInfo.chManufacturerName;
			cameraName = (char*)availableDevices.pDeviceInfo[i]->SpecialInfo.stGigEInfo.chModelName;
			return 0;
		}

		else
		{
			//Zapri povezavo s Hikrobot kamero
			retVal = MV_CC_CloseDevice(cameraHandle);

			if (retVal != MV_OK)
				qDebug() << "Nesupesno zapiranje povezave s Hikrobot kamero";

			//Uniči objekt za povezavo s kamero in sprosti pomnilnik
			retVal = MV_CC_DestroyHandle(cameraHandle);

			if (retVal != MV_OK)
				qDebug() << "Nesupesno unicenje objekta za povezavo s Hikrobot kamero";
		}
	}
	return 0;

}
bool Hikrobot::SaveReferenceSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;
	//filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/ImagingSource_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	filePath = settingsPath;






	QSettings settings(filePath, QSettings::IniFormat);

	settings.beginGroup("Settings");
	const QStringList childKeys = settings.childKeys();
	if (childKeys.size() == 0)
	{
		settings.setValue("GAIN0", 0);
		settings.setValue("EXPO0", 0.0);
		settings.setValue("OFFSETX0", 0);
		settings.setValue("OFFSETY0", 0);
		exposure.push_back(0);
		gain.push_back(0);
		savedOffsetX.push_back(0);
		savedOffsetY.push_back(0);
	}
	else
	{

		QString tmp;
		for (int index = 0; index < num_images; index++)
		{
			tmp = QString("EXPO%1").arg(index);
			settings.setValue(tmp, currentExposure);
			tmp = QString("GAIN%1").arg(index);
			settings.setValue(tmp, currentGain);
			tmp = QString("OFFSETX%1").arg(index);
			settings.setValue(tmp, offsetX);
			tmp = QString("OFFSETY%1").arg(index);
			settings.setValue(tmp, offsetY);
		}


	}
	settings.endGroup();

	return 0;
	

}

bool Hikrobot::SaveReferenceSettings(int index)
{
	QString filePath;
	QStringList values;
	QVariant var;
	//filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/ImagingSource_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	filePath = settingsPath;






	QSettings settings(filePath, QSettings::IniFormat);

	settings.beginGroup("Settings");
	const QStringList childKeys = settings.childKeys();
	if (childKeys.size() == 0)
	{
		settings.setValue("GAIN0", 0);
		settings.setValue("EXPO0", 0.0);
		settings.setValue("OFFSETX0", 0);
		settings.setValue("OFFSETY0", 0);
		exposure.push_back(0);
		gain.push_back(0);
		savedOffsetX.push_back(0);
		savedOffsetY.push_back(0);
	}
	else
	{

		QString tmp;

		tmp = QString("EXPO%1").arg(index);
		settings.setValue(tmp, currentExposure);
		tmp = QString("GAIN%1").arg(index);
		settings.setValue(tmp, currentGain);
		tmp = QString("OFFSETX%1").arg(index);
		settings.setValue(tmp, offsetX);
		tmp = QString("OFFSETY%1").arg(index);
		settings.setValue(tmp, offsetY);


	}
	settings.endGroup();

	return 0;

	
}

bool Hikrobot::LoadReferenceSettings(int index)
{
	QString filePath;
	QStringList values;
	QVariant var;



	//filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/ImagingSource_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	filePath = settingsPath;



	QSettings settings(filePath, QSettings::IniFormat);

	settings.beginGroup("Settings");
	const QStringList childKeys = settings.childKeys();
	if (childKeys.size() == 0)
	{
		settings.setValue("GAIN0", 0);
		settings.setValue("EXPO0", 0.0);
		settings.setValue("OFFSETX0", 0);
		settings.setValue("OFFSETY0", 0);
		exposure.push_back(0);
		gain.push_back(0);
		savedOffsetX.push_back(0);
		savedOffsetY.push_back(0);

	}
	else
	{

		QString tmp;

		tmp = QString("EXPO%1").arg(index);
		exposure.push_back(settings.value(tmp).toFloat());
		tmp = QString("GAIN%1").arg(index);
		gain.push_back(settings.value(tmp).toInt());
		tmp = QString("OFFSETX%1").arg(index);
		savedOffsetX.push_back(settings.value(tmp).toInt());
		tmp = QString("OFFSETY%1").arg(index);
		savedOffsetY.push_back(settings.value(tmp).toInt());

	}
	settings.endGroup();

	return 0;
	
}

bool Hikrobot::LoadReferenceSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	//filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/ImagingSource_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	filePath = settingsPath;



	QSettings settings(filePath, QSettings::IniFormat);

	settings.beginGroup("Settings");
	const QStringList childKeys = settings.childKeys();
	if (childKeys.size() == 0)
	{
		settings.setValue("GAIN0", 0);
		settings.setValue("EXPO0", 0.0);
		settings.setValue("OFFSETX0", 0);
		settings.setValue("OFFSETY0", 0);
		exposure.push_back(0);
		gain.push_back(0);
		savedOffsetX.push_back(0);
		savedOffsetY.push_back(0);
	}
	else
	{
		for (int i = 0; i < num_images; i++)
		{
			QString tmp;

			tmp = QString("EXPO%1").arg(i);
			exposure.push_back(settings.value(tmp).toFloat());
			tmp = QString("GAIN%1").arg(i);
			gain.push_back(settings.value(tmp).toFloat());
			tmp = QString("OFFSETX%1").arg(i);
			savedOffsetX.push_back(settings.value(tmp).toInt());
			tmp = QString("OFFSETY%1").arg(i);
			savedOffsetY.push_back(settings.value(tmp).toInt());
		}
	}
	settings.endGroup();

	return 0;
}
bool Hikrobot::SetPartialOffsetX(int xOff)
{
	MVCC_INTVALUE curr;
	MV_CC_GetAOIoffsetX(cameraHandle, &curr);



	if (xOff < minOffsetX)
		xOff = minOffsetX;
	else if (xOff > maxOffsetX)
		xOff = maxOffsetX;

	offsetX = xOff;
	// Here we set the the exposure value.
	MV_CC_SetAOIoffsetX(cameraHandle, xOff);




	return 0;
}

int Hikrobot::GetPartialOffsetX()
{
	MVCC_INTVALUE curr;
	MV_CC_GetAOIoffsetX(cameraHandle, &curr);

	minOffsetX = curr.nMin;
	maxOffsetX = curr.nMax;

	return 0;
}

bool Hikrobot::SetPartialOffsetY(int yOff)
{

	if (yOff < minOffsetY)
		yOff = minOffsetY;
	else if (yOff > maxOffsetY)
		yOff = maxOffsetY;

	offsetY = yOff;
	// Here we set the the exposure value.
	MV_CC_SetAOIoffsetY(cameraHandle, yOff);




	return 0;
}

int Hikrobot::GetPartialOffsetY()
{
	MVCC_INTVALUE curr;
	MV_CC_GetAOIoffsetY(cameraHandle, &curr);

	minOffsetY = curr.nMin;
	maxOffsetY = curr.nMax;




	return 0;
}