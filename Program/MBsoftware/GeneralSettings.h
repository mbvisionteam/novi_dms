#pragma once

#include "stdafx.h"
#include <QWidget>
#include "ui_GeneralSettings.h"

class GeneralSettings : public QWidget
{
	Q_OBJECT

public:
	//GeneralSettings(QWidget *parent = Q_NULLPTR);
	GeneralSettings(QString referencePath);
	~GeneralSettings();
	void closeEvent(QCloseEvent * event);
	QString referencePath;
	QString filePath;

	int updateDraw;

	int workingMode;
	int nrDowelToStop;
	int workingModePresort;
	int presortSensorEnable;
	int rotatorSpeedMode; 
	QString speedSet;
	int delayToValve;
	int blowFactor;


	bool startCleanEnable;
	bool cleanEnable;
	int cleanPeriod;
	int cleanValve1Offset;
	int cleanValve2Offset;
	int cleanValve1Duration;
	int cleanValve2Duration;

	int presortSensorTimeBlow;
	int presortSensorOffset;
	int presortCameraTimeBlow;
	int presortCameraTimeOffset;
	int elevatorStartTime;
	int elevatorStopTime;
	int dowelStuckStartBlink;
	int dowelStuckStopMachine;

	bool presortStopEnable;
	int nrBadPresortToStop;

	void OnShowDialog();
	void SaveSettings();
	int LoadSettings();
	void SetState();

private:
	Ui::GeneralSettings ui;

	bool slovensko;

public slots:
	void	OnConfirm();
	void	OnRejected();




};
