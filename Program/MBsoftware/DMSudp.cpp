#include "stdafx.h"
#include "DMSudp.h"

#include <QWidget>

QTimer*										DMSudp::connectionTimer;
Measurand*									DMSudp::measureObject; //object with mesurand parameters
std::vector<Timer*>				DMSudp::processingTimer;






DMSudp::DMSudp(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);


}

DMSudp::DMSudp(QString ipAddress, int port, QString referencePath)
{

	ui.setupUi(this);
	this->port = port;
	this->ipAdd = ipAddress;
	this->referencePath = referencePath;
	transitionTestWidget = new(QWidget);
	ui2.setupUi(transitionTestWidget);
	isLive = 0;
	selectedCameraDraw = 1;
	lastPieceScanned = 0;
	isLiveTransitions = 0;
	isTransitionDisplaySet = 0;
	for(int i = 0; i < NR_DMS_CAM; i++)
	{
		sceneVideo[i] = new QGraphicsScene(this);
		pixmapVideo[i] = new QGraphicsPixmapItem();
	}
	
	ui.imageView0->setScene(sceneVideo[0]);
	ui.imageView1->setScene(sceneVideo[1]);
	ui.imageView2->setScene(sceneVideo[2]);  
	ui.imageView3->setScene(sceneVideo[3]);
	ui.imageView4->setScene(sceneVideo[4]);
	ui.imageView5->setScene(sceneVideo[5]);
	
	for(int i = 0; i < NR_DMS_CAM; i++)
	sceneVideo[i]->addItem(pixmapVideo[i]);

	sceneDowel = new QGraphicsScene(this);
	pixmapDowel = new QGraphicsPixmapItem();

	ui2.dowelView->setScene(sceneDowel);
	sceneDowel->addItem(pixmapDowel);

	ui2.dowelView->fitInView(sceneDowel->sceneRect(), Qt::KeepAspectRatio);

	sceneTimeLine = new QGraphicsScene(this); 
	pixmapTimeLine = new QGraphicsPixmapItem();

	ui2.timeLineView->setScene(sceneTimeLine);
	sceneTimeLine->addItem(pixmapTimeLine);

	sceneHorizontalView = new QGraphicsScene(this); ;
	pixmapHorizontalView  = new QGraphicsPixmapItem();

	ui2.horizontalView->setScene(sceneHorizontalView);
	sceneHorizontalView->addItem(pixmapHorizontalView);

	sceneBlowView = new QGraphicsScene(this); ;
	pixmapBlowView = new QGraphicsPixmapItem();

	ui2.viewDowelBlow->setScene(sceneBlowView);
	sceneBlowView->addItem(pixmapBlowView);

	sceneDowelImage = new QGraphicsScene(this); ;
	pixmapDowelImage = new QGraphicsPixmapItem();

	ui2.dowelView->setRenderHint(QPainter::RenderHint::HighQualityAntialiasing);
	ui2.graphicsView->setScene(sceneDowelImage);
	
	//sceneDowelImage->addItem(pixmapDowelImage);



	isInitialized = false;
	//udpClient = new QUdpSocket();
		hostIp.setAddress(ipAddress);
	udpClient = new QUdpSocket();
	connectionOnCounter = 0;
	isConnectedOld = 0;
	isConnected = 0;
	dmsConnected = 0;
	currentDowelDraw = 0;

	this->port = port;
	viewTimer = new QTimer();
	connectionTimer = new QTimer();
	widthDmsCam = 2200;
	nrRecievedDowels = 0;
	//tcpClient->connectToHost(ipAddress, port);
	timout = 5;
	waitTime = 1000;

	parseCounter = 0;
	horLineCurrMeasurement = 0;
	parseCommandCounter = 0;

						isCheckedShowArea = true;
						isCheckedShowTolerance = true;
						isCheckedShowConusLines = true;
						isCheckedShowDowel = true;
						isCheckedShowMeasurements = false;

						ui2.checkShowArea->setChecked(isCheckedShowArea);
						ui2.checkShowBoarders->setChecked(isCheckedShowTolerance);
						ui2.checkShowDowel->setChecked(isCheckedShowDowel);
						ui2.checkShowKonus->setChecked(isCheckedShowConusLines);
						ui2.checkShowMeasurements->setChecked(isCheckedShowMeasurements);
				

	for (int i  = 0; i < NR_DMS_CAM; i++)
	{
	
		gain[i] = 0;
		offset[i] = 0;
		detectWindow[i][0] = 0;
		detectWindow[i][1] = 0;
	}
	CreateDisplay();
	CreateDisplayTransitionTest();
	StartClient();
	ReadCamSettings();
	SetDisplay();

	CreateMeasurementTable();
	CreateDowelImage();

	ui2.comboBoxSelectCamera->addItem("Camera1");
	ui2.comboBoxSelectCamera->addItem("Camera2");
	ui2.comboBoxSelectCamera->addItem("Camera3");
	ui2.comboBoxSelectCamera->addItem("Camera4");
	ui2.comboBoxSelectCamera->addItem("Camera5");

	connect(ui.buttonLive, SIGNAL(pressed()), this, SLOT(OnClickedLive()));
	connect(viewTimer, SIGNAL(timeout()), this, SLOT(OnViewTimer()));
	connect(udpClient, SIGNAL(connected()), this, SLOT(OnClientConnected()));
	connect(connectionTimer, SIGNAL(timeout()), this, SLOT(OnConnectionTimeout()));
	connect(this, SIGNAL(frameReadySignal(int, QByteArray)), this, SLOT(ShowImage(int,QByteArray)));
	connect(this, SIGNAL(transitionReadySignal(int, QByteArray)), this, SLOT(ShowTransitions(int,QByteArray)));
	connect(this, SIGNAL(timeLineSignal(QByteArray)), this, SLOT(ShowTimeLine(QByteArray)));
	connect(this, SIGNAL(versionSignal(QByteArray)), this, SLOT(OnSignalXilinxVersion(QByteArray)));
	connect(ui.buttonSend, SIGNAL(pressed()), this, SLOT(OnSend()));
	connect(ui.lineSend, SIGNAL(returnPressed()), this, SLOT(OnSend()));
	connect(ui.buttonShowDowel, SIGNAL(pressed()), this, SLOT(ReadDowelString()));
	connect(ui2.buttonEnableTest, SIGNAL(pressed()), this, SLOT(OnClickedLiveTranstions()));
	connect(ui2.buttonGetTimeLine, SIGNAL(pressed()), this, SLOT(OnClickedGetTimeBase()));
	connect(ui2.checkShowBoarders, SIGNAL(toggled(bool)), this, SLOT(OnToggledCheckBox1(bool)));
	connect(ui2.checkShowArea, SIGNAL(toggled(bool)), this, SLOT(OnToggledCheckBox2(bool)));
	connect(ui2.checkShowDowel, SIGNAL(toggled(bool)), this, SLOT(OnToggledCheckBox3(bool)));
	connect(ui2.checkShowKonus, SIGNAL(toggled(bool)), this, SLOT(OnToggledCheckBox4(bool)));
	connect(ui2.checkShowMeasurements, SIGNAL(toggled(bool)), this, SLOT(OnToggledCheckBox5(bool)));
	connect(ui2.buttonHorCamUp, SIGNAL(pressed()), this, SLOT(OnClickedNextHorMeasurement()));
	connect(ui2.buttonHorCamDown, SIGNAL(pressed()), this, SLOT(OnClickedPrevHorMeasurement()));
	connect(ui2.buttonHistoryUp, SIGNAL(pressed()), this, SLOT(OnClickedHistoryUp()));
	connect(ui2.buttonHistory, SIGNAL(pressed()), this, SLOT(OnClickedHistoryDown()));
	connect(ui2.buttonHistoryLast, SIGNAL(pressed()), this, SLOT(OnClickedLastScanned()));
	connect(ui2.buttonFrameUp, SIGNAL(pressed()), this, SLOT(OnClickedFrameUp()));
	connect(ui2.buttonFrameDown, SIGNAL(pressed()), this, SLOT(OnClickedFrameDown()));

	connect(ui2.comboBoxSelectCamera, SIGNAL(currentIndexChanged(int)), this, SLOT(OnClickedSelectedDMSCamNew()));



	for (int i = 0; i < DMSHISTORYSIZE; i++)
	{
		processingTimer.push_back(new Timer());
	}

	//udpClient->bind(port);
	//udpClient->connectToHost(hostIp, port, QIODevice::ReadWrite);
	connectionTimer->start(1000);

	udpClient->setReadBufferSize(65535);
	

	for (int i = 0; i < 16; i++)
	{
		dowelReady[i] = 0;
	}
	

	for (int i = 0; i < 12; i++)
	{
		ui2.list->addItem("");
	}

	historyCounter = 0;
	frameUpCounter = 0;


}


DMSudp::~DMSudp()
{
	for (int i = 0; i < processingTimer.size(); i++)
	{
		delete (processingTimer[i]);
	}
}

void DMSudp::ConnectMeasurand(Measurand * objects)
{

	measureObject = objects;

}


void DMSudp::OnShowDialog()
{
	show();
	viewTimer->start(100);
}
void DMSudp::OnShowDialogTransitionTest()
{
	transitionTestWidget->show();
	historyCounter = 0;
	frameUpCounter = 0;
	ShowDowel(lastPieceScanned);
	DrawMeasurementTable(lastPieceScanned);
	ShowMeasurements(lastPieceScanned);
	UpdateDowelImage(lastPieceScanned);
	shownDowel = lastPieceScanned;
	UpdateList();
	ShowHorizontalCamera(shownDowel, 0);
	ShowBlowImage(lastPieceScanned, frameUpCounter);

}

void DMSudp::CreateDisplay()
{
	int singleStep = 50;
	int maxValueGO = 4096;
	int maxValueWidth= 2200;

	QFont spinFont("MS Shell Dlg 2", 12, QFont::Bold);
	
	QVBoxLayout *stopLayout[NR_DMS_CAM];
	QVBoxLayout *startLayout[NR_DMS_CAM];
	QVBoxLayout *gainLayout[NR_DMS_CAM];
	QVBoxLayout *offsetLayout[NR_DMS_CAM];
	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		stopLayout[i] = new QVBoxLayout;
		startLayout[i] = new QVBoxLayout;
		gainLayout[i] = new QVBoxLayout;
		offsetLayout[i] = new QVBoxLayout;
		spinWindwSTOP[i] = new QSpinBox;
		spinWindwSTART[i] = new QSpinBox;
		spinGain[i] = new QSpinBox;
		spinOffset[i] = new QSpinBox;

		spinWindwSTOP[i]->setMinimumHeight(31);
		spinWindwSTOP[i]->setFont(spinFont);
		spinWindwSTOP[i]->setMaximum(maxValueWidth);
		spinWindwSTOP[i]->setMinimum(0);
		spinWindwSTOP[i]->setSingleStep(singleStep);

		spinWindwSTART[i]->setMinimumHeight(31);
		spinWindwSTART[i]->setFont(spinFont);
		spinWindwSTART[i]->setMaximum(maxValueWidth);
		spinWindwSTART[i]->setMinimum(0);
		spinWindwSTART[i]->setSingleStep(singleStep);

		spinGain[i]->setMinimumHeight(31);
		spinGain[i]->setFont(spinFont);
		spinGain[i]->setMaximum(maxValueGO);
		spinGain[i]->setMinimum(0);
		spinGain[i]->setSingleStep(singleStep);

		spinOffset[i]->setMinimumHeight(31);
		spinOffset[i]->setFont(spinFont);
		spinOffset[i]->setMaximum(maxValueGO);
		spinOffset[i]->setMinimum(0);
		spinOffset[i]->setSingleStep(singleStep);

		stopLayout[i]->addWidget(spinWindwSTOP[i]);
		startLayout[i]->addWidget(spinWindwSTART[i]);
		gainLayout[i]->addWidget(spinGain[i]);
		offsetLayout[i]->addWidget(spinOffset[i]);
	}

	ui.groupBoxStop0->setLayout(stopLayout[0]);
	ui.groupBoxStart0->setLayout(startLayout[0]);
	ui.groupBoxGain0->setLayout(gainLayout[0]);
	ui.groupBoxOffset0->setLayout(offsetLayout[0]);

	ui.groupBoxStop1->setLayout(stopLayout[1]);
	ui.groupBoxStart1->setLayout(startLayout[1]);
	ui.groupBoxGain1->setLayout(gainLayout[1]);
	ui.groupBoxOffset1->setLayout(offsetLayout[1]);

	ui.groupBoxStop2->setLayout(stopLayout[2]);
	ui.groupBoxStart2->setLayout(startLayout[2]);
	ui.groupBoxGain2->setLayout(gainLayout[2]);
	ui.groupBoxOffset2->setLayout(offsetLayout[2]);

	ui.groupBoxStop3->setLayout(stopLayout[3]);
	ui.groupBoxStart3->setLayout(startLayout[3]);
	ui.groupBoxGain3->setLayout(gainLayout[3]);
	ui.groupBoxOffset3->setLayout(offsetLayout[3]);

	ui.groupBoxStop4->setLayout(stopLayout[4]);
	ui.groupBoxStart4->setLayout(startLayout[4]);
	ui.groupBoxGain4->setLayout(gainLayout[4]);
	ui.groupBoxOffset4->setLayout(offsetLayout[4]);

	ui.groupBoxStop5->setLayout(stopLayout[5]);
	ui.groupBoxStart5->setLayout(startLayout[5]);
	ui.groupBoxGain5->setLayout(gainLayout[5]);
	ui.groupBoxOffset5->setLayout(offsetLayout[5]);

	
	connect(spinGain[0], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(0, val); });
	connect(spinGain[1], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(1, val); });
	connect(spinGain[2], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(2, val); });
	connect(spinGain[3], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(3, val); });
	connect(spinGain[4], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(4, val); });
	connect(spinGain[5], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(5, val); });

	connect(spinOffset[0], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(0, val); });
	connect(spinOffset[1], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(1, val); });
	connect(spinOffset[2], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(2, val); });
	connect(spinOffset[3], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(3, val); });
	connect(spinOffset[4], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(4, val); });
	connect(spinOffset[5], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(5, val); });

	connect(spinWindwSTART[0], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(0,0, val); });
	connect(spinWindwSTART[1], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(1, 0, val); });
	connect(spinWindwSTART[2], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(2, 0, val); });
	connect(spinWindwSTART[3], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(3, 0, val); });
	connect(spinWindwSTART[4], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(4, 0, val); });
	connect(spinWindwSTART[5], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(5, 0, val); });

	connect(spinWindwSTOP[0], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(0, 1, val); });
	connect(spinWindwSTOP[1], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(1, 1, val); });
	connect(spinWindwSTOP[2], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(2, 1, val); });
	connect(spinWindwSTOP[3], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(3, 1, val); });
	connect(spinWindwSTOP[4], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(4, 1, val); });
	connect(spinWindwSTOP[5], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(5, 1, val); });



}
void DMSudp::CreateDisplayTransitionTest()
{

	QWidget *widget = new QWidget();
	ui2.scrollAreaMeasurements->setWidget(widget);
	ui2.scrollAreaMeasurements->setWidgetResizable(true);
	QGridLayout *layout = new QGridLayout();
	widget->setLayout(layout);




	for (int i = 0; i < MAX_MEASUREMENT; i++)
	{
		labelNrMeasurement[i] = new QLabel;
		labelNrMeasurement[i]->setMinimumHeight(20);
		labelNrMeasurement[i]->setFrameShape(QFrame::Box);
		layout->addWidget(labelNrMeasurement[i], i, 0);
		labelNrMeasurementPosition[i] = new QLabel;
		labelNrMeasurementPosition[i]->setMinimumHeight(20);
		labelNrMeasurementPosition[i]->setFrameShape(QFrame::Box);
		layout->addWidget(labelNrMeasurementPosition[i], i, 1);
		for (int j = 0; j < NR_DMS_CAM; j++)
		{
			labelCameraMeaurements[i][j] = new QLabel;
			labelCameraMeaurements[i][j]->setMinimumHeight(20);
			labelCameraMeaurements[i][j]->setFrameShape(QFrame::Box);
			layout->addWidget(labelCameraMeaurements[i][j], i, j + 2);

		}
	}

	
	isTransitionDisplaySet = 1;
}
void DMSudp::CreateMeasurementTable()
//v priemru DMS naredim 2 tabeli za parametere vseh kamer in ostalo
{
	int  nrParam = 18;
	int nrCollum = 8;
	parametersTable = new QTableView();
	modelTable = new QStandardItemModel();

	parametersTable->setFont(QFont("Times new Roman", 12));
	modelTable->setColumnCount(8);
	modelTable->setHeaderData(0, Qt::Horizontal, tr("param"));
	modelTable->setHeaderData(1, Qt::Horizontal, tr("CAM1"));
	modelTable->setHeaderData(2, Qt::Horizontal, tr("CAM2"));
	modelTable->setHeaderData(3, Qt::Horizontal, tr("CAM3"));
	modelTable->setHeaderData(4, Qt::Horizontal, tr("CAM4"));
	modelTable->setHeaderData(5, Qt::Horizontal, tr("CAM5"));
	modelTable->setHeaderData(6, Qt::Horizontal, tr("MIN"));
	modelTable->setHeaderData(7, Qt::Horizontal, tr("MAX"));
	modelTable->setHeaderData(8, Qt::Horizontal, tr("BAD Proc"));

	modelTable->setRowCount(nrParam);
	parametersTable->setModel(modelTable);

	for (int i = 0; i < nrParam; i++)
	{
		parametersTable->setRowHeight(i, 21);

	}
	standardTableItem.resize(nrParam);

	for (int k = 0; k < nrParam; k++)
	{

		for (int z = 0; z < nrCollum; z++)
		{
			standardTableItem[k].push_back(new QStandardItem());
			modelTable->setItem(k, z, standardTableItem[k].back());
			standardTableItem[k].back()->setText(QString(""));

		}
	}
	parametersTable->setColumnWidth(0, 105);
	for (int i = 1; i < nrCollum; i++)
	{
		parametersTable->setColumnWidth(i, 61);
	}

	int w = parametersTable->width();
	int h = parametersTable->height();
	parametersTable->height();

	ui2.layoutMeasurements->addWidget(parametersTable, Qt::AlignCenter);

}
void DMSudp::DrawMeasurementTable(int index)
{
	float value;
	QString valueTmp;
	QColor goodColor = QColor(150, 255, 150);
	QColor badColor = QColor(255, 150, 150);
	QColor NotActiveColor = QColor(236, 236, 236);
	QColor conditionalColor = QColor(0, 170, 255);


	float badProc;

			for (int row = 0; row < modelTable->rowCount(); row++)
			{
				for (int column = 0; column < modelTable->columnCount(); column++)
				{
					switch (column)
					{
					case 0:
						standardTableItem[row][column]->setText(QString("%1").arg(measureObject->parameterNameHistory[row]));
						break;
					case 1:
						standardTableItem[row][column]->setText(QString("%1").number(measureObject->measureValueHistory[index][row][1], 'f', 2));
						break;
					case 2:
						standardTableItem[row][column]->setText(QString("%1").number(measureObject->measureValueHistory[index][row][2], 'f', 2));
						break;
					case 3:
						standardTableItem[row][column]->setText(QString("%1").number(measureObject->measureValueHistory[index][row][3], 'f', 2));
						break;
					case 4:
						standardTableItem[row][column]->setText(QString("%1").number(measureObject->measureValueHistory[index][row][4], 'f', 2));
						break;
					case 5:
						standardTableItem[row][column]->setText(QString("%1").number(measureObject->measureValueHistory[index][row][5], 'f', 2));
						break;

					case 6:

							standardTableItem[row][column]->setText(QString("%1").number(measureObject->toleranceHistoryLOW[index][row],'f',2));

						break;

					case 7:

							standardTableItem[row][column]->setText(QString("%1").number(measureObject->toleranceHistoryHIGH[index][row], 'f', 2));

						break;

					default:
						break;
					}

				}

			}
			int rowGood = 1;
			//obarva vrstice dobre slabe
			for (int row = 0; row < modelTable->rowCount(); row++)
			{
				rowGood = 1;
				//types[i][currentType]->IsGood(row);
				
					for (int k = 0; k < 5; k++)
					{
						if (measureObject->isGoodHistory[index][row][k+1] == 1) //mera dobra
						{
							standardTableItem[row][k + 1]->setBackground(QBrush(goodColor));

						}
						else if (measureObject->isGoodHistory[index][row][k+1] == 0)// mera slaba
						{
							standardTableItem[row][k + 1]->setBackground(QBrush(badColor));
						}

					}


					if (rowGood == 1)
					{
						standardTableItem[row][0]->setBackground(QBrush(goodColor));
					}
					else
					{
						standardTableItem[row][0]->setBackground(QBrush(badColor));
					}

	

			}
}
void DMSudp::CreateDowelImage()
{

	QGraphicsLineItem* line[6];
	QColor badColor = QColor(255, 150, 150);
	QColor goodColor = QColor(150, 255, 150);
	QPen linePen(QColor(68, 114, 196), 6, Qt::DashDotLine, Qt::RoundCap);

	QString path;
	//path = QDir::currentPath() + "/res/dowel4.bmp";
	path = QDir::currentPath() + "/res/slikaDowel.bmp";
	QImage image;
	image.load(path, 0);
	QGraphicsPixmapItem * slika;


	pixmapDowelImage = sceneDowelImage->addPixmap(QPixmap::fromImage(image));
	
	//slika = sceneDowelImage->addPixmap(QPixmap::fromImage(image));
	ui2.graphicsView->fitInView(QRectF(sceneDowelImage->sceneRect()), Qt::IgnoreAspectRatio);

	int currentPiece = 1;

	QPolygon areaTmp[11];


	QFont timesNewRomanFont("Times New Roman", 40, QFont::Bold);

	QLine vertLine[4], horLine[2];
	int offsety = 194 - 102;
	//start konus Top
	areaTmp[0].append(QPoint(159, 200));
	areaTmp[0].append(QPoint(258, 102));
	areaTmp[0].append(QPoint(258, 200));

	//start konus BOttom
	areaTmp[1].append(QPoint(159, 337));
	areaTmp[1].append(QPoint(258, 337));
	areaTmp[1].append(QPoint(258, 434));

	//EDGE KONUS START  TOP 
	areaTmp[2].append(QPoint(370, 102));
	areaTmp[2].append(QPoint(370, 200));
	areaTmp[2].append(QPoint(257, 200));
	areaTmp[2].append(QPoint(257, 102));

	//EDGE KONUS START BOTTOM
	areaTmp[3].append(QPoint(257, 337));
	areaTmp[3].append(QPoint(370, 337));
	areaTmp[3].append(QPoint(370, 434));
	areaTmp[3].append(QPoint(257, 434));

	//MIDDLE TOP
	areaTmp[4].append(QPoint(370, 102));
	areaTmp[4].append(QPoint(917, 102));
	areaTmp[4].append(QPoint(917, 200));
	areaTmp[4].append(QPoint(370, 200));


	//MIDDLE BOTTOM
	areaTmp[5].append(QPoint(370, 337));
	areaTmp[5].append(QPoint(917, 337));
	areaTmp[5].append(QPoint(917, 434));
	areaTmp[5].append(QPoint(370, 434));

	//EDGE KONUS STOP UP
	areaTmp[6].append(QPoint(917, 102));
	areaTmp[6].append(QPoint(1023, 102));
	areaTmp[6].append(QPoint(1023, 200));
	areaTmp[6].append(QPoint(917, 200));

	//EDGE KONUS STOP BOTTOM
	areaTmp[7].append(QPoint(917, 337));
	areaTmp[7].append(QPoint(1023, 337));
	areaTmp[7].append(QPoint(1023, 434));
	areaTmp[7].append(QPoint(917, 434));

	//KONUS STOP UP
	areaTmp[8].append(QPoint(1023, 102));
	areaTmp[8].append(QPoint(1120, 200));
	areaTmp[8].append(QPoint(1023, 200));

	//KONUS STOP BOTTOM
	areaTmp[9].append(QPoint(1023, 337));
	areaTmp[9].append(QPoint(1120, 337));
	areaTmp[9].append(QPoint(1023, 434));

	//LENGHT AREA
	areaTmp[10].append(QPoint(160, 200));
	areaTmp[10].append(QPoint(1120, 200));
	areaTmp[10].append(QPoint(1120, 337));
	areaTmp[10].append(QPoint(160, 337));

	vertLine[0].setLine(257, 10, 257, 460);
	vertLine[1].setLine(370, 10, 370, 460);
	vertLine[2].setLine(917, 10, 917, 460);
	vertLine[3].setLine(1023, 10, 1023, 460);
	horLine[0].setLine(30, 200, 1250, 200);
	horLine[1].setLine(30, 337, 1250, 337);

	for (int i = 0; i < 11; i++)
	{
		area[i] = sceneDowelImage->addPolygon(areaTmp[i], QPen(goodColor), QBrush(goodColor));

	}
	area[1]->setBrush(badColor);
	area[2]->setBrush(goodColor);
	area[3]->setBrush(badColor);
	area[4]->setBrush(goodColor);
	area[5]->setBrush(badColor);
	area[6]->setBrush(goodColor);
	area[7]->setBrush(badColor);
	area[8]->setBrush(goodColor);
	area[9]->setBrush(badColor);
	area[10]->setBrush(goodColor);
	for (int i = 0; i < 4; i++)
	{
		line[i] = sceneDowelImage->addLine(vertLine[i], linePen);
		line[i]->setZValue(1);
	}
	line[4] = sceneDowelImage->addLine(horLine[0], linePen);
	line[5] = sceneDowelImage->addLine(horLine[1], linePen);
	line[4]->setZValue(1);
	line[5]->setZValue(1);


	timesNewRomanFont.setPointSize(30);
	QGraphicsTextItem *textE = sceneDowelImage->addText("Err.:", timesNewRomanFont);
	textE->setPos(10, 20);

	maxErrorText[0] = sceneDowelImage->addText("5 / 3", timesNewRomanFont);
	maxErrorText[0]->setPos(160, 5);
	negativeErrorText[0] = sceneDowelImage->addText("0 / 1", timesNewRomanFont);
	negativeErrorText[0]->setPos(160, 40);

	maxErrorText[1] = sceneDowelImage->addText("2 / 5", timesNewRomanFont);
	maxErrorText[1]->setPos(270, 5);
	negativeErrorText[1] = sceneDowelImage->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[1]->setPos(270, 40);

	maxErrorText[2] = sceneDowelImage->addText("2 / 5", timesNewRomanFont);
	maxErrorText[2]->setPos(580, 5);
	negativeErrorText[2] = sceneDowelImage->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[2]->setPos(580, 40);

	maxErrorText[3] = sceneDowelImage->addText("2 / 5", timesNewRomanFont);
	maxErrorText[3]->setPos(930, 5);
	negativeErrorText[3] = sceneDowelImage->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[3]->setPos(930, 40);

	maxErrorText[4] = sceneDowelImage->addText("2 / 5", timesNewRomanFont);
	maxErrorText[4]->setPos(1040, 5);
	negativeErrorText[4] = sceneDowelImage->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[4]->setPos(1040, 40);

	maxErrorText[5] = sceneDowelImage->addText("2 / 5", timesNewRomanFont);
	maxErrorText[5]->setPos(1150, 230);
	negativeErrorText[5] = sceneDowelImage->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[5]->setPos(1150, 270);


	timesNewRomanFont.setPointSize(25);
	nrMeasurementesText[0] = sceneDowelImage->addText("00", timesNewRomanFont);
	nrMeasurementesText[0]->setPos(200, 340);
	nrMeasurementesText[1] = sceneDowelImage->addText("11", timesNewRomanFont);
	nrMeasurementesText[1]->setPos(285, 340);
	nrMeasurementesText[2] = sceneDowelImage->addText("22", timesNewRomanFont);
	nrMeasurementesText[2]->setPos(590, 340);
	nrMeasurementesText[3] = sceneDowelImage->addText("33", timesNewRomanFont);
	nrMeasurementesText[3]->setPos(940, 340);
	nrMeasurementesText[4] = sceneDowelImage->addText("44", timesNewRomanFont);
	nrMeasurementesText[4]->setPos(1050, 340);
	nrMeasurementesText[5] = sceneDowelImage->addText("55", timesNewRomanFont);
	nrMeasurementesText[5]->setPos(200, 220);

	timesNewRomanFont.setPointSize(35);
	speedMeasurementText = sceneDowelImage->addText("Speed: 1.23 [m/s]", timesNewRomanFont);
	speedMeasurementText->setPos(820, 650);

	timesNewRomanFont.setPointSize(30);
	lenghtMeasurementText = sceneDowelImage->addText("Lenght = 30.00 [mm]", timesNewRomanFont);
	lenghtMeasurementText->setPos(500, 230);

	maxMeasurementText = sceneDowelImage->addText("Measurements: 123 MIN: 90 MAX: 150", timesNewRomanFont);
	maxMeasurementText->setPos(10, 650);

	elipseText = sceneDowelImage->addText("Elipse: 0.5 [mm]", timesNewRomanFont);
	elipseText->setPos(170, 540);


}

void DMSudp::UpdateDowelImage(int currPiece)
{

	QString tmp;

	QColor badColor = QColor(255, 150, 150);
	QColor goodColor = QColor(150, 255, 150);
	QFont calibriFont("TImes New Roman", 40, QFont::Bold);

	tmp = QString("%1").number(measureObject->dowelSpeed[currPiece], 'f', 2);
	speedMeasurementText->setPlainText(QString("Speed: %1 [m/s]").arg(tmp));

	tmp = QString("%1").number(measureObject->dowelLenght[currPiece], 'f', 2);
	lenghtMeasurementText->setPlainText(QString("Lenght: %1 [mm]").arg(tmp));


	maxMeasurementText->setPlainText(QString("Measure: %1 MIN: %2 MAX: %3 ").arg(measureObject->nrDowelMeasurements[currPiece]).arg(measureObject->dowelAllowedMeasurements[currPiece][0]).arg(measureObject->dowelAllowedMeasurements[currPiece][1]));

	nrMeasurementesText[0]->setPlainText(QString("%1").arg(measureObject->nrMeasurementsArea0[currPiece][1]));
	nrMeasurementesText[1]->setPlainText(QString("%1").arg(measureObject->nrMeasurementsArea1[currPiece][1]));
	nrMeasurementesText[2]->setPlainText(QString("%1").arg(measureObject->nrMeasurementsArea2[currPiece][1]));
	nrMeasurementesText[3]->setPlainText(QString("%1").arg(measureObject->nrMeasurementsArea3[currPiece][1]));
	nrMeasurementesText[4]->setPlainText(QString("%1").arg(measureObject->nrMeasurementsArea4[currPiece][1]));
	//dodaj se za meritev dolzin 
	//nrMeasurementesText[5]->setPlainText(QString("%1").arg(measureObject->nrMeasurementsArea5[currPiece][1]));


	int maxAll[5] = { 0,0,0,0,0 };
	int maxMinus[5] = { 0,0,0,0,0 };
	for (int i = 1; i < 6; i++)
	{
		if (measureObject->nrBadMeasurementsDSKAll[currPiece][i] > maxAll[0])
			maxAll[0] = measureObject->nrBadMeasurementsDSKAll[currPiece][i];

		if (measureObject->nrBadMeasurementsDSKMinus[currPiece][i] > maxMinus[0])
			maxMinus[0] = measureObject->nrBadMeasurementsDSKMinus[currPiece][i];

		if (measureObject->nrBadMeasurementsDISAll[currPiece][i] > maxAll[1])
			maxAll[1] = measureObject->nrBadMeasurementsDISAll[currPiece][i];

		if (measureObject->nrBadMeasurementsDISMinus[currPiece][i] > maxMinus[1])
			maxMinus[1] = measureObject->nrBadMeasurementsDISMinus[currPiece][i];

		if (measureObject->nrBadMeasurementsDICAll[currPiece][i] > maxAll[2])
			maxAll[2] = measureObject->nrBadMeasurementsDICAll[currPiece][i];

		if (measureObject->nrBadMeasurementsDICMinus[currPiece][i] > maxMinus[2])
			maxMinus[2] = measureObject->nrBadMeasurementsDICMinus[currPiece][i];

		if (measureObject->nrBadMeasurementsDIEAll[currPiece][i] > maxAll[3])
			maxAll[3] = measureObject->nrBadMeasurementsDIEAll[currPiece][i];

		if (measureObject->nrBadMeasurementsDIEMinus[currPiece][i] > maxMinus[3])
			maxMinus[3] = measureObject->nrBadMeasurementsDIEMinus[currPiece][i];

		if (measureObject->nrBadMeasurementsDKEAll[currPiece][i] > maxAll[4])
			maxAll[4] = measureObject->nrBadMeasurementsDKEAll[currPiece][i];

		if (measureObject->nrBadMeasurementsDKEMinus[currPiece][i] > maxMinus[4])
			maxMinus[4] = measureObject->nrBadMeasurementsDKEMinus[currPiece][i];
	}

	maxErrorText[0]->setPlainText(QString("%1 / %2").arg(maxAll[0]).arg(measureObject->nrEKonusAll));
	negativeErrorText[0]->setPlainText(QString("%1 / %2").arg(maxMinus[0]).arg(measureObject->nrEKonusMinus));


	maxErrorText[1]->setPlainText(QString("%1 / %2").arg(maxAll[1]).arg(measureObject->nrEEdgeAll));
	negativeErrorText[1]->setPlainText(QString("%1 / %2").arg(maxMinus[1]).arg(measureObject->nrEdgeMinus));

	maxErrorText[2]->setPlainText(QString("%1 / %2").arg(maxAll[2]).arg(measureObject->nrEMiddleAll));
	negativeErrorText[2]->setPlainText(QString("%1 / %2").arg(maxMinus[2]).arg(measureObject->nrEMiddleMinus));


	maxErrorText[3]->setPlainText(QString("%1 / %2").arg(maxAll[3]).arg(measureObject->nrEEdgeAll));
	negativeErrorText[3]->setPlainText(QString("%1 / %2").arg(maxMinus[3]).arg(measureObject->nrEdgeMinus));

	maxErrorText[4]->setPlainText(QString("%1 / %2").arg(maxAll[4]).arg(measureObject->nrEKonusAll));
	negativeErrorText[4]->setPlainText(QString("%1 / %2").arg(maxMinus[4]).arg(measureObject->nrEKonusMinus));

	maxErrorText[5]->setPlainText(QString("%1 / %2").arg(maxAll[4]).arg(measureObject->nrELenghtDowel));
	negativeErrorText[5]->setPlainText(QString("%1 / %2").arg(maxMinus[4]).arg(measureObject->nrELenghtDowelMinus));

	int count = 0;
	for (int i = 11; i < 16; i++)
	{
		if (measureObject->isGoodArray[currPiece][i] == 0)
		{
			maxErrorText[count]->setDefaultTextColor(badColor);
			negativeErrorText[count]->setDefaultTextColor(badColor);
		}
		else
		{
			maxErrorText[count]->setDefaultTextColor(Qt::black);
			negativeErrorText[count]->setDefaultTextColor(Qt::black);
		}
		count++;
	}

	for (int i = 0; i < 10; i++)
	{
		if (measureObject->isGoodArray[currPiece][i] == 1)
			area[i]->setBrush(goodColor);
		else
			area[i]->setBrush(badColor);
	}
	if (measureObject->isGoodArray[currPiece][16] == 1)//okno za dolzino
		area[10]->setBrush(goodColor);
	else
		area[10]->setBrush(badColor);

	tmp = QString("%1").number(measureObject->mesurementElipse[currPiece], 'f', 2);
	elipseText->setPlainText(QString("Elipse: %1 , MAX: %2 [mm]").arg(tmp).arg(measureObject->setElipsse));

	if (measureObject->isGoodArray[currPiece][17] == 1)//elipse
	{
		elipseText->setDefaultTextColor(Qt::black);
	}
	else
	{
		elipseText->setDefaultTextColor(badColor);
	}




	ui2.graphicsView->fitInView(QRectF(0, 0, 1280, 720), Qt::IgnoreAspectRatio);


	//QtConcurrent::run(this, &MBsoftware::CopyThread, currPiece);


	/*QPixmap pixmap(parametersTable->size());
	pixmap = parametersTable->grab(QRect(QPoint(0,0),parametersTable->size()));

	measureObject->measurementTableImage[currPiece] = QImage(pixmap.toImage().convertToFormat(QImage::Format_ARGB32));
	QPixmap pixMap = ui.imageViewMainWindow_0->grab();
	measureObject->dowelImage[currPiece] = QImage(pixMap.toImage().convertToFormat(QImage::Format_ARGB32));
	*/


}
void DMSudp::SetDisplay()
{
	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		spinGain[i]->blockSignals(true);
		spinGain[i]->setValue(gain[i]);
		spinGain[i]->blockSignals(false);

		spinOffset[i]->blockSignals(true);
		spinOffset[i]->setValue(offset[i]);
		spinOffset[i]->blockSignals(false);

		spinWindwSTART[i]->blockSignals(true);
		spinWindwSTART[i]->setValue(detectWindow[i][0]);
		spinWindwSTART[i]->blockSignals(false);

		spinWindwSTOP[i]->blockSignals(true);
		spinWindwSTOP[i]->setValue(detectWindow[i][1]);
		spinWindwSTOP[i]->blockSignals(false);
	}
}
void DMSudp::closeEvent(QCloseEvent *event)
{
	WriteDatagram("L0;");//poslje disable live, da kdo ne pozabi
	viewTimer->stop();
}

void DMSudp::OnClickedLive()
{
	if (isLive == 0)
	{
		isLive = 1;
		WriteDatagram("L1;");
	}
	else
	{
		isLive = 0;
		WriteDatagram("L0;");
	}

	QByteArray test;
	int tmp = 0;
	for (int i = 0; i < 2200; i++)
	{
		test[i] = tmp;

		tmp++;
		tmp = tmp & 0xff;
	}

	for(int i = 0; i < NR_DMS_CAM;i++)
	emit frameReadySignal(i, test);
	
}

void DMSudp::OnClickedLiveTranstions()
{
	nrRecievedDowels = 0;
	if (isLiveTransitions == 0)
	{
		isLiveTransitions = 1;
		WriteDatagram("T1;");
	}
	else
	{
		isLiveTransitions = 0;
		WriteDatagram("T0;");
	}
}

void DMSudp::OnClickedGetTimeBase()
{
	WriteDatagram("V1;");
}

void DMSudp::WriteCamSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = referencePath + QString("/%1/DMS/cam_settings.ini").arg(REF_FOLDER_NAME);
	QSettings settings(filePath, QSettings::IniFormat);

	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		values = settings.value(QString("OFFSET%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			settings.setValue(QString("OFFSET%1").arg(i), QString("%1").arg(offset[i]));
			settings.sync();
			values.clear();
		}

		values = settings.value(QString("GAIN%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("GAIN%1").arg(i), QString("%1").arg(gain[i]));
			settings.sync();
			values.clear();
		}
		values = settings.value(QString("STARTDETECT%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("STARTDETECT%1").arg(i), QString("%1").arg(detectWindow[i][0]));
			settings.sync();
			values.clear();
		}
		values = settings.value(QString("STOPDETECT%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("STOPDETECT%1").arg(i), QString("%1").arg(detectWindow[i][1]));
			settings.sync();
			values.clear();
		}
	}

}

void DMSudp::WriteOneCamSetting(int index, QString setting)
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = referencePath + QString("/%1/DMS/cam_settings.ini").arg(REF_FOLDER_NAME);
	QSettings settings(filePath, QSettings::IniFormat);


	if (setting == "OFFSET")
	{
			settings.setValue(QString("OFFSET%1").arg(index), QString("%1").arg(offset[index]));
			settings.sync();
			values.clear();

	}
	else if (setting == "GAIN")
	{
		settings.setValue(QString("GAIN%1").arg(index), QString("%1").arg(gain[index]));
		settings.sync();
		values.clear();
	}
	else if (setting == "STARTDETECT")
	{
		settings.setValue(QString("STARTDETECT%1").arg(index), QString("%1").arg(detectWindow[index][0]));
		settings.sync();
		values.clear();
	}
	else if (setting == "STOPDETECT")
	{
		settings.setValue(QString("STOPDETECT%1").arg(index), QString("%1").arg(detectWindow[index][1]));
		settings.sync();
		values.clear();
	}

}
void DMSudp::ReadCamSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;


	filePath = referencePath + QString("/%1/DMS/cam_settings.ini").arg(REF_FOLDER_NAME);
	//filePath = QDir::currentPath() + QString("/%1/DMS/cam_settings.ini").arg(REF_FOLDER_NAME);
	QSettings settings(filePath, QSettings::IniFormat);


	//inputs
	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		values = settings.value(QString("OFFSET%1").arg(i)).toStringList();

		if (values.size() > 0)
			offset[i] = (values[0].toInt());
		values.clear();


		values = settings.value(QString("GAIN%1").arg(i)).toStringList();

		if (values.size() > 0)
			gain[i] = (values[0].toInt());
		values.clear();



		values = settings.value(QString("STARTDETECT%1").arg(i)).toStringList();
		if (values.size() > 0)
			detectWindow[i][0] = (values[0].toInt());

		values.clear();

		values = settings.value(QString("STOPDETECT%1").arg(i)).toStringList();
		if (values.size() > 0)
			detectWindow[i][1] = (values[0].toInt());


		values.clear();



	}
}


void DMSudp::ShowImage(int nrCam, QByteArray image)
{

	QByteArray newImageData = image;
	QPainter paint;


	QImage newImage(2250, 256, QImage::Format_RGB32);
	newImage.fill(Qt::white);
	QPainter painter(&newImage);

	QPen linePen;
	QPen dataPen;
	unsigned int pixValue = 0;
	QPainterPath path;
	char tmp = 0;
	unsigned int *dataArray;
	

	QByteArray hex_string = newImageData.toHex();
	path.moveTo(0, 125);
	for (int i = 0; i < newImageData.size(); i++)
	{
		pixValue =  static_cast<uint8_t>(newImageData[i]);
		path.lineTo(i, pixValue);
	}

	dataPen.setWidth(3);
	dataPen.setColor(Qt::blue);
	painter.setPen(dataPen);
	painter.drawPath(path);


	linePen.setWidth(7);
	linePen.setColor(Qt::red);
	
	painter.setPen(linePen);
	int threshold = 128;
	int startY = threshold - 28;
	int stopY = threshold + 28;

	painter.drawLine(QPoint(detectWindow[nrCam][0], threshold), QPoint(detectWindow[nrCam][1], threshold));
	painter.drawLine(QPoint(detectWindow[nrCam][0], startY), QPoint(detectWindow[nrCam][0], stopY));
	painter.drawLine(QPoint(detectWindow[nrCam][1], startY), QPoint(detectWindow[nrCam][1], stopY));
	//painter.drawLine(QPoint(50, 128), QPoint(2200, 128));


	painter.end();
	pixmapVideo[nrCam]->setPixmap(QPixmap::fromImage(newImage));
	if(nrCam == 0)
	ui.imageView0->fitInView(sceneVideo[0]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 1)
	ui.imageView1->fitInView(sceneVideo[1]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 2)
	ui.imageView2->fitInView(sceneVideo[2]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 3)
	ui.imageView3->fitInView(sceneVideo[3]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 4)
	ui.imageView4->fitInView(sceneVideo[4]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 5)
	ui.imageView5->fitInView(sceneVideo[5]->sceneRect(), Qt::IgnoreAspectRatio);

	///pixmapVideo->setPixmap(p);

}

void DMSudp::ShowTransitions(int nrCam, QByteArray image)
{
	QByteArray tmpArray = image;
	unsigned int data;
	unsigned int transitionType;
	unsigned int transtiton;

	int nrTransPos = 0, nrTransNeg = 0;
	for (int i = 0; i < 5; i++)
	{
		transitionDataTest[nrCam][i][0] = -1;
		transitionDataTest[nrCam][i][1] = -1;
	}
	//parsamo podatke 

	for (int i = 2; i < tmpArray.size(); i+=2)
	{
		data = static_cast<uint8_t>(tmpArray[i])<<8;
		data = data + static_cast<uint8_t>(tmpArray[i + 1]);//zdruzim v 16 bitno stevilko
		if (i > 11)
		{
			int bla = 00;
		}
	//sedaj moram razcleniti kateri prehod in vrednost prehoda 
		transitionType = data >> 14 & 0x3;
		transtiton = data & 0x3FFF;
		if (transtiton == 5168)
		{
			int bla = 100;
		}
		if (transitionType == 2)
		{
			transitionDataTest[nrCam][nrTransPos][0] = transtiton;//tabela za zgornje in spodnje prehode pri testnem oknu za prehode
			nrTransPos++;
		}
		else if (transitionType == 1)
		{
			transitionDataTest[nrCam][nrTransNeg][1] = transtiton;
			nrTransNeg++;
		}
		else
		{
	
		}

	}

	for (int i = 0; i < 2; i++)
	{
		if (transitionDataTest[nrCam][0][i] > -1)
			measureObject->calibrationTransitions[nrCam][i] = transitionDataTest[nrCam][0][i];
		else
			measureObject->calibrationTransitions[nrCam][i] = 0;
	}


	
	
	//	if (transitionDataTest[nrCam][i][0] > -1)
		//	transitionDataTestLabel[nrCam][i][0]->setText(QString("%1").arg(transitionDataTest[nrCam][i][0]));
	

	/*for (int i = 0; i < 5; i++)
	{

		if (transitionDataTest[nrCam][i][0] > -1)
			transitionDataTestLabel[nrCam][i][0]->setText(QString("%1").arg(transitionDataTest[nrCam][i][0]));
		else
			transitionDataTestLabel[nrCam][i][0]->setText(QString(""));

		if (transitionDataTest[nrCam][i][1] > -1)
			transitionDataTestLabel[nrCam][i][1]->setText(QString("%1").arg(transitionDataTest[nrCam][i][1]));
		else
			transitionDataTestLabel[nrCam][i][1]->setText(QString(""));
	}*/


}

void DMSudp::ShowBlowImage(int currPiece, int frame)
{
	Mat tmp;
	//cvtColor(measureObject->blowImage[currPiece][frame], tmp, COLOR_BGR2RGB);
	QImage qimgOriginal((uchar*)measureObject->blowImage[currPiece][frame].data, measureObject->blowImage[currPiece][frame].cols, measureObject->blowImage[currPiece][frame].rows, measureObject->blowImage[currPiece][frame].step, QImage::Format_RGB888);

	pixmapBlowView->setPixmap(QPixmap::fromImage(qimgOriginal));

	ui2.viewDowelBlow->resetTransform();
	//ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
	ui2.viewDowelBlow->fitInView(QRectF(0, 0, measureObject->blowImage[currPiece][frame].cols, measureObject->blowImage[currPiece][frame].rows), Qt::KeepAspectRatio);
	//measureObject.blowImage[showPiece][i].copyTo(images[i]->buffer[0]);
}

void DMSudp::ShowTimeLine(QByteArray data)
{
	//parsam podatke za prikaz casovne baze

	QByteArray parsData = data;
	int nrData;
	
	nrData = static_cast<uint8_t>(parsData[2]) << 8;


	nrData = nrData + static_cast<uint8_t>(parsData[1]);//zdruzim v 16 bitno stevilko

	int parseCount = 3;
	int count = 0;
	vector <bool> valveOn;
	vector <bool> vertCam;
	for (int i = 0; i < nrData / 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			
			valveOn.push_back( (parsData[parseCount] >> j) & 0x1);
		}
		parseCount++;
	}
	for (int i = 0; i < nrData / 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{

			vertCam.push_back((parsData[parseCount] >> j) & 0x1);

		}
		parseCount++;
	}

	int bla = 100;

	QImage newImage(nrData, 400, QImage::Format_RGB32);
	newImage.fill(Qt::white);
	QPainter painter(&newImage);
	
	QPen pen(Qt::red, 2);
	QLine line;
	painter.begin(&newImage);
	//painter.drawLine(line1000);
	painter.setPen(pen);
	QPainterPath path;
	int trans = 0;
	int transPrev = 0;
	int lastTrans = 0;
	for (int i = 0; i < nrData; i++)
	{
		if (i == 0)
		{
			if (valveOn[0] == true)
			{
				path.moveTo(0, 20);
				trans = 1;
			}
			else
			{
				path.moveTo(0, 50);
				trans = 0;
			}
		}
		else
		{
			trans = valveOn[i];
		}
		if (trans != transPrev)
		{
			if (trans == 1)
			{
				path.lineTo(i, 50);
				path.lineTo(i, 20);
			}
			else
			{
				path.lineTo(i, 20);
				path.lineTo(i, 50);
			}
			lastTrans = i;
		}
		transPrev = trans;
	}

	path.lineTo(nrData, 50);
	painter.drawPath(path);


	 trans = 0;
	 transPrev = 0;
	 lastTrans = 0;
	//linija 2 verticalne kamere
	QPainterPath vertPath;
	for (int i = 0; i < nrData; i++)
	{
		if (i == 0)
		{
			if (vertCam[0] == true)
			{
				vertPath.moveTo(0, 70);
				trans = 1;
			}
			else
			{
				vertPath.moveTo(0, 100);
				trans = 0;
			}
		}
		else
		{
			trans = vertCam[i];
		}
		if (trans != transPrev)
		{
			if (trans == 1)
			{
				vertPath.lineTo(i, 100);
				vertPath.lineTo(i, 70);
			}
			else
			{
				vertPath.lineTo(i, 70);
				vertPath.lineTo(i, 100);
			}
			lastTrans = i;
		}
		transPrev = trans;
	}

	vertPath.lineTo(nrData, 100);
	painter.drawPath(vertPath);



	pen.setColor(Qt::blue);
	painter.setPen(pen);
	line.setLine(0, 350, nrData, 350);
	painter.drawLine(line);

	line.setLine(nrData, 0, nrData, 350);
	painter.drawLine(line);

	int synhFreq = 5600;
	
	
	for (int i = 1; i < 7; i++)
	{
		line.setLine(synhFreq, 350, synhFreq, 320);
		painter.drawLine(line);
		painter.drawText(synhFreq, 370, QString("-%1 s").arg(i));
		synhFreq += 5600;
	}


	painter.end();
	pixmapTimeLine->setPixmap(QPixmap::fromImage(newImage));
	ui2.timeLineView->scale(1.0,1.0);
		//ui2.timeLineView->fitInView(sceneTimeLine->sceneRect(), Qt::KeepAspectRatio);
}

void DMSudp::OnToggledCheckBox1(bool isActive)
{

	isCheckedShowTolerance = isActive;

	ShowDowel(this->currentDowelDraw);
}

void DMSudp::OnToggledCheckBox2(bool isActive)
{
	isCheckedShowArea = isActive;

	ShowDowel(this->currentDowelDraw);
}

void DMSudp::OnToggledCheckBox3(bool isActive)
{
	isCheckedShowDowel = isActive;

	ShowDowel(this->currentDowelDraw);
}

void DMSudp::OnToggledCheckBox4(bool isActive)
{
	isCheckedShowConusLines = isActive;

	ShowDowel(this->currentDowelDraw);
}

void DMSudp::OnToggledCheckBox5(bool isActive)
{
	isCheckedShowMeasurements = isActive;

	ShowMeasurements(this->currentDowelDraw);
}

void DMSudp::OnClickedNextHorMeasurement()
{
	horLineCurrMeasurement++;
	if (horLineCurrMeasurement > measureObject->nrDowelMeasurements[currentDowelDraw])
		horLineCurrMeasurement = measureObject->nrDowelMeasurements[currentDowelDraw];


	ShowHorizontalCamera(currentDowelDraw, horLineCurrMeasurement);
}

void DMSudp::OnClickedPrevHorMeasurement()
{
	horLineCurrMeasurement--;
	if (horLineCurrMeasurement < 0)
		horLineCurrMeasurement = 0;


	ShowHorizontalCamera(currentDowelDraw, horLineCurrMeasurement);
}

void DMSudp::OnClickedHistoryUp()
{
	historyCounter++;

	if (historyCounter >= DMSHISTORYSIZE)
		historyCounter = 0;
	shownDowel = ((lastPieceScanned) - historyCounter + DMSHISTORYSIZE) % DMSHISTORYSIZE;
	ui2.checkShowMeasurements->setChecked(false);
	ShowDowel(shownDowel);
	DrawMeasurementTable(shownDowel);
	ShowMeasurements(shownDowel);
	UpdateDowelImage(shownDowel);
	UpdateList();
}

void DMSudp::OnClickedHistoryDown()
{

	historyCounter--;
	if (historyCounter < 0)
		historyCounter = DMSHISTORYSIZE - 1;


	shownDowel = ((lastPieceScanned ) - historyCounter + DMSHISTORYSIZE) % DMSHISTORYSIZE;
	//ui2.labelHistoryPieceInHistory->setText(QString("%1").arg(historyCounter+1));
	ui2.checkShowMeasurements->setChecked(false);
	ShowDowel(shownDowel);
	DrawMeasurementTable(shownDowel);
	ShowMeasurements(shownDowel);
	UpdateDowelImage(shownDowel);
	UpdateList();
}

void DMSudp::OnClickedLastScanned()
{
	historyCounter = 0;
	//ui2.labelHistoryPieceInHistory->setText(QString("%1").arg(historyCounter + 1));

	shownDowel = lastPieceScanned;
	ShowDowel(lastPieceScanned);

	UpdateList();

}

void DMSudp::OnClickedFrameUp()
{
	frameUpCounter++;
	if (frameUpCounter > 14)
		frameUpCounter = 14;

	ShowBlowImage( shownDowel,  frameUpCounter);

}

void DMSudp::OnClickedFrameDown()
{
	frameUpCounter--;
	if (frameUpCounter < 0)
		frameUpCounter = 0;
	ShowBlowImage( shownDowel,  frameUpCounter);
}

void DMSudp::UpdateList()
{
	QColor goodColor = QColor(150, 255, 150);
	QColor badColor = QColor(255, 150, 150);
	frameUpCounter = 0;
	if (measureObject->dowelGood[shownDowel] == 1)
	{
		ui2.labelHistoryIsGood->setText(QString("%1").arg("GOOD"));
		ui2.labelHistoryIsGood->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}
	else
	{
		ui2.labelHistoryIsGood->setText(QString("%1").arg("NOT GOOD"));
		ui2.labelHistoryIsGood->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));
	}
	ui2.labelHistoryPieceInHistory->setText(QString("%1").arg(historyCounter + 1));


int index = 0;

ui2.list->item(index)->setText(QString("last scannedDowel:%1").arg(lastPieceScanned));
index++;
ui2.list->item(index)->setText(QString("show history:%1").arg(historyCounter));
index++;
ui2.list->item(index)->setText(QString("realNumDowel:%1").arg(shownDowel));
index++;
ui2.list->item(index)->setText(QString("Alarm Num:%1").arg(measureObject->measureAlarm[shownDowel]));


ShowBlowImage(shownDowel, frameUpCounter);
}

void DMSudp::ShowDowel(int nrDowel)
{
	this->currentDowelDraw = nrDowel;
	int height = 600;
	int width = 1100;
	float ratio;
	CLine leftLine;
	CLine topLine;
	CLine bottomLine;
	CLine rightLine;
	ratio = measureObject->setLenght / measureObject->setDiameter;
	QImage newImage(1100, height, QImage::Format_RGB32);
	newImage.fill(Qt::white);
	QPainter painter(&newImage);
	painter.begin(&newImage);
	painter.setPen(QPen(Qt::green, 6, Qt::SolidLine, Qt::SquareCap));

	QPolygon poly;

	int selectedCamera = selectedCameraDraw;


	int currPiece = nrDowel;
	QPen pen(Qt::red, 2);

	topLine.SetLine(CPointFloat(0, 0), CPointFloat(width,0));
	bottomLine.SetLine(CPointFloat(0, height), CPointFloat(width,height));
	leftLine.SetLine(CPointFloat(0, 0), CPointFloat(0,height));
	rightLine.SetLine(CPointFloat(width, 0), CPointFloat(width,height));
	//painter.drawLine(line1000);
	int refPointStop = 1050;
	int refPointStart = 50;
	float korRef = 33.33;
	korRef = 1000/measureObject->setLenght;
	QPointF testPoint;
	int refPointMiddle = height / 3;
	float korFactorVert = 1000 / 3.75;
	korFactorVert = korFactorVert / measureObject->setDiameter;
	painter.setPen(pen);
	QLineF tmpLine;
	pen.setColor(Qt::blue);
	painter.setPen(pen);
	if (isCheckedShowDowel)
	{
		for (int i = 0; i < measureObject->nrDowelMeasurementsZAC[currPiece]; i++)
		{

			testPoint = QPointF((refPointStart + measureObject->horizontalDataZACreal[currPiece][i] * korRef), 500.0);
			tmpLine.setP1(QPoint(testPoint.x(), refPointMiddle - measureObject->verticalDataZACDraw[currPiece][selectedCamera][i][0] * korFactorVert));
			tmpLine.setP2(QPoint(testPoint.x(), refPointMiddle + measureObject->verticalDataZACDraw[currPiece][selectedCamera][i][1] * korFactorVert));
			painter.drawLine(tmpLine);

		}
		pen.setColor(Qt::red);
		painter.setPen(pen);
		for (int i = 0; i < measureObject->nrDowelMeasurementsKON[currPiece]; i++)
		{
			testPoint = QPointF((refPointStop - measureObject->horizontalDataKONreal[currPiece][i] * korRef), 500.0);
			tmpLine.setP1(QPoint(testPoint.x(), refPointMiddle - measureObject->verticalDataKONDraw[currPiece][selectedCamera][i][0] * korFactorVert));
			tmpLine.setP2(QPoint(testPoint.x(), refPointMiddle + measureObject->verticalDataKONDraw[currPiece][selectedCamera][i][1] * korFactorVert));
			painter.drawLine(tmpLine);
		}
	}
	//tolerancne meje obmocja meritev  
	pen.setColor((Qt::magenta));
	pen.setWidth(2);
	painter.setPen(pen);
	if (isCheckedShowArea)
	{
		tmpLine.setLine(refPointStart + measureObject->measureArea0[0] * korRef, 0, refPointStart + measureObject->measureArea0[0] * korRef, height);
		painter.drawLine(tmpLine);
		tmpLine.setLine(refPointStart + measureObject->measureArea0[1] * korRef, 0, refPointStart + measureObject->measureArea0[1] * korRef, height);
		painter.drawLine(tmpLine);
		tmpLine.setLine(refPointStop - measureObject->measureArea0[0] * korRef, 0, refPointStop - measureObject->measureArea0[0] * korRef, height);
		painter.drawLine(tmpLine);
		tmpLine.setLine(refPointStop - measureObject->measureArea0[1] * korRef, 0, refPointStop - measureObject->measureArea0[1] * korRef, height);
		painter.drawLine(tmpLine);

		pen.setColor((Qt::cyan));
		pen.setWidth(2);
		painter.setPen(pen);

		tmpLine.setLine(refPointStart + measureObject->measureArea1[0] * korRef, 0, refPointStart + measureObject->measureArea1[0] * korRef, height);
		painter.drawLine(tmpLine);
		tmpLine.setLine(refPointStart + measureObject->measureArea1[1] * korRef, 0, refPointStart + measureObject->measureArea1[1] * korRef, height);
		painter.drawLine(tmpLine);
		tmpLine.setLine(refPointStop - measureObject->measureArea1[0] * korRef, 0, refPointStop - measureObject->measureArea1[0] * korRef, height);
		painter.drawLine(tmpLine);
		tmpLine.setLine(refPointStop - measureObject->measureArea1[1] * korRef, 0, refPointStop - measureObject->measureArea1[1] * korRef, height);
		painter.drawLine(tmpLine);
	}

	//linije konusa 
	QPointF p1, p2;
	CLine drawLine,drawLine2;
	CPointFloat tmpP1, tmpP2;
	CPointFloat tmpMiddleP1, tmpMiddleP2;
	CPointFloat intersectP1, intersectP2;
	CPointFloat intersectMP1, intersectMP2;

	if (isCheckedShowConusLines)
	{
		for (int i = 0; i < 4; i++)
		{
			switch (i)
			{
			case 0://US
			{
				tmpP1 = CPointFloat(refPointStart + measureObject->lineKonus[currPiece][selectedCamera][i].p1.x*korRef, refPointMiddle - measureObject->lineKonus[currPiece][selectedCamera][i].p1.y*korFactorVert);
				tmpP2 = CPointFloat(refPointStart + measureObject->lineKonus[currPiece][selectedCamera][i].p2.x*korRef, refPointMiddle - measureObject->lineKonus[currPiece][selectedCamera][i].p2.y*korFactorVert);
				tmpMiddleP1 = CPointFloat(refPointStart + measureObject->lineMiddle[currPiece][selectedCamera][i].p1.x*korRef, refPointMiddle - measureObject->lineMiddle[currPiece][selectedCamera][i].p1.y*korFactorVert);
				tmpMiddleP2 = CPointFloat(refPointStart + measureObject->lineMiddle[currPiece][selectedCamera][i].p2.x*korRef, refPointMiddle - measureObject->lineMiddle[currPiece][selectedCamera][i].p2.y*korFactorVert);
				drawLine.SetLine(tmpP1, tmpP2);
				drawLine2.SetLine(tmpMiddleP1, tmpMiddleP2);
				intersectP1 = drawLine.GetIntersectionPoint(leftLine);
				intersectP2 = drawLine.GetIntersectionPoint(topLine);
				intersectMP1 = drawLine2.GetIntersectionPoint(leftLine);
				intersectMP2 = tmpMiddleP2;
				pen.setColor((Qt::darkYellow));
				break;
			}
			case 1://DS
			{
				tmpP1 = CPointFloat(refPointStart + measureObject->lineKonus[currPiece][selectedCamera][i].p1.x*korRef, refPointMiddle + measureObject->lineKonus[currPiece][selectedCamera][i].p1.y*korFactorVert);
				tmpP2 = CPointFloat(refPointStart + measureObject->lineKonus[currPiece][selectedCamera][i].p2.x*korRef, refPointMiddle + measureObject->lineKonus[currPiece][selectedCamera][i].p2.y*korFactorVert);
				tmpMiddleP1 = CPointFloat(refPointStart + measureObject->lineMiddle[currPiece][selectedCamera][i].p1.x*korRef, refPointMiddle + measureObject->lineMiddle[currPiece][selectedCamera][i].p1.y*korFactorVert);
				tmpMiddleP2 = CPointFloat(refPointStart + measureObject->lineMiddle[currPiece][selectedCamera][i].p2.x*korRef, refPointMiddle + measureObject->lineMiddle[currPiece][selectedCamera][i].p2.y*korFactorVert);
				drawLine.SetLine(tmpP1, tmpP2);
				drawLine2.SetLine(tmpMiddleP1, tmpMiddleP2);
				intersectP1 = drawLine.GetIntersectionPoint(leftLine);
				intersectP2 = drawLine.GetIntersectionPoint(bottomLine);
				intersectMP1 = drawLine2.GetIntersectionPoint(leftLine);
				intersectMP2 = tmpMiddleP2;
				pen.setColor((Qt::magenta));
				break;
			}
			case 2://UE
			{
				tmpP1 = CPointFloat(refPointStop - measureObject->lineKonus[currPiece][selectedCamera][i].p1.x*korRef, refPointMiddle - measureObject->lineKonus[currPiece][selectedCamera][i].p1.y*korFactorVert);
				tmpP2 = CPointFloat(refPointStop - measureObject->lineKonus[currPiece][selectedCamera][i].p2.x*korRef, refPointMiddle - measureObject->lineKonus[currPiece][selectedCamera][i].p2.y*korFactorVert);
				tmpMiddleP1 = CPointFloat(refPointStop - measureObject->lineMiddle[currPiece][selectedCamera][i].p1.x*korRef, refPointMiddle - measureObject->lineMiddle[currPiece][selectedCamera][i].p1.y*korFactorVert);
				tmpMiddleP2 = CPointFloat(refPointStop - measureObject->lineMiddle[currPiece][selectedCamera][i].p2.x*korRef, refPointMiddle - measureObject->lineMiddle[currPiece][selectedCamera][i].p2.y*korFactorVert);
				drawLine.SetLine(tmpP1, tmpP2);
				drawLine2.SetLine(tmpMiddleP1, tmpMiddleP2);
				intersectP1 = drawLine.GetIntersectionPoint(topLine);
				intersectP2 = drawLine.GetIntersectionPoint(rightLine);
				intersectMP1 = tmpMiddleP1;
				intersectMP2 = drawLine2.GetIntersectionPoint(rightLine);

				pen.setColor((Qt::green));
				break;
			}
			case 3://DE
			{
				tmpP1 = CPointFloat(refPointStop - measureObject->lineKonus[currPiece][selectedCamera][i].p1.x*korRef, refPointMiddle + measureObject->lineKonus[currPiece][selectedCamera][i].p1.y*korFactorVert);
				tmpP2 = CPointFloat(refPointStop - measureObject->lineKonus[currPiece][selectedCamera][i].p2.x*korRef, refPointMiddle + measureObject->lineKonus[currPiece][selectedCamera][i].p2.y*korFactorVert);
				tmpMiddleP1 = CPointFloat(refPointStop - measureObject->lineMiddle[currPiece][selectedCamera][i].p1.x*korRef, refPointMiddle + measureObject->lineMiddle[currPiece][selectedCamera][i].p1.y*korFactorVert);
				tmpMiddleP2 = CPointFloat(refPointStop - measureObject->lineMiddle[currPiece][selectedCamera][i].p2.x*korRef, refPointMiddle + measureObject->lineMiddle[currPiece][selectedCamera][i].p2.y*korFactorVert);
				drawLine.SetLine(tmpP1, tmpP2);
				drawLine2.SetLine(tmpMiddleP1, tmpMiddleP2);
				intersectP1 = drawLine.GetIntersectionPoint(bottomLine);
				intersectP2 = drawLine.GetIntersectionPoint(rightLine);
				intersectMP1 = drawLine2.GetIntersectionPoint(rightLine);
				intersectMP2 = tmpMiddleP2;
				pen.setColor((Qt::blue));
				break;
			}
			default:
				break;
			}

			painter.setPen(pen);
			tmpLine.setLine(intersectP1.x, intersectP1.y, intersectP2.x, intersectP2.y);
			painter.drawLine(tmpLine);
			tmpLine.setLine(intersectMP1.x, intersectMP1.y, intersectMP2.x, intersectMP2.y);
			painter.drawLine(tmpLine);
		}

	}

	//tukaj risemo toleracnce MEJE
	//dodati moramo zaradi prikaza na sliko se dodatne faktorje
	if (isCheckedShowTolerance == true)
	{
		float startDistance = 0 + measureObject->konusToleranceMinus;
		float stopDistance = 0 + measureObject->konusTolerancePlus - measureObject->konusToleranceMinus;
		float topDistance = 0 + measureObject->diameterTolerancePlus - measureObject->diameterToleranceMinus;
		float toleranceLineHigh =  measureObject->diameterTolerancePlus;
		float toleranceLineLow =  measureObject->diameterToleranceMinus;
		//spodaj 
		pen.setColor((Qt::red));
		painter.setPen(pen);
		tmpLine.setLine(refPointStart + startDistance * korRef, refPointMiddle + toleranceLineHigh *korFactorVert , refPointStop - startDistance * korRef, refPointMiddle + toleranceLineHigh * korFactorVert);
		painter.drawLine(tmpLine);

		CPointFloat inter;
		drawLine.SetLine(CPointFloat(refPointStart + startDistance * korRef, refPointMiddle + toleranceLineHigh * korFactorVert),( measureObject->setAngleMin +180) * M_PI/180);
		 inter = drawLine.GetIntersectionPoint(leftLine);
		tmpLine.setLine(refPointStart + startDistance * korRef, refPointMiddle + toleranceLineHigh * korFactorVert, inter.x, inter.y);
		painter.drawLine(tmpLine);

		drawLine.SetLine(CPointFloat(refPointStop - startDistance * korRef, refPointMiddle + toleranceLineHigh * korFactorVert),(360- measureObject->setAngleMin) * M_PI/180);
		inter = drawLine.GetIntersectionPoint(rightLine);
		tmpLine.setLine(refPointStop - startDistance * korRef, refPointMiddle + toleranceLineHigh * korFactorVert, inter.x, inter.y);
		painter.drawLine(tmpLine);
		
		pen.setColor((Qt::darkRed));
		painter.setPen(pen);
		tmpLine.setLine(refPointStart + stopDistance * korRef, refPointMiddle + toleranceLineLow * korFactorVert, refPointStop - stopDistance * korRef, refPointMiddle + toleranceLineLow * korFactorVert);
		painter.drawLine(tmpLine);

		drawLine.SetLine(CPointFloat(refPointStart + stopDistance * korRef, refPointMiddle + toleranceLineLow * korFactorVert), (measureObject->setAngleMax + 180) * M_PI / 180);
		 inter = drawLine.GetIntersectionPoint(leftLine);
		tmpLine.setLine(refPointStart + stopDistance * korRef, refPointMiddle + toleranceLineLow * korFactorVert, inter.x, inter.y);
		painter.drawLine(tmpLine);

		drawLine.SetLine(CPointFloat(refPointStop - stopDistance * korRef, refPointMiddle + toleranceLineLow * korFactorVert), (360 - measureObject->setAngleMax) * M_PI / 180);
		inter = drawLine.GetIntersectionPoint(rightLine);
		tmpLine.setLine(refPointStop - stopDistance * korRef, refPointMiddle + toleranceLineLow * korFactorVert, inter.x, inter.y);
		painter.drawLine(tmpLine);

		QPointF drawPoint;
		int result = 0;
		int sizePoint = 2;
		for (int i = 0; i < measureObject->nrMeasurementsArea0[currPiece][selectedCamera]; i++)//DSK
		{
			result = measureObject->diameterDSKGood[currPiece][selectedCamera][i];
			if (result == 1)//meritev GOOD
			{
				pen.setColor((Qt::green));
				pen.setBrush(Qt::green);
				painter.setPen(pen);
			}
			else if (result == 2)//meritev SLABA
			{
				pen.setColor((Qt::red));
				pen.setBrush(Qt::red);
				painter.setPen(pen);
			}
			else if (result == 0)//meritev SLABA
			{
				pen.setColor((Qt::darkRed));
				pen.setBrush(Qt::darkRed);
				painter.setPen(pen);
			}
			painter.drawEllipse(QPointF(refPointStart + measureObject->diametersDSK[currPiece][selectedCamera][i].x()*korRef, (refPointMiddle + measureObject->diametersDSK[currPiece][selectedCamera][i].y()*korFactorVert)), sizePoint, sizePoint);
		}
		for (int i = 0; i < measureObject->nrMeasurementsArea1[currPiece][selectedCamera]; i++)//DIS
		{
			result = measureObject->diameterDISGood[currPiece][selectedCamera][i];
			if (result == 1)//meritev GOOD
			{
				pen.setColor((Qt::green));
				pen.setBrush(Qt::green);
				painter.setPen(pen);
			}
			else if (result == 2)//meritev SLABA
			{
				pen.setColor((Qt::red));
				pen.setBrush(Qt::red);
				painter.setPen(pen);
			}
			else if (result == 0)//meritev SLABA
			{
				pen.setColor((Qt::darkRed));
				pen.setBrush(Qt::darkRed);
				painter.setPen(pen);
			}
			painter.drawEllipse(QPointF(refPointStart + measureObject->diametersDIS[currPiece][selectedCamera][i].x()*korRef,( refPointMiddle +  measureObject->diametersDIS[currPiece][selectedCamera][i].y()*korFactorVert)), sizePoint, sizePoint);
		}
		for (int i = 0; i < measureObject->nrMeasurementsArea2[currPiece][selectedCamera]/2; i++) //DIC
		{
			result = measureObject->diameterDICGood[currPiece][selectedCamera][i];
			if (result == 1)//meritev GOOD
			{
				pen.setColor((Qt::green));
				pen.setBrush(Qt::green);
				painter.setPen(pen);
			}
			else if (result == 2)//meritev SLABA
			{
				pen.setColor((Qt::red));
				pen.setBrush(Qt::red);
				painter.setPen(pen);
			}
			else if (result == 0)//meritev SLABA
			{
				pen.setColor((Qt::darkRed));
				pen.setBrush(Qt::darkRed);
				painter.setPen(pen);
			}
			painter.drawEllipse(QPointF(refPointStart + measureObject->diametersDIC[currPiece][selectedCamera][i].x()*korRef, (refPointMiddle + measureObject->diametersDIC[currPiece][selectedCamera][i].y()*korFactorVert)), sizePoint, sizePoint);
		}
		for (int i = measureObject->nrMeasurementsArea2[currPiece][selectedCamera]; i > measureObject->nrMeasurementsArea2[currPiece][selectedCamera] / 2; i--) //DIC druga polovica
		{
			result = measureObject->diameterDICGood[currPiece][selectedCamera][i];
			if (result == 1)//meritev GOOD
			{
				pen.setColor((Qt::green));
				pen.setBrush(Qt::green);
				painter.setPen(pen);
			}
			else if (result == 2)//meritev SLABA
			{
				pen.setColor((Qt::red));
				pen.setBrush(Qt::red);
				painter.setPen(pen);
			}
			else if (result == 0)//meritev SLABA
			{
				pen.setColor((Qt::darkRed));
				pen.setBrush(Qt::darkRed);
				painter.setPen(pen);
			}
			painter.drawEllipse(QPointF(refPointStop - measureObject->diametersDIC[currPiece][selectedCamera][i].x()*korRef, (refPointMiddle + measureObject->diametersDIC[currPiece][selectedCamera][i].y()*korFactorVert)), sizePoint, sizePoint);
		}
		for (int i = 0; i < measureObject->nrMeasurementsArea3[currPiece][selectedCamera]; i++)
		{
			result = measureObject->diameterDIEGood[currPiece][selectedCamera][i];
			if (result == 1)//meritev GOOD
			{
				pen.setColor((Qt::green));
				pen.setBrush(Qt::green);
				painter.setPen(pen);
			}
			else if (result == 2)//meritev SLABA
			{
				pen.setColor((Qt::red));
				pen.setBrush(Qt::red);
				painter.setPen(pen);
			}
			else if (result == 0)//meritev SLABA
			{
				pen.setColor((Qt::darkRed));
				pen.setBrush(Qt::darkRed);
				painter.setPen(pen);
			}
			painter.drawEllipse(QPointF(refPointStop - measureObject->diametersDIE[currPiece][selectedCamera][i].x()*korRef, (refPointMiddle + measureObject->diametersDIE[currPiece][selectedCamera][i].y()*korFactorVert)), sizePoint, sizePoint);
		}
		for (int i = 0; i < measureObject->nrMeasurementsArea4[currPiece][selectedCamera]; i++)
		{
			result = measureObject->diameterDKEGood[currPiece][selectedCamera][i];
			if (result == 1)//meritev GOOD
			{
				pen.setColor((Qt::green));
				pen.setBrush(Qt::green);
				painter.setPen(pen);
			}
			else if (result == 2)//meritev SLABA
			{
				pen.setColor((Qt::red));
				pen.setBrush(Qt::red);
				painter.setPen(pen);
			}
			else if (result == 0)//meritev SLABA
			{
				pen.setColor((Qt::darkRed));
				pen.setBrush(Qt::darkRed);
				painter.setPen(pen);
			}
			painter.drawEllipse(QPointF(refPointStop - measureObject->diametersDKE[currPiece][selectedCamera][i].x()*korRef, (refPointMiddle + measureObject->diametersDKE[currPiece][selectedCamera][i].y()*korFactorVert)), sizePoint, sizePoint);
		}
	}



	painter.end();
	pixmapDowel->setPixmap(QPixmap::fromImage(newImage));
	QRectF rect = sceneDowel->sceneRect();
	//ui2.timeLineView->scale(2, 2);
	ui2.dowelView->fitInView(sceneDowel->sceneRect(), Qt::KeepAspectRatio);

//ui.labelTest->setMask(pixmap.mask());
	

}

void DMSudp::ShowMeasurements(int nrDowel)
{
	int i = 0;
//izri�emo vse meritve trenutnega moznika za zgodovino
	int m = measureObject->nrDowelMeasurementsKON[nrDowel];
	int count = 0;
	if (isCheckedShowMeasurements == true)
	{
		for (i = 0; i < measureObject->nrDowelMeasurementsZAC[nrDowel]; i++)
		{
			//labelCameraMeaurements[i][0]->setHidden(false);
			//labelNrMeasurementPosition[i]->setHidden(false);
			//labelNrMeasurement[i]->setHidden(false);

			labelNrMeasurement[i]->setText(QString("M=%1").arg(i));


			labelCameraMeaurements[i][0]->setText(QString("S=%1, E=%2, L=%3").arg(measureObject->horizontalTransitionsReal[nrDowel][i][0]).arg(measureObject->horizontalTransitionsReal[nrDowel][i][1]).arg(measureObject->horizontalTransitionsReal[nrDowel][i][1] - measureObject->horizontalTransitionsReal[nrDowel][i][0]));
			if (measureObject->horizontalMeasurementsOn[nrDowel][i] == 1)
				labelCameraMeaurements[i][0]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));

			for (int k = 1; k < NR_DMS_CAM; k++)
			{

				measureObject->verticalDataZAC[nrDowel][k][i][0];
				labelCameraMeaurements[i][k]->setText(QString("C%1; T1=%2, T2=%3, D=%4 DR = %5").arg(k).arg(measureObject->verticalDataZAC[nrDowel][k][i][0]).arg(measureObject->verticalDataZAC[nrDowel][k][i][1]).arg(measureObject->verticalDataZAC[nrDowel][k][i][1] - measureObject->verticalDataZAC[nrDowel][k][i][0]).arg(measureObject->diameterDataZACReal[nrDowel][k][i]));
				//labelCameraMeaurements[i][k]->setHidden(false);
				if (measureObject->measurementsOnArea[nrDowel][i] == 1)
					labelCameraMeaurements[i][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 150, 20);"));
				else if (measureObject->measurementsOnArea[nrDowel][i] == 2)
					labelCameraMeaurements[i][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 150, 100);"));
				else if (measureObject->measurementsOnArea[nrDowel][i] == 3)
					labelCameraMeaurements[i][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 150, 150);"));
				else if (measureObject->measurementsOnArea[nrDowel][i] == 4)
					labelCameraMeaurements[i][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 200, 200);"));
				else if (measureObject->measurementsOnArea[nrDowel][i] == 5)
					labelCameraMeaurements[i][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 255);"));
				else
					labelCameraMeaurements[i][k]->setStyleSheet(QStringLiteral(""));



			}
			labelNrMeasurementPosition[i]->setText(QString("P=%1").arg(measureObject->horizontalDataZACreal[nrDowel][i]));
			count++;
		}
		for (i = measureObject->nrDowelMeasurementsKON[nrDowel]; i >= 0; i--)
		{
			//labelNrMeasurement[count]->setHidden(false);
			//labelNrMeasurementPosition[count]->setHidden(false);

			labelNrMeasurement[count]->setText(QString("M=%1").arg(count));
			labelNrMeasurementPosition[count]->setText(QString("P=%1").arg(measureObject->horizontalDataKONreal[nrDowel][i]));
			//labelCameraMeaurements[count][0]->setHidden(false);
			labelCameraMeaurements[count][0]->setText(QString("S=%1, E=%2, L=%3").arg(measureObject->horizontalTransitionsReal[nrDowel][count][0]).arg(measureObject->horizontalTransitionsReal[nrDowel][count][1]).arg(measureObject->horizontalTransitionsReal[nrDowel][count][1] - measureObject->horizontalTransitionsReal[nrDowel][count][0]));
			if (measureObject->horizontalMeasurementsOn[nrDowel][count] == 1)
				labelCameraMeaurements[count][0]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
			else
				labelCameraMeaurements[count][0]->setStyleSheet(QStringLiteral(""));

			for (int k = 1; k < NR_DMS_CAM; k++)
			{

				measureObject->verticalDataKON[nrDowel][k][i][0];
				labelCameraMeaurements[count][k]->setText(QString("C%1; T1=%2, T2=%3, D=%4, DR=%5").arg(k).arg(measureObject->verticalDataKON[nrDowel][k][i][0]).arg(measureObject->verticalDataKON[nrDowel][k][i][1]).arg(measureObject->verticalDataKON[nrDowel][k][i][1] - measureObject->verticalDataKON[nrDowel][k][i][0]).arg(measureObject->diameterDataKONReal[nrDowel][k][i]));
				//	labelCameraMeaurements[count][k]->setHidden(false);

				if (measureObject->measurementsOnArea[nrDowel][count] == 1)
					labelCameraMeaurements[count][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 150, 20);"));
				else if (measureObject->measurementsOnArea[nrDowel][count] == 2)
					labelCameraMeaurements[count][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 150, 100);"));
				else if (measureObject->measurementsOnArea[nrDowel][count] == 3)
					labelCameraMeaurements[count][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 150, 150);"));
				else if (measureObject->measurementsOnArea[nrDowel][count] == 4)
					labelCameraMeaurements[count][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 200, 200);"));
				else if (measureObject->measurementsOnArea[nrDowel][count] == 5)
					labelCameraMeaurements[count][k]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 255);"));
				else
					labelCameraMeaurements[count][k]->setStyleSheet(QStringLiteral(""));
			}


			count++;
		}
		ui2.scrollAreaMeasurements->setHidden(false);
		for (i = count; i < MAX_MEASUREMENT; i++)
		{
			labelNrMeasurementPosition[i]->setHidden(true);
			labelNrMeasurement[i]->setHidden(true);
			for (int k = 0; k < NR_DMS_CAM; k++)
			{
				labelCameraMeaurements[i][k]->setHidden(true);
			}
		}
	}
	else
	ui2.scrollAreaMeasurements->setHidden(true);
	
}

void DMSudp::ShowHorizontalCamera(int curr, int frame)
{
	int currPiece = curr;
	int selectedMeausurement = frame;
	int height = 200;
	int width = 2200;
	float ratio;
	CLine leftLine;
	CLine topLine;
	CLine bottomLine;
	CLine rightLine;
	
	QImage newImage(width, height, QImage::Format_RGB32);
	newImage.fill(Qt::white);
	QPainter painter(&newImage);
	painter.begin(&newImage);
	painter.setPen(QPen(Qt::green, 3, Qt::SolidLine, Qt::SquareCap));

	QPainterPath potekMoznikaPath;
	int topLevel = 50;;
	int bottomLevel = 70;
	potekMoznikaPath.moveTo(0, bottomLevel);
	potekMoznikaPath.lineTo(measureObject->horizontalTransitions[currPiece][selectedMeausurement][0], bottomLevel);
	potekMoznikaPath.lineTo(measureObject->horizontalTransitions[currPiece][selectedMeausurement][0], topLevel);
	potekMoznikaPath.lineTo(measureObject->horizontalTransitions[currPiece][selectedMeausurement][1], topLevel);
	potekMoznikaPath.lineTo(measureObject->horizontalTransitions[currPiece][selectedMeausurement][1], bottomLevel);
	potekMoznikaPath.lineTo(2200, bottomLevel);


	painter.drawPath(potekMoznikaPath);


	QPainterPath potekHorizontalPath;

	topLevel = 100;
	bottomLevel = 120;

	if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][0] == 1)//prehod iz 0 v 1 
		potekHorizontalPath.moveTo(0, topLevel);
	else
		potekHorizontalPath.moveTo(0, bottomLevel);

	if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][0] == 2)//prehod iz 0 v 1 
	{
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][0], bottomLevel);
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][0], topLevel);
	}
	else if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][0] == 1)
	{
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][0], topLevel);
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][0], bottomLevel);
	}


	if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][1] == 2)//prehod iz 0 v 1 
	{
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][1], bottomLevel);
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][1], topLevel);
	}
	else if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][1] == 1)
	{
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][1], topLevel);
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][1], bottomLevel);
	}

	if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][2] == 2)//prehod iz 0 v 1 
	{
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][2], bottomLevel);
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][2], topLevel);
	}
	else if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][2] == 1)
	{
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][2], topLevel);
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][2], bottomLevel);
	}

	if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][3] == 2)//prehod iz 0 v 1 
	{
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][3], bottomLevel);
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][3], topLevel);
	}
	else if (measureObject->horizontalDataTypes[currPiece][selectedMeausurement][3] == 1)
	{
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][3], topLevel);
		potekHorizontalPath.lineTo(measureObject->horizontalData[currPiece][selectedMeausurement][3], bottomLevel);
	}

	painter.drawPath(potekHorizontalPath);


	painter.end();
	pixmapHorizontalView->setPixmap(QPixmap::fromImage(newImage));
	ui2.horizontalView->fitInView(sceneHorizontalView->sceneRect(), Qt::IgnoreAspectRatio);
}





void DMSudp::ReadDowelString(QByteArray dataD)//preberemo string 
{

	int lowData, highData;
	int value = 0;
	QByteArray data;
	data = dataD;

	int currentDowel = -1;
	measureObject->currentPiece++;
	if (measureObject->currentPiece >= DMSHISTORYSIZE)
		measureObject->currentPiece = 0;
	

	measureObject->ResetMeasurements(measureObject->currentPiece);

	measureObject->dowelStringSize[measureObject->currentPiece] = data.size();
	measureObject->dowelReady[measureObject->currentPiece] = 1;
	measureObject->dowelString[measureObject->currentPiece] = dataD;
	int nrMeasurements = 0;

	processingTimer[measureObject->currentPiece]->SetStart();

	/////////////////shranjujem stringe moznikov za morebitno kasnejso obdelavo
	//QString filename = "C:/data" + QString("/DOWEL_%1.txt").arg(measureObject->currentPiece);
	QString filename = referencePath + QString("/reference/data/DOWEL_%1.txt").arg(measureObject->currentPiece);
	QFile file(filename);
	file.open(QIODevice::WriteOnly);
	file.write(data);
	file.close();
	/////////////////shranjujem stringe moznikov za morebitno kasnejso obdelavo
	int meja = 1200;
	int prehod1 = 1200;
	int prehod2 = 2000;
	int dowelIsOn = 0;
	int prevPrehodType = 0;
	int isSet = 0;
	int isSet2 = 0;
	float korFactorCam0 = measureObject->calibrationkorFactor[measureObject->selectedCalibration][0];

	currentDowel = (unsigned char)data[1];//stevilka moznika 0-15

	lowData = (unsigned char)data[2];
	highData = (unsigned char)data[3];
	dowelNrMeasurements[currentDowel] = (highData << 8) + lowData;	//stevilo meritev moznika

	measureObject->nrDowelMeasurements[measureObject->currentPiece] = dowelNrMeasurements[currentDowel] -1;
	measureObject->dowelNrXilinx[measureObject->currentPiece] = currentDowel;
	for (int i = 0; i < MAX_MEASUREMENT; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			currFramePrehodType[i][j] = 0;
			currDowelPrehodiReal[i][j] = 0.0;
			currFramePrehodAllReal[i][j] = 0.0;
			currFramePrehodAll[i][j] = 0;
		}
	
	currFramePrehod[i][0] = 0;
	currFramePrehod[i][1] = 0;
	}
	
	int m = 4;
	int prevPozicijaZac = 0;
	int prevPozicijaKon = 0;
	for (int nMeritev = 0; nMeritev < dowelNrMeasurements[currentDowel]; nMeritev++)
	{
		int nrPrehodov = (unsigned char)data[m];//stevilo horizontalnih prehodov za vsak frame
		m++;
		isSet = 0;
		isSet2 = 0;
		prevPrehodType = 0;
		for (int i = 0; i < nrPrehodov; i++)
		{
			lowData = (unsigned char)data[m];
			m++;
			highData = (unsigned char)data[m];
			m++;

			int tipPrehoda = highData >> 6;
			currFramePrehodType[nMeritev][i] = tipPrehoda;
			int prehod = ((highData << 8) + lowData) & 0x3FFF;
			currFramePrehodAll[nMeritev][i] = prehod;
			measureObject->horizontalData[measureObject->currentPiece][nMeritev][i] = prehod;
			measureObject->horizontalDataTypes[measureObject->currentPiece][nMeritev][i] = tipPrehoda;
			currFramePrehodAllReal[nMeritev][i] = prehod;

			if (tipPrehoda == 2 && isSet == 0)//zacetek mozbnika
			{
				if (prehod < meja)//sledenje mozniku 
				{
					if ((prehod < prehod1+15) && (prehod > prehod1 -150 ))
					{
						dowelIsOn = 1;
						prehod1 = prehod;
						currFramePrehod[nMeritev][0] = prehod;
						currDowelPrehodiReal[nMeritev][0] = prehod * korFactorCam0;


						measureObject->horizontalTransitions[measureObject->currentPiece][nMeritev][0] = prehod;
						measureObject->horizontalTransitionType[measureObject->currentPiece][nMeritev][0] = tipPrehoda;

						measureObject->horizontalTransitionsReal[measureObject->currentPiece][nMeritev][0] = prehod * korFactorCam0;
						prevPozicijaZac = prehod;
						isSet = 1;
					}
				}

				
			}
			else if (tipPrehoda == 1)//konec moznika
			{
				if (dowelIsOn == 1)
				{
					if (isSet2 == 0)
					{
						if (prevPrehodType == 2 && isSet == 1)//kadar je cel moznik v obmocju 
						{
							prehod2 = prehod;
							currFramePrehod[nMeritev][1] = prehod;
							currDowelPrehodiReal[nMeritev][1] = prehod * korFactorCam0;

							measureObject->horizontalTransitions[measureObject->currentPiece][nMeritev][1] = prehod;
							measureObject->horizontalTransitionsReal[measureObject->currentPiece][nMeritev][1] = prehod * korFactorCam0;
							measureObject->horizontalTransitionType[measureObject->currentPiece][nMeritev][1] = tipPrehoda;
							isSet2 = 1;
						}
						else if ((prevPrehodType == 0 && prevPozicijaZac < prehod))//je ze ven iz obmocja zato 
						{
							prehod2 = prehod;
							currFramePrehod[nMeritev][1] = prehod;
							currDowelPrehodiReal[nMeritev][1] = prehod * korFactorCam0;

							measureObject->horizontalTransitions[measureObject->currentPiece][nMeritev][1] = prehod;
							measureObject->horizontalTransitionsReal[measureObject->currentPiece][nMeritev][1] = prehod * korFactorCam0;
							measureObject->horizontalTransitionType[measureObject->currentPiece][nMeritev][1] = tipPrehoda;
							isSet2 = 1;
						}
					}
				}

			}
			int bla = 100;
			prevPrehodType = tipPrehoda;
		}

		measureObject->currDowelVertical[measureObject->currentPiece][0][nMeritev][0] = 0;
		measureObject->currDowelVertical[measureObject->currentPiece][0][nMeritev][1] = 0;
		//nato sledi prvi in zadnji prehod ostalih kamer
		for (int k = 1; k < 6; k++)
		{

			measureObject->currDowelVertical[measureObject->currentPiece][k][nMeritev][0] = 0;
			measureObject->currDowelVertical[measureObject->currentPiece][k][nMeritev][1] = 0;

			lowData = (unsigned char)data[m];
			m++;
			highData = (unsigned char)data[m];
			m++;


			value = (highData << 8) + lowData;
			if (value == 0)
			{
				int bla = 100;
			}
			//value = value - 500;
			measureObject->currDowelVertical[measureObject->currentPiece][k][nMeritev][0] = value;



			lowData = (unsigned char)data[m];
			m++;
			highData = (unsigned char)data[m];
			m++;


			value = (highData << 8) + lowData;
			//value = value - 500;
			measureObject->currDowelVertical[measureObject->currentPiece][k][nMeritev][1] = value;


		}
	
		


	}
	//////////////////////////////////zacetek obdelave zdruzimo pozicije izracunamo hitrost in dolzino. 
 	measureObject->dowelReady[measureObject->currentPiece] = 2;
	CreateDowelFromString(measureObject->currentPiece); //

	measureObject->dowelReady[measureObject->currentPiece] = 3;
	MeasureDowel(measureObject->currentPiece);


}

void DMSudp::CreateDowelFromString(int currentPiece)
{

//sprehod cez vse meritve in parsanje podatkov 
//merimo dolzino
float  avrageDistance = 0;
int   distanceCounter = 0;


//reset podatkov od prej
for (int i = 0; i < NR_DMS_CAM; i++)
{
	for (int j = 0; j < MAX_MEASUREMENT; j++)
	{
		//measureObject->horizontalData[currentPiece][i] = 0;//pozicije
		measureObject->horizontalDataZACreal[currentPiece][i] = 0.0;//pozicije v mm
		measureObject->horizontalDataKONreal[currentPiece][i] = 0.0;//pozicije v mm
		measureObject->verticalDataZAC[currentPiece][i][j][0] = 0;
		measureObject->verticalDataReal[currentPiece][i][j][0] = 0.0;
		measureObject->verticalDataKON[currentPiece][i][j][1] = 0;
		measureObject->verticalDataReal[currentPiece][i][j][1] = 0.0;
		
	}
}

for (int i = 0; i < measureObject->nrDowelMeasurements[currentPiece]; i++)//merimo dolzino
{

	if ((measureObject->horizontalTransitions[currentPiece][i][0] < 1000) && (measureObject->horizontalTransitions[currentPiece][i][0] > 0) && (measureObject->horizontalTransitions[currentPiece][i][1] > 1000) && (measureObject->horizontalTransitions[currentPiece][i][1] > 0))//v tem obmocju lahko merimo dolzino ker je moznik cel v obmocju
	{
		int middle = measureObject->horizontalTransitions[currentPiece][i][1] - measureObject->horizontalTransitions[currentPiece][i][0];
		middle = middle / 2 + measureObject->horizontalTransitions[currentPiece][i][0];
		if ((middle > 900) && (middle < 1300))
		{
			avrageDistance += measureObject->horizontalTransitionsReal[currentPiece][i][1] - measureObject->horizontalTransitionsReal[currentPiece][i][0];
			measureObject->horizontalMeasurementsOn[currentPiece][i] = 1;
			distanceCounter++;
		}
		else
			measureObject->horizontalMeasurementsOn[currentPiece][i] = 0;
	}

}
if (distanceCounter > 0)
avrageDistance /= distanceCounter;
else
avrageDistance = 0;

//racunamo pozicijo 

int k = 0;
float currSpeedZac = 0;
float currSpeedKon = 0;
int currSpeedCounter = 0;


float tmp[500];
float tmpZad[500];
float offset = 0;

float speedSpredaj = 0;;
float speedZadaj = 0;
float pozicijaSpredaj[500];
float pozicijaZadaj[500];
for (int m = 0; m < 500; m++)
{
	pozicijaSpredaj[m] = 0;
	pozicijaZadaj[m] = 0;
	tmp[m] = 0.0;
	tmpZad[m] = 0.0;
}
for (int i = 0; i < measureObject->nrDowelMeasurements[currentPiece] / 2 + 1; i++)//za racunanje polovice moznika iz sprednje strani 
{
	if (i > 0)
	{
		if ((measureObject->horizontalTransitionsReal[currentPiece][i - 1][0] > 0) && (measureObject->horizontalTransitionsReal[currentPiece][i][0] > 0))
		{
			speedSpredaj += (measureObject->horizontalTransitionsReal[currentPiece][i - 1][0] - measureObject->horizontalTransitionsReal[currentPiece][i][0]);
			currSpeedCounter++;
		}
	}
	if (i == 0)
	{
		tmp[k] = measureObject->horizontalTransitionsReal[currentPiece][0][0] - 40;
		tmpZad[k] = measureObject->horizontalTransitionsReal[currentPiece][0][1] - 40;
	}
	else
	{
		tmp[k] = measureObject->horizontalTransitionsReal[currentPiece][i][0] - 40;
		tmpZad[k] = measureObject->horizontalTransitionsReal[currentPiece][i][1] - 40;
	}



	float cen[6];
	float diameter[6];
	float addCorection[6];

	for (int m = 1; m < NR_DMS_CAM; m++)
	{
		measureObject->verticalDataZAC[currentPiece][m][k][0] = measureObject->currDowelVertical[currentPiece][m][i][0];
		measureObject->verticalDataZAC[currentPiece][m][k][1] = measureObject->currDowelVertical[currentPiece][m][i][1];
		measureObject->verticalDataZACReal[currentPiece][m][k][0] = measureObject->currDowelVertical[currentPiece][m][i][0] * measureObject->calibrationkorFactor[measureObject->selectedCalibration][m];
		measureObject->verticalDataZACReal[currentPiece][m][k][1] = measureObject->currDowelVertical[currentPiece][m][i][1] * measureObject->calibrationkorFactor[measureObject->selectedCalibration][m];
		diameter[m] = measureObject->currDowelVertical[currentPiece][m][i][1] - measureObject->currDowelVertical[currentPiece][m][i][0];
		cen[m] = (diameter[m]) / 2 + measureObject->currDowelVertical[currentPiece][m][i][0];//poiscemo center
	}
	for (int m = 1; m < NR_DMS_CAM; m++)
	{

		if (m == 1)
			addCorection[m] = (cen[2] - measureObject->calibrationCenter[measureObject->selectedCalibration][2])* measureObject->calibrationkorFactorOffset[1];
		else if (m == 2)
			addCorection[m] = (cen[1] - measureObject->calibrationCenter[measureObject->selectedCalibration][1])*  measureObject->calibrationkorFactorOffset[2];
		else if (m == 3)
			addCorection[m] = (cen[1] - measureObject->calibrationCenter[measureObject->selectedCalibration][1])*  measureObject->calibrationkorFactorOffset[3];
		else if (m == 4)
			addCorection[m] = (measureObject->calibrationCenter[measureObject->selectedCalibration][5] - cen[5])*  measureObject->calibrationkorFactorOffset[4];
		else if (m == 5)
			addCorection[m] = (cen[4] - measureObject->calibrationCenter[measureObject->selectedCalibration][4])*  measureObject->calibrationkorFactorOffset[5];

		measureObject->diameterDataZACReal[currentPiece][m][k] =  (diameter[m] * measureObject->calibrationkorFactor[measureObject->selectedCalibration][m]) + addCorection[m];
	}
	

	if (k == 0)
	{
		offset = tmp[0];
		tmp[0] = tmp[0] - offset;
	}
	else
		tmp[k] = abs(tmp[k] - offset);


	measureObject->horizontalDataZACreal[currentPiece][k] = tmp[k];
	k++;


}
measureObject->nrDowelMeasurementsZAC[currentPiece] = k;


speedSpredaj /= currSpeedCounter;
speedSpredaj = speedSpredaj / SYNHPULZTIME / 1000;

///racun pozicije zadaj
k = 0;
currSpeedCounter = 0;

for (int i = measureObject->nrDowelMeasurements[currentPiece] -1; i > measureObject->nrDowelMeasurements[currentPiece] / 2; i--)//za racunanje moznika iz zadnje strani 
{
	if ((measureObject->horizontalTransitionsReal[currentPiece][i - 1][1] > 0) && (measureObject->horizontalTransitionsReal[currentPiece][i][1] > 0))
	{
		speedZadaj += abs(measureObject->horizontalTransitionsReal[currentPiece][i][1] - measureObject->horizontalTransitionsReal[currentPiece][i - 1][1]);
		currSpeedCounter++;
	}
	tmpZad[k] = measureObject->horizontalTransitionsReal[currentPiece][i - 1][1] + 40;

	float cen[6];
	float diameter[6];
	float addCorection[6];

	for (int m = 1; m < NR_DMS_CAM; m++)
	{
		measureObject->verticalDataKON[currentPiece][m][k][0] = measureObject->currDowelVertical[currentPiece][m][i][0];
		measureObject->verticalDataKON[currentPiece][m][k][1] = measureObject->currDowelVertical[currentPiece][m][i][1];
		measureObject->verticalDataKONReal[currentPiece][m][k][0] = measureObject->currDowelVertical[currentPiece][m][i][0] * measureObject->calibrationkorFactor[measureObject->selectedCalibration][m];
		measureObject->verticalDataKONReal[currentPiece][m][k][1] = measureObject->currDowelVertical[currentPiece][m][i][1] * measureObject->calibrationkorFactor[measureObject->selectedCalibration][m];
		diameter[m] = measureObject->currDowelVertical[currentPiece][m][i][1] - measureObject->currDowelVertical[currentPiece][m][i][0];
		cen[m] = (diameter[m]) / 2 + measureObject->currDowelVertical[currentPiece][m][i][0];//poiscemo center
	}


	for (int m = 1; m < NR_DMS_CAM; m++)
	{
		if (m == 1)
			addCorection[m] = (cen[2] - measureObject->calibrationCenter[measureObject->selectedCalibration][2])* measureObject->calibrationkorFactorOffset[1];
		else if (m == 2)
			addCorection[m] = (cen[1] - measureObject->calibrationCenter[measureObject->selectedCalibration][1])*  measureObject->calibrationkorFactorOffset[2];
		else if (m == 3)
			addCorection[m] = (cen[1] - measureObject->calibrationCenter[measureObject->selectedCalibration][1])*  measureObject->calibrationkorFactorOffset[3];
		else if (m == 4)
			addCorection[m] = (measureObject->calibrationCenter[measureObject->selectedCalibration][5] - cen[5])*  measureObject->calibrationkorFactorOffset[4];
		else if (m == 5)
			addCorection[m] = (cen[4] - measureObject->calibrationCenter[measureObject->selectedCalibration][4])*  measureObject->calibrationkorFactorOffset[5];

		measureObject->diameterDataKONReal[currentPiece][m][k] = (diameter[m] * measureObject->calibrationkorFactor[measureObject->selectedCalibration][m]) + addCorection[m];
	}
	if (k == 0)
	{
		offset = tmpZad[0];
		tmpZad[0] = tmpZad[0] - offset;
	}
	else
		tmpZad[k] = tmpZad[k] - offset;


	measureObject->horizontalDataKONreal[currentPiece][k] = tmpZad[k];
	k++;

}
measureObject->nrDowelMeasurementsKON[currentPiece] = k;
speedZadaj /= currSpeedCounter;
speedZadaj = speedZadaj / SYNHPULZTIME / 1000;


measureObject->dowelLenght[currentPiece] = avrageDistance;
measureObject->dowelSpeed[currentPiece] = (speedSpredaj + speedZadaj) / 2;

//izracunamo se teoreticno stevilo meriteve
float teoMeasurements = 0;
teoMeasurements = (measureObject->dowelLenght[currentPiece] / measureObject->dowelSpeed[currentPiece]/1000) / SYNHPULZTIME;
measureObject->dowelAllowedMeasurements[currentPiece][0] = teoMeasurements * 0.8;
measureObject->dowelAllowedMeasurements[currentPiece][1] = teoMeasurements * 1.2;




measureObject->dowelReady[currentPiece] = 3;

//do tukaj preveril podatke o spremembah z zgodovino
}

void DMSudp::MeasureDowel(int currentPiece)
{

	//racunanje na sredini za dolocitev sirine moznika 
	float avrSirinaSredina[NR_DMS_CAM];
	int avrSirinaSredinaCounter[NR_DMS_CAM];
	float avrSirina[NR_DMS_CAM];

	
	int tmpSirina = 0;
	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		avrSirinaSredina[i] = 0;
		avrSirinaSredinaCounter[i] = 0;
		avrSirina[i] = 0;
	}
	for (int m = 1; m < NR_DMS_CAM; m++)
	{
		for (int i = 0; i < measureObject->nrDowelMeasurementsZAC[currentPiece]; i++)
		{
			if (measureObject->horizontalDataZACreal[currentPiece][i] > 7.5)
			{
				tmpSirina = ((measureObject->verticalDataZACReal[currentPiece][m][i][1] - measureObject->verticalDataZACReal[currentPiece][m][i][0]) / 2 + measureObject->verticalDataZACReal[currentPiece][m][i][0]);
				avrSirinaSredina[m] += tmpSirina;
				avrSirinaSredinaCounter[m]++;
			}
		}
		for (int i = 10; i < measureObject->nrDowelMeasurementsKON[currentPiece]; i++)
		{
			if (measureObject->horizontalDataKONreal[currentPiece][i] > 7.5)
			{
				avrSirinaSredina[m] += ((measureObject->verticalDataKONReal[currentPiece][m][i][1] - measureObject->verticalDataKONReal[currentPiece][m][i][0]) / 2 + measureObject->verticalDataKONReal[currentPiece][m][i][0]);
				avrSirinaSredinaCounter[m]++;
			}
		}
		if (avrSirinaSredinaCounter[m] <= 0)
		{
			avrSirinaSredina[m] = 0;
		}
		else
		avrSirinaSredina[m] /= avrSirinaSredinaCounter[m];
	}



	//podatki za izris 
	for (int m = 1; m < NR_DMS_CAM; m++)
	{
		for (int i = 0; i < measureObject->nrDowelMeasurementsZAC[currentPiece]; i++)
		{
			measureObject->verticalDataZACDraw[currentPiece][m][i][0] = avrSirinaSredina[m] - measureObject->verticalDataZACReal[currentPiece][m][i][0];
			measureObject->verticalDataZACDraw[currentPiece][m][i][1] = measureObject->verticalDataZACReal[currentPiece][m][i][1] - avrSirinaSredina[m];

		}
		for (int i = 0; i < measureObject->nrDowelMeasurementsKON[currentPiece]; i++)
		{
			//currDowelTopPointsKon[1][k][1]
			measureObject->verticalDataKONDraw[currentPiece][m][i][0] = avrSirinaSredina[m] - measureObject->verticalDataKONReal[currentPiece][m][i][0];
			measureObject->verticalDataKONDraw[currentPiece][m][i][1] = measureObject->verticalDataKONReal[currentPiece][m][i][1] - avrSirinaSredina[m];
		}

		measureObject->dowelMiddleVertical[currentPiece][m] = avrSirinaSredina[m];
	}

	//racunanje polmerov inpremoerov moznika
	MeasureDowelRadius(currentPiece);
	MeasureDowelConus(currentPiece);


	measureObject->dowelReady[currentPiece] = 4;

}

void DMSudp::MeasureDowelRadius(int currentPiece)
{
	float pos = 0;
	float measureTop;
	float measureBottom;
	float dsk[5];
	float dis[5];
	float dic[5];
	float die[5];
	float dke[5];
	int nrMeasurementsArea[5][5];//pet obmocij 5 kamer
	int nrPositionError[5];
	int nrPositionErrorNegative[5];
	float  roughness[2][5];
	float avrRoughness[2][5];
	float roughnessArray[2][5][500];
	 const int nrAreaRoughenss = 5;
	float dev[5][nrAreaRoughenss];
	float devBottom[5][nrAreaRoughenss];
	float devBetween[5][nrAreaRoughenss];
	float devBetweenBottom[5][nrAreaRoughenss];

	int roughnessArrayCouter = 0;
	int roughnessCounter = 0;
	float maxDiameter = 0;
	float minDiameter = 1000;
	float currDiameter = 0.0;
	float tolHigh =  measureObject->diameterTolerancePlus + measureObject->deltaErrorX;
	float tolLow = measureObject->diameterToleranceMinus - measureObject->deltaErrorX;;


	//tolerancne meje za konus
	float tolLowConus;
	float tolHighConus;
	float tol0 = 0;
	float lenghtKonusPlus = measureObject->konusTolerancePlus;
	float lenghtKonusMinus = measureObject->konusToleranceMinus;
	float diameterPlus =  measureObject->diameterTolerancePlus;
	float diameterMinus=  measureObject->diameterToleranceMinus;
	float midConus = 45;
	float test;
	float sinB = (lenghtKonusPlus) / sin(midConus*M_PI / 180);
	
	test = pow(sinB,2);
	float pitagor = sqrt(test - pow(lenghtKonusPlus,2));

	tolLowConus = diameterMinus - pitagor ;

	sinB = (lenghtKonusMinus) / sin(midConus*M_PI / 180);

	test = pow(sinB,2);
	 pitagor = sqrt(test - pow(lenghtKonusMinus,2));
	 tolHighConus = diameterPlus - pitagor ;

	 sinB = (measureObject->setKonusLenght) / sin(midConus*M_PI / 180);

	 test = pow(sinB, 2);
	 pitagor = sqrt(test - pow(measureObject->setKonusLenght, 2));
	 tol0 = measureObject->setDiameter - pitagor;

	 //izracun enacbe konusa
	 float k,k2, n,n2;

	 k = (tolHighConus - diameterPlus) / (0 - measureObject->konusToleranceMinus);
	 k2 = (tolLowConus - diameterMinus) / (0 - measureObject->konusTolerancePlus);
	 n = diameterPlus - (k*  measureObject->konusToleranceMinus);
	 n2 = diameterMinus - (k* measureObject->konusTolerancePlus);
	 float low;
	 float high;

	for (int i = 0; i < 5; i++)
	{
		for (int z = 0; z < 5; z++)
		{
			dev[i][z] = 0;
			devBottom[i][z] = 0;
			devBetween[i][z] = 0;
			devBetweenBottom[i][z] = 0;
		}
		dsk[i] = 0;
		dis[i] = 0;
		dic[i] = 0;
		die[i] = 0;
		dke[i] = 0;
		nrPositionError[i] = 0;
		nrPositionErrorNegative[i] = 0;
		nrMeasurementsArea[i][0] = 0;
		nrMeasurementsArea[i][1] = 0;
		nrMeasurementsArea[i][2] = 0;
		nrMeasurementsArea[i][3] = 0;
		nrMeasurementsArea[i][4] = 0;
		roughness[0][i] = 0;
		roughness[1][i] = 0;
		avrRoughness[0][i] = 0;
		avrRoughness[1][i] = 0;
		for (int z = 0; z < nrAreaRoughenss; z++)
		{
			dev[i][z] = 0;
			devBottom[i][z] = 0;
			devBetween[i][z] = 0;
			devBetweenBottom[i][z] = 0;
		}
		for (int k = 0; k < 500; k++)
		{
			roughnessArray[0][i][k] = 0;
			roughnessArray[1][i][k] = 0;
		}
	}
	int C = 0;
	for (int m = 1; m < NR_DMS_CAM; m++)
	{
		C = 0;
		roughnessArrayCouter = 0;
		for (int i = 0; i < measureObject->nrDowelMeasurementsZAC[currentPiece]; i++)
		{
			pos = measureObject->horizontalDataZACreal[currentPiece][i];
			measureTop = measureObject->verticalDataZACReal[currentPiece][m][i][0];
			measureBottom = measureObject->verticalDataZACReal[currentPiece][m][i][1];
			//currDiameter = measureBottom - measureTop;
			currDiameter = measureObject->diameterDataZACReal[currentPiece][m][i]; 
			if ((pos >= measureObject->measureArea0[0]) && (pos <= measureObject->measureArea0[1])) //DSK
			{
				dsk[m - 1] += currDiameter;
				measureObject->diametersDSK[currentPiece][m][nrMeasurementsArea[0][m - 1]].setX(pos);
				measureObject->diametersDSK[currentPiece][m ][nrMeasurementsArea[0][m - 1]].setY(currDiameter);

				low = (k * pos + n2) - measureObject->deltaErrorX;
				high = (k * pos +n) + measureObject->deltaErrorX;;

				if (high > tolHigh)
					high = tolHigh;

				if ((currDiameter <= high) && (currDiameter >= low))
				{
					measureObject->diameterDSKGood[currentPiece][m][nrMeasurementsArea[0][m - 1]] = 1;
				}
				else
				{
					measureObject->nrBadMeasurementsDSKAll[currentPiece][m]++;
					measureObject->diameterDSKGood[currentPiece][m][nrMeasurementsArea[0][m - 1]] = 0;
					if (currDiameter < low)
					{
						measureObject->nrBadMeasurementsDSKMinus[currentPiece][m]++;
						measureObject->diameterDSKGood[currentPiece][m][nrMeasurementsArea[0][m - 1]] = 2;//manjse od low
					}

				}
					nrMeasurementsArea[0][m-1]++;
					if (m == 1)
					measureObject->measurementsOnArea[currentPiece][i] = 1;
			}
			else if ((pos >= measureObject->measureArea1[0]) && (pos <= measureObject->measureArea1[1])) //DIS
			{
				dis[m - 1] += currDiameter;
				measureObject->diametersDIS[currentPiece][m][nrMeasurementsArea[1][m - 1]].setX(pos);
				measureObject->diametersDIS[currentPiece][m][nrMeasurementsArea[1][m - 1]].setY(currDiameter);

				if ((currDiameter <= tolHigh) && (currDiameter >= tolLow))
				{
					measureObject->diameterDISGood[currentPiece][m][nrMeasurementsArea[1][m - 1]] = 1;
				}
				else
				{
					measureObject->nrBadMeasurementsDISAll[currentPiece][m]++;
					measureObject->diameterDISGood[currentPiece][m][nrMeasurementsArea[1][m - 1]] = 0;
					if (currDiameter < tolLow)
					{
						measureObject->nrBadMeasurementsDISMinus[currentPiece][m]++;
						measureObject->diameterDISGood[currentPiece][m][nrMeasurementsArea[1][m - 1]] = 2;//manjse od low
					}

				}
				roughness[0][m - 1] += measureTop;
				roughness[1][m - 1] += measureBottom;
				roughnessArray[0][m-1][roughnessArrayCouter] = measureTop;
				roughnessArray[1][m-1][roughnessArrayCouter] = measureBottom;
				roughnessArrayCouter++;
				nrMeasurementsArea[1][m - 1]++;
				if (m == 1)
				measureObject->measurementsOnArea[currentPiece][i] = 2;
			}

			else if ((pos >= measureObject->measureArea2[0]) && (pos <= measureObject->dowelLenght[currentPiece]/2)) //DIC
			{
				dic[m - 1] += currDiameter;
				measureObject->diametersDIC[currentPiece][m][nrMeasurementsArea[2][m - 1]].setX(pos);
				measureObject->diametersDIC[currentPiece][m][nrMeasurementsArea[2][m - 1]].setY(currDiameter);


				if ((currDiameter <= tolHigh) && (currDiameter >= tolLow))
				{
					measureObject->diameterDICGood[currentPiece][m][nrMeasurementsArea[2][m - 1]] = 1;
				}
				else
				{
					measureObject->nrBadMeasurementsDICAll[currentPiece][m]++;
					measureObject->diameterDICGood[currentPiece][m][nrMeasurementsArea[2][m - 1]] = 0;
					if (currDiameter < tolLow)
					{
						measureObject->nrBadMeasurementsDICMinus[currentPiece][m]++;
						measureObject->diameterDICGood[currentPiece][m][nrMeasurementsArea[2][m - 1]] = 2;//manjse od low
					}

				}

				roughness[0][m - 1] += measureTop;
				roughness[1][m - 1] += measureBottom;
				roughnessArray[0][m - 1][roughnessArrayCouter] = measureTop;
				roughnessArray[1][m - 1][roughnessArrayCouter] = measureBottom;
				roughnessArrayCouter++;

				nrMeasurementsArea[2][m - 1]++;
				if (m == 1)
				measureObject->measurementsOnArea[currentPiece][i] = 3;
			}
			else if (pos > measureObject->dowelLenght[currentPiece] / 2)
			{
				nrPositionError[m - 1]++;
			}
			else if (pos < 0)
			{
			nrPositionErrorNegative[m - 1]++;
			}
			
			if(m == 1)
				C++;
		}
		for (int i = measureObject->nrDowelMeasurementsKON[currentPiece]; i > 0; i--)
		{
			pos = measureObject->horizontalDataKONreal[currentPiece][i];
			measureTop = measureObject->verticalDataKONReal[currentPiece][m][i][0];
			measureBottom = measureObject->verticalDataKONReal[currentPiece][m][i][1];
			//currDiameter = measureBottom - measureTop;
			currDiameter =measureObject->diameterDataKONReal[currentPiece][m][i];
			if ((pos >= measureObject->measureArea0[0]) && (pos <= measureObject->measureArea0[1])) //DKE
			{
				dke[m - 1] += currDiameter ;
				measureObject->diametersDKE[currentPiece][m][nrMeasurementsArea[4][m - 1]].setX(pos);
				measureObject->diametersDKE[currentPiece][m][nrMeasurementsArea[4][m - 1]].setY(currDiameter);

				low = (k * pos + n2)  -measureObject->deltaErrorX;
				high = (k * pos + n) + measureObject->deltaErrorX;

				if (high > tolHigh)
					high = tolHigh;

				if ((currDiameter <= high) && (currDiameter >= low))
				{
					measureObject->diameterDKEGood[currentPiece][m][nrMeasurementsArea[4][m - 1]] = 1;
				}
				else
				{
					measureObject->nrBadMeasurementsDKEAll[currentPiece][m]++;
					measureObject->diameterDICGood[currentPiece][m][nrMeasurementsArea[4][m - 1]] = 0;
					if (currDiameter < low)
					{
						measureObject->nrBadMeasurementsDKEMinus[currentPiece][m]++;
						measureObject->diameterDICGood[currentPiece][m][nrMeasurementsArea[4][m - 1]] = 2;//manjse od low
					}

				}
				
				nrMeasurementsArea[4][m - 1]++;
				if (m == 1)
				measureObject->measurementsOnArea[currentPiece][C] = 5;
			}
			else if ((pos >= measureObject->measureArea1[0]) && (pos <= measureObject->measureArea1[1])) //DIE
			{
				die[m - 1] += currDiameter;
				measureObject->diametersDIE[currentPiece][m ][nrMeasurementsArea[3][m - 1]].setX(pos);
				measureObject->diametersDIE[currentPiece][m][nrMeasurementsArea[3][m - 1]].setY(currDiameter);

				if ((currDiameter <= tolHigh) && (currDiameter >= tolLow))
				{
					measureObject->diameterDIEGood[currentPiece][m][nrMeasurementsArea[3][m - 1]] = 1;
				}
				else
				{
					measureObject->nrBadMeasurementsDIEAll[currentPiece][m]++;
					measureObject->diameterDIEGood[currentPiece][m][nrMeasurementsArea[3][m - 1]] = 0;
					if (currDiameter < tolLow)
					{
						measureObject->nrBadMeasurementsDIEMinus[currentPiece][m]++;
						measureObject->diameterDIEGood[currentPiece][m][nrMeasurementsArea[3][m - 1]] = 2;//manjse od low
					}

				}

				roughness[0][m - 1] += measureTop;
				roughness[1][m - 1] += measureBottom;
				roughnessArray[0][m - 1][roughnessArrayCouter] = measureTop;
				roughnessArray[1][m - 1][roughnessArrayCouter] = measureBottom;
				roughnessArrayCouter++;
				nrMeasurementsArea[3][m - 1]++;
				if (m == 1)
				measureObject->measurementsOnArea[currentPiece][C] = 4;
			}

			else if ((pos >= measureObject->measureArea2[0]) && (pos <= measureObject->dowelLenght[currentPiece] / 2)) //DIC
			{
				dic[m - 1] += currDiameter;
				measureObject->diametersDIC[currentPiece][m][nrMeasurementsArea[2][m - 1]].setX(pos);
				measureObject->diametersDIC[currentPiece][m][nrMeasurementsArea[2][m - 1]].setY(currDiameter);

				if ((currDiameter <= tolHigh) && (currDiameter >= tolLow))
				{
					measureObject->diameterDICGood[currentPiece][m][nrMeasurementsArea[2][m - 1]] = 1;
				}
				else
				{
					measureObject->nrBadMeasurementsDICAll[currentPiece][m]++;
					measureObject->diameterDICGood[currentPiece][m][nrMeasurementsArea[2][m - 1]] = 0;
					if (currDiameter < tolLow)
					{
						measureObject->nrBadMeasurementsDICMinus[currentPiece][m]++;
						measureObject->diameterDICGood[currentPiece][m][nrMeasurementsArea[2][m - 1]] = 2;//manjse od low
					}

				}

				roughness[0][m - 1] += measureTop;
				roughness[1][m - 1] += measureBottom;
				roughnessArray[0][m - 1][roughnessArrayCouter] = measureTop;
				roughnessArray[1][m - 1][roughnessArrayCouter] = measureBottom;
				roughnessArrayCouter++;
				nrMeasurementsArea[2][m - 1]++;
				if(m ==1)
				measureObject->measurementsOnArea[currentPiece][C] = 3;
			}
			else if(pos > measureObject->dowelLenght[currentPiece] / 2)
			{
				nrPositionError[m - 1]++;
			}
			else if (pos < 0)
			{
				nrPositionErrorNegative[m - 1]++;
			}
			if (m == 1)
				C++;
		}

		if (nrMeasurementsArea[0][m - 1] > 0)//DSK
			measureObject->measurementDSK[currentPiece][m] = dsk[m - 1]/  nrMeasurementsArea[0][m - 1];
		else
			measureObject->measurementDSK[currentPiece][m] = 0;

		if (nrMeasurementsArea[1][m - 1] > 0)//DIS
			measureObject->measurementDIS[currentPiece][m] = dis[m - 1]/  nrMeasurementsArea[1][m - 1];
		else
			measureObject->measurementDIS[currentPiece][m] = 0;

		if (nrMeasurementsArea[2][m - 1] > 0)//DIC
			measureObject->measurementDIC[currentPiece][m] = dic[m - 1] / nrMeasurementsArea[2][m - 1];
		else
			measureObject->measurementDIC[currentPiece][m] = 0;

		if (nrMeasurementsArea[3][m - 1] > 0)//DIE
			measureObject->measurementDIE[currentPiece][m] = die[m-1] /  nrMeasurementsArea[3][m - 1];
		else
			measureObject->measurementDIE[currentPiece][m] = 0;

		if (nrMeasurementsArea[4][m - 1] > 0)//DKE
			measureObject->measurementDKE[currentPiece][m] = dke[m - 1] / nrMeasurementsArea[4][m - 1];
		else
			measureObject->measurementDKE[currentPiece][m] = 0;

		measureObject->nrMeasurementsArea0[currentPiece][m] = nrMeasurementsArea[0][m - 1];
		measureObject->nrMeasurementsArea1[currentPiece][m] = nrMeasurementsArea[1][m - 1];
		measureObject->nrMeasurementsArea2[currentPiece][m] = nrMeasurementsArea[2][m - 1];//DIC
		measureObject->nrMeasurementsArea3[currentPiece][m] = nrMeasurementsArea[3][m - 1];
		measureObject->nrMeasurementsArea4[currentPiece][m] = nrMeasurementsArea[4][m - 1];

		if (measureObject->measurementDIC[currentPiece][m] < minDiameter)
		{
			minDiameter = measureObject->measurementDIC[currentPiece][m];
		}

		if (measureObject->measurementDIC[currentPiece][m] > maxDiameter)
		{
			maxDiameter = measureObject->measurementDIC[currentPiece][m];
		}
		//roughness
		roughnessCounter = nrMeasurementsArea[1][m - 1] + nrMeasurementsArea[2][m - 1] + nrMeasurementsArea[3][m - 1];
		if (roughnessCounter > 0)
		{
			avrRoughness[0][m - 1] = roughness[0][m - 1] / roughnessCounter;
			avrRoughness[1][m - 1] = roughness[1][m - 1] / roughnessCounter;
		}
		else
		{
			avrRoughness[0][m - 1] = 10000;
			avrRoughness[1][m - 1] = 10000;
		}
		float sum = 0;
		float sumBottom = 0;

		int count = roughnessCounter / nrAreaRoughenss;
		int midCounter = 0;
		int area = 0;
		float maxRoughness = 0;
		for (int z = 0; z < roughnessCounter; z++)
		{
			sum += pow(( roughnessArray[0][m - 1][z] - avrRoughness[0][m - 1]), 2);
			sumBottom += pow(( roughnessArray[1][m - 1][z] - avrRoughness[1][m - 1]), 2);
			midCounter++;
			if (midCounter == count)
			{
				midCounter = 0;
				sum /= roughnessCounter;
				sumBottom /= roughnessCounter;
				dev[m-1][area] = sqrt(sum);
				devBottom[m-1][area] = sqrt(sumBottom);
				area++;
				sum = 0;
				sumBottom = 0;
				if (area >= nrAreaRoughenss)
					break;
			}
		}
		sum = 0;
		sumBottom = 0;
		midCounter = 0;
		area = 0;
		for (int z = count/2; z < roughnessCounter - count/2; z++)
		{
			sum += pow((roughnessArray[0][m - 1][z] - avrRoughness[0][m - 1]), 2);
			sumBottom += pow((roughnessArray[1][m - 1][z] - avrRoughness[1][m - 1]), 2);
			midCounter++;
			if (midCounter == count)
			{
		
				sum /= midCounter;
				sumBottom /= midCounter;
				devBetween[m - 1][area] = sqrt(sum);
				devBetweenBottom[m - 1][area] = sqrt(sumBottom);
				midCounter = 0;
				area++;
				sum = 0;
				sumBottom = 0;
				if (area >= nrAreaRoughenss)
					break;
			}
		}
		for (int b = 0; b < nrAreaRoughenss; b++)
		{
			if (dev[m-1][b] > maxRoughness)
			{
				maxRoughness = dev[m-1][b];
			}
			if (devBottom[m - 1][b] > maxRoughness)
			{
				maxRoughness = devBottom[m - 1][b];
			}
			if (devBetween[m - 1][b] > maxRoughness)
			{
				maxRoughness = devBetween[m - 1][b];
			}
			if (devBetweenBottom[m - 1][b] > devBetweenBottom[m - 1][b])
			{
				maxRoughness = devBetweenBottom[m - 1][b];
			}
		}
		measureObject->measurementRoughness[currentPiece][m] = maxRoughness;
		

		int bla = 100;
	}
	measureObject->mesurementElipse[currentPiece] = maxDiameter - minDiameter;
	measureObject->nrMeasurementsNagative[currentPiece] = nrPositionErrorNegative[0];
 }

void DMSudp::MeasureDowelConus(int currentPiece)
{
	float pos,posPrev;
	float measureTop,measureTopPrev;
	float measureBottom,measureBottomPrev;

	vector<CPointFloat>  konusUS[5];
	vector<CPointFloat>  konusDS[5];
	vector<CPointFloat>  konusDE[5];
	vector<CPointFloat>  konusUE[5];
	vector<CPointFloat>  middleUS[5];
	vector<CPointFloat>  middleDS[5];
	vector<CPointFloat>  middleDE[5];
	vector<CPointFloat>  middleUE[5];
	CLine lineUS, lineDS, lineUE, lineDE;
	CLine lineMiddleUS,lineMiddleDS,lineMiddleUE,lineMiddleDE;
	float d1;
	float d2;
	float deltaX;
	int firstMeasurement = 1;
	float dk = 0;
	posPrev = 0;
	
	for (int m = 1; m < NR_DMS_CAM; m++)
	{
		for (int i = 0; i < measureObject->nrDowelMeasurementsZAC[currentPiece]; i++)
		{
			pos = measureObject->horizontalDataZACreal[currentPiece][i];
			measureTop = measureObject->dowelMiddleVertical[currentPiece][m]- measureObject->verticalDataZACReal[currentPiece][m][i][0];
			measureBottom = measureObject->verticalDataZACReal[currentPiece][m][i][1] - measureObject->dowelMiddleVertical[currentPiece][m];
			
			if ((pos > measureObject->measureArea0[0]) && (pos < measureObject->measureArea0[1])) //US DS DKS
			{
				konusUS[m - 1].push_back(CPointFloat(pos, measureTop));
				konusDS[m - 1].push_back(CPointFloat(pos, measureBottom));

				if (posPrev > 0)
					deltaX = pos - posPrev;
				if (firstMeasurement == 0)
				{
					d1 = measureTopPrev;
					d2 = measureTop;
					dk += (d2 - d1) / deltaX;
				}
				
				firstMeasurement = 0;

			}
			else if ((pos > measureObject->measureArea1[0])&&(pos < measureObject->measureArea1[1]))
			{
				middleUS[m - 1].push_back(CPointFloat(pos, measureTop));
				middleDS[m - 1].push_back(CPointFloat(pos, measureBottom));
				
			}
			posPrev = pos;
			measureTopPrev = measureTop;
			measureBottomPrev = measureBottom;
		}
		

		for (int i = 0; i < measureObject->nrDowelMeasurementsKON[currentPiece]; i++)
		{
			pos = measureObject->horizontalDataKONreal[currentPiece][i];
			measureTop = measureObject->dowelMiddleVertical[currentPiece][m] - measureObject->verticalDataKONReal[currentPiece][m][i][0];
			measureBottom = measureObject->verticalDataKONReal[currentPiece][m][i][1] - measureObject->dowelMiddleVertical[currentPiece][m];

			if ((pos > measureObject->measureArea0[0]) && (pos < measureObject->measureArea0[1])) //UE DE DKE 
			{
				konusUE[m - 1].push_back(CPointFloat(pos, measureTop));
				konusDE[m - 1].push_back(CPointFloat(pos, measureBottom));
			}
			else if ((pos > measureObject->measureArea1[0]) && (pos < measureObject->measureArea1[1]))
			{
				middleUE[m - 1].push_back(CPointFloat(pos, measureTop));
				middleDE[m - 1].push_back(CPointFloat(pos, measureBottom));
			
			}

		}
		lineUS.SetLineNew(konusUS[m - 1]);
		lineDS.SetLineNew(konusDS[m - 1]);
		lineUE.SetLineNew(konusUE[m - 1]);
		lineDE.SetLineNew(konusDE[m - 1]);
		lineMiddleUS.SetLineNew(middleUS[m - 1]);
		lineMiddleDS.SetLineNew(middleDS[m - 1]);
		lineMiddleUE.SetLineNew(middleUE[m - 1]);
		lineMiddleDE.SetLineNew(middleDE[m - 1]);


		measureObject->lineKonus[currentPiece][m][0] = lineUS;
		measureObject->lineKonus[currentPiece][m][1] = lineDS;
		measureObject->lineKonus[currentPiece][m][2] = lineUE;
		measureObject->lineKonus[currentPiece][m][3] = lineDE;
		measureObject->lineMiddle[currentPiece][m][0] = lineMiddleUS;
		measureObject->lineMiddle[currentPiece][m][1] = lineMiddleDS;
		measureObject->lineMiddle[currentPiece][m][2] = lineMiddleUE;
		measureObject->lineMiddle[currentPiece][m][3] = lineMiddleDE;
		

		float tmpDis = 0.0;
		CLine tmpLine;
		CPointFloat tmpPoint;
		//racunam dodatno linijo za izracun kotov  AN
		for (int n = 0; n < 4; n++)
		{
			measureObject->measurementAngleAn[currentPiece][m][n] =abs( measureObject->lineMiddle[currentPiece][m][n].GetLineAngleInDegrees(measureObject->lineKonus[currentPiece][m][n]));
		}


		//izracunam DEL razdaljo med zacetkom konusa in ravno crto na vrhu moznika
		//in dolzino konusa
		CPointFloat interPoint;
		for (int n = 0; n < 4; n++)
		{
			measureObject->measurementDistanceDel[currentPiece][m][n] =  measureObject->lineMiddle[currentPiece][m][n].GetDistanceNew(measureObject->lineKonus[currentPiece][m][n].p1);
			interPoint = measureObject->lineMiddle[currentPiece][m][n].GetIntersectionPointNew(measureObject->lineKonus[currentPiece][m][n]);
			measureObject->measurementKonusLenght[currentPiece][m][n] = abs(interPoint.x);
		}
		

	}




}

void DMSudp::OnClientConnected()
{

	dmsConnected = true;
	//tcpClient->setSocketOption(QAbstractSocket::TypeOfServiceOption, 32);
	//tcpClient->setSocketOption(QAbstractSocket::LowDelayOption, 1);
	InitDMS();




}
void DMSudp::OnClientDisconnected()
{
	dmsConnected = false;
	isInitialized = false;
	xilinxVersion = "";
	connectionTimer->start(waitTime);//poskus ponovne povezave 
	
}

void DMSudp::SpinGainChanged(int index, int gain)
{
	QString sendText;
	QByteArray sendArray;
	int test = index;
	//test = test + 4;
	
	sendText = QString("G%1-%2;").arg(test).arg(gain);

	sendArray = sendText.toLocal8Bit();
	this->gain[index] = gain;
	WriteOneCamSetting(index, "GAIN");
	

	if (sendArray.size() > 0)
		WriteDatagram(sendArray);

}

void DMSudp::SpinOffsetChanged(int index, int offset)
{
	QString sendText;
	QByteArray sendArray;
	int test = index;
	//test = test + 4;
	sendText = QString("O%1-%2;").arg(test).arg(offset);
	this->offset[index] = offset;
	WriteOneCamSetting(index, "OFFSET");
	sendArray = sendText.toLocal8Bit();


	if (sendArray.size() > 0)
		WriteDatagram(sendArray);
}
void DMSudp::SpinDetectWindowChanged(int index,int topBottom, int value)
{
	int bla = 100;

	QString sendText;
	QByteArray sendArray;
	int test = index;
	test = test + 4;
	//sendText = QString("O%1-%2;").arg(test).arg(value); //za nastavljanje registrov moram se pripravit

	QString hexvalueReg, hexvalue;
	int detectReg = 0;
	if(topBottom == 0)
	detectReg = 3 + (3 * index);
	else
	 detectReg = 4 + (3 * index);







	this->detectWindow[index][topBottom] = value;



	hexvalueReg = QString("%1").arg(detectReg, 0, 16);
	hexvalue = QString("%1").arg(detectWindow[index][topBottom], 0, 16);

	sendText = QString("R%1-%2;").arg(hexvalueReg).arg(hexvalue);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);


	if(topBottom == 0)
	WriteOneCamSetting(index, "STARTDETECT");
	else
		WriteOneCamSetting(index, "STOPDETECT");
	//sendArray = sendText.toLocal8Bit();


	/*if (sendArray.size() > 0)
		WriteDatagram(sendArray);*/


}
void DMSudp::SpinGainChangedTest(int gain)
{
	int bla = 100;
}

void DMSudp::ReadDowelFromFile(QString filePath)
{
	
	QString line;

	QFile file(filePath);
	if (file.open(QIODevice::ReadOnly))
	{
		QTextStream stream(&file);


		while (!stream.atEnd())
		{
			QString line = stream.readLine(1); //read one line at a time
			tmpDowelData.push_back(line.toUtf8());
		}

	
		file.close();
	}
}
void DMSudp::OnClickedSelectedDMSCamNew()
{
	int index = ui2.comboBoxSelectCamera->currentIndex();

	selectedCameraDraw = index + 1;

	ShowDowel(currentDowelDraw);
	ShowHorizontalCamera(currentDowelDraw, 0);
}

void DMSudp::OnSignalXilinxVersion(QByteArray version)
{
	int bla = 100;
	if (version.size() > 6)
	{
		xilinxVersion[0] = version[2];
		xilinxVersion[1] = version[3];
		xilinxVersion[2] = version[4];
		xilinxVersion[3] = version[5];
	}
}

void DMSudp::paintEvent(QPaintEvent *)
{


}



void DMSudp::StartClient()
{
	udpClient->bind(port);
	udpClient->connectToHost(hostIp, port, QIODevice::ReadWrite);



}

void DMSudp::InitDMS()
{
	for (int i = 0; i < NR_DMS_CAM; i++)
	{

		SetCameraSettings(i);
	}
	for (int i = 0; i < NR_DMS_CAM; i++)
	{

		SetCameraSettings(i);
	}
	SetBlowValve(measureObject->timeToBlow, measureObject->delayToValve);

	QByteArray sendArray;
	QString sendText;

	//test = test + 4;
	sendText = QString("2643");

	sendArray = sendText.toLocal8Bit();

	WriteDatagram(sendArray);
}

void DMSudp::SetCameraSettings(int nrCam)
{
	QString sendText;
	QByteArray sendArray;

	QString hexvalueReg;
	QString hexvalue;
	int test = nrCam;
	test = test + 4;


	
	int thresholdReg = 2+ (3* nrCam);
	int startDetectReg = 3+ (3 * nrCam);
	int stopDetectReg = 4+ (3 * nrCam);

	//GAIN
	sendText = QString("G%1-%2;").arg(nrCam).arg(gain[nrCam]);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);
	//OFFSET
	sendText = QString("O%1-%2;").arg(nrCam).arg(offset[nrCam]);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);
	////threshold
	hexvalueReg = QString("%1").arg(thresholdReg, 0, 16);
	hexvalue = QString("%1").arg(128, 0, 16);

	sendText = QString("R%1-%2;").arg(hexvalueReg).arg(hexvalue);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);
	////startArea
	hexvalueReg = QString("%1").arg(startDetectReg, 0, 16);
	hexvalue = QString("%1").arg(detectWindow[nrCam][0], 0, 16);

	sendText = QString("R%1-%2;").arg(hexvalueReg).arg(hexvalue);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);
	//stopArea
	hexvalueReg = QString("%1").arg(stopDetectReg, 0, 16);
	hexvalue = QString("%1").arg(detectWindow[nrCam][1], 0, 16);

	sendText = QString("R%1-%2;").arg(hexvalueReg).arg(hexvalue);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);

	int bla = 100;
}

void DMSudp::SetBlowValve(int timeBlow, int timeDelay)
{
	QString sendText;
	QByteArray sendArray;

	sendText = QString("C0-%1;").arg(timeBlow);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);

	sendText = QString("K0-%1;").arg(timeDelay);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);

}

void DMSudp::OnViewTimer()
{

	//ui.labelTakt->setText(QString("%1[Hz]").arg(timeCount));

}
void DMSudp::OnConnectionTimeout()
{
	if ((dmsConnected == 1) && (isInitialized == false))
	{
		InitDMS();
		isInitialized = true;
	}

}

void DMSudp::ReadPendingDatagrams()
{
	 QByteArray recv;
	QByteArray recv2;
	int statusCode = 0;
	char message[4096];



	connectionOnCounter++;
	if (connectionOnCounter > 1000)
	{
		dmsConnected = false;
		isInitialized = false;
		xilinxVersion = "";
		connectionOnCounter = 1001;
	}
	else
		dmsConnected = true;

	recv.clear();

	while ( udpClient->bytesAvailable() > 0)
	{
		connectionOnCounter = 0;
		statusCode = udpClient->bytesAvailable();

			recv = udpClient->readAll();
			int j = 0;
			QByteArray tmp22;
			parseCommandCounter = 0;

			for (int i = 0; i < statusCode; i++)
			{

				parseBuffer[parseCounter] = recv[i];


				if ((parseBuffer[parseCounter - 2] == '\0') && (parseBuffer[parseCounter - 1] == '\r') && (parseBuffer[parseCounter] == '\n'))
				{
					tmp22.clear();
					for (j = 0; j < parseCounter; j++)
					{
						tmp22.append(parseBuffer[j]);
					}
					parseCommands[parseCommandCounter] = tmp22;

					parseCommandCounter++;
					if (parseCommandCounter > 499)
						parseCommandCounter = 0;

					parseCounter = 0;
					j = 0;
				}
				else
				{
					parseCounter++;
				}
			}


			for (int i = 0; i < parseCommandCounter; i++)
			{

				if (parseCommands[i][0] == 'D')//zaenkrat si shranim podatke v fajl
				{
					char tmp = parseCommands[i][1];

					int nrCam = tmp - '0';
					nrCam = (unsigned char)parseCommands[i][1];
				
					//QString line;
					tmpDowelData = parseCommands[i];

					ReadDowelString(parseCommands[i]);


				}
				if (parseCommands[i][0] == 'T') //prehodi
				{
					char tmp = parseCommands[i][1];
					int nrCam = tmp - '0';
					emit transitionReadySignal(nrCam, parseCommands[i]);
				}

				if (parseCommands[i][0] == 'V')//time line
				{
					emit timeLineSignal(parseCommands[i]);
				}

				if (parseCommands[i][0] == 'E')//verzija  xilinxa 
				{
					emit versionSignal(parseCommands[i]);
				}

				if (parseCommands[i][0] == 'L' && parseCommands[0].size() < 3000) //ziva slika
				{
					char tmp = parseCommands[i][1];
					int nrCam = tmp - '0';
					emit frameReadySignal(nrCam, parseCommands[i]);
				}
				parseCommands[i].clear();
				//parseCommandCounter = 0;
			}

		//}
	}
	//} while (tcpClient->bytesAvailable() > 0);
}
void DMSudp::OnSend()
{
	{
		QString sendText;
		QByteArray sendArray;

		sendText = ui.lineSend->text();

		sendArray = sendText.toLocal8Bit();

		ui.lineSend->clear();
		if (sendArray.size() > 0)
			WriteDatagram(sendArray);
	}
}
void DMSudp::WriteDatagram(QByteArray data)
{

	QByteArray sendData = data;
	QByteArray newText = data;

	QByteArrayMatcher carrageReturn;
	carrageReturn.setPattern("\r\n");


	if (carrageReturn.indexIn(sendData, 0) == -1)
	{
		sendData.append("\r\n");
	}
	//lock.lockForWrite();
	//udpClient->writeDatagram(sendData, hostIp, port);
	udpClient->write(sendData);
	udpClient->flush();
	//lock.unlock();
}




