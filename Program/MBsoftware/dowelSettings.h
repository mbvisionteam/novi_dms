#pragma once

#include "stdafx.h"
#include <QWidget>
#include "ui_DowelSettings.h"
#include "AdvancedSettings.h"

class DowelSettings : public QWidget
{
	Q_OBJECT

public:
	DowelSettings(QString referencePath);
	~DowelSettings();
	void closeEvent(QCloseEvent * event);
	void OnShowDialog(int rights, int currentType);
	void ReadAdvancedSettings();


	Ui::dowelSettings ui;



	QString referencePath;
	int currentType,prevType;
	vector<vector<float>> basicSettings;
	vector<QString> typeName;
	vector<QString> typeAdvSettings;
	vector<QString> typeColorSettings;
	float tmpSettings[9];
	int dowelWithConus;
	//vector <AdvancedSettings*> settings;
	QString selectedAdvSettings;
	QString selectedColorCameraSettings;
	bool advDisplayCreated;


	vector<AdvancedSettings> advSettings;//za shranjevanje mbsoftware 


public slots:
	void OnClickedOk();
	void OnClickedCancel();
	void SpinLenghtChanged(double value);
	void SpinWidthChanged(double value);
	void SpinKonusChanged(double value);
	void SpinLenghtMaxChanged();
	void SpinWidthMaxChanged();
	void SpinKonusMaxChanged();
	void OnClickRadioWithKonus();
	void OnClickRadioWithoutKonus();
	
	void TypeChanged();
	void OnClickSelectType();
	void OnClickSelectAdvancedSettings();
	void OnClickSaveAdvancedSettings();
	void SaveChangesToType();
	bool IsTypeChanged();
	int  OnSaveChanges();
	void CreateAdvDisplay();
	void ReplaceAdvanceSettings(QString advSettingsName);
	void OnClickCreateNewAdvancedSettings();
	void CopyAdvSettings(QString existed, QString newName);
	void OnClickedDeleteAdvancedSettings();
	void DeleteAdvancedSettings(QString selected);
	void OnClickedDeleteType();
	void OnClickedNewType();




signals:
	void	newTypeSelected(int selectedType);
	void    advancedSettingsSaved(QString selected);
	void	createNewTypeSignal(QString existed, QString newName);
	void	deleteType(QString name);

};

