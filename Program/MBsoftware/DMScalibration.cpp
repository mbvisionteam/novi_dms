#include "stdafx.h"
#include "DMScalibration.h"

Measurand*									DMScalibration::measureObject; //object with mesurand parameters

DMScalibration::DMScalibration(QString filePath)
{
	autoCalibrationDialog = new QWidget();
	ui.setupUi(this);
	ui2.setupUi(autoCalibrationDialog);
	OnTimer = new QTimer(this);

	referencePath = filePath;
	//Povezave SIGNAL-SLOT
	connect(OnTimer, SIGNAL(timeout()), this, SLOT(ViewTimer()));
	connect(ui.buttonEnterRefDowel, SIGNAL(clicked()), this, SLOT(OnClickedEnterRefDowel()));
	connect(ui.buttonSave, SIGNAL(clicked()), this, SLOT(OnClickedSaveCorrection()));
	connect(ui.buttonClose, SIGNAL(clicked()), this, SLOT(OnClickedClosedDialog()));
	connect(ui.buttonReadRefCenter, SIGNAL(clicked()), this, SLOT(OnClickedGetRefCenters()));
	connect(ui.buttonCalcKorFactor, SIGNAL(clicked()), this, SLOT(OnClickedCalculateKorFactors()));
	connect(ui.buttonCalcKorFactorWidth, SIGNAL(clicked()), this, SLOT(OnClickedCalculateKorFactorWidth()));
	connect(ui.buttonAutoCalibration, SIGNAL(clicked()), this, SLOT(OnClickedAutoCalibrationMode()));
	connect(ui2.buttonSave, SIGNAL(clicked()), this, SLOT(OnClickedSaveAutoCalibration()));
	connect(ui2.buttonCancel, SIGNAL(clicked()), this, SLOT(OnClickedCancelAutoCalibration()));




	//za avtoreferenco
	CreateAutoCalibrationTable();
	
}

DMScalibration::~DMScalibration()
{

}

void DMScalibration::closeEvent(QCloseEvent * event)
{
	OnTimer->stop();
	
}


void DMScalibration::OnShowDialog(int rights, int calibration, float diameter, float lenght)
{
	setWindowModality(Qt::ApplicationModal);
	refDowelDiameter = diameter;
	refDowelLenght = lenght;
	selectedCalibration = calibration;
	this->show();
	OnTimer->start(100);

	if (calibration > -1)
	{
		SelectedReference(selectedCalibration);
	}

	
}
void DMScalibration::ReadCalibrationSettings()
{

	int count = 0;

	QStringList values;

	this->filePath = referencePath + QString("/%1/settings/CalibrationSettings/calibration.ini").arg(REF_FOLDER_NAME);

	if (!QDir(this->filePath).exists())
		QDir().mkdir(this->filePath);

	QSettings settings(this->filePath, QSettings::IniFormat);

	for (int i = 1; i < 20; i++)
	{
		count = 0;
		QString tmpGroup;
		tmpGroup = QString("%1mm").arg(i);
		settings.beginGroup(tmpGroup);
		//inputs
		const QStringList childKeys = settings.childKeys();
		int nrCam = 0;
		if (childKeys.size() > 0)
		{
			do {

				values.clear();
				values = settings.value(QString("parameter%1").arg(count)).toStringList();
				if (values.size() > 0)
				{
					if (count < 6)
						measureObject->calibrationCenter[i][nrCam] = values[0].toInt();
					else
						measureObject->calibrationkorFactor[i][nrCam] = values[0].toFloat();
				}
				nrCam++;
				if (nrCam > 5)
					nrCam = 0;
				count++;

			} while (values.size() > 0);
		}
		settings.endGroup();
	}
}

void DMScalibration::WriteCalibrationSettings(int index)
{

		QSettings settings(filePath, QSettings::IniFormat);
		QStringList list;
		QString tmpGroup;
		tmpGroup = QString("%1mm").arg(index);
		//settings.remove(tmpGroup);
		settings.beginGroup(tmpGroup);


		int nrCam = 0;

				for (int k = 0; k <12; k++)
				{
					if(k <6)
					list << QString("%1").arg(measureObject->calibrationCenter[index][nrCam]);
					else
					list << QString("%1").arg(measureObject->calibrationkorFactor[index][nrCam]);

					nrCam++;
					if (nrCam > 5)
						nrCam = 0;
					settings.setValue(QString("parameter%1").arg(k), list);
					list.clear();
				}
			
		
		settings.endGroup();

}

void DMScalibration::ConnectMeasurand(Measurand * objects)
{
	measureObject = objects; 
	ReadCalibrationSettings();
}

void DMScalibration::CreateAutoCalibrationTable()
{
	parametersTable = new QTableView();
	modelTable = new QStandardItemModel();
	int nrMeasurements = 5;

	parametersTable->setFont(QFont("Times new Roman", 12));
	modelTable->setColumnCount(6);
	modelTable->setHeaderData(0, Qt::Horizontal, tr("WIDTH0"));
	modelTable->setHeaderData(1, Qt::Horizontal, tr("WIDTH1"));
	modelTable->setHeaderData(2, Qt::Horizontal, tr("WIDTH2"));
	modelTable->setHeaderData(3, Qt::Horizontal, tr("WIDTH3"));
	modelTable->setHeaderData(4, Qt::Horizontal, tr("WIDTH4"));
	modelTable->setHeaderData(5, Qt::Horizontal, tr("LENGHT"));


	modelTable->setRowCount(nrMeasurements);
	parametersTable->setModel(modelTable);

	for (int i = 0; i < 5; i++)
	{
		parametersTable->setRowHeight(i, 21);

	}
	standardTableItem.resize(nrMeasurements);

	for (int k = 0; k < nrMeasurements; k++)
	{
		for (int z = 0; z < 6; z++)
		{
			standardTableItem[k].push_back(new QStandardItem());
			modelTable->setItem(k, z, standardTableItem[k].back());
			standardTableItem[k].back()->setText(QString(""));
		}
	}
	
	for (int i = 0; i < 6; i++)
	{
		parametersTable->setColumnWidth(i, 90);
	}

	int w = parametersTable->width();
	int h = parametersTable->height();
	parametersTable->height();

	ui2.layoutMeasurements->addWidget(parametersTable, Qt::AlignCenter);

}

void DMScalibration::UpdateAutoCalibrationTable(int index)
{

	for (int i = 1; i < 6; i++)
	{
		autoCalibrationWidth[autoCalibrationIndexCounter][i -1] = measureObject->measurementDIC[index][i];
	}
	 autoCalibrationLenght[autoCalibrationIndexCounter] = measureObject->dowelLenght[index];



		 for (int column = 0; column < 6; column++)
		 {
			 if (column <= 4)
				 standardTableItem[autoCalibrationIndexCounter][column]->setText(QString("%1").number(autoCalibrationWidth[autoCalibrationIndexCounter][column], 'f', 2));
			 else
				 standardTableItem[autoCalibrationIndexCounter][column]->setText(QString("%1").number(autoCalibrationLenght[autoCalibrationIndexCounter], 'f', 2));
			 // standardTableItem[autoCalibrationIndexCounter][column]->

		 }
	 

		// emit modelTable->dataChanged(modelTable->index(0, 0), modelTable->index(5, 1));
	 // emit  modelTable->layoutChanged();
		// model->index(i, 0), model->index(i, 1)
	  parametersTable->viewport()->update();
		 autoCalibrationIndexCounter++;
	//	 autoCalibrationDialog->update();
		
		// parametersTable->update();
		 
		 if (autoCalibrationIndexCounter == 5)
		 {
			 autoCalibrationIndexCounter = 4;
			 CalculateNewCalibrationFactor();
		 }
		 ui2.layoutMeasurements->update();
	 
}

void DMScalibration::CalculateNewCalibrationFactor()
{
	float avrage[6];
	for (int i = 0; i < 6; i++)
		avrage[i] = 0;



	for (int i = 0; i < 5; i++)
	{
		avrage[1] += autoCalibrationWidth[i][0];
		avrage[2] += autoCalibrationWidth[i][1];
		avrage[3] += autoCalibrationWidth[i][2];
		avrage[4] += autoCalibrationWidth[i][3];
		avrage[5] += autoCalibrationWidth[i][4];
			avrage[0] += autoCalibrationLenght[i];
	}
	
	for (int i = 0; i < 6; i++)
	{
		avrage[i] /= 5;
		if(i > 0)
			newAutoCalibrationFactors[i] = (refDowelDiameter / avrage[i] * measureObject->calibrationkorFactor[selectedCalibration][i]);
		else
			newAutoCalibrationFactors[i] = (refDowelLenght / avrage[i] * measureObject->calibrationkorFactor[selectedCalibration][i]);



	}

	ui2.labelNewCorr0->setText(QString("%1").arg(newAutoCalibrationFactors[0]));
	ui2.labelNewCorr1->setText(QString("%1").arg(newAutoCalibrationFactors[1]));
	ui2.labelNewCorr2->setText(QString("%1").arg(newAutoCalibrationFactors[2]));
	ui2.labelNewCorr3->setText(QString("%1").arg(newAutoCalibrationFactors[3]));
	ui2.labelNewCorr4->setText(QString("%1").arg(newAutoCalibrationFactors[4]));
 	ui2.labelNewCorr5->setText(QString("%1").arg(newAutoCalibrationFactors[5]));
}

void DMScalibration::ViewTimer()
{
	int bla = 100;

	//ui.labelDix1->setText(QString(measureObject->calibrationTransitions[1][1]))
		for (int i = 0; i < 6; i++)
		{
			dix[i] = measureObject->calibrationTransitions[i][1] - measureObject->calibrationTransitions[i][0];
			cen[i] = dix[i] / 2 + measureObject->calibrationTransitions[i][0];
		}

		ui.labelDix1->setText(QString("%1").arg(dix[1]));
		ui.labelDix2->setText(QString("%1").arg(dix[2]));
		ui.labelDix3->setText(QString("%1").arg(dix[3]));
		ui.labelDix4->setText(QString("%1").arg(dix[4]));
		ui.labelDix5->setText(QString("%1").arg(dix[5]));
		
		ui.labelCen1->setText(QString("%1").arg(cen[1]));
		ui.labelCen2->setText(QString("%1").arg(cen[2]));
		ui.labelCen3->setText(QString("%1").arg(cen[3]));
		ui.labelCen4->setText(QString("%1").arg(cen[4]));
		ui.labelCen5->setText(QString("%1").arg(cen[5]));

		ui.labelDia1->setText(QString("%1").arg(dix[1]*measureObject->calibrationkorFactor[selectedCalibration][1]));
		ui.labelDia2->setText(QString("%1").arg(dix[2]*measureObject->calibrationkorFactor[selectedCalibration][2]));
		ui.labelDia3->setText(QString("%1").arg(dix[3]*measureObject->calibrationkorFactor[selectedCalibration][3]));
		ui.labelDia4->setText(QString("%1").arg(dix[4]*measureObject->calibrationkorFactor[selectedCalibration][4]));
		ui.labelDia5->setText(QString("%1").arg(dix[5]*measureObject->calibrationkorFactor[selectedCalibration][5]));

		float withCalibration[6];
		float faktor = 1;

		float vmes[6];
		vmes[1] = (cen[2] - measureObject->calibrationCenter[selectedCalibration][2])* 0.001 + ( dix[1] * measureObject->calibrationkorFactor[selectedCalibration][1]);
		vmes[2] = (cen[1] - measureObject->calibrationCenter[selectedCalibration][1])* 0.01 + ( dix[2] * measureObject->calibrationkorFactor[selectedCalibration][2]);
		vmes[3] = (cen[1] - measureObject->calibrationCenter[selectedCalibration][1])* 0.01 + ( dix[3] * measureObject->calibrationkorFactor[selectedCalibration][3]);
		vmes[4] = (measureObject->calibrationCenter[selectedCalibration][5]- cen[5])* 0.001 + ( dix[4] * measureObject->calibrationkorFactor[selectedCalibration][4]);
		vmes[5] = (cen[4] -measureObject->calibrationCenter[selectedCalibration][4])* 0.001 + ( dix[5] * measureObject->calibrationkorFactor[selectedCalibration][5]);

		//vmes[0] = dix[0] * measureObject->calibrationkorFactor[selectedCalibration][5]);

		ui.labelDiaC1->setText(QString("%1").arg(vmes[1]));
		ui.labelDiaC2->setText(QString("%1").arg(vmes[2]));
		ui.labelDiaC3->setText(QString("%1").arg(vmes[3]));
		ui.labelDiaC4->setText(QString("%1").arg(vmes[4]));
		ui.labelDiaC5->setText(QString("%1").arg(vmes[5]));


		//width
		ui.labelWidthPix->setText(QString("%1").arg(dix[0]));
		ui.labelWidthReal->setText(QString("%1").arg(dix[0] * measureObject->calibrationkorFactor[selectedCalibration][0]));
	//	ui.lineEditKorFactor0->setText(QString("%1").arg(measureObject->calibrationkorFactor[selectedCalibration][0]));
		ui.labelRefDiameter->setText(QString("%1").arg(refDowelDiameter));
		ui.labelRefLenght->setText(QString("%1").arg(refDowelLenght));
		ui.labelSelectedCalibration->setText(QString("%1 [mm]").arg(selectedCalibration));

}

void DMScalibration::OnClickedEnterRefDowel(void)
{
	bool ok;
	QString textLabel;
	QInputDialog dlg;
	QFont timesNewRomanFont("Times New Roman", 14, QFont::Normal);
	dlg.setInputMode(QInputDialog::DoubleInput);;
	dlg.setDoubleStep(0.1);
	dlg.setFont(timesNewRomanFont);
	dlg.setLabelText("Enter ref dowel diameter [mm]");
	dlg.setDoubleValue(0.00);
	if (dlg.exec() == IDOK)
	{
		refDowelDiameter = dlg.doubleValue();

		dlg.setDoubleStep(0.1);
		dlg.setFont(timesNewRomanFont);
		dlg.setLabelText("Enter ref dowel Lenght [mm]");

		dlg.setDoubleValue(0.00);
		if (dlg.exec() == IDOK)
		{
			refDowelLenght = dlg.doubleValue();
		}
		else
		{
			refDowelDiameter = -1;
			refDowelLenght = -1;

		}

	}
	else
	{
		refDowelDiameter = -1;
		refDowelLenght = -1;
	}

	selectedCalibration = round(refDowelDiameter);


	if (selectedCalibration > 20)
		selectedCalibration = 20;

	SelectedReference(selectedCalibration);
	
	//izbran moznik sedaj zapisemo v tabelo katere reference uposteva
}

void DMScalibration::SelectedReference(int index)
{
	if (selectedCalibration < 1)
	{
		ui.lineEditCenter1->setText("");
		ui.lineEditCenter2->setText("");
		ui.lineEditCenter3->setText("");
		ui.lineEditCenter4->setText("");
		ui.lineEditCenter5->setText("");
		ui.lineEditKorFactor0->setText("");
		ui.lineEditKorFactor1->setText("");
		ui.lineEditKorFactor2->setText("");
		ui.lineEditKorFactor3->setText("");
		ui.lineEditKorFactor4->setText("");
		ui.lineEditKorFactor5->setText("");
	}
	else
	{
		ui.lineEditCenter1->setText(QString("%1").arg(measureObject->calibrationCenter[index][1]));
		ui.lineEditCenter2->setText(QString("%1").arg(measureObject->calibrationCenter[index][2]));
		ui.lineEditCenter3->setText(QString("%1").arg(measureObject->calibrationCenter[index][3]));
		ui.lineEditCenter4->setText(QString("%1").arg(measureObject->calibrationCenter[index][4]));
		ui.lineEditCenter5->setText(QString("%1").arg(measureObject->calibrationCenter[index][5]));
		ui.lineEditKorFactor0->setText(QString("%1").arg(measureObject->calibrationkorFactor[index][0]));
		ui.lineEditKorFactor1->setText(QString("%1").arg(measureObject->calibrationkorFactor[index][1]));
		ui.lineEditKorFactor2->setText(QString("%1").arg(measureObject->calibrationkorFactor[index][2]));
		ui.lineEditKorFactor3->setText(QString("%1").arg(measureObject->calibrationkorFactor[index][3]));
		ui.lineEditKorFactor4->setText(QString("%1").arg(measureObject->calibrationkorFactor[index][4]));
		ui.lineEditKorFactor5->setText(QString("%1").arg(measureObject->calibrationkorFactor[index][5]));
	}
}

void DMScalibration::OnClickedSaveCorrection()
{

	measureObject->calibrationCenter[selectedCalibration][1] = ui.lineEditCenter1->text().toInt();
	measureObject->calibrationCenter[selectedCalibration][2] = ui.lineEditCenter2->text().toInt();
	measureObject->calibrationCenter[selectedCalibration][3] = ui.lineEditCenter3->text().toInt();
	measureObject->calibrationCenter[selectedCalibration][4] = ui.lineEditCenter4->text().toInt();
	measureObject->calibrationCenter[selectedCalibration][5] = ui.lineEditCenter5->text().toInt();

	measureObject->calibrationkorFactor[selectedCalibration][0] = ui.lineEditKorFactor0->text().toFloat();
	measureObject->calibrationkorFactor[selectedCalibration][1] = ui.lineEditKorFactor1->text().toFloat();
	measureObject->calibrationkorFactor[selectedCalibration][2] = ui.lineEditKorFactor2->text().toFloat();
	measureObject->calibrationkorFactor[selectedCalibration][3] = ui.lineEditKorFactor3->text().toFloat();
	measureObject->calibrationkorFactor[selectedCalibration][4] = ui.lineEditKorFactor4->text().toFloat();
	measureObject->calibrationkorFactor[selectedCalibration][5] = ui.lineEditKorFactor5->text().toFloat();

	WriteCalibrationSettings(selectedCalibration);

}

void DMScalibration::OnClickedClosedDialog()
{
	this->close();
}

void DMScalibration::OnClickedGetRefCenters()
{
	ui.lineEditCenter1->setText(QString("%1").arg(cen[1]));
	ui.lineEditCenter2->setText(QString("%1").arg(cen[2]));
	ui.lineEditCenter3->setText(QString("%1").arg(cen[3]));
	ui.lineEditCenter4->setText(QString("%1").arg(cen[4]));
	ui.lineEditCenter5->setText(QString("%1").arg(cen[5]));
}

void DMScalibration::OnClickedCalculateKorFactors()
{
	ui.lineEditKorFactor1->setText(QString("%1").arg(refDowelDiameter / dix[1]));
	ui.lineEditKorFactor2->setText(QString("%1").arg(refDowelDiameter / dix[2]));
	ui.lineEditKorFactor3->setText(QString("%1").arg(refDowelDiameter / dix[3]));
	ui.lineEditKorFactor4->setText(QString("%1").arg(refDowelDiameter / dix[4]));
	ui.lineEditKorFactor5->setText(QString("%1").arg(refDowelDiameter / dix[5]));

}

void DMScalibration::OnClickedCalculateKorFactorWidth()
{
	ui.lineEditKorFactor0->setText(QString("%1").arg(refDowelLenght / dix[0]));

}

void DMScalibration::OnClickedAutoCalibrationMode()
{
	//widget.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
	//widget.show();

	autoCalibrationDialog->setWindowFlags(Qt::Window
		| Qt::WindowMinimizeButtonHint
		| Qt::WindowMaximizeButtonHint);
	autoCalibrationDialog->setWindowModality(Qt::ApplicationModal);
	parametersTable->setFocus();
	//autoCalibrationDialog->setFocus();
	emit enterAutoCalibration();

	for (int k = 0; k < 5; k++)
	{
		for (int i = 0; i < 6; i++)
		{
			autoCalibrationWidth[k][i] = 0;
		}
		autoCalibrationLenght[k] = 0;

	}

	for (int k = 0; k < 5; k++)
	{
		for (int column = 0; column < 6; column++)
		{
			if (column <= 4)
				standardTableItem[k][column]->setText(QString(""));
			else
			standardTableItem[k][column]->setText(QString(""));

		}
	}
	ui2.labelOldCorr0->setText(QString("%1").arg(measureObject->calibrationkorFactor[selectedCalibration][0]));
	ui2.labelOldCorr1->setText(QString("%1").arg(measureObject->calibrationkorFactor[selectedCalibration][1]));
	ui2.labelOldCorr2->setText(QString("%1").arg(measureObject->calibrationkorFactor[selectedCalibration][2]));
	ui2.labelOldCorr3->setText(QString("%1").arg(measureObject->calibrationkorFactor[selectedCalibration][3]));
	ui2.labelOldCorr4->setText(QString("%1").arg(measureObject->calibrationkorFactor[selectedCalibration][4]));
	ui2.labelOldCorr5->setText(QString("%1").arg(measureObject->calibrationkorFactor[selectedCalibration][5]));

	ui2.labelNewCorr0->setText("");
	ui2.labelNewCorr1->setText("");
	ui2.labelNewCorr2->setText("");
	ui2.labelNewCorr3->setText("");
	ui2.labelNewCorr4->setText("");
	ui2.labelNewCorr5->setText("");

	parametersTable->viewport()->update();
	autoCalibrationIndexCounter = 0;


	autoCalibrationDialog->show();
}

void DMScalibration::OnClickedSaveAutoCalibration()
{
	ui.lineEditKorFactor1->setText(QString("%1").arg(newAutoCalibrationFactors[1]));
	ui.lineEditKorFactor2->setText(QString("%1").arg(newAutoCalibrationFactors[2]));
	ui.lineEditKorFactor3->setText(QString("%1").arg(newAutoCalibrationFactors[3]));
	ui.lineEditKorFactor4->setText(QString("%1").arg(newAutoCalibrationFactors[4]));
	ui.lineEditKorFactor5->setText(QString("%1").arg(newAutoCalibrationFactors[5]));
	ui.lineEditKorFactor0->setText(QString("%1").arg(newAutoCalibrationFactors[0]));
	autoCalibrationDialog->close();
	 emit leaveAutoCalibration();
}

void DMScalibration::OnClickedCancelAutoCalibration()
{
	autoCalibrationDialog->close();
	emit leaveAutoCalibration();
}
