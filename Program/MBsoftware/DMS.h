#pragma once

#include <QWidget>
#include "ui_DMS.h"
#include "ui_DMStransitionTest.h"
#include <QtNetwork/qudpsocket.h>
#include <QtNetwork/qtcpsocket.h>
#include "Timer.h"
#include "qpainter.h"
#define NR_DMS_CAM 6
#define DMS_CAM_WIDTH 2200

class DMS : public QWidget
{
	Q_OBJECT

public:
	DMS(QWidget *parent = Q_NULLPTR);
	DMS(QString ipAddress, int port);
	QWidget	* transitionTestWidget;
	void closeEvent(QCloseEvent * event);
	~DMS();

private:
	Ui::DMS ui;
	Ui::DMSTestWindow ui2;
	QGraphicsScene*			sceneVideo[6];
	QGraphicsScene*			sceneDowel;
	QGraphicsPixmapItem*	pixmapVideo[6];
	QGraphicsPixmapItem*	pixmapDowel;

	QReadWriteLock lock;
	QLabel*					transitionDataTestLabel[NR_DMS_CAM][5][2];//tabela za zgornje in spodnje prehode 
	int						transitionDataTest[NR_DMS_CAM][5][2];//tabela za zgornje in spodnje prehode pri testnem oknu za prehode 
	int						isTransitionDisplaySet;


	byte					parseBuffer[15000];
	int						parseCounter;
	QByteArray				parseCommands[500];
	int						parseCommandCounter;

public:
	QTimer*							viewTimer;
	static QTimer*							connectionTimer;
	//QUdpSocket*		udpClient;
	QTcpSocket*			tcpClient;
	QHostAddress hostIp;
	Timer							dataReadyFreq;
	int port;
	QString ipAdd;
	int connectionOnCounter, connectionOnPrevCounter;
	bool isConnected, isConnectedOld;
	bool isInitialized;
	void ReadPendingDatagrams();
	//�as �akanja na povezavo v milisekundah
	int waitTime;
	int timout;

	void WriteDatagram(QByteArray data);
	vector <QPoint> dowelPoints;
	vector <int > dowelPrehodi;

	void OnShowDialog();
	void OnShowDialogTransitionTest();
	void CreateDisplay();
	void CreateDisplayTransitionTest();
	void SetDisplay();
	void StartClient();
	void InitDMS();
	void SetCameraSettings(int nrCam);

	void ReadDowelFromFile(QString filePath);
	int isLive;
	int isLiveTransitions;
	virtual void paintEvent(QPaintEvent * event);

	QByteArray tmpDowelData;
	
	int gain[NR_DMS_CAM];
	int offset[NR_DMS_CAM];
	int detectWindow[NR_DMS_CAM][2];
	vector <QPoint> dowelRealPoints[16];
	float			dowelSpeed[16];
	float			dowelLenght[16];
	int				dowelNrMeasurements[16];
	int				dowelReady[16];
	int				currentDowelDraw;
	

	QSpinBox *spinWindwSTOP[NR_DMS_CAM];
	QSpinBox *spinWindwSTART[NR_DMS_CAM];
	QSpinBox *spinGain[NR_DMS_CAM];
	QSpinBox *spinOffset[NR_DMS_CAM];
	QImage liveImage[NR_DMS_CAM];
	int widthDmsCam;
	int dmsConnected;
	int nrRecievedDowels;


private slots:
	//void OnNewConnection();
	//void OnConnectionTimeout();
	void OnViewTimer();

	void OnSend();
	void OnClickedLive();
	void OnClickedLiveTranstions();
	void WriteCamSettings();
	void WriteOneCamSetting(int index, QString setting);
	void ReadCamSettings();

	void ShowImage(int, QByteArray image);//
	void ShowTransitions(int, QByteArray image);//parsa podatke iz stringa in jih prikaze v ui2
	void ShowDowel(int nrDowel);
	void ReadDowelString();
	

public slots:
	void OnClientConnected();

	void OnClientDisconnected();
	void OnConnectionTimeout();
	void SpinGainChanged(int index, int gain);

	void SpinOffsetChanged(int index, int offset);

	void SpinDetectWindowChanged(int index, int topBottom, int value);

	void SpinGainChangedTest(int gain);

	

signals:
	void frameReadySignal(int id, QByteArray image);//signal za prikaz nove slike
	void transitionReadySignal(int nrCam, QByteArray parseData);//signal za prikaz test transitions

	

};
