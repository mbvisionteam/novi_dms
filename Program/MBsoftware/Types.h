#pragma once


#include "stdafx.h"
#include "ui_types.h"
#include "ui_TypesSettings.h"
#include "CProperty.h"
#include "Classifier.h"

using namespace::std;
class Types : public QWidget
{
	Q_OBJECT

public:
	Types();
	Types(int station);
	~Types();
	QWidget	* novoOkno;
	QString					typeName;
	QString					typeSettingsPath;
	QString					typePath;
	int						parameterCounter;
	int						station;
	std::vector<vector<float>>	measuredValueDMS;
	std::vector<vector<int>>		isGoodDMS;
	std::vector<float>		measuredValue;
	std::vector<int>		isGood;
	std::vector<int>		conditional;

	std::vector<float>		toleranceHigh;
	std::vector<float>		toleranceLow;
	std::vector<float>		toleranceHighCond;
	std::vector<float>		toleranceLowCond;
	std::vector<float>		correctFactor;
	std::vector<float>		offset;
	std::vector<float>		nominal;
	std::vector<int>		isActive;
	std::vector<int>		isConditional;
	std::vector<int>		badCounterByParameter;
	std::vector<QString>	name;
	int						id;
	int						conditionalMeasurementsCounter;
	int						goodMeasurementsCounter;
	int						badMeasurementsCounter;
	int						totalMeasurementsCounter;
	int						totalGlobalMeasurementsCounter;
	int						valveOnCounter;
	int						inBoxCounter;
	int						boxCounter;
	int						allGood; //vse dobre meritve
	int						allGoodDMS;
	float					goodPercentage;
	float					badPercentage;
	bool					parametersChanged; //zastavica za zaznavanje spremembe parametrov in toleranc
	std::vector<vector<CProperty*>>	prop;
	std::vector<vector<int>>	dynamicParameters;
	Classifier					classifier;
	

	//za dialog settings 
	QLineEdit*	lineParameter[4][10];
	QCheckBox*	checkParameter[4][10];
	QLabel*		labelParameter[4][10];
	float		setting[4][10];
	int			settingType[4][10];
	QString		settingText[4][10];


	float		tmpSetting[4][10];
	int			tmpSettingType[4][10];
	QString		tmpSettingText[4][10];

	QString		groupBoxName[4];
	QString		tmpGroupBoxName[4];
	//za urejenje dialoga

private:
	vector<QHBoxLayout*> horizontalLayoutParam;
	vector<QLabel*> labelStevilka;
	vector<QLineEdit*> paramEditLine;
	vector<QLineEdit*> editMinTol;
	vector<QLineEdit*> editMaxTol;
	vector<QLineEdit*> editNominal;
	vector<QLineEdit*> editOffset;
	vector<QLineEdit*> editMinTolCond;
	vector<QLineEdit*> editMaxTolCond;
	vector<QLineEdit*> editCorrection;
	vector<QCheckBox*> checkConditional;

	vector<QCheckBox*> checkActive;
	vector<QToolButton*> removeButton;
	QSignalMapper* signalMapper;
	//QWidget *horizontalLayoutWidget;





private:
	Ui::TypeSettings ui2;
	Ui::typesUi ui;

	bool slovensko;

public:
	void Init(QString name, QString filePath);
	void InitSettingsWindow(QString filePath);
	void ReadParameters(QString filePath);
	void ReadTypeSettings();
	void WriteTypeSettings();
	void OnShowDialogSettings(int rights);
	void WriteParameters();
	void WriteParametersBackUp();
	void ReadCounters();
	void OnShowDialog(int rights);


	void WriteCounters();
	void ResetCounters();
	void SetMeasuredValue(float value, int nParam);
	void SetMeasuredValue(int value, int nParam);
	void SetMeasuredValueDMS(float value, int cam, int nParam);
	void SetMeasuredValue(bool value, int nParam);
	int IsGood(int nParam);
	int IsGoodDMS(int nParam);
	int IsGoodDMSCustomTolerance(float tolLow, float tolHigh, float value, int index,float param);
	int AllGood(void);
	int AllGoodDMS(void);
	int IsConditional(void);
	int LowerOrHigher(int nParam); //preverimo ce je meritev visja ali nizja od toleranca zaradi izrisa barv
	int SaveMeasurements(QString path);
	void AddParameter(int index);
	void UpdateCounter(bool good);
	void OnUpdate();

public slots:
	void OnConfirm();
	void OnCancel();
	void OnRemoveParameter(int param);
	void OnAddParameter();
	void OnOpenPlans();

	void OnTypeSettingsCancel();
	void OnTypeSettingsOK();
	int OnAddSetting();
	int OnRemoveSetting();
	int OnChangeGroupName();



	public://za dms spremenljivke tipov
		//float lenght, lenghtMin, lenghtMax, width, widthMin, widthMax, konus, konusMax, konusMin;
		float typeDowelSetting[9];
		QString selectedAdvancedSettings;
		QString selectedColorCameraSettings;
		int dowelWithConus;


};
