﻿// Circle.cpp : implementation file
//

//verzija 5.0

#include "stdafx.h"
#include "CCircle.h"




// CCircle


CCircle::CCircle()
{
	//center.SetPointFloat(0,0);
	radius = 0;

	area = 0.0;
	perimeter = 0.0;

	minRadius = 0;
	maxRadius = 0;
	averageRadius = 0;
	x = 0; //center kroga x
	y = 0; //center kroga y;
}

CCircle::CCircle(CPointFloat center, float radius)
{
	//this->center = center;
	x = center.x;
	y = center.y;
	SetRadius(radius);

	minRadius = 0;
	maxRadius = 0;
	averageRadius = 0;
}

CCircle::CCircle(CPointFloat p1, CPointFloat p2, CPointFloat p3)
{
	SetCircle(p1, p2, p3);
}

CCircle::CCircle(const CCircle& circle)
{
	count = circle.count;
	type = circle.type;
	x = circle.x;
	y = circle.y;
	//center = circle.center;
	radius = circle.radius;
	area = circle.area;
	perimeter = circle.perimeter;
}

CCircle::~CCircle()
{
}




// CCircle message handlers
CCircle CCircle::operator = (CCircle circle)
{
	count = circle.count;
	type = circle.type;
	//center = circle.center;
	x = circle.x;
	y = circle.y;
	radius = circle.radius;
	area = circle.area;
	perimeter = circle.perimeter;

	return *this;
}
bool CCircle::operator == (CCircle circle)
{
	if ((x != circle.x) || (y != circle.y) || (radius != circle.radius))
		return false;

	return true;
}
bool CCircle::operator != (CCircle circle)
{
	if ((x != circle.x) || (y != circle.y) || (radius != circle.radius))
		return true;

	return false;
}



//Martinov SetCircle
void CCircle::SetCircle(CPointFloat p1, CPointFloat p2, CPointFloat p3)
{
	CPointFloat T1((p1 + p2) / 2), T2((p2 + p3) / 2), center;
	CLine daljica1, daljica2;
	daljica1.SetLine(p1, p2);
	daljica2.SetLine(p2, p3);
	CLine sim1, sim2;
	sim1 = daljica1.GetPerpendicular(T1);
	sim2 = daljica2.GetPerpendicular(T2);

	center = sim1.GetIntersectionPoint(sim2);

	x = center.x;	//x in y sta member variables CCircle, ki jih podeduje od CPointFloat in predstavljata center kroga
	y = center.y;

	//SetRadius(sqrt(pow((p1.x - x),2) + pow((p1.y - y),2)));
	SetRadius(p1.GetDistance(x, y));
}

void CCircle::SetCenter(QPoint center)
{
	this->x = center.x();
	this->y = center.y();
}

void CCircle::SetCenter(CPointFloat center)
{
	this->x = center.x;
	this->y = center.y;

}




void CCircle::SetCircle(vector<CPointFloat> points)
{
	int i, k, j, n, imax;
	float sigmaX, sigmaY, q, delta, xTemp, yTemp, temp, rTemp, J, Jprev, Jinner;
	CPointFloat deltaJ, deltaJprev, u, uprev;
	vector<float> d;
	float epsilon;
	epsilon = 0.001;
	sigmaX = 0;
	sigmaY = 0;
	q = 0;
	n = points.size();
	d.resize(n);

	imax = 100; //stevilo korakov

	if (n > 1)
	{
		for (i = 0; i < n - 2; i++)
		{
			for (j = i + 1; j < n - 1; j++)
			{
				for (k = j + 1; k < n; k++)
				{
					delta = ((points[k].x - points[j].x)*(points[j].y - points[i].y)) - ((points[j].x - points[i].x)*(points[k].y - points[j].y));

					if (abs(delta) > epsilon)
					{
						//points are not aligned 
						temp = (((points[k].y - points[j].y) * (pow(points[i].x, 2.0) + pow(points[i].y, 2.0))) +
							((points[i].y - points[k].y) * (pow(points[j].x, 2.0) + pow(points[j].y, 2.0))) +
							((points[j].y - points[i].y) * (pow(points[k].x, 2.0) + pow(points[k].y, 2.0))));
						xTemp = temp / (2 * delta);

						temp = (((points[k].x - points[j].x) * (pow(points[i].x, 2.0) + pow(points[i].y, 2.0))) +
							((points[i].x - points[k].x) * (pow(points[j].x, 2.0) + pow(points[j].y, 2.0))) +
							((points[j].x - points[i].x) * (pow(points[k].x, 2.0) + pow(points[k].y, 2.0))));
						yTemp = temp / (2 * delta);

						int a = pow(100, 2.0);
						sigmaX += xTemp;
						sigmaY += yTemp;
						q++;
					}
				}
			}
		}
		//initial guess for the circle center
		if (q != 0)
		{
			x = abs(sigmaX / q);
			y = abs(sigmaY / q);
		}
		else
			SetCircle(points[0], points[1], points.back());

		//improve center for some definition of best fits against points set using least squares estimator based 
		//on the euclidean distance between points and the circle center
		//optimum circle radius rTemp


		rTemp = 0;
		for (i = 0; i < n; i++)
		{
			d[i] = sqrtf(pow((points[i].x - x), 2.0) + pow((points[i].y - y), 2.0));
			rTemp += d[i];
		}
		rTemp /= n;
		J = 0;
		for (i = 0; i < n; i++)
		{
			J += pow(d[i] - rTemp, 2.0);
		}
		deltaJ.x = 0;
		deltaJ.y = 0;
		for (i = 0; i < n; i++) //gre od 0 preveri tudi na drugih
		{
			deltaJ.x += (x - points[i].x) * (d[i] - rTemp) / d[i];
			deltaJ.y += (y - points[i].y) * (d[i] - rTemp) / d[i];
		}
		deltaJ.x *= 2;
		deltaJ.y *= 2;

		radius = rTemp;
	}


}
	// Set radius
void CCircle::SetRadius(float r)
{
	radius = r;
	ComputeArea();
	ComputePerimeter();
}

// Compute the area
void CCircle::ComputeArea()
{
	area = M_PI * radius * radius;
}


// Compute the perimeter
void CCircle::ComputePerimeter()
{
	perimeter = 2 * M_PI * radius;
}

//    Insert definitions for rest of accessor functions here.
//       ...
CPointFloat CCircle::GetCenter() const
{
	return CPointFloat(x, y);
}

float CCircle::GetX() const
{
	return x;
}

float CCircle::GetY() const
{
	return y;
}

float CCircle::GetRadius() const
{
	return radius;
}

float CCircle::GetArea() const
{
	return area;
}

float CCircle::GetPerimeter() const
{
	return perimeter;
}




void CCircle::Presecisce(CCircle c2, CPointFloat T[2])
{
	CCircle c1 = *this;
	float d, alfa, beta, fi;
	d = sqrt(pow(c1.x - c2.x, 2) + pow(c1.y - c2.y, 2));  //razdalja med centroma
	float a, b, c;  //stranice za kozinusni izrek
	a = d;
	b = c1.radius;
	c = c2.radius;

	//iz kosinusnega izreka izracunamo kot med zveznico center prvega kroga-presečišče ter zveznico med centroma obeh krogov 
	alfa = acos((pow(a, 2) + pow(b, 2) - pow(c, 2)) / (2 * a*b));
	float t;

	beta = atan((c2.y - c1.y) / (c2.x - c1.x));		//polarni kot zveznice med centroma obeh krogov
	if ((c2.x - c1.x) < 0) beta += M_PI;	//dva različna kota imata isti tangen
	fi = alfa + beta;	//polarni kot zveznice center prvega kroga-presečišče
	T[0].SetPointFloat(c1.x + c1.radius*cos(fi), c1.y + c1.radius*sin(fi));
	fi = -alfa + beta;  //še drugo presečišče
	T[1].SetPointFloat(c1.x + c1.radius*cos(fi), c1.y + c1.radius*sin(fi));
}


//Napisal Martin
//Izračuna krožnico skozi vektor točk. Zavrže trojice točk z radijem pod Rmin ali nad Rmax.
bool CCircle::SetCircleNpoints(std::vector<CPointFloat> points, float Rmin, float Rmax)
{

	int i, j, nPoints, nGoodPoints, nCircles;
	int startIndex[3];
	int index[3];

	float xC, yC, R;

	CPointFloat T[3];
	CCircle circleTmp;

	vector<bool> goodPoint(points.size());

	nPoints = points.size();

	xC = 0;
	yC = 0;

	nCircles = 0;

	for (i = 0; i < 3; i++) startIndex[i] = i * (nPoints / 3);

	//center izračunamo kot povprečje središč krožnic skozi 3 točke
	for (j = 0; j < nPoints / 3; j++)
	{
		for (i = 0; i < 3; i++)
		{
			index[i] = startIndex[i] + j;
			T[i] = CPointFloat(points[index[i]]);
		}
		circleTmp.SetCircle(T[0], T[1], T[2]);  	//Izračunamo kroznico skozi točke index[0-2]

		if (circleTmp.radius > 0 && circleTmp.radius >= Rmin && circleTmp.radius <= Rmax)	//Izpustimo premajhne in prevelike radije (nastanejo npr. v primeru, ko so točke preblizu skupaj)
		{
			xC += circleTmp.x;
			yC += circleTmp.y;

			for (i = 0; i < 3; i++) goodPoint[index[i]] = true;
			nCircles++;
		}
		else
		{
			for (i = 0; i < 3; i++) goodPoint[index[i]] = false;
		}

	}

	//radij izračunamo kot srednjo razdaljo točk do centra
	if (nCircles > 0)
	{
		xC /= nCircles;
		yC /= nCircles;
		this->x = xC;
		this->y = yC;

		CPointFloat sredisce;

		sredisce = CPointFloat(xC, yC);

		R = 0;
		nGoodPoints = 0;
		for (i = 0; i < nPoints; i++)
		{
			if (goodPoint[i])
			{
				R += points[i].GetDistance(sredisce);
				nGoodPoints++;
			}
		}

		R /= nGoodPoints;

		this->SetRadius(R);
	}
	else return false;



	return true;

}

//Napisal Martin:
//Za točen izračun krožnice: izvede SetCircleNpoints, odstrani točke ki odstopajo od krožnice za več kot maxRerror, nato ponovi SetCircleNpoints
//nastavi badPointsShare na delež točk, ki odstopajo od krožnice za več kot maxRerror
//stDevRall ter stDevRgood hranita standardno deviacijo radija za vse točke in za dobre točke (napaka R pod maxRerror)
bool CCircle::SetCircleNpointsExact(std::vector<CPointFloat> points, float Rmin, float Rmax, float maxRerror, float* badPointsShare, float* stDevRall, float* stDevRgood)
{
	int i, nBadPoints;
	float approxR, improvedR, distanceFromCenter, Rerror;
	CPointFloat approxCenter, improvedCenter;

	this->SetCircleNpoints(points, Rmin, Rmax);		//Poiščemo približno krožnico (skozi vse točke)

	approxCenter.SetPointFloat(this->x, this->y);
	approxR = this->radius;

	vector<CPointFloat> filteredPoints;


	//filtriramo točke glede na odstopanje od približne krožnice
	for (i = 0; i < points.size(); i++)
	{
		distanceFromCenter = points[i].GetDistance(approxCenter);
		Rerror = abs(distanceFromCenter - approxR);

		if (Rerror < maxRerror)
		{
			filteredPoints.push_back(points[i]);
		}
	}

	if (filteredPoints.size() < 3) return false;	//filtrirane točke morajo biti vsaj 3, da je izračun možen

	this->SetCircleNpoints(filteredPoints, Rmin, Rmax);	//poiščemo izboljšano krožnico (skozi filtrirane točke)

	nBadPoints = 0;
	*stDevRall = 0;
	*stDevRgood = 0;

	improvedCenter.SetPointFloat(this->x, this->y);
	improvedR = this->radius;


	//glede na izboljšano krožnico izračunamo delež slabih točk ter standardno deviacijo radija dobrih ter vseh točk
	for (i = 0; i < points.size(); i++)
	{
		distanceFromCenter = points[i].GetDistance(improvedCenter);
		Rerror = abs(distanceFromCenter - improvedR);

		*stDevRall += pow(Rerror, 2);

		if (Rerror < maxRerror)
		{
			*stDevRgood += pow(Rerror, 2);
		}
		else
		{
			nBadPoints++;
		}
	}

	*stDevRall = sqrt(*stDevRall / points.size());

	*stDevRgood = sqrt(*stDevRgood / (points.size() - nBadPoints));

	*badPointsShare = (float)nBadPoints / points.size();

	return true;
}

void CCircle::BorderIndex(vector<float>& angles, float fiCenter, float fiInterval, int borderIndex[2], bool * includesAngleZero)
{
	int border;

	float fiBorder[2];

	fiBorder[0] = fiCenter - fiInterval / 2;
	fiBorder[1] = fiCenter + fiInterval / 2;

	int N = angles.size();

	if (fiBorder[0] < 0 || fiBorder[1] >= 2 * M_PI) *includesAngleZero = true;
	else *includesAngleZero = false;

	for (border = 0; border < 2; border++)
	{
		if (fiBorder[border] < 0) fiBorder[border] += 2 * M_PI;
		if (fiBorder[border] >= 2 * M_PI) fiBorder[border] -= 2 * M_PI;	//kopija iz CalcIndexMejeEnKotniIzsek projekta 3Dscan

		int firstGreaterOrEqual;  //indeks prvega kota v angles, ki je večji ali enak fiBorder

		//TU Z BISEKCIJO POIŠČEMO PRVI INDEKS, KJER JE POGOJ "P" = angles[indeks] >= fiBorder[border] izpolnjen

			if (angles[0] >= fiBorder[border]) firstGreaterOrEqual = 0;
			else if (!(angles[N - 1] >= fiBorder[border]))
			{
				firstGreaterOrEqual = N;
			}
			else //če smo prišli do tu, vemo da pogoj ni izpolnjen pri indeksu 0, je pa pri indeksu n-1
			{
				int indexConditionFalse = 0;	//doslej največji najden indeks, kjer P ni izpolnjen
				int indexConditionTrue = N - 1;  //doslej najmanjši najden indeks, kjer P je izpolnjen

				int indexCenter = (indexConditionFalse + indexConditionTrue) / 2;

				while (indexCenter != indexConditionFalse && indexCenter != indexConditionTrue)	//ta pogoj bo izpolnjen, dokler bo indexConditionTrue - indexConditionFalse > 1
				{
					if (angles[indexCenter] >= fiBorder[border])
					{
						indexConditionTrue = indexCenter;
					}
					else
					{
						indexConditionFalse = indexCenter;
					}
					indexCenter = (indexConditionFalse + indexConditionTrue) / 2;
				}

				firstGreaterOrEqual = indexConditionTrue;
			}
		

		borderIndex[border] = firstGreaterOrEqual;

	}
}

float CCircle::AvgRsector(vector<float>& distance, int borderIndex[2], bool includesAngleZero)
{
	int N = distance.size();


	int i;

	int nPointsThisSector = 0;
	float avgRsector = 0;

	if (includesAngleZero)
	{
		i = borderIndex[0];

		while (i < N)   //od začetka odseka do kota 0
		{
			avgRsector += distance[i];
			nPointsThisSector++;
			i++;
		}


		i = 0;

		while (i < borderIndex[1])  //od kota 0 do konca odseka
		{
			avgRsector += distance[i];
			nPointsThisSector++;
			i++;
		}

	}
	else
	{
		i = borderIndex[0];


		while (i < borderIndex[1])  //od začetka odseka do konca odseka
		{
			avgRsector += distance[i];
			nPointsThisSector++;
			i++;
		}
	}


	avgRsector /= nPointsThisSector;

	return avgRsector;
}

void CCircle::CircleStDev(vector<float>& angles, vector<CPointFloat>& circlePoints, float fiInterval, float overlap, float Rextreme[2], float fiRextreme[2], float * maxStDevLocal, float * fiMaxStDevLocal, float * maxStDevLocalToGlobal, float * fiMaxStDevLocalToGlobal, float * stDevGlobal, int * minSectorPoints, int * maxSectorPoints)
{
	if (angles.size() != circlePoints.size()) return;

	int N = circlePoints.size();

	CPointFloat center = this->GetCenter();
	float R = this->GetRadius();

	//najprej naračunamo razdalje točk od centra ter nekaj enostavnih izhodnih spremenljivk
	vector<float> distance(N);

	Rextreme[0] = HUGE_VALF;
	Rextreme[1] = -HUGE_VALF;

	fiRextreme[0] = -1;
	fiRextreme[1] = -1;

	*stDevGlobal = 0;

	float dR;
	float dRlocal;

	for (int i = 0; i < N; i++)
	{
		distance[i] = circlePoints[i].GetDistance(center);

		if (distance[i] < Rextreme[0])
		{
			Rextreme[0] = distance[i];
			fiRextreme[0] = angles[i];
		}
		if (distance[i] > Rextreme[1])
		{
			Rextreme[1] = distance[i];
			fiRextreme[1] = angles[i];
		}

		dR = distance[i] - R;

		*stDevGlobal += dR * dR;
	}
	*stDevGlobal /= N;
	*stDevGlobal = sqrt(*stDevGlobal);



	//sedaj naredimo še izračune po kotnih izsekih

	*maxStDevLocal = -HUGE_VALF;
	*maxStDevLocalToGlobal = -HUGE_VALF;
	*fiMaxStDevLocal = -1;
	*fiMaxStDevLocalToGlobal = -1;

	*minSectorPoints = 100000;  //to je veliko več kot je obseg kateregakoli kroga na naših slikah
	*maxSectorPoints = 0;

	int indexFiCenter = 0;
	float fiCenter = 0;

	while (fiCenter < 2 * M_PI)
	{
		int borderIndex[2];
		bool includesAngleZero;

		BorderIndex(angles, fiCenter, fiInterval, borderIndex, &includesAngleZero);

		float avgRsector = AvgRsector(distance, borderIndex, includesAngleZero);

		int i;
		int nPointsThisSector = 0;
		float stDevLocal = 0;
		float stDevLocalToGlobal = 0;

		if (includesAngleZero)
		{

			i = borderIndex[0];

			while (i < N)  //od začetka odseka do kota 0
			{
				//avgRsector += distance[i];
				dR = distance[i] - R;
				dRlocal = distance[i] - avgRsector;
				stDevLocal += dRlocal * dRlocal;
				stDevLocalToGlobal += dR * dR;

				nPointsThisSector++;
				i++;
			}


			i = 0;

			while (i < borderIndex[1])  //od kota 0 do konca odseka
			{
				dR = distance[i] - R;
				dRlocal = distance[i] - avgRsector;
				stDevLocal += dRlocal * dRlocal;
				stDevLocalToGlobal += dR * dR;

				nPointsThisSector++;
				i++;
			}
		}
		else
		{
			i = borderIndex[0];


			while (i < borderIndex[1])  //od začetka odseka do konca odseka
			{
				dR = distance[i] - R;
				dRlocal = distance[i] - avgRsector;
				stDevLocal += dRlocal * dRlocal;
				stDevLocalToGlobal += dR * dR;

				nPointsThisSector++;
				i++;
			}
		}





		if (nPointsThisSector > 0)
		{
			stDevLocal /= nPointsThisSector;
			stDevLocal = sqrt(stDevLocal);

			stDevLocalToGlobal /= nPointsThisSector;
			stDevLocalToGlobal = sqrt(stDevLocalToGlobal);

			if (stDevLocal > *maxStDevLocal)
			{
				*maxStDevLocal = stDevLocal;
				*fiMaxStDevLocal = fiCenter;
			}

			if (stDevLocalToGlobal > *maxStDevLocalToGlobal)
			{
				*maxStDevLocalToGlobal = stDevLocalToGlobal;
				*fiMaxStDevLocalToGlobal = fiCenter;
			}


		}

		if (nPointsThisSector < *minSectorPoints)
		{
			*minSectorPoints = nPointsThisSector;
		}
		if (nPointsThisSector > *maxSectorPoints)
		{
			*maxSectorPoints = nPointsThisSector;
		}

		//na koncu
		fiCenter += fiInterval * overlap;
	}
}

QGraphicsEllipseItem* CCircle::DrawCircle(QPen pen, QBrush brush, int crossSize)
{
	QRect rect(x, y, radius*2, radius*2);
	rect.moveCenter(QPoint(x, y));
	const int DIAMETER = 100;
	QGraphicsEllipseItem* circle = new QGraphicsEllipseItem(rect);
	
	//circle->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
	circle->setBrush(brush);
	circle->setPen(pen);
	
	QGraphicsLineItem* line = new QGraphicsLineItem();
	QGraphicsLineItem* line2 = new QGraphicsLineItem();
	line->setLine(x - crossSize / 2, y, x + crossSize / 2, y);
	line2->setLine(x, y - crossSize / 2, x, y + crossSize / 2);
	line2->setPen(QPen(pen));

	line->setPen(QPen(pen));
	line->setParentItem(circle);
	line2->setParentItem(circle);
	circle->setZValue(1);
	return circle;
}


