#pragma once
#include "stdafx.h"
#include "CRectRotated.h"



CRectRotated::CRectRotated()
{

	setLeft(0);
	setRight(0);
	setBottom(0);
	setTop(0);
	id = 0;
}

CRectRotated::CRectRotated(int left, int top, int right, int bottom)
{
	id = 0;
	SetRectangle(left, top, right, bottom);
}

CRectRotated::CRectRotated(QRect rect)
{
	SetRectangle(rect);
}

CRectRotated::CRectRotated(const CRectRotated& rotatedRect)
{
	id = rotatedRect.id;
	setLeft(rotatedRect.left());
	setRight(rotatedRect.right());
	setBottom(rotatedRect.bottom());
	setTop(rotatedRect.top());

	polygonPoints.resize(rotatedRect.polygonPoints.size());		//Popravil Martin

	for (int i = 0; i < rotatedRect.polygonPoints.size(); i++)
		polygonPoints[i] = rotatedRect.polygonPoints[i];


} // Copy constructor

CRectRotated::~CRectRotated()
{
}


CRectRotated CRectRotated::operator = (CRectRotated rotatedRect)
{
	id = rotatedRect.id;
	setLeft(rotatedRect.left());
	setRight(rotatedRect.right());
	setBottom(rotatedRect.bottom());
	setTop(rotatedRect.top());

	polygonPoints.resize(rotatedRect.polygonPoints.size());		//Popravil Martin


	for (int i = 0; i < rotatedRect.polygonPoints.size(); i++)
		polygonPoints[i] = rotatedRect.polygonPoints[i];


	return *this;
}
bool CRectRotated::operator == (CRectRotated rotatedRect)
{

	if (polygonPoints.size() != rotatedRect.polygonPoints.size())
		return false;

	if ((left() != rotatedRect.left()) || (right() != rotatedRect.right()) || (top() != rotatedRect.top()) || (bottom() != rotatedRect.bottom()))
		return false;

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		if ((polygonPoints[i].x != rotatedRect.polygonPoints[i].x) || (polygonPoints[i].y != rotatedRect.polygonPoints[i].y))
			return false;
	}

	return true;
}

bool CRectRotated::operator != (CRectRotated rotatedRect)
{
	if (polygonPoints.size() != rotatedRect.polygonPoints.size())
		return true;

	if ((left() != rotatedRect.left()) || (right() != rotatedRect.right()) || (top() != rotatedRect.top()) || (bottom() != rotatedRect.bottom()))
		return true;

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		if ((polygonPoints[i].x != rotatedRect.polygonPoints[i].x) || (polygonPoints[i].y != rotatedRect.polygonPoints[i].y))
			return true;
	}

	return false;
}

CRectRotated CRectRotated::operator+= (int val)
{
	setLeft(left() + val);
	setRight(right() + val);
	setTop(top() + val);
	setBottom(bottom() + val);

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		polygonPoints[i].x += val;
		polygonPoints[i].y += val;
	}

	return *this;
}

CRectRotated CRectRotated::operator+= (CPointFloat move)
{

	setLeft(left() + move.x);
	setRight(right() + move.x);
	setTop(top() + move.y);
	setBottom(bottom() + move.y);


	for (int i = 0; i < polygonPoints.size(); i++)
	{
		polygonPoints[i].x += move.x;
		polygonPoints[i].y += move.y;
	}

	return *this;
}

CRectRotated CRectRotated::operator-= (int val)		//opomba: mislim, da bi kodo lahko skraj�ali s klicem operator+= (-val)
{


	setLeft(left() - val);
	setRight(right() - val);
	setTop(top() - val);
	setBottom(bottom() - val);

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		polygonPoints[i].x -= val;
		polygonPoints[i].y -= val;
	}

	return *this;
}

CRectRotated CRectRotated::operator*= (int val)
{

	setLeft(left() * val);
	setRight(right() * val);
	setTop(top() * val);
	setBottom(bottom() *val);

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		polygonPoints[i].x *= val;
		polygonPoints[i].y *= val;
	}

	return *this;
}

CRectRotated CRectRotated::operator/= (int val)
{

	setLeft(left() / val);
	setRight(right() / val);
	setTop(top() / val);
	setBottom(bottom() / val);

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		polygonPoints[i].x /= val;
		polygonPoints[i].y /= val;
	}

	return *this;
}

CRectRotated CRectRotated::operator+ (int val)
{
	CRectRotated rect;
	rect.setLeft(left() + val);
	rect.setRight(right() + val);
	rect.setTop(top() + val);
	rect.setBottom(bottom() + val);

	rect.polygonPoints.resize(polygonPoints.size());	//popravil Martin

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		rect.polygonPoints[i].x = polygonPoints[i].x + val;
		rect.polygonPoints[i].y = polygonPoints[i].y + val;
	}

	return rect;
}

CRectRotated CRectRotated::operator+ (CPointFloat move)
{
	CRectRotated rect;
	rect.setLeft(left() + (int)move.x);
	rect.setRight(right() + (int)move.x);
	rect.setTop(top() + (int)move.y);
	rect.setBottom(bottom() + (int)move.y);

	rect.polygonPoints.resize(polygonPoints.size());

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		rect.polygonPoints[i].x = polygonPoints[i].x + (int)move.x;
		rect.polygonPoints[i].y = polygonPoints[i].y + (int)move.y;
	}

	return rect;
}

CRectRotated CRectRotated::operator- (int val)	//opomba: mislim, da bi kodo lahko skraj�ali s klicem operator+= (-val)
{


	
	CRectRotated rect;
	rect.setLeft( left() - val);
	rect.setRight(right() - val);
	rect.setTop(top() - val);
	rect.setBottom(left() - val);


	rect.polygonPoints.resize(polygonPoints.size());	//popravil Martin

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		rect.polygonPoints[i].x = polygonPoints[i].x - val;
		rect.polygonPoints[i].y = polygonPoints[i].y - val;
	}

	return rect;
}

CRectRotated CRectRotated::operator* (int val)
{
	CRectRotated rect;
	rect.setLeft(left() * val);
	rect.setRight(right() * val);
	rect.setTop(top() * val);
	rect.setBottom(left() * val);

	rect.polygonPoints.resize(polygonPoints.size());	//popravil Martin

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		rect.polygonPoints[i].x = polygonPoints[i].x * val;
		rect.polygonPoints[i].y = polygonPoints[i].y * val;
	}

	return rect;
}

CRectRotated CRectRotated::operator/ (int val) const   //Opomba Martin: const mora biti tu, da lahko uporabljamo operator / tudi na konstantih objektih
{
	CRectRotated rect;
	rect.setLeft(left() / val);
	rect.setRight(right() / val);
	rect.setTop (top() / val);
	rect.setBottom(left() / val);

	rect.polygonPoints.resize(polygonPoints.size());	//popravil Martin

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		rect.polygonPoints[i].x = polygonPoints[i].x / val;
		rect.polygonPoints[i].y = polygonPoints[i].y / val;
	}

	return rect;
}

CRectRotated CRectRotated::operator/ (float val) const	 //Opomba Martin: const mora biti tu, da lahko uporabljamo operator / tudi na konstantih objektih
{
	CRectRotated rect;
	rect.setLeft (left() / val);
	rect.setRight(right() / val);
	rect.setTop(top() / val);
	rect.setBottom (left() / val);

	rect.polygonPoints.resize(polygonPoints.size());

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		rect.polygonPoints[i].x = polygonPoints[i].x / val;
		rect.polygonPoints[i].y = polygonPoints[i].y / val;
	}

	return rect;
}

void CRectRotated::SetRectangle(int left, int top, int right, int bottom)
{
	setLeft(left);
	setRight(right);
	setTop(top);
	setBottom(bottom);

}

void CRectRotated::SetRectangle(QRect rect)
{
	setLeft(rect.left());
	setTop(rect.top());
	
	setRight(rect.right());
	setBottom(rect.bottom());
}

void CRectRotated::SetID(int id)
{
	this->id = id;
}

int CRectRotated::GetID()
{
	return id;
}

QRect CRectRotated::GetCRect(void)
{
	QRect c;
	c.setLeft(left());
	c.setTop(top());
	c.setRight(right());
	c.setBottom(bottom());

	return c;
}
void CRectRotated::SetRectangle()
{
	//SetRectangle(5000, 5000, 0, 0);
	SetRectangle(5000, 5000, -5000, -5000);	//Popravil Martin

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		if (left() > polygonPoints[i].x)
			setLeft((int)polygonPoints[i].x);

		if (top() > polygonPoints[i].y)
			setTop((int)polygonPoints[i].y);

		if (right() < polygonPoints[i].x)
			setRight((int)polygonPoints[i].x);

		if (bottom() < polygonPoints[i].y)
			setBottom((int)polygonPoints[i].y);
	}
}

int CRectRotated::AddPoint(CPointFloat point)
{
	polygonPoints.push_back(point);

	return polygonPoints.size();
}

int CRectRotated::AddPoint(float x, float y)
{

	polygonPoints.push_back(CPointFloat(x, y));

	return polygonPoints.size();
}

int CRectRotated::AddPoint(int x, int y)
{
	polygonPoints.push_back(CPointFloat((float)x, (float)y));

	return polygonPoints.size();
}

void CRectRotated::RotateRectangle(CPointFloat basePoint, double alfaRadians)
{
	//rotira pravokotnik, nove tocke shrani v tocke poligona
	CPointFloat pTmp;

	//rotira vsako tocko pravokotnika 
	pTmp.SetPointFloat(left(), top());
	AddPoint(pTmp.GetRotatedPoint(basePoint, alfaRadians));

	pTmp.SetPointFloat(left(), bottom());
	AddPoint(pTmp.GetRotatedPoint(basePoint, alfaRadians));

	pTmp.SetPointFloat(right(), bottom());
	AddPoint(pTmp.GetRotatedPoint(basePoint, alfaRadians));

	pTmp.SetPointFloat(right(), top());
	AddPoint(pTmp.GetRotatedPoint(basePoint, alfaRadians));

	//glede na nove tocke poligona izracuna nov kvadrat
	SetRectangle();
}

void CRectRotated::RotatePolygon(CPointFloat basePoint, double alfaRadians)
{
	//rotira tocke polygona 

	for (int i = 0; i < polygonPoints.size(); i++)
	{
		//rotira vsako tocko poligona 
		AddPoint(polygonPoints[i].GetRotatedPoint(basePoint, alfaRadians));
	}

	//glede na nove tocke poligona izracuna nov kvadrat
	SetRectangle();
}

void CRectRotated::RemoveLastPoint()
{
	if (polygonPoints.size() > 0)
		polygonPoints.pop_back();
}

void CRectRotated::ClearPolygon()
{
	if (polygonPoints.size() > 0)
		polygonPoints.clear();
}



BOOLEAN CRectRotated::InPolygon(CPointFloat point)
{
	int i, size, lastPoint;
	bool isInside;

	isInside = false;

	size = polygonPoints.size();

	if (size != 0)
	{
		lastPoint = size - 1;

		for (i = 0; i < size; i++)
		{

			if (((polygonPoints[i].y < point.y) && (polygonPoints[lastPoint].y >= point.y) ||
				(polygonPoints[lastPoint].y < point.y) && (polygonPoints[i].y >= point.y)) &&
				((polygonPoints[i].x <= point.x) || (polygonPoints[lastPoint].x <= point.x)))
			{

				//ta pogoj testira, �e x koordinata prese�i��a daljice lastPoint-i seka poltrak point-(inf, p.y), ob predpostavki, da seka premico, na kateri poltrak le�i
				if (polygonPoints[i].x + (point.y - polygonPoints[i].y) / (polygonPoints[lastPoint].y - polygonPoints[i].y) * (polygonPoints[lastPoint].x - polygonPoints[i].x) < point.x)
				{
					if (isInside == false)
						isInside = true;
					else
						isInside = false;
				}
			}
			lastPoint = i;
		}
	}
	else
	{
		return 0;

	}
	return isInside;
}



BOOLEAN CRectRotated::InPolygon(float x, float y)
{
	CPointFloat temp;

	temp.SetPointFloat(x, y);

	return InPolygon(temp);
}

QGraphicsPolygonItem* CRectRotated::DrawPolygonLines(QPen pen,QBrush brush)
{
	int i, j;
	QGraphicsPolygonItem* polygonItem = new QGraphicsPolygonItem();
	/* mode
	PS_SOLID = 0
	*/polygonItem->setPen(pen);
	polygonItem->setBrush(brush);

	QPolygon polygon;

	//QPen pen(mode, size, color);
	/*polygon.setPoint(0, 10, 10);
	polygon.setPoint(1, 100, 10);
	polygon.setPoint(2, 100, 100);
	polygon.setPoint(3, 10, 100);*/

	if (polygonPoints.size() > 1)
	{
		//polygonPoints[0].DrawDot(pDC, RGB(255, 0, 0), 5);
		for (i = 0; i < polygonPoints.size(); i++)
		{
			polygon.putPoints(i, 1, polygonPoints[i].x, polygonPoints[i].y);
		

			
		}
		
	}

	polygonItem->setPolygon(polygon);
	polygonItem->setZValue(1);
	return polygonItem;

}