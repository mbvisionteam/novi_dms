#include "stdafx.h"
#include "MBError.h"

void MBError::operator=(ErrorCode code)
{
	this->_code = code;
}

void MBError::operator=(int code)
{
	this->_code = static_cast<ErrorCode>(code);
}

bool MBError::operator==(ErrorCode code)
{
	return (this->_code == code);
}

bool MBError::operator==(int code)
{
	return (this->_code == static_cast<ErrorCode>(code));
}

bool MBError::operator!=(ErrorCode code)
{
	return (this->_code != code);
}

bool MBError::operator!=(int code)
{
	return (this->_code != static_cast<ErrorCode>(code));
}

bool MBError::IsError()
{
	return _code != ErrorCode::MB_NO_ERROR;
}

const char * MBError::String()
{
	switch (this->_code)
	{
	case ErrorCode::MB_NO_ERROR:
		return "";
		break;
	case ErrorCode::MB_NO_IMAGE:
		return "No image to process";
		break;
	case ErrorCode::MB_IMG_PROC_ERROR:
		return "Error processing the image";
		break;
	case ErrorCode::MB_DEFAULT_ERROR:
		return "Default error";
	default:
		return "Unknown error code";
		break;
	}
}
