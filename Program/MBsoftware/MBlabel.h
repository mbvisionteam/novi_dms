
#include <QWidget>
#include "stdafx.h"


class MBlabel : public QLabel
{
	Q_OBJECT
public:

	MBlabel();
	int index;
	int isGoodLabel;


protected:

	void enterEvent(QEvent *ev) override;
	void leaveEvent(QEvent *ev) override;
	void mousePressEvent(QMouseEvent *e) override;
public:
	void SetIsGoodLabel(int isGood);

signals: 
	void clicked(int index);

};

