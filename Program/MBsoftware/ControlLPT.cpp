#include "stdafx.h"
#include "ControlLPT.h"


int		ControlLPT::objectCount = 0;
ControlLPT::ControlLPT()
{
}
ControlLPT::ControlLPT(short portAddress, bool controlAsInput,QString referencePath)
{
	ui.setupUi(this);
	address = portAddress;
	this->referencePath = referencePath;
	id = objectCount;
	IsDriverOpen();
	portDataValue = 0;
	portStatusValue = 0;
	portControlValue = 0;
	ReadData();
	controlByteInput = controlAsInput;

	//Nastavimo vrednosti bitov
	//InitAddresses();
	InitValues();
	CreateDisplay();
	Out32(address, 0);//Vsi izhodni biti na nizek nivo
	if(controlByteInput == true)
	Out32(address + 2, 0x4);//Vsi kontrolni biti na nizek nivo
	else
		Out32(address + 2, 0xB);//Vsi kontrolni biti na nizek nivo

	//SetControlByte(0x4);
	objectCount++;
}

void ControlLPT::closeEvent(QCloseEvent * event)
{
	viewTimer->stop();
	//disconnect(poModelOutputs, SIGNAL(itemChanged(QStandardItem*)),	0, 0);

	//disconnect(poModelControl, SIGNAL(itemChanged(QStandardItem*)),	0, 0);
}

void ControlLPT::SetControlByteAsInput(bool isInput)
{


}


void ControlLPT::OnShowDialog()
{
	setWindowModality(Qt::NonModal);
	show();
	viewTimer->start(100);
	
}

void ControlLPT::InitAddresses()
{
	UCHAR bitAddress = 0x01;

	for (int i = 0; i < 8; i++)
	{
		data[i].address = bitAddress;

		if (i > 2)//Statusni biti zavzamejo zgornjih 5 naslovov
		{
			status[i - 3].address = bitAddress;
		}
			
		if (i < 4)//Kontrolni biti zavzamejo spodnje 4 naslove
		{
			control[i].address = bitAddress;
		}

		bitAddress = bitAddress << 1;
	}
}

int ControlLPT::ReadData(void)
{
	QString filePath;
	QStringList values;
	QVariant var;

	//filePath = referencePath + QString("/%1/DMS/cam_settings.ini").arg(REF_FOLDER_NAME);
	filePath = referencePath + QString("/%1/Signali/ControlLPT%2.ini").arg(REF_FOLDER_NAME).arg(id);
	//filePath = QApplication::applicationDirPath() + QString("/../%1/Signali/LptControl%2.ini").arg(REF_FOLDER_NAME).arg(id);
	QSettings settings(filePath, QSettings::IniFormat);

	uchar adresses[8] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

	//inputs
	for (int i = 0; i < 5; i++)
	{
		values = settings.value(QString("Status%1").arg(i)).toStringList();

		if (values.size() > 1)
		{
			status[i].SetSignal(values[0], adresses[i + 3], values[1].toInt());
			values.clear();
		}
		else
		{
			status[i].SetSignal(QString("S%1").arg(i), adresses[i + 3], 1);
			values.append(QString("Status%1").arg(i));
			values.append(QString("0"));
			settings.setValue(QString("Status%1").arg(i), values);
			settings.sync();
			values.clear();
		}
	}

	//outputs
	for (int i = 0; i < 8; i++)
	{
		values = settings.value(QString("Data%1").arg(i)).toStringList();

		if (values.size() > 1)
		{
			data[i].SetSignal(values[0], adresses[i], values[1].toInt());
			values.clear();
		}
		else
		{
			data[i].SetSignal(QString("D%1").arg(i), adresses[i], 0);
			values.append(QString("Data%1").arg(i));
			values.append(QString("0"));
			settings.setValue(QString("Data%1").arg(i), values);
			settings.sync();
			values.clear();
		}
	}

	//control
	for (int i = 0; i < 4; i++)
	{
		values = settings.value(QString("Control%1").arg(i)).toStringList();

		if (values.size() > 1)
		{
			control[i].SetSignal(values[0], adresses[i], values[1].toInt());
			values.clear();
		}
		else
		{
			control[i].SetSignal(QString("C%1").arg(i), adresses[i], 0);
			values.append(QString("Control%1").arg(i));
			values.append(QString("0"));
			settings.setValue(QString("Control%1").arg(i), values);
			settings.sync();
			values.clear();
		}
	}

	//portAddr = settings.value("BasePort").toInt();

	return 0;
}


void ControlLPT::InitValues()
{
	UCHAR bitValue = 0x01;

	//Preberemo statusne bite
	GetStatusData();

 	for (int i = 0; i < 8; i++)
	{
		data[i].value = 0;

		if (i > 2)//Statusni biti zavzamejo zgornjih 5 naslovov
		{
			if ((portStatusValue & bitValue) > 0)
				status[i - 3].value = 1;
			else
				status[i - 3].value = 0;

			bitValue = bitValue << 1;
		}

		if (i < 4)//Kontrolni biti zavzamejo spodnje 4 naslove
		{
			if(i == 2)
				control[i].value = 0;
			else
				control[i].value = 1;
		}
	}
}

bool ControlLPT::IsDriverOpen()
{
	bool driverStatus = IsInpOutDriverOpen();

	return driverStatus;
}
unsigned char ControlLPT::GetPortControlValue(void)
{
	lockReadWrite.lockForWrite();
	portControlValue = Inp32(address + 2);
	lockReadWrite.unlock();
	return portControlValue;
}

void ControlLPT::WriteBytes(short data)
{
	lockReadWrite.lockForWrite();
	Out32(address, data);
	lockReadWrite.unlock();
}

short ControlLPT::ReadBytes(short address)
{
	short readData;

	lockReadWrite.lockForRead();
	readData = Inp32(address);
	lockReadWrite.unlock();

	return readData;
}

void ControlLPT::CreateDisplay(void)
{
	int x, y, width, height, xOffset, port;
	//layout = new QHBoxLayout;
	x = 10;
	y = 10;
	width = 360;
	height = 200;
	xOffset = 400;
	QSpacerItem *spacer[3];
	//Podatkovni biti
	setWindowTitle(QString("LPT: %1").arg(id));
	for (int i = 0; i < 3; i++)
	{
		spacer[i] = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	}

	ui.groupData->setTitle("Data register");

	for (int i = 0; i < 8; i++)
	{
		checkData[i] = new QCheckBox();
		checkData[i]->setText(data[i].name);
		ui.layoutData->addWidget(checkData[i]);
		connect(checkData[i], &QPushButton::clicked,[=](int index) { OnClickedData(i); });
	}
	ui.layoutData->addSpacerItem(spacer[0]);


	ui.groupStatus->setTitle("Status register");

	for (int i = 0; i < 5; i++)
	{
		checkStatus[i] = new QCheckBox();
		checkStatus[i]->setText(status[i].name);
		//checkStatus[i]->setCheckState(Qt::Checked);
		//checkStatus[i]->setCheckable(false);
		ui.layoutStatus->addWidget(checkStatus[i]);

	}
	ui.layoutStatus->addSpacerItem(spacer[1]);
	

	ui.groupControl->setTitle("Control register");

	for (int i = 0; i < 4; i++)
	{
		checkControl[i] = new QCheckBox();
		checkControl[i]->setText(control[i].name);
	
		ui.layoutControl->addWidget(checkControl[i]);

		if (controlByteInput == true)
			//checkControl[i]->setCheckable(true);
		{

		}
		else
		{
			connect(checkControl[i], &QPushButton::clicked, [=](int index) { OnClickedControl(i); });
			checkControl[i]->setCheckable(true);
		}


	}
	ui.layoutControl->addSpacerItem(spacer[1]);

	
	viewTimer = new QTimer(this);     //Inicializacija
	connect(viewTimer, SIGNAL(timeout()), this, SLOT(TimerViewSignals()));
	//viewTimer->start(100);
}


void ControlLPT::SetDataByte(UCHAR bits)
{
	portDataValue = bits;
	Out32(address, portDataValue);

	SetOutputSignals();
}

void ControlLPT::SetDataByte(UCHAR bits, bool value)
{

	if (value)
		portDataValue = portDataValue | bits;
	else
		portDataValue = portDataValue & (~bits);

	Out32(address, portDataValue);

	SetOutputSignals();
}

void ControlLPT::SetOutputValue(int outputNumber, bool value)
{
	SetDataByte(data[outputNumber].address, value);
}


void ControlLPT::SetControlByte(int contolBit, bool value)
{
	//contolBit ... control but number (1-4)
	lockReadWrite.lockForWrite();
	if (value)
	{
		switch (contolBit)
		{
		case 1:
			portControlValue = portControlValue & 0xfe;
			break;

		case 2:
			portControlValue = portControlValue & 0xfd;
			break;

		case 3:
			portControlValue = portControlValue | 0x04;
			break;

		case 4:
			portControlValue = portControlValue & 0xf7;
			break;

		default:
			break;
		}
	}
	else
	{
		switch (contolBit)
		{
		case 1:
			portControlValue = portControlValue | 0x01;
			break;

		case 2:
			portControlValue = portControlValue | 0x02;
			break;

		case 3:
			portControlValue = portControlValue & 0xfb;
			break;

		case 4:
			portControlValue = portControlValue | 0x08;
			break;

		default:

			break;

		}
	}
	Out32(address + 2, portControlValue);
	lockReadWrite.unlock();
	//GetControlData();
	//UCHAR convVAlue = GetControlDataForDisplay();
	UCHAR data;
	data = ConvertControl();
	SetControlSignals(data);


}

void ControlLPT::SetControlByte(char bits)
{
	Out32(address + 2, bits);
}

void ControlLPT::SetControlByteAllON(void)
{
	portControlValue = Inp32(address + 2);
	portControlValue = portControlValue & 0xf0;
	portControlValue = portControlValue | 0x04; //samo 3 bit 1, ker ni invertiran	

	Out32(address + 2, portControlValue);
	UCHAR data;
	data = ConvertControl();
	SetControlSignals(data);
}

void ControlLPT::SetControlByteAllOFF(void)
{
	portControlValue = Inp32(address + 2);

	portControlValue = portControlValue & 0xf0;
	portControlValue = portControlValue | 0x0b; //samo 3 bit 1, ker ni invertiran	

	Out32(address + 2, portControlValue);
	UCHAR data;
	data = ConvertControl();
	SetControlSignals(data);

	//SetControlSignals(portControlValue);
}






void ControlLPT::SetOutputSignals()
{
	for (int i = 0; i < 8; ++i)
	{
		if ((portDataValue & data[i].address) == data[i].address)
		{
			data[i].prevValue = data[i].value;
			data[i].value = 1;
		}
		else
		{
			data[i].prevValue = data[i].value;
			data[i].value = 0;
		}
	}
}

void ControlLPT::SetInputSignals()
{
	for (int i = 0; i < 5; ++i)
	{
		if ((portStatusValue & status[i].address) == status[i].address)
		{
			status[i].prevValue = status[i].value;
			status[i].value = 0;
		}
		else
		{
			status[i].prevValue = status[i].value;
			status[i].value = 1;
		}
	}
}

void ControlLPT::SetControlSignals(UCHAR data)
{
	for (int i = 0; i < 4; ++i)
	{
		if ((data & control[i].address) == control[i].address)//Spremenimo vrednost kontrolnega bita
		{
			control[i].prevValue = control[i].value;
			control[i].value = 0;
		}
		else
		{
			control[i].prevValue = control[i].value;
			control[i].value = 1;
		}
	}
}

void ControlLPT::GetOutputData()
{
	lockReadWrite.lockForRead();
	portDataValue = ReadBytes(address);
	lockReadWrite.unlock();
}

void ControlLPT::GetStatusData()
{
	lockReadWrite.lockForRead();
	portStatusValue = ReadBytes(address + 1);

	if ((portStatusValue & 0x80) == 0x80)
		portStatusValue = portStatusValue & 0x7F;
	else
		portStatusValue = portStatusValue | 0x80;

	//Spodnjih 3-eh bitov ne uporabljamo
	portStatusValue = portStatusValue >> 3;
	SetInputSignals();
	lockReadWrite.unlock();



}
unsigned char ControlLPT::GetStatusValue(void)
{
	portStatusValue = Inp32(address + 1);

	//obrne invertiran bit
	if ((portStatusValue & 0x80) == 0x80)
		portStatusValue = portStatusValue & 0x7F;
	else
		portStatusValue = portStatusValue | 0x80;

	SetInputSignals();

	return portStatusValue;
}

short ControlLPT::GetControlData()
{
	unsigned short valueConst, valueNeg, convValue;
	short constBits = 0x04;
	short negBits = 0x0B;

	//lockReadWrite.lockForRead();
	portControlValue = ReadBytes(address + 2);
	valueConst = portControlValue & constBits;//�etrti bit ostaja nespremenjen
	//Ostali trije os negirani
	valueNeg = portControlValue & negBits;
	valueNeg = ~valueNeg & 0xB;
	convValue = valueConst |  valueNeg;

	//lockReadWrite.unlock();
	SetControlSignals(convValue);
	return convValue;
}
short ControlLPT::ConvertControl()
{
	unsigned short valueConst, valueNeg, convValue;
	short constBits = 0x04;
	short negBits = 0x0B;

	valueConst = portControlValue & constBits;//�etrti bit ostaja nespremenjen
	//Ostali trije os negirani
	valueNeg = portControlValue & negBits;
	valueNeg = ~valueNeg & 0xB;
	convValue = valueConst | valueNeg;

	return convValue;
}

short ControlLPT::PrepareDataBytes(UCHAR bitAddress, bool bitValue)
{
	UCHAR newDataValue;
	lockReadWrite.lockForRead();
	//Spremenimo stanje bitov na podatkovnih bitih
	if (bitValue)
		newDataValue = portDataValue | bitAddress;
	else
		newDataValue = portDataValue & (~bitAddress);

	lockReadWrite.unlock();
	
	return newDataValue;
}

short ControlLPT::PrepareControlBytes(UCHAR bitAddress, bool bitValue)
{
	UCHAR newControlValue;
	lockReadWrite.lockForRead();

	if (bitValue)
	{
		switch (bitAddress)
		{
		case 1:
			newControlValue = portControlValue & 0xfe;//Invertiran
			break;

		case 2:
			newControlValue = portControlValue & 0xfd;//Invertiran
			break;

		case 4:
			newControlValue = portControlValue | 0x04;
			break;

		case 8:
			newControlValue = portControlValue & 0xf7;//Invertiran
			break;

		default:
			break;
		}
	}
	else
	{
		switch (bitAddress)
		{
		case 1:
			newControlValue = portControlValue | 0x01;//Invertiran
			break;

		case 2:
			newControlValue = portControlValue | 0x02;//Invertiran
			break;

		case 4:
			newControlValue = portControlValue & 0xfb;
			break;

		case 8:
			newControlValue = portControlValue | 0x08;//Invertiran
			break;

		default:

			break;

		}
	}
	lockReadWrite.unlock();

	return newControlValue;
}



void ControlLPT::OnClickedData(int index)
{
	bool state;

	state = checkData[index]->checkState();
	SetOutputValue(index, state);
	state = state;
}

void ControlLPT::OnClickedControl(int index)
{
	bool state;

	state = checkControl[index]->checkState();
	SetControlByte(index+1, state);
	state = state;
}

void ControlLPT::TimerViewSignals()
{

	UCHAR bitValue = 0x01;

	//Preberemo statusne bite

	GetStatusValue();

	for (int i = 0; i < 5; i++)
	{
		//status[i].value = portStatusValue & bitValue;
		//bitValue = bitValue << 1;

		if (status[i].value != 0)
		{
			checkStatus[i]->setCheckState(Qt::Checked);
		}
		else
		{
			checkStatus[i]->setCheckState(Qt::Unchecked);
		}
	}
	if (controlByteInput == true)
		GetControlData();
	for (int i = 0; i < 4; i++)
	{
		//status[i].value = portStatusValue & bitValue;
		//bitValue = bitValue << 1;

		if (control[i].value != 0)
		{
			checkControl[i]->setCheckState(Qt::Checked);
		}
		else
		{
			checkControl[i]->setCheckState(Qt::Unchecked);
		}
	}

		


	for (int i = 0; i < 8; i++)
	{

		if (data[i].value != 0)
		{
			checkData[i]->setCheckState(Qt::Checked);
		}
		else
		{
			checkData[i]->setCheckState(Qt::Unchecked);
		}

	}
	
	//}
}

ControlLPT::~ControlLPT()
{
	for (int i = 0; i < 8; i++)
	{
		delete checkData[i];
	}


	for (int i = 0; i < 5; i++)
	{
		delete checkStatus[i];
	}
	for (int i = 0; i < 4; i++)
	{
		delete checkControl[i];
	}

	for (int i = 0; i < 3; i++)
	{
		//delete groupBox[i];
	}
	delete viewTimer;

//delete    layout;

}