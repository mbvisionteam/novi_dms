#include "stdafx.h"
#include "DowelSettings.h"

DowelSettings::DowelSettings(QString referencePath)
{

	ui.setupUi(this);
	this->referencePath = referencePath;
	ReadAdvancedSettings();
	prevType = 0;
	currentType = 0;
	dowelWithConus = 0;
	advDisplayCreated = false;

	connect(ui.buttonOk, SIGNAL(pressed()), this, SLOT(OnClickedOk()));
	connect(ui.spinBoxLenght, SIGNAL(valueChanged(double)), this, SLOT(SpinLenghtChanged(double)));
	connect(ui.spinBoxLenghtMax, SIGNAL(editingFinished()), this, SLOT(SpinLenghtMaxChanged()));

	connect(ui.spinBoxWidth, SIGNAL(valueChanged(double)), this, SLOT(SpinWidthChanged(double)));
	connect(ui.spinBoxWidthMax, SIGNAL(editingFinished()), this, SLOT(SpinWidthMaxChanged()));

	connect(ui.spinBoxKonus, SIGNAL(valueChanged(double)), this, SLOT(SpinKonusChanged(double)));
	connect(ui.spinBoxKonusMax, SIGNAL(editingFinished()), this, SLOT(SpinKonusMaxChanged()));


	connect(ui.radioWithKonus, SIGNAL(clicked()), this, SLOT(OnClickRadioWithKonus()));
	connect(ui.radioWithoutKonus, SIGNAL(clicked()), this, SLOT(OnClickRadioWithoutKonus()));

	connect(ui.comboBoxSelectType, SIGNAL(currentIndexChanged(int)), this, SLOT(OnClickSelectType()));
	connect(ui.comboBoxSelectAdvancedSettings, SIGNAL(currentIndexChanged(int)), this, SLOT(OnClickSelectAdvancedSettings()));

	connect(ui.buttonSaveAdvanced, SIGNAL(pressed()), this, SLOT(OnClickSaveAdvancedSettings()));

	connect(ui.buttonCreateNewAdvancedSettings, SIGNAL(pressed()), this, SLOT(OnClickCreateNewAdvancedSettings()));
	connect(ui.buttonDeleteAdvancedSetttings, SIGNAL(pressed()), this, SLOT(OnClickedDeleteAdvancedSettings()));
	connect(ui.buttonCreateType, SIGNAL(pressed()), this, SLOT(OnClickedNewType()));
	connect(ui.buttonButtonRemoveType, SIGNAL(pressed()), this, SLOT(OnClickedDeleteType()));
	connect(ui.buttonCancel, SIGNAL(clicked()), this, SLOT(OnClickedCancel()));

}

DowelSettings::~DowelSettings()
{

}


void DowelSettings::closeEvent(QCloseEvent * event)
{
	close();

}

void DowelSettings::OnShowDialog(int rights, int currentType)
{
	this->currentType = currentType;
	prevType = currentType;
	setWindowModality(Qt::ApplicationModal);
	this->setFocus();


	if (advDisplayCreated == false)
	{
		CreateAdvDisplay();
		advDisplayCreated = true;
	}


	ReplaceAdvanceSettings(selectedAdvSettings);
	
	
	this->show();
	TypeChanged();

}

void DowelSettings::ReadAdvancedSettings()
{
	QString filePath;
	int count = 0;
	QString typePath;
	QString typeName;
	QDir dir;
	filePath = referencePath + QString("/%1/settings/AdvancedSettings/").arg(REF_FOLDER_NAME);

	if (!QDir(filePath).exists())
		QDir().mkdir(filePath);

	QSettings settings(filePath, QSettings::IniFormat);

	dir.setPath(filePath);
	dir.setNameFilters(QStringList() << "*.ini");
	count = dir.count();
	QStringList list = dir.entryList();

	QStringList ls;
	//ls.append("General");
	ls.append("AdvTolerance");
	ls.append("ErrorMeasurements");
	ls.append("AdvToleranceNoKONUS");

	//inputs
	for (int j = 0; j < count; j++)
	{

		advSettings.push_back(AdvancedSettings(filePath, list[j], ls));
	}

	for (int i = 0; i < advSettings.size(); i++)
	{
		ui.comboBoxSelectAdvancedSettings->addItem(advSettings[i].name);
	}
}

void DowelSettings::OnClickedOk()
{
	/*QMessageBox msgBox;
	
	msgBox.setText("The document has been modified.");
	msgBox.setIcon(QMessageBox::Information);
	msgBox.exec();*/
	float val = ui.spinBoxKonus->value();
	tmpSettings[6] = val;
	tmpSettings[7] = val + ui.spinBoxKonusMax->value();
	tmpSettings[8] = val - ui.spinBoxKonusMax->value();

	 val = ui.spinBoxLenght->value();
	tmpSettings[0] = val;
	tmpSettings[1] = val + ui.spinBoxLenghtMax->value();
	tmpSettings[2] = val - ui.spinBoxLenghtMax->value();

	 val = ui.spinBoxWidth->value();
	tmpSettings[3] = val;
	tmpSettings[4] = val + ui.spinBoxWidthMax->value();
	tmpSettings[5] = val - ui.spinBoxWidthMax->value();

	SaveChangesToType();
	emit newTypeSelected(currentType);
	emit closeEvent(NULL);
	//this->close();

}

void DowelSettings::OnClickedCancel()
{
	emit closeEvent(NULL);
}

void DowelSettings::SpinLenghtChanged(double value)
{
	if (this->isVisible())
	{
		tmpSettings[0] = value;
		//tmpSettings[1] = value + ui.spinBoxLenghtMax->value();
		//tmpSettings[2] = value - ui.spinBoxLenghtMax->value();

	}
}

void DowelSettings::SpinWidthChanged(double value)
{

	tmpSettings[3] = value;
	//tmpSettings[4] = value + ui.spinBoxWidthMax->value();
	//tmpSettings[5] = value - ui.spinBoxWidthMax->value();

}

void DowelSettings::SpinKonusChanged(double value)
{

	tmpSettings[6] = value;
	//tmpSettings[7] = value +ui.spinBoxKonusMax->value();
	//tmpSettings[8] = value -ui.spinBoxKonusMax->value();
//
}

void DowelSettings::SpinLenghtMaxChanged()
{
	float val = ui.spinBoxLenght->value();

	tmpSettings[0] = val;
	tmpSettings[1] = val +  ui.spinBoxLenghtMax->value();
	tmpSettings[2] = val - ui.spinBoxLenghtMax->value();
}


void DowelSettings::SpinWidthMaxChanged()
{


	float val = ui.spinBoxWidth->value();
	tmpSettings[3] = val;
	tmpSettings[4] = val + ui.spinBoxWidthMax->value();
	tmpSettings[5] = val - ui.spinBoxWidthMax->value();

	

}

void DowelSettings::SpinKonusMaxChanged()
{
	float val = ui.spinBoxKonus->value();
	tmpSettings[6] = val;
	tmpSettings[7] = val + ui.spinBoxKonusMax->value();
	tmpSettings[8] = val - ui.spinBoxKonusMax->value();

}


void DowelSettings::OnClickRadioWithKonus()
{
	dowelWithConus = 1;
	ui.tabWidget->setTabEnabled(2, false);
	ui.tabWidget->setTabEnabled(0, true);
}

void DowelSettings::OnClickRadioWithoutKonus()
{
	dowelWithConus = 0;
	ui.tabWidget->setTabEnabled(2, true);
	ui.tabWidget->setTabEnabled(0, false);
}

void DowelSettings::TypeChanged()
{
	if (isVisible())
	{
		for (int i = 0; i < 9; i++)
		{
			tmpSettings[i] = basicSettings[currentType][i];
		}
		//tolLenghtMax = tmpSettings[1] - tmpSettings[0];
		//tolLenghtMin = tmpSettings[0] - tmpSettings[2];

		//tolWidthMax = tmpSettings[4] - tmpSettings[3];
		//tolWidthMin = tmpSettings[3] - tmpSettings[5];

		//tolKonusMax = tmpSettings[7] - tmpSettings[6];
		//tolKonusMin = tmpSettings[6] - tmpSettings[8];

		ui.spinBoxLenght->setValue(tmpSettings[0]);
		ui.spinBoxLenghtMax->setValue(tmpSettings[1] - tmpSettings[0]);
		//ui.spinBoxLenghtMin->setValue(tmpSettings[2]);

		ui.spinBoxWidth->setValue(tmpSettings[3]);
		ui.spinBoxWidthMax->setValue(tmpSettings[4] - tmpSettings[3]);
		//ui.spinBoxWidthMin->setValue(tmpSettings[5]);

		ui.spinBoxKonus->setValue(tmpSettings[6]);
		ui.spinBoxKonusMax->setValue(tmpSettings[7] - tmpSettings[6]);
		//ui.spinBoxKonusMin->setValue(tmpSettings[8]);


		if (dowelWithConus == 1)
		{
			ui.radioWithKonus->setChecked(true);
			ui.tabWidget->setTabEnabled(2, false);
			ui.tabWidget->setTabEnabled(0, true);

		}
		else
		{
			ui.radioWithoutKonus->setChecked(true);
			ui.tabWidget->setTabEnabled(0, false);
			ui.tabWidget->setTabEnabled(2, true);
		}
	}
}

void DowelSettings::OnClickSelectType()
{
	if(isVisible())
	{ 
	int index = ui.comboBoxSelectType->currentIndex();
	int ret = QMessageBox::NoButton;
	

	if (index != prevType)
	{
		if(IsTypeChanged() == true)
			ret = OnSaveChanges();
	}
	switch (ret) {
	case QMessageBox::Save:
		SaveChangesToType();
		currentType = index;
		break;
	case QMessageBox::Discard:
		// Don't Save was clicked
		currentType = index;
		break;
	case QMessageBox::Cancel:
		currentType = prevType;
		break;
	case QMessageBox::NoButton:
		currentType = index;
		break;

	default:
		// should never be reached
		break;
	}


	ui.comboBoxSelectType->setCurrentIndex(currentType);
	prevType = currentType;
	TypeChanged();
	ui.comboBoxSelectAdvancedSettings->setCurrentText(typeAdvSettings[index]);
	}

}

void DowelSettings::OnClickSelectAdvancedSettings()
{
	if (isVisible())
	{
		int index = ui.comboBoxSelectAdvancedSettings->currentIndex();
		QString text = ui.comboBoxSelectAdvancedSettings->currentText();



		selectedAdvSettings = text;

		ReplaceAdvanceSettings(text);

	}

}

void DowelSettings::OnClickSaveAdvancedSettings()//shranimo in posljemo signal v mbsoftware razred za advanced settings
{
	QString tmpText =  ui.comboBoxSelectAdvancedSettings->currentText();
	int index = -1;
	for (int i = 0; i < advSettings.size(); i++)
	{
		if (tmpText.compare(advSettings[i].name) == 0)
		{
			index = i;

			for (int j = 0; j < advSettings[i].groupName.size(); j++)
			{
				if (advSettings[i].groupName[j] == "AdvTolerance")
				{
					advSettings[i].parameterValue[j][0] = ui.lineEdit10->text().toFloat();
					advSettings[i].parameterValue[j][1] = ui.lineEdit11->text().toFloat();
					advSettings[i].parameterValue[j][2] = ui.lineEdit12->text().toFloat();
					advSettings[i].parameterValue[j][3] = ui.lineEdit13->text().toFloat();
					advSettings[i].parameterValue[j][4] = ui.lineEdit14->text().toFloat();
					advSettings[i].parameterValue[j][5] = ui.lineEdit15->text().toFloat();
					advSettings[i].parameterValue[j][6] = ui.lineEdit16->text().toFloat();
				}
				if ( advSettings[i].groupName[j] == "ErrorMeasurements")
				{
					advSettings[i].parameterValue[j][0] = ui.lineEdit20->text().toInt();
					advSettings[i].parameterValue[j][1] = ui.lineEdit21->text().toInt();
					advSettings[i].parameterValue[j][2] = ui.lineEdit22->text().toInt();
					advSettings[i].parameterValue[j][3] = ui.lineEdit23->text().toInt();
					advSettings[i].parameterValue[j][4] = ui.lineEdit24->text().toInt();
					advSettings[i].parameterValue[j][5] = ui.lineEdit25->text().toInt();
					advSettings[i].parameterValue[j][6] = ui.lineEdit26->text().toInt();
					advSettings[i].parameterValue[j][7] = ui.lineEdit27->text().toInt();
					advSettings[i].parameterValue[j][8] = ui.lineEdit28->text().toFloat();
				}
				if (advSettings[i].groupName[j] == "AdvToleranceNoKonus")
				{
					advSettings[i].parameterValue[j][0] = ui.lineEdit30->text().toFloat();
					advSettings[i].parameterValue[j][1] = ui.lineEdit31->text().toFloat();
					advSettings[i].parameterValue[j][2] = ui.lineEdit32->text().toFloat();
					advSettings[i].parameterValue[j][3] = ui.lineEdit33->text().toFloat();
					advSettings[i].parameterValue[j][4] = ui.lineEdit34->text().toFloat();
					advSettings[i].parameterValue[j][5] = ui.lineEdit35->text().toFloat();
					advSettings[i].parameterValue[j][6] = ui.lineEdit36->text().toFloat();
				}
				advSettings[i].WriteParameters(advSettings[i].groupName[j]);
			}
		}
	}
	emit advancedSettingsSaved(tmpText);

}

void DowelSettings::SaveChangesToType()
{

	for (int i =  0; i < 9; i++)
	{
		basicSettings[currentType][i] = tmpSettings[i];
	}

}

bool DowelSettings::IsTypeChanged()
{
	bool isChanged = false;

	for (int i = 0; i < 9; i++)
	{
		if (tmpSettings[i] != basicSettings[currentType][i])
			isChanged = true;
	}

	return isChanged;
}

int  DowelSettings::OnSaveChanges()
{
	QMessageBox msgBox;
	msgBox.setText("The document has been modified.");
	msgBox.setInformativeText("Do you want to save your changes?");
	msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Save);
	msgBox.setIcon(QMessageBox::Warning);
	int ret = msgBox.exec();

	return ret;
}

void DowelSettings::CreateAdvDisplay()
{
	if (advSettings.size() > 0)
	{

		for (int i = 0; i < advSettings[0].groupName.size(); i++)
		{
	
			if (advSettings[0].groupName[i] == "AdvTolerance")
			{
					ui.lineEdit10->setText(QString("%1").arg( advSettings[0].parameterValue[i][0]));
					ui.lineEdit11->setText(QString("%1").arg( advSettings[0].parameterValue[i][1]));
					ui.lineEdit12->setText(QString("%1").arg( advSettings[0].parameterValue[i][2]));
					ui.lineEdit13->setText(QString("%1").arg( advSettings[0].parameterValue[i][3]));
					ui.lineEdit14->setText(QString("%1").arg( advSettings[0].parameterValue[i][4]));
					ui.lineEdit15->setText(QString("%1").arg( advSettings[0].parameterValue[i][5]));
					ui.lineEdit16->setText(QString("%1").arg( advSettings[0].parameterValue[i][6]));
			}			
			if (advSettings[0].groupName[i] == "ErrorMeasurements")
			{
				ui.lineEdit20->setText(QString("%1").arg( advSettings[0].parameterValue[i][0]));
				ui.lineEdit21->setText(QString("%1").arg( advSettings[0].parameterValue[i][1]));
				ui.lineEdit22->setText(QString("%1").arg( advSettings[0].parameterValue[i][2]));
				ui.lineEdit23->setText(QString("%1").arg( advSettings[0].parameterValue[i][3]));
				ui.lineEdit24->setText(QString("%1").arg( advSettings[0].parameterValue[i][4]));
				ui.lineEdit25->setText(QString("%1").arg( advSettings[0].parameterValue[i][5]));
				ui.lineEdit26->setText(QString("%1").arg( advSettings[0].parameterValue[i][6]));
				ui.lineEdit27->setText(QString("%1").arg( advSettings[0].parameterValue[i][7]));
				ui.lineEdit28->setText(QString("%1").arg( advSettings[0].parameterValue[i][8]));
			}
			if (advSettings[0].groupName[i] == "AdvToleranceNoKONUS")
			{
				ui.lineEdit30->setText(QString("%1").arg( advSettings[0].parameterValue[i][0]));
				ui.lineEdit31->setText(QString("%1").arg( advSettings[0].parameterValue[i][1]));
				ui.lineEdit32->setText(QString("%1").arg( advSettings[0].parameterValue[i][2]));
				ui.lineEdit33->setText(QString("%1").arg( advSettings[0].parameterValue[i][3]));
				ui.lineEdit34->setText(QString("%1").arg( advSettings[0].parameterValue[i][4]));
				ui.lineEdit35->setText(QString("%1").arg( advSettings[0].parameterValue[i][5]));
				ui.lineEdit36->setText(QString("%1").arg( advSettings[0].parameterValue[i][6]));
			}
		}
	}
}

void DowelSettings::ReplaceAdvanceSettings(QString advSettingsName)
{
	for (int i = 0; i < advSettings.size(); i++)
	{
		if (advSettingsName.compare(advSettings[i].name) == 0)
		{
			for (int j = 0; j < advSettings[i].groupName.size(); j++)
			{
				if (advSettings[i].groupName[j] == "AdvTolerance")
				{

					ui.lineEdit10->setText(QString("%1").arg(advSettings[i].parameterValue[j][0]));
					ui.lineEdit11->setText(QString("%1").arg(advSettings[i].parameterValue[j][1]));
					ui.lineEdit12->setText(QString("%1").arg(advSettings[i].parameterValue[j][2]));
					ui.lineEdit13->setText(QString("%1").arg(advSettings[i].parameterValue[j][3]));
					ui.lineEdit14->setText(QString("%1").arg(advSettings[i].parameterValue[j][4]));
					ui.lineEdit15->setText(QString("%1").arg(advSettings[i].parameterValue[j][5]));
					ui.lineEdit16->setText(QString("%1").arg(advSettings[i].parameterValue[j][6]));
				}
				if (advSettings[i].groupName[j] == "ErrorMeasurements")
				{
					ui.lineEdit20->setText(QString("%1").arg(advSettings[i].parameterValue[j][0]));
					ui.lineEdit21->setText(QString("%1").arg(advSettings[i].parameterValue[j][1]));
					ui.lineEdit22->setText(QString("%1").arg(advSettings[i].parameterValue[j][2]));
					ui.lineEdit23->setText(QString("%1").arg(advSettings[i].parameterValue[j][3]));
					ui.lineEdit24->setText(QString("%1").arg(advSettings[i].parameterValue[j][4]));
					ui.lineEdit25->setText(QString("%1").arg(advSettings[i].parameterValue[j][5]));
					ui.lineEdit26->setText(QString("%1").arg(advSettings[i].parameterValue[j][6]));
					ui.lineEdit27->setText(QString("%1").arg(advSettings[i].parameterValue[j][7]));
					ui.lineEdit28->setText(QString("%1").arg(advSettings[i].parameterValue[j][8]));

				}
				if ( advSettings[0].groupName[j] == "AdvToleranceNoKONUS")
				{
					ui.lineEdit30->setText(QString("%1").arg( advSettings[0].parameterValue[i][0]));
					ui.lineEdit31->setText(QString("%1").arg( advSettings[0].parameterValue[i][1]));
					ui.lineEdit32->setText(QString("%1").arg( advSettings[0].parameterValue[i][2]));
					ui.lineEdit33->setText(QString("%1").arg( advSettings[0].parameterValue[i][3]));
					ui.lineEdit34->setText(QString("%1").arg( advSettings[0].parameterValue[i][4]));
					ui.lineEdit35->setText(QString("%1").arg( advSettings[0].parameterValue[i][5]));
					ui.lineEdit36->setText(QString("%1").arg( advSettings[0].parameterValue[i][6]));
				}
			
			}
		}
	}
}


void DowelSettings::OnClickCreateNewAdvancedSettings()
{

	QInputDialog qDialog;
	QInputDialog nameDialog;
	QStringList items;
	QString name;
	for (int i = 0; i < advSettings.size(); i++)
	{
		items.append(advSettings[i].name);
	}
	QString tmp;
	qDialog.setOptions(QInputDialog::UseListViewForComboBoxItems);
	qDialog.setComboBoxItems(items);
	qDialog.setLabelText("COPY ADV. SETTINGS:");
	qDialog.setWindowTitle("SELECT ADV. SETTINGS!");
	if (qDialog.exec() == IDOK)
	{
		tmp = qDialog.textValue();

		nameDialog.setWindowTitle("Add new type");
		nameDialog.setLabelText("Insert type name:");
	}

	if (nameDialog.exec() == IDOK)
	{
		name =  nameDialog.textValue();
		CopyAdvSettings(tmp, name);
	}

}

void DowelSettings::CopyAdvSettings(QString existed, QString newName)
{
	for (int i = 0; i < advSettings.size(); i++)
	{
		if (existed.compare(advSettings[i].name) == 0)//preverimo katere nastavitve shrani 
		{
			advSettings.push_back(AdvancedSettings(advSettings[i]));
			
			advSettings.back().name = newName;
			advSettings.back().fileName = QString("%1.ini").arg(newName);

			ui.comboBoxSelectAdvancedSettings->addItem(newName);
		}
	}

}

void DowelSettings::DeleteAdvancedSettings(QString selected)
{
	int sel = 0;
	for (int i = 0; i < advSettings.size(); i++)
	{
		if (selected.compare(advSettings[i].name) == 0)//poiscemo izbran index 
		{
			sel = i;
			QString path = advSettings[i].referencePath + advSettings[i].fileName;
			QFile::remove(path);


			advSettings.erase(advSettings.begin() + sel);
			ui.comboBoxSelectAdvancedSettings->removeItem(i);


		}
	}

}

void DowelSettings::OnClickedDeleteType()
{
}

void DowelSettings::OnClickedNewType()
{
	QInputDialog qDialog;
	QInputDialog nameDialog;
	QStringList items;
	QString name;
	for (int i = 0; i < typeName.size(); i++)
	{
		items.append(typeName[i]);
	}
	QString tmp;
	int selected = -1;
	qDialog.setOptions(QInputDialog::UseListViewForComboBoxItems);
	qDialog.setComboBoxItems(items);
	qDialog.setLabelText("COPY TYPE:");
	qDialog.setWindowTitle("SELECT TYPE TO COPY:");
	if (qDialog.exec() == IDOK)
	{
		tmp = qDialog.textValue();
		nameDialog.setWindowTitle("Add new type");
		nameDialog.setLabelText("Insert type name:");
		for (int i = 0; i < typeName.size(); i++)
		{
			if (typeName[i].compare(tmp) == 0)
			{
				selected = i;
				break;
			}
			
		}
	}

	if (nameDialog.exec() == IDOK)
	{
		
		name = nameDialog.textValue();
		basicSettings.resize(basicSettings.size() +1);
		for (int i = 0; i < 9; i++)
		{
			basicSettings.back().push_back(basicSettings[selected][i]);
		}
		typeName.push_back(name);
		typeAdvSettings.push_back(typeAdvSettings[selected]);
		typeColorSettings.push_back(typeColorSettings[selected]);

		emit createNewTypeSignal(tmp, name);
		ui.comboBoxSelectType->addItem(name);

		ui.comboBoxSelectType->setCurrentText(name);
	}
}

void DowelSettings::OnClickedDeleteAdvancedSettings()
{
	QInputDialog qDialog;
	QStringList items;
	QString name;

	if (advSettings.size() > 1)
	{
		for (int i = 0; i < advSettings.size(); i++)
		{
			items.append(advSettings[i].name);
		}
		QString tmp;
		qDialog.setOptions(QInputDialog::UseListViewForComboBoxItems);
		qDialog.setComboBoxItems(items);
		qDialog.setLabelText("DELETE ADV. SETINGS:");
		qDialog.setWindowTitle("SELECT ADV. SETTINGS TO DELETE!");
		if (qDialog.exec() == IDOK)
		{
			tmp = qDialog.textValue();

			int ret = QMessageBox::warning(this, tr("Warning"),
				tr("DELETE SELECTED ITEM?"),
				QMessageBox::Ok | QMessageBox::Cancel);

			if (ret == QMessageBox::Ok)
			{
				//name = nameDialog.textValue();
				DeleteAdvancedSettings(tmp);
			}
		}
	}
	else
	{
		QMessageBox::warning(this, tr("Warning"),
			tr("1. SETTINGS FOUND!\n" "CANT ERASE THE ONLY SETTING"),
			QMessageBox::Ok);
	}

}


