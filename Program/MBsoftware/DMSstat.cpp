#include "stdafx.h"

#include "DMSstat.h"

QMutex CDMSstat::_mutexBackupBox;
QMutex CDMSstat::_mutexPermanentCounters;

int CDMSstat::_saveTimePeriod = 0;
QDateTime CDMSstat::_lastSaveTime;

QDateTime CDMSstat::_lastBackupBoxTime;

QDateTime CDMSstat::_lastStartDateTime;
QDateTime CDMSstat::_beforeLastStopDateTime;
QDateTime CDMSstat::_lastStopDateTime;
int CDMSstat::_startGood;
int CDMSstat::_startBad;
int CDMSstat::_startValve;
bool CDMSstat::_napravaOn = false;

double CDMSstat::_boxSumLength;
double CDMSstat::_boxSumWidth;

unsigned long long int CDMSstat::_permanentCounterGood;
unsigned long long int CDMSstat::_permanentCounterBad;
unsigned long long int CDMSstat::_permanentCounterValve;
QDateTime CDMSstat::_lastPermanentCountersSaveTime;

QString CDMSstat::_countersPath;

CDMSstat::CDMSstat()
{
	
}

CDMSstat::~CDMSstat()
{

}

QString CDMSstat::PodaljsajNiz(QString niz)
{
	const int minLength = 15;  //naj bo daljse od dolzine imena tipa 

	int length;

	if (niz.size() >= minLength) length = niz.size() + 1;
	else length = minLength;


	return PodaljsajNiz(niz, length);
}

QString CDMSstat::PodaljsajNiz(QString niz, int length)
{
	while (niz.length() < length) niz = niz + QString(" ");

	return niz;
}

QString CDMSstat::DMSstatDolociDirPath(int vrstaStatistike)
{
	QString dirPath;

	switch (vrstaStatistike)
	{
	case 0:
		dirPath = QString("C:/Data/Time/");
		break;
	case 1:
		dirPath = QString("C:/Data/Box/");
		break;
	case 2:
		dirPath = QString("C:/Data/Stop/");
		break;
	}

	return dirPath;
}

QString CDMSstat::DMSstatDolociFileName(int vrstaStatistike, int fileNumber, QDate* datum)
{
	QString fileName;

	//Datum raje dolocim le na enem mestu in posredujem kot argument, da ni potencialnih problemov zaradi razlicnih datumov
	//QDate* datum = &QDate::currentDate();

	switch (vrstaStatistike)
	{
	case 0:
		fileName = QString("Time");
		break;
	case 1:
		fileName = QString("Box");
		break;
	case 2:
		fileName = QString("Stop");
		break;
	}




	if (_MirovFormat)
	{
		fileName += QString("%1_").arg(datum->month(), 2, 10, QChar('0'));
		fileName += QString("%1_").arg(datum->day(), 2, 10, QChar('0'));
		fileName += QString("%1_").arg(datum->year() % 100, 2, 10, QChar('0'));
		fileName += QString(" %1.txt").arg(fileNumber);
	}
	else
	{
		fileName += QString("%1_").arg(datum->month());
		fileName += QString("%1_").arg(datum->day());
		fileName += QString("%1_").arg(datum->year());
		fileName += QString("%1.txt").arg(fileNumber);
	}

	return fileName;
}

QString CDMSstat::DMSstatDolociPot(int vrstaStatistike, int fileNumber, QDate* datum)
{
	return DMSstatDolociDirPath(vrstaStatistike) + DMSstatDolociFileName(vrstaStatistike, fileNumber, datum);

}

int CDMSstat::FindFileNumber(int vrstaStatistike, QDate* datum)
{
	int prviNeobstojeciFile = 0;

	QString dirPath = DMSstatDolociDirPath(vrstaStatistike);
	QString fileName;

	do {
		prviNeobstojeciFile++;
		fileName = DMSstatDolociFileName(vrstaStatistike, prviNeobstojeciFile, datum);
	} while (QDir(dirPath).exists(fileName));

	//preveril sem, da se prviNeobstojeciFile pravilno izracuna!

	int zadnjiObstojeciFile = prviNeobstojeciFile - 1;

	int fileNumber = zadnjiObstojeciFile;

	if (fileNumber == 0) fileNumber = 1;

	QString fullPath = DMSstatDolociPot(vrstaStatistike, fileNumber, datum);  //POZOR: to je pot do zadnje obstojece datoteke: ce je ta prevelika, moramo pisati v naslednjo

	QFile file(fullPath);

	if (file.size() > MAX_FILE_SIZE) fileNumber++;

	//PREIZKUSENO DELA!

	return fileNumber;
}

char * CDMSstat::ToChar(QString niz)
{
	QByteArray bArray = niz.toLocal8Bit();
	return (char *)strdup(bArray.constData());
}

void CDMSstat::SaveTime(int dowelGood, int dowelBad, int valve, float length, float tolLen, float width, float tolWidth, float konus, float tolKon, QString operater)
{
	//INDEX 	 Time     	 DowlGood 	  DowlBad 	   Valve   	 Lenght  	 TolLen 	 Width  	  TolWidth  	  Konus 	 TolKon 	 Operater    	 Index
		//1	 00:00 : 31 	  70020 	 169909 	  60240 	  30.00 	   0.50 	   8.00 	   0.20 	   3.00 	   2.00 	           Barut      1
	QDate* datum = &QDate::currentDate();
	QTime* cas = &QTime::currentTime();

	if (_MirovFormat)
	{
		QString novaVrstica;

		char c[200];

		sprintf(c, "%02d:%02d:%02d \t %6d \t %6d \t %6d \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %6.2f \t %15s ", cas->hour(), cas->minute(), cas->second(), dowelGood, dowelBad, valve, length, tolLen, width, tolWidth, konus, tolKon, ToChar(operater));

		novaVrstica = QString(c);

		SaveStatMiro(0, novaVrstica, datum);
	}
	else
	{
		vector<QString> novaVrstica;

		//Prvi in zadnji element v vektorju hranita Index: tu ga nastavim na -1, ker se v SaveStat prebere iz datoteke
		novaVrstica.push_back(QString("%1").arg(-1));
		novaVrstica.push_back(QString("%1:%2:%3").arg(cas->hour()).arg(cas->minute()).arg(cas->second()));
		novaVrstica.push_back(QString("%1").arg(dowelGood));
		novaVrstica.push_back(QString("%1").arg(dowelBad));
		novaVrstica.push_back(QString("%1").arg(valve));
		novaVrstica.push_back(QString("%1").arg(length));
		novaVrstica.push_back(QString("%1").arg(tolLen));
		novaVrstica.push_back(QString("%1").arg(width));
		novaVrstica.push_back(QString("%1").arg(tolWidth));
		novaVrstica.push_back(QString("%1").arg(konus));
		novaVrstica.push_back(QString("%1").arg(tolKon));
		novaVrstica.push_back(operater);
		novaVrstica.push_back(QString("%1").arg(-1));

		SaveStat(0, novaVrstica, datum);
	}


}

void CDMSstat::SaveBox(int boxNumber, float setMass, float mass, int dowelIn, float setLength, float setLengthTol, float setWidth, float setWidthTol, float boxAvgLength, float boxAvgWidth, int badDowels, int goodDowels, QString operater)
{
	//Nub-Box	 SetWeig 	Weight 	 DowelIn 	  SetLeng 	 SetLTol 	 SetWidth	 SetWTol 	 BoxLen  	 BoxWidth 	  BadDow	   GoodDow	    Time	   Date 	 Operater
	//0	   0.00 	  0.00 	  25051 	   30.00	    0.50	    8.00	    0.20	   30.14	    8.06	   22209	   27879	  00:38 : 06 	  10_19_22 	  Barut

	QDate* datum = &QDate::currentDate();
	QTime* cas = &QTime::currentTime();

	if (_MirovFormat)
	{
		QString novaVrstica;

		char c[200];

		sprintf(c, "%6d\t %6.2f \t% 6.2f \t %6d \t%8.2f\t%8.2f\t%8.2f\t%8.2f\t%8.2f\t%8.2f\t%8d\t%8d\t  %02d:%02d:%02d \t  %02d_%02d_%02d \t  %s \n", boxNumber, setMass, mass, dowelIn, setLength, setLengthTol, setWidth, setWidthTol, boxAvgLength, boxAvgWidth, badDowels, goodDowels, cas->hour(), cas->minute(), cas->second(), datum->month(), datum->day(), datum->year() % 100, ToChar(operater));

		novaVrstica = QString(c);

		SaveStatMiro(1, novaVrstica, datum);

	}
	else
	{
		vector<QString> novaVrstica;

		novaVrstica.push_back(QString("%1").arg(boxNumber));
		novaVrstica.push_back(QString("%1").arg(setMass));
		novaVrstica.push_back(QString("%1").arg(mass));
		novaVrstica.push_back(QString("%1").arg(dowelIn));
		novaVrstica.push_back(QString("%1").arg(setLength));
		novaVrstica.push_back(QString("%1").arg(setLengthTol));
		novaVrstica.push_back(QString("%1").arg(setWidth));
		novaVrstica.push_back(QString("%1").arg(setWidthTol));
		novaVrstica.push_back(QString("%1").arg(boxAvgLength));
		novaVrstica.push_back(QString("%1").arg(boxAvgWidth));
		novaVrstica.push_back(QString("%1").arg(badDowels));
		novaVrstica.push_back(QString("%1").arg(goodDowels));
		novaVrstica.push_back(QString("%1:%2:%3").arg(cas->hour()).arg(cas->minute()).arg(cas->second()));
		novaVrstica.push_back(QString("%1_%2_%3").arg(datum->month()).arg(datum->day()).arg(datum->year()));
		novaVrstica.push_back(operater);

		SaveStat(1, novaVrstica, datum);
	}

	

}

void CDMSstat::SaveStop(QTime* startTime, int startGood, int startBad, int startValve, QTime* stopTime, int stopGood, int stopBad, int stopValve, int timeOff, int timeOn, int stopError)
{
	// INDEX 	 StartTime	 StartG 	  StartB 	  StartV 	 StopTime	 StopG  	 StopBad 	 StopVal 	 TimeOFF   	 TimeOn 	 StopERROR 	  Index   
	//715	 23:55 : 51 	  21124 	  41891 	  18480 	 00 : 00 : 09 	  22720 	  45062 	  19895 	      5 	    258 	      4 	    715

	QDate* datum = &QDate::currentDate();

	if (_MirovFormat)
	{
		QString novaVrstica;

		char c[200];

		//%6d\t %s \t % 6d \t % 6d \t % 6d \t %s \t % 6d \t % 6d \t % 6d \t % 6d \t % 6d \t % 6d \t % 6d  \n

		sprintf(c, "%02d:%02d:%02d \t % 6d \t % 6d \t % 6d \t %02d:%02d:%02d \t % 6d \t % 6d \t % 6d \t % 6d \t % 6d \t % 6d \t % ", startTime->hour(), startTime->minute(), startTime->second(), startGood, startBad, startValve, stopTime->hour(), stopTime->minute(), stopTime->second(), stopGood, stopBad, stopValve, timeOff, timeOn, stopError);

		novaVrstica = QString(c);

		SaveStatMiro(2, novaVrstica, datum);
		
	}
	else
	{
		vector<QString> novaVrstica;

		//Prvi in zadnji element v vektorju hranita Index: tu ga nastavim na -1, ker se v SaveStat prebere iz datoteke
		novaVrstica.push_back(QString("%1").arg(-1));
		novaVrstica.push_back(QString("%1:%2:%3").arg(startTime->hour()).arg(startTime->minute()).arg(startTime->second()));
		novaVrstica.push_back(QString("%1").arg(startGood));
		novaVrstica.push_back(QString("%1").arg(startBad));
		novaVrstica.push_back(QString("%1").arg(startValve));
		novaVrstica.push_back(QString("%1:%2:%3").arg(stopTime->hour()).arg(stopTime->minute()).arg(stopTime->second()));
		novaVrstica.push_back(QString("%1").arg(stopGood));
		novaVrstica.push_back(QString("%1").arg(stopBad));
		novaVrstica.push_back(QString("%1").arg(stopValve));
		novaVrstica.push_back(QString("%1").arg(timeOff));
		novaVrstica.push_back(QString("%1").arg(timeOn));
		novaVrstica.push_back(QString("%1").arg(stopError));
		novaVrstica.push_back(QString("%1").arg(-1));

		SaveStat(2, novaVrstica, datum);
	}


}

vector<QString> CDMSstat::VrniNaslovnoVrstico(int vrstaStatistike)
{
	vector<QString> naslovnaVrstica;

	switch (vrstaStatistike)
	{
	case 0:

		naslovnaVrstica = { "Index", "Time", "DowelGood", "DowelBad", "Valve", "Length", "TolLen", "Width", "TolWidth", "Konus", "TolKon", "operater", "Index" };
		break;
	case 1:
		naslovnaVrstica = { "BoxNumber", "SetMass", "Mass", "DowelIn", "SetLength", "SetLengthTol", "SetWidth", "SetWidthTol", "BoxAvgLength", "BoxAvgWidth", "BadDowels", "GoodDowels", "Time", "Date", "operater" };
		break;
	case 2:
		naslovnaVrstica = { "Index", "StartTime", "StartGood", "StartBad", "StartValve", "StopTime", "StopGood", "StopBad", "StopValve", "TimeOff", "TimeOn", "StopError", "Index" };
		break;
	}

	return naslovnaVrstica;
}

QString CDMSstat::VrniNaslovnoVrsticoMiro(int vrstaStatistike)
{
	QString naslovnaVrstica;

	switch (vrstaStatistike)
	{
	case 0:
		naslovnaVrstica = QString(" INDEX \t Time     \t DowlGood \t  DowlBad \t   Valve   \t Lenght  \t TolLen \t Width  \t  TolWidth  \t  Konus \t TolKon \t operater    \t Index   \n");
		break;
	case 1:
		naslovnaVrstica = QString("Nub-Box\t SetWeig \tWeight \t DowelIn \t  SetLeng \t SetLTol \t SetWidth\t SetWTol \t BoxLen  \t BoxWidth \t  BadDow\t   GoodDow\t    Time\t   Date \t operater\n");
		break;
	case 2:
		naslovnaVrstica = QString(" INDEX \t StartTime\t StartG \t  StartB \t  StartV \t StopTime\t StopG  \t StopBad \t StopVal \t TimeOFF   \t TimeOn \t StopERROR \t  Index   \n");
		break;
	}

	return naslovnaVrstica;
}

bool CDMSstat::SaveStat(int vrstaStatistike, vector<QString> novaVrstica, QDate* datum)
{


	//preverimo, ce direktorij obstaja: ce ne, ga kreiramo
	QString dirPath = DMSstatDolociDirPath(vrstaStatistike);

	if (!QDir(dirPath).exists())
	{
		QDir().mkdir(dirPath);
	}
	int fileNumber = FindFileNumber(vrstaStatistike, datum);
	QString fileName = DMSstatDolociFileName(vrstaStatistike, fileNumber, datum);

	//ni treba preverjati, ce file obstaja, ker file.open avtomaticno kreira nov file, ce ta ne obstaja
	
	QFile file(dirPath + fileName);

	if (file.open(QIODevice::ReadWrite | QIODevice::Append))
	{
		QTextStream in(&file);

		int size = file.size();

		vector<QString> naslovnaVrstica = VrniNaslovnoVrstico(vrstaStatistike);

		int index = -1;

		if (size < 50) // ce je prazna datoteka, se nimamo naslovne vrstice
		{
			index = 0;
			
			for (int i = 0; i < naslovnaVrstica.size(); i++)
			{
				in << PodaljsajNiz(naslovnaVrstica[i]);
			}


			in << endl;
		}
		else
		{
			//tu preberemo index iz zadnje besede zadnje vrstice datoteke
			if (vrstaStatistike == 0 || vrstaStatistike == 2)
			{
				const int minLength = 15;  //naj bo daljse od dolzine imena tipa 

				file.seek(file.size() - (minLength + 1));

				QString niz = file.read(minLength + 1);

				QStringList list;

				list = niz.split(" ");

				QString zadnjaBeseda = list.takeFirst();

				index = zadnjaBeseda.toFloat();
			}

		}

		assert(novaVrstica.size() == naslovnaVrstica.size());

		if (vrstaStatistike == 0 || vrstaStatistike == 2)
		{
			index++;
			novaVrstica[0] = QString("%1").arg(index);
			novaVrstica[novaVrstica.size() - 1] = QString("%1").arg(index);
		}

		//tukaj dodam novo vrstico v file
		for (int i = 0; i < novaVrstica.size(); i++)
		{
			int length = PodaljsajNiz(naslovnaVrstica[i]).size();
			in << PodaljsajNiz(novaVrstica[i], length);

		}
		in << endl;

		file.close();

		return true;
	}
	else return false;

}

bool CDMSstat::SaveStatMiro(int vrstaStatistike, QString novaVrstica, QDate* datum)
{

	//preverimo, ce direktorij obstaja: ce ne, ga kreiramo
	QString dirPath = DMSstatDolociDirPath(vrstaStatistike);

	if (!QDir(dirPath).exists())
	{
		QDir().mkdir(dirPath);
	}
	int fileNumber = FindFileNumber(vrstaStatistike, datum);
	QString fileName = DMSstatDolociFileName(vrstaStatistike, fileNumber, datum);

	//ni treba preverjati, ce file obstaja, ker file.open avtomaticno kreira nov file, ce ta ne obstaja

	QFile file(dirPath + fileName);

	if (file.open(QIODevice::ReadWrite | QIODevice::Append))
	{
		QTextStream in(&file);

		int size = file.size();

		QString naslovnaVrstica = VrniNaslovnoVrsticoMiro(vrstaStatistike);

		int index = -1;

		if (size < 50) // ce je prazna datoteka, se nimamo naslovne vrstice
		{
			index = 0;

			in << naslovnaVrstica;

			//in << endl tu ni potreben, ker QString naslovnaVrstica na koncu vsebuje \n 
		}
		else
		{
			//tu preberemo index iz zadnje besede zadnje vrstice datoteke
			if (vrstaStatistike == 0 || vrstaStatistike == 2)
			{
				index = 7;  //kasneje sprogramiraj branje iz datoteke!

				const int Nznakov = 7; //index je v datoteko zapisan s formatom %6d + presledek

				file.seek(file.size() - (Nznakov + 1)); 

				QString niz = file.read(Nznakov + 1);

				index = niz.toFloat();
			}
		}

		QString novaVrsticaRazsirjena;

		if (vrstaStatistike == 0 || vrstaStatistike == 2)
		{
			index++;

			char cSpredaj[20];
			char cZadaj[20];

			sprintf(cSpredaj, "%6d\t ", index);
			sprintf(cZadaj, "%6d \n", index);

			QString nizSpredaj(cSpredaj);
			QString nizZadaj(cZadaj);

			novaVrsticaRazsirjena = nizSpredaj + novaVrstica + nizZadaj;
		}
		else
		{
			novaVrsticaRazsirjena =  novaVrstica;
		}
		
		in << novaVrsticaRazsirjena;
		


		file.close();

		return true;
	}
	else return false;
}



void CDMSstat::ResetCurrentBox()
{
	_boxSumLength = 0;
	_boxSumWidth = 0;

	BackupVariables();
}

void CDMSstat::BackupVariables()
{
	_mutexBackupBox.lock();

	QStringList list;

	QString fullPath = _countersPath + QString("BackupVar.ini");

	QSettings settings(fullPath, QSettings::IniFormat);

	
	list.append(QString("%1").arg(_boxSumLength));

	settings.setValue(QString("_boxSumLength"), list);

	list.clear();

	list.append(QString("%1").arg(_boxSumWidth));

	settings.setValue(QString("_boxSumWidth"), list);

	list.clear();

	list.append(QString("%1").arg(_saveTimePeriod));

	settings.setValue(QString("_saveTimePeriod"), list);

	list.clear();


	_mutexBackupBox.unlock();
}

void CDMSstat::RestoreBoxVariables()
{
	_mutexBackupBox.lock();

	QStringList list;

	//QString fullPath = QString("C:/Data/BackupBox.ini");

	QString fullPath = _countersPath + QString("BackupVar.ini");

	bool fileExists = QFile::exists(fullPath);

	bool dataOK = true;

	if (fileExists)
	{
		QSettings settings(fullPath, QSettings::IniFormat);

		list = settings.value(QString("_boxSumLength")).toStringList();

		if (list.size() > 0)
		{
			_boxSumLength = list[0].toDouble();
		}
		else dataOK = false;

		list.clear();

		list = settings.value(QString("_boxSumWidth")).toStringList();

		if (list.size() > 0)
		{
			_boxSumWidth = list[0].toDouble();
		}
		else dataOK = false;

		list.clear();

		list = settings.value(QString("_saveTimePeriod")).toStringList();

		if (list.size() > 0)
		{
			_saveTimePeriod = (list[0].toInt());

		}
		else dataOK = false;

		list.clear();
	}

	_mutexBackupBox.unlock();  //POZOR: mutex moramo odkleniti pred ResetBoxNumber, ker ga tam spet zaklenemo!

	if (!fileExists || !dataOK) ResetBoxNumber();
	
}

void CDMSstat::Init()
{
	//Po zgledu Aleseve kode dolocitev referencePath pri projektu DMS (konstruktor MBSoftware)
	QDir referenceDirectory = QDir::currentPath();  //direktorij exe datoteke
	referenceDirectory.cdUp();   //vrne naddirektorij 
	referenceDirectory.cdUp();   //vrne naddirektorij

	QString	referencePath = referenceDirectory.path();
	_countersPath = referencePath + QString("/%1/DMSstat/").arg(REF_FOLDER_NAME);
}

bool CDMSstat::SetTimePeriod(int period)
{
	if (period > 0)
	{
		_saveTimePeriod = period;

		BackupVariables();

		return true;
	}
	else return false;
	
}

bool CDMSstat::CheckSaveTime(int dowelGood, int dowelBad, int valve, float length, float tolLen, float width, float tolWidth, float konus, float tolKon, QString operater)
{
	if (!_napravaOn) return false;  //redno shranjevanje je onemogoceno, kadar naprava ne dela

	QDateTime dateTime = QDateTime::currentDateTime();

	bool save;

	if (_lastSaveTime.isValid())
	{
		if (_saveTimePeriod > 0)  //periodicno shranjevanje izvajamo le, ce je perioda ze nastavljena
		{
			if (_lastSaveTime.secsTo(dateTime) >= _saveTimePeriod) save = true;
			else save = false;
		}
		else save = false;

	}
	else
	{
		save = true;  //ob prvem CheckSaveTime po zagonu programa vedno shranimo
	}
	
	if (save)
	{
		_lastSaveTime = dateTime;

		SaveTime(dowelGood, dowelBad, valve, length, tolLen, width, tolWidth, konus, tolKon, operater);

	
	}

	return save;

}

bool CDMSstat::FullBox(int boxNumber, float setMass, float mass, int boxDowelIn, float setLength, float setLengthTol, float setWidth, float setWidthTol, int boxBadDowels, int boxGoodDowels, QString operater)
{
	float boxAvgLength, boxAvgWidth;

	/*
	if (boxDowelIn > 0)
	{
		boxAvgLength = (float)(_boxSumLength / boxDowelIn);
		boxAvgWidth = (float)(_boxSumWidth / boxDowelIn);
	}
	else
	{
		boxAvgLength = 0;
		boxAvgWidth = 0;
	}
	*/
	//POZOR: ker pihanje izvaja Xilinx, Ale� ob ovrednotenju dobrega moznika �e ne ve, �e bo dejansko pihan
	//zato bele�im povpre�no debelino in dolzino za vse dobre moznike in predpostavim, da je pribli�no enaka povpre�ni debelini in dolzini pihanih 
	//ta predpostavka mora dokaj to�no dr�ati, �e je verjetnost pihanja dobrega moznika neodvisna od njegove debeline in dol�ine 
	if (boxGoodDowels)
	{
		boxAvgLength = (float)(_boxSumLength / boxGoodDowels);
		boxAvgWidth = (float)(_boxSumWidth / boxGoodDowels);
	}
	else
	{
		boxAvgLength = 0;
		boxAvgWidth = 0;
	}

	SaveBox(boxNumber, setMass, mass, boxDowelIn, setLength, setLengthTol, setWidth, setWidthTol, boxAvgLength, boxAvgWidth, boxBadDowels, boxGoodDowels, operater);

	ResetCurrentBox();

	return true;
}

bool CDMSstat::CheckBackupVar()
{
	if (!_napravaOn) return false;  //backup je onemogocen, kadar naprava ne dela

	QDateTime dateTime = QDateTime::currentDateTime();

	bool backup;

	if (_lastBackupBoxTime.isValid())
	{
		const int backupPeriod = 5; //perioda za varnostno shranjevanje podatkov skatle v sekundah. Priporocena vrednost je okoli 5 sekund

		if (_lastBackupBoxTime.secsTo(dateTime) >= backupPeriod) backup = true;
		else backup = false;
	}
	else
	{
		backup = true;
	}

	if (backup)
	{
		_lastBackupBoxTime = dateTime;

		BackupVariables();
	}


	return backup;
}

void CDMSstat::SetStart(int startGood, int startBad, int startValve)
{
	


	QDir dir;
	dir.setPath(_countersPath);

	if (!dir.exists())
	{
		dir.mkpath(dir.path());
	}
	

	RestoreBoxVariables();  //RestoreBoxVariables je nujno klicati le pri prvem SetStart po zagonu programa, toda zaradi enostavnosti klicem pri vsakem SetStart

	QDateTime dateTime = QDateTime::currentDateTime();

	_lastStartDateTime = dateTime;

	CDMSstat::_startGood = startGood;
	CDMSstat::_startBad = startBad;
	CDMSstat::_startValve = startValve;

	LoadPermanentCounters();

	_napravaOn = true;  
}

void CDMSstat::SetStop(int stopGood, int stopBad, int stopValve, int stopError)
{
	_napravaOn = false;

	QDateTime dateTime = QDateTime::currentDateTime();

	_beforeLastStopDateTime = _lastStopDateTime;  //v primeru firstStop se pac prepise neveljaven datum

	_lastStopDateTime = dateTime;

	int TimeOff;  //od predzadnjega stop do zadnjega start 
	int TimeOn;  //od zadnjega start do zadnjega stop

	if (_beforeLastStopDateTime.isValid() && _lastStartDateTime.isValid())
	{
		TimeOff = _beforeLastStopDateTime.secsTo(_lastStartDateTime);
	}
	else TimeOff = -1;

	if (_lastStartDateTime.isValid() && _lastStopDateTime.isValid())
	{
		TimeOn = _lastStartDateTime.secsTo(_lastStopDateTime);
	}
	else TimeOn = -1;


	SaveStop(&_lastStartDateTime.time(), _startGood, _startBad, _startValve, &_lastStopDateTime.time(), stopGood, stopBad, stopValve, TimeOff, TimeOn, stopError);

	BackupVariables();  //v SetStop je smiselno narediti varnostno kopijo variables, ker se v casu mirovanja naprave varnostne kopije ne delajo

	SavePermanentCounters(); //v SetStop je smiselno narediti varnostno kopijo stalnih stevcev, ker se v casu mirovanja naprave varnostne kopije ne delajo
}

void CDMSstat::SavePermanentCounters()
{
	_mutexPermanentCounters.lock();

	//QString fullPath = QString("C:/Data/PermanentCounters.txt");
	QString fullPath = _countersPath + QString("PermanentCounters.txt");

	QFile file(fullPath);

	if (file.open(QIODevice::ReadWrite))
	{
		QTextStream stream(&file);

		stream << _permanentCounterGood;
		stream << endl;
		stream << _permanentCounterBad;
		stream << endl;
		stream << _permanentCounterValve;
		stream << endl;

		file.close();
	}

	_mutexPermanentCounters.unlock();
}

void CDMSstat::LoadPermanentCounters()
{
	_mutexPermanentCounters.lock();

	//QString fullPath = QString("C:/Data/PermanentCounters.txt");
	QString fullPath = _countersPath + QString("PermanentCounters.txt");

	QFile file(fullPath);

	if (file.open(QIODevice::ReadOnly))
	{
		QTextStream stream(&file);

		QString line;

		line = stream.readLine();
		_permanentCounterGood = line.toULongLong();

		line = stream.readLine();
		_permanentCounterBad = line.toULongLong();

		line = stream.readLine();
		_permanentCounterValve = line.toULongLong();

		file.close();
	}
	else
	{
		_permanentCounterGood = 0;
		_permanentCounterBad = 0;
		_permanentCounterValve = 0;
	}



	_mutexPermanentCounters.unlock();

}

bool CDMSstat::CheckSavePermanentCounters()
{
	if (!_napravaOn) return false;  //shranjevanje stalnih stevcev ni potrebno, kadar naprava ne dela, ker se tedaj ne spreminjajo

	QDateTime dateTime = QDateTime::currentDateTime();

	bool save;

	if (_lastPermanentCountersSaveTime.isValid())
	{
		if (_lastPermanentCountersSaveTime.secsTo(dateTime) >= _savePermanentCountersPeriod) save = true;
		else save = false;
	}
	else
	{
		save = true;  //ob prvem CheckSavePermanentCounters po zagonu programa vedno shranimo
	}

	if (save)
	{
		_lastPermanentCountersSaveTime = dateTime;

		SavePermanentCounters();
	}
	

	return false;
}

int CDMSstat::GetPermanentGood()
{
	return _permanentCounterGood;

}

int CDMSstat::GetPermanentBad()
{
	return _permanentCounterBad;
}

int CDMSstat::GetPermanentValve()
{
	return _permanentCounterValve;
}



void CDMSstat::ResetBoxNumber()
{
	ResetCurrentBox();
}

bool CDMSstat::DowelToBox(float length, float width, bool isGood)
{
	if (isGood)
	{
		_boxSumLength += length;
		_boxSumWidth += width;

		_permanentCounterGood++;
	}
	else
	{
		_permanentCounterBad++;
	}
	
	return true;
}

bool CDMSstat::BlowDowel()
{
	_permanentCounterValve++;

	return false;
}

