﻿#pragma once
#include "CPointFloat.h"
#include  "CLine.h"

// CCircle
using namespace std;

class CCircle : public CPointFloat
{
	

public:
	CCircle();
	CCircle(CPointFloat center, float radius);
	CCircle(CPointFloat p1, CPointFloat p2, CPointFloat p3);
	CCircle(const CCircle& circle);
	

	virtual ~CCircle();
	std::vector< CPointFloat> circlePoints;
	std::vector< float> centerDistances;
	std::vector< float> averageIntensity;
	std::vector< float> averageIntensityRed;
	std::vector< float> averageIntensityGreen;
	std::vector< float> averageIntensityBlue;


public:
	int count;
	int type;
	float radius;
	float perimeter; //obseg
	float area;		//ploscina
	float minRadius; //searches for deviations on the circle - closes to center
	float maxRadius; //searches for deviations on the circle - 
	float averageRadius;

	float radiousDeviation;  //Dodal Martin: standardna deviacija radija



/*	float averageIntensity;
	float averageIntensityBlue;
	float averageIntensityRed;
	float averageIntensityGreen;*/
	int gravityIndex1;
	int gravityIndex2;

public:
	CCircle operator=(CCircle circle);
	bool operator ==(CCircle circle);
	bool operator !=(CCircle circle);
	void SetCircle(CPointFloat p1, CPointFloat p2, CPointFloat p3);
	void SetCircle(std::vector<CPointFloat> points);
	void SetCenter(QPoint center);
	void SetCenter(CPointFloat center);

	void SetRadius(float r);
	void ComputeArea();
	void ComputePerimeter();
	CPointFloat GetCenter() const;
	float GetX() const;
	float GetY() const;
	float GetRadius() const;
	float GetArea() const;
	float GetPerimeter() const;
	void GetDistancesAroundRadius(unsigned char *memoryBuffer, int radius, int width, int height, int depth, int filterBand, int F, int whiteThreshold, int blackThreshold, bool backWard);
	//void GetDistancesAroundRadius(CMemoryBuffer &memoryBuffer, int radius, int filterBand, int F, int whiteThreshold, int blackThreshold, bool backWard);	
	//float GetDistancesAroundRadius(CMemoryBuffer &memoryBuffer, int radius, int filterBand, int degree, int F, int whiteThreshold, int blackThreshold, bool backWard);	
	//float GetDistancesAroundRadiusGreen(CMemoryBuffer &memoryBuffer, int radius, int filterBand, int degree, int F, int whiteThreshold, int blackThreshold, bool backWard);
	//void GetAverageIntensity(/*CDC* pDC*/ CMemoryBuffer &memoryBuffer, int r1, int r2, int F);
	//RGBQUAD GetAverageIntensity(CDC* pDC, bool draw, CMemoryBuffer &memoryBuffer, int r1, int r2, int F, int degree);



	void Presecisce(CCircle c2, CPointFloat T[2]);

	//Napisal Martin
	//Izračuna krožnico skozi vektor točk. Zavrže trojice točk z radijem pod Rmin ali nad Rmax.
	bool SetCircleNpoints(std::vector<CPointFloat> points, float Rmin, float Rmax);

	//Napisal Martin:
	//Za točen izračun krožnice: izvede SetCircleNpoints, odstrani točke ki odstopajo od krožnice za več kot maxRerror, nato ponovi SetCircleNpoints
	//nastavi badPointsShare na delež točk, ki odstopajo od krožnice za več kot maxRerror
	//stDevRall ter stDevRgood hranita standardno deviacijo radija za vse točke in za dobre točke (napaka R pod maxRerror)
	bool SetCircleNpointsExact(std::vector<CPointFloat> points, float Rmin, float Rmax, float maxRerror, float* badPointsShare, float* stDevRall, float* stDevRgood);

	//potrebno za CalcCircleStDev
	//izračuna minimalni in maksimalni indeks točk na posameznem kotnem izseku
	void BorderIndex(vector<float>& angles, float fiCenter, float fiInterval, int borderIndex[2], bool* includesAngleZero);

	//potrebno za CalcCircleStDev
	//izračuna srednjo oddaljenost od centra na kotnem izseku, ki je definiran z mejnima indeksoma (borderIndex)
	float AvgRsector(vector<float>& distance, int borderIndex[2], bool includesAngleZero);

	///CalcCircleStDev izračuna več spremenljivk za oceno enakomernosti radija kroga
	//predvidoma se ta funkcija kliče za GetFirstEdgePointsVectorRadial in SetCircleNpoints 
	//vhod: krog (pointer this), polarni koti kroga v radianih (angles), točke kroga (circlePoints), širina kotnega izseka v radianih (fiInterval), razmerje med razdaljo med centroma sosednjih izsekov ter širino izseka (overlap) - priporočam 0.5
	//radij bi lahko izračunal iz centra kroga in circlePoints, toda bolj točno je, če v circle nastavim tistega, ki ga vrne funkcija SetCircleNpoints, ki izloči razdalje izven Rmin, Rmax
	//polarne kote bi lahko izračunal iz circle in circlePoints, toda hitreje je, če prenesemo tiste, ki jih vrne funkcija GetFirstEdgePointsVectorRadial
	//izhod: Rextreme[0, 1] in fiExtreme[0, 1] sta vrednosti minimalnega in maksimalnega radija ter ustrezni polarni kot
	//izhod: maxStDevLocal in fiMaxStDevLocal sta maksimalna vrednost standardne deviacije na izsekih in ustrezni kot, če odstopanje računam glede na povprečje razdalj na posameznem odseku
	//izhod: maxStDevLocalToGlobal in fiMaxStDevLocalToGlobal sta maksimalna vrednost standardne deviacije na izsekih in ustrezni kot, če odstopanje računam glede na srednji radij
	//izhod: stDevGlobal in fiStDevGlobal je standardna deviacija razdalje od centra na celotnem krogu
	//izhod: minSectorPoints in maxSectorPoints sta minimalno in maksimalno število točk na kotnem izseku: če je število točk 0, tega izseka ne upošteva pri izračunu max. standardne deviacije
	//ni nujno, da so točke enakomerno porazdeljene po kotih, ker točke razporedim v odseke glede na polarne kote in ne glede na indeks točke
	void CCircle::CircleStDev(vector<float>& angles, vector<CPointFloat>& circlePoints, float fiInterval, float overlap, float Rextreme[2], float fiRextreme[2], float* maxStDevLocal, float* fiMaxStDevLocal, float* maxStDevLocalToGlobal, float* fiMaxStDevLocalToGlobal, float* stDevGlobal, int* minSectorPoints, int* maxSectorPoints);






	QGraphicsEllipseItem* DrawCircle(QPen pen, QBrush brush, int crossSize);
	
};


