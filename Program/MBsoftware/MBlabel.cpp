#include "stdafx.h"
#include "MBlabel.h"


MBlabel::MBlabel()
{
}

void MBlabel::enterEvent(QEvent * ev)
{
	if(isGoodLabel == 0)
	setStyleSheet("border: 10px solid rgb(255, 0, 0);");
	else
		setStyleSheet("border: 10px solid rgb(0, 255, 0);");

}

void MBlabel::leaveEvent(QEvent * ev)
{
	if (isGoodLabel == 0)
		setStyleSheet("border: 5px solid rgb(255, 0, 0);");
	else
		setStyleSheet("border: 5px solid rgb(0, 255, 0);");
}

void MBlabel::mousePressEvent(QMouseEvent * e)
{
	emit clicked(index);
}

void MBlabel::SetIsGoodLabel(int isGood)
{
	this->isGoodLabel = isGood;
	if (isGoodLabel == 0)
		setStyleSheet("border: 5px solid rgb(255, 0, 0);");
	else
		setStyleSheet("border: 5px solid rgb(0, 255, 0);");
}
