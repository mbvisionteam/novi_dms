#pragma once

#include <QTextEdit>
#include <QtNetwork/qtcpserver.h>
#include <QtNetwork/qtcpsocket.h>
#include "LoginWindow.h"
#include "ui_MBsoftware.h"
#include "ui_MBsoftware1.h"
#include "ui_imageProcessing.h"
#include "ui_DowelSettings.h"
#include "ui_EMG.h"
#include "Camera.h"
#include "ImagingSource.h"
#include "PointGray.h"
#include "Hikrobot.h"
#include "Timer.h"
#include "imageProcessing.h"
#include "Types.h"
#include "DlgTypes.h"
#include "statusBox.h"
#include "LoginWindow.h"
#include "Measurand.h"
#include "Pcie.h"
#include "stdafx.h"
#include "CProperty.h"
#include "History.h"
#include <iostream>
#include "GeneralSettings.h"
#include "ControlLPT.h"
#include "TCP.h"
#include "LogWindow.h"
#include "AboutUS.h"
#include "XilinxZynq.h"
#include "DMSudp.h"
#include "MBError.h"
#include "AdvancedSettings.h"
#include "DowelSettings.h"
#include "DMScalibration.h"

#include "qwt_plot.h"
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <qwt_legend.h>
#include <qwt_plot_legenditem.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_zoomer.h>
#include "qcustomplot.h"
#include "axistag.h"

#include "DMSstat.h"

struct MBRange
{
	MBRange() {};
	MBRange(double start, double end) :_start(start), _end(end) {};
	MBRange(const MBRange & range) :_start(range._start), _end(range._end) {};

	double Start() { return _start; };
	double End() { return _end; };

private:
	double _start;
	double _end;
};

class MBsoftware : public QMainWindow
{
	Q_OBJECT

public:
	void closeEvent(QCloseEvent * event);
	MBsoftware(QWidget *parent = Q_NULLPTR);


//Deklaracija globalnih spremenljivk
public:
	QWidget	* EMGwindow;
	QTimer * OnTimer;
	QTimer	*OnTimer2;
	static MBsoftware*						glob;
	static std::vector<CCamera*>			cam;
	static std::vector<CMemoryBuffer*>		images;

	static std::vector<Pcie*>				pcieDio;

	static std::vector<ControlLPT*>			lpt;
	static std::vector<TCP*>				tcp;
	static Timer							MMTimer;
	static Timer							viewTimerTimer;
	static std::vector<Timer*>				TestFun;
	static std::vector<StatusBox*>			statusBox;
	static std::vector<LogWindow*>			log;
	static AboutUS*							about;
	static int								mmCounter;
	static int								viewCounter;
	static DMSudp*							dmsUDP;
	static LoginWindow*						login;
	static GeneralSettings*					settings;
	static imageProcessing*					imageProcess;
	static Measurand						measureObject;
	static QTcpSocket*						client;
	static std::vector<XilinxZynq*>			uZed;
	//4 niti za 4 slike
	static QFuture<int>					future[4];

	static vector<int>						clearLogIndex;
	static vector<int>						logIndex;
	static vector<QString>					logText;
	static QString							referencePath;//pot do datotek kjer se nahajajo vse spremenljivke programa


	static QReadWriteLock					lockReadWrite;

	static std::vector<Types*>		types[NR_STATIONS];
	static History*					history;
	static DowelSettings*			dowelSettingsDlg;
	static DMScalibration*			dmsCalibrationDlg;

	QTableView  *parametersTable;
	QStandardItemModel* modelTable;
	vector<vector<QStandardItem*>>  standardTableItem;

	static int currentType;
	static int prevType;
	 QGraphicsScene * showScene[NR_STATIONS];
	 QGraphicsScene * scenePresort;
	 QGraphicsScene * scenePresortLive;

	QGraphicsPixmapItem*	pixmapVideo[NR_STATIONS];
	virtual void paintEvent(QPaintEvent * event);
	bool isSceneAdded;



	//za izris moznika na sliki
	QGraphicsPolygonItem* area[11];
	QGraphicsTextItem*	maxErrorText[6];
	QGraphicsTextItem*  negativeErrorText[6];
	QGraphicsTextItem* nrMeasurementesText[6];
	QGraphicsTextItem* speedMeasurementText;
	QGraphicsTextItem* lenghtMeasurementText;
	QGraphicsTextItem* maxMeasurementText;
	QGraphicsTextItem* elipseText;

//	QWidget  *EmgPopUp;
	

	//Grafi
	//Glavni CustomPlot prikaz
	QCustomPlot * customPlot[4];
	//Grafi ki jih dodajamo v glavni cCustomPlot prikaz
	QCPBars * barPlotGood[4];
	QCPBars * barPlotBad[4];
	QCPGraph * upperToleranceGraph[4];
	QCPGraph * lowerToleranceGraph[4];
	AxisTag *mTagUpper[4];
	AxisTag *mTagLower[4];

	//Grafi
//Glavni CustomPlot prikaz
	QCustomPlot * plotGauss[2];
	//Grafi ki jih dodajamo v glavni cCustomPlot prikaz
	QCPBars * barPlotGauss[2];
	QCPGraph * upperToleranceGraphGauss[2];
	QCPGraph * lowerToleranceGraphGauss[2];
	AxisTag *mTagUpperGauss[2];
	AxisTag *mTagLowerGauss[2];

private:
	//Ui::MBsoftwareClass ui;
	Ui::MBsoftwareClass1 ui;
	Ui::EMGui uiEMG;


	bool slovensko;

protected:
	static void __stdcall OnMultimediaTimer(UINT uTimerID, UINT, DWORD_PTR  dwUser, DWORD_PTR  dw1, DWORD_PTR  dw2);

	void keyPressEvent(QKeyEvent * event) ;
	QCustomPlot * SetParentQCP(QCustomPlot * parentWidget, QString title, QString xAxisLabel, QString yAxisLabel, MBRange xRange, MBRange yRange, bool setLegend);
	QCPBars * CreateBarPlot(QCustomPlot * parentQCP, QString title, QPen pen, QBrush brush);
	QCPGraph * CreateGraphPlot(QCustomPlot * parentQCP, QString title, QPen pen, QBrush brush);

	void OnLoadBarChartData(int i);
	void OnUpdateBarChartData(int i,int currDowel);
	void OnUpdateGaussChartData(int i, int currDowel);
	void mouseDoubleClickEvent(QMouseEvent * e);

	void OnUpdateStatistics();
	void OnResetStatistics();
	void OnResetTimeCounters();
	void OnResetBoxes();
	void OnUpdateServiceTab();
	void OnCalculateCounters();

public:
	//izgled aplikacije
	void CreateCameraTab();
	void CreateSignalsTab();
	void CreateMeasurementsTable();
	void ReadTypes();
//	void ReadAdvancedSettings();
	void ReadCameraInit();
	void ShowErrorMessage();
	void ClearErrorMessage(int index);
	void AppendClearErrorMessageIndex(int index);
	void DrawImageOnScreen();
	void UpdateImageView(int station);
	void CopyThread(int station);
	void CreateDowelImage();


	void PopulateStatusBox();
	void EnableEMGdialog(bool enable);

	void ValveTestFunction();

	void SetLanguage();

private slots:
	void UpdateDowelImage(int currPiece);
	void DrawMeasurements();
	void ViewTimer(); //timer za izris zaslona
	void GetError();
	void OnFrameReady(int, int); //frameready signal se izvede kadar je nova slika pripravljena
	void OnTcpDataReady(QString conn,int parse); //frameready signal se izvede kadar so podatki na TCp
	void HistoryCall(int station, int currentPiece); //iz zgodovine so priklicane slike za izbran kos
	
	void OnClickedShowLptButton(int);
	void OnClickedShowLptButton0();
	void OnClickedShowLptButton1();
	void OnClickedShowCamButton0();
	void OnClickedShowCamButton1();
	void OnClickedShowUZedButton(int);
	void OnClickedShowCamButton(int);
	void OnClickedShowSmartCardButton(int);
	void OnClickedShowImageProcessing(void);
	void OnClickedShowDowelSettings(void); //prikaz za izbiro tipa moznika
	void OnClickedShowDMScalibration(void);//prikaz dialoga za kalibracijo DMS ja
	void OnClickedShowPresortDialog(void);
	void OnClickedSelectType(void);//prikaz dialoga za izbiro drugega tipa
	void OnClickedEditTypes(void);
	void OnClickedRemoveTypesOld(void);
	void OnClickedResetGlobal();
	//prikaz dialoga za urejenje toleranc
	void OnClickedRemoveTypes(void);
	void OnClickedAddTypes(void);
	void OnSignalCreateTypeDMS(QString exsisting, QString newName);
	void CopyType(QString exsisting, QString newName);
	void OnClickedTypeSettings(void);
	void OnClickedResetCounters(void);
	void OnClickedLogin(void);
	void OnClickedStatusBox(void);
	void OnClickedSmcButton(void);
	void OnClickedShowHistory(void);
	void OnClickedGeneralSettings(void);
	void OnClickedAboutUs();

	void onClientReadyRead();
	void OnClickedTCPconnction(int);
	void OnClickedEMGReset();
	void SaveVariables();	//spremenljivke se shranijo ob spremembi ali ob zaprtju programa //currentType!!!
	void LoadVariables();

	void SaveVariablesStatistics();	//spremenljivke se shranijo ob spremembi ali ob zaprtju programa //currentType!!!
	void LoadVariablesStatistics();

	void CalculateValveTime();
	//type:
	//0-> status OK
	//1-> napaka na napravi
	//2-> opozorilo rumena
	//3-> info modra
	void AddLog(QString text, int type);
	void EraseLastLOG();

	void MeasurePiece(int index);
	void MeasurePiece(int station, int index);
	void UpdateHistory(int station, int index);
	void SetHistory();
	void WriteLogToFile(QString path, QString text, int type);


	//dms
	void OnClickedDMS(void);
	void OnClickedDMStransitionTest(void);
	void SetTolerance(int currentType);
	void SetToleranceAutoCalibrationMode();
	void OnSingnalSelectedType(int selectedType);
	void OnSignalAdvSettingsSaved(QString selected);
	void OnSignalEnterCalibrationMode();
	void OnSignalLeaveCalibrationMode();
	signals:
	void	measurePieceSignal(int currentPiece);


};
