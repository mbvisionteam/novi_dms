﻿#include "stdafx.h"
#include "MBsoftware.h"


//prvi test commit za hrast vijake

//include za media timer ( multimedia timer )
#include<mmsystem.h>
#pragma comment(lib, "winmm.lib")
     
MBsoftware*									MBsoftware::glob;
std::vector<CCamera*>						MBsoftware::cam;
std::vector<CMemoryBuffer*>					MBsoftware::images;
DMSudp*										MBsoftware::dmsUDP;
std::vector<Pcie*>							MBsoftware::pcieDio;
std::vector<Types*>							MBsoftware::types[NR_STATIONS];

std::vector<ControlLPT*>					MBsoftware::lpt;
std::vector<TCP*>							MBsoftware::tcp;
std::vector<XilinxZynq*>					MBsoftware::uZed;
History*									MBsoftware::history;
std::vector<StatusBox*>						MBsoftware::statusBox;
std::vector<LogWindow*>						MBsoftware::log;
Timer										MBsoftware::MMTimer;
Timer										MBsoftware::viewTimerTimer;
std::vector<Timer*>							MBsoftware::TestFun;
int											MBsoftware::mmCounter;
int											MBsoftware::viewCounter;
imageProcessing*							MBsoftware::imageProcess;
LoginWindow*								MBsoftware::login;
AboutUS*									MBsoftware::about;
QString										MBsoftware::referencePath;
int											MBsoftware::currentType = 0;
int											MBsoftware::prevType = 0;


Measurand									MBsoftware::measureObject; //object with mesurand parameters
GeneralSettings*							MBsoftware::settings; //generalSettings


QTcpSocket*									MBsoftware::client;
QFuture<int>								MBsoftware::future[4];

vector<int>									MBsoftware::clearLogIndex;
vector<int>									MBsoftware::logIndex;
vector<QString>								MBsoftware::logText;
QReadWriteLock								MBsoftware::lockReadWrite;
DowelSettings*								MBsoftware::dowelSettingsDlg;
DMScalibration*								MBsoftware::dmsCalibrationDlg;

MMRESULT FTimerID;

QString logPath;
QFile* m_logFile;

int processStatus = 0;

int startKey = 0;

int startErrorSet[20];
int wd = 0;

int updateImage[2];
int stationStatus[NR_STATIONS];

int delaySendDowel[DMSHISTORYSIZE];


float timeDelay[10000];
int startDelay = 0;
int stopDelay = 0;
Timer delayTimer;
float maxDelay = 0;
float  minDelay = 1000;
float stdDelay;
float avr = 0;
float avrCounter = 0;

int timeCounter = 0;
std::chrono::microseconds nanoSecu[1000];

int nrMeritve = 0;
int testDowelNr = 0;

std::chrono::high_resolution_clock::time_point time_diff[2];
bool myFirstTime = true;

int selectedCamera = 1;
int showPiece = 0;
int selectedMeausurement = 0;
int uploadDowel = 0;
int updateMeasurements = true;

int pieceOn = 0;//piece On za zaznavo kosa za testno kamero
int newIndexCamera = 0;

int vf0 = 0, vf1 = 0, vf2 = 0;
QString vfSettingsText;
int tmpCounerValueBlown = 0;

int testImageIndex[20];


int presortTime1;
int presortTime2;

float presortSensorSpeedUsed = 0;
float presortSensorTimeToBlow = 0;
float presortSensorLenghtUsed = 0;

int presortOn = 0;
int filterPresortOn = 0;
int presortSensorDelay = 0;
float presortSpeed = 0;
float presortLenght = 0;
//bool presortSensorArrayStart[20]; //
//bool presortSensorArrayStop[20]; //
int indexPresortDowelStart = 0;
int indexPresortDowelStop = 0;
Timer speedPresortSensorTimer[20];
int isProcessedPresortSensor[20][2];

Timer speedTimerPresort;
Timer lenghtTimerPresort;

int isGoodPresort = 0;
int presortCameraBlow = 0;
float presortCameraLenght = 0;
float presortCameraSpeed = 2;
float presortProcessingTIme = 0;
int centerPresort;
float korFactorPresort = 0.26;
int presortCounterBadStat = 0;
int presortCounterBadinLine = 0;

 


int presortSensorTimeStamp[0xffff+1];
int presortCameraTimeStamp[0xffff + 1];
vector<int> tmpSaveIndex;


int updateStatusRect = 0;
int updateDraw = 0;


//delovaje storja
int rotatorSpeedPrevValue;

int elevatorStatus = 0;
int elevatorCounter = 0;
bool rotatorOn = false;
int rotatorSenCounter = 0;
int dowelOnCounter = 0;
int dowelStuckCounter = 0;
int dowelStuckSen2Counter = 0;
int alarmStatus; //v primeru zaustavitve naprav 
int warningStatus; //samo opozorila 
int warningStatusPrev = 0;
int lightStatus;
int lightOn = 0;

int presureSensorCounter = 0;

int emgStop = 0;
int emgStopPrev = 0;



//test valve 
int testValveX = -1;
int testValveC = -1;//ciscenje dms luci 
int testValveV = -1; //glavni ventil ukaz za posiljanje v xilinx 
int testValveVOn = -1;
int testValveY = -1;
int testValveB = -1;//pihanje ciscenje predsortiranje 
int testValveDuration = 1500;


//ciscenje spremenljivke
int cleaningMode = 0; //ce je clean mode 1 je zacetno ciscenje brez zacetnih delajev
int cleanPeriodeCounter  = 0;
int cleanCounterOffset = 0;
int clean1Finished = false;
int clean2Finished = false;
int timeToFinishCleaning = 0;

int startPresortCamera = 0;

//stevci za stetje kosov na minuto
int goodCounterLastValue = 0;
int badCounterLastValue = 0;
int allCounterLastValue = 0;
int goodCounterValue[4] = { 0,0,0,0 };
int badCounterValue[4] = { 0,0,0,0 };
int allCounterValue[4] = { 0,0,0,0 };
vector<int> allPerHourV;
int counterValue = 0;
int goodPerMin = 0;
int badPerMin = 0;
int allPerMin = 0;
int allPerHour = 0;

int deviceStatus = 0; //podatki za izpis na glavnem zaslonu pr


//statistika za slabe in slab procent
int badOnDifferentCounter[10];
int badpresortColor = 0;
int badpresrotSmall = 0;
int badpresortSensor = 0;
int badPresortType = 0;
int presortPieceCount = 0;


int autoCalibrationMode = 0; //ce je naprava v nacinu za avtokalibracijo
int updateLogWindow = 0; //posodobi log v viewtimerju


//
int dmsModulErrorCounter = 0;//kadar je lasek na katerikoli izmed se mora naprava ustaviti, po vec kot 2s
int firstTransition[6];
int firstTransitionPrev[6];
int timeToValvePrev = 0;
int valveDurationPrev = 0;

int workingModePresortPrev = 0;
int boxFullStat = 0;
int boxFullCounter = 0;

int presortDelay = 0;
int presortTime = 0;
int presortSpeedCounter = 0;
float presortSpeedAvrage = 0;
float presortCameraSpeedUsed = 0;
//za servis tab 
int inputCounter[8] = { 0,0,0,0,0,0,0,0 };


//testiranje multimedei
float timeDur[10000];
int timeDurCounter;
void MBsoftware::closeEvent(QCloseEvent *event)
{
	
	SaveVariables();
	SaveVariablesStatistics();
	disconnect(this, 0, 0, 0);

	//first you have to close all timers
	timeKillEvent(FTimerID);
	timeEndPeriod(1);
	OnTimer->stop();

	for (int i = 0; i < cam.size(); i++)
	{
		cam[i]->DisableLive();
		delete (cam[i]);
	}
	
	for (int i = 0; i < images.size(); i++)
	{
		delete (images[i]);
	}
	for (int i = 0; i < TestFun.size(); i++)
	{
		delete (TestFun[i]);
	}
	//lpt.clear();
	for (int i = 0; i < lpt.size(); i++)
	{
		lpt[i]->SetControlByteAllOFF();
		lpt[i]->SetDataByte(0x00);
		lpt[i]->close();

		//delete (lpt[i]);
	}
	lpt.clear();


	for (int i = 0; i < statusBox.size(); i++)
	{
		delete (statusBox[i]);
	}

	for (int i = 0; i < pcieDio.size(); i++)
	{
		delete (pcieDio[i]);
	}

	for (int i = 0; i < uZed.size(); i++)
	{
		delete (uZed[i]);
	}
	for (int i = 0; i < tcp.size(); i++)
	{
		delete (tcp[i]);
	}

	for (int i = 0; i < NR_STATIONS; i++) //izbrisemo kar je bilo prikazano na zaslonu 
	{
		qDeleteAll(showScene[i]->items());
		delete showScene[i];

	}
	qDeleteAll(scenePresort->items());
	delete scenePresort;

	//showScene.clear();
	for (int i = 0; i < NR_STATIONS; i++)
	{
		delete parametersTable;
		for (int j = 0; j < types[i].size(); j++)
		{
			delete types[i][j];
				
		}
	}

	/*for (int j = 0; j < advSettings.size(); j++)
	{
		delete advSettings[j];

	}*/

	for (int i = 0; i < log.size(); i++)
	{
		delete log[i];
	}
	log.clear();

	if (slovensko) AddLog("Program ustavljen", 3);
	else AddLog("Application stoped", 3);

	logText.clear();
	logIndex.clear();

	delete history;
	delete imageProcess;
	delete login;
	delete settings;
	delete about;
	delete dmsCalibrationDlg;
	delete dowelSettingsDlg;
	
}

MBsoftware::MBsoftware(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);



	QDir referenceDirectory = QDir::currentPath();
	referenceDirectory.cdUp();//Uporablejno pri RLS, da pridem v en nivo nizjo datoteko  projekt/
	referenceDirectory.cdUp();//Uporablejno pri RLS, da pridem v en nivo nizjo datoteko  projekt/
	referencePath = referenceDirectory.path();

	glob = this;
	slovensko = SLOVENSKO;
	SetLanguage();

	QThread::currentThread()->setPriority(QThread::HighPriority);
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);

	
	ReadTypes();
	

	
	
	LoadVariables();
	LoadVariablesStatistics();

	ReadCameraInit();

	measureObject.selectedCalibration = round(types[0][currentType]->typeDowelSetting[3]);

	about = new AboutUS();
	login = new LoginWindow();
	settings = new GeneralSettings(referencePath);
	rotatorSpeedPrevValue = settings->rotatorSpeedMode;
	dowelSettingsDlg = new DowelSettings(referencePath);
	dmsCalibrationDlg = new DMScalibration(referencePath);
	statusBox.push_back(new StatusBox);
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());


#if defined _DEBUG
	login->currentRights = 1;
	login->currentUser = "Administrator";
#else
	login->currentRights = 0;
	login->currentUser = "";
#endif

	QString logPathTmp = referencePath + QString("/%1/Log").arg(REF_FOLDER_NAME);
	if (!QDir(logPathTmp).exists())
		QDir().mkdir(logPathTmp);

	//if (!QDir("C:/log/").exists())
	//	QDir().mkdir("C:/log");
	//logPath = "C:/log/workingLog_" + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
	logPath = QString("%1/workingLog_").arg(logPathTmp) + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
	m_logFile = new QFile();
	m_logFile->setFileName(logPath);
	m_logFile->open(QIODevice::Append | QIODevice::Text);

	if (slovensko) AddLog("Program pognan!", 3);
	else AddLog("Application STARTED!", 3);

	isSceneAdded = false;

	for (int i = 0; i < 20; i++)
	{
		startErrorSet[i] = 0;
	}

	dmsUDP = new DMSudp("172.16.10.54", 7, referencePath);


	
	lpt.push_back(new ControlLPT(0x0378, false,referencePath));
	lpt.push_back(new ControlLPT(0xDFF8, false, referencePath));



	for (int i = 0; i < 100; i++)
	{
		images.push_back(new CMemoryBuffer(640, 480, 3));
	}


	imageProcess = new imageProcessing(referencePath);
	imageProcess->ConnectImages(cam, images);
	imageProcess->currentType = currentType;
	
	dmsUDP->ConnectMeasurand(&measureObject);
	dmsCalibrationDlg->ConnectMeasurand(&measureObject);


	for (int i = 0; i < NR_STATIONS; i++)
	{
		imageProcess->ConnectTypes(i,types[i]);

	}
	//imageProcess->LoadTypesProperty();//nalozimo nastavitve funkcij za vse tipe
	for (int i = 0; i < types[0].size(); i++)
	{
		imageProcess->LoadTypesProperty(i);//nalozimo nastavitve funkcij za vse tipe
	}
	imageProcess->ConnectMeasurand(&measureObject);
	imageProcess->SelectAdvancedPresortSettings();
	//timer za viewDisplay
	OnTimer = new QTimer(this);

	//dinamično dodajanje ikon v tabih
	//CreateCameraTab(); /pri DMS-ju ne uporabljanm
	//CreateSignalsTab(); Pri DMS-ju prototip trenutno ne potrebujem
	CreateMeasurementsTable();
	EnableEMGdialog(true);
	
	//Povezave SIGNAL-SLOT
	connect(OnTimer, SIGNAL(timeout()), this, SLOT(ViewTimer()));


	for (int i = 0; i < cam.size(); i++)
	{
		connect(cam[i], SIGNAL(frameReadySignal(int, int)), this, SLOT(OnFrameReady(int, int)));
	}
	connect(imageProcess, SIGNAL(measurePieceSignal(int, int)), this, SLOT(MeasurePiece(int, int)));
	//connect(imageProcess, SIGNAL(imagesReady()), this, SLOT(ProcessImages()));


	connect(ui.buttonImageProcessing, SIGNAL(triggered()), this, SLOT(OnClickedShowImageProcessing()));
	//connect(ui.buttonTypeSelect, SIGNAL(pressed()), this, SLOT(OnClickedSelectType()));
	//connect(ui.buttonSetTolerance, SIGNAL(pressed()), this, SLOT(OnClickedEditTypes()));
	//connect(ui.buttonAddType, SIGNAL(pressed()), this, SLOT(OnClickedAddTypes()));
	//connect(ui.buttonRemoveType, SIGNAL(pressed()), this, SLOT(OnClickedRemoveTypes()));
	//connect(ui.buttonTypeSettings, SIGNAL(pressed()), this, SLOT(OnClickedTypeSettings()));
	connect(ui.buttonResetCounters, SIGNAL(triggered()), this, SLOT(OnClickedResetCounters()));
	connect(ui.buttonResetAll, SIGNAL(triggered()), this, SLOT(OnClickedResetGlobal()));
	connect(ui.buttonLogin, SIGNAL(triggered()), this, SLOT(OnClickedLogin()));
	connect(ui.buttonStatusBox, SIGNAL(triggered()), this, SLOT(OnClickedStatusBox()));
	connect(ui.buttonDowelSelect, SIGNAL(triggered()), this, SLOT(OnClickedShowDowelSettings()));
	connect(ui.buttonCalibrationSettings, SIGNAL(triggered()), this, SLOT(OnClickedShowDMScalibration()));
	connect(ui.buttonPresortSettings, SIGNAL(triggered()), this, SLOT(OnClickedShowPresortDialog()));
	//connect(ui.buttonHistory, SIGNAL(pressed()), this, SLOT(OnClickedShowHistory()));
	connect(ui.buttonGeneralSettings, SIGNAL(triggered()), this, SLOT(OnClickedGeneralSettings()));
	connect(ui.buttonAboutUs, SIGNAL(triggered()), this, SLOT(OnClickedAboutUs()));

		connect(ui.buttonDMS, SIGNAL(triggered()), this, SLOT(OnClickedDMS()));
	connect(ui.buttonDMStransitionTest, SIGNAL(triggered()), this, SLOT(OnClickedDMStransitionTest()));

	connect(this, SIGNAL(measurePieceSignal(int)), this, SLOT(UpdateDowelImage(int)));
	connect(dowelSettingsDlg, SIGNAL(newTypeSelected(int)), this, SLOT(OnSingnalSelectedType(int)));
	connect(dowelSettingsDlg, SIGNAL(advancedSettingsSaved(QString)), this, SLOT(OnSignalAdvSettingsSaved(QString)));
	connect(dowelSettingsDlg, SIGNAL(createNewTypeSignal(QString, QString)), this, SLOT(OnSignalCreateTypeDMS(QString,QString)));
	
	
	connect(dmsCalibrationDlg, SIGNAL(enterAutoCalibration()), this, SLOT(OnSignalEnterCalibrationMode()));
	connect(dmsCalibrationDlg, SIGNAL(leaveAutoCalibration()), this, SLOT(OnSignalLeaveCalibrationMode()));

	connect(ui.actionLPT_0, SIGNAL(triggered()), this, SLOT(OnClickedShowLptButton0()));
	connect(ui.actionLPT_1, SIGNAL(triggered()), this, SLOT(OnClickedShowLptButton1()));
	connect(ui.actionArea_Camera_0, SIGNAL(triggered()), this, SLOT(OnClickedShowCamButton0()));
	connect(ui.actionArea_Camera_1, SIGNAL(triggered()), this, SLOT(OnClickedShowCamButton1()));
	

	for (int i = 0; i < 100; i++)
	{
		ui.listWidgetStatus->addItem("");
	}
	grabKeyboard();

	
	DrawImageOnScreen();
	CreateDowelImage();
	//history = new History();
	//SetHistory();
	//connect(history, SIGNAL(callFromHistory(int , int)), this, SLOT(HistoryCall(int ,int)));




	MBRange xAxisRange(0, 50);
	MBRange yAxisRange(0, 100);
	const QFont timesNewRomanFont("Times New Roman", 12, QFont::Bold);
	QPen novPen = QPen(QColor(Qt::black), 2, Qt::NoPen);
	//Nastavimo glavni prikaz CustomPlot (imena osi, naslovi, legenda)
	for (int i = 0; i < 4; i++)
	{
		if(i == 0)
			customPlot[i] = SetParentQCP(ui.plotLenght, "", "", "LENGHT", xAxisRange, yAxisRange, false);
		else if (i == 1)
			customPlot[i] = SetParentQCP(ui.plotWidth, "", "", "AVR. WIDTH", xAxisRange, yAxisRange, false);
		else if (i == 2)
			customPlot[i] = SetParentQCP(ui.plotWidthMax, "", "", "MAX. WIDTH", xAxisRange, yAxisRange, false);
		else if (i == 3)
			customPlot[i] = SetParentQCP(ui.plotWidthMin, "", "", "MIN. WIDTH", xAxisRange, yAxisRange, false);



		customPlot[i]->yAxis->setVisible(true);
		customPlot[i]->yAxis->setTicks(false);
		customPlot[i]->yAxis2->setVisible(true);
		customPlot[i]->yAxis2->setTicks(false);

		customPlot[i]->xAxis->setVisible(true);
	

		customPlot[i]->axisRect()->axis(QCPAxis::atRight, 0)->setPadding(60); // add some padding to have space for tags

		//Ustvarimo objekte za prikaz meritev
		barPlotGood[i] = CreateBarPlot(customPlot[i], "Measurement", QPen(QPen(QColor(0, 255, 0, 185).lighter(65))), QBrush(QBrush(QColor(0, 255, 0, 185).lighter(65))));
		barPlotBad[i] = CreateBarPlot(customPlot[i], "Measurement", QPen(QPen(QColor(255, 0, 0, 185).lighter(65))), QBrush(QBrush(QColor(255, 0, 0, 185).lighter(65))));
		//Ustvarimo objekte za prikaz toleranc
		upperToleranceGraph[i] = CreateGraphPlot(customPlot[i], "Upper tolerance", QPen(QColor(Qt::black), 2, Qt::DashDotDotLine), QBrush(QColor(0, 0, 0, 0)));
		lowerToleranceGraph[i] = CreateGraphPlot(customPlot[i], "Lower tolerance", QPen(QColor(Qt::black), 2, Qt::DashDotDotLine), QBrush(QColor(0, 0, 0, 0)));
		
		
	
		
		mTagUpper[i] = new AxisTag(lowerToleranceGraph[i]->valueAxis());
		mTagLower[i] = new AxisTag(upperToleranceGraph[i]->valueAxis());
		mTagUpper[i]->setPen(novPen);
		mTagUpper[i]->setFont(timesNewRomanFont);
		mTagLower[i]->setPen(novPen);
		mTagLower[i]->setFont(timesNewRomanFont);

		OnLoadBarChartData(i);

		mTagUpper[i]->setText(QString("78.33"));
		mTagUpper[i]->updatePosition(78);
		mTagLower[i]->setText(QString("35.56"));
		mTagLower[i]->updatePosition(35);
		customPlot[i]->replot();
	}
	for (int i = 0; i < 2; i++)  /////////////////////////gausova distribucija
	{
		if(i == 0)
		plotGauss[i] = SetParentQCP(ui.plotGauss1, "", "", "LENGHT", xAxisRange, yAxisRange, false);
		else
		plotGauss[i] = SetParentQCP(ui.plotGauss2, "", "", "WIDTH", xAxisRange, yAxisRange, false);

		barPlotGauss[i] = CreateBarPlot(plotGauss[i], "Measurement", QPen(QPen(QColor(0, 255, 0, 185).lighter(65))), QBrush(QBrush(QColor(0, 255, 0, 185).lighter(65))));

		upperToleranceGraphGauss[i] = CreateGraphPlot(plotGauss[i], "Upper tolerance", QPen(QColor(Qt::black), 2, Qt::DashDotDotLine), QBrush(QColor(0, 0, 0, 0)));
		lowerToleranceGraphGauss[i] = CreateGraphPlot(plotGauss[i], "Lower tolerance", QPen(QColor(Qt::black), 2, Qt::DashDotDotLine), QBrush(QColor(0, 0, 0, 0)));
		
		plotGauss[i]->yAxis->setVisible(true);
		plotGauss[i]->yAxis->setTicks(false);
		plotGauss[i]->xAxis->setVisible(true);
		plotGauss[i]->xAxis->setTicks(true);

		plotGauss[i]->replot();

	}
	
	for (int i = 0; i < 50; i++)
	{
		ui.statusList->addItem("");
	}

	lpt[1]->SetOutputValue(0, true);


	for (int i = 0; i < DMSHISTORYSIZE; i++)
	{
		measureObject.blowImageCounter[i] = 30;
	}
	

	//samo za test
	QByteArray tmp;
	//QString filename = "C:/data/testData" + QString("/DOWEL_%1.txt").arg(uploadDowel);
	QString filename = referencePath + QString("/reference/data/testData/DOWEL_%1.txt").arg(uploadDowel);
	QFile file(filename);
	file.open(QIODevice::ReadOnly);
	tmp = file.readAll();
	file.close();
	QString line;
	dmsUDP->tmpDowelData = tmp;
	//dmsUDP->ReadDowelString(tmp);

	QString path;

	//path = QDir::currentPath() + "/res/dowel5.bmp";
	//images[0]->LoadBuffer(path);

	path =referencePath + "/reference/cam1Image.bmp";
	cam[1]->image[0].LoadBuffer(path);

	path = referencePath + "/reference/presortDolg.bmp";
	images[0]->LoadBuffer(path);

	CreateDowelImage();
	


	cam[1]->EnableLive();
	
	
	cam[0]->EnableLive();


	for (int i = 0; i < 0xffff; i++)
	{
		presortSensorTimeStamp[i] = 0;
		presortCameraTimeStamp[i] = 0;
	}
	ui.imageViewMainWindow_0->setViewport(new QOpenGLWidget());
	ui.imageViewPresort->setViewport(new QOpenGLWidget());
	ui.imageViewPresortLive->setViewport(new QOpenGLWidget());
	//ui.imageViewMainWindow_0->setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering))



	goodCounterLastValue = types[0][currentType]->goodMeasurementsCounter;
	badCounterLastValue = types[0][currentType]->badMeasurementsCounter;
	allCounterLastValue = types[0][currentType]->totalMeasurementsCounter;
	goodPerMin = 0;
	badPerMin = 0;
	allPerMin = 0;

	SetTolerance(currentType);
	updateStatusRect = 1;
	settings->updateDraw = 1;
	for (int i = 0; i < 20; i++)
	{
		 isProcessedPresortSensor[i][0] = -1;
		 isProcessedPresortSensor[i][1] = -1;
	}


	CDMSstat::Init(); //statistika DMS
	CDMSstat::SetTimePeriod(120);
	////		MULTIMEDIA	TIMER		////
	UINT uDelay = 1;				// 1 = 1000Hz, 10 = 100Hz
	UINT uResolution = 1;
	DWORD dwUser = NULL;
	UINT fuEvent = TIME_PERIODIC;

	// timeBeginPeriod(1);
	FTimerID = timeSetEvent(uDelay, uResolution, OnMultimediaTimer, dwUser, fuEvent);
	OnTimer->start(100);
	ui.checkInput0->setText(lpt[0]->status[3].name);
	ui.checkInput1->setText(lpt[0]->status[2].name);
	ui.checkInput2->setText(lpt[0]->status[1].name);
	ui.checkInput3->setText(lpt[1]->status[0].name);
	ui.checkInput4->setText(lpt[1]->status[4].name);
	ui.checkInput5->setText(lpt[1]->status[3].name);
	ui.checkInput6->setText(lpt[1]->status[2].name);
	ui.checkInput7->setText(lpt[0]->status[4].name);

	ui.checkOutput0->setText(lpt[0]->data[0].name);
	ui.checkOutput1->setText(lpt[0]->data[1].name);
	ui.checkOutput2->setText(lpt[0]->data[2].name);
	ui.checkOutput3->setText(lpt[0]->data[3].name);
	ui.checkOutput4->setText(lpt[0]->data[4].name);
	ui.checkOutput5->setText(lpt[0]->data[5].name);
	ui.checkOutput6->setText(lpt[0]->data[6].name);
	ui.checkOutput7->setText(lpt[0]->data[7].name);
	ui.checkOutput8->setText(lpt[1]->data[0].name);
	ui.checkOutput9->setText(lpt[1]->data[1].name);
	ui.checkOutput10->setText(lpt[1]->data[2].name);
	ui.checkOutput11->setText(lpt[1]->data[3].name);
	ui.checkOutput12->setText(lpt[1]->data[4].name);
	ui.checkOutput13->setText(lpt[1]->data[5].name);
	ui.checkOutput14->setText(lpt[1]->data[6].name);
	ui.checkOutput15->setText(lpt[1]->data[7].name);



}

void MBsoftware::OnFrameReady(int camera, int imageIndex)
{
	if (camera == 1)
	{
		if(imageIndex > 0)
		{
			int vl = 100;
		}

	}
}

void MBsoftware::OnTcpDataReady(QString conn, int parse)
{
	int value;
	char request;
	if (conn == "EPSON")
	{
		for (int i = 0; i < parse; i++)
		{
			request = tcp[0]->returnCharArray[i];
			value = tcp[0]->returnIntArray[i];


		}
	}
	

}

void MBsoftware::HistoryCall(int station, int currentPiece)
{

	if (currentPiece >= 0)
	{
		if (station == 1)
		{
			*cam[station]->image[0].buffer = history->image[0][station][currentPiece].clone();
			*cam[station]->image[1].buffer = history->image[1][station][currentPiece].clone();
		}
		else
		{
			*cam[station]->image[0].buffer = history->image[0][station][currentPiece].clone();
		}
	}

}

void MBsoftware::MeasurePiece(int index)//tukaj izmerimo moznik in ga posljemo 
{

	TestFun[0]->SetStart();
	
	int isGoodNrMeasurements[5] = { 1,1,1,1,1 };
	int isGoodLenght = 1;
	int isGoodElipse = 1;
	measureObject.isGoodArray[index];

	//za povpparameter na konusu izracunam dinamicne tolerance
	float avrZAC = 0;
	float avrEND = 0;
	float avrMID = 0;
	float avrMax = 0;


	for (int i = 1; i < 6; i++)
	{
		avrZAC += measureObject.measurementDIS[index][i];
		avrEND += measureObject.measurementDIE[index][i];
		avrMID += measureObject.measurementDIC[index][i];

		if (measureObject.measurementDIC[index][i] > avrMax)
			avrMax = measureObject.measurementDIC[index][i];
	}
		avrZAC /= 5;
		avrEND /= 5;
		avrMID /= 5;

		types[0][currentType]->toleranceHigh[0] = avrZAC - measureObject.setDSKDEKoffset;
		types[0][currentType]->toleranceLow[0] = 0;
		types[0][currentType]->toleranceHigh[4] = avrEND - measureObject.setDSKDEKoffset;
		types[0][currentType]->toleranceLow[4] = 0;

		for (int i = 1; i < 6; i++)
		{
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDSK[index][i], i - 1, 0);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDIS[index][i], i - 1, 1);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDIC[index][i], i - 1, 2);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDIE[index][i], i - 1, 3);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDKE[index][i], i - 1, 4);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementAngleAn[index][i][0], i - 1, 5);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementAngleAn[index][i][1], i - 1, 6);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementAngleAn[index][i][2], i - 1, 7);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementAngleAn[index][i][3], i - 1, 8);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDistanceDel[index][i][0], i - 1, 9);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDistanceDel[index][i][1], i - 1, 10);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDistanceDel[index][i][2], i - 1, 11);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementDistanceDel[index][i][3], i - 1, 12);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementKonusLenght[index][i][0], i - 1, 13);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementKonusLenght[index][i][1], i - 1, 14);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementKonusLenght[index][i][2], i - 1, 15);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementKonusLenght[index][i][3], i - 1, 16);
			types[0][currentType]->SetMeasuredValueDMS(measureObject.measurementRoughness[index][i], i - 1, 17);
		}
		for (int i = 1; i < 6; i++)
		{

		types[0][currentType]->IsGoodDMSCustomTolerance(0, measureObject.measurementDIS[index][i] - measureObject.setDSKDEKoffset, measureObject.measurementDSK[index][i], i-1, 0);
		types[0][currentType]->IsGoodDMSCustomTolerance(0, measureObject.measurementDIE[index][i] - measureObject.setDSKDEKoffset, measureObject.measurementDKE[index][i], i-1, 4);
		}
 

	//vpisujem v zgodovino 10000 kosov za gauss

		measureObject.measurementAvrDiameter[index] = avrMID;
	measureObject.widthHistory[measureObject.historyGaussCounter] = avrMID;
	measureObject.lenghtHistory[measureObject.historyGaussCounter] = measureObject.dowelLenght[index];

		 measureObject.historyGaussCounter++;
	 if (measureObject.historyGaussCounter == 10000)
		 measureObject.historyGaussCounter = 0;


	for (int i = 0; i < 18; i++)
	{
		measureObject.toleranceHistoryHIGH[index][i] = types[0][currentType]->toleranceHigh[i];
		measureObject.toleranceHistoryLOW[index][i] = types[0][currentType]->toleranceLow[i];
	}
	int bla = 100;
	int isGoodDMS = 1;
	int isGood2 = 1;
	int goodBlown = 0;
	int isGood = 0;

	int isGoodArea[11];
	for (int i = 0; i < 11; i++)
	{
		isGoodArea[i] = 1;
	}
	for (int k = 0; k < types[0][currentType]->parameterCounter; k++)//stevilo vseh paremtrov 
	{
		//isGood = types[0][currentType]->IsGoodDMS(k);
		isGoodDMS = 1;

		for (int i = 0; i < 5; i++)
		{
			measureObject.measureValueHistory[index][k][i + 1] = types[0][currentType]->measuredValueDMS[k][i];
			if (types[0][currentType]->isGoodDMS[k][i] == 0)
			{
				isGoodDMS = 0;
				measureObject.isGoodHistory[index][k][i + 1] = 0;
				if(i == 0)
				types[0][currentType]->badCounterByParameter[k]++;

			}
			else
				measureObject.isGoodHistory[index][k][i+1] = 1;
		}

		switch (k)
		{
			case 0://area 0 
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[0] = 0;
					isGoodArea[1] = 0;
				}
				break;
			}
			case 1: //area 1 
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[2] = 0;
					isGoodArea[3] = 0;
				}
			break;
			}
			case 2://area 2 
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[4] = 0;
					isGoodArea[5] = 0;
				}
				break;
			}
			case 3: //area 3
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[6] = 0;
					isGoodArea[7] = 0;
				}
				break;
			}
			case 4:
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[8] = 0;
					isGoodArea[9] = 0;
				}
				break;
			}
			case 5:case 9:case 13:
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[0] = 0;

				}
				break;
			}
			case 6:case 10:case 14:
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[1] = 0;

				}
				break;
			}
			case 7:case 11:case 15:
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[8] = 0;

				}
				break;
			}
			case 8:case 12:case 16:
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[9] = 0;

				}
				break;
			}
			case 17://roughness
			{
				if (isGoodDMS == 0)
				{
					isGoodArea[2] = 0;
					isGoodArea[3] = 0;
					isGoodArea[5] = 0;
					isGoodArea[6] = 0;
					isGoodArea[7] = 0;

				}
				break;
			}
		}
	}
	int tmpGoodStat =1;

	int tmpBadCounter[10];
	for (int z = 0; z < 10; z++)
	{
		tmpBadCounter[z] = badOnDifferentCounter[z];
	}

	for (int k = 0; k < types[0][currentType]->parameterCounter; k++)//za statistiko slabih
	{
		tmpGoodStat = 1;
		for (int i = 0; i < 5; i++)
		{
			if (types[0][currentType]->isGoodDMS[k][i] == 0)
			{
				tmpGoodStat = 0;
			}
		}
		switch (k)
		{
		case 0:case 1:case 2:case 3: case 4://sirina 
		{
			if (tmpGoodStat == 0)
			{
				if (tmpBadCounter[1] == badOnDifferentCounter[1])
					badOnDifferentCounter[1]++;
			}
			break;
		}
		case 5:case 6:case 7:case 8: //angle konus
		{
			if (tmpGoodStat == 0)
			{
				if (tmpBadCounter[2] == badOnDifferentCounter[2])
					badOnDifferentCounter[2]++;
			}
			break;
		}
		case 9:case 10:case 11:case 12:case 13:case 14:case 15:case 16: //konus
		{
			if (tmpGoodStat == 0)
			{
				if (tmpBadCounter[3] == badOnDifferentCounter[3])
					badOnDifferentCounter[3]++;
			}
			break;
		}

		case 17://roughness
		{
			if (tmpGoodStat == 0)
			{
				if (tmpBadCounter[4] == badOnDifferentCounter[4])
					badOnDifferentCounter[4]++;
			}
			break;
		}

		}
	}



	for (int i = 1; i < 6; i++)
	{
		if (measureObject.nrBadMeasurementsDICAll[index][i] >= measureObject.nrEMiddleAll)
			isGoodNrMeasurements[2] = 0;
		if (measureObject.nrBadMeasurementsDICMinus[index][i] >= measureObject.nrEMiddleMinus)
			isGoodNrMeasurements[2] = 0;

		if (measureObject.nrBadMeasurementsDIEAll[index][i] >= measureObject.nrEEdgeAll)
			isGoodNrMeasurements[3] = 0;
		if (measureObject.nrBadMeasurementsDIEMinus[index][i] >= measureObject.nrEdgeMinus)
			isGoodNrMeasurements[3] = 0;

		if (measureObject.nrBadMeasurementsDISAll[index][i] >= measureObject.nrEEdgeAll)
			isGoodNrMeasurements[1] = 0;
		if (measureObject.nrBadMeasurementsDISMinus[index][i] >= measureObject.nrEdgeMinus)
			isGoodNrMeasurements[1] = 0;

		if (measureObject.nrBadMeasurementsDSKAll[index][i] >= measureObject.nrEKonusAll)
			isGoodNrMeasurements[0] = 0;
		if (measureObject.nrBadMeasurementsDSKMinus[index][i] >= measureObject.nrEKonusMinus)
			isGoodNrMeasurements[0] = 0;

		if (measureObject.nrBadMeasurementsDKEAll[index][i] >= measureObject.nrEKonusAll)
			isGoodNrMeasurements[4] = 0;
		if (measureObject.nrBadMeasurementsDKEMinus[index][i] >= measureObject.nrEKonusMinus)
			isGoodNrMeasurements[4] = 0;
	}

	for (int i = 0; i < 5; i++)
	{
		if (isGoodNrMeasurements[i] == 0)
		{
			if (tmpBadCounter[5] == badOnDifferentCounter[5])
				badOnDifferentCounter[5]++;
		}
	}


	if ((measureObject.dowelLenght[index] >=  measureObject.lenghtToleranceMinus) && (measureObject.dowelLenght[index] <= measureObject.lenghtTolerancePlus))
		isGoodLenght = 1;
	else
	{
		isGoodLenght = 0;
		badOnDifferentCounter[0]++;
	}

	if (measureObject.mesurementElipse[index] <= measureObject.setElipsse)
		isGoodElipse = 1;
	else
	{
		isGoodElipse = 0;
		badOnDifferentCounter[6]++;
	}
	measureObject.elipseSumStatistics+=measureObject.mesurementElipse[index];
	


	int count = 0;

	for (int i = 0; i < 11; i++)
	{
		measureObject.isGoodArray[index][i] = isGoodArea[i];
		
		count++;
	}
	for (int i = 0; i < 5; i++)
	{
		measureObject.isGoodArray[index][count] = isGoodNrMeasurements[i];
		count++;
	}
	
	if (isGoodLenght == 1)
		measureObject.isGoodArray[index][count] = 1;
	else
		measureObject.isGoodArray[index][count] = 0;
	count++;
	if (isGoodElipse == 1)
		measureObject.isGoodArray[index][count] = 1;
	else
		measureObject.isGoodArray[index][count] = 0;
	count++;

	//isGoodArray  obmocja merjenja
	/*
	0->konusSpredaj
	1->zacetek ravnine spredaj
	2->sredina
	3->zacetek konusa zadaj
	4->konus zadaj 
	5-> dolzina*/
	goodBlown = 1;

	for (int i = 0; i < 18; i++)
	{
		if (measureObject.isGoodArray[index][i] == 0)
		{
			goodBlown = 0;
			break;
		}


	}
	if (goodBlown == 1)
	{
		types[0][currentType]->goodMeasurementsCounter++;
		measureObject.widthSumStatisticsGood += avrMID;
		measureObject.lenghtSumStatisticsGood += measureObject.dowelLenght[index];
		measureObject.statisticsCounterGood++;

		if (autoCalibrationMode == 1)
			dmsCalibrationDlg->UpdateAutoCalibrationTable(index);
	}
	else
	{
		types[0][currentType]->badMeasurementsCounter++;
	}


	measureObject.widthSumStatistics += avrMID;
	measureObject.lenghtSumStatistics += measureObject.dowelLenght[index];
	measureObject.statisticsCounter++;


	measureObject.dowelGood[index] = goodBlown;

	types[0][currentType]->totalMeasurementsCounter++;

	int alarm = 0;
	int bigAlarm = 0;
	if ((measureObject.nrDowelMeasurements[index] >= measureObject.dowelAllowedMeasurements[index][0]) && (measureObject.nrDowelMeasurements[index] <= measureObject.dowelAllowedMeasurements[index][1]))//ZNOTRAJ MEJ MERITEV
	{
		alarm = 0;
		bigAlarm = 0;
	}
	else
	{
		alarm = 1; //poslje v DMS modul pavzo  prepoved pihanja 0.2S
		measureObject.alarmCounter[0]++;
		bigAlarm = 4;
	}


	 if (measureObject.dowelLenght[index] < (measureObject.setLenght * 0.5))
	{
	alarm = 2; //poslje v DMS modul pavzo  prepoved pihanja 0.4S
	measureObject.alarmCounter[2]++;
	}
	 else if (measureObject.dowelLenght[index] < measureObject.setLenght * 0.75)
	{
		alarm = 3; //poslje v DMS modul pavzo  prepoved pihanja 0.2S
		measureObject.alarmCounter[1]++;
	}
	 if ((bigAlarm == 1) && (measureObject.nrMeasurementsNagative[index] > 5))
	 {
		 alarm = 6;//ugasne pihanje za 0.5s ; To se zgodi v priemeru kadar se mozniki zataknejo.
		 measureObject.alarmCounter[3]++;
	 }
	QString alm;
	measureObject.measureAlarm[index] = alarm;
	if (alarm > 0)
	{
		alm = QString("A0-%1;").arg(alarm);
		measureObject.sendAlarmString[index] = alm.toLocal8Bit();
		measureObject.sendAlarm[index] = 1;
	}

	if (measureObject.showInHistory[index] != 1)
	{
		measureObject.sendDowelString[index].clear();
		measureObject.sendDowelString[index].append("D");
		measureObject.sendDowelString[index].append(QString("%1").arg(measureObject.dowelNrXilinx[index]));
		measureObject.sendDowelString[index].append("-");
		measureObject.sendDowelString[index].append(QString("%1").arg(goodBlown));
		int  nr = measureObject.dowelSpeed[index] * 1000;
		//QString tmp = QString("%1").number(measureObject.dowelSpeed[index], 'g', 3);
		QString tmp = QString("%1").arg(nr);
		measureObject.sendDowelString[index].append(tmp);
		measureObject.sendDowelString[index].append(";");
	}
	measureObject.dowelReady[index] = 6;



	updateDraw = index;
	dmsUDP->lastPieceScanned = index;
	TestFun[0]->SetStop();
	TestFun[0]->ElapsedTime();



}

void MBsoftware::MeasurePiece(int nrCam, int index)
{
	int isBad = 1;
	badPresortType = 0;
	if (nrCam == 1)//za predsortiranje 
	{
		if (measureObject.isGoodPresortAll == 0)
		{
			if (settings->workingModePresort > 0)//
			{
				if (settings->workingModePresort ==1)//slabi na dolzini
				{
					if (measureObject.isGoodPresortLenght == 0)
					{
						isBad = 0;
						badPresortType = 1;
					}
				}
				else if (settings->workingModePresort == 2)// slabi na barvi
				{
					if (measureObject.isGoodPresortBlobs == 0)
					{
						isBad = 0;
						badPresortType = 2;
					}
				}
				else if (settings->workingModePresort == 3)// slabi na barvi in dolzini 
				{
					if ((measureObject.isGoodPresortBlobs == 0) || (measureObject.isGoodPresortLenght == 0))
					{
						isBad = 0;
						if (measureObject.isGoodPresortBlobs == 0)
							badPresortType = 2;
						else
							badPresortType = 1;
						if ((measureObject.isGoodPresortBlobs == 0) && (measureObject.isGoodPresortLenght == 0))
							badPresortType = 3;
					}
				}

				isGoodPresort = 0;
				if (isBad == 0)//ob slabem kosu
				{
					centerPresort = measureObject.centerPresort;
					presortCameraLenght = measureObject.presortLenght;
					presortProcessingTIme = measureObject.processingTimePresort;
					presortCameraSpeed = measureObject.presortDowelSpeed;

					presortCameraBlow = 1;
					presortCounterBadinLine++;
				}
				else
					presortCounterBadinLine = 0;
			}

		}


	}


	updateImage[nrCam] = 1;
	UpdateImageView(nrCam);

	
}

void MBsoftware::UpdateHistory(int station, int index)
{
	
		for (int j = 0; j < types[station][currentType]->parameterCounter; j++)
		{
			history->measuredValue[station][history->lastIn[station]][j] = types[station][currentType]->measuredValue[j];
			history->nominal[station][history->lastIn[station]][j] = types[station][currentType]->nominal[j];
			history->toleranceHigh[station][history->lastIn[station]][j] = types[station][currentType]->toleranceHigh[j];
			history->toleranceLow[station][history->lastIn[station]][j] = types[station][currentType]->toleranceLow[j];
			history->isConditional[station][history->lastIn[station]][j] = types[station][currentType]->isConditional[j];
			history->isActive[station][history->lastIn[station]][j] = types[station][currentType]->isActive[j];
			history->name[station][history->lastIn[station]][j] = types[station][currentType]->name[j];
		}
		if (station == 1)
		{
			history->image[0][station][history->lastIn[station]] = cam[station]->image[0].buffer->clone();
			history->image[1][station][history->lastIn[station]] = cam[station]->image[1].buffer->clone();
		}
		else
			history->image[0][station][history->lastIn[station]] = cam[station]->image[0].buffer->clone();


		history->isGood[station][history->lastIn[station]] = types[station][currentType]->AllGood();
	history->currentPiece[history->lastIn[station]] = index;
	history->UpdateCounter(station);

}

void MBsoftware::SetHistory()
{
	//najprej pobrisemo stare vektorje
	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int j = 0; j < HISTORY_SIZE; j++)
		{

			history->measuredValue[i][j].clear();
			history->nominal[i][j].clear();
			history->toleranceHigh[i][j].clear();
			history->toleranceLow[i][j].clear();
			history->toleranceLowCond[i][j].clear();
			history->toleranceHighCond[i][j].clear();
			history->name[i][j].clear();
			history->isActive[i][j].clear();
			history->isConditional[i][j].clear();
			history->image[0][i].clear();
			history->image[1][i].clear();
		}
		history->parametersCounter[i].clear();
	}


	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int j = 0; j < HISTORY_SIZE; j++)
		{
			if (types[i].size() > 0)
			{
				for (int k = 0; k < types[i][currentType]->parameterCounter; k++)
				{
					history->measuredValue[i][j].push_back(types[i][currentType]->measuredValue[k]);

					history->nominal[i][j].push_back(types[i][currentType]->nominal[k]);
					history->toleranceHigh[i][j].push_back(types[i][currentType]->toleranceHigh[k]);
					history->toleranceLow[i][j].push_back(types[i][currentType]->toleranceLow[k]);
					history->toleranceLowCond[i][j].push_back(types[i][currentType]->toleranceLowCond[k]);
					history->toleranceHighCond[i][j].push_back(types[i][currentType]->toleranceHighCond[k]);
					history->isActive[i][j].push_back(types[i][currentType]->isActive[k]);
					history->isConditional[i][j].push_back(types[i][currentType]->isConditional[k]);
					history->name[i][j].push_back(types[i][currentType]->name[k]);


				}
				/*if (i == 1)
				{
					history->image[0][i].push_back(Mat(cam[i]->image[0].buffer[0].rows, cam[i]->image[0].buffer[0].rows, CV_8UC3));
					history->image[1][i].push_back(Mat(cam[i]->image[0].buffer[0].rows, cam[i]->image[0].buffer[0].rows, CV_8UC3));
				}*/

				//history->cam1Image[i].push_back(Mat(images[1]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
			//	history->cam2Image[i].push_back(Mat(images[2]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
			//	history->cam3Image[i].push_back(Mat(images[3]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
				history->parametersCounter[i].push_back(types[i][currentType]->parameterCounter);
			}
		}
	}
	history->CreateMeasurementsTable();
}

void MBsoftware::WriteLogToFile(QString path, QString text, int type)
{
	//lockWrite.lockForWrite();

	//QString fileNameTmp = "C:/log/workingLog_" + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";
	QString logPathTmp = referencePath + QString("/%1/Log").arg(REF_FOLDER_NAME);
	QString fileNameTmp = QString("%1/workingLog_").arg(logPathTmp) + QDateTime::currentDateTime().toString("dd_MM_yyyy") + ".txt";

	QString value;

	value = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ") + text + type + "\n";


	if (fileNameTmp.compare(path) != 0)
	{
		//ko se spremeni datum odpre nov fajl
		m_logFile->close();
		m_logFile->setFileName(fileNameTmp);
		m_logFile->open(QIODevice::Append | QIODevice::Text);
	}
	if (m_logFile != 0)
	{
		QTextStream out(m_logFile);
		out.setCodec("UTF-8");
		out << value;
	}

}

void MBsoftware::OnMultimediaTimer(UINT uTimerID, UINT, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
{
	TestFun[4]->SetStart();

	imageProcess->currentType = currentType; //samo zacasno za dodajanje propBrowserja
	QString robotSend;
	QByteArray robotSendArray;
	if (MMTimer.timeCounter >= 500)
	{
		MMTimer.SetStop();
		MMTimer.CalcFrequency(MMTimer.timeCounter);
		MMTimer.timeCounter = 0;
		MMTimer.SetStart();
	}



	MMTimer.timeCounter++;

	lpt[0]->GetStatusValue();
	lpt[1]->GetStatusValue();
	glob->CalculateValveTime();//sprotno racunanje casa za pih moznikov

	if (mmCounter % 50 == 0)
	{
		if (wd == 0)
			wd = 1;
		else
			wd = 0;

		lpt[1]->SetOutputValue(4, wd);//wd
	}


	dmsUDP->ReadPendingDatagrams();



	if ((lpt[1]->status[1].value == 1) && (lpt[1]->status[1].prevValue == 0))//input glavenega ventila
	{
		if (testValveV < 0)
		{
			types[0][currentType]->valveOnCounter++;
			if (settings->workingMode == 0)
				types[0][currentType]->inBoxCounter = types[0][currentType]->valveOnCounter;
			else if (settings->workingMode == 1)
				types[0][currentType]->inBoxCounter++;

			CDMSstat::BlowDowel();
		}
	}

	if ((lpt[0]->status[0].value == 1) && (lpt[0]->status[0].prevValue == 0))//merilnik pritiska ne deluje zaneslivo samo za test
	{

		presureSensorCounter++;
	}


	if (mmCounter % 15000 == 0)
	{
		glob->OnCalculateCounters();
	}


	//luc 
	if (lightStatus == 1)//zelena luc sveti 
	{
		lightOn = 1;
	}
	else if (lightStatus == 2)//pocasi utripa
	{
		if (mmCounter % 300 == 0)
		{
			if (lightOn == 0)
			{
				lightOn = 1;
			}
			else
				lightOn = 0;
		}
	}
	else if (lightStatus == 3)//utripa hitro
	{
		if (mmCounter % 600 == 0)
		{
			if (lightOn == 0)
			{
				lightOn = 1;
			}
			else
				lightOn = 0;
		}
	}
	lpt[0]->SetOutputValue(1, lightOn);//aa


	if (lpt[0]->status[1].value == 0) //kadar je EMG
	{
		emgStop = 1;
		processStatus = -5;
	}

	if (processStatus > 0)//errorji// 
	{

		if ((startKey == 1) || (lpt[1]->status[2].prevValue == 0 && lpt[1]->status[2].value == 1)) //stop tipka za konec
		{
			processStatus = 0;
			warningStatus = 0; //ponastavim vsa opozorila, ki so se zgodila na zacetku 
			startKey = 0;
			glob->AddLog("STOP PRESSED!", 4);

			lpt[0]->SetOutputValue(7, 0);//CISCENJE 1  //aa
			lpt[0]->SetOutputValue(2, 0);///CISCENJE 2 //aa

			CDMSstat::SetStop(types[0][currentType]->goodMeasurementsCounter, types[0][currentType]->badMeasurementsCounter, types[0][currentType]->valveOnCounter, alarmStatus);
		}

		if (warningStatus > 0)
		{
			if ((warningStatus == 1) && (warningStatusPrev != 1))
			{
				glob->AddLog("NO DOWELS", 2);
			}
			if ((warningStatus == 5)&&(warningStatusPrev !=5))
			{
				glob->AddLog("CLEANING!", 2);
			}
			if ((warningStatus == 6) && (warningStatusPrev != 6))
			{
				glob->AddLog("START CLEANING!", 2);
			}
		}

		if (alarmStatus > 0)//ce se zgodi kr koli damo stroj na 0 
		{
			processStatus = 0;

			if (alarmStatus == 1)
			{
				deviceStatus = 5;
				glob->AddLog("ROTOR ERROR!", 1);
			}
			else if (alarmStatus == 2)
			{
				deviceStatus = 6;
				glob->AddLog("DEVICE EMPTY!", 3);
			}
			else if (alarmStatus == 3)
			{
				glob->AddLog("DOWEL STUCK!", 1);
				deviceStatus = 7;
			}
			else if (alarmStatus == 4)
			{
				glob->AddLog("BOX FULL!", 3);
				deviceStatus = 8;
			}
			else if (alarmStatus == 5)
			{
				glob->AddLog("PRESORT CAMERA STOP!", 1);
				deviceStatus = 9;
			}
			else if (alarmStatus == 6)
			{
				glob->AddLog("DMS modul ERROR!", 1);
				deviceStatus = 10;
			}

			CDMSstat::SetStop(types[0][currentType]->goodMeasurementsCounter, types[0][currentType]->badMeasurementsCounter, types[0][currentType]->valveOnCounter, alarmStatus);
		}
	}
	if (processStatus < 0)//v primeru EMG STOP ali testnega nacina
	{
		if ((startKey == 1) || (lpt[0]->status[4].prevValue == 0 && lpt[0]->status[4].value == 1)) //start tipka za zacetek delovanja
		{
			glob->EMGwindow->activateWindow();
			startKey = 0;
		}
		lpt[1]->SetOutputValue(1, 0);//rele 2 //aa
		lpt[1]->SetOutputValue(2, 0);//rele 3 rotator //aa
		lpt[1]->SetOutputValue(3, 0);//rele 4 elevator  //aa

	}
	warningStatusPrev = warningStatus;

	switch (processStatus)
	{
	case -5://EMG STOP
	{
		if ((lpt[0]->status[1].value == 1) && (lpt[0]->status[1].prevValue == 0)) //kadar je EMG
		{
			processStatus = -4;
			glob->AddLog("EMG STOP RELEASED!", 2);
		}
		if ((emgStop == 1) && (emgStopPrev == 0))
		{
			emgStop = 2;

			glob->AddLog("!!!EMG STOP!!!!", 1);

		}

		break;
	}
	case -4://EMG STOP
	{
		emgStop = 0;
		break;
	}
	case 0://v mirovanju 
	{
		if (alarmStatus == 0)
			deviceStatus = 0;
		if ((startKey == 1) || (lpt[0]->status[4].prevValue == 0 && lpt[0]->status[4].value == 1)) //start tipka za zacetek delovanja
		{
			CDMSstat::SetStart(types[0][currentType]->goodMeasurementsCounter, types[0][currentType]->badMeasurementsCounter, types[0][currentType]->valveOnCounter);
			glob->AddLog("START PRESSED!", 4);
			if(settings->startCleanEnable == 0)//drugace nastavim po koncaju ciscenja
				glob->AddLog("WORKING!", 0);
			processStatus = 1;
			startKey = 0;
			rotatorSenCounter = 0;
			dmsModulErrorCounter = 0;
			if (alarmStatus == 4)//v primeru kadar je skatla polna poresetiramo skatlo 
			{
				types[0][currentType]->inBoxCounter = 0;
				types[0][currentType]->boxCounter++;
			}
			alarmStatus = 0;
			dowelOnCounter = 0;
			dowelStuckCounter = 0;
			dowelStuckSen2Counter = 0;
			lightStatus = 1;
			elevatorStatus = 0;
			elevatorCounter = 0;
			presortCounterBadinLine = 0;

			cleanPeriodeCounter = 0;
			cleanCounterOffset = 0;
			clean1Finished = false;
			clean2Finished = false;
			deviceStatus = 1;
			warningStatus = 0;
			if (settings->startCleanEnable == true)
			{
				processStatus = 2;
				cleaningMode = 1;
			}
			else
				processStatus = 1;
		}


		lpt[1]->SetOutputValue(1, 0);//rele 2 //aa
		lpt[1]->SetOutputValue(2, 0);//rele 3 rotator //aa
		lpt[1]->SetOutputValue(3, 0);//rele 4 elevator  //aa
		lpt[0]->SetOutputValue(7, 0);//CISCENJE 1  //aa
		lpt[0]->SetOutputValue(2, 0);///CISCENJE 2 //aa
		elevatorStatus = 0;
		rotatorOn = false;
		lightOn = 0;




		break;
	}
	case 1://normalno delovanje 
	{
		lpt[1]->SetOutputValue(1, 1);//rele 2  //aa
		lpt[1]->SetOutputValue(2, 1);//rele 3 rotator //aa

		rotatorOn = true;

		cleanPeriodeCounter++;
		if (cleanPeriodeCounter >= settings->cleanPeriod * 1000 * 60)//gre v ciscenje
		{
			processStatus = 2;
			cleanPeriodeCounter = 0;
			cleanCounterOffset = 0;
			clean1Finished = false;
			clean2Finished = false;
			cleaningMode = 2;

		}
		//working mode
		if (settings->workingMode == 1) //autoStop
		{
			if (types[0][currentType]->inBoxCounter >= settings->nrDowelToStop)
			{
				alarmStatus = 4;//ugasne stroj in izpise da je stroj poln 
				boxFullStat = 1;
			}
		}

		if (settings->presortStopEnable == 1) //predsortiran kamera ustavi stroj
		{
			if (settings->nrBadPresortToStop < presortCounterBadinLine)
			{
				alarmStatus = 5;
			}
		}

		int isOn = 0;
		for (int i = 0; i < NR_DMS_CAM; i++)
		{
			if (measureObject.calibrationTransitions[i][0] > 0)
			{
				firstTransition[i] = measureObject.calibrationTransitions[i][0];
				if(firstTransition[i] == firstTransitionPrev[i])
					isOn = 1;

			}
			
			firstTransitionPrev[i] = firstTransition[i];
		}
		
		if (isOn > 0)
			dmsModulErrorCounter++;
		else
			dmsModulErrorCounter = 0;

		if (dmsModulErrorCounter > 2000)
		{
			alarmStatus = 6;
		}

		//zagon rotatorja 
		if ((lpt[1]->status[4].value == 1 && lpt[1]->status[4].prevValue == 0)) //ce je rotator zataknjen
			rotatorSenCounter = 0;
		else
			rotatorSenCounter++;

		int numberToStop = 3000;
		if ((settings->rotatorSpeedMode > 0) && (settings->rotatorSpeedMode < 3))
		{
			numberToStop = 3500;
		}
		else
			numberToStop = 2000;
		if (rotatorSenCounter > numberToStop)
		{
			alarmStatus = 1;
		}

		if ((lpt[0]->status[2].value == 1 && lpt[0]->status[2].prevValue == 0)) // preverjam moznike po drci da ustavim stroj po xx 
		{
			dowelOnCounter = 0;
		}
		else
			dowelOnCounter++;

		if ((dowelOnCounter > settings->dowelStuckStartBlink * 1000) && (dowelOnCounter < settings->dowelStuckStopMachine * 1000))
		{
			lightStatus = 2;
			warningStatus = 1;
		}
		else if (dowelOnCounter > settings->dowelStuckStopMachine * 1000)//no dowel 
		{
			alarmStatus = 2;
		}
		else
		{
			lightStatus = 1;
			if (warningStatus == 1)
				warningStatus = 0;
		}

		if ((lpt[0]->status[2].value == 0)) // kadar so mi+ozniki zataknjeni 
		{
			dowelStuckCounter++;
		}
		else
			dowelStuckCounter = 0;

		if ((lpt[0]->status[3].value == 0)) // kadar so mi+ozniki zataknjeni 
		{
			dowelStuckSen2Counter++;
		}
		else
			dowelStuckSen2Counter = 0;

		if (dowelStuckSen2Counter > 2000) //ce so mozniki zataknjeni za vec kot 40sec
		{
			alarmStatus = 3;
		}
		

		if (dowelStuckCounter > 2000) //ce so mozniki zataknjeni za vec kot 40sec
		{
			alarmStatus = 3;
		}


		//senzor za elevator 
		switch (elevatorStatus)
		{
		case(0)://zacetno stanje elevator ugasnjen stejem kdaj bo rotor prazen 
		{
			lpt[1]->SetOutputValue(3, 0);//rele 3 elevator //aa
			if ((lpt[1]->status[3].value == 1)) //kosov ni v vibratoji 
			{
				elevatorCounter++;
				if (elevatorCounter > settings->elevatorStartTime)
				{
					elevatorCounter = 0;
					elevatorStatus = 1;
				}
			}
			break;
		}
		case(1)://elevator deluje 
		{
			lpt[1]->SetOutputValue(3, 1);//rele 3 elevator //aa
			if ((lpt[1]->status[3].value == 0)) //kos so v rotatorju
			{
				elevatorCounter++;
				if (elevatorCounter > settings->elevatorStopTime)
				{
					elevatorCounter = 0;
					elevatorStatus = 0;
				}
			}
			break;
		}
		default:
			break;

		}


		break;

	}
	case 2: //cleaning
	{
		lpt[1]->SetOutputValue(1, 0);//rele 2  //aa
		lpt[1]->SetOutputValue(2, 0);//rele 3 rotator //aa
		lpt[1]->SetOutputValue(3, 0);//rele 3 elevator //aa
		int timeToStop0; 
		int timeToStop1;  

		cleanCounterOffset++;
		if (cleaningMode == 2)
		{
			if (cleanCounterOffset > settings->cleanValve1Offset) //mirror
			{
				if (cleanCounterOffset > settings->cleanValve1Duration + settings->cleanValve1Offset)
				{
					lpt[0]->SetOutputValue(7, 0);//rele 2  //aa
					
					clean1Finished = 1;
				}
				else
					lpt[0]->SetOutputValue(7, 1);//rele 2  //aa
			}

			if (cleanCounterOffset > settings->cleanValve2Offset) //mirror
			{
				if (cleanCounterOffset > settings->cleanValve2Duration + settings->cleanValve2Offset)
				{
					lpt[0]->SetOutputValue(2, 0);//rele 2  //aa
					clean2Finished = 1;
				}
				else
					lpt[0]->SetOutputValue(2, 1);//rele 2  //aa
			}
			warningStatus = 5;
			 timeToStop0 =  settings->cleanValve1Offset + settings->cleanValve1Duration;
			 timeToStop1 =  settings->cleanValve2Offset + settings->cleanValve2Duration;
		}
		else//zacetno ciscenje
		{
			if (cleanCounterOffset > settings->cleanValve1Duration)
			{
				lpt[0]->SetOutputValue(7, 0);//rele 2  //aa
				clean1Finished = 1;
			}
			else
				lpt[0]->SetOutputValue(7, 1);//rele 2  //aa

			if (cleanCounterOffset > settings->cleanValve2Duration)
			{
				lpt[0]->SetOutputValue(2, 0);//rele 2  //aa
				clean2Finished = 1;
			}
			else
				lpt[0]->SetOutputValue(2, 1);//rele 2  //aa

			warningStatus = 6;
			timeToStop0 =  settings->cleanValve1Duration;
			timeToStop1 =  settings->cleanValve2Duration;
		}

		if (timeToStop0 > timeToStop1)
			timeToFinishCleaning = timeToStop0;
		else
			timeToFinishCleaning = timeToStop1;

		if ((clean1Finished == 1) && (clean2Finished == 1))//konec ciscenja
		{
			processStatus = 1;
			warningStatus = 0;
			rotatorSenCounter = 0;
			glob->AddLog("WORKING!", 0);
		}

		break;
	}
	}

	emgStopPrev = emgStop;


	if (processStatus <= 0)
	{
		glob->ValveTestFunction();
	}
	if (settings->rotatorSpeedMode == 0)
	{
		vf0 = 0; vf1 = 0; vf2 = 0;
		vfSettingsText = QString("ROTOR SPEED 0: MANUAL");
	}
	else if (settings->rotatorSpeedMode == 1)
	{
		vf0 = 0; vf1 = 0; vf2 = 1;
		vfSettingsText = QString("ROTOR SPEED 1: 25Hz");
	}
	else if (settings->rotatorSpeedMode == 2)
	{
		vf0 = 0; vf1 = 1; vf2 = 0;
		vfSettingsText = QString("ROTOR SPEED 2: 40Hz");
	}
	else if (settings->rotatorSpeedMode == 3)
	{
		vf0 = 0; vf1 = 1; vf2 = 1;
		vfSettingsText = QString("ROTOR SPEED 3: 45Hz");
	}
	else if (settings->rotatorSpeedMode == 4)
	{
		vf0 = 1; vf1 = 0; vf2 = 0;
		vfSettingsText = QString("ROTOR SPEED 4: 50Hz");
	}
	else if (settings->rotatorSpeedMode == 5)
	{
		vf0 = 1; vf1 = 0; vf2 = 1;
		vfSettingsText = QString("ROTOR SPEED 5: 54Hz");
	}
	else if (settings->rotatorSpeedMode == 6)
	{
		vf0 = 1; vf1 = 1; vf2 = 0;
		vfSettingsText = QString("ROTOR SPEED 6: 58Hz");
	}
	else if (settings->rotatorSpeedMode == 7)
	{
		vf0 = 1; vf1 = 1; vf2 = 1;
		vfSettingsText = QString("ROTOR SPEED 7: 62Hz");
	}




	for (int i = 0; i < DMSHISTORYSIZE; i++)
	{

		if (measureObject.dowelReady[i] == 4)
		{


			if (delaySendDowel[i] == 3)
			{
				measureObject.dowelReady[i] = 5;
				delaySendDowel[i] = 0;
				showPiece = measureObject.currentPiece;
				//glob->update();
				measureObject.speedForPresort[presortSpeedCounter] = measureObject.dowelSpeed[i];
				presortSpeedCounter++;
				if (presortSpeedCounter > 10)
					presortSpeedCounter = 0;
				
				glob->MeasurePiece(i);

			}
			delaySendDowel[i]++;
		}

	}
	for (int i = 0; i < DMSHISTORYSIZE; i++)
	{
		if (measureObject.sendAlarm[i] == 1)
		{
			measureObject.sendAlarm[i] = 2;
			dmsUDP->WriteDatagram(measureObject.sendAlarmString[i]);
			measureObject.sendAlarm[i] = 3;
		}

		if (measureObject.dowelReady[i] == 6)
		{
			CDMSstat::DowelToBox(measureObject.dowelLenght[i], measureObject.measurementAvrDiameter[i],(bool)measureObject.dowelGood[i]);
			measureObject.dowelReady[i] = 7;
			if (measureObject.showInHistory[i] != 1)
			{
				dmsUDP->WriteDatagram(measureObject.sendDowelString[i]);
				dmsUDP->processingTimer[i]->SetStop();
				measureObject.processingTime[i] = dmsUDP->processingTimer[i]->ElapsedTime();
				//tmpSaveIndex.push_back(i);
				measureObject.blowImageCounter[i] = 0;

			}
			measureObject.showInHistory[i] = 0;
			//cam[0]->image[imageIndex].SaveBuffer(QString("C:\\images\\D%1 N%2.bmp").arg(i).arg(measureObject.blowImageCounter[i]));


		}
	}

	//tukaj je koda za test blow cam 
	if (cam[0]->imageReady[0] == 1)
	{
		cam[0]->imageReady[0] = 2;
		for (int i = 0; i < DMSHISTORYSIZE; i++)
		{
			if (measureObject.blowImageCounter[i] < 20)
			{

				QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera0, i, (measureObject.blowImageCounter[i]), 0);

				measureObject.blowImageCounter[i]++;


			}
		}
	}
	if (startPresortCamera > 2000)//dodam zacetno zakasnitev
	{
		startPresortCamera = 2001;
		if (!imageProcess->presortDialog->isVisible()) //kadar je dialog odprt ne izvaja real time meritev
		{

			if (cam[1]->imageReady[0] == 1)
			{
				QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera1, &cam[1]->image[0], 0);
				cam[1]->imageReady[0] = 2;
			}
		}
	}
	else
		startPresortCamera++;
	if(!dmsCalibrationDlg->isVisible()) //kadar smo v kalibracijskem načinu presortiranje ne deluje. 
	{
	//predsortiranje

		if ((lpt[0]->status[3].prevValue == 1) && (lpt[0]->status[3].value == 0))  //zgornji senzor
		{
			filterPresortOn = 1;
			presortSensorDelay = 0;

		}
		if (filterPresortOn == 1)
		{
			if (lpt[0]->status[3].value == 0)
			{
				presortSensorDelay++;
			}
			else
				presortSensorDelay = 0;
			if (presortSensorDelay > 2)
			{
				speedTimerPresort.SetStart();

				indexPresortDowelStart++;
				if (indexPresortDowelStart > 19)
					indexPresortDowelStart = 0;
				speedPresortSensorTimer[indexPresortDowelStart].SetStart();
				isProcessedPresortSensor[indexPresortDowelStart][0] = 1;
				isProcessedPresortSensor[indexPresortDowelStart][1] = 0;

				//presortSensorDelay = 0;
				filterPresortOn = 0;
				presortOn = 1;
			}
		}
		if ((presortOn == 1))
		{
			if ((lpt[0]->status[2].prevValue == 1) && (lpt[0]->status[2].value == 0))//izmerimo hitrost, ko gre iz 0 v 1
			{


				indexPresortDowelStop++;
				if (indexPresortDowelStop > 19)
					indexPresortDowelStop = 0;

				int startIndex = indexPresortDowelStart-5;
				if (startIndex < 0)
					startIndex = 19 + startIndex;

				for (int i = 0; i < 5; i++)
				{
					startIndex++;
					if (startIndex > 19)
						startIndex = 0;

					if (isProcessedPresortSensor[startIndex][1] == 0)
					{
						isProcessedPresortSensor[startIndex][0] = 2;
						isProcessedPresortSensor[startIndex][1] = 2;
						speedPresortSensorTimer[startIndex].SetStop();
						break;
					}

				}

				speedPresortSensorTimer[indexPresortDowelStop].SetStop();

				speedTimerPresort.SetStop();
				lenghtTimerPresort.SetStart();
				//speedTimerPresort.ElapsedTime();
				float hit = 15 / speedTimerPresort.ElapsedTime();
				//float hit = 15/speedPresortSensorTimer[startIndex].ElapsedTime();
				presortSpeed = hit;
				hit = 10;
			}

			if ((lpt[0]->status[2].prevValue == 0) && (lpt[0]->status[2].value == 1))//konec moznika izmerimo dolzino glede na cas in hitrost
			{
				presortOn = 0;
				lenghtTimerPresort.SetStop();
				
				presortLenght = presortSpeed * lenghtTimerPresort.ElapsedTime();
				
				presortPieceCount++;
				if (settings->presortSensorEnable == 1)//za phanje predsortirnega senzorja
				{
					if (presortLenght < (measureObject.setLenght / 3 * 2))//presort valve  bad piece
					{
						presortSensorLenghtUsed = presortLenght;
						//int time = round(((distanceToValve - delay - measureObject.setLenght / 2) / presortCameraSpeedUsed));
						
						if (presortSpeed < 1)
						{
							presortSensorSpeedUsed = 1;
						}
						else
							presortSensorSpeedUsed = presortSpeed;

						float timeBlow = settings->presortSensorTimeBlow;
						float timeToBlow = (settings->presortSensorOffset - measureObject.setLenght/2) / presortSensorSpeedUsed;
						if (presortSpeed < 1)
						{
							timeBlow = timeBlow + 20;
						}
						
						presortSensorTimeToBlow = timeToBlow;
						int startTime;
						startTime = (mmCounter + (int)presortSensorTimeToBlow) & 0xffff;

						for (int k = 0; k < timeBlow; k++)
						{
							presortSensorTimeStamp[startTime] = 1;
							startTime++;
							startTime = startTime & 0xffff;
						}
					}
				}

			}
		}
	
	int distanceToValve = settings->presortCameraTimeOffset;
	float timeToBlowCamera = 0;
	int offsetToValve = 0;
	float timeBlowCamera = settings->presortCameraTimeBlow;

	
	//float timeBlowCamera  = presortLenght / presortCameraSpeed;

	if (timeBlowCamera < 20)
		timeBlowCamera = 20;
	int cen = 200;


	//predsortiranje kamera


	if (presortCameraBlow == 1)//racunam hitrost moznikov iz povratne vezave ali iz izracunane prejsnje slike
	{
		int co = 0;
		presortSpeedAvrage = 0;
		for (int i = 0; i < 10; i++)
		{
			if (measureObject.speedForPresort[i] > 0)
			{
				presortSpeedAvrage += (measureObject.speedForPresort[i] - 0.2);
				co++;
			}
		}
		if (co > 0)
			presortSpeedAvrage /= co;
		else
			presortSpeedAvrage = 2;

		if ((presortCameraSpeed > 0.5) && (presortCameraSpeed < 3))
		{
			presortCameraSpeedUsed = presortCameraSpeed;
		}
		else
			presortCameraSpeedUsed = presortSpeedAvrage;


		float delay = ((centerPresort - cen) *korFactorPresort);
		int time = round(((distanceToValve - delay -measureObject.setLenght/2) / presortCameraSpeedUsed));
		
		if ((badPresortType == 1) || (badPresortType == 3))
		{
			presortDelay = time - 10;
			presortTime = measureObject.timeToBlow + 20;
		}
		else
		{
			presortTime = measureObject.timeToBlow;
			presortDelay = time;

		}

		presortCameraBlow = 0;

		int startTime = (mmCounter + (int)time) & 0xffff;

		for (int k = 0; k < presortTime; k++)
		{
			presortCameraTimeStamp[startTime] = 1;
			startTime++;
			startTime = startTime & 0xffff;
		}


	}
}

	int eraseTimeStamp = 0;

	eraseTimeStamp = (mmCounter - 50000) & 0xffff;
	presortSensorTimeStamp[eraseTimeStamp] = 0;
	presortCameraTimeStamp[eraseTimeStamp] = 0;



	//presort sensor small
	if (testValveY < 0)
		lpt[0]->SetOutputValue(6, presortSensorTimeStamp[mmCounter]); //aa
	if (testValveX < 0)
		lpt[1]->SetOutputValue(6, presortCameraTimeStamp[mmCounter]); //aa
	
	if ((lpt[1]->data[6].value == 1) && (lpt[1]->data[6].prevValue == 0))//kamera
	{
	
		presortCounterBadStat++;
		if (badPresortType == 3)
		{
			badpresrotSmall++;
			badpresortColor++;
		}
		else if (badPresortType == 1)
			badpresrotSmall++;
		else if (badPresortType == 2)
			badpresortColor++;
	}
	if ((lpt[0]->data[6].value == 1) && (lpt[0]->data[6].prevValue == 0))//presortSensor
	{
		badpresortSensor++;
	}
	//za service tab
	if ((lpt[0]->status[3].value == 0) && (lpt[0]->status[3].prevValue == 1))
		inputCounter[0]++;
	if ((lpt[0]->status[2].value == 0) && (lpt[0]->status[2].prevValue == 1))
		inputCounter[1]++;
	if ((lpt[0]->status[1].value == 0) && (lpt[0]->status[1].prevValue == 1))
		inputCounter[2]++;
	if ((lpt[1]->status[1].value == 0) && (lpt[1]->status[1].prevValue == 1))
		inputCounter[3]++;
	if ((lpt[1]->status[4].value == 0) && (lpt[1]->status[4].prevValue == 1))
		inputCounter[4]++;
	if ((lpt[1]->status[3].value == 0) && (lpt[1]->status[3].prevValue == 1))
		inputCounter[5]++;
	if ((lpt[1]->status[2].value == 0) && (lpt[1]->status[2].prevValue == 1))
		inputCounter[6]++;
	if ((lpt[0]->status[4].value == 0) && (lpt[0]->status[4].prevValue == 1))
		inputCounter[7]++;

	for (int i = 0; i < 8; i++)
		inputCounter[i] = inputCounter[i] & 0xffff;


	//hitrost frekvencnika
	lpt[0]->SetOutputValue(3, vf2); //aa
	lpt[0]->SetOutputValue(4, vf1); //aa
	lpt[0]->SetOutputValue(5, vf0); //aa

	mmCounter++;
	mmCounter = mmCounter & 0xffff;
	
	TestFun[4]->SetStop();
	
	timeDur[timeDurCounter] = TestFun[4]->ElapsedTime();
	timeDurCounter++;

	float min = 10000;
	float max = 0;
	if (timeDurCounter > 9999)
	{
		timeDurCounter = 0;
		for (int i = 0; i < 9999; i++)
		{
			if (timeDur[i] > max)
				max = timeDur[i];

			if (timeDur[i] < min)
				min = timeDur[i];
		}

		max = min + 100;
	}


	

}

void MBsoftware::CreateCameraTab()
{

//QWidget *cameraWidget;
	QGroupBox *camGroup;// = new QGroupBox(ui.menuCameras);;
	QHBoxLayout *camLayout = new QHBoxLayout;
	QToolButton *camButton;
	QIcon camIcon;
	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;
	int k = 0;

	QImage image;

	for (int i = 0; i < cam.size(); i++)
	{
		image.load(QString(QDir::currentPath() + QString("\\res\\cam%1.bmp").arg(i)));
		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));

		camButton = new QToolButton();
		camButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
		
		if (slovensko)  camButton->setText(QString("KAM%1").arg(i));
		else camButton->setText(QString("CAM%1").arg(i));
		camButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
		camButton->setIcon(camIcon);
		camButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
		camButton->setAutoRaise(true);
		camLayout->addWidget(camButton);
		connect(camButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
		signalMapper->setMapping(camButton, i);
	}


	
	//tabLayout->addWidget(camGroup);
	tabLayout->addSpacerItem(spacer);
	//ui.menuCameras->setLayout(tabLayout);
	camGroup->setLayout(camLayout);
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowCamButton(int)));

}

void MBsoftware::CreateSignalsTab()
{
	QHBoxLayout *lptLayout = new QHBoxLayout;
	QToolButton *lptButton;
	QHBoxLayout *smartCardLayout = new QHBoxLayout;
	QToolButton *smartCardButton;
	QHBoxLayout *tcpLayout = new QHBoxLayout;
	QToolButton *tcpButton;
	QHBoxLayout *xilLayout = new QHBoxLayout;
	QToolButton *xilButton;

	QAction *lptAction;

	QIcon camIcon;
	QIcon smartIcon;
	QIcon xilIcon;

	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QSignalMapper* signalSmartCardMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;


	int k = 0;

	
	QImage image;
	ui.menuSignals->setLayout(tabLayout);
	/*if (lpt.size() > 0)
	{
		QGroupBox *lptGroup = new QGroupBox(ui.menuSignals);
		lptGroup->setTitle("LPT");

		image.load(QString(QDir::currentPath() + QString("\\res\\lptIcon.bmp")));

		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < lpt.size(); i++)
		{

			lptButton = new QToolButton();
			lptButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			lptButton->setText(QString("LPT%1").arg(i));
			lptButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			lptButton->setIcon(camIcon);
			lptButton->setAutoRaise(true);
			lptButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			lptLayout->addWidget(lptButton);
			connect(lptButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
			signalMapper->setMapping(lptButton, i);
		}
		tabLayout->addWidget(lptGroup);
		
		ui.menuSignals->setLayout(tabLayout);
		lptLayout->setMargin(0);
		lptGroup->setLayout(lptLayout);
		connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowLptButton(int)));
	}*/
	/*if (lpt.size() > 0)
	{
		for (int i = 0; i < lpt.size(); i++)
		{
			lptAction = new QAction(ui.menuSignals);
			lptAction->setText(QString("LPT%1").arg(i));
		}
	}*/
	if (uZed.size() > 0)
	{
		QGroupBox *uZedBox = new QGroupBox(ui.menuSignals);
		uZedBox->setTitle("Zynq I/O");

		image.load(QString(QDir::currentPath() + QString("\\res\\xilinxIO.bmp")));

		if (!image.isNull())
			xilIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < uZed.size(); i++)
		{

			xilButton = new QToolButton();
			xilButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			xilButton->setText(QString("uZed%1").arg(i));
			xilButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			xilButton->setIcon(xilIcon);
			xilButton->setAutoRaise(true);
			xilButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			xilLayout->addWidget(xilButton);
			connect(xilButton, &QPushButton::clicked, [=](int index) { OnClickedShowUZedButton(i); });
		}

		tabLayout->addWidget(uZedBox);
		xilLayout->setMargin(0);
		uZedBox->setLayout(xilLayout);



	}
	if (tcp.size() > 0)
	{
		QGroupBox *tcpGroup = new QGroupBox(ui.menuSignals);
		tcpGroup->setTitle("TCP");
		image.load(QString(QDir::currentPath() + QString("\\res\\localNetwork.bmp")));

		if (!image.isNull())
			smartIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < tcp.size(); i++)
		{
			tcpButton = new QToolButton();
			tcpButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			tcpButton->setText(QString("TCP%1").arg(i));
			tcpButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			tcpButton->setIcon(smartIcon);
			tcpButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			tcpButton->setAutoRaise(true);
			tcpLayout->addWidget(tcpButton);

			connect(tcpButton, &QPushButton::clicked,
				[=](int index) { OnClickedTCPconnction(i); });

		}
		tabLayout->addWidget(tcpGroup);
		tcpLayout->setMargin(0);
		tcpGroup->setLayout(tcpLayout);
	}


	tabLayout->addSpacerItem(spacer);



}

void MBsoftware::CreateMeasurementsTable()//v priemru DMS naredim 2 tabeli za parametere vseh kamer in ostalo
{
	parametersTable = new QTableView();
	modelTable = new QStandardItemModel();

		parametersTable->setFont(QFont("Times new Roman", 12));
		modelTable->setColumnCount(9);
		modelTable->setHeaderData(0, Qt::Horizontal, tr("param"));
		modelTable->setHeaderData(1, Qt::Horizontal, tr("CAM1"));
		modelTable->setHeaderData(2, Qt::Horizontal, tr("CAM2"));
		modelTable->setHeaderData(3, Qt::Horizontal, tr("CAM3"));
		modelTable->setHeaderData(4, Qt::Horizontal, tr("CAM4"));
		modelTable->setHeaderData(5, Qt::Horizontal, tr("CAM5"));
		modelTable->setHeaderData(6, Qt::Horizontal, tr("MIN"));
		modelTable->setHeaderData(7, Qt::Horizontal, tr("MAX"));
		modelTable->setHeaderData(8, Qt::Horizontal, tr("BAD Proc"));

		modelTable->setRowCount(types[0][currentType]->parameterCounter);
		parametersTable->setModel(modelTable);

		for (int i = 0; i < types[0][0]->parameterCounter; i++)
		{
			parametersTable->setRowHeight(i, 21);

		}
		standardTableItem.resize(types[0][0]->parameterCounter);

		for (int k = 0; k < types[0][0]->parameterCounter; k++)
		{

			for (int z = 0; z < 9; z++)
			{
				standardTableItem[k].push_back(new QStandardItem());
				modelTable->setItem(k, z, standardTableItem[k].back());
				standardTableItem[k].back()->setText(QString(""));
				
			}
		}
		parametersTable->setColumnWidth(0, 105);
		for (int i = 1; i < 9; i++)
		{
			parametersTable->setColumnWidth(i, 61);
		}

		int w = parametersTable->width();
		int h = parametersTable->height();
		parametersTable->height();
		
		ui.layoutMeasurement->addWidget(parametersTable, Qt::AlignCenter); 

}

void MBsoftware::AddLog(QString text, int type)
{
	QTime time = QTime::currentTime();
	QString textt = time.toString("hh:mm:ss");

	logText.push_back(QString("%1 - %2").arg(textt).arg(text));
	logIndex.push_back(type);
	WriteLogToFile(logPath, text, type);
	EraseLastLOG();
	updateLogWindow = 1;
}


void MBsoftware::EraseLastLOG()
{
	if (logIndex.size() > 100)
	{
		logIndex.erase(logIndex.begin());
		logText.erase(logText.begin());
	}
}


void MBsoftware::ReadTypes()
{
	QString filePath;
	QStringList values;
	int count = 0;
	QString typePath;
	QString typeName;
	QDir dir;
	filePath = referencePath + QString("/%1/types/").arg(REF_FOLDER_NAME);

	if (!QDir(filePath).exists())
		QDir().mkdir(filePath);

		QSettings settings(filePath, QSettings::IniFormat);

		
		dir.setPath(filePath);
		dir.setNameFilters(QStringList() << "*.ini");
		count = dir.count();
		QStringList list = dir.entryList();
		//inputs
		for (int j = 0; j < count; j++)
		{
			typePath = filePath;
			typeName = list.at(j);
			typeName.chop(4);
			for (int i = 0; i < NR_STATIONS; i++)
			{
				types[i].push_back(new Types(i));
				//types[i].back()->stationNumber = i;
				types[i].back()->Init(typeName, typePath);
			}
		}
}




void MBsoftware::ReadCameraInit()
{
	QString dirPath;
	QString filePath;
	QStringList values;
	QDir dir;
	int count;
	QSettings cameraSettings;

	typedef struct tmpCameraSettings
	{
		QString vendor = "";
		QString serialNum = "";
		QString customName ="";
		int width = 0;
		int height = 0;
		float fps = 0;
		int depth = 0;
		bool trigger = 0;
		bool realTimeProcessing = 0;
		bool rotated_vertical = 0;
		bool rorated_horizontal = 0;
		int num_images = 1;
	}tmpCameraSettings;

	tmpCameraSettings tmpSettings[NR_CAMERAS];

	//dirPath = QDir::currentPath() + QString("/%1/").arg(CAMERA_INIT_FOLDER);//dodaj dinamicno pot za nastavitve
	dirPath = referencePath + QString("/%1/nastavitveKamer/").arg(REF_FOLDER_NAME);
	int camNr = 0;

	dir.setPath(dirPath);
	dir.setNameFilters(QStringList() << "*.ini");
	count = dir.count();
	QStringList list = dir.entryList();
	//inputs
	if (count > NR_CAMERAS)
		count = NR_CAMERAS;
	
	for (int j = 0; j < count; j++)
	{
		filePath = dirPath + list[j];
		QSettings settings(filePath, QSettings::IniFormat);

		settings.beginGroup("General");
		const QStringList childKeys = settings.childKeys();
		if (childKeys.size() > 11)
		{
			tmpSettings[j].customName = settings.value("CUSTOMNAME").toString();
			tmpSettings[j].vendor = settings.value("VENDOR").toString();
			tmpSettings[j].width = settings.value("WIDTH").toInt();
			tmpSettings[j].height = settings.value("HEIGHT").toInt();
			tmpSettings[j].depth = settings.value("DEPTH").toInt();
			tmpSettings[j].trigger = settings.value("TRIGGER").toBool();
			tmpSettings[j].num_images = settings.value("NUM_IMAGES").toInt();
			tmpSettings[j].rotated_vertical = settings.value("ROTATED_VERTICAL").toBool();
			tmpSettings[j].rorated_horizontal = settings.value("ROTATED_HORIZONTAL").toBool();
			tmpSettings[j].realTimeProcessing = settings.value("REALTIME_PROCESSING").toBool();
			tmpSettings[j].fps = settings.value("FPS").toFloat();
			tmpSettings[j].serialNum = settings.value("SERIAL").toString();
		}
		else
		{
			settings.setValue("CUSTOMNAME", 0);
			settings.setValue("VENDOR", 0);
			settings.setValue("WIDTH", 0);
			settings.setValue("HEIGHT", 0);
			settings.setValue("DEPTH", 0);
			settings.setValue("TRIGGER", 0);
			settings.setValue("NUM_IMAGES", 0);
			settings.setValue("ROTATED_VERTICAL", 0);
			settings.setValue("ROTATED_HORIZONTAL", 0);
			settings.setValue("FPS", 0);
			settings.setValue("SERIAL", 0);
		}
		settings.endGroup();
		int selected = 0;
		if (tmpSettings[j].vendor.compare("imagingSource", Qt::CaseInsensitive) == 0)//imaging source camera
		{
			cam.push_back(new ImagingSource());
			selected = 1;
		}
		else if (tmpSettings[j].vendor.compare("HikRobot", Qt::CaseInsensitive) == 0)//HIK robot camere
		{
			cam.push_back(new Hikrobot());
			selected = 2;
		}
		else if (tmpSettings[j].vendor.compare("Flir", Qt::CaseInsensitive) == 0)//Flir camera
		{
			cam.push_back(new CPointGray());
			selected = 3;
		}

		if (selected > 0)
		{
			cam.back()->customName = tmpSettings[j].customName;
			cam.back()->serialNumber = tmpSettings[j].serialNum;
			cam.back()->width = tmpSettings[j].width;
			cam.back()->height = tmpSettings[j].height;
			cam.back()->depth = tmpSettings[j].depth;
			cam.back()->FPS = tmpSettings[j].fps;
			cam.back()->trigger = tmpSettings[j].trigger;
			cam.back()->realTimeProcessing = tmpSettings[j].realTimeProcessing; //true -> kliče  v MBCameraView
			cam.back()->rotadedHorizontal = tmpSettings[j].rorated_horizontal;
			cam.back()->rotatedVertical = tmpSettings[j].rotated_vertical;
			cam.back()->num_images = tmpSettings[j].num_images;
			cam.back()->settingsPath = filePath;

			cam.back()->LoadReferenceSettings();

			if (cam.back()->Init() == 1)
			{
				if (cam.back()->Start())
				{
					cam.back()->SetExposureAbsolute(cam.back()->exposure[0]);
					cam.back()->SetGainAbsolute(cam.back()->gain[0]);
					cam.back()->SetPartialOffsetX(cam.back()->savedOffsetX[0]);
					cam.back()->SetPartialOffsetY(cam.back()->savedOffsetY[0]);
				}
			}
			if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera nif odprta, inicializiramo vse slike
				cam.back()->InitImages(cam.back()->width, cam.back()->height, cam.back()->depth);
		}

	}



}

void MBsoftware::ShowErrorMessage()
{
	QListWidgetItem *item;
	for (int i = 0; i < logIndex.size(); i++)
	{
		if (logIndex[i] == 0) //status ok - zelena
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(150, 255, 150));
		}
		else if (logIndex[i] == 1) //napaka na napravi - rdeca
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(255, 150, 150));
		}
		else if (logIndex[i] == 2) //warning
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(Qt::yellow));
		}
		else //info - modra
		{
			//rgb(0, 137, 207)
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(0, 137, 207));
		}
		item = ui.listWidgetStatus->item(i);
	}
	if (logIndex.size() > 8)
	{
		ui.listWidgetStatus->scrollToItem(item);
	}
}

void MBsoftware::ClearErrorMessage(int index)
{
	ui.listWidgetStatus->item(index)->setBackgroundColor(QColor(236, 236, 236));
	ui.listWidgetStatus->item(index)->setText("");
	//log[0]->ClearWindowMessage(index);
}

void MBsoftware::AppendClearErrorMessageIndex(int index)
{
	int indexExists = 0;
	for (int i = 0; i < clearLogIndex.size(); i++)
	{
		if (clearLogIndex[i] == index)
		{
			indexExists = 1;
			break;
		}
	}
	if (indexExists == 0)
		clearLogIndex.push_back(index);
}


void MBsoftware::DrawMeasurements()
{
	float value;
	QString valueTmp;


	QColor goodColor = QColor(150, 255, 150);
	QColor badColor = QColor(255, 150, 150);
	QColor NotActiveColor = QColor(236, 236, 236);
	QColor conditionalColor = QColor(0, 170, 255);


	float badProc;
	for (int i = 0; i < NR_STATIONS; i++)
	{

		if (types[i].size() > 0)
		{
			modelTable->rowCount();
			for (int row = 0; row < modelTable->rowCount(); row++)
			{
				for (int column = 0; column < modelTable->columnCount(); column++)
				{
					switch (column)
					{
					case 0:
						standardTableItem[row][column]->setText(QString("%1").arg(types[i][currentType]->name[row]));
						break;
					case 1:
						//standardTableItem[row][column]->setText(QString("%1").arg(10));
						standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->measuredValueDMS[row][0], 'f', 2));
						break;
					case 2:
						//standardTableItem[row][column]->setText(QString("%1").arg(10));
						standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->measuredValueDMS[row][1], 'f', 2));
						break;
					case 3:
						//standardTableItem[row][column]->setText(QString("%1").arg(10));
						standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->measuredValueDMS[row][2], 'f', 2));
						break;
					case 4:
						//standardTableItem[row][column]->setText(QString("%1").arg(10));
						standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->measuredValueDMS[row][3], 'f', 2));
						break;
					case 5:
						//standardTableItem[row][column]->setText(QString("%1").arg(10));
						standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->measuredValueDMS[row][4], 'f', 2));
						break;
					case 6:

						if (types[i][currentType]->nominal[row] == 0)
							standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->toleranceLow[row],'f',2));
						else
							standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->toleranceLow[row] + types[i][currentType]->nominal[row],'f', 2));
						break;

					case 7:
						if (types[i][currentType]->nominal[row] == 0)
							standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->toleranceHigh[row], 'f', 2));
						else
							standardTableItem[row][column]->setText(QString("%1").number(types[i][currentType]->toleranceHigh[row] + types[i][currentType]->nominal[row],'f', 2));
						break;
						
					case 8:
						if (types[i][currentType]->totalMeasurementsCounter == 0)
						{
							badProc = 0;
						}
						else
						{
							badProc = (float)types[i][currentType]->badCounterByParameter[row] / types[i][currentType]->totalMeasurementsCounter;
							badProc *= 1000;
							badProc = roundf(badProc);
							badProc /= 10;
						}
						standardTableItem[row][column]->setText(QString("%1").arg(badProc));
						break;

					default:
						break;
					}

				}

			}
			int rowGood = 1;
			//obarva vrstice dobre slabe
			for (int row = 0; row < modelTable->rowCount(); row++)
			{
				rowGood = 1;
				types[i][currentType]->IsGood(row);
				if (types[i][currentType]->isActive[row])
				{

					for (int k = 0; k < 5; k++)
					{
						if (types[i][currentType]->isGoodDMS[row][k] == 1) //mera dobra
						{
							standardTableItem[row][k + 1]->setBackground(QBrush(goodColor));

						}
						else if (types[i][currentType]->isGoodDMS[row][k ] == 0)// mera slaba
						{
							standardTableItem[row][k + 1]->setBackground(QBrush(badColor));
						}
						else//pogojno dobra
						{
							standardTableItem[row][k + 1]->setBackground(QBrush(conditionalColor));
						}
					}

					/*for (int k = 6; k < 9; k++)
					{
						standardTableItem[row][k]->setBackground(QBrush(NotActiveColor));
					}*/
					if (rowGood == 1)
					{
						standardTableItem[row][0]->setBackground(QBrush(goodColor));
					}
					else
					{
						standardTableItem[row][0]->setBackground(QBrush(badColor));
					}

				}
				else//not active
				{
					for (int k = 0; k < 6; k++)
					{
						standardTableItem[row][k]->setBackground(QBrush(NotActiveColor));
					}
				}

			}
		}
	}

}


void MBsoftware::PopulateStatusBox()
{
	int index = 0;

	index = 0;
	statusBox[0]->AddItem(index, QString("MM timer:  %1 Hz").arg(MMTimer.frequency));
	index++;
	statusBox[0]->AddItem(index, QString("View timer:  %1 Hz").arg(viewTimerTimer.frequency));
	index++;

	for (int i = 0; i < cam.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("cam %1 FPS:  %2").arg(i).arg(cam[i]->frameReadyFreq.frequency));
		index++;
	}
	for (int i = 0; i < cam.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("cam %1 TriggerCounter:  %2").arg(i).arg(cam[i]->triggerCounter));
		index++;
	}
	for (int i = 0; i < imageProcess->processingTimer.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("Obdelave%1 : %2 [ms]").arg(i).arg(imageProcess->processingTimer[i]->elapsed));
		index++;
	}
	for (int i = 0; i < TestFun.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("GlavniStevec%1 : %2 [ms]").arg(i).arg(TestFun[i]->elapsed));
		index++;
	}
	statusBox[0]->AddItem(index, QString("selectedCalibration: %1").arg(measureObject.selectedCalibration));
	index++;

	statusBox[0]->AddItem(index, QString("SenzorRotCounter: %1").arg(rotatorSenCounter));
	index++;


	statusBox[0]->AddItem(index, QString("Alarmstatus: %1").arg(alarmStatus));
	index++;


	statusBox[0]->AddItem(index, QString("dowelOnCounter: %1").arg(dowelOnCounter));
	index++;


	statusBox[0]->AddItem(index, QString("elevatorCounter: %1").arg(elevatorCounter));
	index++;

	statusBox[0]->AddItem(index, QString("elevatorStatus: %1").arg(elevatorStatus));
	index++;
	statusBox[0]->AddItem(index, QString("presureSensorCounter: %1").arg(presureSensorCounter));
	index++;

	statusBox[0]->AddItem(index, QString("volumeOfDowel: %1").arg(measureObject.volumeOfDowel));
	index++;

	statusBox[0]->AddItem(index, QString("Blow valve time: %1").arg(measureObject.timeToBlow));
	index++;

	statusBox[0]->AddItem(index, QString("time to blow : %1").arg(measureObject.delayToValve));
	index++;

	statusBox[0]->AddItem(index, QString("deviceStatus: %1").arg(deviceStatus));
	index++;
	statusBox[0]->AddItem(index, QString("cleanPeriodeCounter: %1").arg(cleanPeriodeCounter));
	index++;
	statusBox[0]->AddItem(index, QString("presortCounterBadinLine: %1").arg(presortCounterBadinLine));
	index++;
	statusBox[0]->AddItem(index, QString("dmsModulErrorCounter: %1").arg(dmsModulErrorCounter));
	index++;
	statusBox[0]->AddItem(index, QString("xilinxVersion: %1").arg(dmsUDP->xilinxVersion));
	index++;
	statusBox[0]->AddItem(index, QString("presortPieceCount: %1").arg(presortPieceCount));
	index++;

	statusBox[0]->AddItem(index, QString("presortCameraDelay: %1").arg(presortDelay));
	index++;
	statusBox[0]->AddItem(index, QString("presortCameraBlowTime: %1").arg(presortTime));
	index++;
	statusBox[0]->AddItem(index, QString("presortSpeedAvrage: %1").arg(presortSpeedAvrage));
	index++;
	statusBox[0]->AddItem(index, QString("Calculated presortSpeed: %1").arg(measureObject.presortDowelSpeed));
	index++;
	statusBox[0]->AddItem(index, QString("Used presort Speed: %1").arg(presortCameraSpeedUsed));
	index++;
	
}



void MBsoftware::EnableEMGdialog(bool enable)
{
	EMGwindow = new(QWidget);
	if (enable)
	{
		uiEMG.setupUi(EMGwindow);

		Qt::WindowFlags flags = windowFlags();
		Qt::WindowFlags closeFlag = Qt::WindowCloseButtonHint;
		Qt::WindowFlags minimizeFlag = Qt::WindowMinimizeButtonHint;
		Qt::WindowFlags maximiize = Qt::WindowMaximizeButtonHint;
		flags = flags & (~closeFlag);
		flags = flags & (~minimizeFlag);
		flags = flags & (~maximiize);
		EMGwindow->setWindowFlags(flags);
		EMGwindow->activateWindow();

		//EMGwindow->setWindowModality(Qt::ApplicationModal);

		connect(uiEMG.buttonReset, SIGNAL(pressed()), this, SLOT(OnClickedEMGReset()));
	}
}

void MBsoftware::ValveTestFunction()
{
	//tukaj lahko tudi testiramo venitile za pihanje 
	if (testValveB > -1)
	{
		lpt[0]->SetOutputValue(2, 1);
		if (testValveB == 0)
		{
			lpt[0]->SetOutputValue(2, 0);
		}
		testValveB--;
	}

	if (testValveC > -1)
	{
		lpt[0]->SetOutputValue(7, 1);

		if (testValveC == 0)
		{
			lpt[0]->SetOutputValue(7, 0);
		}
		testValveC--;
	}
	if (testValveX > -1)
	{
		lpt[1]->SetOutputValue(6, 1);

		if (testValveX == 0)
		{
			lpt[1]->SetOutputValue(6, 0);
		}
		testValveX--;
	}
	if (testValveY > -1)
	{
		lpt[0]->SetOutputValue(6, 1);

		if (testValveY == 0)
		{
			lpt[0]->SetOutputValue(6, 0);
		}
		testValveY--;
	}

	if (testValveV > -1)
	{
		if (testValveV % 50 == 0)
		{
			if (testValveVOn == 1)
			{
				testValveVOn = 0;
				dmsUDP->WriteDatagram("I0-0;");
			}
			else
			{
				testValveVOn = 1;
				dmsUDP->WriteDatagram("I0-3;");
			}

		}

		if (testValveV == 0)
		{
			dmsUDP->WriteDatagram("I0-0;");
		}
		testValveV--;

	}
}







void MBsoftware::SetLanguage()
{
	if (slovensko)
	{
		ui.labelDeviceStatus->setText("STANJE NAPRAVE");

	
/*
		ui.buttonLogin->setText("Prijava");
		//ui.buttonGeneralSettings->setText("Nastavitve");
		//ui.buttonResetGoodBox->setText("Ponastavi stevec kosov v skatli");
		ui.buttonSetTolerance->setText("Tolerance");
		ui.buttonTypeSelect->setText("Izberi tip");
		ui.buttonAddType->setText("Dodaj tip");
		ui.buttonTypeSettings->setText("Nastavitve tipa");
		ui.buttonRemoveType->setText("Odstrani tip");
		ui.buttonResetCounters->setText("Ponastavi stevce");

		ui.buttonImageProcessing->setText("Obdelava slik");

		ui.buttonHistory->setText("Zgodovina");
		ui.buttonStatusBox->setText("Podatki");
		ui.buttonAboutUs->setText("Kontakt");

		ui.MainMenuBar->setTabText(0, "Prijava");
		//ui.MainMenuBar->setTabText(1, "Nastavitve");
		ui.MainMenuBar->setTabText(1, "Tipi");
		ui.MainMenuBar->setTabText(2, "Kamere");
		ui.MainMenuBar->setTabText(3, "Signali");
		ui.MainMenuBar->setTabText(4, "Obdelava slik");
		ui.MainMenuBar->setTabText(5, "Podatki in zgodovina");*/
	}

	//Angleških imen ni treba posebej nastavljati, ker so že nastavljena v ui
}



void MBsoftware::DrawImageOnScreen()
{
	if (!isSceneAdded)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			MBsoftware::showScene[i] = new QGraphicsScene(this);
		}
		ui.imageViewMainWindow_0->setScene(MBsoftware::showScene[0]);
		MBsoftware::scenePresort = new QGraphicsScene(this);
		MBsoftware::scenePresortLive = new QGraphicsScene(this);

		ui.imageViewPresort->setScene(MBsoftware::scenePresort);
		ui.imageViewPresortLive->setScene(MBsoftware::scenePresortLive);

	



		/*pixmapPresort = new QGraphicsPixmapItem();
		pixmapPresortLive = new QGraphicsPixmapItem();

	
		scenePresort->addItem(pixmapPresort);
		scenePresortLive->addItem(pixmapPresortLive);*/

		isSceneAdded = true;
	}
	for (int i = 0; i < NR_STATIONS; i++)
	{
		qDeleteAll(showScene[i]->items());
	}

	ui.imageViewMainWindow_0->setAlignment(Qt::AlignTop | Qt::AlignLeft);

	ui.imageViewMainWindow_0->fitInView(QRectF(0, 0, 1280, 720), Qt::IgnoreAspectRatio);
	

	//ui.imageViewPresort->fitInView(QRectF(0, 0, imageProcess->drawImageMain[1].cols, imageProcess->drawImageMain[1].rows), Qt::KeepAspectRatio);
	//ui.imageViewPresort->resetTransform();
	//ui.imageViewPresort->fitInView(scenePresort->sceneRect(), Qt::KeepAspectRatio);
	//ui.imageViewPresort->setSceneRect(ui.imageViewPresort->rect());

	//ui.imageViewPresortLive->fitInView(scenePresortLive->sceneRect(), Qt::KeepAspectRatio);
	//ui.imageViewPresortLive->setSceneRect(ui.imageViewPresortLive->rect());

	ui.imageViewPresortLive->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	ui.imageViewPresort->setAlignment(Qt::AlignTop | Qt::AlignLeft);

}

void MBsoftware::UpdateImageView(int station)//presort 
{

		if (station == 0)//live
		{
			qDeleteAll(scenePresortLive->items());

			QImage qimgOriginal((uchar*)imageProcess->drawImagePresortAll.data, imageProcess->drawImagePresortAll.cols, imageProcess->drawImagePresortAll.rows, imageProcess->drawImagePresortAll.step, QImage::Format_RGB888);

			scenePresortLive->addPixmap(QPixmap::fromImage(qimgOriginal));
			ui.imageViewPresortLive->fitInView(QRectF(0, 0, imageProcess->drawImagePresortAll.cols, imageProcess->drawImagePresortAll.rows), Qt::IgnoreAspectRatio);
		

		}
		else	if (station == 1) //prikaz slabe slike
		{
			if (badPresortType > 0)
			{
				qDeleteAll(scenePresort->items());
				QImage qimgOriginal((uchar*)imageProcess->drawImagePresortBad.data, imageProcess->drawImagePresortBad.cols, imageProcess->drawImagePresortBad.rows, imageProcess->drawImagePresortBad.step, QImage::Format_RGB888);

				scenePresort->addPixmap(QPixmap::fromImage(qimgOriginal));
				ui.imageViewPresort->fitInView(QRectF(0, 0, imageProcess->drawImagePresortBad.cols, imageProcess->drawImagePresortBad.rows), Qt::IgnoreAspectRatio);
			}

		}
		else//kadar je zamenjava working moda pobrisem sliko
		{
			qDeleteAll(scenePresort->items());
		}
		//UpdateDowelImage(0);
}

void MBsoftware::CopyThread(int currPiece)
{

}

void MBsoftware::CreateDowelImage()
{
	//QGraphicsTextItem*	maxErrorText[6];
	//QGraphicsTextItem*  negativeErrorText[6];
	//QGraphicsTextItem* nrMeasurementes[6];
	QGraphicsLineItem* line[6];
	QColor badColor = QColor(255, 150, 150);
	QColor goodColor = QColor(150, 255, 150);
	QPen linePen(QColor(68, 114, 196), 6, Qt::DashDotLine, Qt::RoundCap);

	QString path;
	//path = QDir::currentPath() + "/res/dowel4.bmp";
	path = QDir::currentPath() + "/res/slikaDowel.bmp";
	QImage image;
	image.load(path, 0);
	QGraphicsPixmapItem * slika;
	slika = showScene[0]->addPixmap(QPixmap::fromImage(image));
	ui.imageViewMainWindow_0->fitInView(QRectF(showScene[0]->sceneRect()), Qt::IgnoreAspectRatio);

	int currentPiece = 1;

	QPolygon areaTmp[11];


	QFont timesNewRomanFont("Times New Roman", 40, QFont::Bold);

	QLine vertLine[4], horLine[2];
	int offsety = 194 - 102;
	//start konus Top
	areaTmp[0].append(QPoint(159, 200));
	areaTmp[0].append(QPoint(258, 102));
	areaTmp[0].append(QPoint(258, 200));

	//start konus BOttom
	areaTmp[1].append(QPoint(159, 337));
	areaTmp[1].append(QPoint(258, 337));
	areaTmp[1].append(QPoint(258, 434));

	//EDGE KONUS START  TOP 
	areaTmp[2].append(QPoint(370, 102));
	areaTmp[2].append(QPoint(370, 200));
	areaTmp[2].append(QPoint(257, 200));
	areaTmp[2].append(QPoint(257, 102));

	//EDGE KONUS START BOTTOM
	areaTmp[3].append(QPoint(257, 337));
	areaTmp[3].append(QPoint(370, 337));
	areaTmp[3].append(QPoint(370, 434));
	areaTmp[3].append(QPoint(257, 434));

	//MIDDLE TOP
	areaTmp[4].append(QPoint(370, 102));
	areaTmp[4].append(QPoint(917, 102));
	areaTmp[4].append(QPoint(917, 200));
	areaTmp[4].append(QPoint(370, 200));


	//MIDDLE BOTTOM
	areaTmp[5].append(QPoint(370, 337));
	areaTmp[5].append(QPoint(917, 337));
	areaTmp[5].append(QPoint(917, 434));
	areaTmp[5].append(QPoint(370, 434));

	//EDGE KONUS STOP UP
	areaTmp[6].append(QPoint(917, 102));
	areaTmp[6].append(QPoint(1023, 102));
	areaTmp[6].append(QPoint(1023, 200));
	areaTmp[6].append(QPoint(917, 200));

	//EDGE KONUS STOP BOTTOM
	areaTmp[7].append(QPoint(917, 337));
	areaTmp[7].append(QPoint(1023, 337));
	areaTmp[7].append(QPoint(1023, 434));
	areaTmp[7].append(QPoint(917, 434));

	//KONUS STOP UP
	areaTmp[8].append(QPoint(1023, 102));
	areaTmp[8].append(QPoint(1120, 200));
	areaTmp[8].append(QPoint(1023, 200));

	//KONUS STOP BOTTOM
	areaTmp[9].append(QPoint(1023, 337));
	areaTmp[9].append(QPoint(1120, 337));
	areaTmp[9].append(QPoint(1023, 434));

	//LENGHT AREA
	areaTmp[10].append(QPoint(160, 200));
	areaTmp[10].append(QPoint(1120, 200));
	areaTmp[10].append(QPoint(1120, 337));
	areaTmp[10].append(QPoint(160, 337));

	vertLine[0].setLine(257, 10, 257, 460);
	vertLine[1].setLine(370, 10, 370, 460);
	vertLine[2].setLine(917, 10, 917, 460);
	vertLine[3].setLine(1023, 10, 1023, 460);
	horLine[0].setLine(30, 200, 1250, 200);
	horLine[1].setLine(30, 337, 1250, 337);

	for (int i = 0; i < 11; i++)
	{
		area[i] = showScene[0]->addPolygon(areaTmp[i], QPen(goodColor), QBrush(goodColor));
		
	}
	area[1]->setBrush(badColor);
	area[2]->setBrush(goodColor);
	area[3]->setBrush(badColor);
	area[4]->setBrush(goodColor);
	area[5]->setBrush(badColor);
	area[6]->setBrush(goodColor);
	area[7]->setBrush(badColor);
	area[8]->setBrush(goodColor);
	area[9]->setBrush(badColor);
	area[10]->setBrush(goodColor);
	for (int i = 0; i < 4; i++)
	{
		line[i] = showScene[0]->addLine(vertLine[i], linePen);
		line[i]->setZValue(1);
	}
	line[4] = showScene[0]->addLine(horLine[0], linePen);
	line[5] = showScene[0]->addLine(horLine[1], linePen);
	line[4]->setZValue(1);
	line[5]->setZValue(1);


	timesNewRomanFont.setPointSize(30);
	QGraphicsTextItem *textE = showScene[0]->addText("Err.:", timesNewRomanFont);
	textE->setPos(10, 20);

	maxErrorText[0] = showScene[0]->addText("5 / 3", timesNewRomanFont);
	maxErrorText[0]->setPos(160, 5);
	negativeErrorText[0] = showScene[0]->addText("0 / 1", timesNewRomanFont);
	negativeErrorText[0]->setPos(160, 40);

	maxErrorText[1] = showScene[0]->addText("2 / 5", timesNewRomanFont);
	maxErrorText[1]->setPos(270, 5);
	negativeErrorText[1] =showScene[0]->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[1]->setPos(270, 40);

	maxErrorText[2] = showScene[0]->addText("2 / 5", timesNewRomanFont);
	maxErrorText[2]->setPos(580, 5);
	negativeErrorText[2] = showScene[0]->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[2]->setPos(580, 40);

	maxErrorText[3] = showScene[0]->addText("2 / 5", timesNewRomanFont);
	maxErrorText[3]->setPos(930, 5);
	negativeErrorText[3] = showScene[0]->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[3]->setPos(930, 40);

	maxErrorText[4] = showScene[0]->addText("2 / 5", timesNewRomanFont);
	maxErrorText[4]->setPos(1040, 5);
	negativeErrorText[4] = showScene[0]->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[4]->setPos(1040, 40);

	maxErrorText[5] = showScene[0]->addText("2 / 5", timesNewRomanFont);
	maxErrorText[5]->setPos(1150, 230);
	negativeErrorText[5] = showScene[0]->addText("1 / 3", timesNewRomanFont);
	negativeErrorText[5]->setPos(1150, 270);


	timesNewRomanFont.setPointSize(25);
	nrMeasurementesText[0] = showScene[0]->addText("00", timesNewRomanFont);
	nrMeasurementesText[0]->setPos(200,340);
	nrMeasurementesText[1] = showScene[0]->addText("11", timesNewRomanFont);
	nrMeasurementesText[1]->setPos(285, 340);
	nrMeasurementesText[2] = showScene[0]->addText("22", timesNewRomanFont);
	nrMeasurementesText[2]->setPos(590, 340);
	nrMeasurementesText[3] = showScene[0]->addText("33", timesNewRomanFont);
	nrMeasurementesText[3]->setPos(940, 340);
	nrMeasurementesText[4] = showScene[0]->addText("44", timesNewRomanFont);
	nrMeasurementesText[4]->setPos(1050, 340);
	nrMeasurementesText[5] = showScene[0]->addText("55", timesNewRomanFont);
	nrMeasurementesText[5]->setPos(200, 220);

	timesNewRomanFont.setPointSize(35);
	speedMeasurementText = showScene[0]->addText("Speed: 1.23 [m/s]", timesNewRomanFont);
	speedMeasurementText->setPos(820, 580);

	timesNewRomanFont.setPointSize(30);
	lenghtMeasurementText = showScene[0]->addText("Lenght = 30.00 [mm]", timesNewRomanFont);
	lenghtMeasurementText->setPos(500, 230);

	maxMeasurementText = showScene[0]->addText("Measurements: 123 MIN: 90 MAX: 150", timesNewRomanFont);
	maxMeasurementText->setPos(10, 580);

	elipseText = showScene[0]->addText("Elipse: 0.5 [mm]", timesNewRomanFont);
	elipseText->setPos(170, 500);


}

void MBsoftware::UpdateDowelImage(int currPiece)
{
	TestFun[1]->SetStart();
	QString tmp;

	QColor badColor = QColor(255, 150, 150);
	QColor goodColor = QColor(150, 255, 150);
	QFont calibriFont("TImes New Roman", 40, QFont::Bold);

	tmp = QString("%1").number(measureObject.dowelSpeed[currPiece], 'f', 2);
	speedMeasurementText->setPlainText(QString("Speed: %1 [m/s]").arg(tmp));

	tmp = QString("%1").number(measureObject.dowelLenght[currPiece], 'f', 2);
	lenghtMeasurementText->setPlainText(QString("Lenght: %1 [mm]").arg(tmp));


	maxMeasurementText->setPlainText(QString("Measure: %1 MIN: %2 MAX: %3 ").arg(measureObject.nrDowelMeasurements[currPiece]).arg(measureObject.dowelAllowedMeasurements[currPiece][0]).arg(measureObject.dowelAllowedMeasurements[currPiece][1]));

	nrMeasurementesText[0]->setPlainText(QString("%1").arg(measureObject.nrMeasurementsArea0[currPiece][1]));
	nrMeasurementesText[1]->setPlainText(QString("%1").arg(measureObject.nrMeasurementsArea1[currPiece][1]));
	nrMeasurementesText[2]->setPlainText(QString("%1").arg(measureObject.nrMeasurementsArea2[currPiece][1]));
	nrMeasurementesText[3]->setPlainText(QString("%1").arg(measureObject.nrMeasurementsArea3[currPiece][1]));
	nrMeasurementesText[4]->setPlainText(QString("%1").arg(measureObject.nrMeasurementsArea4[currPiece][1]));
	//dodaj se za meritev dolzin 
	//nrMeasurementesText[5]->setPlainText(QString("%1").arg(measureObject.nrMeasurementsArea5[currPiece][1]));


	int maxAll[5] = { 0,0,0,0,0 };
	int maxMinus[5] = { 0,0,0,0,0 };
	for (int i = 1; i < 6; i++)
	{
		if (measureObject.nrBadMeasurementsDSKAll[currPiece][i] > maxAll[0])
			maxAll[0] = measureObject.nrBadMeasurementsDSKAll[currPiece][i];

		if (measureObject.nrBadMeasurementsDSKMinus[currPiece][i] > maxMinus[0])
			maxMinus[0] = measureObject.nrBadMeasurementsDSKMinus[currPiece][i];

		if (measureObject.nrBadMeasurementsDISAll[currPiece][i] > maxAll[1])
			maxAll[1] = measureObject.nrBadMeasurementsDISAll[currPiece][i];

		if (measureObject.nrBadMeasurementsDISMinus[currPiece][i] > maxMinus[1])
			maxMinus[1] = measureObject.nrBadMeasurementsDISMinus[currPiece][i];

		if (measureObject.nrBadMeasurementsDICAll[currPiece][i] > maxAll[2])
			maxAll[2] = measureObject.nrBadMeasurementsDICAll[currPiece][i];

		if (measureObject.nrBadMeasurementsDICMinus[currPiece][i] > maxMinus[2])
			maxMinus[2] = measureObject.nrBadMeasurementsDICMinus[currPiece][i];

		if (measureObject.nrBadMeasurementsDIEAll[currPiece][i] > maxAll[3])
			maxAll[3] = measureObject.nrBadMeasurementsDIEAll[currPiece][i];

		if (measureObject.nrBadMeasurementsDIEMinus[currPiece][i] > maxMinus[3])
			maxMinus[3] = measureObject.nrBadMeasurementsDIEMinus[currPiece][i];

		if (measureObject.nrBadMeasurementsDKEAll[currPiece][i] > maxAll[4])
			maxAll[4] = measureObject.nrBadMeasurementsDKEAll[currPiece][i];

		if (measureObject.nrBadMeasurementsDKEMinus[currPiece][i] > maxMinus[4])
			maxMinus[4] = measureObject.nrBadMeasurementsDKEMinus[currPiece][i];
	}

		maxErrorText[0]->setPlainText(QString("%1 / %2").arg(maxAll[0]).arg(measureObject.nrEKonusAll));
		negativeErrorText[0]->setPlainText(QString("%1 / %2").arg(maxMinus[0]).arg(measureObject.nrEKonusMinus));
	

		maxErrorText[1]->setPlainText(QString("%1 / %2").arg(maxAll[1]).arg(measureObject.nrEEdgeAll));
		negativeErrorText[1]->setPlainText(QString("%1 / %2").arg(maxMinus[1]).arg(measureObject.nrEdgeMinus));

		maxErrorText[2]->setPlainText(QString("%1 / %2").arg(maxAll[2]).arg(measureObject.nrEMiddleAll));
		negativeErrorText[2]->setPlainText(QString("%1 / %2").arg(maxMinus[2]).arg(measureObject.nrEMiddleMinus));


		maxErrorText[3]->setPlainText(QString("%1 / %2").arg(maxAll[3]).arg(measureObject.nrEEdgeAll));
		negativeErrorText[3]->setPlainText(QString("%1 / %2").arg(maxMinus[3]).arg(measureObject.nrEdgeMinus));

		maxErrorText[4]->setPlainText(QString("%1 / %2").arg(maxAll[4]).arg(measureObject.nrEKonusAll));
		negativeErrorText[4]->setPlainText(QString("%1 / %2").arg(maxMinus[4]).arg(measureObject.nrEKonusMinus));

		maxErrorText[5]->setPlainText(QString("%1 / %2").arg(maxAll[4]).arg(measureObject.nrELenghtDowel));
		negativeErrorText[5]->setPlainText(QString("%1 / %2").arg(maxMinus[4]).arg(measureObject.nrELenghtDowelMinus));

		int count = 0;
		for (int i = 11; i < 16; i++)
		{
			if (measureObject.isGoodArray[currPiece][i] == 0)
			{
				maxErrorText[count]->setDefaultTextColor(badColor);
				negativeErrorText[count]->setDefaultTextColor(badColor);
			}
			else
			{
				maxErrorText[count]->setDefaultTextColor(Qt::black);
				negativeErrorText[count]->setDefaultTextColor(Qt::black);
			}
			count++;
		}

		for (int i = 0; i < 10; i++)
		{
			if (measureObject.isGoodArray[currPiece][i]  == 1)
				area[i]->setBrush(goodColor);
			else
				area[i]->setBrush(badColor);
		}
		if (measureObject.isGoodArray[currPiece][16] == 1)//okno za dolzino
			area[10]->setBrush(goodColor);
		else
			area[10]->setBrush(badColor);

		tmp = QString("%1").number(measureObject.mesurementElipse[currPiece], 'f', 2);
		elipseText->setPlainText(QString("Elipse: %1 , MAX: %2 [mm]").arg(tmp).arg(measureObject.setElipsse));

		if (measureObject.isGoodArray[currPiece][17] == 1)//elipse
		{
			elipseText->setDefaultTextColor(Qt::black);
		}
		else
		{
			elipseText->setDefaultTextColor(badColor);
		}
	
	

	TestFun[3]->SetStart();
	DrawMeasurements();

	TestFun[3]->SetStop();
	TestFun[3]->ElapsedTime();


	ui.imageViewMainWindow_0->fitInView(QRectF(0, 0, 1280, 720), Qt::IgnoreAspectRatio);


	//QtConcurrent::run(this, &MBsoftware::CopyThread, currPiece);

	
	/*QPixmap pixmap(parametersTable->size());
	pixmap = parametersTable->grab(QRect(QPoint(0,0),parametersTable->size()));

	measureObject.measurementTableImage[currPiece] = QImage(pixmap.toImage().convertToFormat(QImage::Format_ARGB32));
	QPixmap pixMap = ui.imageViewMainWindow_0->grab();
	measureObject.dowelImage[currPiece] = QImage(pixMap.toImage().convertToFormat(QImage::Format_ARGB32));
	*/
	
	TestFun[1]->SetStop();
	TestFun[1]->ElapsedTime();

	update();
}



void MBsoftware::ViewTimer()
{
	int bla = 10;

	QFont font;
	if (viewTimerTimer.timeCounter >= 10)
	{
		viewTimerTimer.SetStop();
		viewTimerTimer.CalcFrequency(viewTimerTimer.timeCounter);
		viewTimerTimer.timeCounter = 0;
		viewTimerTimer.SetStart();
	}

	viewTimerTimer.timeCounter++;
	viewCounter++;

	if (isActiveWindow())
	{
		grabKeyboard();
		//this->setFocus(Qt::StrongFocus);
	}
	else
		releaseKeyboard();


	if (settings->workingModePresort != workingModePresortPrev)
		UpdateImageView(2);
	workingModePresortPrev = settings->workingModePresort;


	float lenTol = (measureObject.lenghtTolerancePlus - measureObject.lenghtToleranceMinus) / 2;
	float konTol = (measureObject.konusTolerancePlus - measureObject.konusToleranceMinus) / 2;
	float widthTol = (measureObject.diameterTolerancePlus - measureObject.diameterToleranceMinus) / 2;

	CDMSstat::CheckSaveTime(types[0][currentType]->goodMeasurementsCounter, types[0][currentType]->badMeasurementsCounter, types[0][currentType]->valveOnCounter, measureObject.setLenght, lenTol, measureObject.setDiameter, widthTol, measureObject.setKonusLenght, konTol, QString("operater"));

	CDMSstat::CheckBackupVar();

	CDMSstat::CheckSavePermanentCounters();

	//if(ui.tabWidget->tabSta)
	if (ui.tabService->isVisible())
	{
		OnUpdateServiceTab();
	}

	if (boxFullStat == 1)
	{
		boxFullCounter++;
		if (boxFullCounter > 40)
		{
			boxFullCounter = 0;
			boxFullStat = 0;
			CDMSstat::FullBox(types[0][currentType]->boxCounter, 0, 0, types[0][currentType]->inBoxCounter, measureObject.setLenght, lenTol, measureObject.setDiameter, widthTol, 0, types[0][currentType]->inBoxCounter, QString("operater"));
		}
	}

	bool obnoviHistory = false;

	if (viewCounter % 100 == 0)
	{
		if (rotatorSpeedPrevValue != settings->rotatorSpeedMode)
		{
			rotatorSpeedPrevValue = settings->rotatorSpeedMode;
			settings->SaveSettings();
		}
	}
	QString  niz;
	if ((processStatus == -5))
	{

		if (!EMGwindow->isVisible())
		{

			uiEMG.buttonReset->hide();
			uiEMG.labelText->setStyleSheet(QStringLiteral("color: rgb(255, 0, 0);"));


			niz = QString("RELEASE BUTTON");
			uiEMG.labelText->setText(niz);

				EMGwindow->show();
		}
	}
	if ((processStatus == -4))
	{
		//ui.labelDeviceStatus->setText(QString("EMG STOP!"));
		//ui.labelDeviceStatus->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		uiEMG.buttonReset->show();
		uiEMG.labelText->setStyleSheet(QStringLiteral("color: rgb(0, 0, 255);"));



		  niz = QString("PRESS RESET TO CONFIRM!");

		uiEMG.labelText->setText(niz);

	}

		



	if (login->currentRights > 0) //uporabnik prijavljen
	{

		if (slovensko) niz = QString("prijavljen");
		else  niz = QString("logged-in");

		ui.labelLogin->setText(QString("%1 %2!").arg(login->currentUser).arg(niz));
		ui.labelLogin->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}
	else
	{

		if (slovensko) niz = QString("Uporabnik odjavljen");
		else  niz = QString("User logged out");

		ui.labelLogin->setText(niz);
		ui.labelLogin->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
	}

	if (dmsUDP->dmsConnected == 0) //uporabnik prijavljen
	{
		ui.labelDMSStatus->setText(QString("%1").arg("DMS MODUL NOT OK"));
		ui.labelDMSStatus->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
	}
	else
	{
		ui.labelDMSStatus->setText(QString("%1").arg("DMS MODUL OK"));
		ui.labelDMSStatus->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}

	if (processStatus == -5)
	{
		ui.labelStatusNaprave->setText(QString("EMG STOP!"));
		ui.labelStatusNaprave->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
	}
	else if (processStatus == -4)
	{
		ui.labelStatusNaprave->setText(QString("EMG STOP!"));
		ui.labelStatusNaprave->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
	}
	else if (processStatus == 0)
	{
		ui.labelStatusNaprave->setText(QString("Press Start!"));
		ui.labelStatusNaprave->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 0);"));
	}
	else if (processStatus == 1)
	{
		ui.labelStatusNaprave->setText(QString("Device ON!"));
		ui.labelStatusNaprave->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}

	if (elevatorStatus == 0)
	{
		ui.labelElevator->setText(QString("ELEVATOR OFF!"));
		ui.labelElevator->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
	}
	else if (elevatorStatus == 1)
	{
		ui.labelElevator->setText(QString("ELEVATOR ON!"));
		ui.labelElevator->setStyleSheet(QStringLiteral("background-color: rgb(0, 137, 207);"));
	}
	if (rotatorOn == 0)
	{
		ui.labelRotator->setText(QString("ROTOR OFF!"));
		ui.labelRotator->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
	}
	else if (rotatorOn == 1)
	{
		ui.labelRotator->setText(QString("ROTOR ON!"));
		ui.labelRotator->setStyleSheet(QStringLiteral("background-color: rgb(0, 137, 207);"));
	}

	ui.labelRotatorSpeed->setText(vfSettingsText);

	//stevci::
	float procent1 = 0;
	QString tmpString;
	if (types[0][currentType]->goodMeasurementsCounter > 0)
	{
		procent1 = (float)types[0][currentType]->goodMeasurementsCounter / types[0][currentType]->totalMeasurementsCounter * 100;
		tmpString = QString("%1").number(procent1, 'f', 2);
	}
	else
	{
		procent1 = 0;
		tmpString = "";
	}
	ui.labelGood->setText(QString("%1").arg(types[0][currentType]->goodMeasurementsCounter));
	ui.labelGoodProc->setText(QString("%1 %").arg(tmpString));

		if (types[0][currentType]->badMeasurementsCounter > 0)
		{
			procent1 = (float)types[0][currentType]->badMeasurementsCounter / types[0][currentType]->totalMeasurementsCounter * 100;
			tmpString = QString("%1").number(procent1, 'f', 2);
		}
		else
		{
			procent1 = 0;
			tmpString = "";
		}
	ui.labelBadProc->setText(QString("%1 %").arg(tmpString));
	ui.labelBad->setText(QString("%1").arg(types[0][currentType]->badMeasurementsCounter));
	ui.labelValve->setText(QString("%1").arg(types[0][currentType]->valveOnCounter));
	
		if(updateStatusRect == 1)
		{
			updateStatusRect = 0;
			ui.labelSelectedType->setText(types[0][currentType]->typeName);
			ui.labelAdvanceSettings->setText(types[0][currentType]->selectedAdvancedSettings);


			tmpString = QString("%1").number(measureObject.setLenght, 'f', 2);
			ui.LabelLenght->setText(QString("%1 mm").arg(tmpString));
			tmpString = QString("%1").number((measureObject.lenghtTolerancePlus - measureObject.lenghtToleranceMinus) / 2, 'f', 2);
			ui.LabelLenghtTol->setText(QString("+/-%1").arg(tmpString));

			tmpString = QString("%1").number(measureObject.setDiameter, 'f', 2);
			ui.labelWidth->setText(QString("%1 mm").arg(tmpString));
			tmpString = QString("%1").number((measureObject.diameterTolerancePlus - measureObject.diameterToleranceMinus) / 2, 'f', 2);
			ui.LabelWidthTol->setText(QString("+/-%1").arg(tmpString));

			tmpString = QString("%1").number(measureObject.setKonusLenght, 'f', 2);
			ui.labelKonus->setText(QString("%1 mm").arg(tmpString));
			tmpString = QString("%1").number((measureObject.konusTolerancePlus - measureObject.konusToleranceMinus) / 2, 'f', 2);
			ui.LabelKonusTol->setText(QString("+/-%1").arg(tmpString));



			if (types[0][currentType]->dowelWithConus == 1)
				ui.labelKonusOn->setText("With KONUS");
			else
				ui.labelKonusOn->setText("Without KONUS");
		}
		if (settings->updateDraw == 1)
		{
			settings->updateDraw = 1;


			if (settings->workingMode == 0)
				ui.labelWorkingMode->setText(QString("Nonstop"));
			else if (settings->workingMode == 1)
				ui.labelWorkingMode->setText(QString("Auto stop"));

			if (settings->workingModePresort == 0)
				ui.labelwWorkingmodePresort->setText(QString("OFF"));
			else if (settings->workingModePresort == 1)
				ui.labelwWorkingmodePresort->setText(QString("Small out"));
			else if (settings->workingModePresort == 2)
				ui.labelwWorkingmodePresort->setText(QString("Color out"));
			else if (settings->workingModePresort == 3)
				ui.labelwWorkingmodePresort->setText(QString("Color + small"));

			ui.labelPresortSettings->setText(types[0][currentType]->selectedColorCameraSettings);

			if (settings->presortSensorEnable == 0)
				ui.labelWorkingModePresortSensor->setText(QString("OFF "));
			else if (settings->presortSensorEnable == 1)
				ui.labelWorkingModePresortSensor->setText(QString("ON "));
		}

		//stevec skatel

		//ui.labelInB->setText(QString("ON "));


		ui.labelInBOX->setText(QString("%1").arg(types[0][currentType]->inBoxCounter));
		ui.labelNrBox->setText(QString("%1").arg(types[0][currentType]->boxCounter));

		if (settings->workingMode == 1)
		{
			ui.labelInBoxMax->setText(QString("%1").arg(settings->nrDowelToStop));

		}
		else if (settings->workingMode == 0)
		{
			uint32_t val = 0x221E;  //infinity
			QString symbolText = QChar(val);
			ui.labelInBoxMax->setText(symbolText);		
		}
			

	GetError();
	if(updateLogWindow == 1)
	{
		updateLogWindow = 0;
		ShowErrorMessage();
	}

	if (viewCounter % 2 == 0)
	{
		PopulateStatusBox();


	}

	if (viewCounter % 50 == 0)
	{
		types[0][currentType]->WriteCounters();
		SaveVariablesStatistics();

	}
	if (updateMeasurements == 1)
	{
		updateMeasurements = false;
		
	}


	for (int i = 0; i < 2; i++)
	{
		if (updateImage[i] == 1)
		{
			updateImage[i] = 0;
			

		}
	}

	//zacasni status boy
	int index;
	QString text = 0;
	index = 0;
	text = QString("SHOW DOWEL: %1 ").arg(showPiece);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("SCANNED DOWEL: %1 ").arg(measureObject.currentPiece);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("dowel Xilinx NR: %1 ").arg(measureObject.dowelNrXilinx[showPiece]);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("dowel nrMeasurements: %1 ").arg(measureObject.nrDowelMeasurements[showPiece]);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("dowelSpeed: %1 ").arg(measureObject.dowelSpeed[showPiece]);
	ui.statusList->item(index)->setText(text);
	index++;
	text = QString("dowelLenght: %1 ").arg(measureObject.dowelLenght[showPiece]);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("Allowed Measur. MIN: %1 MAX: %2 ").arg(measureObject.dowelAllowedMeasurements[showPiece][0]).arg(measureObject.dowelAllowedMeasurements[showPiece][1]);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("tmp UPLOADED DOWEL : %1 ").arg(uploadDowel);
	ui.statusList->item(index)->setText(text);
	index++;

	for (int i = 0; i < TestFun.size(); i++)
	{
		text = QString("TestFun Time[%1] : %2 ").arg(i).arg(TestFun[i]->elapsed);
		ui.statusList->item(index)->setText(text);
		index++;
	}
	text = QString("presorting Count : %1 ").arg(imageProcess->presortingCameraCounter);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("presorting Count Bad : %1 ").arg(imageProcess->presortingCameraCounterBad);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("presortSpeed : %1 [m/s]").arg(presortSpeed);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("presortSensorSpeedUsed : %1 [m/s]").arg(presortSensorSpeedUsed);
	ui.statusList->item(index)->setText(text);
	index++;

	text = QString("presortSensorTimeToBlow : %1 [ms]").arg(presortSensorTimeToBlow);
	ui.statusList->item(index)->setText(text);
	index++;
	text = QString("PresortLenghtLast : %1 [mm]").arg(presortSensorLenghtUsed);
	ui.statusList->item(index)->setText(text);
	index++;

	
	text = QString("presortLenght : %1 [mm]").arg(presortLenght);
	ui.statusList->item(index)->setText(text);
	index++;

	if (viewCounter % 3 == 0)
	{
		if (updateDraw > -1)
		{
			UpdateDowelImage(updateDraw);
			updateDraw = -1;
		}

	}
	if (viewCounter % 10 == 0)
	{
		OnUpdateBarChartData(0, measureObject.currentPiece);
		OnUpdateBarChartData(3, measureObject.currentPiece);
		OnUpdateBarChartData(1, measureObject.currentPiece);
		OnUpdateBarChartData(2, measureObject.currentPiece);
	}

	if ((viewCounter) % 20 == 0)
	{
		
		OnUpdateGaussChartData(0, measureObject.currentPiece);
		OnUpdateGaussChartData(1, measureObject.currentPiece);
		OnUpdateStatistics();
	}


	if ((viewCounter + 1) % 3 == 0)
	{
		if (imageProcess->updateImagePresortBad == 1)
		{
			imageProcess->updateImagePresortBad = 0;
			UpdateImageView(1);
		}

		if (imageProcess->updateImagePresortAll == 1)
		{
			imageProcess->updateImagePresortAll = 0;
			UpdateImageView(0);
		}

		//ui.labelPresort->setText(QString("Selected settings: %1").arg(types[0][currentType]->selectedColorCameraSettings));
		QString tmp;
		float procValue;
		if (types[0][currentType]->totalMeasurementsCounter == 0)
			procValue = 0;
		else
			procValue = (float)imageProcess->presortingCameraCounterBad / (float)types[0][currentType]->totalMeasurementsCounter;
		tmp = QString("%1").number(procValue, 'f', 2);
		//ui.labelPresort1->setText(QString("Bad presort: %1 Bad Proc: %2 %").arg(imageProcess->presortingCameraCounterBad).arg(tmp));

		int col;

		if (deviceStatus == 0)
		{
			col = 1;
			ui.labelStatusRect->setText(QString("STOP"));
		}
		if (deviceStatus == 1)
		{
			col = 1;
			ui.labelStatusRect->setText(QString("WORKING"));
		}
		else if (deviceStatus == 5)
		{
			col = 2;
			ui.labelStatusRect->setText(QString("ROTOR ERROR!"));
		}
		else if (deviceStatus == 6)
		{
			col = 3;
			ui.labelStatusRect->setText(QString("DEVICE EMPTY!"));
		}
		else if (deviceStatus == 7)
		{
			col = 4;
			ui.labelStatusRect->setText(QString("DOWEL STUCK!"));
		}
		else if (deviceStatus == 8)
		{
			col = 1;
			ui.labelStatusRect->setText(QString("AUTO-STOP BOX FULL!"));
		}
		else if (deviceStatus == 9)
		{
			col = 5;
			ui.labelStatusRect->setText(QString("PRESORT CAMERA STOP!"));
		}
		else if (deviceStatus == 10)
		{
			col = 6;
			ui.labelStatusRect->setText(QString("DMS MODUL ERROR!"));
		}

		if (deviceStatus < 5)
		{
			QString tmp;
			int diff = timeToFinishCleaning - cleanCounterOffset;
			float sec = (float)diff / 1000;
			tmp = QString("%1").number(sec, 'f', 1);
			if (warningStatus == 1)
			{
				col = 3;
				ui.labelStatusRect->setText(QString("NO DOWELS!"));
			}
			else if (warningStatus == 5)//cleaning in odstevanje //
			{
				col = 7;
				ui.labelStatusRect->setText(QString("CLEANING  %1 s").arg(tmp));
			}
			else if (warningStatus == 6)//cleaning in odstevanje //
			{
				col = 7;
				ui.labelStatusRect->setText(QString("CLEANING  %1 s").arg(tmp));
			}



		}

		QString  current = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ");

		ui.labelTimeAndDate->setText(current);

		if (col == 1)
		{
			ui.FrameStatus->setStyleSheet(QStringLiteral("background-color: rgb(0, 137, 207) ; outline:2px black;")); //default modra MBvision
		}
		else if (col == 2) //error
		{
			ui.FrameStatus->setStyleSheet(QStringLiteral("background-color: rgb(185, 60, 170) ; outline:2px black;")); //vijola
		}
		else if (col == 3) //warning
		{
			ui.FrameStatus->setStyleSheet(QStringLiteral("background-color: rgb(50, 30, 255) ; outline:2px black;"));//temno modra
		}
		else if (col == 4) //warning
		{
			ui.FrameStatus->setStyleSheet(QStringLiteral("background-color: rgb(250, 150, 130) ; outline:2px black;"));//skoraj rdeca
		}
		else if (col == 5) //warning
		{
			ui.FrameStatus->setStyleSheet(QStringLiteral("background-color: rgb(245, 130, 240) ; outline:2px black;"));//roza
		}
		else if (col == 6) //warning
		{
			ui.FrameStatus->setStyleSheet(QStringLiteral("background-color: rgb(0, 0, 0) ; outline:2px black;"));//crna
		}
		else if (col == 7) //warning
		{
			ui.FrameStatus->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150) ; outline:2px black;"));//zelena
		}
		else
			ui.FrameStatus->setStyleSheet(QStringLiteral("background-color: rgb(0, 137, 207) ; outline:2px black;"));

		ui.labelGoodMin->setText(QString("%1").arg(goodPerMin));
		ui.labelBadMin->setText(QString("%1").arg(badPerMin));
		ui.labelAllMin->setText(QString("%1").arg(allPerMin));

		ui.labelAllHour->setText(QString("%1").arg(allPerHour));
	}

	

}
void MBsoftware::GetError()
{
	QString text;
	int index = 0;
	for (int i = 0; i < cam.size(); i++)
	{
		if ((cam[i]->isLive == 1) && (cam[i]->isLiveOld == false))
		{


			if (slovensko) text = QString("Kamera %1 POVEZANA").arg(i);
			else text = QString("Camera%1 ONLINE").arg(i);
			AddLog(text, 0);
			startErrorSet[index] = 0;
		}
		if ((cam[i]->isLive == 0) && (startErrorSet[index] == 0))
		{
			if (slovensko) text = QString("Kamera %1 NI povezana").arg(i);
			else text = QString("Camera %1 OFFLINE").arg(i);
			AddLog(text, 1);
			startErrorSet[index] = 1;
		}

		cam[i]->isLiveOld = cam[i]->isLive;
		index++;
	}
	for (int i = 0; i < uZed.size(); i++)
	{
		if ((uZed[i]->isConnected == 1) && (uZed[i]->isConnectedOld == 0))
		{
			if (slovensko) text = QString("Xilinx POVEZANA");
			else text = QString("Xilinx  ONLINE");

			AddLog(text, 0);
			startErrorSet[index] = 0;
		}
		if ((uZed[i]->isConnected == 0) && (startErrorSet[index] == 0))
		{
			if (slovensko) text = QString("Xilinx NI povezana");
			else text = QString("Xilinx OFFLINE");
			AddLog(text, 1);
			startErrorSet[index] = 1;
		}
		uZed[i]->isConnectedOld = uZed[i]->isConnected;
	}
	index++;




}


void MBsoftware::OnClickedShowLptButton(int lptNum)
{
	lpt[lptNum]->OnShowDialog();
}

void MBsoftware::OnClickedShowLptButton0()
{
	lpt[0]->OnShowDialog();
}

void MBsoftware::OnClickedShowLptButton1()
{
	lpt[1]->OnShowDialog();
}

void MBsoftware::OnClickedShowCamButton0()
{
	cam[0]->ShowDialog(login->currentRights);
}

void MBsoftware::OnClickedShowCamButton1()
{
	cam[1]->ShowDialog(login->currentRights);
}

void MBsoftware::OnClickedShowUZedButton(int cardNum)
{
	uZed[cardNum]->OnShowDialog();
}

void MBsoftware::OnClickedShowCamButton(int camNum)
{
	cam[camNum]->ShowDialog(login->currentRights);
}

void MBsoftware::OnClickedShowSmartCardButton(int cardNum)
{
	//controlCardMB[cardNum]->ShowDialog();
}

void MBsoftware::OnClickedShowImageProcessing()
{
	QStringList list;
	QMessageBox::StandardButton reply;
	int stNum = 0;
	if (login->currentRights > 0)
	{
		imageProcess->ShowDialog(login->currentRights);
	}

	else
	{
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
}

void MBsoftware::OnClickedShowDowelSettings(void)
{
//	uiDowelSettings.spinBoxLenght.set
	dowelSettingsDlg->currentType = currentType;
	dowelSettingsDlg->dowelWithConus = types[0][currentType]->dowelWithConus;

	for (int i = 0; i < dowelSettingsDlg->basicSettings.size(); i++)
	{
		dowelSettingsDlg->basicSettings[i].clear();
	}
	dowelSettingsDlg->basicSettings.resize(types[0].size());

	dowelSettingsDlg->typeName.clear();
	dowelSettingsDlg->typeAdvSettings.clear();
	dowelSettingsDlg->typeColorSettings.clear();

	for (int i = 0; i < types[0].size(); i++)
	{
		
		for (int j = 0; j < 9; j++)
		{
			dowelSettingsDlg->basicSettings[i].push_back(types[0][i]->typeDowelSetting[j]);

	

		}
		dowelSettingsDlg->typeName.push_back(types[0][i]->typeName);
		dowelSettingsDlg->typeAdvSettings.push_back(types[0][i]->selectedAdvancedSettings);
		dowelSettingsDlg->typeColorSettings.push_back(types[0][i]->selectedColorCameraSettings);
	}
	dowelSettingsDlg->ui.comboBoxSelectType->clear();
	for (int i = 0; i < types[0].size(); i++)
	{
		dowelSettingsDlg->ui.comboBoxSelectType->addItem(types[0][i]->typeName);
		
	}
	dowelSettingsDlg->ui.comboBoxSelectType->setCurrentIndex(currentType);


	//dowelSettingsDlg->settings.clear();
	//dowelSettingsDlg->ui.comboBoxSelectAdvancedSettings->clear();


	dowelSettingsDlg->selectedAdvSettings = types[0][currentType]->selectedAdvancedSettings;
	dowelSettingsDlg->selectedColorCameraSettings = types[0][currentType]->selectedColorCameraSettings;
	int sel = -1;
	//dowelSettingsDlg->settings.clear();
	for (int i = 0; i < dowelSettingsDlg->advSettings.size(); i++)
	{
		//dowelSettingsDlg->settings.push_back(advSettings[i]);
		//dowelSettingsDlg->ui.comboBoxSelectAdvancedSettings->addItem(advSettings[i]->name);

		if (dowelSettingsDlg->advSettings[i].name.compare(types[0][currentType]->selectedAdvancedSettings, Qt::CaseInsensitive) == 0)
		{
			sel = i;
		}
	}

	dowelSettingsDlg->ui.comboBoxSelectAdvancedSettings->setCurrentIndex(sel);
	

	dowelSettingsDlg->OnShowDialog(login->currentRights,currentType);


}

void MBsoftware::OnClickedShowDMScalibration(void)
{
	dmsCalibrationDlg->OnShowDialog(login->currentRights,measureObject.selectedCalibration,types[0][currentType]->typeDowelSetting[3], types[0][currentType]->typeDowelSetting[0]);
}

void MBsoftware::OnClickedShowPresortDialog(void)
{
	imageProcess->ShowDialogPresort(login->currentRights, measureObject.currPresortDowel,measureObject.selectedCalibration);
}

void MBsoftware::OnClickedSelectType(void)
{
	QStringList list;
	int stNum;
	int result;
	QMessageBox::StandardButton reply;

	if (login->currentRights > 0)
	{
			stNum = 0;

		DlgTypes dlg(this, types[stNum].size());

		for (int i = 0; i < types[stNum].size(); i++)
		{
			dlg.types[i] = types[stNum][i]->typeName;


		}
		result = dlg.ShowDialogSelectType(currentType);
		if (result >= 0)
		{
			prevType = currentType;
			currentType = result;
			SaveVariables();
			if (slovensko) AddLog("Tip spremenjen!", 3);
			else AddLog("Type changed!", 3);

			for (int i = 0; i < NR_STATIONS; i++)
			{

				types[i][currentType]->ResetCounters();
				types[i][currentType]->WriteCounters();

			}

			
		}
	}
	else
	{
		QString niz;

		if (slovensko) niz = QString("Za izbiro tipa morate biti prijavljeni! Se zelite prijaviti?");
		else niz = QString("You need to be logged in to select type. Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);

	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}  
void MBsoftware::OnClickedEditTypes(void)
{
	QMessageBox::StandardButton reply;
	QStringList list;
	int stNum = 0;
	if (login->currentRights > 0)
	{
		if (NR_STATIONS > 1)
		{
			for (int i = 0; i < NR_STATIONS; i++)
			{
				list.push_back(QString("%1").arg(i));
			}
			bool ok;
			QString text;

			if (slovensko)  text = QInputDialog::getItem(this, tr("Izberi postajo"), tr("Postaja:"), list, 0, false, &ok);
			else text = QInputDialog::getItem(this, tr("Select station"), tr("Station:"), list, 0, false, &ok);

			if (!ok) return;

			stNum = text.toInt();
		}
		else
			stNum = 0;
		types[stNum][currentType]->OnShowDialog(login->currentRights);


	}

	else
	{
		
		QString niz;

		if (slovensko) niz = QString("Za nastavljanje toleranc morate biti prijavljeni! Se zelite prijaviti?");
		else niz = QString("You need to be logged in to edit type. Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}
void MBsoftware::OnClickedRemoveTypes(void)
{
	bool ok;
	QMessageBox box;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString destPath;
	QString sourcePath;
	QString sourcePropertyPath;
	QString sourcePath2;
	QString destPath2;

	QMessageBox::StandardButton reply;


	if (login->currentRights == 1 || login->currentRights == 2)  //tipe lahko odstranjuje administrator ali operater, worker pa ne
	{

		if (types[0].size() > 1)
		{
			for (int i = 0; i < types[0].size(); i++)
				list.append(types[0][i]->typeName);

			dialog.setComboBoxItems(list);

			if (slovensko)
			{
				dialog.setWindowTitle("Izbris tipa");
				dialog.setLabelText("Izberi tip za izbris");
			}
			else
			{
				dialog.setWindowTitle("Delete type");
				dialog.setLabelText("Select type to remove");
			}

			if (dialog.exec() == IDOK)
			{

					tip = dialog.textValue();
					destPath = referencePath + QString("/%1/types/DeletedTypes").arg(REF_FOLDER_NAME);
					destPath2 = destPath;

					if (!QDir(destPath).exists())
						QDir().mkdir(destPath);

					destPath += QString("/%1_deleted.ini").arg(tip);
					destPath2 += QString("/counters_%1_deleted.txt").arg(tip);


					sourcePath = referencePath + QString("/%1/types/%2.ini").arg(REF_FOLDER_NAME).arg(tip);
					sourcePath2 = referencePath + QString("/%1/types/counters_%2.ini").arg(REF_FOLDER_NAME).arg(tip);



					QFile::copy(sourcePath, destPath);
					QFile::copy(sourcePath2, destPath2);


					QFile::remove(sourcePath);
					QFile::remove(sourcePath2);
	

					int cur = 0;
					for (int j = 0; j < NR_STATIONS; j++)
					{
						for (int i = 0; i < types[j].size(); i++)
						{
							if (tip == types[j][i]->typeName)
							{
								types[j].erase(types[j].begin() + i);
							}
						}
						types[j][currentType]->parametersChanged = true;
					}
					//izbrisemo tudi dynamicVariables za property tipov 
				/*	sourcePropertyPath = QDir::currentPath() + QString("/%1/dynamicVariables/types_Station%2/parameters_%3").arg(REF_FOLDER_NAME).arg(j).arg(tip);

					QDir propDir(sourcePropertyPath);
					propDir.removeRecursively();
					*/

					prevType = currentType;
					currentType = 0;

				
			}

		}
		else
		{
			box.setStandardButtons(QMessageBox::Ok);
			box.setIcon(QMessageBox::Warning);
			if (slovensko) box.setText("Ne morem izbrisati edinega tipa!");
			else box.setText("Unable to delete all types!");
			box.exec();
		}


	}
	else
	{
		QString niz;

		if (slovensko) niz = QString("Za brisanje tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to delete types! Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}
void MBsoftware::OnClickedRemoveTypesOld(void)
{
	bool ok;
	QMessageBox box;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString destPath;
	QString sourcePath;
	QString sourcePropertyPath;
	QString sourcePath2,sourcePath3;
	QString destPath2;
	QString destPath3;
	QMessageBox::StandardButton reply;
	

	if (login->currentRights == 1 || login->currentRights == 2)  //tipe lahko odstranjuje administrator ali operater, worker pa ne
	{

			if (types[0].size() > 1)
			{
				for (int i = 0; i < types[0].size(); i++)
					list.append(types[0][i]->typeName);

				dialog.setComboBoxItems(list);

				if (slovensko)
				{
					dialog.setWindowTitle("Izbris tipa");
					dialog.setLabelText("Izberi tip za izbris");
				}
				else
				{
					dialog.setWindowTitle("Delete type");
					dialog.setLabelText("Select type to remove");
				}

				if (dialog.exec() == IDOK)
				{
					for (int j = 0; j < NR_STATIONS; j++)
					{
						tip = dialog.textValue();
						destPath = QDir::currentPath() + QString("/%1/types/types_Station%2/DeletedTypes").arg(REF_FOLDER_NAME).arg(j);
						destPath2 = destPath;
						destPath3 = destPath;
						if (!QDir(destPath).exists())
							QDir().mkdir(destPath);

						destPath += QString("/%1_deleted.ini").arg(tip);
						destPath2 += QString("/counters_%1_deleted.txt").arg(tip);
						destPath3 += QString("/settings_%1_deleted.ini").arg(tip);

						sourcePath = QDir::currentPath() + QString("/%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(j);
						sourcePath2 = sourcePath + QString("/counters_%1.txt").arg(tip);
						sourcePath3 = sourcePath + QString("/Type_settings/settings_%1.ini").arg(tip);
						sourcePath += QString("/%1.ini").arg(tip);


						QFile::copy(sourcePath, destPath);
						QFile::copy(sourcePath2, destPath2);
						QFile::copy(sourcePath3, destPath3);

						QFile::remove(sourcePath);
						QFile::remove(sourcePath2);
						QFile::remove(sourcePath3);

						int cur = 0;
						for (int i = 0; i < types[j].size(); i++)
						{
							if (tip == types[j][i]->typeName)
							{
								types[j].erase(types[j].begin() + i);
							}
						}
						//izbrisemo tudi dynamicVariables za property tipov 
						sourcePropertyPath = QDir::currentPath() + QString("/%1/dynamicVariables/types_Station%2/parameters_%3").arg(REF_FOLDER_NAME).arg(j).arg(tip);

						QDir propDir(sourcePropertyPath);
						propDir.removeRecursively();


						prevType = currentType;
						currentType = 0;
						types[j][currentType]->parametersChanged = true;
					}
				}

			}
			else
			{
				box.setStandardButtons(QMessageBox::Ok);
				box.setIcon(QMessageBox::Warning);
				if (slovensko) box.setText("Ne morem izbrisati edinega tipa!");
				else box.setText("Unable to delete all types!");
				box.exec();
			}
		

	}
	else
	{
		QString niz;

		if (slovensko) niz = QString("Za brisanje tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to delete types! Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}
void MBsoftware::OnClickedResetGlobal()

{
	QMessageBox::StandardButton reply;


		//QMessageBox::StandardButton reply;
		QString niz;
		if (slovensko) niz = QString("Naj ponastavim stevce?");
		else niz = QString("Reset piece counters?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);

		if (reply == QMessageBox::Yes)
		{
			for (int i = 0; i < NR_STATIONS; i++)
			{

				types[i][currentType]->ResetCounters();
				types[i][currentType]->inBoxCounter = 0;
				types[i][currentType]->boxCounter = 0;
				
				types[i][currentType]->WriteCounters();

				OnResetStatistics();
				OnResetTimeCounters();
				CDMSstat::ResetBoxNumber();

			}
		}


}
void MBsoftware::OnClickedAddTypes(void)//zaenkrat kopira celoten TIP 
{
	bool ok;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString filePath;
	QMessageBox::StandardButton reply;

	if (login->currentRights == 1 || login->currentRights == 2)  //tipe lahko dodaja administrator ali operater, worker pa ne
	{

		if (slovensko)
		{
			dialog.setWindowTitle("Nov tip");
			dialog.setLabelText("Vnesi ime tipa:");
		}
		else
		{
			dialog.setWindowTitle("Add new type");
			dialog.setLabelText("Insert type name:");
		}

		if (dialog.exec() == IDOK)
		{
			tip = dialog.textValue();


				filePath = referencePath + QString("/%1/types/").arg(REF_FOLDER_NAME);

			for (int j = 0; j < NR_STATIONS; j++)
			{
				types[j].push_back(new Types(j));

				//types[j].back()->stationNumber = 0;
				types[j].back()->Init(tip, filePath);
				types[j].back()->InitSettingsWindow(filePath);

				for (int i = 0; i < types[j][currentType]->parameterCounter; i++)
				{
					types[j].back()->toleranceHigh.push_back(types[j][currentType]->toleranceHigh[i]);
					types[j].back()->toleranceLow.push_back(types[j][currentType]->toleranceLow[i]);
					types[j].back()->toleranceHighCond.push_back(types[j][currentType]->toleranceHighCond[i]);
					types[j].back()->toleranceLowCond.push_back(types[j][currentType]->toleranceLowCond[i]);
					types[j].back()->correctFactor.push_back(types[j][currentType]->correctFactor[i]);
					types[j].back()->offset.push_back(types[j][currentType]->offset[i]);
					types[j].back()->nominal.push_back(types[j][currentType]->nominal[i]);
					types[j].back()->isActive.push_back(types[j][currentType]->isActive[i]);
					types[j].back()->isConditional.push_back(types[j][currentType]->isConditional[i]);
					types[j].back()->name.push_back(types[j][currentType]->name[i]);
					types[j].back()->badCounterByParameter.push_back(types[j][currentType]->badCounterByParameter[i]);
					types[j].back()->dynamicParameters.resize(i + 1);
					for (int z = 0; z < types[j][currentType]->dynamicParameters[i].size(); z++)
					{
						types[j].back()->dynamicParameters[i].push_back((types[j][currentType]->dynamicParameters[i][z]));
					}

					types[j].back()->AddParameter(i);
					types[j].back()->measuredValue.push_back(0);
					types[j].back()->isGood.push_back(0);
					types[j].back()->conditional.push_back(0);

				}
				types[j].back()->parameterCounter = types[j][currentType]->parameterCounter;
				
				types[j].back()->WriteParameters();
				types[j].back()->WriteCounters();


				for (int i = 0; i < 4; i++)
				{
					for (int k = 0; k < 10; k++)
					{
						types[j].back()->setting[i][k] = types[j][currentType]->setting[i][k];
						types[j].back()->settingType[i][k] = types[j][currentType]->settingType[i][k];
						types[j].back()->settingText[i][k] = types[j][currentType]->settingText[i][k];
					}
					types[j].back()->groupBoxName[i] = types[j][currentType]->groupBoxName[i];
				}
				if(j == 0)
				types[j].back()->WriteTypeSettings();

				for (int i = 0; i < types[j][currentType]->prop.size(); i++)
				{
					types[j].back()->prop[i] = types[j][currentType]->prop[i];

				}
				imageProcess->ConnectTypes(j,types[j]);

				for (int i = 0; i < types[j][currentType]->prop.size(); i++)
				{
					imageProcess->SaveFunctionParametersOnNewType(types[j].size() - 1, i);
				}
			}



		}
	}
	else
	{
		QString niz;

		if (slovensko) niz = QString("Za dodajanje tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to add types! Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
}
void MBsoftware::OnSignalCreateTypeDMS(QString exsisting, QString newName)
{

	CopyType(exsisting, newName);

}
void MBsoftware::CopyType(QString exsisting, QString newName)
{
	int selected = -1;
	for (int i = 0; i < types[0].size(); i++)
	{
		if (exsisting.compare(types[0][i]->typeName) == 0)//kopiram izbran tip 
		{
			selected = i;
			break;
		}

	}

	if (selected == -1)
		selected = 0;
	types[0].push_back(new Types(0));

	types[0].back()->Init(newName, types[0][selected]->typePath);
	//types[0].back()->InitSettingsWindow(filePath);
	types[0].back()->measuredValueDMS.resize(types[0][selected]->parameterCounter);
	types[0].back()->isGoodDMS.resize(types[0][selected]->parameterCounter);



	for (int i = 0; i < types[0][selected]->parameterCounter; i++)
	{
		types[0].back()->toleranceHigh.push_back(types[0][selected]->toleranceHigh[i]);
		types[0].back()->toleranceLow.push_back(types[0][selected]->toleranceLow[i]);
		types[0].back()->toleranceHighCond.push_back(types[0][selected]->toleranceHighCond[i]);
		types[0].back()->toleranceLowCond.push_back(types[0][selected]->toleranceLowCond[i]);
		types[0].back()->correctFactor.push_back(types[0][selected]->correctFactor[i]);
		types[0].back()->offset.push_back(types[0][selected]->offset[i]);
		types[0].back()->nominal.push_back(types[0][selected]->nominal[i]);
		types[0].back()->isActive.push_back(types[0][selected]->isActive[i]);
		types[0].back()->isConditional.push_back(types[0][selected]->isConditional[i]);
		types[0].back()->name.push_back(types[0][selected]->name[i]);
		types[0].back()->badCounterByParameter.push_back(types[0][selected]->badCounterByParameter[i]);
		types[0].back()->dynamicParameters.resize(i + 1);
		for (int z = 0; z < types[0][selected]->dynamicParameters[i].size(); z++)
		{
			types[0].back()->dynamicParameters[i].push_back((types[0][selected]->dynamicParameters[i][z]));
		}

		types[0].back()->AddParameter(i);
		types[0].back()->measuredValue.push_back(0);

		types[0].back()->isGood.push_back(0);
		types[0].back()->conditional.push_back(0);

		for (int j = 0; j < 5; j++)
		{
			types[0].back()->measuredValueDMS[i].push_back(0);
			types[0].back()->isGoodDMS[i].push_back(0);
		}

	}

	for (int i = 0; i < 9; i++)
	{
		types[0].back()->typeDowelSetting[i] = types[0][selected]->typeDowelSetting[i];
	}
	types[0].back()->selectedAdvancedSettings = types[0][selected]->selectedAdvancedSettings;
	types[0].back()->selectedColorCameraSettings = types[0][selected]->selectedColorCameraSettings;
	types[0].back()->dowelWithConus = types[0][selected]->dowelWithConus;
	types[0].back()->parameterCounter = types[0][selected]->parameterCounter;

	types[0].back()->WriteParameters();
	types[0].back()->WriteCounters();


	for (int i = 0; i < 4; i++)
	{
		for (int k = 0; k < 10; k++)
		{
			types[0].back()->setting[i][k] = types[0][selected]->setting[i][k];
			types[0].back()->settingType[i][k] = types[0][selected]->settingType[i][k];
			types[0].back()->settingText[i][k] = types[0][selected]->settingText[i][k];
		}
		types[0].back()->groupBoxName[i] = types[0][selected]->groupBoxName[i];
	}

		types[0].back()->WriteTypeSettings();

	for (int i = 0; i < types[0][selected]->prop.size(); i++)
	{
		types[0].back()->prop[i] = types[0][selected]->prop[i];

	}
	imageProcess->ConnectTypes(0, types[0]);

	for (int i = 0; i < types[0][selected]->prop.size(); i++)
	{
		imageProcess->SaveFunctionParametersOnNewType(types[0].size() - 1, i);
	}

}
void MBsoftware::OnClickedTypeSettings(void)
{
	QMessageBox::StandardButton reply;
	QStringList list;
	int stNum = 0;
	
	if (login->currentRights == 1 || login->currentRights == 2)  //tipe lahko dodaja administrator ali operater, worker pa ne
	{
			stNum = 0;
		types[stNum][currentType]->OnShowDialogSettings(login->currentRights);
	}

	else
	{

		QString niz;

		if (slovensko) niz = QString("Za spreminjanje nastavitev tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to change type settings! Do you want to log in?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
}
void MBsoftware::OnClickedResetCounters(void)
{
	QMessageBox::StandardButton reply;

	//if (login->currentRights > 0)
	//{
		//QMessageBox::StandardButton reply;
		QString niz;
		if (slovensko) niz = QString("Naj ponastavim stevce?");
		else niz = QString("Reset piece counters?");

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,
			QMessageBox::Yes | QMessageBox::No);

		//reply = QMessageBox::Yes;

		if (reply == QMessageBox::Yes)
		{
			for (int i = 0; i < NR_STATIONS; i++)
			{

				types[i][currentType]->ResetCounters();
				types[i][currentType]->WriteCounters();

				OnResetStatistics();
				OnResetTimeCounters();
			}
		}

	//}
	/*else
	{
		
		QString niz;

		if (slovensko) niz = QString("Za ponastavitev stevcev morate biti prijavljeni! Se zelite prijaviti?");
		else niz = QString("You need to be logged to reset counters! Do you want to log in?");

		//reply = QMessageBox::question(this, tr("QMessageBox::question()"),
		//	niz,
		//	QMessageBox::Yes | QMessageBox::No);

		//if (reply == QMessageBox::Yes)
			login->ShowDialog();
	}*/

}
void MBsoftware::OnClickedLogin(void)
{
	
	login->ShowDialog();
}
void MBsoftware::OnClickedStatusBox(void)
{
	statusBox[0]->ShowDialog();

}



void MBsoftware::OnClickedSmcButton(void)
{
/*	if(smc.size() > 0)
	smc[0]->ShowDialog();*/
}
void MBsoftware::OnClickedShowHistory(void)
{
	QStringList list;
	int stNum;
	if (NR_STATIONS > 1)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			if (slovensko) list.push_back(QString("Postaja: %1").arg(i));
			else list.push_back(QString("Station: %1").arg(i));
		}
		bool ok;
		QString text;
		if (slovensko) text = QInputDialog::getItem(this, tr("Izberi postajo:"), tr(""), list, 0, false, &ok);
		else text = QInputDialog::getItem(this, tr("Select station:"), tr(""), list, 0, false, &ok);

		for (int i = 0; i < list.size(); i++)
		{
			if (text == list[i])
				stNum = i;
		}
	}
	else
		stNum = 0;

	history->OnShowDialog(stNum);
}
void MBsoftware::OnClickedGeneralSettings(void)
{
	QString niz;
	QMessageBox::StandardButton reply;
	//if ((login->currentRights == 1) || (login->currentRights == 2))
		settings->OnShowDialog();
	/*else
	{
		if (slovensko) niz = QString("Za spreminjanje nastavitev tipov morate biti prijavljeni kot administrator ali operater. Se zelite prijaviti?");
		else niz = QString("You need to be logged in as administrator or operator to change type settings! Do you want to log in?");
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			niz,QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();*/
	
}

void MBsoftware::OnClickedAboutUs()
{
	about->OnShowDialog();
}
void MBsoftware::onClientReadyRead()
{
	QString msg = client->readAll();

}
void MBsoftware::OnClickedTCPconnction(int index)
{
	if (tcp.size() >= index)
		tcp[index]->OnShowDialog();
}
void MBsoftware::OnClickedDMS(void)
{
	dmsUDP->OnShowDialog();
}
void MBsoftware::OnClickedDMStransitionTest(void)
{
	dmsUDP->OnShowDialogTransitionTest();
}


void MBsoftware::SetTolerance(int currentType)//vpisemo vse tolerance v measureObject in v tolerancno tableo 
{

	measureObject.setLenght = types[0][currentType]->typeDowelSetting[0];
	measureObject.lenghtTolerancePlus = types[0][currentType]->typeDowelSetting[1];
	measureObject.lenghtToleranceMinus = types[0][currentType]->typeDowelSetting[2];
	measureObject.setDiameter = types[0][currentType]->typeDowelSetting[3];
	measureObject.diameterTolerancePlus = types[0][currentType]->typeDowelSetting[4];
	measureObject.diameterToleranceMinus = types[0][currentType]->typeDowelSetting[5];
	measureObject.setKonusLenght = types[0][currentType]->typeDowelSetting[6];
	measureObject.konusTolerancePlus = types[0][currentType]->typeDowelSetting[7];
	measureObject.konusToleranceMinus = types[0][currentType]->typeDowelSetting[8];

	AdvancedSettings tmpSettings;
	int isSel = -1;
	for (int i = 0; i < dowelSettingsDlg->advSettings.size(); i++)
	{
		if (types[0][currentType]->selectedAdvancedSettings.compare(dowelSettingsDlg->advSettings[i].name) == 0)
		{
			tmpSettings = dowelSettingsDlg->advSettings[i];
			isSel = i;
		}
	}
	if (isSel == -1)
		tmpSettings = dowelSettingsDlg->advSettings[0];
	//vse nominalne vrednosti dam na 0

	for (int i = 0; i < types[0][currentType]->parameterCounter; i++)
	{		
		types[0][currentType]->nominal[i] = 0;
	}


	//diameter middle
	for (int i = 1; i < 4; i++)
	{
		types[0][currentType]->toleranceHigh[i] = measureObject.diameterTolerancePlus;
		types[0][currentType]->toleranceLow[i] = measureObject.diameterToleranceMinus;
	}

	//angle meausrements
	int count = 0;
	for (int i = 5; i < 9; i++)
	{
		types[0][currentType]->toleranceHigh[i] = tmpSettings.parameterValue[0][1];
		types[0][currentType]->toleranceLow[i] = tmpSettings.parameterValue[0][0];
	}
	measureObject.setAngleMax  = tmpSettings.parameterValue[0][1];
	measureObject.setAngleMin  = tmpSettings.parameterValue[0][0];

	//del visina konusa
	for (int i = 9; i < 13; i++)
	{
		types[0][currentType]->toleranceHigh[i] = tmpSettings.parameterValue[0][3];
		types[0][currentType]->toleranceLow[i] = tmpSettings.parameterValue[0][2];
	}

	for (int i = 13; i < 17; i++)
	{
		types[0][currentType]->toleranceHigh[i] = measureObject.konusTolerancePlus;
		types[0][currentType]->toleranceLow[i] = measureObject.konusToleranceMinus;
	}

	types[0][currentType]->toleranceHigh[17] = tmpSettings.parameterValue[0][4]; //roughness
	measureObject.setElipsse = tmpSettings.parameterValue[0][5];
	measureObject.setDSKDEKoffset = tmpSettings.parameterValue[0][6];
	measureObject.nrEKonusAll = tmpSettings.parameterValue[1][0]; //zacetek konusa
	measureObject.nrEEdgeAll = tmpSettings.parameterValue[1][1]; //vrh konusa
	measureObject.nrEMiddleAll =tmpSettings.parameterValue[1][2]; //v sredini moznika
	measureObject.nrELenghtDowel = tmpSettings.parameterValue[1][3]; //dolzina moznika
	measureObject.nrEKonusMinus = tmpSettings.parameterValue[1][4];
	measureObject.nrEdgeMinus = tmpSettings.parameterValue[1][5];
	measureObject.nrEMiddleMinus = tmpSettings.parameterValue[1][6];
	measureObject.nrELenghtDowelMinus = tmpSettings.parameterValue[1][7];
	measureObject.deltaErrorX = tmpSettings.parameterValue[1][8];

	
	//types[0][currentType]->toleranceHigh[0] = measureObject.setDiameter  - tmpSettings.parameterValue[0][2];
	//types[0][currentType]->toleranceLow[0] = measureObject.setDiameter - measureObject.setDSKDEKoffset;

	types[0][currentType]->toleranceHigh[0] = measureObject.setDiameter - measureObject.setDSKDEKoffset;
	types[0][currentType]->toleranceLow[0] = 0;
	types[0][currentType]->toleranceHigh[4] = measureObject.setDiameter - measureObject.setDSKDEKoffset;
	types[0][currentType]->toleranceLow[4] = 0;

	//types[0][currentType]->toleranceHigh[4] = measureObject.setDiameter - tmpSettings.parameterValue[0][2];
	//types[0][currentType]->toleranceLow[4] = measureObject.setDiameter - measureObject.setDSKDEKoffset;


	//za tolerance v zgodovini
	for (int i = 0; i < 18; i++)
	{
		//measureObject.toleranceHistoryHIGH[i] = types[0][currentType]->toleranceHigh[i];
		//measureObject.toleranceHistoryLOW[i] = types[0][currentType]->toleranceLow[i];
		measureObject.parameterNameHistory[i] = types[0][currentType]->name[i];
	}

	//DrawMeasurements();
	UpdateDowelImage(showPiece);

}

void MBsoftware::SetToleranceAutoCalibrationMode()
{
	float refDiameter = dmsCalibrationDlg->refDowelDiameter;
	float refLenght = dmsCalibrationDlg->refDowelLenght;

	measureObject.setLenght = refLenght;
	measureObject.lenghtTolerancePlus = refLenght +1;
	measureObject.lenghtToleranceMinus = refLenght -1;
	measureObject.setDiameter = refDiameter;
	measureObject.diameterTolerancePlus = refDiameter +1;
	measureObject.diameterToleranceMinus = refDiameter -1;
	measureObject.setKonusLenght = 2;
	measureObject.konusTolerancePlus = 4;
	measureObject.konusToleranceMinus = 0.1;
	
	AdvancedSettings tmpSettings;
	int isSel = -1;
	for (int i = 0; i < dowelSettingsDlg->advSettings.size(); i++)
	{
		if (types[0][currentType]->selectedAdvancedSettings.compare(dowelSettingsDlg->advSettings[i].name) == 0)
		{
			tmpSettings = dowelSettingsDlg->advSettings[i];
			isSel = i;
		}
	}
	if (isSel == -1)
		tmpSettings = dowelSettingsDlg->advSettings[0];
	//vse nominalne vrednosti dam na 0

	for (int i = 0; i < types[0][currentType]->parameterCounter; i++)
	{
		types[0][currentType]->nominal[i] = 0;
	}


	//diameter middle
	for (int i = 1; i < 4; i++)
	{
		types[0][currentType]->toleranceHigh[i] = measureObject.diameterTolerancePlus;
		types[0][currentType]->toleranceLow[i] = measureObject.diameterToleranceMinus;
	}

	//angle meausrements
	int count = 0;
	for (int i = 5; i < 9; i++)
	{
		types[0][currentType]->toleranceHigh[i] = 60;
		types[0][currentType]->toleranceLow[i] = 1;
	}
	measureObject.setAngleMax = 60;
	measureObject.setAngleMin = 1;

	//del visina konusa
	for (int i = 9; i < 13; i++)
	{
		types[0][currentType]->toleranceHigh[i] = 5;
		types[0][currentType]->toleranceLow[i] = 0.01;
	}

	for (int i = 13; i < 17; i++)
	{
		types[0][currentType]->toleranceHigh[i] = measureObject.konusTolerancePlus;
		types[0][currentType]->toleranceLow[i] = measureObject.konusToleranceMinus;
	}

	types[0][currentType]->toleranceHigh[17] = 2; //roughness
	measureObject.setElipsse = 2;
	measureObject.setDSKDEKoffset = 0.01;
	measureObject.nrEKonusAll = 5; //zacetek konusa
	measureObject.nrEEdgeAll = 5; //vrh konusa
	measureObject.nrEMiddleAll = 5; //v sredini moznika
	measureObject.nrELenghtDowel = 5; //dolzina moznika
	measureObject.nrEKonusMinus = 5;
	measureObject.nrEdgeMinus = 5;
	measureObject.nrEMiddleMinus = 5;
	measureObject.nrELenghtDowelMinus = 5;
	measureObject.deltaErrorX = 0.3;



	types[0][currentType]->toleranceLow[0] = 0;
	types[0][currentType]->toleranceHigh[0] = refDiameter +1;


	
	types[0][currentType]->toleranceLow[4] = 0;
	types[0][currentType]->toleranceHigh[4] = refDiameter +1;


	//za tolerance v zgodovini
	for (int i = 0; i < 18; i++)
	{
		//measureObject.toleranceHistoryHIGH[i] = types[0][currentType]->toleranceHigh[i];
		//measureObject.toleranceHistoryLOW[i] = types[0][currentType]->toleranceLow[i];
		measureObject.parameterNameHistory[i] = types[0][currentType]->name[i];
	}

	//DrawMeasurements();
	UpdateDowelImage(showPiece);
	
}



void MBsoftware::OnSingnalSelectedType(int selectedType)
{
	int diff = 0;
	if (currentType != selectedType)
		diff = 1;
	currentType = selectedType;
	
	for (int i = 0; i < 9; i++)
	{
		types[0][currentType]->typeDowelSetting[i] = dowelSettingsDlg->basicSettings[selectedType][i];
	}
	

	types[0][currentType]->selectedAdvancedSettings = dowelSettingsDlg->selectedAdvSettings;
	types[0][currentType]->selectedColorCameraSettings = dowelSettingsDlg->selectedColorCameraSettings;

	types[0][currentType]->dowelWithConus = dowelSettingsDlg->dowelWithConus;
	measureObject.selectedCalibration = round(types[0][currentType]->typeDowelSetting[3]);
	SetTolerance(currentType);
	SaveVariables();
	if (diff == 1)
	{
		OnResetStatistics();
		OnResetTimeCounters();
	}
	types[0][currentType]->WriteParameters();
	updateStatusRect = 1;	
}

void MBsoftware::OnSignalAdvSettingsSaved(QString selected)
{
	int bla = 10;
	//advSettings.size = settings;
	/*for (int i = 0; i < advSettings.size(); i++)
	{
		if (settingName.compare(advSettings[i]->name) == 0)
		{
			advSettings[i] = dowelSettingsDlg->tmpAdvSettings;
			for(int k  = 0; k < advSettings[i]->groupName.size();k++)
			
			advSettings[i]->WriteParameters(advSettings[i]->groupName[k]);
		}
	}*/

}

void MBsoftware::OnSignalEnterCalibrationMode()
{
	int bla = 100;
	autoCalibrationMode = 1;
	SetToleranceAutoCalibrationMode();
}

void MBsoftware::OnSignalLeaveCalibrationMode()
{
	autoCalibrationMode = 0;
	SetTolerance(currentType);
}

void MBsoftware::OnClickedEMGReset()
{

		processStatus = 0;
		
	
	EMGwindow->close();
}

void MBsoftware::SaveVariables()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = referencePath + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentVariables.txt";
	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream stream(&file);

		
		stream << types[0][currentType]->typeName;
		stream << endl;




		file.close();
	}
}
void MBsoftware::LoadVariables()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;
	
	QString tmpType = 0;
	dirPath = referencePath + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentVariables.txt";
		QFile file(fileName);
		if (file.open(QIODevice::ReadOnly))
		{
			QTextStream stream(&file);

			line = stream.readLine();
			tmpType = line;


		}
		else
		{

			currentType = 0;

		}

		for (int i = 0; i < types[0].size(); i++)
		{
			if (tmpType.compare(types[0][i]->typeName) == 0)
			{
				currentType = i;
			}
		}
		

		if (currentType >= types[0].size())
			currentType = 0;
		//prevType = currentType;
}
void MBsoftware::SaveVariablesStatistics()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = referencePath + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentStatistics.txt";
	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream stream(&file);

		for (int i = 0; i < 10; i++)
		{
			
			//badOnDifferentCounter[i] = line.toInt();
			stream << QString("%1").arg(badOnDifferentCounter[i]);
			stream << endl;

		}
		stream << QString("%1").arg(presortCounterBadStat);
		stream << endl;

		stream << QString("%1").arg(badpresortColor);
		stream << endl;

		stream << QString("%1").arg(badpresrotSmall);
		stream << endl;

		stream << QString("%1").arg(badpresortSensor);
		stream << endl;

		stream << QString("%1").arg(presortPieceCount);
		stream << endl;

		

	
		file.close();
	}
}
void MBsoftware::LoadVariablesStatistics()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;

	QString tmpType = 0;
	dirPath = referencePath + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentStatistics.txt";
	QFile file(fileName);
	if (file.open(QIODevice::ReadOnly))
	{
		QTextStream stream(&file);

		for (int i = 0; i < 10; i++)
		{
			line = stream.readLine();
			badOnDifferentCounter[i] = line.toInt();
			
		}
		line = stream.readLine();
		presortCounterBadStat = line.toInt();

		line = stream.readLine();
		badpresortColor = line.toInt();
		line = stream.readLine();
		badpresrotSmall = line.toInt();
		line = stream.readLine();
		badpresortSensor = line.toInt();
		line = stream.readLine();
		presortPieceCount = line.toInt();

	

	}
	else
	{

		for (int i = 0; i < 10; i++)
		{
			
			badOnDifferentCounter[i] = 0;
		}
		presortCounterBadStat = 0;
		 badpresortColor = 0;
		 badpresrotSmall = 0;
		 badpresortSensor = 0;
		 presortPieceCount = 0;
		
	}


	//prevType = currentType;
}
void MBsoftware::CalculateValveTime()
{
	float time;
	float timeReal;
	//dodamo se podatke v xilinx za pih ventila 
	measureObject.volumeOfDowel = (((measureObject.setDiameter/2) *  (measureObject.setDiameter/2)) * M_PI * measureObject.setLenght) / 4;
	time = measureObject.volumeOfDowel *0.010;
	timeReal = time +settings->blowFactor;

	measureObject.timeToBlow = round(timeReal);

	measureObject.delayToValve = settings->delayToValve - measureObject.setLenght/2 - time/2;

	if ((measureObject.timeToBlow != valveDurationPrev) || (measureObject.delayToValve != timeToValvePrev))
	{
		dmsUDP->SetBlowValve(measureObject.timeToBlow, measureObject.delayToValve);
	}

	valveDurationPrev = measureObject.timeToBlow;
	timeToValvePrev = measureObject.delayToValve;
	
}
void MBsoftware::mouseDoubleClickEvent(QMouseEvent * e)
{
	QRect containtRect;
	int windowPos = -1;
	int nrStation = -1;
	int chartSelected = -1;
	int station = 0;
	int param = 0;
	bool ok = false;
	int curr = 0;

	if (e->button() == Qt::LeftButton)
	{

		containtRect = ui.imageViewMainWindow_0->geometry();
		if (containtRect.contains(e->pos()) == true)
		{

				windowPos = 0;
				nrStation = 0;

		}
		if (containtRect.contains(e->pos()) == true)
		{

				curr = QInputDialog::getInt(this, tr("Izberi Postajo"), tr("Postaja:"), 0, 0, NR_STATIONS-1, 1, &ok, NULL);
				if (ok)
				{
					station = curr;
					curr = QInputDialog::getInt(this, tr("Izberi mero"), tr("Mera:"), 0, 1, types[station][currentType]->parameterCounter, 1, &ok, NULL);
					if (ok)
					{


					}
				}

		}
		
	}


}

void MBsoftware::OnUpdateStatistics()
{
	QString tmpText;
	float value;
	if (measureObject.statisticsCounter > 0)
		value = measureObject.lenghtSumStatistics / measureObject.statisticsCounter;
	else
		value = 0;

	tmpText = QString("%1").number(value, 'f', 2);
	ui.labelLenghtStatistics->setText(QString("%1 mm").arg(tmpText));

	if (measureObject.statisticsCounter > 0)
		value = measureObject.widthSumStatistics / measureObject.statisticsCounter;
	else
		value = 0;

	tmpText = QString("%1").number(value, 'f', 2);
	ui.labelWidthStatistics->setText(QString("%1 mm").arg(tmpText));


	if (measureObject.statisticsCounterGood > 0)
		value = measureObject.lenghtSumStatisticsGood / measureObject.statisticsCounterGood;
	else
		value = 0;

	tmpText = QString("%1").number(value, 'f', 2);
	ui.labelLenghtStatisticsGood->setText(QString("%1 mm").arg(tmpText));

	if (measureObject.statisticsCounterGood > 0)
		value = measureObject.widthSumStatisticsGood / measureObject.statisticsCounterGood;
	else
		value = 0;

	tmpText = QString("%1").number(value, 'f', 2);
	ui.labelWidthStatisticsGood ->setText(QString("%1 mm").arg(tmpText));



	if (measureObject.statisticsCounter > 0)
		value = measureObject.elipseSumStatistics / measureObject.statisticsCounter;
	else
		value = 0;

	

	tmpText = QString("%1").number(value, 'f', 2);
	ui.labelElipseStatistics->setText(QString("%1").arg(tmpText));



	ui.labelAlarm1->setText(QString("%1").arg(measureObject.alarmCounter[0]));
	ui.labelAlarm2->setText(QString("%1").arg(measureObject.alarmCounter[1]));
	ui.labelAlarm3->setText(QString("%1").arg(measureObject.alarmCounter[2]));
	ui.labelAlarm4->setText(QString("%1").arg(measureObject.alarmCounter[3]));


	//statistics bad
	//int badCounter = measureObject.statisticsCounter - measureObject.statisticsCounterGood;
	int badCounter = types[0][currentType]->badMeasurementsCounter;
	float val[10];
	QString valText[10];

	for (int i = 0; i < 10; i++)
	{
		if (badCounter > 0)
			val[i] = (float)(badOnDifferentCounter[i] / (float)badCounter) * 100;
		else
			val[i] = 0;

		valText[i] = QString("%1").number(val[i], 'f', 2);
	}

	ui.labelBadStat->setText(QString("%1 (%2 %)").arg(badOnDifferentCounter[0]).arg(valText[0]));
	ui.labelBadStat_2->setText(QString("%1 (%2 %)").arg(badOnDifferentCounter[1]).arg(valText[1]));
	ui.labelBadStat_3->setText(QString("%1 (%2 %)").arg(badOnDifferentCounter[2]).arg(valText[2]));
	ui.labelBadStat_4->setText(QString("%1 (%2 %)").arg(badOnDifferentCounter[3]).arg(valText[3]));
	ui.labelBadStat_5->setText(QString("%1 (%2 %)").arg(badOnDifferentCounter[6]).arg(valText[6]));
	ui.labelBadStat_6->setText(QString("%1 (%2 %)").arg(badOnDifferentCounter[4]).arg(valText[4]));
	ui.labelBadStat_7->setText(QString("%1 (%2 %)").arg(badOnDifferentCounter[5]).arg(valText[5]));


	if (presortCounterBadStat > 0)
		value = (float)(badpresortColor / (float)presortCounterBadStat) * 100;
	else
		value = 0;

	tmpText = QString("%1").number(value, 'f', 2);
	ui.labelBadStat_8->setText(QString("%1 (%2 %)").arg(badpresortColor).arg(tmpText));

	//colorStatistics

	if (presortCounterBadStat > 0)
		value = (float)(badpresortColor / (float)presortCounterBadStat) * 100;
	else
		value = 0;

	tmpText = QString("%1").number(value, 'f', 2);
	ui.labelBadpresortColor->setText(QString("%1 (%2 %)").arg(badpresortColor).arg(tmpText));

	if (presortCounterBadStat > 0)
		value = (float)(badpresrotSmall / (float)presortCounterBadStat) * 100;
	else
		value = 0;

	tmpText = QString("%1").number(value, 'f', 2);
	ui.labelBadpresortSmall->setText(QString("%1 (%2 %)").arg(badpresrotSmall).arg(tmpText));

	if (presortPieceCount > 0)
		value = (float)(badpresortSensor / (float)presortPieceCount) * 100;
	else
		value = 0;

	tmpText = QString("%1").number(value, 'f', 2);
	if(settings->presortSensorEnable == 1)
	ui.labelBadpresortSensor->setText(QString("%1 (%2 %)").arg(badpresortSensor).arg(tmpText));
	else
		ui.labelBadpresortSensor->setText(QString("Off"));


	//ui.labelBadStat_8->setText(QString("%1 (%2 %)").arg(badOnDifferentCounter[0]).arg(valText[0]));


}

void MBsoftware::OnResetStatistics()
{

	imageProcess->presortingCameraCounter = 0;
	imageProcess->presortingCameraCounterBad = 0;
	presureSensorCounter = 0;
	measureObject.currentPiece = 0;
	for (int i = 0; i < 10; i++)
	{
		measureObject.alarmCounter[i] = 0;
	}
	measureObject.statisticsCounter = 0;
	measureObject.statisticsCounterGood = 0;
	measureObject.widthSumStatistics = 0;
	measureObject.widthSumStatisticsGood = 0;
	measureObject.lenghtSumStatisticsGood = 0;
	measureObject.lenghtSumStatistics = 0;
	measureObject.elipseSumStatistics = 0;
	for (int i = 0; i < 10000; i++)
	{
		measureObject.lenghtHistory[i] = 0;
		measureObject.widthHistory[i] = 0;
		measureObject.historyGaussCounter = 0;
	}



	for (int m = 0; m < DMSHISTORYSIZE; m++)
	{
		measureObject.ResetBlowImages(m);
		measureObject.ResetMeasurements(m);

		for (int n = 0; n < 18; n++)
		{
			for (int z = 0; z < 6; z++)
			{
				measureObject.measureValueHistory[m][n][z] = 0;
				measureObject.isGoodHistory[m][n][z] = 0;
			}
		}
	}

	for (int m = 0; m < 18; m++)
	{
		for (int n = 0; n < 5; n++)
			types[0][currentType]->SetMeasuredValueDMS(0, n, m);
	}

OnUpdateBarChartData(0, measureObject.currentPiece);
OnUpdateBarChartData(1, measureObject.currentPiece);
OnUpdateBarChartData(2, measureObject.currentPiece);
OnUpdateBarChartData(3, measureObject.currentPiece);
OnUpdateGaussChartData(0, measureObject.currentPiece);
OnUpdateGaussChartData(1, measureObject.currentPiece);

presortCounterBadStat = 0;

badpresortColor = 0;
badpresortSensor = 0;
badpresrotSmall = 0;
presortPieceCount = 0;

UpdateDowelImage(measureObject.currentPiece); 
OnUpdateStatistics();

}

void MBsoftware::OnResetTimeCounters()
{
	goodCounterLastValue = 0;
	badCounterLastValue = 0;
	allCounterLastValue = 0;
	presortCounterBadStat = 0;


	for (int i = 0; i < 4; i++)
	{
		goodCounterValue[i] = 0;
		badCounterValue[i] = 0;
		allCounterValue[i] = 0;
	}
	for (int i = 0; i < allPerHourV.size(); i++)
		allPerHourV[i] = 0;

	goodCounterLastValue = types[0][currentType]->goodMeasurementsCounter;
	badCounterLastValue = types[0][currentType]->badMeasurementsCounter;
	allCounterLastValue = types[0][currentType]->totalMeasurementsCounter;
	goodPerMin = 0;
	badPerMin = 0;
	allPerMin = 0;


	allPerHour = 0;
	for (int i = 0; i < 10; i++)
	{
		badOnDifferentCounter[i] = 0;
	}
}

void MBsoftware::OnResetBoxes()
{
}

void MBsoftware::OnUpdateServiceTab()
{
//do sedaj je narejno se z LPT-ji
	//ui.checkInput0->setChecke



	ui.checkInput0->setChecked((bool)lpt[0]->status[3].value);
	ui.checkInput1->setChecked((bool)lpt[0]->status[2].value);
	ui.checkInput2->setChecked((bool)lpt[0]->status[1].value);
	ui.checkInput3->setChecked((bool)lpt[1]->status[0].value);
	ui.checkInput4->setChecked((bool)lpt[1]->status[4].value);
	ui.checkInput5->setChecked((bool)lpt[1]->status[3].value);
	ui.checkInput6->setChecked((bool)lpt[1]->status[2].value);
	ui.checkInput7->setChecked((bool)lpt[0]->status[4].value);

	ui.labelCheckInput0->setText(QString("%1").arg(inputCounter[0]));
	ui.labelCheckInput1->setText(QString("%1").arg(inputCounter[1]));
	ui.labelCheckInput2->setText(QString("%1").arg(inputCounter[2]));
	ui.labelCheckInput3->setText(QString("%1").arg(inputCounter[3]));
	ui.labelCheckInput4->setText(QString("%1").arg(inputCounter[4]));
	ui.labelCheckInput5->setText(QString("%1").arg(inputCounter[5]));
	ui.labelCheckInput6->setText(QString("%1").arg(inputCounter[6]));
	ui.labelCheckInput7->setText(QString("%1").arg(inputCounter[7]));


	ui.checkOutput0->setChecked((bool)lpt[0]->data[0].value);
	ui.checkOutput1->setChecked((bool)lpt[0]->data[1].value);
	ui.checkOutput2->setChecked((bool)lpt[0]->data[2].value);
	ui.checkOutput3->setChecked((bool)lpt[0]->data[3].value);
	ui.checkOutput4->setChecked((bool)lpt[0]->data[4].value);
	ui.checkOutput5->setChecked((bool)lpt[0]->data[5].value);
	ui.checkOutput6->setChecked((bool)lpt[0]->data[6].value);
	ui.checkOutput7->setChecked((bool)lpt[0]->data[7].value);
	ui.checkOutput8->setChecked((bool)lpt[1]->data[0].value);
	ui.checkOutput9->setChecked((bool)lpt[1]->data[1].value);
	ui.checkOutput10->setChecked((bool)lpt[1]->data[2].value);
	ui.checkOutput11->setChecked((bool)lpt[1]->data[3].value);
	ui.checkOutput12->setChecked((bool)lpt[1]->data[4].value);
	ui.checkOutput13->setChecked((bool)lpt[1]->data[5].value);
	ui.checkOutput14->setChecked((bool)lpt[1]->data[6].value);
	ui.checkOutput15->setChecked((bool)lpt[1]->data[7].value);


	///za prikaz prehodov
	
	//ui.labelTrans
	//dmsUDP->measureObject->calibrationTransitions[0][0];
		
	//ui.labelTrans
	ui.labelTrans0_0->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[0][0]));
	ui.labelTrans0_1->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[0][1]));

	ui.labelTrans1_0->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[1][0]));
	ui.labelTrans1_1->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[1][1]));

	ui.labelTrans2_0->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[2][0]));
	ui.labelTrans2_1->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[2][1]));

	ui.labelTrans3_0->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[3][0]));
	ui.labelTrans3_1->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[3][1]));

	ui.labelTrans4_0->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[4][0]));
	ui.labelTrans4_1->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[4][1]));

	ui.labelTrans5_0->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[5][0]));
	ui.labelTrans5_1->setText(QString("%1").arg(dmsUDP->measureObject->calibrationTransitions[5][1]));
	
}

void MBsoftware::OnCalculateCounters()
{
	goodCounterValue[counterValue] = (types[0][currentType]->goodMeasurementsCounter - goodCounterLastValue);
	badCounterValue[counterValue] = (types[0][currentType]->badMeasurementsCounter - badCounterLastValue);
	allCounterValue[counterValue] = (types[0][currentType]->totalMeasurementsCounter - allCounterLastValue);

	goodCounterLastValue = types[0][currentType]->goodMeasurementsCounter;
	badCounterLastValue = types[0][currentType]->badMeasurementsCounter;
	allCounterLastValue = types[0][currentType]->totalMeasurementsCounter;

	int c0 = 0;
	int c1 = 0;
	int c2 = 0;
	int c3 = 0;
	counterValue++;
	if (counterValue == 4)//v eni minuti si zapolnem rezultate za avr per hour 
	{
		for (int i = 0; i < 4; i++)
		{
			c0 += goodCounterValue[i];
			c1 += badCounterValue[i];
			c2 += allCounterValue[i];

		}

		allPerHourV.push_back(c2);

		if (allPerHourV.size() > 10)
		{
			allPerHourV.erase(allPerHourV.begin(), allPerHourV.begin() + 1);
		}
		for (int i = 0; i < allPerHourV.size(); i++)
		{
			c3 += allPerHourV[i];
		}
		c3 /= allPerHourV.size();

		allPerHour = c3 * 60;

		if (c0 > 0)
			goodPerMin = c0;
		else
			goodPerMin = 0;
		if (c1 > 0)
			badPerMin = c1;
		else
			badPerMin = 0;
		if (c2 > 0)
			allPerMin = c2;
		else
			allPerMin = 0;


	}

	if (counterValue >= 4)
		counterValue = 0;

}

void MBsoftware::paintEvent(QPaintEvent * event)
{
}

void MBsoftware::keyPressEvent(QKeyEvent *event)
{
	qDebug() << event->key();
	QString fileP;
	CPointFloat points[3];
	int i = 0;
	QString name;
	int tmp;
	QString TT = "00ff";
	switch (event->key())
	{
	case  Qt::Key_0:
	{
		settings->rotatorSpeedMode = 0;
		break;
	}
	case  Qt::Key_1:
	{
		settings->rotatorSpeedMode = 1;
		break;
	}
	case  Qt::Key_2:
	{
		settings->rotatorSpeedMode = 2;
		break;
	}
	case  Qt::Key_3:
	{
		settings->rotatorSpeedMode = 3;
		break;
	}
	case  Qt::Key_4:
	{
		settings->rotatorSpeedMode = 4;
		break;
	}
	case  Qt::Key_5:
	{
		settings->rotatorSpeedMode = 5;
		
		break;
	}
	case  Qt::Key_6:
	{
		settings->rotatorSpeedMode = 6;
		
		break;
	}
	case  Qt::Key_7:
	{
		settings->rotatorSpeedMode = 7;
	
		break;
	}
	case  Qt::Key_8:

		break;

	case  Qt::Key_9:
		dmsUDP->ShowDowel(0);
		break;
	case  Qt::Key_F4:

		dmsCalibrationDlg->UpdateAutoCalibrationTable(0);
		break;

	case  Qt::Key_F6:



		break;

	case  Qt::Key_A:
	{
		QByteArray tmp;
		//QString filename = "C:/data/testData" + QString("/DOWEL_%1.txt").arg(uploadDowel);
		QString filename = referencePath + QString("/reference/data/testData/DOWEL_%1.txt").arg(uploadDowel);
		QFile file(filename);
		file.open(QIODevice::ReadOnly);
		tmp = file.readAll();
		file.close();
		QString line;
		dmsUDP->tmpDowelData = tmp;
		dmsUDP->ReadDowelString(tmp);


		break;
	}

	case  Qt::Key_S:
	{
		uploadDowel++;
		if (uploadDowel > 127)
			uploadDowel = 127;

		repaint();
		break;
	}
	case Qt::Key_D:
	{
		uploadDowel--;
		if (uploadDowel < 0)
			uploadDowel = 0;


		repaint();
		break;
	}
	case  Qt::Key_R:



		break;

	case  Qt::Key_T:



		break;
	case  Qt::Key_E:


		break;
	case  Qt::Key_G:


		break;
	case  Qt::Key_I:
		QtConcurrent::run(imageProcess, &imageProcessing::ProcessingCamera1, &cam[1]->image[0], 0);

	

		break;
	case  Qt::Key_J:

		break;

	case  Qt::Key_B:
		if(processStatus <= 0)
		 testValveB = testValveDuration;
		break;


	case  Qt::Key_C:
		if (processStatus <= 0)
		testValveC = testValveDuration;
		break;


	case  Qt::Key_Y:
		if (processStatus <= 0)
		testValveY = testValveDuration;
		break;


	case  Qt::Key_V:
		if (processStatus <= 0)
		testValveV = testValveDuration;
		break;
	case  Qt::Key_X:
		if (processStatus <= 0)
		testValveX = testValveDuration;
		break;
	case  Qt::Key_K:

		break;
	case  Qt::Key_Up:
	{
		showPiece++;
		if (showPiece > 127)
			showPiece = 0;
		for (int i = 0; i < 20; i++)
		{
			//images[i]->buffer[0] = measureObject.blowImage[showPiece][measureObject.blowImageCounter[i]].clone();
			measureObject.blowImage[showPiece][i].copyTo(images[i]->buffer[0]);
			testImageIndex[i] = measureObject.imageIndex[showPiece][i];

		}
		measureObject.showInHistory[showPiece] = 1;
		MeasurePiece(showPiece);
		imageProcess->displayImage = 0;
		imageProcess->ReplaceCurrentBuffer();
		dmsUDP->ShowDowel(showPiece);
		repaint();
		break;
	}
	case  Qt::Key_Down:
	{
		showPiece--;
		if (showPiece < 0)
			showPiece = 127;

		for (int i = 0; i < 20; i++)
		{
			//images[i]->buffer[0] = measureObject.blowImage[showPiece][measureObject.blowImageCounter[i]].clone();
			measureObject.blowImage[showPiece][i].copyTo(images[i]->buffer[0]);
			testImageIndex[i] = measureObject.imageIndex[showPiece][i];
			//images[i]->buffer[0] = measureObject.blowImage[showPiece][i].clone();

		}
		measureObject.showInHistory[showPiece] = 1;
		MeasurePiece(showPiece);
		imageProcess->displayImage = 0;
		imageProcess->ReplaceCurrentBuffer();
		dmsUDP->ShowDowel(showPiece);
		repaint();
		break;
	}
	case  Qt::Key_Left:

		selectedMeausurement++;
		if (selectedMeausurement >= measureObject.nrDowelMeasurements[showPiece])
			selectedMeausurement = measureObject.nrDowelMeasurements[showPiece];



		repaint();
		break;
	case  Qt::Key_Right:
		selectedMeausurement--;
		if (selectedMeausurement < 0)
			selectedMeausurement = 0;



		repaint();
		break;


	case Qt::Key_Space:
		if (startKey == 0)
			startKey = 1;
		else
			startKey = 0;
		break;

		break;
	case  Qt::Key_Escape:
		this->repaint();
		showPiece = measureObject.currentPiece;
		for (int i = 0; i < cam.size(); i++)
		{
			if (cam[i]->isVisible())
				cam[i]->close();
		}


		for (int i = 0; i < statusBox.size(); i++)
		{
			if (statusBox[i]->isVisible())
				statusBox[i]->close();
		}
		for (int i = 0; i < lpt.size(); i++)
		{
			if (lpt[i]->isVisible())
				lpt[i]->close();
		}
		imageProcess->close();
		break;

	default:

		break;
	}
}
QCustomPlot * MBsoftware::SetParentQCP(QCustomPlot * parentWidget, QString title, QString xAxisLabel, QString yAxisLabel, MBRange xRange, MBRange yRange, bool setLegend)
{
	QCustomPlot * parentQCP = parentWidget;
	const QFont timesNewRomanFont("Times New Roman", 12/*, QFont::Bold*/);
	if (title != "")
	{



	parentQCP->plotLayout()->insertRow(0);
	parentQCP->plotLayout()->addElement(0, 0, new QCPTextElement(parentQCP, title, timesNewRomanFont));

	//int bla = parentQCP->plotLayout()->autoMargins();
	//parentQCP->plotLayout()->setAutoMargins(QCP::setMarginValue(QCP::QMargins,QCP::MarginSide(0),0))
	//parentQCP->plotLayout()->setAutoMargins(QCP::msNone);
	// bla = parentQCP->plotLayout()->autoMargins();
	//parentQCP->plotLayout()->elementAt(0)->setAutoMargins(QCP::msNone);
	//parentQCP->plotLayout()->element(0, 0)->setOuterRect(QRect(10, 10, 20, 20));
	//parentQCP->plotLayout()->setOuterRect(QRect(10, 10, 50, 50));
		//parentQCP->plotLayout()->setMaximumSize(QSize(10, 10));
		//parentQCP->plotLayout()->elementAt(0)->setMaximumSize(QSize(10, 10));
		
	}
	//dolžine osi
	parentQCP->xAxis->setRange(xRange.Start(), xRange.End());
	parentQCP->xAxis->setRange(yRange.Start(), yRange.End());
	//imena osi
	parentQCP->xAxis->setLabel(xAxisLabel);
	parentQCP->yAxis->setLabel(yAxisLabel);
	parentQCP->yAxis->setLabelFont(timesNewRomanFont);
	parentQCP->yAxis2->setLabelFont(timesNewRomanFont);
	//legenda
	parentQCP->legend->setVisible(setLegend);

	parentQCP->setFont(timesNewRomanFont);


	return parentQCP;
}

QCPBars * MBsoftware::CreateBarPlot(QCustomPlot * parentQCP, QString title, QPen pen, QBrush brush)
{
	QCPBars * barPlot = new QCPBars(parentQCP->xAxis, parentQCP->yAxis);

	//naslov
	barPlot->setName(title);
	//barva stolpcev
	barPlot->setPen(pen);
	barPlot->setBrush(brush);
	//popravljen izris
	barPlot->setAntialiased(true);
	//širina stolpcev
	barPlot->setWidthType(QCPBars::WidthType::wtPlotCoords);
	barPlot->setWidth(1);
	//barPlot->addToLegend();

	return barPlot;
}

QCPGraph* MBsoftware::CreateGraphPlot(QCustomPlot * parentQCP, QString title, QPen pen, QBrush brush)
{
	QCPGraph * graphPlot = new QCPGraph(parentQCP->xAxis, parentQCP->yAxis);
	//naslov
	graphPlot->setName(title);
	//barva in izgled grafa
	graphPlot->setPen(pen);
	graphPlot->setBrush(brush);
	//popravljen izris
	graphPlot->setAntialiased(true);
	//legenda
	
	graphPlot->addToLegend();
	

	return graphPlot;
}
void MBsoftware::OnLoadBarChartData(int i)
{
	double upperToleranceVal = 70.0f;//Zgornja toleranca
	double lowerToleranceVal = 15.0f;//Spodanja toleranca
	QVector<double> measurementIdx(100, 0);//število meritev
	QVector<double> measurementValGood(100, 0.0f);//meritve dobre
	QVector<double> measurementValBad(100, 0.0f);//meritve slabe
	QVector<double> upperToleranceData(100, lowerToleranceVal);//spodanja toleranca
	QVector<double> lowerToleranceData(100, upperToleranceVal);//zgornja toleranca

	srand(time(0));

	for (int i = 0; i < measurementIdx.size(); ++i)
	{
		double yValue = rand() % 100;
		measurementIdx[i] = (double)i;

		if (yValue >= lowerToleranceVal && yValue <= upperToleranceVal)//znotraj tolerančnega območja
		{
			measurementValGood[i] = yValue;
			measurementValBad[i] = 0;
		}

		else//izven tolerančnega območja
		{
			measurementValBad[i] = yValue;
			measurementValGood[i] = 0;
		}
	}
	//nastavimo tolerančne meje (ne bi bilo potrebno to početi vsakič če ostaja konstantno)
	upperToleranceGraph[i]->setData(measurementIdx, upperToleranceData);
	lowerToleranceGraph[i]->setData(measurementIdx, lowerToleranceData);
	//posodobimo slabe in dobre stolpce
	barPlotGood[i]->setData(measurementIdx, measurementValGood);
	barPlotBad[i]->setData(measurementIdx, measurementValBad);

	//Posodobimo graf
	customPlot[i]->replot();
}

void MBsoftware::OnUpdateBarChartData(int i, int currDowel)
{
		double upperToleranceVal = 70.0f;//Zgornja toleranca
		double lowerToleranceVal = 15.0f;//Spodanja toleranca
		QVector<double> measurementIdx(100, 0);//število meritev
		QVector<double> measurementValGood(100, 0.0f);//meritve dobre
		QVector<double> measurementValBad(100, 0.0f);//meritve slabe

		int currPiece = currDowel;
		double yValue;
		//srand(time(0));
		int index = i;
		float avrage = 0;
		float value;
		float min = 1000;
		float max = 0;
		float razpon;

			if (index == 0)
			{
				upperToleranceVal = measureObject.lenghtTolerancePlus;
				lowerToleranceVal = measureObject.lenghtToleranceMinus;

			}
			else
			{
				upperToleranceVal = measureObject.diameterTolerancePlus;
				lowerToleranceVal = measureObject.diameterToleranceMinus;
			}

			razpon = upperToleranceVal - lowerToleranceVal;
			razpon = razpon * 0.3; //da dobimo 10 prodentov za zgoraj in spodaj 
			customPlot[i]->xAxis->setRange(0, 100);
			customPlot[i]->yAxis->setRange(lowerToleranceVal - razpon, upperToleranceVal + razpon);
			mTagUpper[i]->setText(QString("%1").arg(upperToleranceVal));
			mTagUpper[i]->updatePosition(upperToleranceVal);
			mTagLower[i]->setText(QString("%1").arg(lowerToleranceVal));
			mTagLower[i]->updatePosition(lowerToleranceVal);

			//upperToleranceData[0] = lowerToleranceVal;
			//upperToleranceData[99] = upperToleranceVal;

			//for (int i = 0; i < measurementIdx.size(); ++i)
			//{
			int count = currPiece;
				for (int j = measurementIdx.size()-1;  j >=0; j--)//v zdodovini grem v minus
				{
					avrage = 0;
					min = 1000;
					max = 0;
				measurementIdx[j] = (double)j;

				if (index == 0)				
					yValue = measureObject.dowelLenght[count];
				else
				{
					for (int k = 1; k < 6; k++)
					{
						value = measureObject.measurementDIC[count][k];

						if (value < min)
							min = value;

						if (value > max)
							max = value;

						avrage += value;
						

					}
					if (index == 1) //avr
						yValue = avrage / 5;
					else if (index == 2)
						yValue = max;
					else if (index == 3)
						yValue = min;

				}

				if (yValue >= lowerToleranceVal && yValue <= upperToleranceVal)//znotraj tolerančnega območja
				{
					measurementValGood[j] = yValue;
					measurementValBad[j] = 0;
				}

				else//izven tolerančnega območja
				{
					measurementValBad[j] = yValue;
					measurementValGood[j] = 0;
				}
				count--;
				if (count < 0)
					count = 127;
			}
			//nastavimo tolerančne meje (ne bi bilo potrebno to početi vsakič če ostaja konstantno)
				QVector<double> upperToleranceData(100, lowerToleranceVal);//spodanja toleranca
				QVector<double> lowerToleranceData(100, upperToleranceVal);//zgornja toleranca
			//upperToleranceGraph[index]->setData(1, upperToleranceVal);
			upperToleranceGraph[index]->setData(measurementIdx, upperToleranceData);
			lowerToleranceGraph[index]->setData(measurementIdx, lowerToleranceData);
			//posodobimo slabe in dobre stolpce
			barPlotGood[index]->setData(measurementIdx, measurementValGood);
			barPlotBad[index]->setData(measurementIdx, measurementValBad);

			//Posodobimo graf
			customPlot[index]->replot();
		
}

void MBsoftware::OnUpdateGaussChartData(int i, int currDowel)
{
	double upperToleranceVal = 70.0f;//Zgornja toleranca
	double lowerToleranceVal = 15.0f;//Spodanja toleranca
	QVector<double> measurementIdx(100, 0);//število meritev
	



	int currPiece = currDowel;
	double yValue;
	//srand(time(0));
	int index = i;
	float value;
	float razpon;

	if (index == 0)
	{
		upperToleranceVal = measureObject.lenghtTolerancePlus;
		lowerToleranceVal = measureObject.lenghtToleranceMinus;
	}
	else
	{
		upperToleranceVal = measureObject.diameterTolerancePlus;
		lowerToleranceVal = measureObject.diameterToleranceMinus;
	}

	razpon = upperToleranceVal - lowerToleranceVal;
	razpon = razpon * 0.8; //da dobimo 10 prodentov za zgoraj in spodaj 
	

	float minValue = lowerToleranceVal - razpon;
	float maxValue = upperToleranceVal + razpon;

	float range = (maxValue - minValue) / 40;

	int nrM = 40;
	float area[40];
	int nrMeasurements[40];
	for (int i = 0; i < 40; i++)
	{
		nrMeasurements[i] = 0;
		if (i == 0)
			area[i] = minValue;
		else
			area[i] = area[i - 1] + range;

	}
	QVector<double> measurementXX(nrM, 0);//stevilo meritev v x smeri pri gaussu
	QVector<double> measurementVal(nrM, 0.0);//meritve dobre
	for (int z = 0; z < nrM; z++)
	{
		measurementXX[z] = (double)area[z];
		/*if (z == 0)
			measurementXX[z] = minValue + range;
		else
			measurementXX[z] = measurementXX[z - 1] + range;*/
	}
	
	int count = currPiece;
	for (int j = 0; j < 10000; j++)//v zdodovini grem v minus
	{
		//avrage = 0;

		//measurementIdx[j] = (double)j;

		if (index == 0)
			yValue = measureObject.lenghtHistory[j];
		else
		{		
			yValue = measureObject.widthHistory[j];
		}
		for (int k = 0; k < nrM; k++)
		{
			if (yValue >= area[k] && yValue <= area[k+1])//znotraj tolerančnega območja
			{
				nrMeasurements[k]++;
				break;
			}

		}


		count--;
		if (count < 0)
			count = 127;
	}
	int max = 0;
	for (int k = 0; k < nrM; k++)
	{
		if (nrMeasurements[k] > max)
			max = nrMeasurements[k];

		measurementVal[k] = (double)nrMeasurements[k];
	}
	plotGauss[index]->yAxis->setRange(0,max + max *0.3);//maax measurements + 5
	QVector<double> upperToleranceData(100, lowerToleranceVal);//spodanja toleranca
	QVector<double> lowerToleranceData(100, upperToleranceVal);//zgornja toleranca
//upperToleranceGraph[index]->setData(1, upperToleranceVal);

	measurementIdx[0] = 0;

	measurementIdx[1] = 10000;
	upperToleranceGraphGauss[index]->setData(upperToleranceData, measurementIdx);
	lowerToleranceGraphGauss[index]->setData(lowerToleranceData, measurementIdx);


	plotGauss[index]->xAxis->setRange(lowerToleranceVal - razpon, upperToleranceVal + razpon);

	//posodobimo slabe in dobre stolpce
 	barPlotGauss[index]->setData(measurementXX, measurementVal);
	barPlotGauss[index]->setWidth(range);


	plotGauss[index]->replot();

}

