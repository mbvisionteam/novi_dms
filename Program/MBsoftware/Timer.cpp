#include "stdafx.h"
#include "Timer.h"

Timer::Timer()
{
	start.QuadPart = 0;
	stop.QuadPart = 0;
	timeCounter = 0;
	frequency = 0;
	elapsed = 0;
	// frekvenca procesorja...ticks per second
	QueryPerformanceFrequency(&PROC_FREQUENCY);
	PROC_FREQUENCY_GHZ = (PROC_FREQUENCY.QuadPart * 1000.0) / 1000000000.0;
}

Timer::~Timer()
{
}

void Timer::Init()
{
	start.QuadPart = 0;
	stop.QuadPart = 0;
	timeCounter = 0;
	frequency = 0;
	elapsed = 0;

	// frekvenca procesorja...ticks per second
	QueryPerformanceFrequency(&PROC_FREQUENCY);
	PROC_FREQUENCY_GHZ = (PROC_FREQUENCY.QuadPart * 1000.0) / 1000000000.0;
}


double Timer::ElapsedTime()
{
	double diff = 0;

	if ((stop.QuadPart > 0) && (start.QuadPart > 0))
	{
		elapsed = ((stop.QuadPart - start.QuadPart) * 1000.0 / PROC_FREQUENCY.QuadPart);
	}

	return elapsed;
}

double Timer::CalcFrequency(int counter)
{
	double diff = 0;

	if ((stop.QuadPart > 0) && (start.QuadPart > 0))
	{
		frequency = (1000 / ((stop.QuadPart - start.QuadPart) * 1000.0 / PROC_FREQUENCY.QuadPart)) * counter;
	}

	return frequency;
}

void Timer::SetStart(void)
{
	QueryPerformanceCounter(&start);
}

void Timer::SetStop(void)
{
	QueryPerformanceCounter(&stop);
}
