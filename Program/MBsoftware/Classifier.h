#pragma once

#include "stdafx.h"
#include <QObject>
#include "CLine.h"

typedef cv::ml::SVM SVM;
typedef cv::ml::TrainData TrainData;


struct HogProperties
{

};

struct PreprocessingSettings
{
	int imageDepth = 1;
	cv::Size imageSize = cv::Size(128, 128);
};

struct LearningSettings
{
	int maxIter = 100;
	float epsilon = 0.005;
};


class Classifier : public QObject
{
	Q_OBJECT

public:
	Classifier(QObject *parent);
	Classifier();
	~Classifier();

	void SetImagesPath(QString dirPath, QString mode = "train");
	void GetTrainImages();
	void GetTestImages();
	void PrepareTrainData();
	void PrepareTestData();
	void GetHOGFromImages();
	void GetTrainingFeatures(int featureType);

	bool LearnClassifier();
	void TestClassifier();
	Ptr<SVM> classifier;
	HogProperties hogProp;
	PreprocessingSettings preSett;
	LearningSettings learningSettings;


private:
	QStringList allImages[2];
	QStringList trainImages[2];
	QStringList testImages[2];
	QString imagesDir;
	QString testImagesDir;

	int *labels;
	HOGDescriptor hog;
	cv::SIFT sift;
	Ptr<TrainData> trainingData;
	Ptr<TrainData> testData;

};

