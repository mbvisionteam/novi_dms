#pragma once
//#include <qwidget.h>
#include <inpout32.h>
#include "ui_ControlLPT.h"
#include "Signals.h"

class ControlLPT :public QWidget
{
	Q_OBJECT;

public:
	ControlLPT();
	ControlLPT(short portAddress, bool controlAsInput,QString referencePath);
	~ControlLPT();
	void closeEvent(QCloseEvent * event);
	void SetControlByteAsInput(bool isInput);

public:

	short address;
	QTimer*				viewTimer;

	UCHAR				portStatusValue;
	UCHAR				portDataValue;
	UCHAR				portControlValue;
	UCHAR				controlValue;
	bool				controlByteInput;

	//Podatkovni, statusni in kontrolni biti
	Signals				data[8];
	Signals				status[5];
	Signals				control[4];
	QCheckBox			*checkData[8];
	QCheckBox			*checkStatus[5];
	QCheckBox			*checkControl[4];
	QReadWriteLock		lockReadWrite;
	QString				referencePath;



private:
	Ui::LPT ui;
	int id;
	static int			objectCount; //how many created objects

public:
	void OnShowDialog();
	void InitAddresses();
	int ReadData(void);
	void InitValues();
	void CreateDisplay(void);
	void SetDataByte(UCHAR bits);
	void SetDataByte(UCHAR bits, bool value);
	void SetOutputValue(int outputNumber, bool value);
	unsigned char GetStatusValue(void);
	void SetControlByte(int contolBit, bool value);
	void SetControlByte(char bits);
	void SetControlByteAllON(void);
	void SetControlByteAllOFF(void);
	bool IsDriverOpen();

	unsigned char GetPortControlValue(void);

	void WriteBytes(short data);
	short ReadBytes(short address);
	void SetOutputSignals();
	void SetInputSignals();
	void SetControlSignals(UCHAR data);
	void GetOutputData();
	void GetStatusData();
	short GetControlData();
	short ConvertControl();
	short PrepareDataBytes(UCHAR bitAddress, bool bitValue);
	short PrepareControlBytes(UCHAR bitAddress, bool bitValue);

public slots:
	void OnClickedData(int index);
	void OnClickedControl(int index);
	void TimerViewSignals();
};
