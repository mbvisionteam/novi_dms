#pragma once

#include "stdafx.h"
class CCustomGraphicsItem: public QGraphicsItem
{
public:


	CCustomGraphicsItem();
	QRectF boundingRect() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
	~CCustomGraphicsItem();

	void DrawDot(QPoint center, int size);
};

