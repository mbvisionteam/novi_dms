#pragma once

#include <QWidget>
#include "ui_DMS.h"
#include "ui_DMStransitionTest.h"
#include <QtNetwork/qudpsocket.h>
#include <QtNetwork/qtcpsocket.h>
#include "Timer.h"
#include "qpainter.h"
#include "Measurand.h"


class DMSudp : public QWidget
{
	Q_OBJECT

public:
	DMSudp(QWidget *parent = Q_NULLPTR);
	DMSudp(QString ipAddress, int port,QString referencePath);
	QWidget	* transitionTestWidget;
	void closeEvent(QCloseEvent * event);
	~DMSudp();

	void ConnectMeasurand(Measurand * objects);


	static Measurand*						measureObject;
	static std::vector<Timer*>				DMSudp::processingTimer;
private:
	Ui::DMS ui;
	Ui::DMSTestWindow ui2;
	QGraphicsScene*			sceneVideo[6];
	QGraphicsScene*			sceneDowel;
	QGraphicsPixmapItem*	pixmapVideo[6];
	QGraphicsPixmapItem*	pixmapDowel;

	QGraphicsScene*			sceneTimeLine;
	QGraphicsPixmapItem*	pixmapTimeLine;

	QGraphicsScene*			sceneHorizontalView;
	QGraphicsPixmapItem*	pixmapHorizontalView;

	QGraphicsScene*			sceneBlowView;
	QGraphicsPixmapItem*	pixmapBlowView;



	QGraphicsScene*			sceneDowelImage;
	QGraphicsPixmapItem*	pixmapDowelImage;

	//za izris moznika na sliki
	QGraphicsPolygonItem* area[11];
	QGraphicsTextItem*	maxErrorText[6];
	QGraphicsTextItem*  negativeErrorText[6];
	QGraphicsTextItem* nrMeasurementesText[6];
	QGraphicsTextItem* speedMeasurementText;
	QGraphicsTextItem* lenghtMeasurementText;
	QGraphicsTextItem* maxMeasurementText;
	QGraphicsTextItem* elipseText;

	QString					referencePath;
	QLabel*				labelNrMeasurement[MAX_MEASUREMENT];
	QLabel*				labelNrMeasurementPosition[MAX_MEASUREMENT];
	QLabel*				labelCameraMeaurements[MAX_MEASUREMENT][NR_DMS_CAM];

	QTableView  *parametersTable;
	QStandardItemModel* modelTable;
	vector<vector<QStandardItem*>>  standardTableItem;

	int						transitionDataTest[NR_DMS_CAM][5][2];//tabela za zgornje in spodnje prehode pri testnem oknu za prehode 
	int						isTransitionDisplaySet;

	
	bool					isCheckedShowArea;
	bool					isCheckedShowTolerance;
	bool					isCheckedShowConusLines;
	bool					isCheckedShowDowel;
	bool					isCheckedShowMeasurements;
	int						horLineCurrMeasurement;
	byte					parseBuffer[15000];
	int						parseCounter;
	QByteArray				parseCommands[500];
	int						parseCommandCounter;


public:
	QTimer*							viewTimer;
	static QTimer*							connectionTimer;
	QUdpSocket*		udpClient;

	QHostAddress hostIp;
	Timer							dataReadyFreq;
	int port;
	QString ipAdd;
	int connectionOnCounter;
	bool isConnected, isConnectedOld;
	bool isInitialized;
	void ReadPendingDatagrams();


	//�as �akanja na povezavo v milisekundah
	int waitTime;
	int timout;
	int selectedCameraDraw;
	QString		xilinxVersion;

	void WriteDatagram(QByteArray data);


	void OnShowDialog();
	void OnShowDialogTransitionTest();
	void CreateDisplay();
	void CreateDisplayTransitionTest();
	void CreateMeasurementTable();
	void DrawMeasurementTable(int param);
	void CreateDowelImage();
	
	void UpdateDowelImage(int currPiece);

	void SetDisplay();
	void StartClient();
	void InitDMS();
	void SetCameraSettings(int nrCam);
	void SetBlowValve(int timeBlow, int timeDelay);


	void ReadDowelFromFile(QString filePath);
	int isLive;
	int isLiveTransitions;
	virtual void paintEvent(QPaintEvent * event);

	QByteArray tmpDowelData;
	
	int gain[NR_DMS_CAM];
	int offset[NR_DMS_CAM];
	int detectWindow[NR_DMS_CAM][2];

	int currDowelTopPointsZac[6][500][2];
	int currDowelTopPointsKon[6][500][2];
	int currDowelTopPointsZacDraw[6][500][2];
	int currDowelTopPointsKonDraw[6][500][2];


	float currDowelSirinaZgoraj[500];
	float currDowelSirinaSpodaj[500];


	int currFramePrehod[MAX_MEASUREMENT][2];//horizontalni prehodi spredaj in zadaj
	float currDowelPrehodiReal[MAX_MEASUREMENT][2];//v mm 
	int currFramePrehodAll[MAX_MEASUREMENT][16];//shranjujem v tabele vse predhode za lazje debagiranje //posiljam si max 16 prehodov
	int currFramePrehodAllReal[MAX_MEASUREMENT][16];//realne kkordinate ze v milimetrih
	int currFramePrehodType[MAX_MEASUREMENT][16];//tip prehoda bom potreboval se za risanje horizontalne linije na ekran


	float			dowelSpeed[16];
	float			dowelLenght[16];
	int				dowelNrMeasurements[16];
	int				dowelReady[16];
	int				currentDowelDraw;
	int			lastPieceScanned;//zadnji poskeniran moznik
	int			shownDowel; //ternutni prikazani moznik
	int			historyCounter;
	int frameUpCounter;
	


	QSpinBox *spinWindwSTOP[NR_DMS_CAM];
	QSpinBox *spinWindwSTART[NR_DMS_CAM];
	QSpinBox *spinGain[NR_DMS_CAM];
	QSpinBox *spinOffset[NR_DMS_CAM];
	QImage liveImage[NR_DMS_CAM];
	int widthDmsCam;
	int dmsConnected;
	int nrRecievedDowels;


private slots:
	void OnViewTimer();

	void OnSend();
	void OnClickedLive();
	void OnClickedLiveTranstions();
	void OnClickedGetTimeBase();
	void WriteCamSettings();
	void WriteOneCamSetting(int index, QString setting);
	void ReadCamSettings();

	void ShowImage(int, QByteArray image);//
	void ShowTransitions(int, QByteArray image);//parsa podatke iz stringa in jih prikaze v ui2
	void ShowBlowImage(int currPiece, int frame);
	void ShowTimeLine(QByteArray image);//parsa podatke iz stringa in jih prikaze v ui2 za casovno bazo
	void OnToggledCheckBox1(bool);//funkcije za ogled slike
	void OnToggledCheckBox2(bool);
	void OnToggledCheckBox3(bool);
	void OnToggledCheckBox4(bool);
	void OnToggledCheckBox5(bool);
	void OnClickedNextHorMeasurement();
	void OnClickedPrevHorMeasurement();
	void OnClickedHistoryUp();
	void OnClickedHistoryDown();
	void OnClickedLastScanned();
	void OnClickedFrameUp();
	void OnClickedFrameDown();
	void UpdateList();
	

public slots:
	void ReadDowelString(QByteArray Qstring);//parsam podatke, ki so v stringu moznika
	void CreateDowelFromString(int currentPiece);//iz paraanga stringa prepisemo vse podatke o mozniku in zacnemo pripravo izracunov za meritve in pozicij
	void MeasureDowel(int currentPiece);
	void MeasureDowelRadius(int currentPiece);
	void MeasureDowelConus(int currentPiece);


	void OnClientConnected();

	void OnClientDisconnected();
	void OnConnectionTimeout();
	void SpinGainChanged(int index, int gain);

	void SpinOffsetChanged(int index, int offset);

	void SpinDetectWindowChanged(int index, int topBottom, int value);

	void SpinGainChangedTest(int gain);
	void ShowDowel(int nrDowel);
	void ShowMeasurements(int nrDowel);
	void ShowHorizontalCamera(int curr, int frame);
	void OnClickedSelectedDMSCamNew();
	void OnSignalXilinxVersion(QByteArray array);

	
	

signals:
	void frameReadySignal(int id, QByteArray image);//signal za prikaz nove slike
	void transitionReadySignal(int nrCam, QByteArray parseData);//signal za prikaz test transitions
	void timeLineSignal(QByteArray parseData);//signal za sprejem casovne baze
	void versionSignal(QByteArray version); //signl za verzijo iz xilinxa
	

	

};
