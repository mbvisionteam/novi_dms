#pragma once
#include <QtNetwork/qtcpsocket.h>
#include <QtNetwork/qtcpserver.h>
#include <QtNetwork/qhostaddress.h>
#include <QObject>
#include "ui_Tcp.h"
#define REQUEST_SIZE 1024
#define MAX_CONN	3

class TCP : public QWidget
{
	Q_OBJECT

public:
	TCP(QString ipAddress, int port, bool isServer);
	~TCP();
private: 
	Ui::TcpForm ui;

public:
	static QTcpSocket*						tcpConnections[MAX_CONN];
	static QString							tcpConnectionsName[MAX_CONN];
	static QTimer*							connectionTimer;
	static QTimer*							sendRcvTImer;
	QString address;
	QString recievedData;
	quint16 port;
	QTcpServer*								server;
	QTcpSocket*								client;
	bool									clientConnected;
	bool									isServer;
	char									dataBuf[REQUEST_SIZE];
	int										dataBufCounter;
	int										returnIntArray[REQUEST_SIZE];
	char									returnCharArray[REQUEST_SIZE];



	int numOfConnections;
	//�as �akanja na povezavo v milisekundah
	int waitTime;
	int timout;
	

	//PLC
	bool dataSend;
	bool readState;
	bool writeState;
	int readWriteCounter = 0;
	int D200;
	int D201;

private:
	QHostAddress addr;

	bool slovensko;

public:
	void OnShowDialog();
	void StartServer();
	void StartClient();
	void WriteData(QByteArray text);
	void Disconnect();

	//Mitchubitshi PLC
	void ReadDataRegister();
	void RequestPLCregister(int regNum);
	void WriteD201Register(int data);
	void WritePLC(QByteArray data);
	int  WriteServerToClient(QString name, QByteArray data);

	//�as �akanja na povezavo v milisekundah

public slots:
	void OnNewConnection();
	void OnConnectionTimeout();
	void SendRcvLoop();
	void OnServerReadyRead(int connection);
	void OnClientReadyRead();
	void OnClientReadyReadPLC();
	void OnClientConnected();
	void OnClientDisconnected();
	void OnClientDisconnected(int get);
	void OnClickedSend();
	void OnAppendText(QString text);

	




signals:
	void connectionTimeout();
	void dataReady(QString connection, int parseCounter);
	void appendText(QString text);
	
};
