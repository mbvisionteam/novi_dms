#include "stdafx.h"
#include "GeneralSettings.h"

/*GeneralSettings::GeneralSettings(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}*/

GeneralSettings::GeneralSettings(QString referencePath)
{
	ui.setupUi(this);
	this->referencePath = referencePath;

	connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(OnConfirm()));
	connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(OnRejected()));



	ui.comboBox->addItem("NonStop Mode");
	ui.comboBox->addItem("AutoStopMode");

	ui.comboPresortMode->addItem("Camera OFF");
	ui.comboPresortMode->addItem("Small parts OUT");
	ui.comboPresortMode->addItem("Color OUT");
	ui.comboPresortMode->addItem("COLOR + SMALL parts OUT");

	ui.comboRotatorSpeed->addItem("Manual Speed");
	ui.comboRotatorSpeed->addItem("Setting 1 25Hz");
	ui.comboRotatorSpeed->addItem("Setting 2 40Hz");
	ui.comboRotatorSpeed->addItem("Setting 3 45Hz");
	ui.comboRotatorSpeed->addItem("Setting 4 50Hz");
	ui.comboRotatorSpeed->addItem("Setting 5 54Hz");
	ui.comboRotatorSpeed->addItem("Setting 6 58Hz");
	ui.comboRotatorSpeed->addItem("Setting 7 62Hz");



	LoadSettings();
	SetState();

	speedSet = ui.comboRotatorSpeed->itemText(rotatorSpeedMode);


	slovensko = SLOVENSKO;

	if (slovensko)
	{
		this->setWindowTitle("Splosne nastavitve");
	}
}

GeneralSettings::~GeneralSettings()
{

}

void GeneralSettings::closeEvent(QCloseEvent * event)
{
	close();
	updateDraw = 1;
}

void GeneralSettings::OnShowDialog()
{
	setWindowModality(Qt::ApplicationModal);
	SetState();
	show();
}

void GeneralSettings::SaveSettings()
{

	QSettings settings(this->filePath, QSettings::IniFormat);
	QString tmpGroup;
	QStringList list;
	tmpGroup = QString("Settings");
	settings.beginGroup(tmpGroup);

	int count = 0;

		list << QString("%1").arg(workingMode);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(nrDowelToStop);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(workingModePresort);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(presortSensorEnable);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(rotatorSpeedMode);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(blowFactor);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(delayToValve);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(startCleanEnable);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(cleanEnable);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(cleanPeriod);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(cleanValve1Offset);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(cleanValve2Offset);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(cleanValve1Duration);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(cleanValve2Duration);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(presortSensorTimeBlow);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(presortSensorOffset);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(presortCameraTimeBlow);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(presortCameraTimeOffset);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(elevatorStartTime);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(elevatorStopTime);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(dowelStuckStartBlink);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(dowelStuckStopMachine);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;
	
		list << QString("%1").arg(presortStopEnable);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;

		list << QString("%1").arg(nrBadPresortToStop);
		settings.setValue(QString("parameter%1").arg(count), list);
		list.clear();
		count++;


	settings.endGroup();

}

int GeneralSettings::LoadSettings()
{
	QStringList values;
	int count = 0;
	this->filePath = referencePath + QString("/%1/settings/GeneralSettings/GeneralSettings.ini").arg(REF_FOLDER_NAME);

	QSettings settings(this->filePath, QSettings::IniFormat);


		QString tmpGroup;
		tmpGroup = QString("Settings");
		settings.beginGroup(tmpGroup);
		//inputs
		const QStringList childKeys = settings.childKeys();
		int nrCam = 0;
		if (childKeys.size() > 0)
		{
			do {

				values.clear();
				values = settings.value(QString("parameter%1").arg(count)).toStringList();
				if (values.size() > 0)
				{
					switch (count)
					{
						case(0):
						{
							workingMode = values[0].toInt();
							break;
						}
						case(1):
						{
							nrDowelToStop = values[0].toInt();
							break;
						}
						case(2):
						{
							workingModePresort = values[0].toInt();
							break;
						}
						case(3):
						{
							presortSensorEnable = values[0].toInt();
							break;
						}
						case(4):
						{
							rotatorSpeedMode = values[0].toInt();
							break;
						}
						case(5):
						{
							blowFactor = values[0].toInt();
							break;
						}
						case(6):
						{
							delayToValve = values[0].toInt();
							break;
						}
						case(7):
						{
							startCleanEnable = values[0].toInt();
							break;
						}
						case(8):
						{
							cleanEnable = values[0].toInt();
							break;
						}
						case(9):
						{
							cleanPeriod = values[0].toInt();
							break;
						}
						case(10):
						{
							cleanValve1Offset = values[0].toInt();
							break;
						}
						case(11):
						{
							cleanValve2Offset = values[0].toInt();
							break;
						}
						case(12):
						{
							cleanValve1Duration = values[0].toInt();
							break;
						}
						case(13):
						{
							cleanValve2Duration = values[0].toInt();
							break;
						}

						case(14):
						{
							presortSensorTimeBlow = values[0].toInt();
							break;
						}
						case(15):
						{
							presortSensorOffset = values[0].toInt();
							break;
						}
						case(16):
						{
							presortCameraTimeBlow = values[0].toInt();
							break;
						}
						case(17):
						{
							presortCameraTimeOffset = values[0].toInt();
							break;
						}
						case(18):
						{
							elevatorStartTime = values[0].toInt();
							break;
						}
						case(19):
						{
							elevatorStopTime = values[0].toInt();
							break;
						}
						case(20):
						{
							dowelStuckStartBlink = values[0].toInt();
							break;
						}
						case(21):
						{
							dowelStuckStopMachine = values[0].toInt();
							break;
						}
						case(22):
						{
							presortStopEnable = values[0].toInt();
							break;
						}
						case(23):
						{
							nrBadPresortToStop = values[0].toInt();
							break;
						}

					}

				}
				count++;
			} while (values.size() > 0);
		}
		settings.endGroup();


		return 1;
}

void GeneralSettings::SetState()
{


	//lamele
	ui.comboBox->setCurrentIndex(workingMode);
	ui.comboPresortMode->setCurrentIndex(workingModePresort);
	ui.comboRotatorSpeed->setCurrentIndex(rotatorSpeedMode);
	ui.lineNrDowelsAvtoStop->setText(QString("%1").arg(nrDowelToStop));

	if (presortSensorEnable == 1)
		ui.checkPresortSensorEnable->setChecked(true);
	else
		ui.checkPresortSensorEnable->setChecked(false);

	
		ui.editTimeBlow->setText(QString("%1").arg(blowFactor));
		ui.editOffsetToBlow->setText(QString("%1").arg(delayToValve));


		ui.checkStartClean->setChecked((bool)startCleanEnable);
		ui.checkCleanEnable->setChecked((bool)cleanEnable);

		ui.editCleanPeriode->setText(QString("%1").arg(cleanPeriod));
		ui.editCleanValve1Offset->setText(QString("%1").arg(cleanValve1Offset));
		ui.editCleanValve2Offset->setText(QString("%1").arg(cleanValve2Offset));
		ui.editCleanValve1Duration->setText(QString("%1").arg(cleanValve1Duration));
		ui.editCleanValve2Duration->setText(QString("%1").arg(cleanValve2Duration));


		ui.editBlowTimeY->setText(QString("%1").arg(presortSensorTimeBlow));
		ui.editBlowOffsetY->setText(QString("%1").arg(presortSensorOffset));
		ui.editBlowTimeX->setText(QString("%1").arg(presortCameraTimeBlow));
		ui.editBlowOffsetX->setText(QString("%1").arg(presortCameraTimeOffset));
		ui.editElevatorStart->setText(QString("%1").arg(elevatorStartTime));
		ui.ediElevatorStop->setText(QString("%1").arg(elevatorStopTime));
		ui.editDowelStuckBlink->setText(QString("%1").arg(dowelStuckStartBlink));
		ui.editDowelStuckStop->setText(QString("%1").arg(dowelStuckStopMachine));



		speedSet = ui.comboRotatorSpeed->itemText(rotatorSpeedMode);



		ui.checkPresortStopEnable->setChecked(presortStopEnable);
		ui.editPresortStopNrPieces->setText(QString("%1").arg(nrBadPresortToStop));

}

void GeneralSettings::OnConfirm()
{

	workingMode = ui.comboBox->currentIndex();
	workingModePresort = ui.comboPresortMode->currentIndex();
	rotatorSpeedMode = ui.comboRotatorSpeed->currentIndex();
	nrDowelToStop = ui.lineNrDowelsAvtoStop->text().toInt();


	presortSensorEnable = (bool)ui.checkPresortSensorEnable->checkState();

	blowFactor = ui.editTimeBlow->text().toInt();
	delayToValve = ui.editOffsetToBlow->text().toInt();

	

	startCleanEnable = (bool)ui.checkStartClean->checkState();
	cleanEnable = (bool)ui.checkCleanEnable->checkState();
	cleanPeriod = ui.editCleanPeriode->text().toInt();
	cleanValve1Offset = ui.editCleanValve1Offset->text().toInt();
	cleanValve2Offset = ui.editCleanValve2Offset->text().toInt();
	cleanValve1Duration = ui.editCleanValve1Duration->text().toInt();
	cleanValve2Duration = ui.editCleanValve2Duration->text().toInt();

	presortSensorTimeBlow = ui.editBlowTimeY->text().toInt();
	presortSensorOffset = ui.editBlowOffsetY->text().toInt();
	presortCameraTimeBlow = ui.editBlowTimeX->text().toInt();
	presortCameraTimeOffset = ui.editBlowOffsetX->text().toInt();
	elevatorStartTime = ui.editElevatorStart->text().toInt();
	elevatorStopTime = ui.ediElevatorStop->text().toInt();
	dowelStuckStartBlink = ui.editDowelStuckBlink->text().toInt();
	dowelStuckStopMachine = ui.editDowelStuckStop->text().toInt();

	presortStopEnable = (bool)ui.checkPresortStopEnable->checkState();
	nrBadPresortToStop = ui.editPresortStopNrPieces->text().toInt();
	


	SaveSettings();
	closeEvent(NULL);
}

void GeneralSettings::OnRejected()
{
	closeEvent(NULL);
}





