#include "stdafx.h"
#include "DMS.h"

#include <QWidget>

QTimer*										DMS::connectionTimer;

double timeCount = 0;
Timer testTimer;

float timeDelay11[1000];


DMS::DMS(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);


}

DMS::DMS(QString ipAddress, int port)
{

	ui.setupUi(this);
	this->port = port;
	this->ipAdd = ipAddress;

	transitionTestWidget = new(QWidget);
	ui2.setupUi(transitionTestWidget);
	isLive = 0;
	isLiveTransitions = 0;
	isTransitionDisplaySet = 0;
	for(int i = 0; i < NR_DMS_CAM; i++)
	{
		sceneVideo[i] = new QGraphicsScene(this);
		pixmapVideo[i] = new QGraphicsPixmapItem();
	}
	
	ui.imageView0->setScene(sceneVideo[0]);
	ui.imageView1->setScene(sceneVideo[1]);
	ui.imageView2->setScene(sceneVideo[2]);
	ui.imageView3->setScene(sceneVideo[3]);
	ui.imageView4->setScene(sceneVideo[4]);
	ui.imageView5->setScene(sceneVideo[5]);
	
	for(int i = 0; i < NR_DMS_CAM; i++)
	sceneVideo[i]->addItem(pixmapVideo[i]);

	sceneDowel = new QGraphicsScene(this);
	pixmapDowel = new QGraphicsPixmapItem();
	//ui.imageViewDowel->setScene(sceneDowel);

	isInitialized = false;
	//udpClient = new QUdpSocket();
		//hostIp.setAddress(ipAddress);
	tcpClient = new QTcpSocket();
	connectionOnCounter = 0;
	connectionOnPrevCounter = 0;
	isConnectedOld = 0;
	isConnected = 0;
	dmsConnected = 0;

	this->port = port;
	viewTimer = new QTimer();
	connectionTimer = new QTimer();
	connectionTimer->setSingleShot(true);
	widthDmsCam = 2200;
	nrRecievedDowels = 0;
	//tcpClient->connectToHost(ipAddress, port);
	timout = 5;
	waitTime = 1000;

	parseCounter = 0;

	parseCommandCounter = 0;

	for (int i  = 0; i < NR_DMS_CAM; i++)
	{
	
		gain[i] = 0;
		offset[i] = 0;
		detectWindow[i][0] = 0;
		detectWindow[i][1] = 0;
	}
	CreateDisplay();
	CreateDisplayTransitionTest();
	StartClient();
	ReadCamSettings();
	SetDisplay();

	connect(ui.buttonLive, SIGNAL(pressed()), this, SLOT(OnClickedLive()));
	connect(viewTimer, SIGNAL(timeout()), this, SLOT(OnViewTimer()));
	connect(connectionTimer, SIGNAL(timeout()), this, SLOT(OnConnectionTimeout()));
	connect(this, SIGNAL(frameReadySignal(int, QByteArray)), this, SLOT(ShowImage(int,QByteArray)));
	connect(this, SIGNAL(transitionReadySignal(int, QByteArray)), this, SLOT(ShowTransitions(int,QByteArray)));
	connect(ui.buttonSend, SIGNAL(pressed()), this, SLOT(OnSend()));
	connect(ui.lineSend, SIGNAL(returnPressed()), this, SLOT(OnSend()));
	connect(ui.buttonShowDowel, SIGNAL(pressed()), this, SLOT(ReadDowelString()));
	connect(ui2.buttonEnableTest, SIGNAL(pressed()), this, SLOT(OnClickedLiveTranstions()));

	connect(tcpClient, SIGNAL(connected()), this, SLOT(OnClientConnected()));
	connect(tcpClient, SIGNAL(disconnected()), this, SLOT(OnClientDisconnected()));
	//connectionTimer->start(2000);

	tcpClient->setReadBufferSize(65535);
	

	for (int i = 0; i < 16; i++)
	{
		dowelReady[i] = 0;
	}
}


DMS::~DMS()
{

}

void DMS::OnShowDialog()
{
	show();
	viewTimer->start(100);
}
void DMS::OnShowDialogTransitionTest()
{
	transitionTestWidget->show();

}

void DMS::CreateDisplay()
{
	int singleStep = 50;
	int maxValueGO = 4096;
	int maxValueWidth= 2200;

	QFont spinFont("MS Shell Dlg 2", 12, QFont::Bold);
	
	QVBoxLayout *stopLayout[NR_DMS_CAM];
	QVBoxLayout *startLayout[NR_DMS_CAM];
	QVBoxLayout *gainLayout[NR_DMS_CAM];
	QVBoxLayout *offsetLayout[NR_DMS_CAM];
	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		stopLayout[i] = new QVBoxLayout;
		startLayout[i] = new QVBoxLayout;
		gainLayout[i] = new QVBoxLayout;
		offsetLayout[i] = new QVBoxLayout;
		spinWindwSTOP[i] = new QSpinBox;
		spinWindwSTART[i] = new QSpinBox;
		spinGain[i] = new QSpinBox;
		spinOffset[i] = new QSpinBox;

		spinWindwSTOP[i]->setMinimumHeight(31);
		spinWindwSTOP[i]->setFont(spinFont);
		spinWindwSTOP[i]->setMaximum(maxValueWidth);
		spinWindwSTOP[i]->setMinimum(0);
		spinWindwSTOP[i]->setSingleStep(singleStep);

		spinWindwSTART[i]->setMinimumHeight(31);
		spinWindwSTART[i]->setFont(spinFont);
		spinWindwSTART[i]->setMaximum(maxValueWidth);
		spinWindwSTART[i]->setMinimum(0);
		spinWindwSTART[i]->setSingleStep(singleStep);

		spinGain[i]->setMinimumHeight(31);
		spinGain[i]->setFont(spinFont);
		spinGain[i]->setMaximum(maxValueGO);
		spinGain[i]->setMinimum(0);
		spinGain[i]->setSingleStep(singleStep);

		spinOffset[i]->setMinimumHeight(31);
		spinOffset[i]->setFont(spinFont);
		spinOffset[i]->setMaximum(maxValueGO);
		spinOffset[i]->setMinimum(0);
		spinOffset[i]->setSingleStep(singleStep);

		stopLayout[i]->addWidget(spinWindwSTOP[i]);
		startLayout[i]->addWidget(spinWindwSTART[i]);
		gainLayout[i]->addWidget(spinGain[i]);
		offsetLayout[i]->addWidget(spinOffset[i]);
	}

	ui.groupBoxStop0->setLayout(stopLayout[0]);
	ui.groupBoxStart0->setLayout(startLayout[0]);
	ui.groupBoxGain0->setLayout(gainLayout[0]);
	ui.groupBoxOffset0->setLayout(offsetLayout[0]);

	ui.groupBoxStop1->setLayout(stopLayout[1]);
	ui.groupBoxStart1->setLayout(startLayout[1]);
	ui.groupBoxGain1->setLayout(gainLayout[1]);
	ui.groupBoxOffset1->setLayout(offsetLayout[1]);

	ui.groupBoxStop2->setLayout(stopLayout[2]);
	ui.groupBoxStart2->setLayout(startLayout[2]);
	ui.groupBoxGain2->setLayout(gainLayout[2]);
	ui.groupBoxOffset2->setLayout(offsetLayout[2]);

	ui.groupBoxStop3->setLayout(stopLayout[3]);
	ui.groupBoxStart3->setLayout(startLayout[3]);
	ui.groupBoxGain3->setLayout(gainLayout[3]);
	ui.groupBoxOffset3->setLayout(offsetLayout[3]);

	ui.groupBoxStop4->setLayout(stopLayout[4]);
	ui.groupBoxStart4->setLayout(startLayout[4]);
	ui.groupBoxGain4->setLayout(gainLayout[4]);
	ui.groupBoxOffset4->setLayout(offsetLayout[4]);

	ui.groupBoxStop5->setLayout(stopLayout[5]);
	ui.groupBoxStart5->setLayout(startLayout[5]);
	ui.groupBoxGain5->setLayout(gainLayout[5]);
	ui.groupBoxOffset5->setLayout(offsetLayout[5]);

	
	connect(spinGain[0], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(0, val); });
	connect(spinGain[1], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(1, val); });
	connect(spinGain[2], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(2, val); });
	connect(spinGain[3], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(3, val); });
	connect(spinGain[4], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(4, val); });
	connect(spinGain[5], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinGainChanged(5, val); });

	connect(spinOffset[0], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(0, val); });
	connect(spinOffset[1], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(1, val); });
	connect(spinOffset[2], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(2, val); });
	connect(spinOffset[3], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(3, val); });
	connect(spinOffset[4], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(4, val); });
	connect(spinOffset[5], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinOffsetChanged(5, val); });

	connect(spinWindwSTART[0], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(0,0, val); });
	connect(spinWindwSTART[1], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(1, 0, val); });
	connect(spinWindwSTART[2], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(2, 0, val); });
	connect(spinWindwSTART[3], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(3, 0, val); });
	connect(spinWindwSTART[4], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(4, 0, val); });
	connect(spinWindwSTART[5], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(5, 0, val); });

	connect(spinWindwSTOP[0], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(0, 1, val); });
	connect(spinWindwSTOP[1], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(1, 1, val); });
	connect(spinWindwSTOP[2], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(2, 1, val); });
	connect(spinWindwSTOP[3], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(3, 1, val); });
	connect(spinWindwSTOP[4], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(4, 1, val); });
	connect(spinWindwSTOP[5], (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [this](int val) { SpinDetectWindowChanged(5, 1, val); });



}
void DMS::CreateDisplayTransitionTest()
{
	/*for (int i = 0; i < NR_DMS_CAM; i++)
	{
		for (int j = 0; j < 5; j++)//collum
		{
			for (int k = 0; k < 2; k++)//row
			{
				transitionDataTestLabel[i][j][k] = new QLabel();
				transitionDataTestLabel[i][j][k]->setFrameStyle(QFrame::Panel | QFrame::Sunken);
				if (i == 0)
				{
					ui2.gridLayoutCam0->addWidget(transitionDataTestLabel[i][j][k], k+1, j+1);
				}
				if (i == 1)
				{
					ui2.gridLayoutCam1->addWidget(transitionDataTestLabel[i][j][k], k + 1, j + 1);
				}
				if (i == 2)
				{
					ui2.gridLayoutCam2->addWidget(transitionDataTestLabel[i][j][k], k + 1, j + 1);
				}
				if (i ==3)
				{
					ui2.gridLayoutCam3->addWidget(transitionDataTestLabel[i][j][k], k + 1, j + 1);
				}
				if (i ==4)
				{
					ui2.gridLayoutCam4->addWidget(transitionDataTestLabel[i][j][k], k + 1, j + 1);
				}
				if (i == 5)
				{
					ui2.gridLayoutCam5->addWidget(transitionDataTestLabel[i][j][k], k + 1, j + 1);
				}
			}
		}

	}*/
	isTransitionDisplaySet = 1;
}
void DMS::SetDisplay()
{
	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		spinGain[i]->blockSignals(true);
		spinGain[i]->setValue(gain[i]);
		spinGain[i]->blockSignals(false);

		spinOffset[i]->blockSignals(true);
		spinOffset[i]->setValue(offset[i]);
		spinOffset[i]->blockSignals(false);

		spinWindwSTART[i]->blockSignals(true);
		spinWindwSTART[i]->setValue(detectWindow[i][0]);
		spinWindwSTART[i]->blockSignals(false);

		spinWindwSTOP[i]->blockSignals(true);
		spinWindwSTOP[i]->setValue(detectWindow[i][1]);
		spinWindwSTOP[i]->blockSignals(false);
	}
	



}
void DMS::closeEvent(QCloseEvent *event)
{
	viewTimer->stop();
}

void DMS::OnClickedLive()
{
	if (isLive == 0)
	{
		isLive = 1;
		WriteDatagram("L1;");
	}
	else
	{
		isLive = 0;
		WriteDatagram("L0;");
	}

	QByteArray test;
	int tmp = 0;
	for (int i = 0; i < 2200; i++)
	{
		test[i] = tmp;

		tmp++;
		tmp = tmp & 0xff;
	}

	for(int i = 0; i < NR_DMS_CAM;i++)
	emit frameReadySignal(i, test);
	
}

void DMS::OnClickedLiveTranstions()
{
	nrRecievedDowels = 0;
	if (isLiveTransitions == 0)
	{
		isLiveTransitions = 1;
		WriteDatagram("T1;");
	}
	else
	{
		isLiveTransitions = 0;
		WriteDatagram("T0;");
	}



}

void DMS::WriteCamSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = QDir::currentPath() + QString("/%1/DMS/cam_settings.ini").arg(REF_FOLDER_NAME);
	QSettings settings(filePath, QSettings::IniFormat);

	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		values = settings.value(QString("OFFSET%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			settings.setValue(QString("OFFSET%1").arg(i), QString("%1").arg(offset[i]));
			settings.sync();
			values.clear();
		}

		values = settings.value(QString("GAIN%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("GAIN%1").arg(i), QString("%1").arg(gain[i]));
			settings.sync();
			values.clear();
		}
		values = settings.value(QString("STARTDETECT%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("STARTDETECT%1").arg(i), QString("%1").arg(detectWindow[i][0]));
			settings.sync();
			values.clear();
		}
		values = settings.value(QString("STOPDETECT%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("STOPDETECT%1").arg(i), QString("%1").arg(detectWindow[i][1]));
			settings.sync();
			values.clear();
		}
	}



}

void DMS::WriteOneCamSetting(int index, QString setting)
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = QDir::currentPath() + QString("/%1/DMS/cam_settings.ini").arg(REF_FOLDER_NAME);
	QSettings settings(filePath, QSettings::IniFormat);


	if (setting == "OFFSET")
	{
			settings.setValue(QString("OFFSET%1").arg(index), QString("%1").arg(offset[index]));
			settings.sync();
			values.clear();

	}
	else if (setting == "GAIN")
	{
		settings.setValue(QString("GAIN%1").arg(index), QString("%1").arg(gain[index]));
		settings.sync();
		values.clear();
	}
	else if (setting == "STARTDETECT")
	{
		settings.setValue(QString("STARTDETECT%1").arg(index), QString("%1").arg(detectWindow[index][0]));
		settings.sync();
		values.clear();
	}
	else if (setting == "STOPDETECT")
	{
		settings.setValue(QString("STOPDETECT%1").arg(index), QString("%1").arg(detectWindow[index][1]));
		settings.sync();
		values.clear();
	}

}
void DMS::ReadCamSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = QDir::currentPath() + QString("/%1/DMS/cam_settings.ini").arg(REF_FOLDER_NAME);
	QSettings settings(filePath, QSettings::IniFormat);


	//inputs
	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		values = settings.value(QString("OFFSET%1").arg(i)).toStringList();

		if (values.size() > 0)
			offset[i] = (values[0].toInt());
		values.clear();


		values = settings.value(QString("GAIN%1").arg(i)).toStringList();

		if (values.size() > 0)
			gain[i] = (values[0].toInt());
		values.clear();



		values = settings.value(QString("STARTDETECT%1").arg(i)).toStringList();
		if (values.size() > 0)
			detectWindow[i][0] = (values[0].toInt());

		values.clear();

		values = settings.value(QString("STOPDETECT%1").arg(i)).toStringList();
		if (values.size() > 0)
			detectWindow[i][1] = (values[0].toInt());


		values.clear();



	}
}


void DMS::ShowImage(int nrCam, QByteArray image)
{

	QByteArray newImageData = image;
	QPainter paint;


	QImage newImage(2250, 256, QImage::Format_RGB32);
	newImage.fill(Qt::white);
	QPainter painter(&newImage);

	QPen linePen;
	QPen dataPen;
	unsigned int pixValue = 0;
	QPainterPath path;
	char tmp = 0;
	unsigned int *dataArray;
	

	QByteArray hex_string = newImageData.toHex();
	path.moveTo(0, 125);
	for (int i = 0; i < newImageData.size(); i++)
	{
		pixValue =  static_cast<uint8_t>(newImageData[i]);
		path.lineTo(i, pixValue);
	}

	dataPen.setWidth(3);
	dataPen.setColor(Qt::blue);
	painter.setPen(dataPen);
	painter.drawPath(path);


	linePen.setWidth(7);
	linePen.setColor(Qt::red);
	
	painter.setPen(linePen);
	int threshold = 128;
	int startY = threshold - 28;
	int stopY = threshold + 28;

	painter.drawLine(QPoint(detectWindow[nrCam][0], threshold), QPoint(detectWindow[nrCam][1], threshold));
	painter.drawLine(QPoint(detectWindow[nrCam][0], startY), QPoint(detectWindow[nrCam][0], stopY));
	painter.drawLine(QPoint(detectWindow[nrCam][1], startY), QPoint(detectWindow[nrCam][1], stopY));
	//painter.drawLine(QPoint(50, 128), QPoint(2200, 128));


	painter.end();
	pixmapVideo[nrCam]->setPixmap(QPixmap::fromImage(newImage));
	if(nrCam == 0)
	ui.imageView0->fitInView(sceneVideo[0]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 1)
	ui.imageView1->fitInView(sceneVideo[1]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 2)
	ui.imageView2->fitInView(sceneVideo[2]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 3)
	ui.imageView3->fitInView(sceneVideo[3]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 4)
	ui.imageView4->fitInView(sceneVideo[4]->sceneRect(), Qt::IgnoreAspectRatio);
	else if (nrCam == 5)
	ui.imageView5->fitInView(sceneVideo[5]->sceneRect(), Qt::IgnoreAspectRatio);

	///pixmapVideo->setPixmap(p);

}

void DMS::ShowTransitions(int nrCam, QByteArray image)
{
	QByteArray tmpArray = image;
	unsigned int data;
	unsigned int transitionType;
	unsigned int transtiton;

	int nrTransPos = 0, nrTransNeg = 0;
	for (int i = 0; i < 5; i++)
	{
		transitionDataTest[nrCam][i][0] = -1;
		transitionDataTest[nrCam][i][1] = -1;
	}
	//parsamo podatke 

	for (int i = 2; i < tmpArray.size(); i+=2)
	{
		data = static_cast<uint8_t>(tmpArray[i])<<8;
		data = data + static_cast<uint8_t>(tmpArray[i + 1]);//zdruzim v 16 bitno stevilko
		if (i > 11)
		{
			int bla = 00;
		}
	//sedaj moram razcleniti kateri prehod in vrednost prehoda 
		transitionType = data >> 14 & 0x3;
		transtiton = data & 0x3FFF;
		if (transtiton == 5168)
		{
			int bla = 100;
		}
		if (transitionType == 2)
		{
			transitionDataTest[nrCam][nrTransPos][0] = transtiton;//tabela za zgornje in spodnje prehode pri testnem oknu za prehode
			nrTransPos++;
		}
		else if (transitionType == 1)
		{
			transitionDataTest[nrCam][nrTransNeg][1] = transtiton;
			nrTransNeg++;
		}
		else
		{
	
		}

	}

	for (int i = 0; i < 5; i++)
	{
		if (transitionDataTest[nrCam][i][0] > -1)
			transitionDataTestLabel[nrCam][i][0]->setText(QString("%1").arg(transitionDataTest[nrCam][i][0]));
		else
			transitionDataTestLabel[nrCam][i][0]->setText(QString(""));

		if (transitionDataTest[nrCam][i][1] > -1)
			transitionDataTestLabel[nrCam][i][1]->setText(QString("%1").arg(transitionDataTest[nrCam][i][1]));
		else
			transitionDataTestLabel[nrCam][i][1]->setText(QString(""));
	}


}

void DMS::ShowDowel(int nrDowel)
{
	QPainter painter(this);

	painter.drawRect(QRect(52, 132, 600, 600));



	//QPainter painter(this);

	//painter.drawRect(QRect(10, 10, 500, 500));

	/*QByteArray newImageData = image;



	QImage newImage(2200, 256, QImage::Format_RGB32);
	newImage.fill(Qt::white);

	int pixValue = 0;

	for (int i = 0; i < newImageData.size(); i++)
	{
		pixValue = (uchar)newImageData[i];
		//if (pixValue > 254)
		//	pixValue = 254;
		newImage.setPixel(QPoint(i, pixValue), RGB(255, 0, 0));
	}

	pixmapVideo[nrCam]->setPixmap(QPixmap::fromImage(newImage));
	ui.imageView0->fitInView(sceneVideo[0]->sceneRect(), Qt::IgnoreAspectRatio);
	ui.imageView1->fitInView(sceneVideo[1]->sceneRect(), Qt::IgnoreAspectRatio);
	//ui.imageView1->fitInView(sceneVideo[nrCam]->sceneRect(), Qt::IgnoreAspectRatio);
	//pixmapVideo->setPixmap(p);
	*/

}

void DMS::ReadDowelString()
{

	int lowData, highData;
	int value = 0;
	int nrCamera = 0;
	int y = 50;
	int currentDowel = -1;
	dowelPrehodi.clear();
	dowelPoints.clear();
	int lenght;
	int nrMeasurements;
	int speed;
	int yData = 0;
	currentDowel = (unsigned char)tmpDowelData[1];

	lowData = (unsigned char)tmpDowelData[2];
	highData = (unsigned char)tmpDowelData[3];
	dowelSpeed[currentDowel] = (highData << 8) + lowData;

	lowData = (unsigned char)tmpDowelData[4];
	highData = (unsigned char)tmpDowelData[5];
	dowelLenght[currentDowel] = (highData << 8) + lowData;

	lowData = (unsigned char)tmpDowelData[6];
	highData = (unsigned char)tmpDowelData[7];
	dowelNrMeasurements[currentDowel] = (highData << 8) + lowData;

	for (int i = 0; i < tmpDowelData.size(); i+=2)
	{
		if (i > 12)
		{
			lowData = (unsigned char)tmpDowelData[i - 2];
			highData = (unsigned char)tmpDowelData[i - 1];

			value = (highData << 8) + lowData;
			value = value - 500;

			if (value < 0)
				int bla = 100;
			dowelPrehodi.push_back(value);
			//if ((nrCamera == 0) || (nrCamera == 12))
			if (nrCamera % 5 == 0)
			{
				dowelPoints.push_back(QPoint(y, value));
				yData++;
			}
			nrCamera++;
			/*if (nrCamera == 12)
			{
				nrCamera = 0;
				y+=10;
			}*/

			if (yData == 2)
			{
				yData = 0;
				y += 1;
			}
		}
	}
	dowelRealPoints[currentDowel] = dowelPoints;
	dowelReady[currentDowel] = 1;
	update();


}

void DMS::OnClientConnected()
{

	dmsConnected = true;
	//tcpClient->setSocketOption(QAbstractSocket::TypeOfServiceOption, 32);
	//tcpClient->setSocketOption(QAbstractSocket::LowDelayOption, 1);
	InitDMS();

}
void DMS::OnClientDisconnected()
{
	dmsConnected = false;
	isInitialized = false;
	connectionTimer->start(waitTime);//poskus ponovne povezave 
	
}

void DMS::SpinGainChanged(int index, int gain)
{
	QString sendText;
	QByteArray sendArray;
	int test = index;
	//test = test + 4;
	
	sendText = QString("G%1-%2;").arg(test).arg(gain);

	sendArray = sendText.toLocal8Bit();
	this->gain[index] = gain;
	WriteOneCamSetting(index, "GAIN");
	

	if (sendArray.size() > 0)
		WriteDatagram(sendArray);

}

void DMS::SpinOffsetChanged(int index, int offset)
{
	QString sendText;
	QByteArray sendArray;
	int test = index;
	//test = test + 4;
	sendText = QString("O%1-%2;").arg(test).arg(offset);
	this->offset[index] = offset;
	WriteOneCamSetting(index, "OFFSET");
	sendArray = sendText.toLocal8Bit();


	if (sendArray.size() > 0)
		WriteDatagram(sendArray);
}
void DMS::SpinDetectWindowChanged(int index,int topBottom, int value)
{
	int bla = 100;

	QString sendText;
	QByteArray sendArray;
	int test = index;
	test = test + 4;
	//sendText = QString("O%1-%2;").arg(test).arg(value); //za nastavljanje registrov moram se pripravit

	QString hexvalueReg, hexvalue;
	int detectReg = 0;
	if(topBottom == 0)
	detectReg = 3 + (3 * index);
	else
	 detectReg = 4 + (3 * index);







	this->detectWindow[index][topBottom] = value;



	hexvalueReg = QString("%1").arg(detectReg, 0, 16);
	hexvalue = QString("%1").arg(detectWindow[index][topBottom], 0, 16);

	sendText = QString("R%1-%2;").arg(hexvalueReg).arg(hexvalue);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);


	if(topBottom == 0)
	WriteOneCamSetting(index, "STARTDETECT");
	else
		WriteOneCamSetting(index, "STOPDETECT");
	//sendArray = sendText.toLocal8Bit();


	/*if (sendArray.size() > 0)
		WriteDatagram(sendArray);*/


}
void DMS::SpinGainChangedTest(int gain)
{
	int bla = 100;
}

void DMS::ReadDowelFromFile(QString filePath)
{
	
	QString line;

	QFile file(filePath);
	if (file.open(QIODevice::ReadOnly))
	{
		QTextStream stream(&file);


		while (!stream.atEnd())
		{
			QString line = stream.readLine(1); //read one line at a time
			tmpDowelData.push_back(line.toUtf8());
		}

	
		file.close();
	}
}

void DMS::paintEvent(QPaintEvent *)
{

	QPainter painter(this);



	painter.setPen(QPen(Qt::green, 6, Qt::SolidLine, Qt::SquareCap));

	
	//painter.drawLine(100, 830, 100, 1100);
	

	QPolygon poly;

	for (int i = 2; i < dowelPoints.size(); i+=2)
	{
	poly << dowelPoints[i];



		//painter.drawLine(dowelPoints[i-2].x(), dowelPoints[i-1].y(), dowelPoints[i-2].x(), dowelPoints[i-2].y());

		//if(i == 20)
		//break;
	}
	/*poly << QPoint(0, 85) << QPoint(75, 75)
		<< QPoint(100, 10) << QPoint(125, 75)
		<< QPoint(200, 85) << QPoint(150, 125)
		<< QPoint(160, 190) << QPoint(100, 150)
		<< QPoint(40, 190) << QPoint(50, 125)*/
		/*<< QPoint(0, 85)*/;

	// QPen: style(), width(), brush(), capStyle() and joinStyle().
	QPen pen(Qt::red, 3, Qt::DashDotLine, Qt::RoundCap, Qt::RoundJoin);
	painter.setPen(pen);
	painter.drawPolygon(poly);

}



void DMS::StartClient()
{

	tcpClient->connectToHost(ipAdd, port);
	connectionTimer->start(waitTime);
}

void DMS::InitDMS()
{
	for (int i = 0; i < NR_DMS_CAM; i++)
		SetCameraSettings(i);

}

void DMS::SetCameraSettings(int nrCam)
{
	QString sendText;
	QByteArray sendArray;

	QString hexvalueReg;
	QString hexvalue;
	int test = nrCam;
	test = test + 4;


	
	int thresholdReg = 2+ (3* nrCam);
	int startDetectReg = 3+ (3 * nrCam);
	int stopDetectReg = 4+ (3 * nrCam);

	//GAIN
	sendText = QString("G%1-%2;").arg(nrCam).arg(gain[nrCam]);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);
	//OFFSET
	sendText = QString("O%1-%2;").arg(nrCam).arg(offset[nrCam]);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);
	////threshold
	hexvalueReg = QString("%1").arg(thresholdReg, 0, 16);
	hexvalue = QString("%1").arg(128, 0, 16);

	sendText = QString("R%1-%2;").arg(hexvalueReg).arg(hexvalue);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);
	////startArea
	hexvalueReg = QString("%1").arg(startDetectReg, 0, 16);
	hexvalue = QString("%1").arg(detectWindow[nrCam][0], 0, 16);

	sendText = QString("R%1-%2;").arg(hexvalueReg).arg(hexvalue);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);
	//stopArea
	hexvalueReg = QString("%1").arg(stopDetectReg, 0, 16);
	hexvalue = QString("%1").arg(detectWindow[nrCam][1], 0, 16);

	sendText = QString("R%1-%2;").arg(hexvalueReg).arg(hexvalue);
	sendArray = sendText.toLocal8Bit();
	WriteDatagram(sendArray);

	int bla = 100;
}

void DMS::OnViewTimer()
{

	ui.labelTakt->setText(QString("%1[Hz]").arg(timeCount));

}
void DMS::OnConnectionTimeout()
{

	/*if (connectionOnCounter != connectionOnPrevCounter)
	{
		isConnected = true;
	}
	else
	{
		isConnected = false;
		isInitialized = false;
	}


	if ((isConnected == true) && (isInitialized == false));
	{
		isInitialized = false;
		InitDMS();
		isInitialized = true;
	}
	connectionOnPrevCounter = connectionOnCounter;*/

	int state = -1;

		state = tcpClient->state();

		if (state == 2)//se se povezuje ponovno preverimo cez en timer
		{
			timout++;
			if (timout > 5)
			{
				tcpClient->waitForConnected(0);
				timout = 0;
			}
			connectionTimer->start(waitTime);
		}
		else if (state == 0)
		{
			tcpClient->disconnectFromHost();
			StartClient();
			connectionTimer->start(waitTime);
			timout = 0;
		}
		else if (state == 3)//connected to host
		{
			timout = 0;
		}



	


}

void DMS::ReadPendingDatagrams()
{
	 QByteArray recv;
	QByteArray recv2;
	int statusCode = 0;
	char message[4096];


	/*testTimer.SetStop();
	timeDelay11[connectionOnCounter] = testTimer.ElapsedTime();
	testTimer.SetStart();
	connectionOnCounter++;
	if (connectionOnCounter > 999)
		connectionOnCounter = 0;*/

	recv.clear();
	///recv = tcpClient->readAll();
	//tcpClient->waitForReadyRead(0);
	while ( tcpClient->bytesAvailable() > 0)
	{
		statusCode = tcpClient->bytesAvailable();
		//recv = QByteArray(message, statusCode);
		//if (udpClient->pendingDatagramSize() > 0)
		//if (statusCode > 0)
		//{

			/*if (dataReadyFreq.timeCounter >= 100)
			{
				dataReadyFreq.SetStop();

				timeCount = dataReadyFreq.CalcFrequency(dataReadyFreq.timeCounter);
				dataReadyFreq.timeCounter = 0;
				dataReadyFreq.SetStart();
			}
			dataReadyFreq.timeCounter++;*/
			//recv = tcpClient->read(1024);
			recv = tcpClient->readAll();
			//recv = QByteArray(message, statusCode);
			//connectionOnCounter++;
			//statusCode = udpClient->pendingDatagramSize();
			//statusCode = tcpClient->bytesAvailable();
			char t[3000];
			//udpClient->readDatagram(t,2000);
		   //recv = udpClient->readAll();
		   //recv = tcpClient->readAll();
		   //recv2 = udpClient->readAll();

			int j = 0;
			QByteArray tmp22;
			parseCommandCounter = 0;

			for (int i = 0; i < statusCode; i++)
			{

				parseBuffer[parseCounter] = recv[i];


				if ((parseBuffer[parseCounter - 2] == '\0') && (parseBuffer[parseCounter - 1] == '\r') && (parseBuffer[parseCounter] == '\n'))
				{
					tmp22.clear();
					for (j = 0; j < parseCounter; j++)
					{
						tmp22.append(parseBuffer[j]);
					}
					parseCommands[parseCommandCounter] = tmp22;
					parseCommandCounter++;
					parseCounter = 0;
					j = 0;
				}
				else
				{
					parseCounter++;
				}
			}


			for (int i = 0; i < parseCommandCounter; i++)
			{

				if (parseCommands[i][0] == 'D')//zaenkrat si shranim podatke v fajl
				{
					char tmp = parseCommands[i][0];

					int nrCam = tmp - '0';
					QString filename = "C:/data" + QString("/DOWEL_%1.txt").arg(nrCam);
					QString line;
					tmpDowelData = parseCommands[i];
					ReadDowelString();


				}
				if (parseCommands[i][0] == 'T')//zaenkrat si shranim podatke v fajl
				{
					char tmp = parseCommands[i][1];

					int nrCam = tmp - '0';
					//if(isTransitionDisplaySet == 1)
					if (nrCam == 1)
						int bla = 100;
					emit transitionReadySignal(nrCam, parseCommands[i]);
				}

				if (parseCommands[i][0] == 'L' && parseCommands[0].size() < 3000)
				{
					char tmp = parseCommands[i][1];

					int nrCam = tmp - '0';
					emit frameReadySignal(nrCam, parseCommands[i]);

				}
				parseCommands[i].clear();
				//parseCommandCounter = 0;
			}

		//}
	}
	//} while (tcpClient->bytesAvailable() > 0);
}
void DMS::OnSend()
{
	{
		QString sendText;
		QByteArray sendArray;

		sendText = ui.lineSend->text();

		sendArray = sendText.toLocal8Bit();

		ui.lineSend->clear();
		if (sendArray.size() > 0)
			WriteDatagram(sendArray);
	}
}
void DMS::WriteDatagram(QByteArray data)
{

	QByteArray sendData = data;
	QByteArray newText = data;

	QByteArrayMatcher carrageReturn;
	carrageReturn.setPattern("\r\n");


	if (carrageReturn.indexIn(sendData, 0) == -1)
	{
		sendData.append("\r\n");
	}
	lock.lockForWrite();
	//udpClient->writeDatagram(sendData, hostIp, port);
	tcpClient->write(sendData);
	tcpClient->flush();
	lock.unlock();
}




