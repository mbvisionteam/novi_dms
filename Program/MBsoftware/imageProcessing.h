﻿#pragma once

#include <QWidget>
#include "ui_imageProcessing.h"
#include "Camera.h"
#include "CCustomGraphicsItem.h"
#include "Timer.h"
#include "Measurand.h"
#include "qtpropertymanager.h"
#include "qtvariantproperty.h"
#include "qttreepropertybrowser.h"
#include "Types.h"
#include "DelayedDraw.h"
#include "ui_DMSPresortDialog.h"
#include "MBlabel.h"
#include "AdvancedSettings.h"

using namespace std;
using namespace cv;


class imageProcessing : public QWidget
{
	Q_OBJECT

public:
	imageProcessing(QString referencePath);
	
	//imageProcessing();
	~imageProcessing();


	static std::vector<CCamera*>									cam;
	static std::vector<CMemoryBuffer*>								images;
	static std::vector<Timer*>										processingTimer;
	static Measurand*												measureObject;
	static std::vector<Types*>										types[NR_STATIONS];
	static CMemoryBuffer*											currImage;
	static Mat														DrawImage;
	static std::vector<AdvancedSettings>							advSettingsPresort;
	
	QWidget	* presortDialog;
	float zoomFactor;
	float zoomMultiplier;
	int displayWindow;
	int displayImage;
	int prevDisplayWindow;
	int prevDisplayImage;
	int rowNum;
	int colNum;
	int currentStation;
	bool isSceneAdded;
	int loginRights;
	QString referencePath;

	QRubberBand*  rubberBand;
	QPoint origin;
	QPoint pos;
	QPoint pos2;
	QSizeGrip* gripTopLeft;
	QSizeGrip* gripBottomRight;
	int start;
	int createRubber;
	int creatingRubber;
	int changeRubberBand;
	int rubberGripperSelected;
	vector <QRect> rubberRect;
	vector< QGraphicsRectItem*> RectItem;
	QRect rubberRectOld[2];
	vector <int> rubberRectFunction;
	int selectedRubberRect;
	int selectedFunction;
	int drawRubberFunction;
	int tmpDynamicParameter[30];
	int tmpDynamicParameterold[30];
	int selectedDynamicParameter;
	QTimer* timer; //zacasno za prepoznavo , ce se je propertyBrowser dinamicnih funkcij  spremenil
	QRect dynamicFunctionRect1;
	QRect dynamicFunctionRect2;

	int presortingCameraCounter;
	int presortingCameraCounterBad;
	int presortingIndex;
	int positionForSpeed[2];
	int positionCounter;
	float speedLastDowel;

	QGraphicsPixmapItem*	pixmapVideo;

	void ConnectImages(std::vector<CCamera*> inputCam, std::vector <CMemoryBuffer*> inputImages);
	void ConnectMeasurand(Measurand* objects);
	void ConnectTypes(int station,std::vector<Types*> type);
	void ConnectImages(std::vector<CCamera*> inputCam);
	void ShowDialog(int rights);

	void ClearDialog();
	void ReplaceCurrentBuffer();

	QGraphicsScene*	scene;
	QGraphicsScene*	sceneForMainWindow;

	//qpropertyBrowser za lastnosti tipov in funkcij
	QtVariantPropertyManager *variantManager; //dodajamo QtProperty in QtVariantProperty
	QtVariantEditorFactory *variantFactory; //dodat v editor
	QtTreePropertyBrowser *variantEditor; //widget za prikaz GPropertyBrowser
	QtProperty *topItem;				//top item
	vector<QtVariantProperty*> item;
	int currentFunction;
	int currentType;
	void PopulatePropertyBrowser(int function);			//prepise tabele v qpropertyBrowser
	void PopulatePropertyBrowserDynamicFunctions(int function);			//prepise tabele s funkcijami za univerzalne obdelave
	void UpdatePropertyBrwserDynamicFunctions(int funcion);
	void ReadPropertyBrowserDynamicFunctions(int function);
	void SaveFunctionParameters(int typeNr, int function);
	void SaveFunctionParametersOnNewType(int typeNr, int function);
	//shrani v datoteke parametre funkcije kadar se spreminja oz dodaja parametri
	void DeleteTypeProperty(QString type);
	void LoadTypesProperty(int nrType);

	CDelayedDraw mainScreenDrawTekoci[4];
	CDelayedDraw mainScreenDrawFinal[4];

	void NastaviJezikImgProc();

protected:
	bool eventFilter(QObject *obj, QEvent *event);
	void keyPressEvent(QKeyEvent * event);

	void propertyChanged(QtProperty *property);
	void closeEvent(QCloseEvent * event);


private:
	Ui::imageProcessing ui;
	Ui::DMSpresortDialog ui2;
	


	bool slovensko;


public slots:
	void OnPressedImageUp();
	void OnPressedImageDown();
	void OnPressedCamUp();
	void OnPressedCamDown();
	int ConvertImageForDisplay(int imageNumber);
	void ShowImages();
	void ResizeDisplayRect();
	void OnProcessCam0(); //za klicanje obdelave iz gumba
	void OnProcessCam1();
	void OnProcessCam2();
	void OnProcessCam3();
	void OnProcessCam4();
	void OnDoneEditingLineInsertImageIndex();
	void onPressedAddParameter();
	void onPressedRemoveParameters();
	void onPressedUpdateParameters();
	void onPressedSurfaceInspection();
	void OnPressedCreateRubber();
	void OnPressedResizeRubber();
	void OnPressedSelectRubber();
	
	//testFunctions
	void OnPressedTestIntensityFunction();


	//funkcije za obdelave
	void OnViewTimer();

	int ProcessingCamera0(int id, int imageIndex,int draw); //obdelava orientacije
	int ProcessingCamera1(int id, int imageIndex, int draw);// obdelava kamera 1
	void GetPresortParameters(int draw);
	int ProcessingCamera2(int id, int imageIndex, int draw);// obdelava kamera 2
	int ProcessingCamera3(int id, int imageIndex, int draw);// obdelava kamera karakteristika
	int ProcessingCamera4(int id, int imageIndex, int draw);// obdelava kamera karakteristika

	int ProcessingCamera3(CMemoryBuffer* image, int draw);
	int ProcessingCamera1(CMemoryBuffer* image, int draw);// obdelava kamera 1
	int CheckDefects(int indexImage, int nrPiece, int draw);

	//
	int ProcessCameraForBlobs(int cam, int index, int draw);


	//Razvrščevalnik
	void OnSetClassifierTrainImagesPath();
	void OnSetClassifierTestImagesPath();

	//Obdelava površine


	void ClearDraw();
	void ClearFunctionList();
	void SetCurrentBuffer(int dispWindow, int dispImage);
	void ZoomOut();
	void ZoomIn();
	void ZoomReset();
	void OnLoadImage();
	void OnLoadMultipleImage();
	void OnSaveImage();

signals:
	void imagesReady();
	void measurePieceSignal(int nrCam, int imageIndex);//signal za v razred MBVISION OnFrameReady


//testno risanje 
	public:

	Mat							drawImageMain[2];

	///presortiranje diloa FMS
	QGraphicsScene*	scenePresortDialog;
	QGraphicsPixmapItem*	pixmapPresortDialog;
	QGraphicsTextItem* mouseText;
	QGraphicsItem* savedItem;
	MBlabel *labelHistoryImage[20];
	QLineEdit *AdvToleranceLineEdit[3][10];//center top bottom
	QLabel *presortMeasurementsResult[3][10];//center top bottom
	int tmpAdvancedSettins[3][10];//tmp settings predn se shranijo nastavitve
	int currPiecePresort;
	int selectedImage;
	int lastInHistory;
	int presortCalibrationIndex;
	int presortCalibrationIndexTmp;
	QString basicPresortSettingsPath;
	QString advSettingsPresortPath;
	QString calibrationSettingsPresortPath;
	QString selectedPresortAdvancedSettings;
	int selectedPresortAdvancedSettingsIndex; 
	int selectedPresortAdvancedSettingsIndexTmp;
	int isLivePresort;
	Mat drawImagePresortAll;
	Mat drawImagePresortBad;
	int updateImagePresortAll;
	int updateImagePresortBad;



	CMemoryBuffer *presortImage;
	public slots:
		void ShowDialogPresort(int rights,int currPiece, int presortCalibration);
		void UpdateHistoryWindow();
		void CreateScenePresortDialog();

		void ShowImagePresort(int index);
		void ClearDrawPresort();
		void ReadBasicParametersPresort();
		void WriteBasicParametersPresort();
		void UpdateBasicSettings();
		void OnClickedSaveBasicSettings();
		void ReadAdvancedSettingsPresort();
		void WriteAdvancedSettingsPresort(int index);
		void SelectAdvancedPresortSettings();


		void OnClickedHistoryLabel(int index);
		void OnClickedMeasurePresortDowel();
		void OnClickedPresortZoomIn();
		void OnClickedPresortZoomOut();
		void OnClickedPresortZoomReset();
		void OnClickedSelectedCalibrationPresort();
		void OnClickedSelectedAdvSettingsPresrot();
		void ReadCalibrationSettingsPresort();
		void WriteCalibrationSettingsPresort(int index);
		void OnClickedRadioButtonLive();
		void OnClickedClose();
		void OnClickedCreateNewPresortSettings();
		void CopyAdvancedSetting(QString existed, QString newName);
		void DeleteAdvancedSettings(QString selected);
		void OnClickedDeletePresortSettings();
		

	//public signals:





};
