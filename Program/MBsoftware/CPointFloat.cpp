// PointFloat.cpp : implementation file
//

#include "stdafx.h"

#include "CPointFloat.h"



// CPointFloat
//************************************ UPORABA***************************************
/*

	CPointFloat p(); //vse tocke so 0
	CPointFloat p1(5, 5); //x=5, y=5
	CPointFloat p2(0,0, 5, 1.5); //tocka podana z bazno tocko (0,0), razraljo 5 in kotom 1.5 radianov

	
	//razdalje
	float GetDistance(const CPointFloat & p) const ;  //razdalja med this in podano tocko
	float GetDistance(float pX, float pY);  //razdalja med this in podano tocko v koordinatah
	float GetDistance();   //razdalja od izhodisca


	//polarne koordinate - vrne polarne koordinate, ce imamo tocke podane s koti
	CPointFloat PolarCoordinatesRadian( float theta, float dis );
	CPointFloat PolarCoordinatesRadian();
	CPointFloat PolarCoordinatesDegree( float theta, float dis );

	//operatorji
	CPointFloat operator=(CPointFloat point);	 //priredi tocko
	bool operator ==(CPointFloat point);		//primerja ce sta tocki enaki
	bool operator !=(CPointFloat point);		//primerja ce sta razlicni
	void operator +=(CPointFloat point);		// this tocki pristeje vsako komponento posebaj
	void operator -=(CPointFloat point);		//this tocki odsteje vsako komponento posebaj
	CPointFloat operator +(CPointFloat point);	//sesteje obe tocki
	CPointFloat operator -(CPointFloat point);	//odsteje obe tocki,
	CPointFloat operator +(CPointFloat point);
	CPointFloat operator -(CPointFloat point);
	CPointFloat operator *(float k);
	CPointFloat operator /(float k);
	void operator *=(float k);
	void operator /=(float k);

	//functions
	float GetPointAngleInDegrees();			//vrne	kot tocke v stopinjah bazna tocka je izhodisce
	float GetPointAngleInRadians();			//vrne	kot tocke v radianih bazna tocka je izhodisce
	CPointFloat Normalize();				//normalizira - dolzina je od izhodisca (x /= distance; y /= distance;)
	CPointFloat Normalize( float size );	// x = x*size/dis; y = y*size/dis;
	void SetActive(bool value);				//nastavi zastavico isActive na value
	bool IsActive();						//vrne vrednost zastavice isActive
	void GetDelta(CPointFloat point);		//dx = x - point.x; dy = y - point.y; dx in dy sta spremenljivki classa in sta pulic
	float GetDeltaX(CPointFloat point);		//izravuna in vrne dx
	float GetDeltaY(CPointFloat point);		//izravuna in vrne dy
	bool IsInside(CRect r);					//preveri ce je this tocka v kvadratu
	bool IsXInside(CRect r);				//preveri ce je this.x  v kvadratu
	bool IsYInside(CRect r);				//preveri ce je this.y  v kvadratu
	void RotatePoint(CPointFloat basePoint, double alfaRadians);			//this tocko rotira okoli bazne tocke za alpha
	void RotatePoint(CPointFloat basePoint, int alfaDegrees);
	CPointFloat GetRotatedPoint(CPointFloat basePoint, double alfaRadians);	//vrne tocko, ki je na this tocko rotirana okoli basepoint za alpha
	CPointFloat GetRotatedPoint(CPointFloat basePoint, int alfaDegrees);
	CPointFloat GetPointAtDistanceAndAlpha(double distance, double alfaRadians);	//vrne tocko, ki je od this tocke rotirana za alpha na razdalji distance
	CPointFloat GetPointAtDistanceAndAlpha(double distance, int alfaDegrees);
	double DegreesToRadians(double alfaDegree);		//pretvorba stopinj v radiane
	double RadiansToDegree(double alfaRadians);		//pretvorba radianov v stopinje

	Clear() postavi vse vrednosti na 0
*/

//IMPLEMENT_DYNAMIC(CPointFloat)

CPointFloat::CPointFloat()
{
	Clear();
}


CPointFloat::CPointFloat(float _x, float _y)
{
	Clear();
	x = _x;
	y = _y;
}

CPointFloat::CPointFloat(float baseX, float baseY, double distance, double alphaRadians)
{
	//tocka, ki je iz izhodisca (basex, basey) oddaljena distance in je pod kotom alphaRadians na izhodisce

	//x = sin(alphaRadians) / distance;
	//y = cos(alphaRadians) / distance;
	this->basex = baseX;
	this->basey = baseY;
	this->distance = distance;
	this->alphaRadians = alphaRadians;
}

CPointFloat::CPointFloat(const CPointFloat& point)
{
	x = point.x;
	y = point.y;
	dx = point.dx;
	dy = point.dy;
	active = point.active;
	offsetX = point.offsetX;
	offsetY = point.offsetY;
	basex = point.basex;
	basey = point.basey;
	distance = point.distance;
	alphaRadians = point.alphaRadians;
} // Copy constructor



CPointFloat::~CPointFloat()
{
}
//BEGIN_MESSAGE_MAP(CPointFloat)
//END_MESSAGE_MAP()


// CPointFloat message handlers

void CPointFloat::SetPointFloat(float x, float y)
{
	this->x = x;
	this->y = y;
}


void CPointFloat::SetPointFloat(QPoint point)
{
	this->x = point.x();
	this->y = point.y();
}

void CPointFloat::SetPointFloat(float baseX, float baseY, double distance, double alphaRadians)
{
	//tocka, ki je iz izhodisca (basex, basey) oddaljena distance in je pod kotom alphaRadians na izhodisce

	//x = sin(alphaRadians) / distance;
	//y = cos(alphaRadians) / distance;
	this->basex = baseX;
	this->basey = baseY;
	this->distance = distance;
	this->alphaRadians = alphaRadians;
}

void CPointFloat::SetPointFloat(float baseX, float baseY, double distance, int alphaDegrees)
{
	double alfa;

	alfa = DegreesToRadians(alphaDegrees);
	SetPointFloat(distance, alfa);
}


//distance between point
float CPointFloat::GetDistance(const CPointFloat & p) const
{
	float vmx = 0, vmy = 0;


	if ((x > -HUGE_VAL) && (y > -HUGE_VAL) && (x < HUGE_VAL) && (y < HUGE_VAL)
		&& (p.x > -HUGE_VAL) && (p.y > -HUGE_VAL) && (p.x < HUGE_VAL) && (p.y < HUGE_VAL))
	{
		vmx = (x - p.x);
		vmy = (y - p.y);
		return sqrt(vmx*vmx + vmy * vmy);
	}

	return -1;
}

float CPointFloat::GetDistance(float pX, float pY)  //namesto RazdTT
{
	CPointFloat p1;

	p1.SetPointFloat(pX, pY);

	return GetDistance(p1);
}

float CPointFloat::GetDistance()
{
	return sqrt(x*x + y * y);
}

//izracuna polarne koordinate iz alphe in distance od tocke
CPointFloat CPointFloat::PolarCoordinatesRadian(float theta, float dis)
{
	x = dis * cos(theta);
	y = dis * sin(theta);
	alphaRadians = theta;
	distance = dis;
	basex = 0;
	basey = 0;

	return *this;
}
//izracuna polarne koordinate iz alphe in distance od tocke
CPointFloat CPointFloat::PolarCoordinatesRadian()
{
	x = distance * cos(alphaRadians) + basex;
	y = distance * sin(alphaRadians) + basey;

	return *this;
}

CPointFloat CPointFloat::PolarCoordinatesDegree(float theta, float dis)
{
	x = dis * cos(theta*M_PI / 180);
	y = dis * sin(theta*M_PI / 180);

	return *this;
}

CPointFloat CPointFloat::operator=(CPointFloat point)
{
	x = point.x;
	y = point.y;
	dx = point.dx;
	dy = point.dy;
	active = point.active;
	offsetX = point.offsetX;
	offsetY = point.offsetY;
	return *this;
}
bool CPointFloat::operator ==(CPointFloat point)
{
	if (x == point.x && y == point.y)
		return true;

	return false;
}

bool CPointFloat::operator !=(CPointFloat point)
{
	if (x != point.x || y != point.y)
		return true;

	return false;
}

void CPointFloat::operator +=(CPointFloat point)
{
	x += point.x;
	y += point.y;
}

void CPointFloat::operator -=(CPointFloat point)
{
	x -= point.x;
	y -= point.y;
}

CPointFloat CPointFloat::operator +(CPointFloat point)
{
	return CPointFloat(x + point.x, y + point.y);
}

CPointFloat CPointFloat::operator -(CPointFloat point)
{
	return CPointFloat(x - point.x, y - point.y);
}



CPointFloat CPointFloat::operator *(float k)
{
	return CPointFloat(x*k, y*k);
}

CPointFloat CPointFloat::operator /(float k)
{
	return CPointFloat(x / k, y / k);
}

void CPointFloat::operator *=(float k)
{
	x *= k;
	y *= k;
}

void CPointFloat::operator /=(float k)
{
	x /= k;
	y /= k;
}
//vrne kot od izhodisca 0,0 v stopinjah
float CPointFloat::GetPointAngleInDegrees()
{
	float angle = atan2(y, x) * 180.0 / M_PI;
	while (angle < -180) angle += 360;
	while (angle > 180) angle -= 360;

	return  angle;
}
//vrne kot od izhodisca 0,0 v stopinjah
float CPointFloat::GetPointAngleInRadians()
{
	float angle = atan2(y, x);
	while (angle < -M_PI) angle += (float)(M_PI * 2);
	while (angle > M_PI) angle -= (float)(M_PI * 2);

	return  angle;
}

CPointFloat CPointFloat::Normalize()
{
	float dis = GetDistance();

	if (dis != 0)
	{
		x /= dis;
		y /= dis;
	}

	return *this;
}

CPointFloat CPointFloat::Normalize(float size)
{
	float dis = GetDistance();

	if (dis != 0)
	{
		x = x * size / dis;
		y = y * size / dis;
	}

	return *this;
}

void CPointFloat::SetActive(bool value)
{
	active = value;
}

bool CPointFloat::IsActive()
{
	return active;
}

void CPointFloat::GetDelta(CPointFloat point)
{
	dx = x - point.x;
	dy = y - point.y;
}

float CPointFloat::GetDeltaX(CPointFloat point)
{
	dx = x - point.x;
	return dx;
}

float CPointFloat::GetDeltaY(CPointFloat point)
{
	dy = y - point.y;
	return dy;
}

/*bool CPointFloat::IsInside(CRect r)
{
	if (x > r.left)
		if (x <= r.right)
			if (y > r.top)
				if (y <= r.bottom)
					return true;

	return false;
}

bool CPointFloat::IsXInside(CRect r)
{
	if (x > r.left)
		if (x <= r.right)
			return true;

	return false;
}

bool CPointFloat::IsYInside(CRect r)
{
	if (y > r.top)
		if (y <= r.bottom)
			return true;

	return false;
}
**/
//tocko rotira za kot alfa iz bazne tocke basepoint
void CPointFloat::RotatePoint(CPointFloat basePoint, double alphaRadians)
{
	float x1, y1;
	float xTmp, yTmp;

	xTmp = x;
	xTmp = y;

	//translacija v izhodisce
	x1 = x - basePoint.x;
	y1 = y - basePoint.y;

	//rotacija
	x = x1 * cos(alphaRadians) - y1 * sin(alphaRadians);
	y = x1 * sin(alphaRadians) + y1 * cos(alphaRadians);

	//translacija
	x += basePoint.x;
	y += basePoint.y;

}

void CPointFloat::RotatePoint(CPointFloat basePoint, int alphaDegrees)
{
	double alfa;
	alfa = DegreesToRadians(alphaDegrees);

	RotatePoint(basePoint, alfa);
}

//vrne tocko, ki je na this tocko rotirana za alfa iz izhodisca base point
CPointFloat CPointFloat::GetRotatedPoint(CPointFloat basePoint, double alphaRadians)
{
	float x1, y1;
	CPointFloat newPoint;

	//translacija v izhodisce
	x1 = x - basePoint.x;
	y1 = y - basePoint.y;

	//rotacija
	newPoint.x = x1 * cos(alphaRadians) - y1 * sin(alphaRadians);
	newPoint.y = x1 * sin(alphaRadians) + y1 * cos(alphaRadians);

	//translacija
	newPoint.x += basePoint.x;
	newPoint.y += basePoint.y;

	return newPoint;
}

CPointFloat CPointFloat::GetRotatedPoint(CPointFloat basePoint, int alphaDegrees)
{
	double alfa;
	alfa = DegreesToRadians(alphaDegrees);

	return GetRotatedPoint(basePoint, alfa);
}

//vrne tocko, ki je od this tocke oddaljena za distance in je pod kotom alfa 
CPointFloat CPointFloat::GetPointAtDistanceAndAlpha(double distance, double alphaRadians)
{
	float x1, y1;

	if (abs(alphaRadians == M_PI / 2)) //270 ali 90 stopinj - navpicnica
	{
		x1 = x;
		y1 = y + distance;

	}
	else if ((alphaRadians == 0) || (alphaRadians == M_PI)) //0 ali 180 stopinj - vodoravna
	{
		x1 = x + distance;
		y1 = y;
	}
	else
	{
		x1 = (cos(alphaRadians) * distance) + x;
		y1 = (sin(alphaRadians) * distance) + y;
	}


	return CPointFloat(x1, y1);
}

CPointFloat CPointFloat::GetPointAtDistanceAndAlpha(double distance, int alphaDegrees)
{
	double alfa;
	alfa = DegreesToRadians(alphaDegrees);

	return GetPointAtDistanceAndAlpha(distance, alfa);
}

void CPointFloat::SetPointAtDistanceAndAlpha(double distance, double alphaRadians)
{
	CPointFloat point;
	point = GetPointAtDistanceAndAlpha(distance, alphaRadians);
	x = point.x;
	y = point.y;
}
void CPointFloat::SetPointAtDistanceAndAlpha(double distance, int alphaDegrees)
{
	double alfa;
	alfa = DegreesToRadians(alphaDegrees);
	SetPointAtDistanceAndAlpha(distance, alfa);
}

CPointFloat CPointFloat::GetMiddlePoint(CPointFloat point)
{
	CPointFloat middlePoint;

	middlePoint.x = (x + point.x) / 2;
	middlePoint.y = (y + point.y) / 2;

	return middlePoint;
}


double CPointFloat::DegreesToRadians(double alfaDegree)
{
	return alfaDegree * M_PI / 180;
}

double CPointFloat::RadiansToDegree(double alphaRadians)
{
	return alphaRadians * 180 / M_PI;
}

void CPointFloat::Clear()
{
	x = 0;
	y = 0;
	dx = 0;
	dy = 0;
	offsetX = 0;
	offsetY = 0;
	active = 0;
	basex = 0;
	basey = 0;
	distance = 0;
	alphaRadians = 0;
}

QGraphicsEllipseItem* CPointFloat::DrawDot(QPen pen, QBrush brush,int size)
{	
	
	

	QRect rect(x, y, size, size);
	rect.moveCenter(QPoint(x, y));
	QGraphicsEllipseItem* elipse = new QGraphicsEllipseItem(rect);
	elipse->setActive(true);
	elipse->setPen(pen);
	elipse->setBrush(brush);
	elipse->setZValue(1);
	
	


	return elipse;
}

/*QGraphicsEllipseItem CPointFloat::DrawDot()
{
	QPainter painter;

	QGraphicsEllipseItem* circle = new QGraphicsEllipseItem(20, 20, 20, 20);
	circle->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
	circle->setBrush(Qt::green);

	const int WIDTH = 10;
	const int LENGTH = 20;
	auto line = new QGraphicsRectItem(45, 0, WIDTH, LENGTH);
	line->setParentItem(circle);
	line->setBrush(Qt::black);



	return QGraphicsEllipseItem();
}*/
