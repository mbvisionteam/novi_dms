#pragma once

#include <map>
#include <string>

enum ErrorCode {

	MB_NO_ERROR = 0,
	MB_NO_IMAGE = -1,
	MB_IMG_PROC_ERROR = -2,
	MB_DEFAULT_ERROR = -3
};

class MBError
{
public:
	MBError() :_code(ErrorCode::MB_DEFAULT_ERROR){}
	MBError(ErrorCode code) : _code(code) {}
	MBError(int code) : _code(static_cast<ErrorCode>(code)) {}

	void operator = (ErrorCode code);
	void operator = (int code);
	bool operator == (ErrorCode code);
	bool operator == (int code);
	bool operator != (ErrorCode code);
	bool operator != (int code);

	//Preveri ali gre za napako ali ne
	bool IsError();
	//Vrne znakovni niz, ki opisuje napako
	const char* String();

private:
	ErrorCode _code;
};

