#include "stdafx.h"
#include "XilinxZynq.h"





XilinxZynq::XilinxZynq(QString ipAddress, int port)
{
	ui.setupUi(this);

	isInitialized = false;
//	isInputP3Low = false;
//	isInputP3High = false;
//	isInputP8 = false;
	dataBufCounter = 0;
	udpClient = new QUdpSocket();
	isOutputP8 = false;
	isOutputP3 = false;
	connectionOnCounter = 0;
	connectionOnPrevCounter = 0;
	isConnectedOld = 0;
	isConnected = 0;
	currVersion = 0;
	hostIp.setAddress(ipAddress);
	this->port = port;

	waitTime = 1000; // po pribli�no 6ih sekundah  client neha po�iljati pro�njo za povezavo 
	connectionTimer = new QTimer();
	
	//connectionTimer->setInterval(5000);

	viewTimer = new QTimer();
	connect(viewTimer, SIGNAL(timeout()), this, SLOT(OnViewTimer()));



	connect(this, SIGNAL(bytesAvailable()), this, SLOT(readPendingDatagrams()));
	connect(udpClient, SIGNAL(connected()), this, SLOT(OnClientConnected()));
	connect(connectionTimer, SIGNAL(timeout()), this, SLOT(OnConnectionTimeout()));
	connectionTimer->start(500); 
	connect(ui.buttonSend, SIGNAL(pressed()), this, SLOT(OnSend()));
	connect(ui.lineSend, SIGNAL(returnPressed()), this, SLOT(OnSend()));

	StartClient();
	CreateDisplay();
	InitAddresses();

	for (int i = 0; i < NR_REG; i++)
	{
		writeRegisters[i] = 0x0000;
	}


	//zacasno sda odstranjena taba, ki jih ne potrebujem P8 in P9
	ui.tabWidget->removeTab(4);
	ui.tabWidget->removeTab(3);
	//ui.tabP8->hide();
}

void XilinxZynq::OnShowDialog()
{
	show();
	viewTimer->start(100);
}


XilinxZynq::~XilinxZynq()
{

}
void XilinxZynq::closeEvent(QCloseEvent *event)
{
	viewTimer->stop();
}

void XilinxZynq::StartClient()
{
	udpClient->bind(port);
	udpClient->connectToHost(hostIp, port, QIODevice::ReadWrite);



	//SendHandShake();
	//SendSetTransmitSpeed(fps);
}

void XilinxZynq::OnNewConnection()
{
}

void XilinxZynq::OnConnectionTimeout()
{


	if (isConnected == false)
	{
		isInitialized = false;
		SendHandShake();
		SendSetTransmitSpeed(fps);
	}


	if (connectionOnCounter != connectionOnPrevCounter)
	{
		isConnected = true;
	}
	else
	{
		currVersion = 0;
		isConnected = false;
	}


	connectionOnPrevCounter = connectionOnCounter;
}

void XilinxZynq::OnViewTimer()
{
	QString hexValue;
	for (int i = 0; i < NR_REG; i++)
	{
		hexValue = QString::number(readRegisters[i], 16);
		readRegValueLabel[i]->setText(QString("0x%1").arg(hexValue));

		hexValue = QString::number(writeRegisters[i], 16);
		writeRegValueLabel[i]->setText(QString("0x%1").arg(hexValue));

	}
	ui.labelReadSpeed->setText(QString("%1 [Hz]").arg(dataReadyFreq.frequency));

	if (isConnected == true)
	{
		if (isInitialized == true)
		{
			ui.labelConnected->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
			ui.labelConnected->setText("Connected!");
		}
		else
		{
			ui.labelConnected->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
			ui.labelConnected->setText("Not initialized!");
		}		
	}
	else 
	{
		ui.labelConnected->setStyleSheet(QStringLiteral("background-color: rgb(255, 0, 0);"));
		ui.labelConnected->setText("NOT OK!");
	}
	ui.labelcurrVersion->setText(QString("V: %1").arg(currVersion));


	//dataReadyFreq

	if (ui.outputP2Tab->isVisible() == true)
	{
		for (int i = 0; i < 16; i++)
		{
			if (output[0][i].value != 0)
			{
				checkBoxOutput[0][i]->setCheckState(Qt::Checked);
			}
			else
			{
				checkBoxOutput[0][i]->setCheckState(Qt::Unchecked);
			}
		}
	}
	else if (ui.inputP1Tab->isVisible() == true)
	{
		for (int i = 0; i < 16; i++)
		{
			if (input[0][i].value != 0)
			{
				checkBoxInput[0][i]->setCheckState(Qt::Checked);
			}
			else
			{
				checkBoxInput[0][i]->setCheckState(Qt::Unchecked);
			}
		}
	}
	else if (ui.tabP8->isVisible() == true)
	{
		for (int i = 0; i < 16; i++)
		{
			if (isOutputP8 == 1)
			{
				if (output[2][i].value != 0)
				{
					checkBoxIOP8[i]->setCheckState(Qt::Checked);
				}
				else
				{
					checkBoxIOP8[i]->setCheckState(Qt::Unchecked);
				}
			}
			else
			{
				if (input[2][i].value != 0)
				{
					checkBoxIOP8[i]->setCheckState(Qt::Checked);
				}
				else
				{
					checkBoxIOP8[i]->setCheckState(Qt::Unchecked);
				}
			}
		}
	}
	else if (ui.tabP3->isVisible() == true)
	{
		for (int i = 0; i < 16; i++)
		{
			if (isOutputP3 == 1)
			{
				if (output[1][i].value != 0)
				{
					checkBoxIOP3[1]->setCheckState(Qt::Checked);
				}
				else
				{
					checkBoxIOP3[1]->setCheckState(Qt::Unchecked);
				}
			}
			else
			{
				if (input[1][i].value != 0)
				{
					checkBoxIOP3[i]->setCheckState(Qt::Checked);
				}
				else
				{
					checkBoxIOP3[i]->setCheckState(Qt::Unchecked);
				}
			}
		}
	}
	else if (ui.tabP9->isVisible() == true)
	{
		for (int i = 0; i < 8; i++)
		{
			if (input[3][i].value != 0)
			{
				checkBoxInput[3][i]->setCheckState(Qt::Checked);
			}
			else
			{
				checkBoxInput[3][i]->setCheckState(Qt::Unchecked);
			}
		}
	}

	for (int i = 0; i < 8; i++)
	{
		if(luci[i].value == 1)
			buttonLight[i]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
		else
			buttonLight[i]->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));


		triggerCounter[i]->setText(QString("C%1: %2").arg(i).arg(readRegisters[23 + i]));
	}


}

void XilinxZynq::OnSend()
{
	{
		QString sendText;
		QByteArray sendArray;

		sendText = ui.lineSend->text();

		sendArray = sendText.toLocal8Bit();

		ui.lineSend->clear();
		if (sendArray.size() > 0)
			WriteDatagram(sendArray);
	}
}

void XilinxZynq::OnClickedOutput(int index,int bit)
{
	bool state;
	if(index == 2)
		state = checkBoxIOP8[bit]->checkState();
	if(index == 1)
		state = checkBoxIOP3[bit]->checkState();
	else
	state = checkBoxOutput[index][bit]->checkState();


	SetOutputValue(index, bit, state);
	
}

void XilinxZynq::OnClickedButtonLight(int index)
{
	if(luci[index].value == 1)
	RocnoLuci(index, 0);
	else
		RocnoLuci(index, 1);
}

void XilinxZynq::OnClickedButtonCam(int index)
{
	RocnoProzenjeKamere(index);
}



void XilinxZynq::SendHandShake()
{
	WriteDatagram("2643");
}

void XilinxZynq::SendSetTransmitSpeed(int speed)
{
	QString tmp;
	QByteArray send;
	tmp = QString("H%1;").arg(speed);

	send = tmp.toUtf8();
	WriteDatagram(send);
}

void XilinxZynq::SendWD()
{

}

int XilinxZynq::WriteRegister(int regNum)
{
	QString tmp; 
	tmp = QString("R%1-%2;").arg(regNum, 0, 16).arg(writeRegisters[regNum], 0, 16);
	WriteDatagram(tmp.toLocal8Bit());
	return 0;
}

void XilinxZynq::InitAddresses()
{
		int  bitAddress = 0x01;

		for (int j = 0; j < 4; j++)
		{
			bitAddress = 0x01;
			for (int i = 0; i < 16; i++)
			{
				if (j < 3)
					output[j][i].address = bitAddress;

				input[j][i].address = bitAddress;

				bitAddress = bitAddress << 1;
			}
		}
}

void XilinxZynq::SetInputSignals(int reg,int  index)
{
	for (int i = 0; i < 16; ++i)
	{
		if ((readRegisters[reg] & input[index][i].address) == input[index][i].address)
		{
			input[index][i].prevValue = input[index][i].value;
			input[index][i].value = 0;//OBRNJENE VREDNOSTI ZARADI DVOJNE NEGACIJE 
		}
		else
		{
			input[index][i].prevValue = input[index][i].value;
			input[index][i].value = 1;//OBRNJENE VREDNOSTI ZARADI DVOJNE NEGACIJE 
		}
	}
	

}


void XilinxZynq::SetOutputSignals(int reg, int index)
{
	for (int i = 0; i < 16; i++)
	{
		if ((writeRegisters[reg] & output[index][i].address) == output[index][i].address)
		{
			output[index][i].prevValue = output[index][i].value;
			output[index][i].value = 1;
		}
		else
		{
			output[index][i].prevValue = output[index][i].value;
			output[index][i].value = 0;
		}
	}
}

/*void XilinxZynq::SetOutputSignals()
{
	for (int i = 0; i < 16; ++i)
	{
		if ((writeRegisters[42] & output[i].address) == output[i].address)
		{
			output[i].prevValue = output[i].value;
			output[i].value = 1;
		}
		else
		{
			output[i].prevValue = output[i].value;
			output[i].value = 0;
		}
	}
}*/







void XilinxZynq::SetOutputValue(int index, int bits, bool value)
{
	int reg = 0;
	if (index == 0)
		reg = 0x2A;
	else if (index == 1)
		reg = 0x29;
	else if (index == 2)
		reg = 0x2B;
	else
		reg = 0;

	QString tmp;
	QString tmpReg;
	if (value)
	{
		writeRegisters[reg] = writeRegisters[reg] | output[index][bits].address;
	}
	else
	{
		writeRegisters[reg] = writeRegisters[reg] & (~output[index][bits].address);
	}
	WriteRegister(reg);
	
	SetOutputSignals(reg,index);

}


void XilinxZynq::SetOutputReg(int reg, int bits, bool value)
{
	//write register DEC 42 , HEX 0x2A -> nastavljen za output P2 konnekor
	

}

void XilinxZynq::SetP3Direction(int part, int direction)
{
	//dolocitev smeri se nastavlja na write registru 40, 0x28
	//direction 
	int low = 0;
	int high = 0;

	if (part == 0)
	{
		low = 1;
		high = 1;
		
	}
	else if (part == 1)
	{
		low = 1;
		high = 0;
	}
	else if (part == 2)
	{
		low = 0;
		high = 1;
	}
	if (direction == 0)
	{
		low = !low;
		high = !high;
		isOutputP3 = false;
	}
	else
		isOutputP3 = true;
	if (low == 1)
	{
		writeRegisters[40] = writeRegisters[40] | 0x0001;
	}
	else
	{
		writeRegisters[40] = writeRegisters[40] & (~0x0001);
	}

	if (high == 1)
	{
		writeRegisters[40] = writeRegisters[40] | 0x0002;
	}
	else
	{
		writeRegisters[40] = writeRegisters[40] & (~0x0002);
	}
	

	WriteRegister(40);

}

void XilinxZynq::SetP8Direction(int direction)
{
	//dolocitev smeri se nastavlja na write registru 40, 0x28 bit 3
	if (direction == 1)
	{
		writeRegisters[40] = writeRegisters[40] | 0x0004;
		isOutputP8 = true;
	}
	else
	{
		writeRegisters[40] = writeRegisters[40] & (~0x0004);
		isOutputP8 = false;
	}
	WriteRegister(40);
	//SetIONames();//to je samo zacasno da spremeni zapise v dialogu za P8
}


void XilinxZynq::RocnoProzenjeKamere(int cam)
{
	int posRocno = 0; 
	int posAvto = 0; 
	QString tmp;
	int reg;

	int value = 0;
	if ((cam == 0)||(cam == 4))
	{
		posRocno = 1;
		posAvto = 0;
	}
	else if ((cam == 1) || (cam == 5))
	{
		posRocno = 5;
		posAvto = 4;
	}
	else if ((cam == 2) || (cam == 6))
	{
		posRocno = 9;
		posAvto = 8;
	}
	else if ((cam == 3) || (cam == 7))
	{
		posRocno = 13;
		posAvto = 12;
	}

	posRocno = pow(2, posRocno);
	posAvto = pow(2, posAvto);
	if ((cam >= 0) && (cam < 4))
		reg = 20;
	else if ((cam >= 4) && (cam < 8))
		reg = 30;


		 writeRegisters[reg] = writeRegisters[reg] | posRocno;
		 writeRegisters[reg] = writeRegisters[reg] & ~posAvto;
		 WriteRegister(reg);
		 value = writeRegisters[reg];
		 for (int i = 0; i < 1000000; i++)
		 {

		 }
		 writeRegisters[reg] = writeRegisters[reg] & ~posRocno;
		 writeRegisters[reg] = writeRegisters[reg] & ~posAvto;
		 WriteRegister(reg);
		 value = writeRegisters[reg];


}

void XilinxZynq::RocnoLuci(int luc,bool enable)
{
	int posRocno = 0;
	int posAvto = 0;
	QString tmp;
	int reg;

	int value = 0;
	if ((luc == 0) || (luc == 4))
	{
		posRocno = 3;
		posAvto = 2;
	}
	else if ((luc == 1) || (luc == 5))
	{
		posRocno = 7;
		posAvto = 6;
	}
	else if ((luc == 2) || (luc == 6))
	{
		posRocno = 11;
		posAvto = 10;
	}
	else if ((luc == 3) || (luc == 7))
	{
		posRocno = 15;
		posAvto = 14;
	}

	posRocno = pow(2, posRocno);
	posAvto = pow(2, posAvto);
	if ((luc >= 0) && (luc < 4))
		reg = 20;
	else if ((luc >= 4) && (luc < 8))
		reg = 30;

	if(enable == 1)
	writeRegisters[reg] = writeRegisters[reg] | posRocno;
	else
	writeRegisters[reg] = writeRegisters[reg] & ~posRocno;


	writeRegisters[reg] = writeRegisters[reg] & ~posAvto;
	WriteRegister(reg);
	luci[luc].value = enable;

}



void XilinxZynq::readPendingDatagrams()
{
	QByteArray recv;
	int statusCode = 0;
	int tmpIndexMinRegNum;
	int tmpIndexMaxRegNum;
	int tmpIndexMaxRegValue;
	int delta = 0;
	bool getRegNum = false;
	bool getRegValue = false;
	bool regNumSet = false;
	bool regValueSet = false;
	QByteArray tmpArrayRegNum;
	QByteArray tmpArrayRegValue;
	recv.clear();

	if (udpClient->pendingDatagramSize() > 0)
	{
		if (dataReadyFreq.timeCounter >= fps)
		{
			dataReadyFreq.SetStop();

			dataReadyFreq.CalcFrequency(dataReadyFreq.timeCounter);
			dataReadyFreq.timeCounter = 0;
			dataReadyFreq.SetStart();
		}
		dataReadyFreq.timeCounter++;


		connectionOnCounter++;
		statusCode = udpClient->pendingDatagramSize();
		recv = udpClient->readAll();


		float timeValue[200];
		int getsize[200];
		QString tmpVersion;
		dataReadyFreq.SetStop();
		timeValue[dataReadyFreq.timeCounter] = dataReadyFreq.ElapsedTime();
		getsize[dataReadyFreq.timeCounter] = statusCode;


		for (int i = 0; i < statusCode; i++)
		{
			dataBuf[dataBufCounter] = recv[i];

			if ((dataBuf[dataBufCounter - 1] == 13) && (dataBuf[dataBufCounter] == 10))//
			{
				for (int j = 0; j < dataBufCounter; j++)
				{
					if (dataBuf[j] == 'V')
					{
						int t = j + 2;
						for (int i = t; i < t + 4; i++)
							tmpVersion.append(dataBuf[i]);
						currVersion = tmpVersion.toFloat();
					}
					if (dataBuf[j] == 'R')
						tmpIndexMinRegNum = j;
					if (dataBuf[j] == '-')
					{
						tmpIndexMaxRegNum = j;
						getRegNum = true;
						tmpArrayRegNum.clear();
					}
					if (dataBuf[j] == ';')
					{
						tmpIndexMaxRegValue = j;
						getRegValue = true;
						tmpArrayRegValue.clear();
					}

					if (getRegNum == true)
					{
						getRegNum = false;
						for (int i = tmpIndexMinRegNum + 1; i < tmpIndexMaxRegNum; i++)
							tmpArrayRegNum.append(dataBuf[i]);

						regNumSet = true;

					}

					if (getRegValue == true)
					{
						getRegValue = false;
						for (int i = tmpIndexMaxRegNum + 1; i < tmpIndexMaxRegValue; i++)
							tmpArrayRegValue.append(dataBuf[i]);
						regValueSet = true;

					}

					if ((regNumSet == true) && (regValueSet == true))
					{
						regNumSet = false;
						regValueSet = false;
						int num = 0;
						int value = 0;
						num = tmpArrayRegNum.toInt();
						value = tmpArrayRegValue.toInt();
						readRegisters[num] = value;

						if (num == 1)	//prebere input  register
							SetInputSignals(num, 0);
						else if (num == 39)	//inpu register konektor P3
							SetInputSignals(num, 1);
						else if (num == 41)	//input register Konektor P8
							SetInputSignals(num, 2);
						else if (num == 40) //input register Konektor P9
							SetInputSignals(num, 3);



					}

				}
				dataBufCounter = 0;
			}
			else
			{
				dataBufCounter++;
			}
		}
	}
}

void XilinxZynq::CreateDisplay()
{
	QSpacerItem *spacerRead;
	QSpacerItem *spacerReadHigh;
	QVBoxLayout *vLayout,*vLayout2;
	vLayout = new QVBoxLayout();
	vLayout2 = new QVBoxLayout();
	spacerRead = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	spacerReadHigh = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	ui.scrollAreaReadReg->setLayout(vLayout);
	ui.scrollAreaReadReg->setWidgetResizable(true);
	ui.scrollAreaReadReg->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	ui.scrollAreaWriteReg->setLayout(vLayout2);
	ui.scrollAreaWriteReg->setWidgetResizable(true);
	ui.scrollAreaWriteReg->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	QString tmp;
	for (int i = 0; i < 64; i++)
	{
		readHLayout[i] = new QHBoxLayout();
		readRegLabel[i] = new QLabel();
		readRegValueLabel[i] = new QLabel();
		tmp = QString("x1").number(i, 16);
		readRegLabel[i]->setText(QString("REG: 0x")+ tmp);
		readRegLabel[i]->setMaximumWidth(50);
		readRegValueLabel[i]->setText("0xffff");
		readRegValueLabel[i]->setFrameShape(QFrame::Box);
		readRegValueLabel[i]->setMaximumWidth(50);
		readHLayout[i]->addWidget(readRegLabel[i]);
		readHLayout[i]->addWidget(readRegValueLabel[i]);
		vLayout->addLayout(readHLayout[i]);
		ui.scrollAreaReadReg->widget()->setLayout(vLayout);
	}
	for (int i = 0; i < 64; i++)
	{
		writeHLayout[i] = new QHBoxLayout();
		writeRegLabel[i] = new QLabel();
		writeRegValueLabel[i] = new QLabel();
		tmp = QString("x1").number(i, 16);
		writeRegLabel[i]->setText(QString("REG: 0x") + tmp);
		writeRegLabel[i]->setMaximumWidth(50);
		writeRegValueLabel[i]->setText("0xffff");
		writeRegValueLabel[i]->setFrameShape(QFrame::Box);
		writeRegValueLabel[i]->setMaximumWidth(50);
		writeHLayout[i]->addWidget(writeRegLabel[i]);
		writeHLayout[i]->addWidget(writeRegValueLabel[i]);
		vLayout2->addLayout(writeHLayout[i]);
		ui.scrollAreaWriteReg->widget()->setLayout(vLayout2);
	}

	const int nrSpacerItem = 4;
	QSpacerItem *spacer[nrSpacerItem];
	for(int i = 0; i< nrSpacerItem; i++)
	spacer[i] = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);

	//input P1

		for (int i = 0; i < 16; i++)
		{
			checkBoxInput[0][i] = new QCheckBox();
			checkBoxInput[0][i]->setCheckable(true);
			ui.verticalLayoutInput->addWidget(checkBoxInput[0][i]);
		}
	
	ui.verticalLayoutInput->addSpacerItem(spacer[0]);
	//output P2
	int j = 0;

		for (int i = 0; i < 16; i++)
		{
			checkBoxOutput[0][i] = new QCheckBox();
			checkBoxOutput[0][i]->setCheckable(true);
			ui.verticalLayoutOutput->addWidget(checkBoxOutput[0][i]);
			connect(checkBoxOutput[0][i], &QPushButton::clicked, [=](int index) { OnClickedOutput(0, i); });

		}
	ui.verticalLayoutOutput->addSpacerItem(spacer[1]);

	//output P3
	for (int i = 0; i < 16; i++)
	{
		checkBoxIOP3[i] = new QCheckBox();
		checkBoxIOP3[i]->setCheckable(true);
		ui.verticalLayoutIOP3->addWidget(checkBoxIOP3[i]);
		connect(checkBoxIOP3[i], &QPushButton::clicked, [=](int index) { OnClickedOutput(1, i); });

		checkBoxIOP8[i] = new QCheckBox();
		checkBoxIOP8[i]->setCheckable(true);
		ui.verticalLayoutIOP8->addWidget(checkBoxIOP8[i]);
		connect(checkBoxIOP8[i], &QPushButton::clicked, [=](int index) { OnClickedOutput(2, i); });

	}
	ui.verticalLayoutIOP3->addSpacerItem(spacer[2]);

	//input P9
	for (int i = 0; i < 8; i++)
	{
		checkBoxInput[3][i] = new QCheckBox();
		checkBoxInput[3][i]->setCheckable(true);
		ui.verticalLayoutInputP9->addWidget(checkBoxInput[3][i]);
	}
	ui.verticalLayoutInputP9->addSpacerItem(spacer[3]);
	//cam and light
	QVBoxLayout * box[8];
	for (int i = 0; i < 8; i++)
	{
		buttonLight[i] = new QToolButton();
		buttonLight[i]->setMinimumHeight(50);
		buttonLight[i]->setMinimumWidth(50);
		buttonLight[i]->setText(QString("L:%1").arg(i));
		ui.layoutLight->addWidget(buttonLight[i]);
		connect(buttonLight[i], &QPushButton::clicked, [=](int index) { OnClickedButtonLight(i); });

		box[i] = new QVBoxLayout();
		buttonCamera[i] = new QToolButton();
		triggerCounter[i] = new QLabel();
		triggerCounter[i]->setText("");
		buttonCamera[i]->setMinimumHeight(50);
		buttonCamera[i]->setMinimumWidth(50);
		buttonCamera[i]->setText(QString("Cam:%1").arg(i));
		buttonCamera[i]->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
		box[i]->addWidget(buttonCamera[i]);
		box[i]->addWidget(triggerCounter[i]);
		ui.layoutCamera->addLayout(box[i]);
		connect(buttonCamera[i], &QPushButton::clicked, [=](int index) { OnClickedButtonCam(i); });
	}

	//input / output P3


	/*for (int i = 0; i < 8; i++)
	{
		checkBoxP3[i] = new QCheckBox();
		checkBoxP3[i]->setCheckable(false);
		ui.verticalLayoutIOP3->addWidget(checkBoxP3[i]);
		connect(checkBoxP3[i], &QPushButton::clicked, [=](int index) { OnClickedOutput(i); });
	}

	for (int i = 8; i < 16; i++)
	{
		checkBoxP3[i] = new QCheckBox();
		checkBoxP3[i]->setCheckable(false);
		ui.verticalLayoutIOP3->addWidget(checkBoxP3[i]);
		connect(checkBoxP3[i], &QPushButton::clicked, [=](int index) { OnClickedOutput(i); });

	}*/

	//ui.verticalLayoutIOP3->addSpacerItem(spacer[2]);

}

void XilinxZynq::SetIONames()
{
	for (int j = 0; j < 2; j++)
	{
		for (int i = 0; i < 16; i++)
		{
			if(j == 0)
			checkBoxOutput[j][i]->setText(QString("[%1][%2]   %3").arg(j).arg(i).arg(output[j][i].name));
			if (j == 0)
			{
				checkBoxInput[j][i]->setText(QString("[%1][%2]   %3").arg(j).arg(i).arg(input[j][i].name));

				if (isOutputP8 == 1)
					checkBoxIOP8[i]->setText(QString("[%1][%2]   %3").arg(2).arg(i).arg(output[2][i].name));
				else
					checkBoxIOP8[i]->setText(QString("[%1][%2]   %3").arg(2).arg(i).arg(input[2][i].name));

				if (isOutputP3 == 1)
					checkBoxIOP3[i]->setText(QString("[%1][%2]   %3").arg(1).arg(i).arg(output[1][i].name));
				else
					checkBoxIOP3[i]->setText(QString("[%1][%2]   %3").arg(1).arg(i).arg(input[1][i].name));
				
				for (int k = 0; k < 8; k++)

				{
					checkBoxInput[3][k]->setText(QString("[%1][%2]   %3").arg(3).arg(i).arg(input[3][k].name));
				}

			}


		}
	}


	

}

void XilinxZynq::WriteDatagram(QByteArray data)
{
	
	QByteArray sendData = data;
	QByteArray newText = data;

	QByteArrayMatcher carrageReturn;
	carrageReturn.setPattern("\r\n");


	if (carrageReturn.indexIn(sendData, 0) == -1)
	{
		sendData.append("\r\n");
	}
	lock.lockForWrite();
	udpClient->writeDatagram(sendData, hostIp, port);
	lock.unlock();
}

