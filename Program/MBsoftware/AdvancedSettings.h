#pragma once
#include "stdafx.h"
#include <QObject>


class AdvancedSettings : public QObject
{
	Q_OBJECT

public:
	AdvancedSettings();
	AdvancedSettings(QString referencePath, QString fileName,QStringList groupBoxList);
	AdvancedSettings(const AdvancedSettings& settings);// Copy constructor
	AdvancedSettings operator=(AdvancedSettings line);
	~AdvancedSettings();



public:
	void ReadParameters();
	void WriteParameters();
	void WriteParameters(QString groupName);
	void CopyParameters();
	void DeleteParameters();

	QString referencePath;
	QString name;
	QString				 fileName;
	vector<QString>			groupName;
	vector<vector<QString>> parameterName;
	vector<vector<float>>	parameterValue;
	vector<vector<int>>		parameterType;
};

