﻿// MemoryBuffer.cpp : implementation file
//

//#define _USE_MATH_DEFINES
#include "stdafx.h"
#include "MemoryBuffer.h"
#include "Timer.h"

//#include <direct.h>
//#include <ppl.h>

//Ta CMemoryBuffer ima že Aleševe popravke za openCV ter popravek dvojnega zrcaljenja in Martinove popravke do 23.5.


// CMemoryBuffer


CMemoryBuffer::CMemoryBuffer()
{
	Init();
}

CMemoryBuffer::CMemoryBuffer(int width, int height, int depth)
{
	Init();

	Create(width, height, depth);

}

CMemoryBuffer::CMemoryBuffer(QString path)
{
	LoadBuffer(path);

}

CMemoryBuffer::CMemoryBuffer(const CMemoryBuffer& buffer)// Copy constructor
{
	Init();

	if (buffer.isActive)
	{
		if (buffer.bufferSize)
		{
			Create(buffer.width, buffer.height, buffer.depth);
			std::copy(buffer.buffer->data, buffer.buffer->data + buffer.bufferSize, this->buffer->data);
		}
	}

	if (buffer.isActiveEdges) //ce so aktivni robovi prekopira tudi robove
	{

		CreateEdges();

		for (int i = 0; i < height; i++)
			std::copy(buffer.Xedges[i], buffer.Xedges[i] + (width), Xedges[i]);

		for (int i = 0; i < width; i++)
			std::copy(buffer.Yedges[i], buffer.Yedges[i] + (height), Yedges[i]);


		edgesXRectangle = buffer.edgesXRectangle;
		edgesYRectangle = buffer.edgesYRectangle;
	}

	if (buffer.isActiveCommonPointsWhite) //ce so aktivni robovi prekopira tudi robove
	{
		CreateCommonPointsWhite(buffer.maxShapesWhite, buffer.maxPointsWhite);

		maxShapesWhite = buffer.maxShapesWhite;
		maxPointsWhite = buffer.maxPointsWhite;

		std::copy(buffer.numberOfPointsWhite, buffer.numberOfPointsWhite + buffer.maxShapesWhite, numberOfPointsWhite);
		std::copy(buffer.commonPointsBufferWhite, buffer.commonPointsBufferWhite + buffer.width*buffer.height, commonPointsBufferWhite);
		std::copy(buffer.activePointWhite, buffer.activePointWhite + buffer.width*buffer.height, activePointWhite);
		std::copy(buffer.centerWhite, buffer.centerWhite + buffer.maxShapesWhite, centerWhite);

		std::copy(buffer.commonPointsWhiteX, buffer.commonPointsWhiteX + buffer.maxShapesWhite*buffer.maxPointsWhite, commonPointsWhiteX);
		std::copy(buffer.commonPointsWhiteY, buffer.commonPointsWhiteY + buffer.maxShapesWhite*buffer.maxPointsWhite, commonPointsWhiteY);

		nrShapesWhite = buffer.nrShapesWhite;

	}

	if (buffer.isActiveCommonPointsBlack) //ce so aktivni robovi prekopira tudi robove
	{
		CreateCommonPointsBlack(buffer.maxShapesBlack, buffer.maxPointsBlack);

		maxShapesBlack = buffer.maxShapesBlack;
		maxPointsBlack = buffer.maxPointsBlack;

		std::copy(buffer.numberOfPointsBlack, buffer.numberOfPointsBlack + buffer.maxShapesBlack, numberOfPointsBlack);
		std::copy(buffer.commonPointsBufferBlack, buffer.commonPointsBufferBlack + buffer.width*buffer.height, commonPointsBufferBlack);
		std::copy(buffer.activePointBlack, buffer.activePointBlack + buffer.width*buffer.height, activePointBlack);
		std::copy(buffer.centerBlack, buffer.centerBlack + buffer.maxShapesBlack, centerBlack);

		std::copy(buffer.commonPointsBlackX, buffer.commonPointsBlackX + buffer.maxShapesBlack*buffer.maxPointsBlack, commonPointsBlackX);
		std::copy(buffer.commonPointsBlackY, buffer.commonPointsBlackY + buffer.maxShapesBlack*buffer.maxPointsBlack, commonPointsBlackY);

		nrShapesBlack = buffer.nrShapesBlack;
	}
}



CMemoryBuffer::~CMemoryBuffer()
{
	DeleteBuffer();

}


// CMemoryBuffer message handlers
void CMemoryBuffer::Init()
{
	
	encoder = 0;
	depth = 0; //0: null, 1 - 8 bit, 3 - 24 bit, 4 - 32 bit
	width = 0;
	height = 0;
	bufferSize = 0;
	isActive = false;
	isActiveEdges = false;
	isActiveCommonPointsWhite = false;
	isActiveCommonPointsBlack = false;
	isActiveIntensity = false;
	isActiveIntensityRed = false;
	isActiveIntensityGreen = false;
	isActiveIntensityBlue = false;

	isActiveIntensityRadial = false;
	isActiveIntensityAngular = false;
	isActiveIntensityRotated = false;

	intensityRotatedLength = 0;

	xPosition = 0;
	yPosition = 0;
	edgesXRectangle.setRect(0, 0, 0, 0);
	edgesYRectangle.setRect(0, 0, 0, 0);
	maxShapesBlack = 0;
	maxShapesWhite = 0;
}
void CMemoryBuffer::Swap(CMemoryBuffer &firstBuffer, CMemoryBuffer &secondBuffer)
{
	// enable ADL (not necessary in our case, but good practice)
	using std::swap;

	// by swapping the members of two classes,
	// the two classes are effectively swapped
	swap(firstBuffer.depth, secondBuffer.depth);
	swap(firstBuffer.width, secondBuffer.width);
	swap(firstBuffer.height, secondBuffer.height);
	swap(firstBuffer.bufferSize, secondBuffer.bufferSize);
	swap(firstBuffer.bufferSize, secondBuffer.bufferSize);
	swap(firstBuffer.isActive, secondBuffer.isActive);


	swap(firstBuffer.buffer, secondBuffer.buffer);


	//  swap(firstBuffer.Xedges, secondBuffer.Xedges);
	//  swap(firsBuffer.Xedges, firsBuffer.Xedges);
	//  swap(firstBuffer.Yedges, secondBuffer.Yedges);
   //   swap(firsBuffer.Yedges, firsBuffer.Yedges);
}

CMemoryBuffer CMemoryBuffer::operator =(CMemoryBuffer buffer)
{
	Init();

	if (buffer.isActive)
	{
		if (buffer.bufferSize)
		{
			Create(buffer.width, buffer.height, buffer.depth);

			std::copy(buffer.buffer->data, buffer.buffer->data + buffer.bufferSize, this->buffer->data);
		}
	}

	if (buffer.isActiveEdges) //ce so aktivni robovi prekopira tudi robove
	{

		CreateEdges();

		for (int i = 0; i < height; i++)
			std::copy(buffer.Xedges[i], buffer.Xedges[i] + (width), Xedges[i]);

		for (int i = 0; i < width; i++)
			std::copy(buffer.Yedges[i], buffer.Yedges[i] + (height), Yedges[i]);

		edgesXRectangle = buffer.edgesXRectangle;
		edgesYRectangle = buffer.edgesYRectangle;
	}

	if (buffer.isActiveCommonPointsWhite) //ce so aktivni robovi prekopira tudi robove
	{
		CreateCommonPointsWhite(buffer.maxShapesWhite, buffer.maxPointsWhite);

		maxShapesWhite = buffer.maxShapesWhite;
		maxPointsWhite = buffer.maxPointsWhite;

		std::copy(buffer.numberOfPointsWhite, buffer.numberOfPointsWhite + buffer.maxShapesWhite, numberOfPointsWhite);
		std::copy(buffer.commonPointsBufferWhite, buffer.commonPointsBufferWhite + buffer.width*buffer.height, commonPointsBufferWhite);
		std::copy(buffer.activePointWhite, buffer.activePointWhite + buffer.width*buffer.height, activePointWhite);
		std::copy(buffer.centerWhite, buffer.centerWhite + buffer.maxShapesWhite, centerWhite);

		std::copy(buffer.commonPointsWhiteX, buffer.commonPointsWhiteX + buffer.maxShapesWhite*buffer.maxPointsWhite, commonPointsWhiteX);
		std::copy(buffer.commonPointsWhiteY, buffer.commonPointsWhiteY + buffer.maxShapesWhite*buffer.maxPointsWhite, commonPointsWhiteY);

		nrShapesWhite = buffer.nrShapesWhite;

	}

	if (buffer.isActiveCommonPointsBlack) //ce so aktivni robovi prekopira tudi robove
	{
		CreateCommonPointsBlack(buffer.maxShapesBlack, buffer.maxPointsBlack);

		maxShapesBlack = buffer.maxShapesBlack;
		maxPointsBlack = buffer.maxPointsBlack;

		std::copy(buffer.numberOfPointsBlack, buffer.numberOfPointsBlack + buffer.maxShapesBlack, numberOfPointsBlack);
		std::copy(buffer.commonPointsBufferBlack, buffer.commonPointsBufferBlack + buffer.width*buffer.height, commonPointsBufferBlack);
		std::copy(buffer.activePointBlack, buffer.activePointBlack + buffer.width*buffer.height, activePointBlack);
		std::copy(buffer.centerBlack, buffer.centerBlack + buffer.maxShapesBlack, centerBlack);

		std::copy(buffer.commonPointsBlackX, buffer.commonPointsBlackX + buffer.maxShapesBlack*buffer.maxPointsBlack, commonPointsBlackX);
		std::copy(buffer.commonPointsBlackY, buffer.commonPointsBlackY + buffer.maxShapesBlack*buffer.maxPointsBlack, commonPointsBlackY);

		nrShapesBlack = buffer.nrShapesBlack;
	}

	//CMemoryBuffer temp(buffer);
	//Swap(*this, temp); // (2)

	return *this;
}
bool CMemoryBuffer::operator ==(CMemoryBuffer buffer)
{
	if (width == buffer.width && height == buffer.height && depth == buffer.depth)
		return true;

	return false;
}
bool CMemoryBuffer::operator !=(CMemoryBuffer buffer)
{
	if (width != buffer.width || height != buffer.height || depth != buffer.depth)
		return true;

	return false;
}


int CMemoryBuffer::Create(int width, int height, int depth)
{

	//cce se je spremenila velikost bufferja ga pobrisemo 
	if (isActive) //buff je ze bil alociran 
	{
		if ((this->width != width) || (this->height != height) || (this->depth != depth))
		{
			//izbrisemo trenutni buffer
			DeleteBuffer();
		
		}

	}

	if (!isActive) //buffer se ni bil inicializiran
	{
		this->depth = depth; //0: null, 1 - 8 bit, 3 - 24 bit, 4 - 32 bit
		this->width = width;
		this->height = height;

		bufferSize = width * height*depth;


		//POZOR: ob priliki dodati v CMemoryBuffer spremenljivki bytesPerChanel in numberOfChannels, tako da je depth = bytesPerChannel * numberOfChanells

		buffer = new Mat(height, width, CV_8UC3, 1); //od 9.12.2020 pretvorim vse slike v 3byte format zaradi risanja na sliko z OPENCV 
	/*if (depth == 1)
		buffer = new Mat(height, width, CV_8UC3, 1);
	else if (depth == 2)
		buffer = new Mat(height, width, CV_8UC3, 1);
	else if (depth == 3)
		buffer = new Mat(height, width, CV_8UC3, 1);
	else if (depth == 4)
		buffer = new Mat(height, width, CV_8UC3, 1);
		*/
	isActive = true;


		//Opomba: zadnji argument nima veze s formatom, je le vrednost za inicializacijo pikslov!
	//https://docs.opencv.org/3.1.0/d3/d63/classcv_1_1Mat.html#a3620c370690b5ca4d40c767be6fb4ceb




	
	}

	return 0;
}

/**
Uporaba common point:
1.	kreiramo tabele glede na velikost bufferja, stevilo ploskev in stevilo tock
	kreiramo glede na to kaksne ploskve iscemo - bele ali crne
	CreateCommonPointsWhite ali CreateCommonPointsBlack
2.  iscemo ploskve glede na barvo in vrsto iskanja
	FindCommonPointsWhiteFast, FindCommonPointsWhiteWiki
	FindCommonPointsBlackFast, FindCommonPointsBlackWiki
*/

int CMemoryBuffer::CreateCommonPointsWhite(int maxShapes, int maxPoints)
{
	if (isActiveCommonPointsWhite) //buff je ze bil alociran 
	{
		if ((this->maxShapesWhite != maxShapes) || (this->maxPointsWhite != maxPoints))
		{
			//izbrisemo trenutni bufferdraw
			DeleteCommonPointsWhite();
		}
	}

	if (isActive && !isActiveCommonPointsWhite) //robove naredi samo ce je bila slika alocirana in robovi se niso bili
	{
		maxShapesWhite = maxShapes;
		maxPointsWhite = maxPoints;

		boundingBoxWhite = new QRect[maxShapes];
		centerWhite = new CPointFloat[maxShapes];
		numberOfPointsWhite = new int[maxShapes];

		commonPointsBufferWhite = new int*[width];
		activePointWhite = new int*[width];

		for (int i = 0; i < width; i++)
		{
			commonPointsBufferWhite[i] = new int[height];
			activePointWhite[i] = new int[height];
		}

		commonPointsWhiteX = new UINT16*[maxShapesWhite];
		commonPointsWhiteY = new UINT16*[maxShapesWhite];

		for (int i = 0; i < maxShapesWhite; i++)
		{
			commonPointsWhiteX[i] = new UINT16[maxPointsWhite];
			commonPointsWhiteY[i] = new UINT16[maxPointsWhite];
		}

		commonEdgePointsWhite.resize(maxShapesWhite, vector<CPointFloat>(4, CPointFloat(0, 0))); //vsaka ploskev ima 4 robne tocke


		isActiveCommonPointsWhite = true;
		nrShapesWhite = 0;

		return 1;
	}


	return 0;
}

int CMemoryBuffer::CreateCommonPointsBlack(int maxShapes, int maxPoints)
{
	if (isActiveCommonPointsBlack) //buff je ze bil alociran 
	{
		if ((this->maxShapesBlack != maxShapes) || (this->maxPointsBlack != maxPoints))
		{
			//izbrisemo trenutni buffer
			DeleteCommonPointsBlack();
		}
	}

	if (isActive && !isActiveCommonPointsBlack) //robove naredi samo ce je bila slika alocirana in robovi se niso bili
	{
		maxShapesBlack = maxShapes;
		maxPointsBlack = maxPoints;

		numberOfPointsBlack = new int[maxShapes];
		boundingBoxBlack = new QRect[maxShapes];
		centerBlack = new CPointFloat[maxShapes];
		commonPointsBufferBlack = new int*[width];
		activePointBlack = new int*[width];

		for (int i = 0; i < width; i++)
		{
			commonPointsBufferBlack[i]	= new int[height];
			activePointBlack[i]			= new int[height];

		}

		commonPointsBlackX		= new UINT16*[maxShapesBlack];
		commonPointsBlackY		= new UINT16*[maxShapesBlack];

		for (int i = 0; i < maxShapesBlack; i++)
		{
			commonPointsBlackX[i] = new UINT16[maxPointsBlack];
			commonPointsBlackY[i] = new UINT16[maxPointsBlack];
		}

		commonEdgePointsBlack.resize(maxShapesBlack, vector<CPointFloat>(4, CPointFloat(0, 0))); //vsaka ploskev ima 4 robne tocke


		isActiveCommonPointsBlack = true;
		nrShapesBlack = 0;

		return 1;

	}
	return 0;
}

void CMemoryBuffer::CreateConnectedComponentsTable(Mat labels, Mat stats)
{
	int area, shapeNumber, seqPixel;
	int maxArea = 0;
	this->numShapes = stats.rows;
	//Tabela za beleženje zaporedne številke piksla v določeni obliki
	seqPixTable = new int[this->numShapes];

	//Rezerviramo pomnilnik za tabelo povezanih oblik
	//Tabela za vsako številko oblike in vsako številko zaporednega piksla znotraj oblike shranjuje koordinato piksla
	connectedComponentsCoorX = new float*[this->numShapes];
	connectedComponentsCoorY = new float*[this->numShapes];

	for (int i = 0; i < this->numShapes; i++)
	{
		area = stats.at<int>(i, CC_STAT_AREA);
		connectedComponentsCoorX[i] = new float[area];
		connectedComponentsCoorY[i] = new float[area];
		seqPixTable[i] = 0;
	}

	//Napolnimo tabelo povezanih oblik z x,y - koordinatami pikslov pripadajoče oblike
	for (int i = 0; i < labels.rows; i++)
	{
		for (int j = 0; j < labels.cols; j++)
		{
			shapeNumber = labels.at<int>(i, j);
			if (shapeNumber >= 0 && shapeNumber < this->numShapes)
			{
				seqPixel = seqPixTable[shapeNumber];
				connectedComponentsCoorX[shapeNumber][seqPixel] = j;
				connectedComponentsCoorY[shapeNumber][seqPixel] = i;
				seqPixTable[shapeNumber] += 1;
			}
		}
	}
}

int CMemoryBuffer::CreateEdges()
{
	int i, j;
	if (isActive && !isActiveEdges) //robove naredi samo ce je bila slika alocirana in robovi se niso bili
	{
		edgesXRectangle.setRect(0, 0, 0, 0);
		edgesYRectangle.setRect(0, 0, 0, 0);

		Xedges = new float*[height];

		for (i = 0; i < height; i++)
			Xedges[i] = new float[width];

		Yedges = new float*[width];
		for (i = 0; i < width; i++)
			Yedges[i] = new float[height];

		isActiveEdges = true;

		ClearEdgesXY();

		return 1;
	}

	return 0;
}

int CMemoryBuffer::CreateIntensity()
{
	int size;

	if (isActive && !isActiveIntensity) //robove naredi samo ce je bila slika alocirana in robovi se niso bili
	{
		//		rect.setRect(0,0,0,0); da ne pobrisemo definiranega kvadrata

		size = std::max(buffer->cols, buffer->rows);

		intensity.reserve(size);
		for (int i = 0; i < size; i++)
			intensity.push_back(0);
		//intensity = new float[size];

		vertical = false;

		isActiveIntensity = true;

		return 1;
	}

	return 0;
}

int CMemoryBuffer::CreateIntensityRed()
{
	int size;

	if (isActive && !isActiveIntensityRed) //robove naredi samo ce je bila slika alocirana in robovi se niso bili
	{
		//rect.setRect(0,0,0,0);

		size = std::max(width,height);

		intensityRed = new float[size];

		vertical = false;


		isActiveIntensityRed = true;

		return 1;
	}

	return 0;
}

int CMemoryBuffer::CreateIntensityBlue()
{
	int size;

	if (isActive && !isActiveIntensityBlue) //robove naredi samo ce je bila slika alocirana in robovi se niso bili
	{
		//rect.setRect(0,0,0,0);

		size = std::max(width, height);

		intensityBlue = new float[size];

		vertical = false;

		isActiveIntensityBlue = true;

		return 1;
	}

	return 0;
}

int CMemoryBuffer::CreateIntensityGreen()
{
	int size;

	if (isActive && !isActiveIntensityGreen) //robove naredi samo ce je bila slika alocirana in robovi se niso bili
	{
		//rect.setRect(0,0,0,0);

		size = std::max(width, height);

		intensityGreen = new float[size];

		vertical = false;

		isActiveIntensityGreen = true;

		return 1;
	}

	return 0;
}

int	CMemoryBuffer::CreateIntensityRadial()
{
	int size = 2 * (width + height);  //maksimalni obseg kroznice ocenim z obsegom celotne slike

	if (isActive && !isActiveIntensityRadial)
	{

		intensityRadial = new float[size];

		isActiveIntensityRadial = true;

		return 1;
	}

	return 0;
}
int	CMemoryBuffer::CreateIntensityAngular()
{
	int size = std::max(width, height);

	if (isActive && !isActiveIntensityAngular)
	{
		intensityAngular = new float[size];

		isActiveIntensityAngular = true;

		return 1;
	}

	return 0;
}

int	CMemoryBuffer::CreateIntensityRotated()
{
	int size = sqrt(pow(width, 2) + pow(height, 2));

	if (isActive && !isActiveIntensityRadial)
	{
		intensityRotated = new float[size];
		intensityRotatedGraph = new CPointFloat[size];

		isActiveIntensityRotated = true;

		return 1;
	}

	return 0;
}

void CMemoryBuffer::DeleteBuffer()
{
	if (isActive)
	{
		delete buffer;


		isActive = false;	 //v spodnjem if ga odpremo v novih velikostih

		DeleteEdges(); //ce brisemo buffer moramo nujno tudi robove, da ne bodo ostale napacne dimenzije 
		DeleteCommonPointsWhite();
		DeleteCommonPointsBlack();
		DeleteIntensity();
		DeleteIntensityRed();
		DeleteIntensityGreen();
		DeleteIntensityBlue();

		DeleteIntensityRadial();
		DeleteIntensityAngular();
		DeleteIntensityRotated();
	}
}

void CMemoryBuffer::DeleteCommonPointsWhite()
{


	if (isActiveCommonPointsWhite)
	{
		delete[] numberOfPointsWhite;
		delete[] boundingBoxWhite;
		delete[]  centerWhite;
		for (int i = 0; i < width; ++i)
		{
			delete[] commonPointsBufferWhite[i];
			delete[] activePointWhite[i];
		}
		delete[] commonPointsBufferWhite;
		delete[] activePointWhite;

		for (int i = 0; i < this->maxShapesWhite; i++)
		{
			delete[] commonPointsWhiteX[i];
			delete[] commonPointsWhiteY[i];
		}
		delete[] commonPointsWhiteX;
		delete[] commonPointsWhiteY;


		nrShapesWhite = 0;
		isActiveCommonPointsWhite = false;	 //v spodnjem ifu ga odpremo v novih velikostih
	}
}

void CMemoryBuffer::DeleteCommonPointsBlack()
{
	if (isActiveCommonPointsBlack)
	{
		delete[] numberOfPointsBlack;
		delete[] boundingBoxBlack;
		delete[]  centerBlack;
		for (int i = 0; i < width; ++i)
		{
			delete[] commonPointsBufferBlack[i];
			delete[] activePointBlack[i];
		}
		delete[] commonPointsBufferBlack;
		delete[] activePointBlack;

		for (int i = 0; i < this->maxShapesBlack; i++)
		{
			delete[] commonPointsBlackX[i];
			delete[] commonPointsBlackY[i];
		}
		delete[] commonPointsBlackX;
		delete[] commonPointsBlackY;


		nrShapesWhite = 0;
		isActiveCommonPointsBlack = false;	 //v spodnjem ifu ga odpremo v novih velikostih
	}


}
void CMemoryBuffer::DeleteConnectedComponentsTable()
{
	for (int i = 0; i < this->numShapes; i++)
	{
		delete[] connectedComponentsCoorX[i];
		delete[] connectedComponentsCoorY[i];
	}
	delete[] seqPixTable;
	delete[] connectedComponentsCoorX;
	delete[] connectedComponentsCoorY;

}
void CMemoryBuffer::DeleteEdges()
{
	if (isActiveEdges)
	{
		edgesXRectangle.setRect(0, 0, 0, 0);
		edgesYRectangle.setRect(0, 0, 0, 0);

		for (int i = 0; i < this->width; ++i)
		{
			delete[] this->Yedges[i];
		}

		for (int i = 0; i < this->height; ++i)
		{
			delete[] this->Xedges[i];
		}

		isActiveEdges = false;	 //v spodnjem ifu ga odpremo v novih velikostih
	}

}

void CMemoryBuffer::DeleteIntensity()
{
	if (isActiveIntensity)
	{
		//delete [] intensity;

		isActiveIntensity = false;	 //v spodnjem ifu ga odpremo v novih velikostih
	}
}

void CMemoryBuffer::DeleteIntensityRed()
{
	if (isActiveIntensityRed)
	{
		delete[] intensityRed;

		isActiveIntensityRed = false;	 //v spodnjem ifu ga odpremo v novih velikostih
	}
}

void CMemoryBuffer::DeleteIntensityBlue()
{
	if (isActiveIntensityBlue)
	{
		delete[] intensityBlue;

		isActiveIntensityBlue = false;	 //v spodnjem ifu ga odpremo v novih velikostih
	}
}



void CMemoryBuffer::DeleteIntensityGreen()
{
	if (isActiveIntensityGreen)
	{
		delete[] intensityGreen;

		isActiveIntensityGreen = false;	 //v spodnjem ifu ga odpremo v novih velikostih
	}
}

void CMemoryBuffer::DeleteIntensityRadial()
{
	if (isActiveIntensityRadial)
	{
		delete[] intensityRadial;

		isActiveIntensityRadial = false;
	}
}
void CMemoryBuffer::DeleteIntensityAngular()
{
	if (isActiveIntensityAngular)
	{
		delete[] intensityAngular;

		isActiveIntensityAngular = false;
	}
}

void CMemoryBuffer::DeleteIntensityRotated()
{
	if (isActiveIntensityRotated)
	{
		delete[] intensityRotated;
		delete[] intensityRotatedGraph;

		isActiveIntensityRotated = false;
	}
}

int CMemoryBuffer::Copy(CMemoryBuffer newBuffer)
{
	this->buffer = newBuffer.buffer;
	return 0;
}



int CMemoryBuffer::LoadBuffer(QString path)
{
	String name;
	Mat tmp;

	name = path.toStdString();

	tmp = imread(name, -1);

	if (!tmp.empty())
		*buffer = tmp;


	return 0;
}

int CMemoryBuffer::LoadBuffer(QString path, int depth)
{
	QFile file;

	
	
	return 0;
}



int CMemoryBuffer::SaveBuffer(QString path)
{
	QString dirPath = path;
	QDir dir;


	String name;

	name = path.toStdString();
	imwrite(name, *buffer);


	return 0;
}
int CMemoryBuffer::SaveBuffer(QString path, QString name)
{
	QString dirPath = path;
	QDir dir;
	QString filePath;

	dir.setPath(dirPath);
	if (!dir.exists())
		dir.mkpath(dirPath);

	filePath = dirPath + name;
	String nameCv;

	nameCv = filePath.toStdString();
	imwrite(nameCv, *buffer);

	return 0;
}
int CMemoryBuffer::SaveBuffer(QString path, QRect roi)
{

	return 0;
}
 
int CMemoryBuffer::GetPosition(int x, int y)
{
	if ((x >= 0) && (y >= 0) && (x < width) && (y < height))
		return (int)((height - 1 - y) * (width * depth) + (x * depth));

	return -1;
}

int CMemoryBuffer::GetRed(int position)
{
	if ((position >= 0) && (position < bufferSize))
	{
		if (depth > 1)
			return (int)buffer->data[position + 2]; //BGR0

		return (int)buffer->data[position];
	}

	return -1;
}

int CMemoryBuffer::GetRed(int x, int y)
{
	/*
	Scalar intensity;
	intensity = buffer->at<uchar>(y, x);

	return intensity[1];
	*/
	if (x < 0 || x >= this->width)
	{
		int bla = 0;
	}
	if (y < 0 || y >= this->width)
	{
		int bla = 0;
	}


	Vec3b intensity;
	intensity = buffer->at<Vec3b>(y, x);
	return intensity.val[2];

}


int CMemoryBuffer::GetGreen(int x, int y)
{
	//Scalar intensity;

	//intensity = buffer->at<uchar>(y, x);
	//return intensity[0];

	if (x < 0 || x >= this->width)
	{
		int bla = 0;
	}
	if (y < 0 || y >= this->width)
	{
		int bla = 0;
	}

	Vec3b intensity;
	intensity = buffer->at<Vec3b>(y, x);
	return intensity.val[1];
}



int CMemoryBuffer::GetBlue(int x, int y)
{
	/*
	Scalar intensity;
	intensity = buffer->at<uchar>(y, x);

	return intensity[2];
	*/

	if (x < 0 || x >= this->width)
	{
		int bla = 0;
	}
	if (y < 0 || y >= this->width)
	{
		int bla = 0;
	}

	Vec3b intensity;
	intensity = buffer->at<Vec3b>(y, x);
	return intensity.val[0];
}



int CMemoryBuffer::GetAverageIntensity(int x, int y)
{
	int position;
	int avrageOld;
	int pos = 0;
	//if (InRectangle(x, y) == 1)
	//	{

	//	}
	int vh = buffer[0].channels();
	//POZOR: ob priliki dodati v CMemoryBuffer spremenljivki bytesPerChanel in numberOfChannels, tako da je depth = bytesPerChannel * numberOfChanells
	if (depth == 1)
	{
		pos = buffer[0].data[buffer[0].channels()*(buffer[0].cols*y + x)];
		Scalar intensity3;
		intensity3 = buffer->at<uchar>(y, x);
		return pos;
	}
	else if (depth == 3)
	{
		Vec3b intensity;
		intensity = buffer->at<Vec3b>(y, x);

		return (intensity.val[0] + intensity.val[1] + intensity.val[2]) / 3;
	}
	return 0;
}

int CMemoryBuffer::GetThreshold(Rect region)
{
	float information[256];
	float H0, H1, P, maxInformation;
	int pixVal, optThresh;

	//Poiščemo relativne frekvence sivih nivojev
	GetRelativeFrequencies(region);

	//Računanje entropije za posamezen prag
	for (int k = 0; k < 256; k++)
	{
		H0 = 0;
		H1 = 0;
		P = 0;

		for (int t = 0; t <= k; t++)
		{
			P += relativeFrequencies[t];
		}

		for (int i = 0; i <= k; i++)
		{
			if (P != 0 && relativeFrequencies[i] != 0)
				H0 -= (relativeFrequencies[i] / P) * log(relativeFrequencies[i] / P);
		}

		for (int j = k + 1; j < 256; j++)
		{
			if (relativeFrequencies[j] != 0)
				H1 -= (relativeFrequencies[j] / (1 - P)) * log(relativeFrequencies[j] / (1 - P));
		}
		//Shranimo vsoto entropij(informacija) za posamezen prag
		information[k] = H0 + H1;
	}

	maxInformation = 0;
	//Poiščemo prag pri katerem je informacija maksimalna
	for (int l = 0; l < 256; l++)
	{
		if (information[l] > maxInformation)
		{
			maxInformation = information[l];
			optThresh = l;
		}
	}
	
	return optThresh;
}

int CMemoryBuffer::FilterShapes(Mat labelStats, int minArea, int maxArea)
{
	int area;
	int counter = 0;

	//Sprehodimo se po vseh oblikah in jih filtriramo glede na površino
	for (int i = 0; i < labelStats.rows; i++)
	{
		area = labelStats.at<int>(i, CC_STAT_AREA);

		if (area >= minArea && area <= maxArea)
		{
			counter++;
		}
	}

	return counter;
}


//izraèuna dinamièno mejo intenzitete kot srednjo vrednost med belim in èrnim (belo in èrno loèi glede na približno mejo intenzitete)
bool CMemoryBuffer::CalcDynamicTreshold(float approxTreshold, int latticePeriod)
{
	int x, y, position, nWhite, nBlack, intensity;

	float sumWhite, sumBlack, avgWhite, avgBlack;

	sumWhite = 0;
	sumBlack = 0;
	nWhite = 0;
	nBlack = 0;

	for (x = 0; x < this->width; x += latticePeriod)
	{
		for (y = 0; y < this->height; y += latticePeriod)
		{
			//position = (pImage->height - 1 - j)*pImage->width + i;	//stara pozicija

			position = y * this->width + x;
			intensity = this->buffer->data[position];



			if (intensity >= approxTreshold)
			{
				sumWhite += intensity;
				nWhite++;
			}
			else
			{
				sumBlack += intensity;
				nBlack++;
			}
		}
	}

	avgWhite = sumWhite / nWhite;
	avgBlack = sumBlack / nBlack;

	this->dynamicTreshold = (avgWhite + avgBlack) / 2;

	return true;
}



float CMemoryBuffer::GetInterpolatedIntensity(float x, float y)
{
	int xCeli, yCeli;
	int sirina, visina, pozicija;
	float xDec, yDec;
	float fy1, fy2;

	byte Intensity[2][2];	//intenziteta pri štirih najbližjih celih indeksih

	sirina = this->buffer->cols;
	visina = this->buffer->rows;

	xCeli = (int)x;
	yCeli = (int)y;

	//ÈE JE KATEROKOLI OD ŠTIRIH OGLIŠÈ ZA INTERPOLACIJO ZUNAJ SLIKE, POTEM NE MOREMO INTERPOLIRATI
	if (xCeli < 0) return -1;
	if (yCeli < 0) return -1;
	if (xCeli + 1 > sirina - 1) return -1;
	if (yCeli + 1 > visina - 1) return -1;

	xDec = x - xCeli;
	yDec = y - yCeli;



	//Pri transformaciji x,y v indeks bufferja predpostavljam zlaganje vrstic v buffer v smeri y-- (ker so tako delali prejšnji programerji v funkcijah GetAverageIntensity)
	
	//pozicija = yCeli * sirina + xCeli;
	pozicija = buffer->channels()*(buffer->cols*yCeli + xCeli);
	Intensity[0][0] = buffer[0].data[pozicija];
	//Intensity[0][0] = this->buffer->data[pozicija];
	//int bla = buffer->channels()*(buffer->cols*y + x);
	//Intensity[0][0] = buffer[0].data[pozicija];


	//pozicija = (yCeli + 1)*sirina + xCeli;
	
	pozicija = buffer->channels()*((buffer->cols*(yCeli +1)) + xCeli);
	Intensity[0][1] = buffer[0].data[pozicija];


	//Intensity[0][1] = this->buffer->data[pozicija];


	//pozicija = yCeli * sirina + (xCeli + 1);
	pozicija = buffer->channels()*((buffer->cols*yCeli) + (xCeli +1));
	Intensity[1][0] = this->buffer->data[pozicija];

	//pozicija = (yCeli + 1)*sirina + xCeli + 1;
	pozicija = buffer->channels()*((buffer->cols*(yCeli + 1)) + (xCeli+1));
	Intensity[1][1] = this->buffer->data[pozicija];



	fy1 = (1 - xDec)*Intensity[0][0] + xDec * Intensity[1][0];
	fy2 = (1 - xDec)*Intensity[0][1] + xDec * Intensity[1][1];

	return (1 - yDec)*fy1 + yDec * fy2;
}


/*
bool CMemoryBuffer::RotateBuffer(CMemoryBuffer* pStaraSlika, float fiDeg, float zoomFactor, float xOffset, float yOffset, bool interpoliraj)
{
	int i, j, iIntOld, jIntOld, xRot, yRot;
	int sirina, visina, pozicija;
	float fi,xOld, yOld, iFloatOld, jFloatOld;
	byte Intensity;

	fi = fiDeg * M_PI / 180;

	sirina = this->width;
	visina = this->height;

	for (i = 0; i < sirina; i++)
	{
		for (j = 0; j < visina; j++)
		{

			//Grafika programa MBCamera preverjeno prikazuje buffer slike tako, da smer i+ ustreza x+ (desno), j+ pa y+ (navzdol)
			//center koordinatnega sistema postavim na sredo slike, da bo tam os rotacije
			xRot = i - 0.5*(sirina-1);
			yRot = j - 0.5*(visina-1);


			//izraèunamo toèko xOld,yOld, ki se preslika v xRot,yRot:
			// 1. Obrnemo translacijo
			xOld = xRot - xOffset;
			yOld = yRot - yOffset;
			//2. Obrnemo skaliranje
			xOld = xOld / zoomFactor;
			yOld = yOld / zoomFactor;
			//3. Obrnemo rotacijo
			xOld = cos(fi)*xOld + sin(fi)*yOld;
			yOld = -sin(fi)*xOld + cos(fi)*yOld;

			//rotirana slika mora imeti pri xRot,yRot tako intenziteto, kot jo je imela originalna v toèki, ki se preslika v xRot,yRot


			iFloatOld = xOld + 0.5*(sirina - 1);
			jFloatOld = yOld + 0.5*(visina - 1);

			if (interpoliraj)
			{
				//intenziteta z interpolacijo: odberemo jo na float koordinatah, njeno vrednost zaokrožimo na najbližje celo število
				Intensity = (int) (pStaraSlika->GetInterpolatedIntensity(iFloatOld, jFloatOld) + 0.5);
			}
			else
			{
				iIntOld =(int) iFloatOld;
				jIntOld = (int) jFloatOld;

				pozicija = (visina - 1 -jIntOld)*sirina + iIntOld;	//stara pozicija

				if ((iIntOld >= 0 && iIntOld < sirina) && (jIntOld >= 0 && jIntOld < visina)) //Intenziteta brez interpolacije (na celoštevilskih koordinatah)
					Intensity = pStaraSlika->buffer->data[pozicija];
				else Intensity = -1;

			}

			pozicija = (visina - 1 - j)*sirina + i;  //nova pozicija

			this->buffer->data[pozicija] = Intensity;

		}
	}


	return true;
}
*/
int CMemoryBuffer::GetWhiteIntensityCount(CRectRotated polygon, int threshold)
{
	int count = 0;
	int i, j, tmp, size;

	if (polygon.left() > polygon.right())
	{
		tmp = polygon.left();
		polygon.setLeft(polygon.right());
		polygon.setRight( tmp);
	}

	if (polygon.top() > polygon.bottom())
	{
		tmp = polygon.top();
		polygon.setTop( polygon.bottom());
		polygon.setBottom(tmp);
	}

	size = polygon.polygonPoints.size();
	//ce v kvadratu se niso bili najdeni robovi, jih poisce 
	if ((polygon.left() >= 0) && (polygon.right() <= width) &&
		(polygon.top() >= 0) && (polygon.bottom() <= height))
	{

		for (i = polygon.left(); i < polygon.right(); i++)
		{
			for (j = polygon.top(); j < polygon.bottom(); j++)
			{
				if (size > 0)
				{
					if (polygon.InPolygon(i, j))
					{
						if (GetAverageIntensity(i, j) > threshold)
							count++;
					}
				}
				else
				{
					if (GetAverageIntensity(i, j) > threshold)
						count++;
				}
			}
		}
	}

	return count;
}

int CMemoryBuffer::GetBlackIntensityCount(CRectRotated polygon, int threshold)
{
	int count = 0;
	int i, j, tmp, size;

	if (polygon.left() > polygon.right())
	{
		tmp = polygon.left();
		polygon.setLeft(polygon.right());
		polygon.setRight(tmp);
	}

	if (polygon.top() > polygon.bottom())
	{
		tmp = polygon.top();
		polygon.setTop( polygon.bottom());
		polygon.setBottom(tmp);
	}

	size = polygon.polygonPoints.size();
	//ce v kvadratu se niso bili najdeni robovi, jih poisce 
	if ((polygon.left() >= 0) && (polygon.right() <= width) &&
		(polygon.top() >= 0) && (polygon.bottom() <= height))
	{

		for (i = polygon.left(); i < polygon.right(); i++)
		{
			for (j = polygon.top(); j < polygon.bottom(); j++)
			{
				if (polygon.InPolygon(i, j))
				{
					if (size > 0)
					{
						if (polygon.InPolygon(i, j))
						{
							if (GetAverageIntensity(i, j) < threshold)
								count++;
						}
					}
					else
					{
						if (GetAverageIntensity(i, j) < threshold)
							count++;
					}
				}
			}
		}
	}

	return count;
}

void CMemoryBuffer::SetAverageIntensity(int position, int value)
{
	if ((position >= 0) && (position < bufferSize))
	{
		buffer->data[position] = value; //BGR0
	}
}

void CMemoryBuffer::SetAverageIntensity(int x, int y, int value)
{
	int position;

	//position = GetPosition(x, y);
	//SetAverageIntensity(position, value);

	buffer->at<uchar>(y, x) = value;
}

void CMemoryBuffer::SetRed(int position, int value)
{
	if ((position >= 0) && (position < bufferSize))
	{
		if (depth > 1)
			buffer->data[position + 2] = value; //BGR0
		else
			buffer->data[position] = value; //BGR0
	}
}

void CMemoryBuffer::SetRed(int x, int y, int value)
{
	int position;

	position = GetPosition(x, y);

	SetRed(position, value);
}

void CMemoryBuffer::SetBlue(int position, int value)
{
	if ((position >= 0) && (position < bufferSize))
	{
		buffer->data[position] = value; //BGR0
	}
}

void CMemoryBuffer::SetBlue(int x, int y, int value)
{
	int position;

	position = GetPosition(x, y);

	SetBlue(position, value);
}

int CMemoryBuffer::GetImageFocus(int gradientDegree, QString gradientDirection)
{
	long unsigned int value = 0;
	int gradient;

	if (gradientDirection == "vertical")//vsota kvadratov vertikalnih gradientov
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height - 2; y++)
			{
				gradient = abs(buffer->data[GetPosition(x, y + 2) - buffer->data[GetPosition(x, y)]]);
				value += gradient; //* gradient;
			}
		}
	}

	else//vsota kvadratov horizontalnih gradientov
	{
		for (int x1 = 0; x1 < width - 2; x1++)
		{
			for (int y1 = 0; y1 < height; y1++)
			{
				gradient = abs(buffer->data[GetPosition(x1 + 2, y1) - buffer->data[GetPosition(x1, y1)]]);
				value += gradient; //* gradient;
			}
		}
	}

	return value;
}

void CMemoryBuffer::SetGreen(int position, int value)
{
	if ((position >= 0) && (position < bufferSize))
	{
		if (depth > 1)
			buffer->data[position + 1] = value; //BGR0
		else
			buffer->data[position] = value; //BGR0
	}
}

void CMemoryBuffer::SetGreen(int x, int y, int value)
{
	int position;

	position = GetPosition(x, y);

	SetGreen(position, value);
}





void CMemoryBuffer::ClearBuffer()
{

	DeleteBuffer();
}



void CMemoryBuffer::AddBufferVertical(CMemoryBuffer &picture)
{
	int position = yPosition * width * depth;

	if ((width >= picture.width) && (picture.depth == depth))
	{
		for (int i = 0; i < width * picture.height * depth; i++)
		{
			buffer->data[position + i] = picture.buffer->data[i];
		}
		yPosition = yPosition + picture.height;
	}
}

void CMemoryBuffer::AddBufferVertical(BYTE* pBuf, int width, int height, int depth)
{
	int position = yPosition * width * depth;

	if ((this->width >= width) && (this->depth == depth))
	{
		for (int i = 0; i < width * height * depth; i++)
		{
			buffer->data[position + i] = pBuf[i];
		}
		yPosition = yPosition + height;
	}
}

void CMemoryBuffer::AddBufferVertical(BYTE* pBuf, int width, int height, int depth, int yPosition)
{
	int position = yPosition * width * depth;

	if ((this->width >= width) && (this->depth == depth))
	{
		for (int i = 0; i < width * height * depth; i++)
		{
			buffer->data[position + i] = pBuf[i];
		}
		yPosition = yPosition + height;
	}
	this->yPosition = yPosition; //remember last y index
}

void CMemoryBuffer::AddBufferHorizontal(CMemoryBuffer &picture)
{
	int i, j, n;
	int position;

	if ((height >= picture.height) && (depth == picture.depth))
	{
		n = 0;
		for (j = 0; j < picture.height; j++)
		{
			for (i = 0; i < picture.width * picture.depth; i++)
			{
				position = (width * j) + (xPosition * depth) + i;
				if (position < bufferSize)
					buffer->data[position] = picture.buffer->data[n];
				else
					break;
				n++;
			}
		}
		xPosition += picture.width;
	}
}
void CMemoryBuffer::AddBufferHorizontal(BYTE* pBuf, int width, int height, int depth)
{
	int i, j, n;
	int position;

	if ((this->height >= height) && (this->depth == depth))
	{
		n = 0;
		for (j = 0; j < height; j++)
		{
			for (i = 0; i < width * depth; i++)
			{
				position = (this->width * j) + (xPosition * depth) + i;
				if (position < bufferSize)
					buffer->data[position] = pBuf[n];
				else
					break;
				n++;
			}
		}
		xPosition += width;
	}
}
void CMemoryBuffer::AddBufferHorizontal(BYTE* pBuf, int width, int height, int depth, int xPosition)
{
	int i, j, n;
	int position;

	if ((this->height >= height) && (this->depth == depth))
	{
		n = 0;
		for (j = 0; j < height; j++)
		{
			for (i = 0; i < width * depth; i++)
			{
				position = (this->width * j) + (xPosition * depth) + i;
				if (position < bufferSize)
					buffer->data[position] = pBuf[n];
				else
					break;

				n++;
			}
		}
		xPosition += width;
	}
	this->xPosition = xPosition; //remember last x index
}

int CMemoryBuffer::GetMinIntensity(CRectRotated rec)
{
	int i, j;
	int value = 0;
	int minInt = 255;

	for (i = rec.left(); i < rec.right() - 1; i++)
	{
		for (j = rec.top(); j < rec.bottom() - 1; j++)
		{
			value = buffer->data[GetPosition(i, j)];
			if (value < minInt)
				minInt = value;
		}
	}
	return minInt;
}


int	CMemoryBuffer::GetMinIntensity(QRect rec)
{
	return GetMinIntensity(CRectRotated(rec.left(), rec.top(), rec.right(), rec.bottom()));
}

int	CMemoryBuffer::GetMinIntensity()
{
	return GetMinIntensity(CRectRotated(0, 0, width, height));
}

int	CMemoryBuffer::GetMaxIntensity(CRectRotated rec)
{
	int i, j;
	int value = 0;
	int maxInt = 0;

	for (i = rec.left(); i < rec.right() - 1; i++)
	{
		for (j = rec.top(); j < rec.bottom() - 1; j++)
		{
			value = buffer->data[GetPosition(i, j)];
			if (value > maxInt)
				maxInt = value;
		}
	}
	return maxInt;
}

int	CMemoryBuffer::GetMaxIntensity(QRect rec)
{
	return GetMaxIntensity(CRectRotated(rec.left(), rec.top(), rec.right(), rec.bottom()));
}

int	CMemoryBuffer::GetMaxIntensity()
{
	return GetMaxIntensity(QRect(0, 0, width, height));
}

//average intensity in square
int CMemoryBuffer::GetAverageIntensity(QRect rec)
{
	int i, j;
	int value = 0;
	int counter = 0;

	for (i = rec.left(); i < rec.right() - 1; i++)
	{
		for (j = rec.top(); j < rec.bottom() - 1; j++)
		{
			value += GetAverageIntensity(i, j);
			counter++;
		}
	}
	if (counter > 0)
		return value / counter;

	return 0;
}

int CMemoryBuffer::GetAverageIntensity(QRect rec, int whiteThreshold, int blackThreshold)
{
	//povprecna intenziteta, v obmocju thresholdov
	int i, j;
	int value = 0;
	int counter = 0;
	int position;

	for (i = rec.left(); i < rec.right() - 1; i++)
	{
		for (j = rec.top(); j < rec.bottom() - 1; j++)
		{
			position = GetPosition(i, j);
			if ((whiteThreshold > buffer->data[position]) && (blackThreshold < buffer->data[position]))
			{
				value += buffer->data[position];
				counter++;
			}
		}
	}
	if (counter > 0)
		return value / counter;

	return 0;
}
//average intensity in whole image
int CMemoryBuffer::GetAverageIntensity()
{
	int i;
	int value = 0;

	for (i = 0; i < bufferSize; i++)
	{
		value += buffer->data[i];
	}
	if (bufferSize > 0)
		return value / bufferSize;

	return 0;
}

int CMemoryBuffer::GetImageThresholds(CRectRotated whiteArea, CRectRotated blackArea)
{
	int middle;
	whiteThreshold = GetAverageIntensity(whiteArea);
	blackThreshold = GetAverageIntensity(blackArea);
	middle = (blackThreshold + whiteThreshold) / 2;

	if (whiteThreshold > 15)
		whiteThreshold -= 15;

	if (blackThreshold < 240)
		blackThreshold += 15;

	return middle;
}

int CMemoryBuffer::GetImageThresholds(QRect whiteArea, QRect blackArea)
{
	return GetImageThresholds(CRectRotated(whiteArea.left(), whiteArea.top(), whiteArea.right(), whiteArea.bottom()), CRectRotated(blackArea.left(), blackArea.top(), blackArea.right(), blackArea.bottom()));
}

int CMemoryBuffer::GetImageThresholds(CRectRotated area)
{
	//cela dobro, ce ni povprecna intenziteta podobna kot crni in beli nivo

	int i, j;
	int value = 0;
	int minInt, maxInt, threshold, counterWhite, counterBlack;

	threshold = GetAverageIntensity(area);

	counterWhite = 0;
	counterBlack = 0;
	blackThreshold = 0;
	whiteThreshold = 0;

	for (i = area.left(); i < area.right() - 1; i++)
	{
		for (j = area.top(); j < area.bottom() - 1; j++)
		{
			value = GetAverageIntensity(i, j);

			if (value < threshold)
			{
				blackThreshold += value;
				counterBlack++;
			}
			else
			{
				whiteThreshold += value;
				counterWhite++;
			}
		}
	}

	if (counterBlack > 0)
		blackThreshold /= counterBlack;

	if (counterWhite > 0)
		whiteThreshold /= counterWhite;

	return (blackThreshold + whiteThreshold) / 2;
}

int CMemoryBuffer::GetImageThresholds(QRect area)
{
	return GetImageThresholds(CRectRotated(area.left(), area.top(), area.right(), area.bottom()));
}

int CMemoryBuffer::GetImageThresholds()
{
	return GetImageThresholds(CRectRotated(0, 0, width, height));
}

RGBQUAD CMemoryBuffer::GetAverageIntensityRGB(QRect rec)
{
	int i, j;
	int value[3];
	int counter = 0;
	int position;
	RGBQUAD averateInt;

	value[0] = 0;
	value[1] = 0;
	value[2] = 0;

	for (i = rec.left(); i < rec.right() - 1; i++)
	{
		for (j = rec.top(); j < rec.bottom() - 1; j++)
		{
			position = GetPosition(i, j);
			value[0] += buffer->data[position];
			value[1] += buffer->data[position + 1];
			value[2] += buffer->data[position + 2];

			counter++;
		}
	}

	averateInt.rgbBlue = value[0] / counter;
	averateInt.rgbGreen = value[1] / counter;
	averateInt.rgbRed = value[2] / counter;

	return averateInt;
}

int CMemoryBuffer::FindEdges(QRect R, int threshold)
{
	//x in y robovi
	int counter = 0;
	counter = FindEdgesX(R, threshold);
	counter = counter + FindEdgesY(R, threshold);

	return counter;
}

void CMemoryBuffer::ClearEdgesXY()
{
	if (isActiveEdges)
	{
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				Xedges[i][j] = 0;
				Yedges[j][i] = 0;
			}
		}
	}
}
void CMemoryBuffer::ClearEdgesXY(QRect R)
{
	if (isActiveEdges)
	{
		for (int i = R.top(); i < R.bottom(); i++)
		{
			for (int j = R.left(); j < R.right(); j++)
			{
				Xedges[i][j] = 0;
				Yedges[j][i] = 0;
			}
		}
	}
}

void CMemoryBuffer::ClearEdgesX(void)
{
	if (isActiveEdges)
	{
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				Xedges[i][j] = 0;
	}
}

void CMemoryBuffer::ClearEdgesY(void)
{
	if (isActiveEdges)
	{
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				Yedges[i][j] = 0;
	}
}


int CMemoryBuffer::FindEdgesX(QRect R, int threshold)
{
	int x, y, intensity, intensityOld, binaryIntensity, binaryIntensityOld, tmp;
	float mediana, dif;
	int counter = 0;

	//preveri, ce so robovi alocirani
	CreateEdges();


	if (isActiveEdges)
	{
		if ((threshold > 255) || (threshold < 0))
			threshold = 128;

		if (R.left() < 0)
			R.setLeft(0);
		if (R.right() > width)
			R.setRight(width);
		if (R.top() < 0)
			R.setTop(0);
		if (R.bottom() > height)
			R.setBottom(height);

		if (R.left() > R.right())
		{
			tmp = R.left();
			R.setLeft(R.right());
			R.setRight(tmp);
		}

		if (R.top() > R.bottom())
		{
			tmp = R.top();
			R.setTop(R.bottom());
			R.setBottom(tmp);
		}

		edgesXRectangle = R;

		for (y = R.top(); y < R.bottom(); y++)
		{
			intensity = 0;
			intensityOld = 0;
			binaryIntensityOld = 0;
			binaryIntensity = 0;

			for (x = R.left(); x < R.right(); x++)
			{
				intensity = GetAverageIntensity(x, y);

				if (intensity > threshold)
					binaryIntensity = 1;
				else
					binaryIntensity = 0;

				if (x == R.left())		// prvi piksel
				{
					binaryIntensityOld = binaryIntensity;
				}
				else
				{
					if (binaryIntensity > binaryIntensityOld)		// prehod iz 0 v 1
					{
						mediana = (float)(intensity - intensityOld);
						dif = intensity - threshold;

						if (mediana > 0)
							Xedges[y][x] = (float)x - (dif / mediana);
						else
							Xedges[y][x] = (float)x;

						counter++;


					}
					else if (binaryIntensity < binaryIntensityOld)		// prehod iz 1 v 0
					{
						mediana = (float)(intensityOld - intensity);
						dif = threshold - intensity;

						if (mediana > 0)
							Xedges[y][x] = (float)x - (dif / mediana);
						else
							Xedges[y][x] = (float)x;


						counter++;
					}
					else
						Xedges[y][x] = (float)0;

				}

				intensityOld = intensity;
				binaryIntensityOld = binaryIntensity;
			}
		}

		//nastavi rectangle kjer so najdeni robovi
		edgesXRectangle = R;
	}
	return counter;
}

int CMemoryBuffer::FindEdgesY(QRect R, int threshold)
{
	int x, y, intensity, intensityOld, binaryIntensity, binaryIntensityOld, tmp;
	float mediana, dif;
	int counter = 0;

	//preveri, ce so robovi alocirani
	CreateEdges();


	if (isActiveEdges)
	{
		if ((threshold > 255) || (threshold < 0))
			threshold = 128;

		if (R.left() < 0)
			R.setLeft(0);
		if (R.right() > width)
			R.setRight(width);
		if (R.top() < 0)
			R.setTop(0);
		if (R.bottom() > height)
			R.setBottom( height);

		if (R.left() > R.right())
		{
			tmp = R.left();
			R.setLeft(R.right());
			R.setRight(tmp);
		}

		if (R.top() > R.bottom())
		{
			tmp = R.top();
			R.setTop(R.bottom());
			R.setBottom(R.bottom());
		}


		edgesYRectangle = R;

		for (x = R.left(); x < R.right(); x++)
		{
			intensity = 0;
			intensityOld = 0;
			binaryIntensityOld = 0;
			binaryIntensity = 0;

			for (y = R.top(); y < R.bottom(); y++)
			{
				intensity = GetAverageIntensity(x, y);

				if (intensity > threshold)
					binaryIntensity = 1;
				else
					binaryIntensity = 0;

				if (y == R.top())		// prvi piksel
				{
					binaryIntensityOld = binaryIntensity;
				}
				else
				{
					if (binaryIntensity > binaryIntensityOld)		// prehod iz 0 v 1
					{
						mediana = (float)(intensity - intensityOld);
						dif = intensity - threshold;

						if (mediana > 0)
							Yedges[x][y] = (float)y - (dif / mediana);
						else
							Yedges[x][y] = (float)y;

						counter++;
					}
					else if (binaryIntensity < binaryIntensityOld)		// prehod iz 1 v 0
					{
						mediana = (float)(intensityOld - intensity);
						dif = threshold - intensity;

						if (mediana > 0)
							Yedges[x][y] = (float)y - (dif / mediana);
						else
							Yedges[x][y] = (float)y;

						counter++;

					}
					else
						Yedges[x][y] = (float)0;

				}

				intensityOld = intensity;
				binaryIntensityOld = binaryIntensity;
			}
		}

		//nastavi rectangle kjer so najdeni robovi
		edgesYRectangle = R;
	}
	return counter;
}



//returns point that is in the middle of this point ant argument point
CPointFloat CMemoryBuffer::GetTopEdgePoint(QRect area)
{
	//** vrne najvisjo  tocko v kvadratu (najmanjsi y)
	int i, j, tmp;
	CPointFloat point;

	if (isActiveEdges)
	{
		point.SetPointFloat(0, height + 1000);

		if (area.left() > area.right())
		{
			tmp = area.left();
			area.setLeft(area.right());
			area.setRight(tmp);
		}

		if (area.top() > area.bottom())
		{
			tmp = area.top();
			area.setTop(area.bottom());
			area.setBottom(tmp);
		}

		//ce v kvadratu se niso bili najdeni robovi, jih poisce 
		if ((area.left() >= 0) && (area.right() <= width) &&
			(area.top() >= 0) && (area.bottom() <= height))
		{

			for (i = area.left(); i < area.right(); i++)
			{
				for (j = area.top(); j < area.bottom(); j++)
				{

					//preveri X in Y robove, da odkrije najmanjsega
					if (Yedges[i][j] > 0)
					{
						if (point.y > Yedges[i][j])
						{
							point.y = Yedges[i][j];

							if (Xedges[j][i] > 0)
								point.x = Xedges[j][i];
							else
								point.x = i;

						}
					}

					if (Xedges[j][i] > 0)
					{
						if (point.y > j)
						{
							point.x = Xedges[j][i];

							if (Yedges[i][j] > 0)
								point.y = Yedges[i][j];
							else
								point.y = j;
						}
					}
				}
			}
		}
	}
	return point;

}
CPointFloat CMemoryBuffer::GetBottomEdgePoint(QRect area)
{
	//** vrne najnizjo tocko v kvadratu (najvecji y)
	int i, j, tmp;
	CPointFloat point;
	point.SetPointFloat(0, -1);

	if (isActiveEdges)
	{
		if (area.left() > area.right())
		{
			tmp = area.left();
			area.setLeft(area.right());
			area.setRight(tmp);
		}

		if (area.top() > area.bottom())
		{
			tmp = area.top();
			area.setTop(area.bottom());
			area.setBottom(tmp);
		}

		//ce v kvadratu se niso bili najdeni robovi, jih poisce 
		if ((area.left() >= 0) && (area.right() <= width) &&
			(area.top() >= 0) && (area.bottom() <= height))
		{

			for (i = area.left(); i < area.right(); i++)
			{
				for (j = area.top(); j < area.bottom(); j++)
				{

					//preveri X in Y robove, da odkrije najmanjsega
					if (Yedges[i][j] > 0)
					{
						if (point.y < Yedges[i][j])
						{

							point.y = Yedges[i][j];

							if (Xedges[j][i] > 0)
								point.x = Xedges[j][i];
							else
								point.x = i;

						}
					}

					if (Xedges[j][i] > 0)
					{
						if (point.y < j)
						{
							point.x = Xedges[j][i];

							if (Yedges[i][j] > 0)
								point.y = Yedges[i][j];
							else
								point.y = j;
						}
					}
				}
			}
		}
	}
	return point;
}
CPointFloat CMemoryBuffer::GetRightEdgePoint(QRect area)
{
	//**isce najbolj desno tocko v kvadratu
	int i, j, tmp;
	CPointFloat point;
	point.SetPointFloat(0, 0);

	if (isActiveEdges)
	{
		if (area.left() > area.right())
		{
			tmp = area.left();
			area.setLeft(area.right());
			area.setRight(tmp);
		}

		if (area.top() > area.bottom())
		{
			tmp = area.top();
			area.setTop(area.bottom());
			area.setBottom(tmp);
		}

		//ce v kvadratu se niso bili najdeni robovi, jih poisce 
		if ((area.left() >= 0) && (area.right() <= width) &&
			(area.top() >= 0) && (area.bottom() <= height))
		{

			for (i = area.left(); i < area.right(); i++)
			{
				for (j = area.top(); j < area.bottom(); j++)
				{

					//preveri X in Y robove, da odkrije najmanjsega
					if (Yedges[i][j] > 0)
					{
						if (point.x < i)
						{
							point.y = Yedges[i][j];

							if (Xedges[j][i] > 0)
								point.x = Xedges[j][i];
							else
								point.x = i;
						}
					}

					if (Xedges[j][i] > 0)
					{
						if (point.x < Xedges[j][i])
						{
							point.x = Xedges[j][i];

							if (Yedges[i][j] > 0)
								point.y = Yedges[i][j];
							else
								point.y = j;

						}
					}
				}
			}
		}
	}
	return point;
}
CPointFloat CMemoryBuffer::GetLeftEdgePoint(QRect area)
{
	//**isce najbolj levo tocko v kvadratu
	int i, j, tmp;
	CPointFloat point;
	point.SetPointFloat(width + 1000, 0);

	if (isActiveEdges)
	{
		if (area.left() > area.right())
		{
			tmp = area.left();
			area.setLeft(area.right());
			area.setRight(tmp);
		}

		if (area.top() > area.bottom())
		{
			tmp = area.top();
			area.setTop(area.bottom());
			area.setBottom(tmp);
		}

		if ((area.left() >= 0) && (area.right() <= width) &&
			(area.top() >= 0) && (area.bottom() <= height))
		{

			for (i = area.left(); i < area.right(); i++)
			{
				for (j = area.top(); j < area.bottom(); j++)
				{

					//preveri X in Y robove, da odkrije pravega
					if (Yedges[i][j] > 0)
					{
						if (point.x > i)
						{
							point.x = i;
							point.y = Yedges[i][j];
						}
					}

					if (Xedges[j][i] > 0)
					{
						if (point.x > Xedges[j][i])
						{
							point.x = Xedges[j][i];
							point.y = j;
						}
					}
				}
			}
		}
	}
	return point;
}
CPointFloat CMemoryBuffer::GetTopEdgePoint(CRectRotated polygon)
{
	//** vrne najvisjo  tocko v kvadratu (najmanjsi y)
	int i, j, tmp;
	CPointFloat point;
	point.SetPointFloat(0, height + 1000);

	if (isActiveEdges)
	{
		if (polygon.left() > polygon.right())
		{
			tmp = polygon.left();
			polygon.setLeft(polygon.right());
			polygon.setRight(tmp);
		}

		if (polygon.top() > polygon.bottom())
		{
			tmp = polygon.top();
			polygon.setTop(polygon.bottom());
			polygon.setBottom(tmp);
		}

		//ce v kvadratu se niso bili najdeni robovi, jih poisce 
		if ((polygon.left() >= 0) && (polygon.right() <= width) &&
			(polygon.top() >= 0) && (polygon.bottom() <= height))
		{

			for (i = polygon.left(); i < polygon.right(); i++)
			{
				for (j = polygon.top(); j < polygon.bottom(); j++)
				{
					if (polygon.InPolygon(i, j))
					{
						//preveri X in Y robove, da odkrije najmanjsega
						if (Yedges[i][j] > 0)
						{
							if (point.y > Yedges[i][j])
							{
								point.y = Yedges[i][j];

								if (Xedges[j][i] > 0)
									point.x = Xedges[j][i];
								else
									point.x = i;

							}
						}

						if (Xedges[j][i] > 0)
						{
							if (point.y > j)
							{
								point.x = Xedges[j][i];

								if (Yedges[i][j] > 0)
									point.y = Yedges[i][j];
								else
									point.y = j;
							}
						}
					}
				}
			}
		}
	}
	return point;
}
CPointFloat CMemoryBuffer::GetBottomEdgePoint(CRectRotated polygon)
{
	//** vrne najnizjo tocko v kvadratu (najvecji y)
	int i, j, tmp;
	CPointFloat point;
	point.SetPointFloat(0, -1);

	if (isActiveEdges)
	{
		if (polygon.left() > polygon.right())
		{
			tmp = polygon.left();
			polygon.setLeft(polygon.right());
			polygon.setRight(tmp);
		}

		if (polygon.top() > polygon.bottom())
		{
			tmp = polygon.top();
			polygon.setTop(polygon.bottom());
			polygon.setBottom(tmp);
		}
		//ce v kvadratu se niso bili najdeni robovi, jih poisce 
		if ((polygon.left() >= 0) && (polygon.right() <= width) &&
			(polygon.top() >= 0) && (polygon.bottom() <= height))
		{

			for (i = polygon.left(); i < polygon.right(); i++)
			{
				for (j = polygon.top(); j < polygon.bottom(); j++)
				{
					if (polygon.InPolygon(i, j))
					{

						//preveri X in Y robove, da odkrije najmanjsega
						if (Yedges[i][j] > 0)
						{
							if (point.y < Yedges[i][j])
							{

								point.y = Yedges[i][j];

								if (Xedges[j][i] > 0)
									point.x = Xedges[j][i];
								else
									point.x = i;

							}
						}

						if (Xedges[j][i] > 0)
						{
							if (point.y < j)
							{
								point.x = Xedges[j][i];

								if (Yedges[i][j] > 0)
									point.y = Yedges[i][j];
								else
									point.y = j;
							}
						}
					}
				}
			}
		}
	}
	return point;
}
CPointFloat CMemoryBuffer::GetRightEdgePoint(CRectRotated polygon)
{
	//**isce najbolj desno tocko v kvadratu
	int i, j, tmp;
	CPointFloat point;
	point.SetPointFloat(0, 0);

	if (isActiveEdges)
	{
		if (polygon.left() > polygon.right())
		{
			tmp = polygon.left();
			polygon.setLeft(polygon.right());
			polygon.setRight(tmp);
		}

		if (polygon.top() > polygon.bottom())
		{
			tmp = polygon.top();
			polygon.setTop(polygon.bottom());
			polygon.setBottom(tmp);
		}
		//ce v kvadratu se niso bili najdeni robovi, jih poisce 
		if ((polygon.left() >= 0) && (polygon.right() <= width) &&
			(polygon.top() >= 0) && (polygon.bottom() <= height))
		{

			for (i = polygon.left(); i < polygon.right(); i++)
			{
				for (j = polygon.top(); j < polygon.bottom(); j++)
				{
					if (polygon.InPolygon(i, j))
					{
						//preveri X in Y robove, da odkrije najmanjsega
						if (Yedges[i][j] > 0)
						{
							if (point.x < i)
							{
								point.y = Yedges[i][j];

								if (Xedges[j][i] > 0)
									point.x = Xedges[j][i];
								else
									point.x = i;
							}
						}

						if (Xedges[j][i] > 0)
						{
							if (point.x < Xedges[j][i])
							{
								point.x = Xedges[j][i];

								if (Yedges[i][j] > 0)
									point.y = Yedges[i][j];
								else
									point.y = j;
							}
						}
					}
				}
			}
		}
	}
	return point;
}
CPointFloat CMemoryBuffer::GetLeftEdgePoint(CRectRotated polygon)
{
	//**isce najbolj levo tocko v kvadratu
	int i, j, tmp;
	CPointFloat point;
	point.SetPointFloat(width + 1000, 0);

	if (isActiveEdges)
	{
		if (polygon.left() > polygon.right())
		{
			tmp = polygon.left();
			polygon.setLeft(polygon.right());
			polygon.setRight(tmp);
		}

		if (polygon.top() > polygon.bottom())
		{
			tmp = polygon.top();
			polygon.setTop(polygon.bottom());
			polygon.setBottom(tmp);
		}
		if ((polygon.left() >= 0) && (polygon.right() <= width) &&
			(polygon.top() >= 0) && (polygon.bottom() <= height))
		{

			for (i = polygon.left(); i < polygon.right(); i++)
			{
				for (j = polygon.top(); j < polygon.bottom(); j++)
				{
					if (polygon.InPolygon(i, j))
					{

						//preveri X in Y robove, da odkrije pravega
						if (Yedges[i][j] > 0)
						{
							if (point.x > i)
							{
								point.x = i;
								point.y = Yedges[i][j];
							}
						}

						if (Xedges[j][i] > 0)
						{
							if (point.x > Xedges[j][i])
							{
								point.x = Xedges[j][i];
								point.y = j;
							}
						}
					}
				}
			}
		}
	}
	return point;
}

//Opomba Martin: ta funkcija raèuna težišèe robov (prehodov intenzitete): za težišèe belega/èrnega je treba napisati drugo funkcijo
CPointFloat CMemoryBuffer::GetCenterOfGravity(QRect area)
{
	int x, y, tmp;
	int counter;
	CPointFloat point;
	point.SetPointFloat(0, 0);
	counter = 0;

	if (isActiveEdges)
	{
		if (area.left() > area.right())
		{
			tmp = area.left();
			area.setLeft( area.right());
			area.setRight( tmp);
		}

		if (area.top() > area.bottom())
		{
			tmp = area.top();
			area.setTop(area.bottom());
			area.setBottom( tmp);
		}

		if ((area.left() >= 0) && (area.right() <= width) &&
			(area.top() >= 0) && (area.bottom() <= height))
		{
			for (x = area.left(); x < area.right(); x++)
			{
				for (y = area.top(); y < area.bottom(); y++)
				{
					if (Xedges[y][x] > 0)
					{
						point.x += Xedges[y][x];

						if (Yedges[x][y] > 0)
							point.y += Yedges[x][y];
						else
							point.y += y;

						counter++;

					}
					else if (Yedges[x][y] > 0)
					{
						point.x += x;
						point.y += Yedges[x][y];

						counter++;
					}
				}
			}
		}

		if (counter != 0)
		{
			point.x /= counter;
			point.y /= counter;

		}
	}
	return point;
}
CPointFloat CMemoryBuffer::GetCenterOfGravity(CRectRotated polygon)
{
	int x, y, tmp;

	int counter;
	CPointFloat point;
	point.SetPointFloat(0, 0);
	counter = 0;

	if (isActiveEdges)
	{
		if (polygon.left() > polygon.right())
		{
			tmp = polygon.left();
			polygon.setLeft(polygon.right());
			polygon.setRight(tmp);
		}

		if (polygon.top() > polygon.bottom())
		{
			tmp = polygon.top();
			polygon.setTop(polygon.bottom());
			polygon.setBottom(tmp);
		}
		if ((polygon.left() >= 0) && (polygon.right() <= width) &&(polygon.top() >= 0) && (polygon.bottom() <= height))
		{
			for (x = polygon.left(); x < polygon.right(); x++)
			{
				for (y = polygon.top(); y < polygon.bottom(); y++)
				{
					if (polygon.InPolygon(x, y))
					{
						if (Xedges[y][x] > 0)
						{
							point.x += Xedges[y][x];

							if (Yedges[x][y] > 0)
								point.y += Yedges[x][y];
							else
								point.y += y;

							counter++;

						}
						else if (Yedges[x][y] > 0)
						{
							point.x += x;
							point.y += Yedges[x][y];

							counter++;
						}
					}
				}
			}
		}

		if (counter != 0)
		{
			point.x /= counter;
			point.y /= counter;

		}
	}
	return point;
}



CLine CMemoryBuffer::GetLine(std::vector<CRectRotated> polygons)
{
	int x, y, nrPol, polygonSize, counter;
	float sumX, sumY, sumSquareX, sumSquareY, sumProduct;
	float xMean, yMean;
	float refK, refN;

	CLine lineTmp;

	sumX = 0;
	sumY = 0;
	sumSquareX = 0;
	sumSquareY = 0;
	sumProduct = 0;
	counter = 0;
	polygonSize = polygons.size();


	if (isActiveEdges)
	{
		for (nrPol = 0; nrPol < polygonSize; nrPol++) //in all polygons
		{
			if (polygons[nrPol].left() >= 0 && polygons[nrPol].top() >= 0 && polygons[nrPol].right() < width && polygons[nrPol].bottom() < height)
			{
				for (x = polygons[nrPol].left(); x < polygons[nrPol].right(); x++)
				{
					for (y = polygons[nrPol].top(); y < polygons[nrPol].bottom(); y++)
					{
						if (polygons[nrPol].InPolygon(x, y))
						{
							if (Xedges[y][x] != 0)
							{
								sumX += Xedges[y][x];
								sumSquareX += (Xedges[y][x] * Xedges[y][x]);

								if (Yedges[x][y] != 0)
								{
									sumY += Yedges[x][y];
									sumSquareY += (Yedges[x][y] * Yedges[x][y]);
									sumProduct += (Yedges[x][y] * Xedges[y][x]);
								}
								else
								{
									sumY += y;
									sumSquareY += (y * y);
									sumProduct += (y * Xedges[y][x]);
								}

								counter++;

							}
							else if (Yedges[x][y] != 0)
							{
								sumX += x;
								sumY += Yedges[x][y];
								sumSquareX += (x * x);
								sumSquareY += (Yedges[x][y] * Yedges[x][y]);
								sumProduct += (Yedges[x][y] * x);

								counter++;
							}
						}
					}
				}
			}
		}

		//if at least 2 points exist
		if (counter > 2)
		{
			xMean = sumX / counter;
			yMean = sumY / counter;

			if (abs(sumSquareX - sumX * xMean) < counter)
			{
				lineTmp.k = HUGE_VAL;
				lineTmp.p1.SetPointFloat(xMean, 10);
				lineTmp.p2.SetPointFloat(xMean, 100);
			}
			else
			{
				lineTmp.k = (sumProduct - sumX * yMean) / (sumSquareX - sumX * xMean);
				lineTmp.p1.SetPointFloat(xMean, yMean);
				lineTmp.p2.SetPointFloat(xMean, yMean);
			}

			lineTmp.n = yMean - lineTmp.k * xMean;
		}
	}
	return lineTmp;
}

CLine CMemoryBuffer::GetLine(CRectRotated polygon)
{
	std::vector<CRectRotated> polygons;
	polygons.push_back(polygon);
	return GetLine(polygons);
}
/**on segment detects egde point, directionForward = 1 -> forward,  = 0 backward, threshold for finding edges*/


CPointFloat CMemoryBuffer::GetEdgePointOnSegment(CLine segment, int directionForward, int threshold)
{
	QRect rect;
	bool foundXedge = false;
	bool foundYedge = false;
	int xD, xU, yD, yU;
	int i, start, end, iStart, iEnd, smer, x, y, xRound[2], yRound[2];//0 - round down, 1 - round up , xRoundU, yRoundU;
	CPointFloat pointOnSegment;
	CPointFloat edgePoint;
	edgePoint.SetPointFloat(0, 0);


	//sestavimo kvadrat okoli tock daljice
	if (isActiveEdges)
	{
		if (segment.p1.x > segment.p2.x)
		{
			rect.setLeft(segment.p2.x);
			rect.setRight(segment.p1.x);
		}
		else
		{
			rect.setLeft(segment.p1.x);
			rect.setRight(segment.p2.x);
		}

		if (segment.p1.y > segment.p2.y)
		{
			rect.setTop(segment.p2.y);
			rect.setBottom(segment.p1.y);
		}
		else
		{
			rect.setTop(segment.p1.y);
			rect.setBottom(segment.p2.y);
		}

		if (rect.top() < 0)
			rect.setTop(0);

		if (rect.left() < 0)
			rect.setLeft(0);

		if (rect.right() > width)
			rect.setRight(width);

		if (rect.bottom() > height)
			rect.setBottom(height);

		if ((rect.right() < 0) || (rect.bottom() < 0) || (rect.left() > width) || (rect.bottom() > height))
			return CPointFloat(0, 0);

		//uporabno za redefinicijo funkcije, lahko klicemo iskanja tobov, ce podamo negativen threshold
		if ((threshold > 0) || (!isActiveEdges))
		{
			FindEdgesX(rect, threshold);
			FindEdgesY(rect, threshold);
		}

		if (abs(segment.k) < 1) //premica bolj vodoravna - hodimo po x ih
		{
			start = rect.left();
			end = rect.right();
		}
		else //hodimo po y
		{
			start = rect.top();
			end = rect.bottom();
		}

		if (directionForward == 1)
		{
			iStart = start;
			iEnd = end;
			smer = 1;
		}
		else
		{
			iStart = end;
			iEnd = start;
			smer = -1;
		}


		for (i = iStart; i*smer < iEnd*smer; i += smer)
		{
			edgePoint.SetPointFloat(0, 0);
			edgePoint.SetActive(false);

			if (abs(segment.k) < 1) //premica bolj vodoravna - hodimo po x ih
			{
				if (abs(segment.k) == 0)
					pointOnSegment.SetPointFloat(i, segment.p1.y);
				else
					pointOnSegment.SetPointFloat(i, (segment.k * i + segment.n));

			}
			else //premica bolj navpicna
			{
				if (abs(segment.k) < HUGE_VAL)
					pointOnSegment.SetPointFloat((i - segment.n) / segment.k, i);
				else
					pointOnSegment.SetPointFloat(segment.p1.x, i);

			}

			//round down
			xRound[0] = (int)ceil(pointOnSegment.x);
			yRound[0] = (int)ceil(pointOnSegment.y);

			xRound[1] = (int)floor(pointOnSegment.x);
			yRound[1] = (int)floor(pointOnSegment.y);

			for (xD = 0; xD < 2; xD++)		//pogleda 4 tocke, ce so razlicno zaokrozeni x, y		
			{
				for (yD = 0; yD < 2; yD++)
				{
					if (!foundXedge)
					{
						if (Xedges[yRound[yD]][xRound[xD]] != 0)
						{
							edgePoint.x = Xedges[yRound[yD]][xRound[xD]];
							edgePoint.SetActive(true);
							foundXedge = true;
						}
						else
						{
							edgePoint.x = pointOnSegment.x;
						}
					}
					if (!foundYedge)
					{
						if (Yedges[xRound[xD]][yRound[yD]] != 0)
						{
							edgePoint.y = Yedges[xRound[xD]][yRound[yD]];
							edgePoint.SetActive(true);
							foundYedge = true;
						}
						else
						{
							edgePoint.y = pointOnSegment.y;
						}
					}

					if (foundXedge && foundYedge) //ce predcasno najdemo robova vrnemo tocko, drugace pregledamo vse kombinacije
						return edgePoint;
				}
			}
			//ce je bil rob najden
			if (edgePoint.IsActive())
			{
				return edgePoint;
			}
		}


	}

	return CPointFloat(0, 0);
}
//poisce robno tocko na daljici, robovi morajo biti ze predhodno najdeni
CPointFloat CMemoryBuffer::GetEdgePointOnSegment(CLine segment, int directionForward)
{
	return GetEdgePointOnSegment(segment, directionForward, -1);
}



CPointFloat CMemoryBuffer::GetEdgePointOnSegment2D(CLine segment, bool forward, float step, bool blackToWhite, float treshold, int minThickness, bool transitionRequired)
{

	if (segment.p1 == segment.p2) return CPointFloat(0, 0);

	CPointFloat p1, p2;

	if (forward)
	{
		p1 = segment.p1;
		p2 = segment.p2;
	}
	else
	{
		p1 = segment.p2;
		p2 = segment.p1;
	}


	//binarne intenzitete so true, če je intenziteta večja ali enaka treshold in false sicer
	bool originalBinInt;	//binarna intenziteta na pred robom
	bool targetBinInt;    //binarna intenziteta za robom
	bool binInt;	//binarna intenziteta na zadnjem mestu
	bool binIntOld;  //binarna intenziteta na predzadnjem mestu
	bool foundOriginalBinInt;   //ima vrednost true, če smo že našli originalno binarno intenziteto 

	int nPixAfterTransition;
	float length, distance;
	float intensity, lastIbeforeTransition, firstIafterTransition;
	float stepPart;
	CPointFloat Told, Tcurrent, move;
	CPointFloat lastTbeforeTransition, firstTafterTransition;
	length = segment.p1.GetDistance(segment.p2);

	Tcurrent = CPointFloat(0, 0);
	lastTbeforeTransition = CPointFloat(0, 0);
	firstTafterTransition = CPointFloat(0, 0);

	move = p2 - p1;

	intensity = -1;

	if (blackToWhite)
	{
		originalBinInt = false;
		targetBinInt = true;
	}
	else
	{
		originalBinInt = true;
		targetBinInt = false;
	}


	nPixAfterTransition = 0;

	foundOriginalBinInt = false;

	for (distance = 0; distance <= length; distance += step)	//gremo od zaèetne do konène toèke linije v korakih step
	{

		Tcurrent = p1 + move * (distance / length);

		intensity = this->GetInterpolatedIntensity(Tcurrent.x, Tcurrent.y);

		if (intensity >= 0)	//s tem pogojem se izognemo primeru, ko je točka izven slike in GetInterpolatedIntensity vrne -1
		{
			if (intensity >= treshold) binInt = true;
			else binInt = false;

			if (binInt == originalBinInt) foundOriginalBinInt = true;

			if (binInt == targetBinInt)
			{
				if (!transitionRequired || foundOriginalBinInt)
				{
					if (nPixAfterTransition == 0)
					{
						firstTafterTransition = Tcurrent;	//zabeležimo prvo točko po prehodu
						firstIafterTransition = intensity;	//zabeležimo intenziteto prve točke po prehodu
					}

					nPixAfterTransition++;

					if (nPixAfterTransition * step >= minThickness) //našli smo rob, ki je debel vsaj minThickness
					{
						if (lastTbeforeTransition == CPointFloat(0, 0)) return firstTafterTransition;  //nismo našli nobene točke z originalno intenziteto, zato ne moremo interpolirati
						else
						{
							stepPart = (treshold - lastIbeforeTransition) / (firstIafterTransition - lastIbeforeTransition);
							return lastTbeforeTransition + (firstTafterTransition - lastTbeforeTransition) * stepPart;
						}
					}
				}

			}
			else
			{
				lastTbeforeTransition = Tcurrent;		//zabeležimo zadnjo točko pred prehodom
				lastIbeforeTransition = intensity;   //zabeležimo intenziteto zadnje točke pred prehodom
				nPixAfterTransition = 0;
			}

		}




	}

	return CPointFloat(0, 0);	//nismo našli prehoda
}


CPointFloat CMemoryBuffer::GetEdgePointOnSegment2D(CLine segment, bool forward, float step, bool blackToWhite, float treshold)
{
	return GetEdgePointOnSegment2D(segment, forward, step, blackToWhite, treshold, 1, true);
}

//Izraèuna krog iz težišèa in plošèine belih ali èrnih toèk v pravokotniku
/*CCircle CMemoryBuffer::GetCircle(CRectRotated rect, int whiteThreshold, int blackThreshold, bool whitePoints)
{
	int tmp;
	int ix, iy;
	int siv;
	int middleThreshold;
	float vsotakrx, vsotakry;

	CCircle circleTmp;

	middleThreshold = (whiteThreshold + blackThreshold) / 2;

	vsotakrx = 0;
	vsotakry = 0;
	circleTmp.area = 0;

	if (rect.top > height) rect.top = height;
	if (rect.bottom > height) rect.bottom = height;
	if (rect.top < 0) rect.top = 0;
	if (rect.bottom < 0) rect.top = 0;
	if (rect.left > width) rect.left = width;
	if (rect.right > width) rect.right = width;
	if (rect.left < 0) rect.left = 0;
	if (rect.right < 0) rect.left = 0;

	if (rect.top > rect.bottom)
	{
		tmp = rect.top;
		rect.top = rect.bottom;
		rect.bottom = tmp;
	}

	if (rect.left > rect.right)
	{
		tmp = rect.left;
		rect.left = rect.right;
		rect.right = tmp;
	}



	for (iy = rect.top; iy < rect.bottom; iy++)
	{
		for (ix = rect.left; ix < rect.right; ix++)
		{

			if (rect.InPolygon(ix, iy))
			{
				siv = GetAverageIntensity(ix, iy);

				if (whitePoints)
				{
					if (siv > middleThreshold)
					{
						//pDC->SetPixel(ix, iy, RGB(0,0,255));
						circleTmp.area++;
						vsotakrx = vsotakrx + ix;
						vsotakry = vsotakry + iy;
					}
				}
				else
				{
					if (siv < middleThreshold)
					{
						//pDC->SetPixel(ix, iy, RGB(0,0,255));
						circleTmp.area++;
						vsotakrx = vsotakrx + ix;
						vsotakry = vsotakry + iy;
					}
				}
			}
		}
	}

	if (circleTmp.area != 0)
	{
		//circleTmp.x =  vsotakrx/circleTmp.area;
		//circleTmp.y = vsotakry/circleTmp.area;
		circleTmp.x = vsotakrx / circleTmp.area;
		circleTmp.y = vsotakry / circleTmp.area;
		circleTmp.SetRadius(sqrt(circleTmp.area / M_PI));
	}
	else
	{
		circleTmp.x = 0;
		circleTmp.y = 0;

		//circleTmp.SetPointFloat(0, 0);
		circleTmp.SetRadius(0.0);
	}

	return circleTmp;
}

//CCircle CMemoryBuffer::GetCircle(CRectRotated polygon) //set circle from edges
{
	int x, y, counter;
	float sumX, sumY;

	CCircle circleTmp;
	sumX = 0;
	sumY = 0;
	counter = 0;
	if (isActiveEdges)
	{
		if (polygon.left >= 0 && polygon.top >= 0 && polygon.right < width && polygon.bottom < height)
		{
			//sesteje vse x in y tocke v poligonu
			for (x = polygon.left; x < polygon.right; x++)
			{
				for (y = polygon.top; y < polygon.bottom; y++)
				{
					if (polygon.InPolygon(x, y))
					{
						if (Xedges[y][x] != 0)
						{
							sumX += Xedges[y][x];

							if (Yedges[x][y] != 0)
							{
								sumY += Yedges[x][y];
								circleTmp.circlePoints.push_back(CPointFloat(Xedges[y][x], Yedges[x][y]));
							}
							else
							{
								sumY += y;
								circleTmp.circlePoints.push_back(CPointFloat(Xedges[y][x], y));
							}

							counter++;

						}
						else if (Yedges[x][y] != 0)
						{
							sumX += x;
							sumY += Yedges[x][y];
							circleTmp.circlePoints.push_back(CPointFloat(x, Yedges[x][y]));
							counter++;
						}
					}
				}
			}

			if (counter > 0)
			{
				circleTmp.SetPointFloat(sumX / counter, sumY / counter);
				circleTmp.SetRadius(circleTmp.GetDistance(circleTmp.circlePoints[0]));
				circleTmp.ComputeArea();
				circleTmp.ComputePerimeter();
			}
		}
	}
	return circleTmp;

}*/


int CMemoryBuffer::FindCommonPointsBlackFast(int threshold, int minPoints, QRect rect)
{
	int i, j;
	int ix, j4, j2, j1, counter;

	//napolnimo tabelo slika ki ni v kvadratu mora nujno biti crna
	//ce je rectangle enak velikosti, napolnimo crno samo 1px roba okrog slike
	CreateCommonPointsBlack(maxShapesBlack, maxPointsBlack);

	//ce so tabele alocirane
	if (isActiveCommonPointsBlack)
	{
		if (rect.top() <= 0)
			rect.setTop(1);

		if (rect.bottom() >= height)
			rect.setBottom(height- 1);

		if (rect.left() <= 0)
			rect.setLeft(1);

		if (rect.right() >= width)
			rect.setRight(width - 1);

		for (i = 0; i < height; i++)
		{
			for (j = 0; j < width; j++)
			{
				if ((i > rect.top()) && (i < rect.bottom()) && (j < rect.right()) && (j > rect.left()))
				{
					commonPointsBufferBlack[j][i] = GetAverageIntensity(j, i);
				}
				else
				{
					commonPointsBufferBlack[j][i] = 0;
				}

				activePointBlack[j][i] = 0;
			}
		}

		for (i = 0; i < maxShapesBlack; i++)
		{
			numberOfPointsBlack[i] = 0;
			for (j = 0; j < maxPointsBlack; j++)
			{
				commonPointsBlackX[i][j] = 0;
				commonPointsBlackY[i][j] = 0;

			}
		}

		nrShapesBlack = 0;
		j1 = 0;


		for (i = rect.top(); i < rect.bottom(); i++)
		{
			for (j = rect.left(); j < rect.right(); j++)
			{
				if (commonPointsBufferBlack[j][i] < threshold) //+
				{
					if (commonPointsBufferBlack[j - 1][i - 1] < threshold)//*
					{
						j2 = activePointBlack[j - 1][i - 1];

						if (j2 == 0)
							int bla = 0;
						AddNewBlackPoint(j, i, j2);

						if (commonPointsBufferBlack[j + 1][i - 1] < threshold) //+
						{
							j4 = activePointBlack[j + 1][i - 1];
							if (j4 != j2)
							{
								CopyBlackShape(j2, j4);
							}
						}
					}
					else
					{
						if (commonPointsBufferBlack[j][i - 1] < threshold)//+
						{
							j2 = activePointBlack[j][i - 1];//*
							if (j2 == 0)
								int bla = 0;
							AddNewBlackPoint(j, i, j2);

							if (commonPointsBufferBlack[j - 1][i] < threshold) //+
							{
								j4 = activePointBlack[j - 1][i]; //+
								if (j4 != j2)//+
								{
									CopyBlackShape(j2, j4);
								}
							}
						}
						else
						{
							if (commonPointsBufferBlack[j + 1][i - 1] < threshold) //+
							{
								j2 = activePointBlack[j + 1][i - 1];//+
								if (j2 == 0)
									int bla = 0;
								AddNewBlackPoint(j, i, j2);


								if (commonPointsBufferBlack[j - 1][i] < threshold)
								{
									j4 = activePointBlack[j - 1][i];
									if (j4 != j2)
									{
										CopyBlackShape(j2, j4);
									}
								}

							}
							else
							{
								if (commonPointsBufferBlack[j - 1][i] < threshold)
								{

									j2 = activePointBlack[j - 1][i];

									if (j2 == 0)
										int bla = 0;
									AddNewBlackPoint(j, i, j2);
								}
								else
								{
									if (FindFreeBlackShape() < maxShapesBlack)
									{
										AddNewBlackPoint(j, i, nrShapesBlack);

									}
								}
							}
						}
					}
				}
			}
		}
		nrShapesBlack = 0;
		for (ix = 1; ix < maxShapesBlack; ix++)
		{
			if (numberOfPointsBlack[ix] < minPoints)
			{
				numberOfPointsBlack[ix] = 0;
			}
			else
				nrShapesBlack++;
		}
	}

	return nrShapesBlack;
}

int CMemoryBuffer::FindCommonPointsWhiteFastBoundingBox(int threshold, QRect rect)
{
	int i, j;
	int ix, j4, j2, j1, counter, tmp;

	// poisce se prilegajoci kvadrat
	//napolnimo tabelo slika ki ni v kvadratu mora nujno biti crna
	//ce je rectangle enak velikosti, napolnimo crno samo 1px roba okrog slike

	//ce so tabele alocirane
	if (isActiveCommonPointsWhite)
	{
		if (rect.top() <= 0)
			rect.setTop(1);

		if (rect.bottom() >= height)
			rect.setBottom(height - 1);

		if (rect.left() <= 0)
			rect.setLeft(1);

		if (rect.right() >= width)
			rect.setRight(width - 1);

		if (rect.right() < rect.left())
		{
			tmp = rect.right();
			rect.setRight(rect.left());
			rect.setLeft(tmp);
		}

		if (rect.bottom() < rect.top())
		{
			tmp = rect.top();
			rect.setTop(rect.bottom());
			rect.setBottom(tmp);
		}

		for (i = 0; i < height; i++)
		{
			for (j = 0; j < width; j++)
			{
				if ((i > rect.top()) && (i < rect.bottom()) && (j < rect.right()) && (j > rect.left()))
				{
					commonPointsBufferWhite[j][i] = GetAverageIntensity(j, i);
				}
				else
				{
					commonPointsBufferWhite[j][i] = 0;
				}

				activePointWhite[j][i] = 0;
			}
		}

		for (i = 0; i < maxShapesWhite; i++)
		{
			boundingBoxWhite[i].setCoords(10000, 10000, 0, 0);
			numberOfPointsWhite[i] = 0;
			centerWhite[i].SetPointFloat(0, 0);
			centerWhite[i].SetActive(false);

			for (j = 0; j < maxPointsWhite; j++)
			{
				commonPointsWhiteX[i][j] = 0;
				commonPointsWhiteY[i][j] = 0;

			}
		}

		nrShapesWhite = 0;
		j1 = 0;



		for (i = rect.top(); i < rect.bottom(); i++)
		{
			for (j = rect.left(); j < rect.right(); j++)
			{
				if (commonPointsBufferWhite[j][i] > threshold) //+
				{
					if (commonPointsBufferWhite[j - 1][i - 1] > threshold)//*
					{
						j2 = activePointWhite[j - 1][i - 1];

						if (j2 == 0)
							int bla = 0;
						AddNewWhitePoint(j, i, j2);

						if (commonPointsBufferWhite[j + 1][i - 1] > threshold) //+
						{
							j4 = activePointWhite[j + 1][i - 1];
							if (j4 != j2)
							{
								CopyWhiteShape(j2, j4);
							}
						}
					}
					else
					{
						if (commonPointsBufferWhite[j][i - 1] > threshold)//+
						{
							j2 = activePointWhite[j][i - 1];//*
							if (j2 == 0)
								int bla = 0;
							AddNewWhitePoint(j, i, j2);

							if (commonPointsBufferWhite[j - 1][i] > threshold) //+
							{
								j4 = activePointWhite[j - 1][i]; //+
								if (j4 != j2)//+
								{
									CopyWhiteShape(j2, j4);
								}
							}
						}
						else
						{
							if (commonPointsBufferWhite[j + 1][i - 1] > threshold) //+
							{
								j2 = activePointWhite[j + 1][i - 1];//+
								if (j2 == 0)
									int bla = 0;
								AddNewWhitePoint(j, i, j2);


								if (commonPointsBufferWhite[j - 1][i] > threshold)
								{
									j4 = activePointWhite[j - 1][i];
									if (j4 != j2)
									{
										CopyWhiteShape(j2, j4);
									}
								}

							}
							else
							{
								if (commonPointsBufferWhite[j - 1][i] > threshold)
								{

									j2 = activePointWhite[j - 1][i];

									if (j2 == 0)
										int bla = 0;
									AddNewWhitePoint(j, i, j2);
								}
								else
								{
									if (FindFreeWhiteShape() < maxShapesWhite)
									{
										AddNewWhitePoint(j, i, nrShapesWhite);

									}
								}
							}
						}
					}
				}
			}
		}
	}

	return nrShapesWhite;
}
int CMemoryBuffer::FindCommonPointsBlackFastBoundingBox(int threshold, QRect rect)
{
	int i, j, tmp;
	int ix, j4, j2, j1, counter;

	// poisce se prilegajoci kvadrat
	//napolnimo tabelo. slika, ki ni v kvadratu, mora nujno biti bela
	//ce je rectangle enak velikosti, napolnimo belo samo 1px roba okrog slike
	//CreateCommonPointsBlack(MAX_SHAPES, MAX_POINTS);
	//ce so tabele alocirane
	if (isActiveCommonPointsBlack)
	{
		if (rect.top() <= 0)
			rect.setTop(1);

		if (rect.bottom() >= height)
			rect.setBottom(height - 1);

		if (rect.left() <= 0)
			rect.setLeft(1);

		if (rect.right() >= width)
			rect.setRight(width - 1);

		if (rect.right() < rect.left())
		{
			tmp = rect.right();
			rect.setRight(rect.left());
			rect.setLeft(tmp);
		}

		if (rect.bottom() < rect.top())
		{
			tmp = rect.top();
			rect.setTop(rect.bottom());
			rect.setBottom(tmp);
		}


		int pozicija = 0; //= y * pSlika->width + x;

		int intenziteta = 0;// = pSlika->buffer->data[pozicija];

		uchar* pDataStart;

		pDataStart = &(buffer->data[0]);

		uchar* pPixel = 0;
		int I = 0;

/*for (i = rect.top() -2; i < rect.bottom() +2; i++)
{
	pPixel = pDataStart + i * width + rect.left() - 2;
	for (j =  rect.left()-2; j < rect.right() +2; j++)
	{
		if ((j >= 0) && (i >= 0))
		{
			if ((i > rect.top()) && (i < rect.bottom()) && (j < rect.right()) && (j > rect.left()))
			{
				commonPointsBufferBlack[j][i] = *pPixel;
			}
			else
			{
				commonPointsBufferBlack[j][i] = 255;
			}
			activePointBlack[j][i] = 0;
		}
		pPixel++;
	}
	*
}*/

for (i = rect.top() -2; i < rect.bottom() +2; i++)
{
	for (j =  rect.left()-2; j < rect.right() +2; j++)
	{
		if ((j >= 0) && (i >= 0))
		{
			if ((i > rect.top()) && (i < rect.bottom()) && (j < rect.right()) && (j > rect.left()))
			{
				commonPointsBufferBlack[j][i] = GetAverageIntensity(j, i);
			}
			else
			{
				commonPointsBufferBlack[j][i] = 255;
			}
			activePointBlack[j][i] = 0;
		}
		pPixel++;
	}
	
}


		for (i = 0; i < maxShapesBlack; i++)
		{
			boundingBoxBlack[i].setCoords(10000, 10000, 0, 0);
			numberOfPointsBlack[i] = 0;
			centerBlack[i].SetPointFloat(0, 0);
			centerBlack[i].SetActive(false);

			for (j = 0; j < maxPointsBlack; j++)
			{
				commonPointsBlackX[i][j] = 0;
				commonPointsBlackX[i][j] = 0 ;
			}
		}

		nrShapesBlack = 0;
		j1 = 0;


		for (i = rect.top(); i < rect.bottom(); i++)
		{
			for (j = rect.left(); j < rect.right(); j++)
			{
				if (commonPointsBufferBlack[j][i] < threshold) //+
				{
					if (commonPointsBufferBlack[j - 1][i - 1] < threshold)//*
					{
						j2 = activePointBlack[j - 1][i - 1];

						AddNewBlackPoint(j, i, j2);

						if (commonPointsBufferBlack[j + 1][i - 1] < threshold) //+
						{
							j4 = activePointBlack[j + 1][i - 1];
							if (j4 != j2)
							{
								CopyBlackShape(j2, j4);
							}
						}
					}
					else
					{
						if (commonPointsBufferBlack[j][i - 1] < threshold)//+
						{
							j2 = activePointBlack[j][i - 1];
							AddNewBlackPoint(j, i, j2);

							if (commonPointsBufferBlack[j - 1][i] < threshold) //+
							{
								j4 = activePointBlack[j - 1][i]; //+
								if (j4 != j2)//+
								{
									CopyBlackShape(j2, j4);
								}
							}
						}
						else
						{
							if (commonPointsBufferBlack[j + 1][i - 1] < threshold) //+
							{
								j2 = activePointBlack[j + 1][i - 1];//+

								AddNewBlackPoint(j, i, j2);


								if (commonPointsBufferBlack[j - 1][i] < threshold)
								{
									j4 = activePointBlack[j - 1][i];
									if (j4 != j2)
									{
										CopyBlackShape(j2, j4);
									}
								}

							}
							else
							{
								if (commonPointsBufferBlack[j - 1][i] < threshold)
								{

									j2 = activePointBlack[j - 1][i];

									AddNewBlackPoint(j, i, j2);
								}
								else
								{
									if (FindFreeBlackShape() < maxShapesBlack)
									{
										AddNewBlackPoint(j, i, nrShapesBlack);

									}
								}
							}
						}
					}
				}
			}
		}
	}

	return nrShapesBlack;
}
int CMemoryBuffer::FindCommonPointsBlackFastBoundingBoxRedused(int threshold, QRect rect,int step)
{
	int i, j, tmp;
	int ix, j4, j2, j1, counter;

	// poisce se prilegajoci kvadrat
	//napolnimo tabelo. slika, ki ni v kvadratu, mora nujno biti bela
	//ce je rectangle enak velikosti, napolnimo belo samo 1px roba okrog slike
	//CreateCommonPointsBlack(MAX_SHAPES, MAX_POINTS);
	//ce so tabele alocirane
	if (isActiveCommonPointsBlack)
	{
		if (rect.top() <= 0)
			rect.setTop(1);

		if (rect.bottom() >= height)
			rect.setBottom(height - 1);

		if (rect.left() <= 0)
			rect.setLeft(1);

		if (rect.right() >= width)
			rect.setRight(width - 1);

		if (rect.right() < rect.left())
		{
			tmp = rect.right();
			rect.setRight(rect.left());
			rect.setLeft(tmp);
		}

		if (rect.bottom() < rect.top())
		{
			tmp = rect.top();
			rect.setTop(rect.bottom());
			rect.setBottom(tmp);
		}


		int pozicija = 0; //= y * pSlika->width + x;

		int intenziteta = 0;// = pSlika->buffer->data[pozicija];

		uchar* pDataStart;

		pDataStart = &(buffer->data[0]);

		uchar* pPixel = 0;
		int I = 0;
		int inte = 0;

		for (i = rect.top() - step; i < rect.bottom() + step; i+=step)
		{
			//pPixel = pDataStart + i * width + rect.left() - step;
			for (j = rect.left() - step; j < rect.right() + step; j+=step)
			{
				if ((j >= 0) && (i >= 0) && (i < rect.bottom()) && (j < rect.right()))
				{
					if ((i > rect.top()) && (i < rect.bottom()) && (j < rect.right()) && (j > rect.left()))
					{
						//commonPointsBufferBlack[j][i] = *pPixel;
						commonPointsBufferBlack[j][i] = GetAverageIntensity(j, i);
					}
					else
					{
						commonPointsBufferBlack[j][i] = 255;
					}
					activePointBlack[j][i] = 0;
				}
				//pPixel++;
			}

		}
		for (i = 0; i < maxShapesBlack; i++)
		{
			boundingBoxBlack[i].setCoords(10000, 10000, 0, 0);
			numberOfPointsBlack[i] = 0;
			centerBlack[i].SetPointFloat(0, 0);
			centerBlack[i].SetActive(false);

			for (j = 0; j < maxPointsBlack; j++)
			{
				commonPointsBlackX[i][j] = 0;
				commonPointsBlackX[i][j] = 0;
			}
		}

		nrShapesBlack = 0;
		j1 = 0;


		for (i = rect.top(); i < rect.bottom(); i+=step)
		{
			for (j = rect.left(); j < rect.right(); j+=step)
			{
				if (commonPointsBufferBlack[j][i] < threshold) //+
				{
					
						if (commonPointsBufferBlack[j][i - step] < threshold)//+
						{
							j2 = activePointBlack[j][i - step];
							AddNewBlackPoint(j, i, j2);

							if (commonPointsBufferBlack[j - step][i] < threshold) //+
							{
								j4 = activePointBlack[j - step][i]; //+
								if (j4 != j2)//+
								{
									CopyBlackShape(j2, j4);
								}
							}
						}
						else
						{

							if (commonPointsBufferBlack[j - step][i] < threshold)
							{

								j2 = activePointBlack[j - step][i];

								AddNewBlackPoint(j, i, j2);
							
								if (commonPointsBufferBlack[j - step][i] < threshold)
								{
									j4 = activePointBlack[j - step][i];
									if (j4 != j2)
									{
										CopyBlackShape(j2, j4);
									}
								}

							}
							else
							{
								
								
							if (FindFreeBlackShape() < maxShapesBlack)
							{
								AddNewBlackPoint(j, i, nrShapesBlack);

							}
								
							}
						}

				}
			}
		}
	}

	return nrShapesBlack;
}

int CMemoryBuffer::FilterWhiteShapesBySize(int minWidth, int maxWidth, int minHeight, int maxHeight)
{
	int i, maxShape = -1;
	nrShapesWhite = 0;

	for (i = 0; i < maxShapesWhite; i++)
	{
		if (numberOfPointsWhite[i] > 0)
		{
			//dodaj primerjavo èe je primerne velikosti!!!
			if ((boundingBoxWhite[i].width() < minWidth) || (boundingBoxWhite[i].width() > maxWidth) || (boundingBoxWhite[i].height() < minHeight) || (boundingBoxWhite[i].height() > maxHeight))
			{
				numberOfPointsWhite[i] = 0;
				boundingBoxWhite[i].setCoords(10000, 10000, 0, 0);
				centerWhite[i].SetPointFloat(0, 0);
				centerWhite[i].SetActive(false);
			}
			else
			{
				if (maxShape < numberOfPointsWhite[i])
				{
					maxShape = i;
				}

				nrShapesWhite++;

				if (!centerWhite[i].IsActive()) //ce ni se ni nastavljen center, ga nastavimo
				{
					centerWhite[i].x /= numberOfPointsWhite[i];
					centerWhite[i].y /= numberOfPointsWhite[i];
					centerWhite[i].SetActive(true);
				}
			}
		}
	}
	return maxShape;
}

int CMemoryBuffer::FilterWhiteShapesBySize(int minSize, int maxSize)
{
	int i, maxShape = -1;
	nrShapesWhite = 0;

	for (i = 0; i < maxShapesWhite; i++)
	{
		if (numberOfPointsWhite[i] > 0)
		{
			//sirina manjsa kot minSize in visina vecja kot maxSize (ozka navpicna crta)     ||    sirina vecja kot maxSize in dolzina manjsa kot minsize (ozka vodoravna crta)  
			if (((boundingBoxWhite[i].width() < minSize) && (boundingBoxWhite[i].height() > maxSize)) || ((boundingBoxWhite[i].height() < minSize) && (boundingBoxWhite[i].width() > maxSize)))
			{
				numberOfPointsWhite[i] = 0;
				boundingBoxWhite[i].setCoords(10000, 10000, 0, 0);
				centerWhite[i].SetPointFloat(0, 0);
				centerWhite[i].SetActive(false);
			}
			else
			{
				if (maxShape < numberOfPointsWhite[i])
				{
					maxShape = i;
				}
				nrShapesWhite++;
				if (!centerWhite[i].IsActive()) //ce ni se ni nastavljen center, ga nastavimo
				{
					centerWhite[i].x /= numberOfPointsWhite[i];
					centerWhite[i].y /= numberOfPointsWhite[i];
					centerWhite[i].SetActive(true);
				}
			}
		}
	}
	return maxShape;
}

int CMemoryBuffer::FilterWhiteShapesByArea(int minSize, int maxSize) //vrne indeks najvecje ploskve
{
	int i, maxShape = -1;

	nrShapesWhite = 0;

	for (i = 0; i < maxShapesWhite; i++)
	{
		if (numberOfPointsWhite[i] > 0)
		{
			if ((numberOfPointsWhite[i] < minSize) || (numberOfPointsWhite[i] > maxSize))
			{
				numberOfPointsWhite[i] = 0;
				boundingBoxWhite[i].setCoords(10000, 10000, 0, 0);
				centerWhite[i].SetPointFloat(0, 0);
				centerWhite[i].SetActive(false);
			}
			else
			{
				if (maxShape < numberOfPointsWhite[i])
				{
					maxShape = i;
				}
				nrShapesWhite++;
				if (!centerWhite[i].IsActive()) //ce ni se ni nastavljen center, ga nastavimo
				{
					centerWhite[i].x /= numberOfPointsWhite[i];
					centerWhite[i].y /= numberOfPointsWhite[i];
					centerWhite[i].SetActive(true);
				}
			}
		}
	}
	return maxShape;
}

int CMemoryBuffer::FilterWhiteShapesInPolygon(std::vector<CRectRotated> polygons)
{
	int i, j, maxShape = -1;
	bool inPolygon = false;
	nrShapesWhite = 0;

	for (i = 0; i < maxShapesWhite; i++)
	{
		if (numberOfPointsWhite[i] > 0)
		{
			inPolygon = false;
			if (!centerWhite[i].IsActive()) //ce ni se ni nastavljen center, ga nastavimo
			{
				centerWhite[i].x /= numberOfPointsWhite[i];
				centerWhite[i].y /= numberOfPointsWhite[i];
				centerWhite[i].SetActive(true);
			}
			for (j = 0; j < polygons.size(); j++)
			{
				if (polygons[j].InPolygon(centerWhite[i]))
				{
					//mora biti znotraj poligona
					if (polygons[j].InPolygon(boundingBoxWhite[i].left(), boundingBoxWhite[i].top()) && polygons[j].InPolygon(boundingBoxWhite[i].right(), boundingBoxWhite[i].bottom()))
					{
						numberOfPointsWhite[i] = 0;
						boundingBoxWhite[i].setCoords(10000, 10000, 0, 0);

						centerWhite[i].SetPointFloat(0, 0);
						centerWhite[i].SetActive(false);
						inPolygon = true;
						break;
					}
				}
			}

			if (!inPolygon)
			{
				if (maxShape < numberOfPointsWhite[i])
				{
					maxShape = i;
				}
				nrShapesWhite++;

			}
		}
	}
	return maxShape;
}

int CMemoryBuffer::FilterBlackShapesBySize(int minWidth, int maxWidth, int minHeight, int maxHeight)
{
	//filtrira in jih razoredi po vrsti
	int i, maxShape = -1;
	nrShapesBlack = 0;

	for (i = 0; i < maxShapesBlack; i++)
	{
		if (numberOfPointsBlack[i] > 0)
		{
			//dodaj primerjavo èe je primerne velikosti!!!
			if ((boundingBoxBlack[i].width() < minWidth) || (boundingBoxBlack[i].width() > maxWidth) || (boundingBoxBlack[i].height() < minHeight) || (boundingBoxBlack[i].height() > maxHeight))
			{

				numberOfPointsBlack[i] = 0;
				boundingBoxBlack[i].setCoords(10000, 10000, 0, 0);
				centerBlack[i].SetPointFloat(0, 0);
				centerBlack[i].SetActive(false);
			}
			else
			{
				if (maxShape < numberOfPointsBlack[i])
				{
					maxShape = i;
				}


				nrShapesBlack++;
				if (!centerBlack[i].IsActive())
				{
					centerBlack[i].x /= numberOfPointsBlack[i];
					centerBlack[i].y /= numberOfPointsBlack[i];
					centerBlack[i].SetActive(true);
				}
			}
		}
	}

	return maxShape;
}

int CMemoryBuffer::FilterBlackShapesBySize(int minSize, int maxSize)
{
	//filtrira in jih razoredi po vrsti
	int i, maxShape = -1;
	nrShapesBlack = 0;

	for (i = 0; i < maxShapesBlack; i++)
	{
		if (numberOfPointsBlack[i] > 0)
		{
			//sirina manjsa kot minSize in visina vecja kot maxSize (ozka navpicna crta)     ||    sirina vecja kot maxSize in dolzina manjsa kot minsize (ozka vodoravna crta)  
			if (((boundingBoxBlack[i].width() < minSize) && (boundingBoxBlack[i].height() > maxSize)) || ((boundingBoxBlack[i].height() < minSize) && (boundingBoxBlack[i].width() > maxSize)))
			{
				numberOfPointsBlack[i] = 0;
				boundingBoxBlack[i].setCoords(10000, 10000, 0, 0);
				centerBlack[i].SetPointFloat(0, 0);
				centerBlack[i].SetActive(false);
			}
			else
			{
				if (maxShape < numberOfPointsBlack[i])
				{
					maxShape = i;
				}


				nrShapesBlack++;
				if (!centerBlack[i].IsActive())
				{
					centerBlack[i].x /= numberOfPointsBlack[i];
					centerBlack[i].y /= numberOfPointsBlack[i];
					centerBlack[i].SetActive(true);
				}
			}
		}
	}

	return maxShape;
}

int CMemoryBuffer::FilterBlackShapesByArea(int minSize, int maxSize)
{
	int i, maxShape = -1;
	nrShapesBlack = 0;

	for (i = 0; i < maxShapesBlack; i++)
	{
		if (numberOfPointsBlack[i] > 0)
		{
			if ((numberOfPointsBlack[i] < minSize) || (numberOfPointsBlack[i] > maxSize))
			{
				centerBlack[i].SetPointFloat(0, 0);
				numberOfPointsBlack[i] = 0;
				boundingBoxBlack[i].setCoords(10000, 10000, 0, 0);
				centerBlack[i].SetActive(false);
			}
			else
			{
				if (maxShape < numberOfPointsBlack[i])
					maxShape = numberOfPointsBlack[i];

				nrShapesBlack++;
				if (!centerBlack[i].IsActive())
				{
					centerBlack[i].x /= numberOfPointsBlack[i];
					centerBlack[i].y /= numberOfPointsBlack[i];
					centerBlack[i].SetActive(true);
				}
			}
		}
	}

	return maxShape;
}
int CMemoryBuffer::FilterBlackShapesInPolygon(std::vector<CRectRotated> polygons)
{
	int i, j, maxShape = -1;
	bool inPolygon = false;
	nrShapesBlack = 0;

	for (i = 0; i < maxShapesBlack; i++)
	{
		if (numberOfPointsBlack[i] > 0)
		{
			inPolygon = false;
			if (!centerBlack[i].IsActive())
			{
				centerBlack[i].x /= numberOfPointsBlack[i];
				centerBlack[i].y /= numberOfPointsBlack[i];
				centerBlack[i].SetActive(true);
			}
			for (j = 0; j < polygons.size(); j++)
			{
				if (polygons[j].InPolygon(centerBlack[i]))
				{
					if (polygons[j].InPolygon(boundingBoxBlack[i].left(), boundingBoxBlack[i].top()) && polygons[j].InPolygon(boundingBoxBlack[i].right(), boundingBoxBlack[i].bottom()))
					{
						numberOfPointsBlack[i] = 0;
						boundingBoxBlack[i].setCoords(10000, 10000, 0, 0);
						centerBlack[i].SetPointFloat(0, 0);
						centerBlack[i].SetActive(false);
						inPolygon = true;
						break;
					}
				}
			}

			if (!inPolygon)
			{
				if (maxShape < numberOfPointsBlack[i])
				{
					maxShape = i;
				}
				nrShapesBlack++;

			}
		}
	}
	return maxShape;
}
int  CMemoryBuffer::FindFreeWhiteShape()
{
	//poisce prosto ploskev
	//poisce prosto ploskev
	int counter;

	counter = 0;
	//pregledamo vse ploskve, ce so polne oz naslednja prazna
	do
	{
		nrShapesWhite++;
		counter++;

		if (nrShapesWhite >= (maxShapesWhite - 1))		//OPOMBA MARTIN: napaka?  nrShapesWhite > (maxShapesWhite - 1)  ?
			nrShapesWhite = 1;

	} while ((numberOfPointsWhite[nrShapesWhite] != 0) && (counter < maxShapesWhite));

	//ce so vse ploskve polne se preverimo, ce je slucajno kaksna premajhna, ce je jo izbrisemo
	if (counter >= maxShapesWhite)
	{
		for (int ix = 1; ix < maxShapesWhite; ix++)
		{
			if (numberOfPointsWhite[ix] < 10)
			{
				numberOfPointsWhite[ix] = 0;
				centerWhite[ix].SetPointFloat(0, 0);
				nrShapesWhite = ix;
				boundingBoxWhite[ix].setCoords(10000, 10000, 0, 0);
				counter = 0; //postavimo na manjse od max shapes, 
				break;
			}
		}
	}

	return counter;
}
int  CMemoryBuffer::FindFreeBlackShape()
{
	//poisce prosto ploskev
	//poisce prosto ploskev
	int counter;

	counter = 0;
	//pregledamo vse ploskve, ce so polne oz naslednja prazna
	do
	{
		nrShapesBlack++;
		counter++;

		if (nrShapesBlack >= (maxShapesBlack - 1))
			nrShapesBlack = 1;

	} while ((numberOfPointsBlack[nrShapesBlack] != 0) && (counter < maxShapesBlack));

	//ce so vse ploskve polne se preverimo, ce je slucajno kaksna premajhna, ce je jo izbrisemo
	if (counter >= maxShapesBlack)
	{
		for (int ix = 1; ix < maxShapesBlack; ix++)
		{
			if (numberOfPointsBlack[ix] < 10)
			{
				numberOfPointsBlack[ix] = 0;
				nrShapesBlack = ix;
				centerBlack[ix].SetPointFloat(0, 0);
				boundingBoxBlack[ix].setCoords(10000, 10000, 0, 0);
				counter = 0; //postavimo na manjse od max shapes, 
				break;
			}
		}
	}

	return counter;
}

int CMemoryBuffer::JoinCommonBlackShapes(double maxDistance)
{
	int i, j, joints = 0, maxHeight, maxWidth;


	for (i = 0; i < maxShapesBlack; i++)
	{
		if (numberOfPointsBlack[i] > 0)
		{
			for (j = 1; j < maxShapesBlack; j++)
			{
				if ((j != i) && (numberOfPointsBlack[j] > 0))
					//sirina manjsa kot minSize in visina vecja kot maxSize (ozka navpicna crta)     ||    sirina vecja kot maxSize in dolzina manjsa kot minsize (ozka vodoravna crta)  
				{
					if ((abs(boundingBoxBlack[i].left() - boundingBoxBlack[j].right()) < maxDistance) || (abs(boundingBoxBlack[i].right() - boundingBoxBlack[j].left()) < maxDistance))
					{
						if ((abs(boundingBoxBlack[i].top() - boundingBoxBlack[j].top()) < maxDistance) || (abs(boundingBoxBlack[i].top() - boundingBoxBlack[j].bottom()) < maxDistance) || (abs(boundingBoxBlack[i].bottom() - boundingBoxBlack[j].top()) < maxDistance) || (abs(boundingBoxBlack[i].bottom() - boundingBoxBlack[j].bottom()) < maxDistance))
						{
							CopyBlackShape(i, j);
							joints++;
						}
						else if ((boundingBoxBlack[j].top() > boundingBoxBlack[i].top()) && (boundingBoxBlack[j].top() < boundingBoxBlack[i].bottom()))
						{
							CopyBlackShape(i, j);
							joints++;
						}
					}
					else if ((abs(boundingBoxBlack[i].top() - boundingBoxBlack[j].bottom()) < maxDistance) || (abs(boundingBoxBlack[i].bottom() - boundingBoxBlack[j].top()) < maxDistance))
					{
						if ((abs(boundingBoxBlack[i].right() - boundingBoxBlack[j].right()) < maxDistance) || (abs(boundingBoxBlack[i].left() - boundingBoxBlack[j].left()) < maxDistance) || (abs(boundingBoxBlack[i].left() - boundingBoxBlack[j].right()) < maxDistance) || (abs(boundingBoxBlack[i].right() - boundingBoxBlack[j].left()) < maxDistance))
						{
							CopyBlackShape(i, j);
							joints++;
						}
						else if ((boundingBoxBlack[j].left() > boundingBoxBlack[i].left()) && (boundingBoxBlack[j].left() < boundingBoxBlack[i].right()))
						{
							CopyBlackShape(i, j);
							joints++;
						}
					}
				}
			}

		}
	}
	return joints;
}


bool CMemoryBuffer::AddNewWhitePoint(int x, int y, int shapeNumber)
{
	//x .... opazovani x
	//y .... opazovani y
	int counter = 0;
	if (numberOfPointsWhite[shapeNumber] > (maxPointsWhite - 1))
	{
		counter = FindFreeWhiteShape();
		shapeNumber = nrShapesWhite;
	}

	if (counter < maxShapesWhite)
	{
		commonPointsWhiteX[shapeNumber][numberOfPointsWhite[shapeNumber]] = x;
		commonPointsWhiteY[shapeNumber][numberOfPointsWhite[shapeNumber]] = y;

		if (boundingBoxWhite[shapeNumber].left() > x)
		{
			boundingBoxWhite[shapeNumber].setLeft(x);
			commonEdgePointsWhite[shapeNumber][0].SetPointFloat(x, y);
		}
		int right = boundingBoxWhite[shapeNumber].right();
		QRect test;
		
		test.setRect(10, 10, 20, 20);
		test.setBottom(20);

		int bottom = test.bottom();
		int left = test.left();

		if (boundingBoxWhite[shapeNumber].right() < x)
		{
			boundingBoxWhite[shapeNumber].setRight(x);
			commonEdgePointsWhite[shapeNumber][1].SetPointFloat(x, y);

		}
		if (boundingBoxWhite[shapeNumber].top() > y)
		{
			boundingBoxWhite[shapeNumber].setTop(y);
			commonEdgePointsWhite[shapeNumber][2].SetPointFloat(x, y);

		}

		if (boundingBoxWhite[shapeNumber].bottom() < y)
		{
			boundingBoxWhite[shapeNumber].setBottom(y);
			commonEdgePointsWhite[shapeNumber][3].SetPointFloat(x, y);

		}

		centerWhite[shapeNumber].x += x;
		centerWhite[shapeNumber].y += y;

		numberOfPointsWhite[shapeNumber]++;
		activePointWhite[x][y] = shapeNumber;
	}

	return true;

	//return false;
}

bool CMemoryBuffer::AddNewBlackPoint(int x, int y, int shapeNumber)
{
	//x .... opazovani x
	//y .... opazovani y
	int counter = 0;
	if (numberOfPointsBlack[shapeNumber] > (maxPointsBlack - 1))
	{
		counter = FindFreeBlackShape();
		shapeNumber = nrShapesBlack;
	}

	if (counter < maxShapesBlack)
	{
		commonPointsBlackX[shapeNumber][numberOfPointsBlack[shapeNumber]] = x;//+
		commonPointsBlackY[shapeNumber][numberOfPointsBlack[shapeNumber]] = y;//+

		if (boundingBoxBlack[shapeNumber].left() > x)
		{
			boundingBoxBlack[shapeNumber].setLeft(x);
			commonEdgePointsBlack[shapeNumber][0].SetPointFloat(x, y);

		}

		if (boundingBoxBlack[shapeNumber].right() < x)
		{
			boundingBoxBlack[shapeNumber].setRight(x);
			commonEdgePointsBlack[shapeNumber][1].SetPointFloat(x, y);

		}

		if (boundingBoxBlack[shapeNumber].top() > y)
		{
			boundingBoxBlack[shapeNumber].setTop(y);
			commonEdgePointsBlack[shapeNumber][2].SetPointFloat(x, y);

		}

		if (boundingBoxBlack[shapeNumber].bottom() < y)
		{
			boundingBoxBlack[shapeNumber].setBottom(y);
			commonEdgePointsBlack[shapeNumber][3].SetPointFloat(x, y);

		}

		centerBlack[shapeNumber].x += x;
		centerBlack[shapeNumber].y += y;

		numberOfPointsBlack[shapeNumber]++;
		activePointBlack[x][y] = shapeNumber;
	}

	return true;

	//return false;
}

void CMemoryBuffer::CopyWhiteShape(int shapeNew, int shapeOld)
{
	int numOfPointsSh1, numOfPointsSh2, numOffreePoints, ix;
	int shape1, shape2;


	if (numberOfPointsWhite[shapeOld] > numberOfPointsWhite[shapeNew]) //prepisemo nove tocke k starim, ker jih je manj
	{
		shape1 = shapeOld;
		shape2 = shapeNew;
	}
	else //prepisemo stare tocke k novim, ker jih je manj
	{
		shape1 = shapeNew;
		shape2 = shapeOld;
	}

	if ((numberOfPointsWhite[shapeOld] + numberOfPointsWhite[shapeNew]) > (maxPointsWhite - 1))
	{
		numOffreePoints = maxPointsWhite - numberOfPointsWhite[shape1]; //koliko je se prostgih
		numOfPointsSh2 = numberOfPointsWhite[shape2];
	}
	else
	{
		numOffreePoints = numberOfPointsWhite[shape2];
		numOfPointsSh2 = 0;
	}


	for (ix = 0; ix < numOffreePoints; ix++)
	{
		commonPointsWhiteX[shape1][numberOfPointsWhite[shape1]] = commonPointsWhiteX[shape2][ix];
		commonPointsWhiteY[shape1][numberOfPointsWhite[shape1]] = commonPointsWhiteY[shape2][ix];
		numberOfPointsWhite[shape1]++;
		activePointWhite[(int)commonPointsWhiteX[shape2][ix]][(int)commonPointsWhiteY[shape2][ix]] = shape1;

		if (boundingBoxWhite[shape1].left() > commonPointsWhiteX[shape2][ix])
			boundingBoxWhite[shape1].setLeft(commonPointsWhiteX[shape2][ix]);

		if (boundingBoxWhite[shape1].right() < commonPointsWhiteX[shape2][ix])
			boundingBoxWhite[shape1].setRight(commonPointsWhiteX[shape2][ix]);

		if (boundingBoxWhite[shape1].top() > commonPointsWhiteY[shape2][ix])
			boundingBoxWhite[shape1].setTop(commonPointsWhiteY[shape2][ix]);

		if (boundingBoxWhite[shape1].bottom() < commonPointsWhiteY[shape2][ix])
			boundingBoxWhite[shape1].setBottom(commonPointsWhiteY[shape2][ix]);

		centerWhite[shape1].x += commonPointsWhiteX[shape2][ix];
		centerWhite[shape1].y += commonPointsWhiteY[shape2][ix];
	}

	numberOfPointsWhite[shape2] = 0;
	boundingBoxWhite[shape2].setCoords(10000, 10000, 0, 0);
	centerWhite[shape2].SetPointFloat(0, 0);
	for (ix = numOffreePoints; ix < numOfPointsSh2; ix++)
	{
		//stevilo neskopiranih, prepisemo jih povrsti
		commonPointsWhiteX[shape2][numberOfPointsWhite[shape2]] = commonPointsWhiteX[shape2][ix];
		commonPointsWhiteY[shape2][numberOfPointsWhite[shape2]] = commonPointsWhiteY[shape2][ix];

		if (boundingBoxWhite[shape2].left() > commonPointsWhiteX[shape2][ix])
			boundingBoxWhite[shape2].setLeft( commonPointsWhiteX[shape2][ix]);

		if (boundingBoxWhite[shape2].right() < commonPointsWhiteX[shape2][ix])
			boundingBoxWhite[shape2].setRight(commonPointsWhiteX[shape2][ix]);

		if (boundingBoxWhite[shape2].top() > commonPointsWhiteY[shape2][ix])
			boundingBoxWhite[shape2].setTop(commonPointsWhiteY[shape2][ix]);

		if (boundingBoxWhite[shape2].bottom() < commonPointsWhiteY[shape2][ix])
			boundingBoxWhite[shape2].setBottom( commonPointsWhiteY[shape2][ix]);

		centerWhite[shape2].x += commonPointsWhiteX[shape2][ix];
		centerWhite[shape2].y += commonPointsWhiteY[shape2][ix];
		//activePointWhite ostane shape2
		numberOfPointsWhite[shape2]++;
	}

}

void CMemoryBuffer::CopyBlackShape(int shapeNew, int shapeOld)
{
	//staro ploskev izbrisemo
	int numOfPointsSh1, numOfPointsSh2, numOffreePoints, ix;
	int shape1, shape2;


	if (numberOfPointsBlack[shapeOld] > numberOfPointsBlack[shapeNew]) //prepisemo nove tocke k starim, ker jih je manj
	{
		shape1 = shapeOld;
		shape2 = shapeNew;
	}
	else //prepisemo stare tocke k novim, ker jih je manj
	{
		shape1 = shapeNew;
		shape2 = shapeOld;
	}


	if ((numberOfPointsBlack[shapeOld] + numberOfPointsBlack[shapeNew]) > (maxPointsBlack - 1))
	{
		numOffreePoints = maxPointsBlack - numberOfPointsBlack[shape1]; //koliko je se prostgih
		numOfPointsSh2 = numberOfPointsBlack[shape2];
	}
	else
	{
		numOffreePoints = numberOfPointsBlack[shape2];
		numOfPointsSh2 = 0;
	}


	for (ix = 0; ix < numOffreePoints; ix++)
	{
		commonPointsBlackX[shape1][numberOfPointsBlack[shape1]] = commonPointsBlackX[shape2][ix];
		commonPointsBlackY[shape1][numberOfPointsBlack[shape1]] = commonPointsBlackY[shape2][ix];
		numberOfPointsBlack[shape1]++;
		activePointBlack[(int)commonPointsBlackX[shape2][ix]][(int)commonPointsBlackY[shape2][ix]] = shape1;

		if (boundingBoxBlack[shape1].left() > commonPointsBlackX[shape2][ix])
			boundingBoxBlack[shape1].setLeft(commonPointsBlackX[shape2][ix]);

		if (boundingBoxBlack[shape1].right() < commonPointsBlackX[shape2][ix])
			boundingBoxBlack[shape1].setRight(commonPointsBlackX[shape2][ix]);

		if (boundingBoxBlack[shape1].top() > commonPointsBlackY[shape2][ix])
			boundingBoxBlack[shape1].setTop(commonPointsBlackY[shape2][ix]);

		if (boundingBoxBlack[shape1].bottom() < commonPointsBlackY[shape2][ix])
			boundingBoxBlack[shape1].setBottom(commonPointsBlackY[shape2][ix]);

		centerBlack[shape1].x += commonPointsBlackX[shape2][ix];
		centerBlack[shape1].y += commonPointsBlackY[shape2][ix];
	}

	numberOfPointsBlack[shape2] = 0;
	boundingBoxBlack[shape2].setCoords(10000, 10000, 0, 0);
	centerBlack[shape2].SetPointFloat(0, 0);
	for (ix = numOffreePoints; ix < numOfPointsSh2; ix++)
	{
		//stevilo neskopiranih, prepisemo jih povrsti
		commonPointsBlackX[shape2][numberOfPointsBlack[shape2]] = commonPointsBlackX[shape2][ix];
		commonPointsBlackY[shape2][numberOfPointsBlack[shape2]] = commonPointsBlackY[shape2][ix];

		if (boundingBoxBlack[shape2].left() > commonPointsBlackX[shape2][ix])
			boundingBoxBlack[shape2].setLeft(commonPointsBlackX[shape2][ix]);

		if (boundingBoxBlack[shape2].right() < commonPointsBlackX[shape2][ix])
			boundingBoxBlack[shape2].setRight(commonPointsBlackX[shape2][ix]);

		if (boundingBoxBlack[shape2].top() > commonPointsBlackY[shape2][ix])
			boundingBoxBlack[shape2].setTop(commonPointsBlackY[shape2][ix]);

		if (boundingBoxBlack[shape2].bottom() < commonPointsBlackY[shape2][ix])
			boundingBoxBlack[shape2].setBottom ( commonPointsBlackY[shape2][ix]);

		centerBlack[shape2].x += commonPointsBlackX[shape2][ix];
		centerBlack[shape2].y += commonPointsBlackY[shape2][ix];
		//activePointWhite ostane shape2
		numberOfPointsBlack[shape2]++;
	}
}


QGraphicsPixmapItem* CMemoryBuffer::DrawCommonPointsBoundingBox()
{

	int  j, r, g, b;

	Mat test;
	cvtColor(*buffer, test, COLOR_GRAY2RGB);
	Mat DrawImage;
	Rect testRect;

	DrawImage = test(Rect(0, 0, test.cols, test.rows));
	QImage image(DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	QPainter painter(&image);
	//painter.begin(&image);

	painter.setPen(Qt::red);
	int middleX;
	int middleY;
	int i = 0;

	if (isActiveCommonPointsWhite == true)
	{
		for (i = 0; i < maxShapesWhite; i++)
		{
			if (numberOfPointsWhite[i] > 0)
			{
				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;
				QRgb value = qRgb(r, g, b);

				painter.setBrush(Qt::NoBrush);
				painter.drawRect(boundingBoxWhite[i]);


				middleX = ((boundingBoxWhite[i].left() + boundingBoxWhite[i].right()) / 2);
				middleY = ((boundingBoxWhite[i].top() + boundingBoxWhite[i].bottom()) / 2);

				for (j = 0; j < numberOfPointsWhite[i]; j++)
				{

					image.setPixel(commonPointsWhiteX[i][j], commonPointsWhiteY[i][j], value);
				}
				painter.setBrush(Qt::yellow);
				painter.drawEllipse(QPoint(middleX, middleY), 2, 2);

				for (j = 0; j < commonEdgePointsWhite[i].size(); j++)
				{
					painter.drawEllipse(QPoint(commonEdgePointsWhite[i][j].x, commonEdgePointsWhite[i][j].y), 2, 2);
				}


			}


		}
	}
	if (isActiveCommonPointsBlack == true)
	{

		for (i = 0; i < maxShapesBlack; i++)
		{
			if (numberOfPointsBlack[i] > 0)
			{
				r = rand() % 255;
				g = rand() % 255;
				b = rand() % 255;
				QRgb value = qRgb(r, g, b);

				painter.setBrush(Qt::NoBrush);
				painter.drawRect(boundingBoxBlack[i]);


				middleX = ((boundingBoxBlack[i].left() + boundingBoxBlack[i].right()) / 2);
				middleY = ((boundingBoxBlack[i].top() + boundingBoxBlack[i].bottom()) / 2);

				for (j = 0; j < numberOfPointsBlack[i]; j++)
				{

					image.setPixel(commonPointsBlackX[i][j], commonPointsBlackY[i][j], value);
				}
				painter.setBrush(Qt::yellow);
				painter.drawEllipse(QPoint(middleX, middleY), 2, 2);

				for (j = 0; j < commonEdgePointsBlack[i].size(); j++)
				{
					painter.drawEllipse(QPoint(commonEdgePointsBlack[i][j].x, commonEdgePointsBlack[i][j].y), 2, 2);
				}


			}


		}
	}
	painter.end();
	QGraphicsPixmapItem*  QGaphicsPixmap = new QGraphicsPixmapItem;
	QGaphicsPixmap->setPixmap(QPixmap::fromImage(image));
	
	return QGaphicsPixmap;
}
QGraphicsPixmapItem * CMemoryBuffer::DrawCommonPointsBoundingBoxTest(QRect rect)
{

	int  j, r, g, b;

	Mat test;
	int nrChann = buffer->channels();
	cvtColor(*buffer, test, COLOR_GRAY2RGB);
	nrChann = test.channels();
	//cvtColor(*buffer, test, CV_GRAY2BGRA);
	Mat DrawImage;
	Rect testRect;

	DrawImage = test(Rect(0, 0, test.cols, test.rows));
	QImage image(DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	QImage destImage(rect.width(), rect.height(), QImage::Format_RGB888);
	QPainter painter(&image);
	//painter.begin(&image);

	painter.setPen(Qt::red);
	int middleX;
	int middleY;
	int i = 0;
	for (i = 0; i < maxShapesWhite; i++)
	{
		if (numberOfPointsWhite[i] > 0)
		{
			r = rand() % 255;
			g = rand() % 255;
			b = rand() % 255;
			QRgb value = qRgb(r, g, b);

			painter.setBrush(Qt::NoBrush);
			painter.drawRect(boundingBoxWhite[i]);


			middleX = ((boundingBoxWhite[i].left() + boundingBoxWhite[i].right()) / 2);
			middleY = ((boundingBoxWhite[i].top() + boundingBoxWhite[i].bottom()) / 2);

			for (j = 0; j < numberOfPointsWhite[i]; j++)
			{

				image.setPixel(commonPointsWhiteX[i][j], commonPointsWhiteY[i][j], value);
			}
			painter.setBrush(Qt::yellow);
			painter.drawEllipse(QPoint(middleX, middleY), 2, 2);

			for (j = 0; j < commonEdgePointsWhite[i].size(); j++)
			{
				painter.drawEllipse(QPoint(commonEdgePointsWhite[i][j].x, commonEdgePointsWhite[i][j].y), 2, 2);
			}


		}
	}

	for (i = 0; i < maxShapesBlack; i++)
	{
		if (numberOfPointsBlack[i] > 0)
		{
			r = rand() % 255;
			g = rand() % 255;
			b = rand() % 255;
			QRgb value = qRgb(r, g, b);

			painter.setBrush(Qt::NoBrush);
			painter.drawRect(boundingBoxBlack[i]);


			middleX = ((boundingBoxBlack[i].left() + boundingBoxBlack[i].right()) / 2);
			middleY = ((boundingBoxBlack[i].top() + boundingBoxBlack[i].bottom()) / 2);

			for (j = 0; j < numberOfPointsBlack[i]; j++)
			{

				image.setPixel(commonPointsBlackX[i][j], commonPointsBlackY[i][j], value);
			}
			painter.setBrush(Qt::yellow);
			painter.drawEllipse(QPoint(middleX, middleY), 2, 2);

			for (j = 0; j < commonEdgePointsBlack[i].size(); j++)
			{
				painter.drawEllipse(QPoint(commonEdgePointsBlack[i][j].x, commonEdgePointsBlack[i][j].y), 2, 2);
			}


		}


	}

	destImage  = image.copy(rect);
	
	QGraphicsPixmapItem*  QGaphicsPixmap = new QGraphicsPixmapItem;
	QGaphicsPixmap->setPixmap(QPixmap::fromImage(destImage));
	QGaphicsPixmap->setOffset(QPoint(rect.left(), rect.top()));
	QGaphicsPixmap->setOpacity(0.5);
	QGaphicsPixmap->setZValue(1);// da se vedno narise v ozadju

	return QGaphicsPixmap;
}

QGraphicsPixmapItem * CMemoryBuffer::DrawBiggestCommonPoints(QRect rect)
{

	int  j, r, g, b;

	int pointsInShape = 0;
	int indexShape;
	Mat test;
	cvtColor(*buffer, test, COLOR_GRAY2RGB);
	Mat DrawImage;
	Rect testRect;

	DrawImage = test(Rect(0, 0, test.cols, test.rows));
	QImage image(DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	QImage destImage(rect.width(), rect.height(), QImage::Format_RGB888);
	QPainter painter(&image);

	painter.setPen(Qt::red);
	int middleX;
	int middleY;

	for (int i = 0; i < maxShapesWhite; i++)
	{
		if (numberOfPointsWhite[i] > 0)
		{
			if (numberOfPointsWhite[i] > pointsInShape)
			{
				pointsInShape = numberOfPointsWhite[i];
				indexShape = i;
			}
		}
	}
	if (pointsInShape > 0)
	{

		r = rand() % 255;
		g = rand() % 255;
		b = rand() % 255;
		QRgb value = qRgb(r, g, b);

		painter.setBrush(Qt::NoBrush);
		painter.drawRect(boundingBoxWhite[indexShape]);


		middleX = ((boundingBoxWhite[indexShape].left() + boundingBoxWhite[indexShape].right()) / 2);
		middleY = ((boundingBoxWhite[indexShape].top() + boundingBoxWhite[indexShape].bottom()) / 2);

		for (j = 0; j < numberOfPointsWhite[indexShape]; j++)
		{

			image.setPixel(commonPointsWhiteX[indexShape][j], commonPointsWhiteY[indexShape][j], value);
		}
		painter.setBrush(Qt::yellow);
		painter.drawEllipse(QPoint(middleX, middleY), 2, 2);

		for (j = 0; j < commonEdgePointsWhite[indexShape].size(); j++)
		{
			painter.drawEllipse(QPoint(commonEdgePointsWhite[indexShape][j].x, commonEdgePointsWhite[indexShape][j].y), 2, 2);
		}
	}
	pointsInShape = 0;
	indexShape = 0;

	for (int i = 0; i < maxShapesBlack; i++)
	{
		if (numberOfPointsBlack[i] > 0)
		{
			if (numberOfPointsBlack[i] > pointsInShape)
			{
				pointsInShape = numberOfPointsBlack[i];
				indexShape = i;
			}
		}
	}

	if (pointsInShape > 0)
	{
		r = rand() % 255;
		g = rand() % 255;
		b = rand() % 255;
		QRgb value = qRgb(r, g, b);

		painter.setBrush(Qt::NoBrush);
		painter.drawRect(boundingBoxBlack[indexShape]);


		middleX = ((boundingBoxBlack[indexShape].left() + boundingBoxBlack[indexShape].right()) / 2);
		middleY = ((boundingBoxBlack[indexShape].top() + boundingBoxBlack[indexShape].bottom()) / 2);

		for (j = 0; j < numberOfPointsBlack[indexShape]; j++)
		{

			image.setPixel(commonPointsBlackX[indexShape][j], commonPointsBlackY[indexShape][j], value);
		}
		painter.setBrush(Qt::yellow);
		painter.drawEllipse(QPoint(middleX, middleY), 2, 2);

		for (j = 0; j < commonEdgePointsBlack[indexShape].size(); j++)
		{
			painter.drawEllipse(QPoint(commonEdgePointsBlack[indexShape][j].x, commonEdgePointsBlack[indexShape][j].y), 2, 2);
		}

	}

	destImage = image.copy(rect);

	QGraphicsPixmapItem*  QGaphicsPixmap = new QGraphicsPixmapItem;
	QGaphicsPixmap->setPixmap(QPixmap::fromImage(destImage));
	QGaphicsPixmap->setOffset(QPoint(rect.left(), rect.top()));
	QGaphicsPixmap->setOpacity(0.5);
	QGaphicsPixmap->setZValue(1);// da se vedno narise v ozadju

	return QGaphicsPixmap;
}
//Opomba Martin: GetDistancesAroundRadius poišèe toèke prehoda intenzitete v radialnih smereh od centralne toèke in v objektu circle nastavi min, max in avgRadius na razdalje do center-prehod 
//in vector centerDistances saves all distances in filterBand around radius 
/*void CMemoryBuffer::GetDistancesAroundRadius(CCircle &circle, int radius, int filterBand, int F, int whiteThreshold, int blackThreshold)
{
	int x, y, i, r, middleThreshold, position, intensity, tempIntens, counter, z;
	float alfaCos = 0, alfaSin = 0, alfa, distance = 0;

	alfa = M_PI / (180 * F);

	if (radius > filterBand)
	{
		if ((circle.x > radius) && (circle.x < (width - radius)))
		{
			if ((circle.y > radius) && (circle.y < (height - radius)))
			{
				//	circle.circlePoints.clear();
				//	circle.centerDistances.clear();
				middleThreshold = (blackThreshold + whiteThreshold) / 2;
				circle.minRadius = radius + filterBand;
				circle.maxRadius = 0;
				circle.averageRadius = 0;
				counter = 0;

				for (i = 0; i < (360 * F); i++)
				{
					alfaCos = cos(i * alfa);
					alfaSin = sin(i * alfa);

					r = radius - filterBand;
					x = circle.x + r * alfaCos;
					y = circle.y + r * alfaSin;

					//position = GetPosition(x, y);

					tempIntens = 0;
					for (z = 0; z < 3; z++)
					{
						tempIntens += GetAverageIntensity(x, y);
					}
					tempIntens /= 3;

					for (r = radius - filterBand; r < radius + filterBand; r++)
					{
						x = circle.x + r * alfaCos;
						y = circle.y + r * alfaSin;
						position = GetPosition(x, y);

						intensity = GetAverageIntensity(x, y);

						//if transition is found stops at i degree
						if (tempIntens > middleThreshold) //white
						{
							if (intensity < blackThreshold) //transition from white to black
							{
								circle.circlePoints.push_back(CPointFloat(x, y));
								distance = circle.GetDistance(CPointFloat(x, y));
								circle.centerDistances.push_back(distance);

								if (distance > circle.maxRadius)
									circle.maxRadius = distance;

								if (distance < circle.minRadius)
									circle.minRadius = circle.minRadius;

								circle.averageRadius += distance;
								counter++;

								break;
							}
						}
						else //black
						{
							if (intensity > whiteThreshold) //transition from black to white
							{
								circle.circlePoints.push_back(CPointFloat(x, y));
								distance = circle.GetDistance(CPointFloat(x, y));
								circle.centerDistances.push_back(distance);

								if (distance > circle.maxRadius)
									circle.maxRadius = distance;

								if (distance < circle.minRadius)
									circle.minRadius = circle.minRadius;

								circle.averageRadius += distance;
								counter++;
								break;
							}
						}
					}
				}
				if (counter > 0)
					circle.averageRadius /= counter;
			}
		}
	}
}*/
//in vector centerDistances saves all distances in filterBand around radius backward
/*void CMemoryBuffer::GetDistancesAroundRadiusBack(CCircle &circle, int radius, int filterBand, int F, int whiteThreshold, int blackThreshold)
{
	int x, y, i, r, z, middleThreshold, position, intensity, tempIntens, counter;
	float alfaCos = 0, alfaSin = 0, alfa, distance = 0;

	alfa = M_PI / (180 * F);

	if (radius > filterBand)
	{
		if ((circle.x > radius) && (circle.x < (width - radius)))
		{
			if ((circle.y > radius) && (circle.y < (height - radius)))
			{
				//	circle.circlePoints.clear();
				//	circle.centerDistances.clear();
				middleThreshold = (blackThreshold + whiteThreshold) / 2;
				circle.minRadius = radius + filterBand;
				circle.maxRadius = 0;
				circle.averageRadius = 0;
				counter = 0;

				for (i = 0; i < (360 * F); i++)
				{
					alfaCos = cos(i * alfa);
					alfaSin = sin(i * alfa);

					r = radius + filterBand;
					x = circle.x + r * alfaCos;
					y = circle.y + r * alfaSin;
					//	position = GetPosition(x, y);

					tempIntens = 0;
					for (z = 0; z < 3; z++)
					{
						tempIntens += GetAverageIntensity(x, y);
					}
					tempIntens /= 3;

					for (r = radius + filterBand; r > radius - filterBand; r--)
					{
						x = circle.x + r * alfaCos;
						y = circle.y + r * alfaSin;
						position = GetPosition(x, y);

						intensity = GetAverageIntensity(x, y);

						//if transition is found stops at i degree
						if (tempIntens > middleThreshold) //white
						{
							if (intensity < blackThreshold) //transition from white to black
							{
								circle.circlePoints.push_back(CPointFloat(x, y));
								distance = circle.GetDistance(CPointFloat(x, y));
								circle.centerDistances.push_back(distance);

								if (distance > circle.maxRadius)
									circle.maxRadius = distance;

								if (distance < circle.minRadius)
									circle.minRadius = circle.minRadius;

								circle.averageRadius += distance;
								counter++;

								break;
							}

						}
						else //black
						{
							if (intensity > whiteThreshold) //transition from black to white
							{
								circle.circlePoints.push_back(CPointFloat(x, y));
								distance = circle.GetDistance(CPointFloat(x, y));
								circle.centerDistances.push_back(distance);

								if (distance > circle.maxRadius)
									circle.maxRadius = distance;

								if (distance < circle.minRadius)
									circle.minRadius = circle.minRadius;

								circle.averageRadius += distance;
								counter++;
								break;
							}

						}
					}
				}
				if (counter > 0)
					circle.averageRadius /= counter;
			}
		}
	}
}*/
//return  distance in filterBand from radius at selected degree
/*float CMemoryBuffer::GetDistanceAroundRadius(CCircle &circle, int r1, int r2, int degree, int F, int whiteThreshold, int blackThreshold)
{
	int x, y, r, z, middleThreshold, position, intensity, tempIntens;
	float distance = 0;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);

	if (circle.radius > (r2 - r1))
	{
		if ((circle.x > r2) && (circle.x < (width - r2)))
		{
			if ((circle.y > r2) && (circle.y < (height - r2)))
			{
				//	circle.circlePoints.clear();
				//	circle.centerDistances.clear();
				middleThreshold = (blackThreshold + whiteThreshold) / 2;

				r = r1;
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;
				//	position = GetPosition(x, y);

				tempIntens = 0;
				for (z = -1; z < 2; z++)
				{
					tempIntens += GetAverageIntensity(x, y);
				}
				tempIntens /= 3;

				for (r = r1; r < r2; r++)
				{
					x = circle.x + r * alfaCos;
					y = circle.y + r * alfaSin;
					//	position = GetPosition(x, y);

					intensity = 0;
					for (z = -1; z < 2; z++)
						intensity += GetAverageIntensity(x, y);

					intensity /= 3;

					//if transition is found stops at i degree
					if (tempIntens > middleThreshold) //white
					{
						if (intensity < blackThreshold) //transition from white to black
						{
							circle.circlePoints.push_back(CPointFloat(x, y));
							distance = circle.GetDistance(CPointFloat(x, y));
							circle.centerDistances.push_back(distance);
							break;
						}

					}
					else //black
					{
						if (intensity > whiteThreshold) //transition from black to white
						{
							circle.circlePoints.push_back(CPointFloat(x, y));
							distance = circle.GetDistance(CPointFloat(x, y));
							circle.centerDistances.push_back(distance);
							break;
						}
					}
				}
			}
		}
	}
	return distance;
}
//return  distance in filterBand from radius at selected degree
float CMemoryBuffer::GetDistanceAroundRadiusBack(CCircle &circle, int r1, int r2, int degree, int F, int whiteThreshold, int blackThreshold)
{
	int x, y, r, z, middleThreshold, position, intensity, tempIntens;
	float distance = 0;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);

	if (circle.radius > (r2 - r1))
	{
		if ((circle.x > r2) && (circle.x < (width - r2)))
		{
			if ((circle.y > r2) && (circle.y < (height - r2)))
			{
				//	circle.circlePoints.clear();
				//	circle.centerDistances.clear();
				middleThreshold = (blackThreshold + whiteThreshold) / 2;

				r = r2;
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;
				//position = GetPosition(x, y);

				tempIntens = 0;
				for (z = 0; z < 3; z++)
					tempIntens += GetAverageIntensity(x, y);

				tempIntens /= 3;

				for (r = r2; r > r1; r--)
				{
					x = circle.x + r * alfaCos;
					y = circle.y + r * alfaSin;
					//	position = GetPosition(x, y);

					intensity = 0;
					for (z = -1; z < 2; z++)
						intensity += GetAverageIntensity(x, y);

					intensity /= 3;

					//if transition is found stops at i degree
					if (tempIntens > middleThreshold) //white
					{
						if (intensity < blackThreshold) //transition from white to black
						{
							circle.circlePoints.push_back(CPointFloat(x, y));
							distance = circle.GetDistance(CPointFloat(x, y));
							circle.centerDistances.push_back(distance);
							break;
						}
					}
					else //black
					{
						if (intensity > whiteThreshold) //transition from black to white
						{
							circle.circlePoints.push_back(CPointFloat(x, y));
							distance = circle.GetDistance(CPointFloat(x, y));
							circle.centerDistances.push_back(distance);
							break;
						}
					}
				}
			}
		}
	}
	return distance;
}
//return  distance in filterBand from radius at selected degree
float CMemoryBuffer::GetDistanceAroundRadius(CCircle &circle, int r1, int r2, int degree, int F, int differenceThreshold)
{
	int x, y, r, z, position, intensity, tempIntens;
	float distance;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);

	distance = 0;

	if ((circle.x > r2) && (circle.x < (width - r2)))
	{
		if ((circle.y > r2) && (circle.y < (height - r2)))
		{
			//circle.circlePoints.clear();
			//circle.centerDistances.clear();

			x = circle.x + r1 * alfaCos;
			y = circle.y + r1 * alfaSin;
			//	position = GetPosition(x, y);

			tempIntens = 0;
			for (z = 0; z < 3; z++)
				tempIntens += GetAverageIntensity(x, y);

			tempIntens /= 3;

			for (r = r1; r < r2; r++)
			{
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;
				//position = GetPosition(x, y);

				intensity = 0;
				for (z = -1; z < 2; z++)
					intensity += GetAverageIntensity(x, y);

				intensity /= 3;

				//if transition is found stops at i degree
				if (abs(tempIntens - intensity) > differenceThreshold)
				{
					circle.circlePoints.push_back(CPointFloat(x, y));
					distance = circle.GetDistance(CPointFloat(x, y));
					circle.centerDistances.push_back(distance);
					break;
				}
			}
		}
	}

	return distance;
}

float CMemoryBuffer::GetDistanceAroundRadiusBack(CCircle &circle, int r1, int r2, int degree, int F, int differenceThreshold)
{
	int x, y, r, z, position, intensity, i, tempIntens;
	float distance;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);

	distance = 0;

	if ((circle.x > r2) && (circle.x < (width - r2)))
	{
		if ((circle.y > r2) && (circle.y < (height - r2)))
		{
			//	circle.circlePoints.clear();
			//	circle.centerDistances.clear();

			x = circle.x + r2 * alfaCos;
			y = circle.y + r2 * alfaSin;
			//	position = GetPosition(x, y);

			tempIntens = 0;
			for (z = 0; z < 3; z++)
				tempIntens += GetAverageIntensity(x, y);

			tempIntens /= 3;

			for (r = r2; r > r1; r--)
			{
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;

				//	position = GetPosition(x, y);

				intensity = 0;
				for (z = -1; z < 2; z++)
					intensity += GetAverageIntensity(x, y);

				intensity /= 3;

				//if transition is found stops at i degree
				if (abs(tempIntens - intensity) > differenceThreshold)
				{
					circle.circlePoints.push_back(CPointFloat(x, y));
					distance = circle.GetDistance(CPointFloat(x, y));
					circle.centerDistances.push_back(distance);
					break;
				}
			}
		}
	}

	return distance;
}
//return  distance in filterBand from radius at selected degree - looks only green component
float CMemoryBuffer::GetDistanceAroundRadiusGreen(CCircle &circle, int r1, int r2, int degree, int F, int whiteThreshold, int blackThreshold)
{
	int x, y, r, z, position, intensity, middleThreshold, tempIntens;
	float distance;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);
	distance = 0;

	if (circle.radius > (r2 - r1))
	{
		if ((circle.x > (r2)) && (circle.x < (width - r2)))
		{
			if ((circle.y > (r2)) && (circle.y < (height - r2)))
			{
				middleThreshold = (blackThreshold + whiteThreshold) / 2;

				//	circle.circlePoints.clear();
				//	circle.centerDistances.clear();

				r = r1;
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;
				position = GetPosition(x, y);

				tempIntens = 0;
				for (z = 0; z < 3; z++)
					tempIntens += GetGreen(x, y);

				tempIntens /= 3;

				for (r = r1; r < r2; r++)
				{
					x = circle.x + r * alfaCos;
					y = circle.y + r * alfaSin;
					position = GetPosition(x, y);

					intensity = 0;
					for (z = -1; z < 2; z++)
						intensity += GetGreen(x, y);

					intensity /= 3;

					//if transition is found stops at i degree
					if (tempIntens > middleThreshold) //white
					{
						if (intensity < blackThreshold) //transition from white to black
						{
							circle.circlePoints.push_back(CPointFloat(x, y));
							distance = circle.GetDistance(CPointFloat(x, y));
							circle.centerDistances.push_back(distance);
							break;
						}
					}
					else //black
					{
						if (intensity > whiteThreshold) //transition from black to white
						{
							circle.circlePoints.push_back(CPointFloat(x, y));
							distance = circle.GetDistance(CPointFloat(x, y));
							circle.centerDistances.push_back(distance);
							break;
						}
					}
				}
			}
		}
	}
	return distance;
}
//return  distance in filterBand from radius at selected degree backward - looks only green component
float CMemoryBuffer::GetDistanceAroundRadiusBackGreen(CCircle &circle, int r1, int r2, int degree, int F, int whiteThreshold, int blackThreshold)
{
	int x, y, r, z, position, intensity, middleThreshold, i, tempIntens;
	float distance;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);

	distance = 0;
	if (circle.radius > (r2 - r1))
	{
		if ((circle.x > r2) && (circle.x < (width - r2)))
		{
			if ((circle.y > r2) && (circle.y < (height - r2)))
			{
				middleThreshold = (blackThreshold + whiteThreshold) / 2;
				//	circle.circlePoints.clear();
				//	circle.centerDistances.clear();

				r = r2;
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;

				position = GetPosition(x, y);

				tempIntens = 0;
				for (z = 0; z < 3; z++)
					tempIntens += GetGreen(x, y);

				tempIntens /= 3;

				for (r = r2; r > r1; r--)
				{
					x = circle.x + r * alfaCos;
					y = circle.y + r * alfaSin;
					position = GetPosition(x, y);

					intensity = 0;
					for (z = -1; z < 2; z++)
						intensity += GetGreen(x, y);

					intensity /= 3;

					//if transition is found stops at i degree
					if (tempIntens > middleThreshold) //white
					{
						if (intensity < blackThreshold) //transition from white to black
						{
							circle.circlePoints.push_back(CPointFloat(x, y));
							distance = circle.GetDistance(CPointFloat(x, y));
							circle.centerDistances.push_back(distance);
							break;
						}

					}
					else //black
					{
						if (intensity > whiteThreshold) //transition from black to white
						{
							circle.circlePoints.push_back(CPointFloat(x, y));
							distance = circle.GetDistance(CPointFloat(x, y));
							circle.centerDistances.push_back(distance);
							break;
						}
					}
				}
			}
		}
	}
	return distance;
}

float CMemoryBuffer::GetDistanceAroundRadiusGreen(CCircle &circle, int r1, int r2, int degree, int F, int differenceThreshold)
{
	int x, y, z, r, position, intensity, tempIntens;
	float distance;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);
	distance = 0;

	if ((circle.x > r2) && (circle.x < (width - r2)))
	{
		if ((circle.y > r2) && (circle.y < (height - r2)))
		{
			//	circle.circlePoints.clear();
			//	circle.centerDistances.clear();

			x = circle.x + r1 * alfaCos;
			y = circle.y + r1 * alfaSin;
			position = GetPosition(x, y);

			tempIntens = 0;
			for (z = 0; z < 3; z++)
				tempIntens += GetGreen(x, y);

			tempIntens /= 3;

			for (r = r1; r < r2; r++)
			{
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;
				position = GetPosition(x, y);

				intensity = 0;
				for (z = -1; z < 2; z++)
					intensity += GetGreen(x, y);

				intensity /= 3;

				//if transition is found stops at i degree
				if (abs(tempIntens - intensity) > differenceThreshold)
				{
					circle.circlePoints.push_back(CPointFloat(x, y));
					distance = circle.GetDistance(CPointFloat(x, y));
					circle.centerDistances.push_back(distance);
					break;
				}
			}
		}
	}

	return distance;
}
//return  distance in filterBand from radius at selected degree backward - looks only green component
float CMemoryBuffer::GetDistanceAroundRadiusBackGreen(CCircle &circle, int r1, int r2, int degree, int F, int differenceThreshold)
{
	int x, y, z, r, position, intensity, i, tempIntens;
	float distance;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);

	distance = 0;

	if ((circle.x > r2) && (circle.x < (width - r2)))
	{
		if ((circle.y > r2) && (circle.y < (height - r2)))
		{
			//	circle.circlePoints.clear();
			//	circle.centerDistances.clear();

			x = circle.x + r2 * alfaCos;
			y = circle.y + r2 * alfaSin;

			position = GetPosition(x, y);

			tempIntens = 0;
			for (z = 0; z < 3; z++)
				tempIntens += GetGreen(x, y);

			tempIntens /= 3;

			for (r = r2; r > r1; r--)
			{
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;
				position = GetPosition(x, y);

				intensity = 0;
				for (z = -1; z < 2; z++)
					intensity += GetGreen(x, y);

				intensity /= 3;

				//if transition is found stops at i degree
				if (abs(tempIntens - intensity) > differenceThreshold)
				{
					circle.circlePoints.push_back(CPointFloat(x, y));
					distance = circle.GetDistance(CPointFloat(x, y));
					circle.centerDistances.push_back(distance);
					break;
				}
			}
		}
	}
	return distance;
}
*/
////AVERAGE INTENSITY in the circle in the middle r1 and r2 radius
void CMemoryBuffer::GetAverageIntensity(CCircle &circle, int r1, int r2, int F)
{
	int x, y, i, r, position, intensity, sumRed, sumGreen, sumBlue, counter;
	float alfaCos, alfaSin;

	alfaCos = M_PI / (180 * F);
	alfaSin = M_PI / (180 * F);

	if (r1 > r2)
	{
		i = r1;
		r1 = r2;
		r2 = i;
	}

	if ((circle.x > r2) && (circle.x < (width - r2)))
	{
		if ((circle.y > r2) && (circle.y < (height - r2)))
		{
			circle.averageIntensity.clear();
			circle.averageIntensityBlue.clear();
			circle.averageIntensityRed.clear();
			circle.averageIntensityGreen.clear();

			for (i = 0; i < (360 * F); i++)
			{
				intensity = 0;
				sumRed = 0;
				sumGreen = 0;
				sumBlue = 0;
				counter = 0;
				for (r = r1; r < r2; r++)
				{
					x = circle.x + r * cos(i * alfaCos);
					y = circle.y + r * sin(i * alfaSin);



					intensity += GetAverageIntensity(x, y);
					//sumRed += GetRed(x, y);
					//sumGreen += GetGreen(x, y);
					//sumBlue += GetBlue(x, y);

					//pDC->SetPixel(x,y, RGB(0,0,255));
					counter++;
				}

				if (counter > 0)
				{
					circle.averageIntensity.push_back(intensity / counter);
					//circle.averageIntensityBlue.push_back(sumBlue / counter);
					//circle.averageIntensityRed.push_back(sumRed / counter);
					//circle.averageIntensityGreen.push_back(sumGreen / counter);

				}
			}
		}
	}
}
/*
////AVERAGE INTENSITY in the circle in the middle r1 and r2 radius on selected degree
//average intensity of each rgb component
RGBQUAD CMemoryBuffer::GetAverageIntensityRGB(CCircle &circle, CDC* pDC, bool draw, int r1, int r2, int F, int degree)
{
	int x, y, r, position, intensity, previntensity = 0, sum, sumRed, sumGreen, sumBlue, counter;
	RGBQUAD averInt;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);

	averInt.rgbBlue = 0;
	averInt.rgbGreen = 0;
	averInt.rgbRed = 0;
	averInt.rgbReserved = 0;

	if (r1 > r2)
	{
		x = r1;
		r1 = r2;
		r2 = x;
	}

	if ((circle.x > r2) && (circle.x < (width - r2)))
	{
		if ((circle.y > r2) && (circle.y < (height - r2)))
		{
			sum = 0;
			sumRed = 0;
			sumGreen = 0;
			sumBlue = 0;
			counter = 0;
			for (r = r1; r < r2; r++)
			{
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;



				intensity = GetAverageIntensity(x, y);
				sumRed += GetRed(x, y);
				sumGreen += GetGreen(x, y);
				sumBlue += GetBlue(x, y);

				if (previntensity == 0)
					previntensity = intensity;

				if (draw)
					pDC->SetPixel(x* zoomFactor + xOffset, y * zoomFactor + yOffset, RGB(0, 0, 255));


				sum += intensity;
				previntensity = intensity;
				counter++;
			}

			if (counter > 0)
			{
				averInt.rgbBlue = sumBlue / counter;
				averInt.rgbGreen = sumGreen / counter;
				averInt.rgbRed = sumRed / counter;
				averInt.rgbReserved = sum / counter;
			}
		}
	}
	return averInt;
}
//average intensity of all rgb components
int CMemoryBuffer::GetAverageIntensity(CDC* pDC, bool draw, CCircle &circle, int r1, int r2, int F, int degree)
{
	int x, y, r, position, intensity, sum, counter = 0;
	float alfaCos, alfaSin, alfa;

	alfa = degree * M_PI / (180 * F);
	alfaCos = cos(alfa);
	alfaSin = sin(alfa);

	if (r1 > r2)
	{
		x = r1;
		r1 = r2;
		r2 = x;
	}

	if ((circle.x > r2) && (circle.x < (width - r2)))
	{
		if ((circle.y > r2) && (circle.y < (height - r2)))
		{
			sum = 0;
			counter = 0;

			for (r = r1; r < r2; r++)
			{
				x = circle.x + r * alfaCos;
				y = circle.y + r * alfaSin;



				intensity = GetAverageIntensity(x, y);

				if (draw)
					pDC->SetPixel(x* zoomFactor + xOffset, y* zoomFactor + yOffset, RGB(0, 0, 255));


				sum += intensity;
				counter++;
			}
		}
	}
	if (counter > 0)
		return sum / counter;

	return -1;
}*/

//Dodal Martin: GetWhiteAndBlackArea vrne površino belega oziroma èrnega znotraj zasukanega pravokotnika
//èe je whiteArea true, vrne površino z intenziteto nad treshold, sicer površino z intenziteto pod treshold
bool CMemoryBuffer::GetWhiteAndBlackArea(CRectRotated* rectRot, float* whiteArea, float* blackArea, float treshold)
{
	int x, y;

	CPointFloat T;
	*whiteArea = 0;
	*blackArea = 0;

	if (rectRot->left() < 0 || rectRot->right() >= width) return false;
	if (rectRot->top() < 0 || rectRot->bottom() >= height) return false;

	for (x = rectRot->left(); x < rectRot->right(); x++)
	{
		for (y = rectRot->top(); y < rectRot->bottom(); y++)
		{
			T.SetPointFloat(x, y);

			if (rectRot->InPolygon(T))
			{
				//if (draw)  T.DrawDot(pDC, RGB(255, 255, 0), 1, zoomFactor, xOffset, yOffset);
				if (GetAverageIntensity(x, y) >= treshold) *whiteArea = *whiteArea + 1;
				else *blackArea = *blackArea + 1;
			}
		}
	}

	return true;
}


//poišèe težišèe èrnega in belega podmreži pravokotnika: perioda doloèa gostoto podmreže (pri perioda = 1 pregledamo vse toèke). Hkrati izraèuna število belih in èrnih toèk ter srednjo vrednost belega in èrnega
//pri izraèunu težišèa pikslov ne utežim z intenziteto
bool CMemoryBuffer::GetWhiteAndBlackGravity(CRectRotated rect, int perioda, int* nWhite, int* nBlack, float* avgWhite, float* avgBlack, CPointFloat* whiteGravity, CPointFloat* blackGravity, float treshold)
{
	

	byte intensity;
	int x, y, position;
	double whiteIsum, blackIsum;
	double whiteXsum, whiteYsum, blackXsum, blackYsum;


	*nWhite = 0;
	*nBlack = 0;

	//funkcija vrne false, èe ni cel pravokotnik v obmoèju slike
	if (rect.left() < 0 || rect.right() >= this->width) return false;
	if (rect.top() < 0 || rect.bottom() >= this->height) return false;

	whiteIsum = 0;
	blackIsum = 0;

	whiteXsum = 0;
	whiteYsum = 0;
	blackXsum = 0;
	blackYsum = 0;

	for (x = rect.left() + perioda / 2; x < rect.right(); x += perioda)	//zamaknem zaèetek podmreže za perioda/2, da se njeno središèe v povpreèju bolje ujema s središèem pravokotnika
	{
		for (y = rect.top() + perioda / 2; y < rect.bottom(); y += perioda)
		{
			position = y * this->width + x;
			intensity = this->buffer->data[position];

			if (intensity >= treshold)
			{
				*nWhite = *nWhite + 1;
				whiteIsum += intensity;
				whiteXsum += x;
				whiteYsum += y;
				whiteGravity->x += x;
				whiteGravity->y += y;

				//izris belih toèk
				//if (draw) CPointFloat(i,j).DrawDot(pDC, RGB(0, 0, 255), 1, this->zoomFactor, this->xOffset, this->yOffset);
			}
			else
			{
				*nBlack = *nBlack + 1;
				blackIsum += intensity;
				blackXsum += x;
				blackYsum += y;
				blackGravity->x += x;
				blackGravity->y += y;
			}
		}

	}

	if (*nWhite > 0)
	{
		*avgWhite = whiteIsum / *nWhite;
		whiteGravity->x = (float)whiteXsum / (*nWhite);
		whiteGravity->y = (float)whiteYsum / (*nWhite);

	}
	else
	{
		*avgWhite = 0;
		*whiteGravity = CPointFloat(0, 0);
	}

	if (*nBlack > 0)
	{
		*avgBlack = blackIsum / *nBlack;
		blackGravity->x = blackXsum / (*nBlack);
		blackGravity->y = blackYsum / (*nBlack);
	}
	else
	{
		*avgWhite = 0;
		*whiteGravity = CPointFloat(0, 0);
	}

	//izris težišèa


	return true;
}


//Dodal Martin: izraèuna toèke prvega roba v pravokotniku v smeri direction: 0: x++,  1: x--, 2: y++, 3: y--
//prej je treba izraèunati robove s FindEdges
void CMemoryBuffer::GetFirstEdgePointsVector(QRect rect, int direction, vector<CPointFloat>& edgePoints)
{
	int x, y;

	float xDrawFloat, yDrawFloat;

	CRectRotated rectRot;

	edgePoints.clear();

	rectRot = CRectRotated(rect);

	

	bool naselRob;
	int xStart, yStart, xStep, yStep;
	float xRob, yRob;

	if (direction < 2)
	{
		if (direction == 0)	//išèemo prvi prehod intenzitete v smeri x++
		{
			xStart = rect.left();
			xStep = +1;
		}
		else     //išèemo prvi prehod intenzitete v smeri x--
		{
			xStart = rect.right() - 1;
			xStep = -1;
		}

		for (y = rect.top(); y < rect.bottom(); y++)
		{
			naselRob = false;

			for (x = xStart; !naselRob && ((xStep == 1 && x < rect.right()) || (xStep == -1 && x >= rect.left())); x += xStep)
			{
				xRob = this->Xedges[y][x];
				yRob = this->Yedges[x][y];

				if ((xRob > 0) || (yRob > 0))
				{
					naselRob = true;

					if (xRob == 0)
					{
						edgePoints.push_back(CPointFloat(x, yRob));
					}
					else if (yRob == 0)
					{
						edgePoints.push_back(CPointFloat(xRob, y));
					}
					else
					{
						edgePoints.push_back(CPointFloat(xRob, yRob));
					}
				}

			}
		}
	}
	else
	{
		if (direction == 2)  //išèemo prehod v smeri y++
		{
			yStart = rect.top();
			yStep = +1;
		}
		else     //išèemo prehod v smeri y--
		{
			yStart = rect.bottom() - 1;
			yStep = -1;
		}

		for (x = rect.left(); x < rect.right(); x++)
		{
			naselRob = false;

			for (y = yStart; !naselRob && ((yStep == 1 && y < rect.bottom()) || (yStep == -1 && y >= rect.top())); y += yStep)
			{
				xRob = this->Xedges[y][x];
				yRob = this->Yedges[x][y];

				if ((xRob > 0) || (yRob > 0))
				{
					naselRob = true;

					if (xRob == 0)
					{
						edgePoints.push_back(CPointFloat(x, yRob));
					}
					else if (yRob == 0)
					{
						edgePoints.push_back(CPointFloat(xRob, y));
					}
					else
					{
						edgePoints.push_back(CPointFloat(xRob, yRob));
					}
				}
			}
		}
	}

	int i;




}


void CMemoryBuffer::GetFirstEdgePointsVectorRotated(CRectRotated& rectRot, int direction, int nLines, float step, bool blackToWhite, float treshold, vector<CPointFloat>& edgePoints, int minThickness, bool transitionRequired)
{

	int i;
	float xDrawFloat, yDrawFloat;

	CPointFloat T00, deltaT[2], Ttmp, Ttransition;

	CLine searchLine;
	if (rectRot.polygonPoints.size() > 3)
	{
		T00 = rectRot.polygonPoints[0];
		deltaT[0] = rectRot.polygonPoints[1] - rectRot.polygonPoints[0];
		deltaT[1] = rectRot.polygonPoints[3] - rectRot.polygonPoints[0];
	}
	else//to je v primeru kadar kvadrat ni zasukan in dela na navadnem kvadratu, brez polygonPoints
	{
		T00.x = rectRot.left();
		T00.y = rectRot.top();

		deltaT[0].x = 0;
		deltaT[0].y = rectRot.bottom() - rectRot.top();
		deltaT[1].x = rectRot.right() - rectRot.left();
		deltaT[1].y = 0;
	}
	edgePoints.clear();

	for (i = 0; i < nLines; i++)
	{
		if (direction < 2)
		{
			searchLine.p1 = T00 + deltaT[1] * ((float)i) / nLines;
			searchLine.p2 = searchLine.p1 + deltaT[0];
		}
		else
		{
			searchLine.p1 = T00 + deltaT[0] * ((float)i) / nLines;
			searchLine.p2 = searchLine.p1 + deltaT[1];
		}

		if (direction % 2 == 1)
		{
			Ttmp = searchLine.p1;
			searchLine.p1 = searchLine.p2;
			searchLine.p2 = Ttmp;
		}

		searchLine.SetLine(searchLine.p1, searchLine.p2);

		Ttransition = this->GetEdgePointOnSegment2D(searchLine, true, step, blackToWhite, treshold, minThickness, transitionRequired);

		if (Ttransition != CPointFloat(0, 0))
		{
			edgePoints.push_back(Ttransition);

			
		}

	}

}

void CMemoryBuffer::GetDoubleEdgePointsVectorRotated(CRectRotated& rectRot, int direction, int nLines, float step, bool blackToWhite, float treshold, vector<CPointFloat> edgePoints[2], int minThickness, bool transitionRequired)
{
	int rob, i;
	float xDrawFloat, yDrawFloat;

	CPointFloat T00, deltaT[2], Ttmp, Ttransition[2];

	CLine searchLine;

	/*T00 = rectRot.polygonPoints[0];
	deltaT[0] = rectRot.polygonPoints[1] - rectRot.polygonPoints[0];
	deltaT[1] = rectRot.polygonPoints[3] - rectRot.polygonPoints[0];

	*/
	if (rectRot.polygonPoints.size() > 3)
	{
		T00 = rectRot.polygonPoints[0];
		deltaT[0] = rectRot.polygonPoints[1] - rectRot.polygonPoints[0];
		deltaT[1] = rectRot.polygonPoints[3] - rectRot.polygonPoints[0];
	}
	else//to je v primeru kadar kvadrat ni zasukan in dela na navadnem kvadratu, brez polygonPoints
	{
		T00.x = rectRot.left();
		T00.y = rectRot.top();

		deltaT[0].x = 0;
		deltaT[0].y = rectRot.bottom() - rectRot.top();
		deltaT[1].x = rectRot.right() - rectRot.left();
		deltaT[1].y = 0;
	}



	for (rob = 0; rob < 2; rob++) edgePoints[rob].clear();

	for (i = 0; i < nLines; i++)
	{
		if (direction < 2)
		{
			searchLine.p1 = T00 + deltaT[1] * ((float)i) / nLines;
			searchLine.p2 = searchLine.p1 + deltaT[0];
		}
		else
		{
			searchLine.p1 = T00 + deltaT[0] * ((float)i) / nLines;
			searchLine.p2 = searchLine.p1 + deltaT[1];
		}

		if (direction % 2 == 1)
		{
			Ttmp = searchLine.p1;
			searchLine.p1 = searchLine.p2;
			searchLine.p2 = Ttmp;
		}

		Ttransition[0] = this->GetEdgePointOnSegment2D(searchLine, true, step, blackToWhite, treshold, minThickness, transitionRequired);

		if (Ttransition[0] != CPointFloat(0, 0))
		{
			searchLine.p1 = Ttransition[0];	//novo obmoèje iskanja je od prvega prehoda naprej

			Ttransition[1] = this->GetEdgePointOnSegment2D(searchLine, true, step, !blackToWhite, treshold, minThickness, transitionRequired);

			if (Ttransition[1] != CPointFloat(0, 0))	//robova zabeležimo, le če najdemo prvega in drugega
			{
				for (rob = 0; rob < 2; rob++)
				{
					edgePoints[rob].push_back(Ttransition[rob]);

				}
			}
		}
	}
}



float CMemoryBuffer::GetAverageThickness(CRectRotated& rectRot, int direction, int nLines, float step, bool blackToWhite, float treshold, int minThickness, bool transitionRequired)
{
	float Davg;

	vector<CPointFloat> edgePoints[2];

	GetDoubleEdgePointsVectorRotated(rectRot,direction, nLines, step, blackToWhite, treshold, edgePoints, minThickness, transitionRequired);

	Davg = 0;

	for (int i = 0; i < edgePoints[0].size(); i++) Davg += edgePoints[0][i].GetDistance(edgePoints[1][i]);		//vektorja edgePoints [0] in [1] imata enako število elementov

	if (edgePoints[0].size() == 0) return -1;
	else return Davg / edgePoints[0].size();
}


//Dodal Martin:  izraèuna toèke prvega roba v pravokotniku rect iz centra v radialni smeri navzven oz. navznoter (glede na directionOut)
//ni potrebno prej izraèunati FindEdges, ker išèem prehod z mojo 2D interpolacijo intenzitet
//išèe samo na kolobarju med Rmin in Rmax, pri tem izpusti vse toèke izven pravokotnika rect
//foundEdgesShare nastavi na delež najdenih robov (prehodov intenzitete)
//Èe je omitAngles nastavljen na true, izpustimo kote, pri katerih funkcija OmitAngle(alfa) vrne false
//Rstep je korak za iskanje prehoda v radialni smeri v pikslih, fiStep pa  v kotni smeri v pikslih (fiStep je dolžina loka med dvema smerema iskanja pri Rmax)
void CMemoryBuffer::GetFirstEdgePointsVectorRadial(QRect rect, bool directionOut, float Rstep, float fiStep, bool blackToWhite, float treshold, CPointFloat center, float Rmin, float Rmax, bool omitAngles, bool OmitAngle(float alpha), vector<float>& angles, vector<CPointFloat>& edgePoints, float* foundEdgesShare)
{
	CRectRotated rectRot;

	rectRot = CRectRotated(rect);

	edgePoints.clear();
	angles.clear();

	




	//zgornjo mejo za radij izraèunamo kot polovico daljše stranice pravokotnika
	float alpha, alphaStep;



	alphaStep = fiStep / Rmax;	//Moj naèin: korak kota je tak, da je ustrezni lok pri maksimalnem radiju dolg najmanj 1 piksel

	int i, directionXY, nSearches, nEdgesFound;
	float distance, minDistance;
	CLine line;
	CLine rectEdge[4];
	CPointFloat firstIntersection, move, Ttransition, Ttmp;
	CPointFloat intersection[4];


	rectEdge[0].SetLine(CPointFloat(rect.left(), rect.bottom()), CPointFloat(rect.right(), rect.bottom()));
	rectEdge[1].SetLine(CPointFloat(rect.right(), rect.bottom()), CPointFloat(rect.right(), rect.top()));
	rectEdge[2].SetLine(CPointFloat(rect.right(), rect.top()), CPointFloat(rect.left(), rect.top()));
	rectEdge[3].SetLine(CPointFloat(rect.left(), rect.top()), CPointFloat(rect.left(), rect.bottom()));

	alpha = 0;

	nSearches = 0;
	nEdgesFound = 0;

	while (alpha < 2 * M_PI)
	{
		if (!omitAngles || !OmitAngle(alpha))
		{

			line.SetLine(center, alpha);

			//od štirih preseèišè z okvirjem pravokotnika izberemo tistega, ki je prvi v smeri alfa
			for (i = 0; i < 4; i++) intersection[i] = line.GetIntersectionPoint(rectEdge[i]);

			firstIntersection.SetPointFloat(0, 0);

			minDistance = Rmax;

			//poišèemo, kje linija iz centra v smeri alpha seka rob pravokotnika
			for (i = 0; i < 4; i++)
			{
				move = intersection[i] - center;
				distance = move.x*cos(alpha) + move.y*sin(alpha);
				if (distance > 0 && distance < minDistance)
				{
					firstIntersection = intersection[i];
					minDistance = distance;
				}
			}

			if (minDistance < Rmin) line.p1 = firstIntersection;	//linija prej seka rob pravokotnika
			else line.p1 = center + CPointFloat(cos(alpha), sin(alpha)) * Rmin;  //linija prej seka krožnico z radijem Rmin okoli centra

			if (minDistance < Rmax) line.p2 = firstIntersection;	//linija prej seka rob pravokotnika
			else line.p2 = center + CPointFloat(Rmax * cos(alpha), Rmax * sin(alpha));  //linija prej seka krožnico z radijem Rmax okoli centra



			if (!directionOut)
			{
				Ttmp = line.p1;
				line.p1 = line.p2;
				line.p2 = Ttmp;
			}




			Ttransition = this->GetEdgePointOnSegment2D(line, true, Rstep, blackToWhite, treshold);

			nSearches++;

			if (Ttransition != CPointFloat(0, 0))
			{
				nEdgesFound++;

				edgePoints.push_back(Ttransition);
				angles.push_back(alpha);


			}

		}


		alpha += alphaStep;
	}

	*foundEdgesShare = ((float)nEdgesFound) / nSearches;
}


//izraèuna povpreèni odklon vektorja toèk od znane krožnice (v pikslih)
//vektor toèk izraèunaš npr. z GetFirstEdgePointVector ali GetFirstEdgePointVectorRadial
//krog izraèunaš npr. z FindCircleInRectangle
/*float CMemoryBuffer::GetDeviationFromCircle(vector<CPointFloat>& points, CCircle* circle)
{

	int i;
	double distanceFromCenter, Rerror, stDevR;


	CPointFloat center;

	stDevR = 0;
	center = CPointFloat(circle->x, circle->y);

	if (points.size() == 0) return -1;

	for (i = 0; i < points.size(); i++)
	{
		distanceFromCenter = points[i].GetDistance(center);

		Rerror = abs(distanceFromCenter - circle->radius);

		stDevR += pow(Rerror, 2);
	}

	stDevR = sqrt(stDevR / points.size());

	return stDevR;
}
*/
//DrawRadiusAtAngle izriše graf razdalje toèk (points) od centra v odvisnosti od kota (angles: v radianih). GraphDxPerDeg ter graphDyPerRadiusPix doloèita merilo v smeri x in y.
bool  CMemoryBuffer::DrawRadiusAtAngle(vector<CPointFloat>& points, CPointFloat center, CPointFloat graphCenter, int graphDxPerDeg, float Rnominal, float yMagnification)
{
	int i;
	int x, y;
	float R, Rmin, Rmax, deltaX, deltaY;
	float polarAngle;
	CPointFloat T;

	Rmin = HUGE_VAL;
	Rmax = 0;

	for (i = 0; i < points.size(); i++)
	{
		R = points[i].GetDistance(center);
		if (R < Rmin) Rmin = R;
		if (R > Rmax) Rmax = R;

		T = graphCenter;

		deltaX = points[i].x - center.x;
		deltaY = points[i].y - center.y;

		//izraèun polarnega kota
		if (deltaX != 0)
		{
			polarAngle = atan(deltaY / deltaX);

			if (polarAngle >= 0)
			{
				if (deltaX < 0) polarAngle += M_PI;
			}
			else
			{
				if (deltaY < 0) polarAngle += 2 * M_PI;
				else  polarAngle += M_PI;
			}

			T.x += (180 / M_PI) * (polarAngle - M_PI) * graphDxPerDeg;


			T.y -= yMagnification * (R - Rnominal);		//pozitivne odklone radija rišem navzgor, v smer y-

			

			//T.DrawDot(pDC, RGB(255, 255, 0), 1, 1, 0, 0); //Brez zoom, offset
		}


	}




	CRectRotated graphArea;

	graphArea.setRect(graphCenter.x - 180 * graphDxPerDeg, graphCenter.y - yMagnification * (Rmin - Rnominal), graphCenter.x + 180 * graphDxPerDeg, graphCenter.y - yMagnification * (Rmax - Rnominal));

	

	//graphArea.DrawRectangle(pDC, RGB(0, 0, 255), 1, 0, 1, 0, 0);


	//Brez zoom, offset:
	//pDC->MoveTo(graphArea.left , graphCenter.y);
	//pDC->LineTo(graphArea.right, graphCenter.y);

	return true;
}

//Z 2D interpolacijo izraèuna vrednosti intenzitete na kolobarju z radijem R1 do R2, okoli toèke center. Naredi nR korakov v radialni in nFi korakov v kotni smeri
//prvi indeks teèe v smeri R, drugi v fi
//vrednosti shranjuje v openCV buffer v CMemoryBuffer objektu stretchRing
//smer fi pretvori v smer x, smer R v smer y
bool CMemoryBuffer::GetPolarCVbuffer(cv::Mat& stretchedRing, CPointFloat center, float R1, float R2, float delKroga, int nR, int nFi, float* avgIntensity)
{
	int j, i, position;


	float R, fi, Rstep, fiStep;
	float intensity;

	Rstep = (R2 - R1) / nR;
	fiStep = delKroga * 2 * M_PI / nFi;

	stretchedRing = cv::Mat(nR, nFi, CV_8UC1);

	*avgIntensity = 0;


	for (j = 0; j < nFi; j++)
	{
		fi = j * fiStep;

		for (i = 0; i < nR; i++)
		{
			R = R1 + i * Rstep;  //R narašèa z y 



			intensity = GetInterpolatedIntensity(center.x + R * cos(fi), center.y + R * sin(fi));

			if (intensity < 0) return false;



			//position = i*nFi + j;
			//stretchedRing.data[position] = intensity;

			stretchedRing.at<uchar>(i, j) =(int)intensity;



			*avgIntensity += intensity;
		}
	}

	*avgIntensity /= nR * nFi;

	return true;
}

//naloži ustrezno skrèeno sliko imageToPaste v sliko pravokotnik rect slike this 
//nastavi floatImage na true, èe imageToPaste hrani float intenzitete oziroma false, èe hrani int intenzitete
bool CMemoryBuffer::PasteImage(cv::Mat& imageToPaste, CRectRotated* rect, bool mirrorX, bool mirrorY, bool floatImage)
{

	int x, y, xRel, yRel, xOld, yOld, pasteWidth, pasteHeight, position, oldPosition;
	float xZoom, yZoom;

	xZoom = ((float)rect->width()) / imageToPaste.cols;
	yZoom = ((float)rect->height()) / imageToPaste.rows;


	for (x = rect->left(); x < rect->right(); x++)
	{
		for (y = rect->top(); y < rect->bottom(); y++)
		{
			//position = (this->height - 1 - y)*this->width + x;
			position = y * this->width + x;
			xRel = x - rect->left();
			yRel = y - rect->top();
			xOld = (int)xRel / xZoom;
			yOld = (int)yRel / yZoom;

			if (mirrorX) xOld = imageToPaste.cols - 1 - xOld;	//zrcaljenje v smeri x
			if (mirrorY) yOld = imageToPaste.rows - 1 - yOld;	//zrcaljenje v smeri y

			//oldPosition = (imageToPaste.rows - 1 - yOld) * imageToPaste.cols + xOld;


			int intensity;

			if (floatImage)
			{
				intensity = imageToPaste.at<float>(yOld, xOld);
			}
			else
			{
				oldPosition = yOld * imageToPaste.cols + xOld;
				intensity = imageToPaste.data[oldPosition];
			}

			this->buffer->data[position] = intensity;	//dela

		}
	}

	return true;
}

bool CMemoryBuffer::FilterMatrix1D(vector<vector<float>>& matrix, int filterDirection, bool circular, int filterInterval)
{
	int i, j, k, kBounded, kStart, kStop, counter;
	double sum;

	int filteredIndex, otherIndex;
	int filteredSize, otherSize;

	vector<vector<float>> tmpMatrix;

	tmpMatrix.resize(matrix.size(), vector<float>(matrix[0].size(), 0));

	if (filterInterval == 0) return true;  //filtiranje na intervalu 0 pomeni, da ostane matrika nespremenjena 

	if (filterInterval < 0) filterInterval *= -1;  	//dolžina intervala za filtriranje mora biti pozitivna


	if (filterDirection == 0)
	{
		filteredSize = matrix.size();
		otherSize = matrix[0].size();
	}
	else if (filterDirection == 1)
	{
		filteredSize = matrix[0].size();
		otherSize = matrix.size();
	}
	else return false;

	for (otherIndex = 0; otherIndex < otherSize; otherIndex++)
	{
		for (filteredIndex = 0; filteredIndex < filteredSize; filteredIndex++)
		{
			sum = 0;
			counter = 0;

			kStart = filteredIndex - filterInterval;
			kStop = filteredIndex + filterInterval;

			for (k = kStart; k <= kStop; k++)
			{
				if (circular)
				{
					kBounded = k;
					while (kBounded < 0) kBounded += filteredSize;
					while (kBounded >= filteredSize) kBounded -= filteredSize;

					if (filterDirection == 0) sum += matrix[kBounded][otherIndex];
					else sum += matrix[otherIndex][kBounded];
					counter++;
				}
				else
				{
					if (k >= 0 && k < filteredSize)
					{
						if (filterDirection == 0) sum += matrix[k][otherIndex];
						else sum += matrix[otherIndex][k];
						counter++;
					}
				}
			}

			if (counter > 0)
			{
				if (filterDirection == 0) tmpMatrix[filteredIndex][otherIndex] = sum / counter;
				else tmpMatrix[otherIndex][filteredIndex] = sum / counter;
			}
			else return false;

		}
	}

	for (i = 0; i < matrix.size(); i++)
	{
		for (j = 0; j < matrix[0].size(); j++)
		{
			matrix[i][j] = tmpMatrix[i][j];
		}
	}

	return true;
}



bool CMemoryBuffer::FilterMatrix2D(vector<vector<float>>& matrix, bool circularFirstCoord, bool circularSecondCoord, int filterIntervalFirstCoord, int filterIntervalSecondCoord)
{
	bool result1, result2;

	result1 = FilterMatrix1D(matrix, 0, circularFirstCoord, filterIntervalFirstCoord);

	result2 = FilterMatrix1D(matrix, 1, circularSecondCoord, filterIntervalSecondCoord);

	return result1 && result2;
}


bool CMemoryBuffer::FilterMatrix1Dfast(vector<vector<float>>& matrix, int filterDirection, bool circular, int filterInterval)
{
	int i, j, k, kBounded, kStart, kStop, kStartBounded, kStopBounded, kStartOld, kStopOld, kStartBoundedOld, kStopBoundedOld, counter;
	double sum;

	int filteredIndex, otherIndex;
	int filteredSize, otherSize;

	vector<vector<float>> tmpMatrix;

	tmpMatrix.resize(matrix.size(), vector<float>(matrix[0].size(), 0));

	if (filterInterval == 0) return true;  //filtiranje na intervalu 0 pomeni, da ostane matrika nespremenjena 

	if (filterInterval < 0) filterInterval *= -1;  	//dolžina intervala za filtriranje mora biti pozitivna

	if (filterDirection == 0)
	{
		filteredSize = matrix.size();
		otherSize = matrix[0].size();
	}
	else if (filterDirection == 1)
	{
		filteredSize = matrix[0].size();
		otherSize = matrix.size();
	}
	else return false;

	for (otherIndex = 0; otherIndex < otherSize; otherIndex++)
	{
		kStart = -filterInterval;
		kStop = filterInterval;

		sum = 0;
		counter = 0;

		//filtriranje na mestu prvega elementa izvedemo z obièajnim povpreèenjem
		for (k = kStart; k <= kStop; k++)
		{
			if (circular)
			{
				kBounded = k;
				while (kBounded < 0) kBounded += filteredSize;
				while (kBounded >= filteredSize) kBounded -= filteredSize;

				if (filterDirection == 0) sum += matrix[kBounded][otherIndex];
				else sum += matrix[otherIndex][kBounded];
				counter++;

			}
			else
			{
				if (k >= 0 && k < filteredSize)
				{
					if (filterDirection == 0) sum += matrix[k][otherIndex];
					else sum += matrix[otherIndex][k];
					counter++;
				}
			}
		}

		if (counter > 0)
		{
			if (filterDirection == 0) tmpMatrix[0][otherIndex] = sum / counter;
			else tmpMatrix[otherIndex][0] = sum / counter;
		}
		else
		{
			return false;
		}


		//izraèunamo štartno in konèno toèko povpreèenja prvega elementa
		if (circular)
		{
			kStartBoundedOld = kStart;
			while (kStartBoundedOld < 0) kStartBoundedOld += filteredSize;
			while (kStartBoundedOld >= filteredSize) kStartBoundedOld -= filteredSize;

			kStopBoundedOld = kStop;
			while (kStopBoundedOld < 0) kStopBoundedOld += filteredSize;
			while (kStopBoundedOld >= filteredSize) kStopBoundedOld -= filteredSize;
		}
		else
		{
			kStartBoundedOld = kStart;
			if (kStartBoundedOld < 0) kStartBoundedOld = 0;
			if (kStartBoundedOld >= filteredSize) kStartBoundedOld = filteredSize - 1;

			kStopBoundedOld = kStop;
			if (kStopBoundedOld < 0) kStopBoundedOld = 0;
			if (kStopBoundedOld >= filteredSize) kStopBoundedOld = filteredSize - 1;
		}

		for (filteredIndex = 1; filteredIndex < filteredSize; filteredIndex++)
		{
			kStart = filteredIndex - filterInterval;
			kStop = filteredIndex + filterInterval;

			if (circular)
			{
				kStartBounded = kStart;
				while (kStartBounded < 0) kStartBounded += filteredSize;
				while (kStartBounded >= filteredSize) kStartBounded -= filteredSize;

				kStopBounded = kStop;
				while (kStopBounded < 0) kStopBounded += filteredSize;
				while (kStopBounded >= filteredSize) kStopBounded -= filteredSize;

				counter = 2 * filterInterval + 1;
			}
			else
			{
				kStartBounded = kStart;
				if (kStartBounded < 0) kStartBounded = 0;
				if (kStartBounded >= filteredSize) kStartBounded = filteredSize - 1;

				kStopBounded = kStop;
				if (kStopBounded < 0) kStopBounded = 0;
				if (kStopBounded >= filteredSize) kStopBounded = filteredSize - 1;
				counter = kStopBounded - kStartBounded + 1;
			}

			if (kStartBounded != kStartBoundedOld)
			{
				//nova vsota nima veè enega elementa stare
				if (filterDirection == 0) sum -= matrix[kStartBoundedOld][otherIndex];
				else sum -= matrix[otherIndex][kStartBoundedOld];
			}
			if (kStopBounded != kStopBoundedOld)
			{
				//nova vsota ima en nov element glede na staro
				if (filterDirection == 0) sum += matrix[kStopBounded][otherIndex];
				else sum += matrix[otherIndex][kStopBounded];
			}

			if (counter > 0)
			{
				if (filterDirection == 0) tmpMatrix[filteredIndex][otherIndex] = sum / counter;
				else tmpMatrix[otherIndex][filteredIndex] = sum / counter;
			}
			else
			{
				return false;
			}


			kStartBoundedOld = kStartBounded;
			kStopBoundedOld = kStopBounded;
		}
	}

	for (i = 0; i < matrix.size(); i++)
	{
		for (j = 0; j < matrix[0].size(); j++)
		{
			matrix[i][j] = tmpMatrix[i][j];
		}
	}

	return true;
}


//hitrejša razlièica FilterMatrix2D
bool CMemoryBuffer::FilterMatrix2Dfast(vector<vector<float>>& matrix, bool circularFirstCoord, bool circularSecondCoord, int filterIntervalFirstCoord, int filterIntervalSecondCoord)
{
	bool result1, result2;

	result1 = FilterMatrix1Dfast(matrix, 0, circularFirstCoord, filterIntervalFirstCoord);

	result2 = FilterMatrix1Dfast(matrix, 1, circularSecondCoord, filterIntervalSecondCoord);

	return result1 && result2;
}

bool CMemoryBuffer::FilterImage1Dfast(cv::Mat& inputImage, cv::Mat& outputImage, int filterDirection, bool circular, int filterInterval)
{
	int i, j, k, kBounded, kStart, kStop, kStartBounded, kStopBounded, kStartOld, kStopOld, kStartBoundedOld, kStopBoundedOld, counter;
	double sum;

	int filteredIndex, otherIndex;
	int filteredSize, otherSize;

	if (filterDirection == 0)
	{
		filteredSize = inputImage.rows;
		otherSize = inputImage.cols;
	}
	else if (filterDirection == 1)
	{
		filteredSize = inputImage.cols;
		otherSize = inputImage.rows;
	}
	else return false;

	for (otherIndex = 0; otherIndex < otherSize; otherIndex++)
	{
		kStart = -filterInterval;
		kStop = filterInterval;

		sum = 0;
		counter = 0;

		//filtriranje na mestu prvega elementa izvedemo z obièajnim povpreèenjem
		for (k = kStart; k <= kStop; k++)
		{
			if (circular)
			{
				kBounded = k;
				while (kBounded < 0) kBounded += filteredSize;
				while (kBounded >= filteredSize) kBounded -= filteredSize;

				//if (filterDirection == 0) sum += matrix[kBounded][otherIndex];
				//else sum += matrix[otherIndex][kBounded];
				if (filterDirection == 0) sum += inputImage.at<float>(kBounded, otherIndex);
				else sum += inputImage.at<float>(otherIndex, kBounded);

				counter++;
			}
			else
			{
				if (k >= 0 && k < filteredSize)
				{
					if (filterDirection == 0) sum += inputImage.at<float>(k, otherIndex);
					else sum += inputImage.at<float>(otherIndex, k);
					counter++;
				}
			}

			if (filterDirection == 0) outputImage.at<float>(0, otherIndex) = sum / counter;
			else outputImage.at<float>(otherIndex, 0) = sum / counter;

		}

		//izracunamo startno in koncno tocko povprecenja prvega elementa
		if (circular)
		{
			kStartBoundedOld = kStart;
			while (kStartBoundedOld < 0) kStartBoundedOld += filteredSize;
			while (kStartBoundedOld >= filteredSize) kStartBoundedOld -= filteredSize;

			kStopBoundedOld = kStop;
			while (kStopBoundedOld < 0) kStopBoundedOld += filteredSize;
			while (kStopBoundedOld >= filteredSize) kStopBoundedOld -= filteredSize;
		}
		else
		{
			kStartBoundedOld = kStart;
			if (kStartBoundedOld < 0) kStartBoundedOld = 0;
			if (kStartBoundedOld >= filteredSize) kStartBoundedOld = filteredSize - 1;

			kStopBoundedOld = kStop;
			if (kStopBoundedOld < 0) kStopBoundedOld = 0;
			if (kStopBoundedOld >= filteredSize) kStopBoundedOld = filteredSize - 1;
		}

		for (filteredIndex = 1; filteredIndex < filteredSize; filteredIndex++)
		{
			kStart = filteredIndex - filterInterval;
			kStop = filteredIndex + filterInterval;

			if (circular)
			{
				kStartBounded = kStart;
				while (kStartBounded < 0) kStartBounded += filteredSize;
				while (kStartBounded >= filteredSize) kStartBounded -= filteredSize;

				kStopBounded = kStop;
				while (kStopBounded < 0) kStopBounded += filteredSize;
				while (kStopBounded >= filteredSize) kStopBounded -= filteredSize;

				counter = 2 * filterInterval + 1;
			}
			else
			{
				kStartBounded = kStart;
				if (kStartBounded < 0) kStartBounded = 0;
				if (kStartBounded >= filteredSize) kStartBounded = filteredSize - 1;

				kStopBounded = kStop;
				if (kStopBounded < 0) kStopBounded = 0;
				if (kStopBounded >= filteredSize) kStopBounded = filteredSize - 1;
				counter = kStopBounded - kStartBounded + 1;
			}

			if (kStartBounded != kStartBoundedOld)
			{
				//nova vsota nima vec enega elementa stare
				if (filterDirection == 0) sum -= inputImage.at<float>(kStartBoundedOld, otherIndex);
				else sum -= inputImage.at<float>(otherIndex, kStartBoundedOld);
			}
			if (kStopBounded != kStopBoundedOld)
			{
				//nova vsota ima en nov element glede na staro
				if (filterDirection == 0) sum += inputImage.at<float>(kStopBounded, otherIndex);
				else sum += inputImage.at<float>(otherIndex, kStopBounded);
			}

			if (filterDirection == 0) outputImage.at<float>(filteredIndex, otherIndex) = sum / counter;
			else  outputImage.at<float>(otherIndex, filteredIndex) = sum / counter;

			kStartBoundedOld = kStartBounded;
			kStopBoundedOld = kStopBounded;
		}
	}
	return 0;
}

bool CMemoryBuffer::FilterImage2Dfast(cv::Mat& inputImage, cv::Mat& outputImage, bool circularFirstCoord, bool circularSecondCoord, int filterIntervalFirstCoord, int filterIntervalSecondCoord)
{
	bool result1, result2;

	if (filterIntervalFirstCoord > 0)
	{
		result1 = FilterImage1Dfast(inputImage, outputImage, 0, circularFirstCoord, filterIntervalFirstCoord);


	http://answers.opencv.org/question/7682/copyto-and-clone-functions/
	cv:Mat tmpImage = outputImage.clone();

		result2 = FilterImage1Dfast(tmpImage, outputImage, 1, circularSecondCoord, filterIntervalSecondCoord);
	}
	else
	{
		result1 = true;
		result2 = FilterImage1Dfast(inputImage, outputImage, 1, circularSecondCoord, filterIntervalSecondCoord);
	}

	return result1 && result2;
}



//intentitete slike this nastavi na intenzitete v matriki intenstiyMatrix (z interpolacijo pretvori iz polarnih v karteziène koordinate)
//zoom < 1 omogoèa manjšanja slike, da pospešimo izris: èe je bila matrika redko vzorèena, s tem ne izgubimo kvalitete slike
//dela s openCV bufferjem
bool CMemoryBuffer::ImageFromPolarCVbuffer(cv::Mat& stretchedRing, CPointFloat center, float R1, float R2, float delKroga, float zoom)
{
	int x, y, nR, nFi;
	int iCeli, jCeli, position;
	float xZoom, yZoom;
	float i, j, iDec, jDec;	//i,j so decimalni indeksi v matriki intenzitet (v polarnih koordinatah), ki jih izraèunamo iz celoštevilskih karteziènih koordinat
	float Fj1, Fj2;
	float deltaX, deltaY, R, Rstep, fi, fiStep, interpolatedIntensity;

	byte Intensity[2][2];	//intenziteta pri štirih najbližjih celih indeksih

	if (zoom > 1) return false;

	nR = stretchedRing.rows;
	nFi = stretchedRing.cols;

	Rstep = (R2 - R1) / nR;
	fiStep = delKroga * 2 * M_PI / nFi;

	for (xZoom = 0; xZoom < this->width * zoom; xZoom++)
	{
		for (yZoom = 0; yZoom < this->height * zoom; yZoom++)
		{
			x = xZoom / zoom;
			y = yZoom / zoom;

			deltaX = x - center.x;
			deltaY = y - center.y;

			R = sqrt(pow(deltaX, 2) + pow(deltaY, 2));

			if (R >= R1 && R <= R2)	//preslikavo intenzitet iz polarnega v kartezièni koordinatni sistem izvajamo le na trenutnem kolobarju
			{
				//izraèun polarnega kota
				if (deltaX != 0)
				{
					fi = atan(deltaY / deltaX);

					if (fi >= 0)
					{
						if (deltaX < 0) fi += M_PI;
					}
					else
					{
						if (deltaY < 0) fi += 2 * M_PI;
						else  fi += M_PI;
					}
				}
				else
				{
					if (deltaY >= 0)
					{
						fi = M_PI / 2;
					}
					else
					{
						fi = 3 * M_PI / 2;
					}
				}

				i = (R - R1) / Rstep;
				j = fi / fiStep;

				if ((i >= 0) && (i < nR - 1) && (j >= 0) && (j < nFi - 1))
				{
					//interpolacija
					iCeli = (int)i;
					jCeli = (int)j;

					iDec = i - iCeli;
					jDec = j - jCeli;

					Intensity[0][0] = stretchedRing.at<float>(iCeli, jCeli);
					Intensity[0][1] = stretchedRing.at<float>(iCeli, jCeli + 1);
					Intensity[1][0] = stretchedRing.at<float>(iCeli + 1, jCeli);
					Intensity[1][1] = stretchedRing.at<float>(iCeli + 1, jCeli);

					Fj1 = (1 - iDec)*Intensity[0][0] + iDec * Intensity[1][0];
					Fj2 = (1 - iDec)*Intensity[0][1] + iDec * Intensity[1][1];

					interpolatedIntensity = (1 - jDec) * Fj1 + jDec * Fj2;

					//position = (this->height - 1 - yZoom) * this->width + xZoom;
					position = yZoom * this->width + xZoom;
					this->SetAverageIntensity(position, interpolatedIntensity);	//nastavimo intenziteto piksla slike this na interpolirano vrednost iz matrike intenzitet 
				}

			}


		}
	}

	return true;
}

int CMemoryBuffer::VerticalIntensity(int filter)
{

	int sumintensity, i, j, sumTemp, deltay, tmp, sumintensityB, sumintensityG, sumintensityR;
	int stevecC, stevecB, vsotaC, vsotaB, position;

	CreateIntensity();
	CreateIntensityRed();
	CreateIntensityGreen();
	CreateIntensityBlue();

	stevecC = 0;
	stevecB = 0;
	vsotaC = 0;
	vsotaB = 0;
	deltay = abs(rect.top() - rect.bottom()) + 1;
	vertical = true;
	sumTemp = 0;
	position = 0;


	//if ( intensity != NULL)
	{

		if (rect.left() > rect.right())
		{
			tmp = rect.left();
			rect.setLeft( rect.right());
			rect.setRight(tmp);
		}

		if (rect.top() > rect.bottom())
		{
			tmp = rect.top();
			rect.setTop(rect.bottom());
			rect.setBottom(tmp);
		}

		if (rect.bottom() == buffer->rows)
		{
			rect.setBottom(buffer->rows -1);
			deltay--;
		}

		if (rect.right() == buffer->cols)
			rect.setRight(buffer->cols - 1);

		if ((rect.left() >= 0) && (rect.right() >= 0) && (rect.top() >= 0) && (rect.bottom() >= 0) && (rect.left() < buffer->cols) && (rect.right() < buffer->cols) && (rect.top() < buffer->rows) && (rect.bottom() < buffer->rows))
		{

			for (i = rect.left(); i <= rect.right(); i++)
			{
				sumintensity = 0;
				sumintensityB = 0;
				sumintensityG = 0;
				sumintensityR = 0;

				for (j = rect.top(); j <= rect.bottom(); j++)
				{
					position = GetPosition(i, j);
					sumTemp = GetAverageIntensity(i,j);
					sumintensity += sumTemp;

					if (this->depth == 3)
					{
						sumintensityB += GetBlue(i, j);
						sumintensityG += GetGreen(i, j);
						sumintensityR += GetRed(i, j);
					}

				}

				if (deltay > 0)
				{
					intensity[i] = sumintensity / deltay;
					intensityBlue[i] = sumintensityB / deltay;
					intensityGreen[i] = sumintensityG / deltay;
					intensityRed[i] = sumintensityR / deltay;
				}

				if (intensity[i] < 128)
				{
					vsotaC += intensity[i];
					stevecC++;
				}
				else
				{
					vsotaB += intensity[i];
					stevecB++;
				}
			}

			if (filter > 1)
			{
				stevecC = 0;
				stevecB = 0;
				vsotaC = 0;
				vsotaB = 0;

				for (i = rect.left(); i < rect.right() - filter; i++)
				{
					sumTemp = 0;
					for (j = 0; j < filter; j++)
						sumTemp += intensity[i + j];

					intensity[i] = sumTemp / filter;

					if (intensity[i] < 128)
					{
						vsotaC += intensity[i];
						stevecC++;
					}
					else
					{
						vsotaB += intensity[i];
						stevecB++;
					}
				}
			}
		}
		else
			return 0;

		if (stevecC != 0)
			vsotaC /= stevecC;

		if (stevecB != 0)
			vsotaB /= stevecB;

		return (vsotaC + vsotaB) / 2;
	}

	return 1;
}

int CMemoryBuffer::HorizontalIntensity(int filter)
{
	int sumHoryInt, i, j, sumTemp, deltay, tmp, position, sumintensityB, sumintensityG, sumintensityR;
	int stevecC, stevecB, vsotaC, vsotaB;

	/*CreateIntensity();
	CreateIntensityRed();
	CreateIntensityGreen();
	CreateIntensityBlue();*/

	vertical = false;
	sumTemp = 0;
	stevecC = 0;
	stevecB = 0;
	vsotaC = 0;
	vsotaB = 0;
	deltay = abs(rect.left() - rect.right()) + 1;
	position = 0;


	//if ( intensity != NULL)
	{
		if (rect.top() < 0)
		{
			rect.setTop(0);
		}
		if (rect.left() < 0)
		{
			rect.setLeft(0);
		}

		if (rect.left() > rect.right())
		{
			tmp = rect.left();
			rect.setLeft( rect.right());
			rect.setRight(tmp);
		}

		if (rect.top() > rect.bottom())
		{
			tmp = rect.top();
			rect.setTop(rect.bottom());
			rect.setBottom(tmp);
		}

		if (rect.bottom() > buffer->rows)
			rect.setBottom(buffer->rows - 1);


		if (rect.right() > buffer->cols)
		{
			rect.setRight(buffer->cols - 1);
			deltay--;
		}



		if ((rect.left() >= 0) && (rect.right() >= 0) && (rect.top() >= 0) && (rect.bottom() >= 0) && (rect.left() < buffer->cols) && (rect.right() < buffer->cols) && (rect.top() < buffer->rows) && (rect.bottom() < buffer->rows))
		{
			for (j = rect.top(); j <= rect.bottom(); j++)
			{
				sumHoryInt = 0;
				sumintensityB = 0;
				sumintensityG = 0;
				sumintensityR = 0;

				for (i = rect.left(); i <= rect.right(); i++)
				{

					sumTemp = GetAverageIntensity(i, j);
					sumHoryInt += sumTemp;

					if (this->depth == 3)
					{
						sumintensityB += GetBlue(i, j);
						sumintensityG += GetGreen(i, j);
						sumintensityR += GetRed(i, j);
					}
				}
				if (deltay > 0)
				{
					intensity[j] = sumHoryInt / deltay;
					//intensityRed[j] = sumintensityR / deltay;
					//intensityGreen[j] = sumintensityG / deltay;
					//intensityBlue[j] = sumintensityB / deltay;
				}

				if (intensity[j] < 128)
				{
					vsotaC += intensity[j];
					stevecC++;
				}
				else
				{
					vsotaB += intensity[j];
					stevecB++;
				}
			}
			if (filter > 1)
			{
				stevecC = 0;
				stevecB = 0;
				vsotaC = 0;
				vsotaB = 0;

				for (i = rect.top(); i < rect.bottom() - filter; i++)
				{
					sumTemp = 0;
					for (j = 0; j < filter; j++)
						sumTemp += intensity[i + j];

					//intensity[j] = sumTemp / filter;	//narobe 
					intensity[i] = sumTemp / filter;		//Popravil Martin

					if (intensity[j] < 128)
					{
						vsotaC += intensity[j];
						stevecC++;
					}
					else
					{
						vsotaB += intensity[j];
						stevecB++;
					}
				}
			}

		}
		else
			return 0;


		if (stevecC != 0)
			vsotaC /= stevecC;

		if (stevecB != 0)
			vsotaB /= stevecB;

		return (vsotaC + vsotaB) / 2;
	}

	return 1;
}

int CMemoryBuffer::RotatedIntensity(CRectRotated rectRot, bool sumDirection01, int sumStep, int otherStep, bool interpolate, int filter)
{
	CreateIntensityRotated();

	int nPoints = 0;

	int sumDistance, otherDistance;
	int sirina, visina, position;
	int counter;
	float sumSize, otherSize;
	float x, y;
	float intensity;
	double Isum, IsumOfSums;

	CPointFloat testPoint;
	testPoint.SetPointFloat(rectRot.topLeft());

	CPointFloat T00, T01, T10, deltaTsum, deltaTother, TlineStart, T;

	if (rectRot.polygonPoints.size() > 3)
	{
		T00 = rectRot.polygonPoints[0];

		if (sumDirection01)
		{
			T01 = rectRot.polygonPoints[1];
			T10 = rectRot.polygonPoints[3];
		}
		else
		{
			T01 = rectRot.polygonPoints[3];
			T10 = rectRot.polygonPoints[1];
		}
	}
	else
	{
		T00.SetPointFloat(rectRot.topLeft());

		if (sumDirection01)
		{
			T01.SetPointFloat(rectRot.bottomLeft());
			T10.SetPointFloat(rectRot.topRight());
		}
		else
		{
			T01.SetPointFloat(rectRot.topRight());
			T10.SetPointFloat(rectRot.bottomLeft());
		}

	}

	deltaTsum = T01 - T00;
	sumSize = T00.GetDistance(T01);

	deltaTother = T10 - T00;
	otherSize = T00.GetDistance(T10);

	sirina = this->width;  //sirina slike
	visina = this->height;  //višina slike

	for (otherDistance = 0; otherDistance < otherSize; otherDistance += otherStep)
	{
		TlineStart = T00 + deltaTother * ((float)otherDistance) / otherSize;

		Isum = 0;
		counter = 0;

		for (sumDistance = 0; sumDistance < sumSize; sumDistance += sumStep)
		{
			T = TlineStart + deltaTsum * ((float)sumDistance) / sumSize;

			if (interpolate)
			{
				intensity = this->GetInterpolatedIntensity(T.x, T.y);	//GetInterpolatedIntensity vrne -1, če smo izven slike
			}
			else
			{
				x = (int)(T.x + 0.5);
				y = (int)(T.y + 0.5);

				position = y * sirina + x;

				if (position >= 0 && position < sirina * visina)
				{
					intensity = this->buffer->data[position];
				}
				else intensity = -1;
			}

			if (intensity >= 0)
			{
				Isum += intensity;
				counter++;
			}
		}


		if (counter > 0)
		{
			intensityRotated[nPoints] = Isum / counter;
		}
		else
		{
			intensityRotated[nPoints] = -1;
		}

		nPoints++;

	}



	//povprečimo filter sosednjih intenzitet v smeri "other"
	if (filter > 1)
	{
		intensityRotatedLength = nPoints - filter + 1;

		for (int i = 0; i < intensityRotatedLength; i++)
		{
			IsumOfSums = 0;

			counter = 0;

			for (int j = 0; j < filter; j++)
			{
				intensity = intensityRotated[i + j];
				if (intensity >= 0)
				{
					IsumOfSums += intensityRotated[i + j];
					counter++;
				}
			}
			if (counter > 0) intensityRotated[i] = IsumOfSums / counter;
			else intensityRotated[i] = -1;
		}

	}
	else
	{
		intensityRotatedLength = nPoints;
	}

	//izračunamo točke grafa

	CPointFloat TdrawLineStart, Tdraw;

	for (int i = 0; i < intensityRotatedLength; i++)
	{
		TdrawLineStart = T00 + deltaTother * ((float)(i * otherStep)) / otherSize;

		//Tdraw = TdrawLineStart + deltaTsum * ((float)intensityRotated[i]) / 255;
		Tdraw = TdrawLineStart + deltaTsum * ((float)intensityRotated[i]) / (2 * sumSize);

		intensityRotatedGraph[i] = Tdraw;

	//	intensityRotatedGraph[i].x = TdrawLineStart.x;
	//	intensityRotatedGraph[i].y = 

	}


	return 1;
}

QGraphicsPathItem* CMemoryBuffer::DrawRotatedIntensity(QPen pen)
{
	
	QPainterPath path;

	QGraphicsPathItem* path2 = new QGraphicsPathItem();


	


	path.moveTo(intensityRotatedGraph[0].x, intensityRotatedGraph[0].y);
	
	//rectPath.lineTo(80.0, 70.0);
	//rectPath.lineTo(20.0, 70.0);
	

//	intensityRotatedGraph[i];

	for (int i = 0; i < intensityRotatedLength; i++)
	{
		path.lineTo(intensityRotatedGraph[i].x, intensityRotatedGraph[i].y);
	}
	
	path2->setPath(path);
	path2->setPen(pen);
	path2->setZValue(2);


	return path2;
}

QGraphicsPathItem * CMemoryBuffer::DrawIntensity(QPen pen)
{
	
	QPainterPath path;

	QGraphicsPathItem* path2 = new QGraphicsPathItem();
	

		path.moveTo(rect.left(),rect.top());
		path.lineTo(rect.left(), rect.bottom());
		path.lineTo(rect.right(), rect.bottom());
		path.lineTo(rect.right(), rect.top());
		path.lineTo(rect.left(), rect.top());

	
		if (vertical)
		{
			path.moveTo(rect.left(), rect.top());


			for (int ix = rect.left(); ix < rect.right(); ix++)
				path.lineTo(ix,rect.top()+ intensity[ix] / 2); //,RGB(255,255,0));
		}
		else
		{
			path.moveTo(rect.left(), rect.top());



			for (int ix = rect.top(); ix < rect.bottom(); ix++)
				path.lineTo((int)(rect.left()+intensity[ix] / 2) , ix); //,RGB(255,255,0));
		}


	
		path2->setPen(pen);
		path2->setPath(path);
		path2->setZValue(2);
	return path2;
}

QGraphicsPathItem * CMemoryBuffer::DrawVerticalIntensity(QPen pen)
{
	return nullptr;
}

QGraphicsPixmapItem * CMemoryBuffer::DrawEdges(QRgb value)
{
	int  j, r, g, b;

	Mat test;
	cvtColor(*buffer, test, COLOR_GRAY2RGB);
	Mat DrawImage;
	Rect testRect;

	DrawImage = test(Rect(0, 0, test.cols, test.rows));
	QImage image(DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	QImage destImage(edgesXRectangle.width(), edgesXRectangle.height(), QImage::Format_RGB888);


	QPainter painter(&image);
	painter.begin(&image);
	painter.setPen(Qt::red);


	

	int y, x;

	for (y = edgesXRectangle.top(); y < edgesXRectangle.bottom(); y++)
	{
		for (x = edgesXRectangle.left(); x < edgesXRectangle.right(); x++)
		{
			if (Xedges[y][x] > 0)
			{
				//pDC->SetPixel(xDraw, yDraw, B);
				image.setPixel(x, y , value);
			}
		}
	}
	for (x = edgesYRectangle.left(); x < edgesYRectangle.right(); x++)
	{
		for (y = edgesYRectangle.top(); y < edgesYRectangle.bottom(); y++)
		{
			if (Yedges[x][y] > 0)
			{
				//pDC->SetPixel(xDraw * zoomFactor + xOffset, yDraw * zoomFactor + yOffset, B);
				image.setPixel(x, y, value);
			}
		}
	}

	destImage = image.copy(rect);

	QGraphicsPixmapItem*  QGaphicsPixmap = new QGraphicsPixmapItem;
	QGaphicsPixmap->setPixmap(QPixmap::fromImage(destImage));
	QGaphicsPixmap->setOffset(QPoint(rect.left(), rect.top()));
	QGaphicsPixmap->setZValue(0);// da se vedno narise v ozadju

	return QGaphicsPixmap;



}

int CMemoryBuffer::RadialIntensity(CPointFloat center, int Rmin, int Rmax, int nFi, int filter)
{
	bool znotrajSlike;
	int pozicija, nR, Rindeks, fiIndex, x, y;
	int intensity;
	float IavgRad, sumTemp;
	float R, fi, sinFi, cosFi, Rstep, fiStep;

	CreateIntensityRadial();

	nR = Rmax - Rmin;

	fiStep = 2 * M_PI / nFi;



	for (fiIndex = 0; fiIndex < nFi; fiIndex++)
	{

		IavgRad = 0;

		fi = fiIndex * fiStep;
		sinFi = sin(fi);
		cosFi = cos(fi);

		znotrajSlike = true;

		for (R = Rmin; R < Rmax; R++)
		{
			x = (int)(center.x + R * cosFi + 0.5);

			y = (int)(center.y + R * sinFi + 0.5);

			if (x < 0 || y < 0 || x >= this->width || y >= this->height)
			{
				znotrajSlike = false;

				break;
			}


			pozicija = y * this->width + x;
			intensity = this->buffer->data[pozicija];

			IavgRad += intensity;
		}

		if (!znotrajSlike) IavgRad = 0;
		else IavgRad /= nR;


		this->intensityRadial[fiIndex] = IavgRad;
	}

	if (filter > 1)
	{

		for (fiIndex = 0; fiIndex < nFi - filter + 1; fiIndex++)
		{
			sumTemp = 0;

			for (int sosed = 0; sosed < filter; sosed++)
			{
				sumTemp += this->intensityRadial[fiIndex + sosed];
			}

			this->intensityRadial[fiIndex] = sumTemp / filter;

		}
	}

	return 1;
}

int CMemoryBuffer::AngularIntensity(CPointFloat center, int Rmin, int Rmax, int nFi, int filter)
{
	bool znotrajSlike;

	int pozicija, nR, Rindex, fiIndex, x, y;
	int intensity;
	float IavgAng, sumTemp;



	float R, Rstep, fiStep;
	float* fi;
	float* sinFi;
	float* cosFi;


	CreateIntensityAngular();


	Rstep = 1;
	fiStep = 2 * M_PI / nFi;



	fi = new float[nFi];
	sinFi = new float[nFi];
	cosFi = new float[nFi];

	for (fiIndex = 0; fiIndex < nFi; fiIndex++)
	{
		fi[fiIndex] = fiIndex * fiStep;
		sinFi[fiIndex] = sin(fi[fiIndex]);
		cosFi[fiIndex] = cos(fi[fiIndex]);
	}

	for (Rindex = 0; Rindex < Rmax - Rmin; Rindex++)
	{
		R = Rmin + Rindex;

		IavgAng = 0;

		znotrajSlike = true;

		for (fiIndex = 0; fiIndex < nFi; fiIndex++)
		{
			x = (int)(center.x + R * cosFi[fiIndex] + 0.5);

			y = (int)(center.y + R * sinFi[fiIndex] + 0.5);

			if (x < 0 || y < 0 || x >= this->width || y >= this->height)
			{
				znotrajSlike = false;

				break;
			}

			pozicija = y * this->width + x;
			intensity = this->buffer->data[pozicija];

			IavgAng += intensity;
		}

		if (!znotrajSlike) IavgAng = 0;
		else IavgAng /= nFi;


		this->intensityAngular[Rindex] = IavgAng;
	}

	delete[] fi;
	delete[] sinFi;
	delete[] cosFi;

	if (filter > 1)
	{

		for (Rindex = 0; Rindex < Rmax - Rmin - filter + 1; Rindex++)
		{
			sumTemp = 0;

			for (int sosed = 0; sosed < filter; sosed++)
			{
				sumTemp += this->intensityAngular[Rindex + sosed];
			}

			this->intensityAngular[Rindex] = sumTemp / filter;

		}
	}

	return 1;
}

void CMemoryBuffer::MeanShiftClustering(int winSize, cv::TermCriteria termCriteria)
{
	QPoint calculatedGravity, predictedGravity;
	int rowDiff, colDiff;
	float diff = 0.0f;

	std::vector<QPoint> clusters;

	if (!termCriteria.isValid())
	{
		termCriteria.maxCount = 10;
		termCriteria.epsilon = 5;
	}


	int iterations;
	for (int row = 0; row < this->buffer->rows; ++row)
	{
		for (int col = 0; col < this->buffer->cols; ++col)
		{
			predictedGravity = QPoint(row, col);//Predpostavljeno težišče
			iterations = 0;

			//Iterativno popravljamo predpostavljeno težišče
			do
			{
				calculatedGravity = CalculateGravity(predictedGravity, winSize);
				predictedGravity = calculatedGravity;

				rowDiff = abs(predictedGravity.x() - calculatedGravity.x());
				colDiff = abs(predictedGravity.y() - calculatedGravity.y());

				diff = std::sqrt(rowDiff^2 + colDiff^2);
				iterations++;

			} while (diff > termCriteria.epsilon || iterations < termCriteria.maxCount);


		}
	}
}

QPoint CMemoryBuffer::CalculateGravity(QPoint center, int winSize)
{
	return QPoint(0.0f, 0.0f);
}





//Popravil Martin za pravilen izris grafa povpreène intenzitete stolpcev oziroma vrstic
//z upoštevanjem zoom, offset:



//detects all transitions from x = 0 to field_width
int CMemoryBuffer::DetectTransitions(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;

	tempIntens = 0;
	detectedPoints.clear();
	detectedPointsLightToDark.clear();
	detectedPointsDarkToLight.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensity[rect.left() + 1] + intensity[rect.left() + 2] + intensity[rect.left() + 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensity[rect.top() + 1] + intensity[rect.top() + 2] + intensity[rect.top() + 3]) / 3;
	}

	for (x = begin; x < end; x++)
	{
		if (tempIntens > threshold) //white
		{
			if (intensity[x] < blackThreshold) //black
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensity[x];
				pointCount++;
			}
		}
		else //black
		{
			if (intensity[x] > whiteThreshold) //white
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsDarkToLight.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsDarkToLight.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensity[x];
				pointCount++;
			}
		}
	}


	return pointCount;
}

int CMemoryBuffer::DetectTransitions()
{
	return DetectTransitions(whiteThreshold, blackThreshold);
}
int CMemoryBuffer::DetectTransitionsBackward(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;
	tempIntens = 0;
	detectedPoints.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensity[rect.right() - 1] + intensity[rect.right() - 2] + intensity[rect.right() - 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensity[rect.bottom() - 1] + intensity[rect.bottom() - 2] + intensity[rect.bottom() - 3]) / 3;
	}

	for (x = end; x > begin; x--)
	{
		if (tempIntens > threshold) //white
		{
			if (intensity[x] < blackThreshold) //black
			{
				if (vertical)
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
				else
					detectedPoints.push_back(CPointFloat((sum / 2), x));

				tempIntens = intensity[x];
				pointCount++;
			}
		}
		else //black
		{
			if (intensity[x] > whiteThreshold) //white
			{
				if (vertical)
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
				else
					detectedPoints.push_back(CPointFloat((sum / 2), x));

				tempIntens = intensity[x];
				pointCount++;
			}
		}
	}

	return pointCount;
}

int CMemoryBuffer::DetectTransitionsBackward()
{
	return DetectTransitionsBackward(whiteThreshold, blackThreshold);
}
int CMemoryBuffer::DetectTransitionsWhiteToBlack(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;
	threshold = (whiteThreshold);
	tempIntens = 0;

	detectedPointsLightToDark.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensity[rect.left() + 1] + intensity[rect.left() + 2] + intensity[rect.left() + 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensity[rect.top() + 1] + intensity[rect.top() + 2] + intensity[rect.top() + 3]) / 3;
	}

	for (x = begin; x < end; x++)
	{
		if (tempIntens > threshold) //white
		{
			if (intensity[x] < blackThreshold) //black
			{
				if (vertical)
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				else
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));

				tempIntens = intensity[x];
				pointCount++;
			}
		}
		else //black
		{
			tempIntens = intensity[x]; //until intensity is white		
		}
	}

	return pointCount;
}

int CMemoryBuffer::DetectTransitionsBlackToWhite(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;
	tempIntens = 0;

	detectedPointsDarkToLight.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensity[rect.left() + 1] + intensity[rect.left() + 2] + intensity[rect.left() + 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensity[rect.top() + 1] + intensity[rect.top() + 2] + intensity[rect.top() + 3]) / 3;
	}


	for (x = begin; x < end; x++)
	{
		if (tempIntens > threshold) //white
		{
			tempIntens = intensity[x]; //until intensity is black	
		}
		else //black
		{
			if (intensity[x] > whiteThreshold) //black
			{
				if (vertical)
					detectedPointsDarkToLight.push_back(CPointFloat(x, (sum / 2)));
				else
					detectedPointsDarkToLight.push_back(CPointFloat((sum / 2), x));

				tempIntens = intensity[x];
				pointCount++;
			}

		}
	}


	return pointCount;
}

int CMemoryBuffer::DetectTransitionsWhiteToBlackBackWard(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;
	threshold = whiteThreshold;
	

	tempIntens = 0;

	detectedPointsLightToDark.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensity[rect.right() - 1] + intensity[rect.right() - 2] + intensity[rect.right() - 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensity[rect.bottom() - 1] + intensity[rect.bottom() - 2] + intensity[rect.bottom() - 3]) / 3;
	}

	for (x = end; x > begin; x--)
	{
		if (tempIntens > threshold) //white
		{
			if (intensity[x] < blackThreshold) //black
			{
				if (vertical)
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				else
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));

				tempIntens = intensity[x];
				pointCount++;
			}
		}
		else //black
		{
			tempIntens = intensity[x]; //until intensit is white		
		}
	}

	return pointCount;
}

int CMemoryBuffer::DetectTransitionsBlackToWhiteBackWard(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;
	tempIntens = 0;

	detectedPointsDarkToLight.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensity[rect.right() - 1] + intensity[rect.right() - 2] + intensity[rect.right() - 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensity[rect.bottom() - 1] + intensity[rect.bottom() - 2] + intensity[rect.bottom() - 3]) / 3;
	}

	for (x = end; x > begin; x--)
	{
		if (tempIntens > threshold) //white
		{
			tempIntens = intensity[x]; //until intensity is black	
		}
		else //black
		{
			if (intensity[x] > whiteThreshold) //black
			{
				if (vertical)
					detectedPointsDarkToLight.push_back(CPointFloat(x, (sum / 2)));
				else
					detectedPointsDarkToLight.push_back(CPointFloat((sum / 2), x));

				tempIntens = intensity[x];
				pointCount++;
			}

		}
	}

	return pointCount;
}

int CMemoryBuffer::DetectTransitionsRed(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;

	tempIntens = 0;
	detectedPoints.clear();
	detectedPointsLightToDark.clear();
	detectedPointsDarkToLight.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensityRed[rect.left()] + intensityRed[rect.left() + 1] + intensityRed[rect.left() + 2]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensityRed[rect.top()] + intensityRed[rect.top() + 1] + intensityRed[rect.top() + 2]) / 3;
	}

	for (x = begin; x < end; x++)
	{
		if (tempIntens > threshold) //white
		{
			if (intensityRed[x] < blackThreshold) //black
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensityRed[x];
				pointCount++;
			}
		}
		else //black
		{
			if (intensityRed[x] > whiteThreshold) //white
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsDarkToLight.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsDarkToLight.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensityRed[x];
				pointCount++;
			}
		}
	}


	return pointCount;
}

int CMemoryBuffer::DetectTransitionsGreen(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;

	tempIntens = 0;
	detectedPoints.clear();
	detectedPointsLightToDark.clear();
	detectedPointsDarkToLight.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensityGreen[rect.left()] + intensityGreen[rect.left() + 1] + intensityGreen[rect.left() + 2]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensityGreen[rect.top()] + intensityGreen[rect.top() + 1] + intensityGreen[rect.top() + 2]) / 3;
	}

	for (x = begin; x < end; x++)
	{
		if (tempIntens > threshold) //white
		{
			if (intensityGreen[x] < blackThreshold) //black
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensityGreen[x];
				pointCount++;
			}
		}
		else //black
		{
			if (intensityGreen[x] > whiteThreshold) //white
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsDarkToLight.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsDarkToLight.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensityGreen[x];
				pointCount++;
			}
		}
	}


	return pointCount;
}

int CMemoryBuffer::DetectTransitionsGreenBack(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;

	tempIntens = 0;
	detectedPoints.clear();
	detectedPointsLightToDark.clear();
	detectedPointsDarkToLight.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensityGreen[rect.right() - 1] + intensityGreen[rect.right() - 2] + intensityGreen[rect.right() - 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensityGreen[rect.bottom() - 1] + intensityGreen[rect.bottom() - 2] + intensityGreen[rect.bottom() - 3]) / 3;
	}

	for (x = end; x > begin; x--)
	{
		if (tempIntens > threshold) //white
		{
			if (intensityGreen[x] < blackThreshold) //black
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensityGreen[x];
				pointCount++;
			}
		}
		else //black
		{
			if (intensityGreen[x] > whiteThreshold) //white
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsDarkToLight.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsDarkToLight.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensityGreen[x];
				pointCount++;
			}
		}
	}


	return pointCount;
}

int CMemoryBuffer::DetectTransitionsBlue(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;

	tempIntens = 0;
	detectedPoints.clear();
	detectedPointsLightToDark.clear();
	detectedPointsDarkToLight.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensityBlue[rect.left()] + intensityBlue[rect.left() + 1] + intensityBlue[rect.left() + 2]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensityBlue[rect.top()] + intensityBlue[rect.top() + 1] + intensityBlue[rect.top() + 2]) / 3;
	}

	for (x = begin; x < end; x++)
	{
		if (tempIntens > threshold) //white
		{
			if (intensityBlue[x] < blackThreshold) //black
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensityBlue[x];
				pointCount++;
			}
		}
		else //black
		{
			if (intensityBlue[x] > whiteThreshold) //white
			{
				if (vertical)
				{
					detectedPoints.push_back(CPointFloat(x, (sum / 2)));
					detectedPointsDarkToLight.push_back(CPointFloat(x, (sum / 2)));
				}
				else
				{
					detectedPoints.push_back(CPointFloat((sum / 2), x));
					detectedPointsDarkToLight.push_back(CPointFloat((sum / 2), x));
				}

				tempIntens = intensityBlue[x];
				pointCount++;
			}
		}
	}


	return pointCount;
}

int CMemoryBuffer::DetectTransitionsGreenToBlackBackWard(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;
	tempIntens = 0;

	detectedPointsLightToDark.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensityGreen[rect.right() - 1] + intensityGreen[rect.right() - 2] + intensityGreen[rect.right() - 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensityGreen[rect.bottom() - 1] + intensityGreen[rect.bottom() - 2] + intensityGreen[rect.bottom() - 3]) / 3;
	}

	for (x = end; x > begin; x--)
	{
		if (tempIntens > threshold) //white
		{
			if (intensityGreen[x] < blackThreshold) //black
			{
				if (vertical)
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				else
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));

				tempIntens = intensityGreen[x];
				pointCount++;
			}
		}
		else //black
		{
			tempIntens = intensityGreen[x]; //until intensit is white		
		}
	}

	return pointCount;
}

int CMemoryBuffer::DetectTransitionsGreenToBlack(int whiteThreshold, int blackThreshold)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	threshold = (whiteThreshold + blackThreshold) / 2;
	tempIntens = 0;

	detectedPointsLightToDark.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensityGreen[rect.left()] + intensityGreen[rect.left() + 1] + intensityGreen[rect.left() + 2]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensityGreen[rect.top()] + intensityGreen[rect.top() + 1] + intensityGreen[rect.top() + 2]) / 3;
	}

	for (x = begin; x < end; x++)
	{
		if (tempIntens > threshold) //white
		{
			if (intensityGreen[x] < blackThreshold) //black
			{
				if (vertical)
					detectedPointsLightToDark.push_back(CPointFloat(x, (sum / 2)));
				else
					detectedPointsLightToDark.push_back(CPointFloat((sum / 2), x));

				tempIntens = intensityGreen[x];
				pointCount++;
			}
		}
		else //black
		{
			tempIntens = intensityGreen[x]; //until intensity is white		
		}
	}

	return pointCount;
}

int CMemoryBuffer::DetectTransitions(int difference)
{
	int x, sum, tempIntens, pointCount, threshold;
	int begin, end;

	pointCount = 0;
	threshold = 128;
	tempIntens = 0;
	detectedPoints.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensity[rect.left() + 1] + intensity[rect.left() + 2] + intensity[rect.left() + 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensity[rect.top() + 1] + intensity[rect.top() + 2] + intensity[rect.top() + 3]) / 3;
	}

	for (x = begin; x < end; x++)
	{
		if (abs(intensity[x] - tempIntens) > difference)
		{
			if (vertical)
			{
				detectedPoints.push_back(CPointFloat(x, (sum / 2)));


			}
			else
			{
				detectedPoints.push_back(CPointFloat((sum / 2), x));


			}

			tempIntens = intensity[x];
			pointCount++;


		}
	}

	return pointCount;
}

int CMemoryBuffer::DetectTransitionsBackward(int difference)
{
	int x, sum, tempIntens, threshold, pointCount;
	int begin, end;

	pointCount = 0;

	tempIntens = 0;
	detectedPoints.clear();

	if (vertical)
	{
		begin = rect.left();
		end = rect.right();
		sum = (rect.top() + rect.bottom());
		tempIntens = (intensity[rect.right() - 1] + intensity[rect.right() - 2] + intensity[rect.right() - 3]) / 3;
	}
	else
	{
		begin = rect.top();
		end = rect.bottom();
		sum = (rect.left() + rect.right());
		tempIntens = (intensity[rect.bottom() - 1] + intensity[rect.bottom() - 2] + intensity[rect.bottom() - 3]) / 3;
	}

	for (x = end; x > begin; x--)
	{
		if (abs(intensity[x] - tempIntens) > difference)
		{
			if (vertical)
			{
				detectedPoints.push_back(CPointFloat(x, (sum / 2)));
			}
			else
			{
				detectedPoints.push_back(CPointFloat((sum / 2), x));
			}

			tempIntens = intensity[x];
			pointCount++;
		}
	}

	return pointCount;
}

void CMemoryBuffer::Flip90andConcatenate(BYTE* pBuf, int width, int height, int depth, QRect focusRectangle, int imageIndex)
{
	int i, j, i1, index, intensity, columnIndex = 0, rowIndex, sum;
	rowIndex = (focusRectangle.height() * imageIndex) + focusRectangle.height() - 1;
	i1 = 0;
	sum = 0;
	for (j = focusRectangle.top(); j < focusRectangle.bottom(); j++)
	{
		i1++;

		for (i = focusRectangle.left(); i < focusRectangle.right(); i++)
		{
			intensity = pBuf[((height - j)* width) + i]/* + i1 * 0.05*/;

			if (intensity > 255)
			{
				intensity = 255;
			}
			index = (columnIndex * this->width) + rowIndex;
			buffer->data[index] = intensity;
			columnIndex++;
		}
		columnIndex = 0;
		rowIndex--;
	}
}

void CMemoryBuffer::Flip90andConcatenateLeft(BYTE* pBuf, int width, int height, int depth, QRect focusRectangle, int imageIndex)
{
	int i, j, i1, index, intensity, columnIndex = 0, rowIndex, sum;
	rowIndex = (focusRectangle.height() * imageIndex) + focusRectangle.height() - 1;
	i1 = 0;
	sum = 0;
	for (j = focusRectangle.top(); j < focusRectangle.bottom(); j++)
	{
		i1++;

		for (i = focusRectangle.left(); i < focusRectangle.right(); i++)
		{
			intensity = pBuf[(j * width) + i] + i1 * 0.05;

			if (intensity > 255)
			{
				intensity = 255;
			}
			index = (columnIndex * this->width) + rowIndex;
			buffer->data[index] = intensity;
			columnIndex++;
		}
		columnIndex = 0;
		rowIndex--;
	}
}


void CMemoryBuffer::Filter(BYTE* pBuf, int width, int height, int depth, int topFocus, int bottomFocus, int maskWidth, int maskHeight)
{
	int intensity = 0;
	double aroundIntensity[100000];
	int tempPosition = 0;

	if ((maskWidth % 2) != 0)
	{
		maskWidth += 1;
	}

	if ((maskHeight % 2) != 0)
	{
		maskHeight -= 1;
	}

	for (int j = height - bottomFocus; j < height - topFocus - maskHeight / 2; j++)
		//for (int j = topFocus; j < bottomFocus - maskHeight / 2; j++)
	{
		for (int i = 1; i < width - maskWidth / 2; i++)
		{
			for (int y = -maskHeight / 2; y <= maskHeight / 2; y++)
			{
				for (int x = -maskWidth / 2; x <= maskWidth / 2; x++)
				{
					//aroundIntensity[tempPosition] = pBuf[((height - (j + y)) * width) + (i + x)];
					aroundIntensity[tempPosition] = pBuf[((j + y) * width) + (i + x)];
					tempPosition++;
				}
			}

			for (int k = 0; k < tempPosition; k++)
			{
				intensity += aroundIntensity[k];
			}

			intensity /= (int)tempPosition;

			if (intensity > 255)
			{
				intensity = 255;
			}

			SetAverageIntensity(i, j, intensity);
			tempPosition = 0;
		}
	}

	/*else if (maskWidth == 5)
	{
	int aroundIntensity[15];

	for (int j = 1; j < height - 2; j++)
	{
	for (int i = 1; i < width - 2; i++)
	{
	aroundIntensity[0] = pBuf[((j - 1) * width) + (i - 2)];
	aroundIntensity[1] = pBuf[((j - 1) * width) + (i - 1)];
	aroundIntensity[2] = pBuf[((j - 1) * width) + (i)];
	aroundIntensity[3] = pBuf[((j - 1) * width) + (i + 1)];
	aroundIntensity[4] = pBuf[((j - 1) * width) + (i + 2)];

	aroundIntensity[5] = pBuf[((j) * width) + (i - 2)];
	aroundIntensity[6] = pBuf[((j) * width) + (i - 1)];
	aroundIntensity[7] = pBuf[((j) * width) + (i)] * 2;
	aroundIntensity[8] = pBuf[((j) * width) + (i + 1)];
	aroundIntensity[9] = pBuf[((j) * width) + (i + 2)];

	aroundIntensity[10] = pBuf[((j + 1) * width) + (i - 2)];
	aroundIntensity[11] = pBuf[((j + 1) * width) + (i - 1)];
	aroundIntensity[12] = pBuf[((j + 1) * width) + (i)];
	aroundIntensity[13] = pBuf[((j + 1) * width) + (i + 1)];
	aroundIntensity[14] = pBuf[((j + 1) * width) + (i + 2)];

	for (int k = 0; k < (sizeof(aroundIntensity) / sizeof(int)); k++)
	{
	intensity += aroundIntensity[k];
	}

	intensity /= (sizeof(aroundIntensity) / sizeof(int) + 1);

	if (intensity > 255)
	{
	intensity = 255;
	}

	position = (j)* width + (i);
	buffer[position] = intensity;
	}
	}
	}

	else if (maskWidth == 11)
	{
	int aroundIntensity[33];

	for (int j = 1; j < height - 5; j++)
	{
	for (int i = 1; i < width - 5; i++)
	{
	aroundIntensity[0] = pBuf[((j - 1) * width) + (i - 5)];
	aroundIntensity[1] = pBuf[((j - 1) * width) + (i - 4)];
	aroundIntensity[2] = pBuf[((j - 1) * width) + (i - 3)];
	aroundIntensity[3] = pBuf[((j - 1) * width) + (i - 2)];
	aroundIntensity[4] = pBuf[((j - 1) * width) + (i - 1)];
	aroundIntensity[5] = pBuf[((j - 1) * width) + (i)];
	aroundIntensity[6] = pBuf[((j - 1) * width) + (i + 1)];
	aroundIntensity[7] = pBuf[((j - 1) * width) + (i + 2)];
	aroundIntensity[8] = pBuf[((j - 1) * width) + (i + 3)];
	aroundIntensity[9] = pBuf[((j - 1) * width) + (i + 4)];
	aroundIntensity[10] = pBuf[((j - 1) * width) + (i + 5)];

	aroundIntensity[11] = pBuf[((j) * width) + (i - 5)];
	aroundIntensity[12] = pBuf[((j) * width) + (i - 4)];
	aroundIntensity[13] = pBuf[((j) * width) + (i - 3)];
	aroundIntensity[14] = pBuf[((j) * width) + (i - 2)];
	aroundIntensity[15] = pBuf[((j) * width) + (i - 1)];
	aroundIntensity[16] = pBuf[((j) * width) + (i)];
	aroundIntensity[17] = pBuf[((j) * width) + (i + 1)];
	aroundIntensity[18] = pBuf[((j) * width) + (i + 2)];
	aroundIntensity[19] = pBuf[((j) * width) + (i + 3)];
	aroundIntensity[20] = pBuf[((j) * width) + (i + 4)];
	aroundIntensity[21] = pBuf[((j) * width) + (i + 5)];

	aroundIntensity[22] = pBuf[((j + 1) * width) + (i - 5)];
	aroundIntensity[23] = pBuf[((j + 1) * width) + (i - 4)];
	aroundIntensity[24] = pBuf[((j + 1) * width) + (i - 3)];
	aroundIntensity[25] = pBuf[((j + 1) * width) + (i - 2)];
	aroundIntensity[26] = pBuf[((j + 1) * width) + (i - 1)];
	aroundIntensity[27] = pBuf[((j + 1) * width) + (i)];
	aroundIntensity[28] = pBuf[((j + 1) * width) + (i + 1)];
	aroundIntensity[29] = pBuf[((j + 1) * width) + (i + 2)];
	aroundIntensity[30] = pBuf[((j + 1) * width) + (i + 3)];
	aroundIntensity[31] = pBuf[((j + 1) * width) + (i + 4)];
	aroundIntensity[32] = pBuf[((j + 1) * width) + (i + 5)];

	for (int k = 0; k < (sizeof(aroundIntensity) / sizeof(int)); k++)
	{
	intensity += aroundIntensity[k];
	}

	intensity /= (sizeof(aroundIntensity) / sizeof(int) + 1);

	if (intensity > 255)
	{
	intensity = 255;
	}

	position = (j)* width + (i);
	buffer[position] = intensity;
	}
	}
	}*/
}

//kvadratna razlika
int CMemoryBuffer::SetDifference(QRect areaRef, QRect area, CMemoryBuffer &bufferRef, CMemoryBuffer &buffer)
{
	int i, j, difference, differenceSum, intensity1, intensity2, position = 0, position2 = 0;

	differenceSum = 0;

	if ((area.left() >= 0) && (area.right() >= 0) && (area.top() >= 0) && (area.bottom() >= 0) && (area.left() < width) && (area.right() < width) && (area.top() < height) && (area.bottom() < height))
	{

		for (i = 0; i < areaRef.width(); i++)
		{
			for (j = 0; j < areaRef.height(); j++)
			{
				position = buffer.GetPosition(i + area.left(), j + area.top());
				position2 = bufferRef.GetPosition(i + areaRef.left(), j + areaRef.top());

				intensity1 = bufferRef.GetAverageIntensity(i + areaRef.left(), j + areaRef.top());//bufferRef.buffer[position2];
				intensity2 = buffer.GetAverageIntensity(i + area.left(), j + area.top()); //buffer.buffer[position];

				difference = (intensity1 - intensity2) * (intensity1 - intensity2);
				differenceSum += difference;

				if (difference < 0)
				{
					difference = 0;
				}
				else if (difference > 255)
				{
					difference = 255;
				}
				SetAverageIntensity(position, difference);
			}
		}
	}
	else
		differenceSum = HUGE_VAL;

	return differenceSum;

}

/*int CMemoryBuffer::SetDifference(QRect positionRect1, QRect positionRect2, CMemoryBuffer &buffer1, CMemoryBuffer &buffer2)
{
	CTimer time;
	CString outputbuff;
	int  difference, differenceSum, intensity1, intensity2, rectWidth, rectHeight, position = 0;

	differenceSum = 0;
	rectWidth = positionRect1.Width();
	rectHeight = positionRect1.Height();
	time.SetStart();

	if ((positionRect1.left >= 0) && (positionRect1.right >= 0) && (positionRect1.top >= 0) && (positionRect1.bottom >= 0) && (positionRect1.left < width) && (positionRect1.right < width) && (positionRect1.top < height) && (positionRect1.bottom < height))
	{
		//for (int i = positionRect1.left; i < positionRect1.right; i++)

		parallel_transform(int(positionRect1.left), int(positionRect1.right), 1, [&](int i)
		{
			//for (int j = positionRect1.top; j < positionRect1.bottom; j++)
			parallel_transform(int(positionRect1.top), int(positionRect1.bottom), 1, [&](int j)
			{
				position = GetPosition(i, j);
				intensity1 = buffer1.buffer[position];
				intensity2 = buffer2.buffer[position];
				//difference += abs(intenziteta1 - intenziteta2);
				difference = (intensity1 - intensity2) * (intensity1 - intensity2);
				differenceSum += difference;

				if (difference < 0)
				{
					difference = 0;
				}
				else if (difference > 255)
				{
					difference = 255;
				}
				buffer[position] = difference;
				//SetAverageIntensity(i, j, difference);
			});
		});
	}
	else
		differenceSum = HUGE_VAL;

	time.SetStop();
	time.ElapsedTime();

	outputbuff.Format(_T("Paralell for: %f"), time.elapsed / 1000);
	g_pMainFrame->OnWriteLogMessage(outputbuff);

	return differenceSum;

}*/
int CMemoryBuffer::SetDifference(QRect areaRef, QRect area, CMemoryBuffer &bufferRef, CMemoryBuffer &buffer, float gain)
{
	Timer time;
	QString outputbuff;
	double temp;
	int i, j, difference, differenceSum, intensity1, intensity2, position = 0, position2 = 0;

	differenceSum = 0;

	//time.SetStart();
	if ((area.left() >= 0) && (area.right() >= 0) && (area.top() >= 0) && (area.bottom() >= 0) && (area.left() < width) && (area.right() < width) && (area.top() < height) && (area.bottom() < height))
	{
		for (i = 0; i < areaRef.width(); i++)
		{
			for (j = 0; j < areaRef.height(); j++)
			{
				//	position = buffer.GetPosition(i + area.left, j + area.top);
					//position2 = bufferRef.GetPosition(i + areaRef.left, j + areaRef.top);

				intensity1 = bufferRef.GetAverageIntensity(i + areaRef.left(), j + areaRef.top());//bufferRef.buffer[position2];
				intensity2 = buffer.GetAverageIntensity(i + area.left(), j + area.top()); //buffer.buffer[position];

				temp = (intensity1 - intensity2) *  gain;
				difference = temp + 128;
				differenceSum += (temp * temp);

				if (difference < 0)
				{
					difference = 0;
				}
				else if (difference > 255)
				{
					difference = 255;
				}
				SetAverageIntensity(position, difference);
			}
		}
	}
	else
		differenceSum = HUGE_VAL;

	//time.SetStop();
	//time.ElapsedTime();

	//outputbuff.Format(_T("Normal: %f"), time.elapsed / 1000);
	//g_pMainFrame->OnWriteLogMessage(outputbuff);
	return differenceSum;
}

int CMemoryBuffer::SetDifference(QRect areaRef, QRect area, CMemoryBuffer &bufferRef, CMemoryBuffer &buffer, float gain, int middleInt)
{
	Timer time;
	QString outputbuff;
	double temp, differenceSum;
	int i, j, difference, intensity1, intensity2, position = 0, position2 = 0;

	differenceSum = 0;

	if ((area.left() >= 0) && (area.right() >= 0) && (area.top() >= 0) && (area.bottom() >= 0) && (area.left() < width) && (area.right() < width) && (area.top() < height) && (area.bottom() < height))
	{
		for (i = 0; i < areaRef.width(); i++)
		{
			for (j = 0; j < areaRef.height(); j++)
			{

				//position = buffer.GetPosition(i + area.left, j + area.top);
				//position2 = bufferRef.GetPosition(i + areaRef.left, j + areaRef.top);

				intensity1 = bufferRef.GetAverageIntensity(i + areaRef.left(), j + areaRef.top());//bufferRef.buffer[position2];
				intensity2 = buffer.GetAverageIntensity(i + area.left(), j + area.top()); //buffer.buffer[position];

				temp = (intensity1 - intensity2) * gain;
				difference = temp + middleInt;

				differenceSum += (temp * temp);

				if (difference < 0)
				{
					difference = 0;
				}
				else if (difference > 255)
				{
					difference = 255;
				}
				SetAverageIntensity(position, difference);
			}
		}
	}
	else
		differenceSum = HUGE_VAL;

	return (int)differenceSum;
}
int CMemoryBuffer::GetDifference(QRect area, QRect areaRef, CMemoryBuffer &comparingBuffer, float gain)
{
	int i, j, intensity1, intensity2;
	double temp, difference;
	difference = 0;

	if ((comparingBuffer.width < width) && (comparingBuffer.height < height))
	{
		if ((areaRef.left() >= 0) && (areaRef.top() >= 0) && (areaRef.right() < width) && (areaRef.bottom() < height))
		{

			for (i = 0; i < areaRef.width(); i++)
			{
				for (j = 0; j < areaRef.height(); j++)
				{
					intensity1 = GetAverageIntensity(i + area.left(), j + area.top());
					intensity2 = comparingBuffer.GetAverageIntensity(i + areaRef.left(), j + areaRef.top());

					temp = (intensity2 - intensity1) * gain;
					difference += (temp * temp);

				}
			}
		}
	}
	else
		difference = HUGE_VAL;

	return (int)difference;
}

int CMemoryBuffer::GetDifference(QRect area, QRect areaRef, CMemoryBuffer &comparingBuffer)
{
	//kvadratna razlika med slikama, zgronji levi rob comparingBuffer-ja postavimo na x, y  
	int difference, i, j, intensity1, intensity2;
	double temp;

	difference = 0;

	if ((comparingBuffer.width < width) && (comparingBuffer.height < height))
	{
		if ((areaRef.left() >= 0) && (areaRef.top() >= 0) && (areaRef.right() < width) && (areaRef.bottom() < height))
		{

			for (i = 0; i < areaRef.width(); i++)
			{
				for (j = 0; j < areaRef.height(); j++)
				{
					intensity1 = GetAverageIntensity(i + area.left(), j + area.top());
					intensity2 = comparingBuffer.GetAverageIntensity(i + areaRef.left(), j + areaRef.top());

					temp = (intensity2 - intensity1);
					difference += (temp*temp);

				}
			}
		}
	}
	else
		difference = HUGE_VAL;

	return difference;
}

int CMemoryBuffer::GetDifferenceInterpolate(QRect area, QRect areaRef, CMemoryBuffer &comparingBuffer, int step)
{
	//kvadratna razlika med slikama, zgronji levi rob comparingBuffer-ja postavimo na x, y  
	int difference, i, j, intensity1[4], intensity2;

	difference = 0;
	/*
	//okrog roba pustimo 1 piksel, da ne prekoracimo slike
	if (x == 0)
		x = 1;

	if (y == 0)
		y = 1;


	if ((comparingBuffer.width < width) && (comparingBuffer.height < height))
	{
		if ((x >= 0) && (y >= 0) && (x < width) && (y < height))
		{

			for (i = 0; i < comparingBuffer.width; i++)
			{
				for (j = 0; j < comparingBuffer.height; j++)
				{
					intensity1[0] = GetAverageIntensity(i + x - 1, j + y - 1);
					intensity1[1] = GetAverageIntensity(i + x - 1, j + y);
					intensity1[2] = GetAverageIntensity(i + x, j + y - 1);
					intensity1[3] = GetAverageIntensity(i + x, j + y);

					delx = -ix;
					delx = delx / korak;
					dely = -iy;
					dely = dely / korak;
					intenziteta1 = (inten1*delx + inten2*(1 - delx))*dely + (inten3*delx + inten4*(1 - delx))*(1 - dely);

				}
			}
		}
	}
	else
		difference = HUGE_VAL;
	*/
	return difference;
}

int CMemoryBuffer::GetDifferenceAbsolute(QRect area, QRect areaRef, CMemoryBuffer &comparingBuffer)
{
	int difference, i, j, intensity1, intensity2;
	//z bufferjem primerjamo comparingBuffer
	//comparingBuffer mora biti manjsi od original bufferja
	//x = odmik v original bufferju
	//y = odmik v original bufferju
	//primerja velikost comparingBufferja
	difference = 0;

	if ((comparingBuffer.width <= width) && (comparingBuffer.height <= height))
	{
		if ((areaRef.left() >= 0) && (areaRef.top() >= 0) && (areaRef.right() < width) && (areaRef.bottom() < height))
		{

			for (i = 0; i < areaRef.width(); i++)
			{
				for (j = 0; j < areaRef.height(); j++)
				{
					intensity1 = GetAverageIntensity(i + area.left(), j + area.top());
					intensity2 = comparingBuffer.GetAverageIntensity(i + areaRef.left(), j + areaRef.top());

					difference += abs(intensity2 - intensity1);

				}
			}
		}
	}
	else
		difference = HUGE_VAL;

	return difference;
}

int CMemoryBuffer::GetMinDifferencePosition(CRectRotated area, CRectRotated areaRef, CMemoryBuffer &bufferRef, int whiteThreshold, int blackThreshold)
{
	//vrne pozicijo, kjer je najmanjsa razlika intenzitet med slikama

	int i, j, k, difference, minDifference, intensity1[9], intensity2[9], averInt[2], minDiffPosition = -1, temp;
	int minIntensity = 255;
	int diffX, diffY;
	int position = 0, position2 = 0;

	minDifference = 255;

	diffX = areaRef.left() - area.left();
	temp = areaRef.right() - area.right();
	if (diffX > temp)
		diffX = temp;

	diffY = areaRef.top() - area.top();
	temp = areaRef.bottom() - area.bottom();
	if (diffY > temp)
		diffY = temp;

	if ((area.left() >= 0) && (area.right() >= 0) && (area.top() >= 0) && (area.bottom() >= 0) && (area.left() < width) && (area.right() < width) && (area.top() < height) && (area.bottom() < height))
	{
		for (i = area.left(); i < area.right(); i++)
		{
			for (j = area.top(); j < area.bottom(); j++)
			{
				position = GetPosition(i, j);
				position2 = bufferRef.GetPosition(i + diffX, j + diffY);

				//filtriramo z 9imi piksli
				intensity1[0] = this->buffer->data[position];
				intensity1[1] = this->buffer->data[position - 1];
				intensity1[2] = this->buffer->data[position + 1];
				intensity1[3] = this->buffer->data[position - width];
				intensity1[4] = this->buffer->data[position + width];
				intensity1[5] = this->buffer->data[position + width - 1];
				intensity1[6] = this->buffer->data[position + width + 1];
				intensity1[7] = this->buffer->data[position - width + 1];
				intensity1[8] = this->buffer->data[position - width - 1];
				intensity2[0] = bufferRef.buffer->data[position2];
				intensity2[1] = bufferRef.buffer->data[position2 - 1];
				intensity2[2] = bufferRef.buffer->data[position2 + 1];
				intensity2[3] = bufferRef.buffer->data[position2 - bufferRef.width];
				intensity2[4] = bufferRef.buffer->data[position2 + bufferRef.width];
				intensity2[5] = bufferRef.buffer->data[position2 - bufferRef.width - 1];
				intensity2[6] = bufferRef.buffer->data[position2 - bufferRef.width + 1];
				intensity2[7] = bufferRef.buffer->data[position2 + bufferRef.width + 1];
				intensity2[8] = bufferRef.buffer->data[position2 + bufferRef.width - 1];
				averInt[0] = 0;
				averInt[1] = 0;

				for (k = 0; k < 9; k++)
				{
					averInt[0] += intensity1[k];
					averInt[1] += intensity2[k];
				}
				averInt[0] /= 9;
				averInt[1] /= 9;
				if ((intensity1[0] < whiteThreshold) && (intensity1[0] > blackThreshold))
				{
					difference = abs(averInt[0] - averInt[1]);

					if (difference < minDifference)
					{
						minDifference = difference;
						minDiffPosition = position;
						minIntensity = intensity1[0];
					}

					if (difference == minDifference)
					{
						if (minIntensity > intensity1[0])
						{
							minDiffPosition = position;
							minIntensity = intensity1[0];
						}
					}
				}
			}
		}
	}

	return minDiffPosition;
}


//Opomba Martin: mislim, da ta funkcija izraèuna srednji indeks belega (vseh toèk z intenziteto nad treshold) v vektorju
int CMemoryBuffer::GetGravityOfMaximum(std::vector< float> vec, int start, int stop, float threshold)
{
	int index = 0;
	int index2 = 0;
	float sum[2] = { 0 }; //ce je razsekan na 2 dela na zacetku in na koncu
	float sumHory[2] = { 0 };
	int sumIndex = 0;
	int middle = 0;
	int arrayIndex = 0;
	int numHory[2] = { 0 };
	float min, max;
	int size = vec.size();

	if ((size > 0) && (start >= 0) && (stop <= size))
	{
		if ((vec[start] > threshold)) //maksimum je že na zacetku,  potrebno je pogledati nazaj po vektorju
		{
			//zamaknemmo strat in stop
			middle = (stop - start) / 3;
			start -= middle;
			stop -= middle;
		}

		for (int i = start; i < stop; i++)
		{
			if (i >= 0)
			{
				if (start < 0)
					sumIndex = 1;

				arrayIndex = i;
			}
			else
			{
				arrayIndex = size + i;
			}

			if (vec[arrayIndex] > threshold)
			{
				sumHory[sumIndex] += (vec[arrayIndex] - threshold) * (arrayIndex);
				sum[sumIndex] += (vec[arrayIndex] - threshold);
				numHory[sumIndex]++;
			}
		}

		if (sum[0] > 0)
		{
			index = (int)(sumHory[0] / sum[0]);
			if (sum[1] > 0)
			{
				index2 = (int)(sumHory[1] / sum[1]);

				if (numHory[0] > numHory[1])
				{
					index += index2;
					if (index > size)
						index = size - 1;
				}
				else
				{
					index = abs(index2 - (size - index));
				}
			}
		}

	}

	return index;
}

//izracuna threshold iz minimuma in maksimuma v vektorju
int CMemoryBuffer::GetGravityOfMaximum(std::vector< float> vec, int start, int stop)
{
	int index = 0;
	int index2 = 0;
	float sum[2] = { 0 }; //ce je razsekan na 2 dela na zacetku in na koncu
	float sumHory[2] = { 0 };
	int sumIndex = 0;
	int middle = 0;
	int arrayIndex = 0;
	int numHory[2] = { 0 };
	float min, max, threshold;
	int size = vec.size();

	if ((size > 0) && (start >= 0) && (stop <= size))
	{
		auto result = std::minmax_element(vec.begin() + start, vec.begin() + stop);
		index = result.first - vec.begin(); //index minimuma
		min = vec[index];
		index = result.second - vec.begin();
		max = vec[index];

		threshold = max - (max - min) / 2;
		if ((vec[start] > threshold)) //maksimum je že na zacetku,  potrebno je pogledati nazaj po vektorju
		{
			//zamaknemmo strat in stop
			middle = (stop - start) / 3;
			start -= middle;
			stop -= middle;
		}

		for (int i = start; i < stop; i++)
		{
			if (i >= 0)
			{
				if (start < 0)
					sumIndex = 1;

				arrayIndex = i;
			}
			else
			{
				arrayIndex = size + i;
			}

			if (vec[arrayIndex] > threshold)
			{
				sumHory[sumIndex] += (vec[arrayIndex] - threshold) * (arrayIndex);
				sum[sumIndex] += (vec[arrayIndex] - threshold);
				numHory[sumIndex]++;
			}
		}

		if (sum[0] > 0)
		{
			index = (int)(sumHory[0] / sum[0]);
			if (sum[1] > 0)
			{
				index2 = (int)(sumHory[1] / sum[1]);

				if (numHory[0] > numHory[1])
				{
					index += index2;
					if (index > size)
						index = size - 1;
				}
				else
				{
					index = abs(index2 - (size - index));
				}
			}
		}

	}

	return index;
}

int CMemoryBuffer::GetGravityOfMinimum(std::vector< float> vec, int start, int stop)
{
	int index = 0;
	int index2 = 0;
	float sum[2] = { 0 }; //ce je razsekan na 2 dela na zacetku in na koncu
	float sumHory[2] = { 0 };
	int sumIndex = 0;
	int middle = 0;
	int arrayIndex = 0;
	int numHory[2] = { 0 };
	float min, max, threshold;
	int size = vec.size();

	if ((size > 0) && (start >= 0) && (stop <= size))
	{
		auto result = std::minmax_element(vec.begin() + start, vec.begin() + stop);
		index = result.first - vec.begin(); //index minimuma
		min = vec[index];
		index = result.second - vec.begin();
		max = vec[index];

		threshold = max - (max - min) / 2;
		if ((vec[start] < threshold)) //minimum je že na zacetku,  potrebno je pogledati nazaj po vektorju
		{
			//zamaknemmo strat in stop
			middle = (stop - start) / 3;
			start -= middle;
			stop -= middle;
		}

		for (int i = start; i < stop; i++)
		{
			if (i >= 0)
			{
				if (start < 0)
					sumIndex = 1;

				arrayIndex = i;
			}
			else
			{
				arrayIndex = size + i;
			}

			if (vec[arrayIndex] < threshold)
			{
				sumHory[sumIndex] += (threshold - vec[arrayIndex]) * (arrayIndex);
				sum[sumIndex] += (threshold - vec[arrayIndex]);
				numHory[sumIndex]++;
			}
		}

		if (sum[0] > 0)
		{
			index = (int)(sumHory[0] / sum[0]);
			if (sum[1] > 0)
			{
				index2 = (int)(sumHory[1] / sum[1]);

				if (numHory[0] > numHory[1])
				{
					index += index2;
					if (index > vec.size())
						index = vec.size() - 1;
				}
				else
				{
					index = abs(index2 - (size - index));
				}
			}
		}
	}

	return index;
}

void CMemoryBuffer::ImageBinaryDilationBlack(CMemoryBuffer &source, QRect rect, int threshold, int kernelSize)
{
	int x, y, i, j, d;
	int intensity = 0;
	bool nextPix = false;

	for (y = rect.top() + kernelSize / 2; y < rect.bottom() - kernelSize / 2; y++)
	{
		for (x = rect.left() + kernelSize / 2; x < rect.right() + kernelSize / 2; x++)
		{
			intensity = source.GetAverageIntensity(x, y);
			if (intensity < threshold)
				SetAverageIntensity(x, y, 0);
			else
			{
				for (j = y - kernelSize / 2; j <= y + kernelSize / 2; j++)
				{
					for (i = x - kernelSize / 2; i <= x + kernelSize / 2; i++)
					{
						intensity = source.GetAverageIntensity(i, j);
						if (intensity < threshold)
						{
							SetAverageIntensity(x, y, 255);
							nextPix = true;
							break;
						}
					}
					if (nextPix)
						break;
				}
			}

			nextPix = false;
			d = 0;
		}
	}
}


void CMemoryBuffer::ResetBufferOnValue(int value)
{
	for (int i = 0; i < bufferSize; i++)
		SetAverageIntensity(i, value);
}

void CMemoryBuffer::BinarizeImage(CMemoryBuffer &source, QRect rect, int threshold)
{
	int x, y;
	int intensity = 0;

	for (y = rect.top(); y < rect.bottom(); y++)
	{
		for (x = rect.left(); x < rect.right(); x++)
		{
			intensity = source.GetAverageIntensity(x, y);
			if (intensity > threshold)
				SetAverageIntensity(x, y, 255);
			else
				SetAverageIntensity(x, y, 0);
		}
	}
}
//binarizira sliko razen pri exeptionThreshold intenziteti nastavi  intensityValue (robni)
void CMemoryBuffer::BinarizeImage(CMemoryBuffer &source, QRect rect, int threshold, int exeptionThreshold, int intensityValue)
{
	int x, y;
	int intensity = 0;

	for (y = rect.top(); y < rect.bottom(); y++)
	{
		for (x = rect.left(); x < rect.right(); x++)
		{
			intensity = source.GetAverageIntensity(x, y);

			if (intensityValue == 0)
			{
				if (intensity > exeptionThreshold)
					SetAverageIntensity(x, y, intensityValue);
				else if (intensity > threshold)
					SetAverageIntensity(x, y, 255);
				else
					SetAverageIntensity(x, y, 0);
			}
			else
			{
				if (intensity > threshold)
					SetAverageIntensity(x, y, 255);
				if (intensity < exeptionThreshold)
					SetAverageIntensity(x, y, intensityValue);
				else
					SetAverageIntensity(x, y, 0);
			}



		}
	}
}



void CMemoryBuffer::MedianFilter(CMemoryBuffer &source, QRect rect, int kernelSize)
{
	int x, y, i, j, index;
	int intensity = 0;
	int size = kernelSize * kernelSize;
	int *intArray;
	//int intArray[size];
	intArray = new int[size];
	if (kernelSize >= 20)
		kernelSize = 19;

	for (y = rect.top() + kernelSize / 2; y < rect.bottom() - kernelSize / 2; y++)
	{
		for (x = rect.left() + kernelSize / 2; x < rect.right() + kernelSize / 2; x++)
		{
			index = 0;
			for (j = y - kernelSize / 2; j <= y + kernelSize / 2; j++)
			{
				for (i = x - kernelSize / 2; i <= x + kernelSize / 2; i++)
				{
					intArray[index] = source.GetAverageIntensity(i, j);
					index++;
				}
			}

			std::sort(intArray, intArray + index);

			if (kernelSize % 2 == 1)	// liha matrika
				SetAverageIntensity(x, y, intArray[(int)(index / 2)]);
			else						// soda matrika
				SetAverageIntensity(x, y, (intArray[(int)(index / 2)] + intArray[(int)(index / 2) - 1]) / 2);

		}
	}

	delete intArray;
}



void CMemoryBuffer::MedianBinaryFilter(CMemoryBuffer &source, QRect rect, int kernelSize)
{
	int x, y, i, j, index;
	int intensity = 0;
	int size = kernelSize * kernelSize / 2;
	int kernelHalf = kernelSize / 2;
	int counter = 0;

	if (rect.width() > source.width)
	{
		rect.setRight(source.width - 1);
	}

	if (rect.height() == source.height)
	{
		rect.setBottom(source.height - 1);
	}

	for (y = rect.top() + kernelHalf; y < rect.bottom() - kernelHalf; y++)
	{
		for (x = rect.left() + kernelHalf; x < rect.right() - kernelHalf; x++)
		{
			for (j = y - kernelHalf; j <= y + kernelHalf; j++)
			{
				for (i = x - kernelHalf; i <= x + kernelHalf; i++)
				{
					intensity = source.GetAverageIntensity(i, j);

					if ((intensity < 128) && (intensity > -1))
						counter++;

					if (counter >= size)
					{
						SetAverageIntensity(x, y, 0);
						goto nextPix;

					}
				}
			}

			SetAverageIntensity(x, y, 255);

		nextPix:
			counter = 0;

		}
	}
}


void CMemoryBuffer::GetHistogram(CRectRotated rect)
{
	int i, j, inten;
	int maxValue = 0;

	for (i = 0; i < 256; i++)
		histogram[i] = 0;


	for (i = rect.top(); i < rect.bottom(); i++)
	{
		for (j = rect.left(); j < rect.right(); j++)
		{
			inten = GetAverageIntensity(j, i);
			histogram[inten]++;
			if (histogram[inten] > maxValue)
				maxValue = histogram[inten];
		}
	}

	for (i = 0; i < 256; i++)
		histogram[i] = (int)(histogram[i] * (255.0 / maxValue));
}

void CMemoryBuffer::GetRelativeFrequencies(Rect region)
{
	int pixVal;
	int startX = region.x;
	int startY = region.y;
	int stopX = region.x + region.width;
	int stopY = region.y + region.height;
	int area = region.width* region.height;

	for (int k = 0; k < 256; k++)
		relativeFrequencies[k] = 0;

	for (int i = startX; i < stopX; i++)
	{
		for (int j = startY; j < stopY; j++)
		{
			pixVal = this->GetAverageIntensity(i, j);
			relativeFrequencies[pixVal]++;
		}
	}

	for (int j = 0; j < 256; j++)
	{
		relativeFrequencies[j] = relativeFrequencies[j] / area;
	}
}



int CMemoryBuffer::GetBestMatchValue(CMemoryBuffer &comparingBuffer, CMemoryBuffer &drawBuffer, QRect comparingArea)
{

	//step - korak za koliko se premikamo po kvadratu
	//comparingArea - kvadrat na this sliki, po katerem se premikamo in primerjamo sliko
	float minDifference, maxDifference;
	float difference, difference2;
	int i, j, i1, j1, intensity1, intensity2;

	minDifference = HUGE_VAL;
	maxDifference = 0.f;

	difference = 0;

	for (i = comparingArea.left(); i < comparingArea.right(); i++)
	{
		for (j = comparingArea.top(); j < comparingArea.bottom(); j++)
		{
			for (i1 = 0; i1 < comparingBuffer.width; i1++)
			{
				for (j1 = 0; j1 < comparingBuffer.height; j1++)
				{
					intensity1 = this->GetAverageIntensity(i1 + i, j1 + j);
					intensity2 = comparingBuffer.GetAverageIntensity(i1, j1);

					difference += abs(intensity1 - intensity2);
				}
			}
			//difference = GetDifferenceAbsolute(i, j, comparingBuffer);
			//difference = drawBuffer.SetDifference(comparingArea, comparingArea, *this, comparingBuffer);

			if (difference < minDifference)				//zapomni si kje je razlika najmanjsa			
				minDifference = difference;
		}
	}

	return minDifference;
}





