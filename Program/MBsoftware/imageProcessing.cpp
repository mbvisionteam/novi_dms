﻿#include "stdafx.h"
#include "imageProcessing.h"
#include "math.h"
#include "CLine.h"
#include "CPointFloat.h"
#include "CRectRotated.h"
#include "CCircle.h"
#include <conio.h>
#include "Classifier.h"




std::vector<CCamera*>										imageProcessing::cam;
std::vector<CMemoryBuffer*>									imageProcessing::images;
CMemoryBuffer*												imageProcessing::currImage;
std::vector<Types*>											imageProcessing::types[NR_STATIONS];
Measurand*													imageProcessing::measureObject;
std::vector<Timer*>											imageProcessing::processingTimer;

Mat															imageProcessing::DrawImage;
std::vector<AdvancedSettings>								imageProcessing::advSettingsPresort;

QRect testRect;

int nrSavedImages[4];

int rubberResizeMode = 0;  
int imageCounterPresort = 0;

imageProcessing::imageProcessing(QString referencePath)
{
	presortDialog = new(QWidget);
	ui2.setupUi(presortDialog);
	this->referencePath = referencePath;
	ui.setupUi(this);
	zoomFactor = 1;
	displayImage = 0;
	displayWindow = 0;
	prevDisplayImage = 0;
	prevDisplayWindow = 0;
	rowNum = 10;
	colNum = 4;
	colNum = 4;
	currentStation = 0;
	isSceneAdded = false;
	selectedFunction = 0;
	drawRubberFunction = 0;
	presortingCameraCounter = 0;
	presortingCameraCounterBad = 0;
	presortingIndex = 30;
	positionCounter = 0;
	ui.lineInsertImageIndex->setText("0");
	
	connect(ui.buttonImageUp, SIGNAL(pressed()), this, SLOT(OnPressedImageUp()));
	connect(ui.buttonImageDown, SIGNAL(pressed()), this, SLOT(OnPressedImageDown()));
	connect(ui.buttonCamDown, SIGNAL(pressed()), this, SLOT(OnPressedCamDown()));
	connect(ui.buttonCamUp, SIGNAL(pressed()), this, SLOT(OnPressedCamUp()));
	connect(ui.buttonZoomIn, SIGNAL(pressed()), this, SLOT(ZoomIn()));
	connect(ui.buttonReset, SIGNAL(pressed()), this, SLOT(ZoomReset()));
	connect(ui.buttonZoomOut, SIGNAL(pressed()), this, SLOT(ZoomOut()));
	connect(ui.buttonLoadImage, SIGNAL(pressed()), this, SLOT(OnLoadImage()));
	connect(ui.buttonLoadMultipleImages, SIGNAL(pressed()), this, SLOT(OnLoadMultipleImage()));
	connect(ui.buttonSaveImage, SIGNAL(pressed()), this, SLOT(OnSaveImage()));
	connect(ui.buttonProcessCam0, SIGNAL(pressed()), this, SLOT(OnProcessCam0()));
	connect(ui.buttonProcessCam1, SIGNAL(pressed()), this, SLOT(OnProcessCam1()));
	connect(ui.buttonProcessCam2, SIGNAL(pressed()), this, SLOT(OnProcessCam2()));
	connect(ui.buttonProcessCam3, SIGNAL(pressed()), this, SLOT(OnProcessCam3()));
	connect(ui.buttonProcessCam4, SIGNAL(pressed()), this, SLOT(OnProcessCam4()));
	connect(ui.buttonCreateRubber, SIGNAL(pressed()), this, SLOT(OnPressedCreateRubber()));
	connect(ui.buttonRubberResize, SIGNAL(pressed()), this, SLOT(OnPressedSelectRubber()));


	connect(ui.addParameter, SIGNAL(pressed()), this, SLOT(onPressedAddParameter()));
	connect(ui.removeParameter, SIGNAL(pressed()), this, SLOT(onPressedRemoveParameters()));
	connect(ui.update, SIGNAL(pressed()), this, SLOT(onPressedUpdateParameters()));
	connect(ui.buttonSurfaceInspection, SIGNAL(pressed()), this, SLOT(onPressedSurfaceInspection()));
	//connect(ui.imageView, SIGNAL(pressed()), this, SLOT(mousePressEvent()));
	//connect(ui.imageView, SIGNAL(released()), this, SLOT(mouseReleaseEvent()));
	connect(ui.lineInsertImageIndex, SIGNAL(editingFinished()), this, SLOT(OnDoneEditingLineInsertImageIndex()));

	//testFunctions
	connect(ui.buttonFunctionHeight, SIGNAL(pressed()), this, SLOT(AddnewHeightMeasurement()));
	connect(ui.buttonFunctionLineDeviation, SIGNAL(pressed()), this, SLOT(LineDeviationMeasurement()));
	connect(ui.buttonFunctionFunctionAddAsMeasuredParameter, SIGNAL(pressed()), this, SLOT(SaveSelectedDynamicFunctionAsMeasurment()));
	connect(ui.buttonParameterEdit, SIGNAL(pressed()), this, SLOT(EditSelectedDynamicFunctionParameter()));

	//Razvrščevalnik
	connect(ui.setClassifierImagesPathButton, SIGNAL(pressed()), this, SLOT(OnSetClassifierTrainImagesPath()));
	connect(ui.setClassifierTestImages, SIGNAL(pressed()), this, SLOT(OnSetClassifierTestImagesPath()));

	for (int i = 0; i < 7; i++)
	{
		processingTimer.push_back(new Timer());
	}
	rubberBand = new QRubberBand(QRubberBand::Rectangle, ui.imageView);//new rectangle band
	//gripTopLeft = new QSizeGrip(this);
	//gripBottomRight = new QSizeGrip(this);

	ui.imageView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	start = 0;

	variantManager = new QtVariantPropertyManager();
	
	slovensko = SLOVENSKO;

	QString niz;
	if (slovensko) niz = QString("Konstanta funkcije");
	else niz = QString("Function property");
	topItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
		niz);

	variantFactory = new QtVariantEditorFactory();

	variantEditor = new QtTreePropertyBrowser();
	variantEditor->setFactoryForManager(variantManager, variantFactory);
	variantEditor->addProperty(topItem);
	variantEditor->setPropertiesWithoutValueMarked(true);
	variantEditor->setResizeMode(QtTreePropertyBrowser::ResizeMode::ResizeToContents);
	variantEditor->setRootIsDecorated(true);
	ui.ParameterLayout->addWidget(variantEditor);


	for (int i = 0; i < 50; i++) //za izpise kaj vracajo funkcije
	{
		ui.listFunctionsReturn->addItem("");
	}


	NastaviJezikImgProc();




	QHBoxLayout* layout = new QHBoxLayout(rubberBand);
	//layout->setContentsMargins(0, 0, 0, 0);
	//setWindowFlags(Qt::SubWindow);

	gripTopLeft = new QSizeGrip(rubberBand);
	gripBottomRight = new QSizeGrip(rubberBand);

	layout->addWidget(gripTopLeft, 0, Qt::AlignLeft | Qt::AlignTop);
	layout->addWidget(gripBottomRight, 0, Qt::AlignRight | Qt::AlignBottom);

	//rubberBand->resize(50, 50);
	gripTopLeft->setVisible(false);
	gripBottomRight->setVisible(false);



	for (int i = 0; i < 4; i++)
	{
		nrSavedImages[i] = 0;
	}

	//create presort dialog


}

void imageProcessing::closeEvent(QCloseEvent * event)
{
	drawRubberFunction = 0;
	selectedFunction = 0;
	timer->stop();
	ClearDraw();
}

imageProcessing::~imageProcessing()
{
	delete variantManager;
	delete variantFactory;
	delete variantEditor;
	qDeleteAll(scene->items());
	item.erase(item.begin(), item.end());
	cam.clear();
	delete scene;

	//delete item;
	//delete topItem;

}

void imageProcessing::ConnectImages(std::vector<CCamera*> inputCam, std::vector<CMemoryBuffer*> inputImages)
{

	cam = inputCam;
	images = inputImages;
	currImage = &cam[0]->image[0];
	displayWindow =0;
	displayImage = 0;

	ResizeDisplayRect();
	ShowImages();

}

void imageProcessing::ConnectMeasurand(Measurand * objects)
{

	measureObject = objects;

	CreateScenePresortDialog();//pri dms
	
}

void imageProcessing::ConnectTypes(int station, std::vector<Types*> type)
{

	types[station] = type;
}
		


void imageProcessing::ConnectImages(std::vector<CCamera*> inputCam)
{
	cam = inputCam;

}


void imageProcessing::ShowDialog(int rights)
{	
	
		
	loginRights = rights;
	ReplaceCurrentBuffer();
	activateWindow();
	currentFunction = -1;
	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}
	item.clear();
	//Če uporabnik nima administratorskih pravic se onemogoči nastavljanje parametrov slik
	ui.buttonProcessCam4->setEnabled(false);
	if ((rights == 1)|| (rights == 2))
	{
		if (rights == 1)
		{
			ui.addParameter->setEnabled(true);
			ui.removeParameter->setEnabled(true);
			ui.buttonProcessCam0->setEnabled(true);
			ui.buttonProcessCam1->setEnabled(true);
			ui.buttonProcessCam2->setEnabled(true);
			ui.buttonProcessCam3->setEnabled(true);
			ui.buttonRubberResize->setEnabled(true);
			ui.buttonCreateRubber->setEnabled(true);
			ui.buttonDrawIntensity->setEnabled(true);
			ui.buttonFunctionFunctionAddAsMeasuredParameter->setEnabled(true);
			ui.buttonFunctionHeight->setEnabled(true);
			ui.buttonFunctionLineDeviation->setEnabled(true);
			ui.buttonParameterEdit->setEnabled(true);

		}
		else
		{
			ui.addParameter->setEnabled(false);
			ui.removeParameter->setEnabled(false);
			ui.buttonProcessCam0->setEnabled(true);
			ui.buttonProcessCam1->setEnabled(true);
			ui.buttonProcessCam2->setEnabled(true);
			ui.buttonProcessCam3->setEnabled(true);
			ui.buttonRubberResize->setEnabled(false);
			ui.buttonCreateRubber->setEnabled(false);
			ui.buttonDrawIntensity->setEnabled(false);
			ui.buttonFunctionFunctionAddAsMeasuredParameter->setEnabled(false);
			ui.buttonFunctionHeight->setEnabled(false);
			ui.buttonFunctionLineDeviation->setEnabled(false);
			ui.buttonParameterEdit->setEnabled(false);

		}
		ui.update->setEnabled(true);
		ui.Cancel->setEnabled(true);

		
		ui.buttonSurfaceInspection->setEnabled(false);
	}

	else
	{
		ui.addParameter->setEnabled(false);
		ui.removeParameter->setEnabled(false);
		ui.update->setEnabled(false);
		ui.Cancel->setEnabled(false);
		ui.buttonProcessCam0->setEnabled(false);
		ui.buttonProcessCam1->setEnabled(false);
		ui.buttonProcessCam2->setEnabled(true);
		ui.buttonProcessCam3->setEnabled(false);
		ui.buttonSurfaceInspection->setEnabled(false);
	}
	//to je v primeru RLS projekta, Tukaj ne more nobeden spreminjati nicesar

		setWindowState( Qt::WindowActive);
	show();
}

void imageProcessing::ShowDialogPresort(int rights,int currPiece, int presortCalibration)
{
	presortDialog->setWindowModality(Qt::ApplicationModal);
	presortDialog->show();
	currPiecePresort = currPiece;
	lastInHistory = currPiece;
	ShowImagePresort(currPiece);
	presortCalibrationIndex = presortCalibration;
	presortCalibrationIndexTmp = presortCalibration;//index za odprtje combo box na pravem mestu
	UpdateBasicSettings();

	UpdateHistoryWindow();
	OnClickedSelectedAdvSettingsPresrot();
	QString tmp = QString("%1 mm calibration").arg(presortCalibrationIndex);

	for (int i = 0; i < ui2.comboPresortCalibration->count(); i++)
	{
		if (ui2.comboPresortCalibration->itemText(i).compare(tmp) == 0)//nastavimo na index 
		{
			ui2.comboPresortCalibration->setCurrentIndex(i);
		}
	}
	ui2.radioHistory->setChecked(true);
	isLivePresort = 0;

	timer->start(200);
	
}

void imageProcessing::UpdateHistoryWindow()
{
	int count = currPiecePresort;
	if (count > -1)
	{
		for (int i = i = 0; i < 20; i++)
		{
			//labelHistoryImage
			QImage qimgOriginal((uchar*)measureObject->presortHistory[count].buffer->data, measureObject->presortHistory[count].buffer->cols, measureObject->presortHistory[count].buffer->rows, measureObject->presortHistory[count].buffer->step, QImage::Format_RGB888);
			QPixmap pixmap = QPixmap::fromImage(qimgOriginal);
			labelHistoryImage[i]->setPixmap(pixmap);
			labelHistoryImage[i]->setScaledContents(true);
			labelHistoryImage[i]->SetIsGoodLabel(measureObject->presortIsGoodHistory[count]);
			count--;
			if (count < 0)
				count = 19;
		}
	}
}

void imageProcessing::CreateScenePresortDialog()
{
	scenePresortDialog = new QGraphicsScene(this);

	QFont timesNewRomanFont("Times New Roman", 12, QFont::Bold);
	
	ui2.imageView->setScene(scenePresortDialog);
	ui2.imageView->setMouseTracking(true);
	mouseText = new QGraphicsTextItem();
	//mouseText->setPlainText("bla");
	//{ background - color: rgb(255, 0, 0) }
	 
	mouseText = scenePresortDialog->addText("");
	mouseText->setZValue(100);
	mouseText->setFlags(QGraphicsItem::ItemIgnoresTransformations);

	mouseText->setFont(timesNewRomanFont);


	QWidget *widget = new QWidget();
	ui2.scrollAreaHistory->setWidget(widget);
	ui2.scrollAreaHistory->setWidgetResizable(true);
	QHBoxLayout *layout = new QHBoxLayout();
	widget->setLayout(layout);
	ReadBasicParametersPresort();
	ReadCalibrationSettingsPresort();
	ReadAdvancedSettingsPresort();

	ui2.comboAdvancedSettings->clear();
	int sel = -1;
	if (advSettingsPresort.size() > 0)
	{
		for (int i = 0; i< advSettingsPresort.size(); i++)
		{
			ui2.comboAdvancedSettings->addItem(advSettingsPresort[i].name);


			if (types[0][currentType]->selectedColorCameraSettings.compare(advSettingsPresort[i].name) == 0)
			{
				sel = i;
				this->selectedPresortAdvancedSettingsIndex = sel;
				this->selectedPresortAdvancedSettingsIndexTmp = sel;
			}
		}
		if (sel > -1)
		{
			ui2.comboAdvancedSettings->setCurrentIndex(sel);
			selectedPresortAdvancedSettings = advSettingsPresort[sel].name;
		}
	}

	int x; int y;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			AdvToleranceLineEdit[i][j] = new QLineEdit;
			AdvToleranceLineEdit[i][j]->setText(QString("%1-%2").arg(i).arg(j));
			ui2.gridAdvanceSettings->addWidget(AdvToleranceLineEdit[i][j], j + 1, i + 1);



			presortMeasurementsResult[i][j] = new QLabel;
			presortMeasurementsResult[i][j]->setFrameStyle(QFrame::StyledPanel);
			presortMeasurementsResult[i][j]->setText(QString("%1-%2").arg(i).arg(j));
			ui2.gridLayoutResult->addWidget(presortMeasurementsResult[i][j], j + 1, i + 1);
		}
	}

	

	isLivePresort = 0;

	timer = new QTimer();

	connect(timer, SIGNAL(timeout()), this, SLOT(OnViewTimer()));

	for (int i = 4; i < 13; i++)
	{
		ui2.comboPresortCalibration->addItem(QString("%1 mm calibration").arg(i));
	}
	for (int i = 0; i < 20; i++)
	{
		labelHistoryImage[i] = new MBlabel;

		labelHistoryImage[i]->index = i;
		labelHistoryImage[i]->setMinimumHeight(50);
		labelHistoryImage[i]->setMinimumWidth(160);
		labelHistoryImage[i]->setText(QString("%1").arg(i));

		labelHistoryImage[i]->setStyleSheet("border: 5px solid rgb(0, 255, 0);");
		layout->addWidget(labelHistoryImage[i]);
		//layout->insertWidget(i,labelHistoryImage[i]);

		connect(labelHistoryImage[i], SIGNAL(clicked(int)), this, SLOT(OnClickedHistoryLabel(int)));


	
	}
	connect(ui2.buttonSave, SIGNAL(pressed()), this, SLOT(OnClickedSaveBasicSettings()));
	connect(ui2.buttonMeasure, SIGNAL(pressed()), this, SLOT(OnClickedMeasurePresortDowel()));
	connect(ui2.buttonZoomIn, SIGNAL(clicked()), this, SLOT(OnClickedPresortZoomIn()));
	connect(ui2.buttonZoomOut, SIGNAL(clicked()), this, SLOT(OnClickedPresortZoomOut()));
	connect(ui2.buttonZoomReset, SIGNAL(clicked()), this, SLOT(OnClickedPresortZoomReset()));
	connect(ui2.groupBox, SIGNAL(toggled()), this, SLOT(GroupboxChanged()));
	connect(ui2.comboPresortCalibration, SIGNAL(currentIndexChanged(int)), this, SLOT(OnClickedSelectedCalibrationPresort()));
	connect(ui2.comboAdvancedSettings, SIGNAL(currentIndexChanged(int)), this, SLOT(OnClickedSelectedAdvSettingsPresrot()));
	connect(ui2.radioLive, SIGNAL(clicked()), this, SLOT(OnClickedRadioButtonLive()));
	connect(ui2.radioHistory, SIGNAL(clicked()), this, SLOT(OnClickedRadioButtonLive()));
	connect(ui2.buttonCreateNewPresortSettings, SIGNAL(clicked()), this, SLOT(OnClickedCreateNewPresortSettings()));
	connect(ui2.buttonDeletePresortSettings, SIGNAL(clicked()), this, SLOT(OnClickedDeletePresortSettings()));

	//ui2.imageView->fitInView(QRectF(0, 0, measureObject->presortHistory[0].buffer->cols, measureObject->presortHistory[0].buffer->rows), Qt::KeepAspectRatio);

	connect(ui2.buttonClose, SIGNAL(clicked()),this, SLOT(OnClickedClose()));
	//connect(ui2., SIGNAL(destroyed()),this, SLOT(OnClickedClose()));
}

void imageProcessing::ShowImagePresort(int index)
{

	if (index >= 0)
	{
		if (isLivePresort)
			presortImage = &cam[1]->image[0];
		else
			presortImage = &measureObject->presortHistory[index];

		if (presortImage->bufferSize > 0)
		{
			QImage qimgOriginal((uchar*)presortImage->buffer->data, presortImage->buffer->cols, presortImage->buffer->rows, presortImage->buffer->step, QImage::Format_RGB888);
			//pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));

			scenePresortDialog->addPixmap(QPixmap::fromImage(qimgOriginal));
			if(!isLivePresort)
			ui2.imageView->fitInView(QRectF(0, 0, presortImage->buffer->cols, presortImage->buffer->rows), Qt::KeepAspectRatio);
			scenePresortDialog->setSceneRect(QRectF(0, 0, presortImage->buffer->cols, presortImage->buffer->rows));

		}
	}

}

void imageProcessing::ClearDrawPresort()
{
	QList<QGraphicsItem*> selectedItems = scenePresortDialog->items(); // get list of selected items
	foreach(QGraphicsItem* item, selectedItems)
	{
		if (item != mouseText)
		{
			scenePresortDialog->removeItem(item);
			delete item;
		}
	}

	
	ShowImagePresort(currPiecePresort);
}

void imageProcessing::ReadBasicParametersPresort()
{
	int count = 0;
	QStringList values;

	this->basicPresortSettingsPath = referencePath + QString("/%1/Settings/PresortBasicSettings/basic.ini").arg(REF_FOLDER_NAME);


	QSettings settings(this->basicPresortSettingsPath, QSettings::IniFormat);

		count = 0;
		QString tmpGroup;
		tmpGroup = QString("BasicSettingsDetectWindow");
		settings.beginGroup(tmpGroup);
		//inputs
		const QStringList childKeys = settings.childKeys();

		if (childKeys.size() > 0)
		{
			do {

				values.clear();
				values = settings.value(QString("parameter%1").arg(count)).toStringList();
				if (values.size() > 0)
				{
						measureObject->presortBasicDetectWindow[count]= values[0].toFloat();

				}
				count++;
			} while (values.size() > 0);

		}
		settings.endGroup();

		tmpGroup = QString("BasicSettingsDowelSet");
		settings.beginGroup(tmpGroup);
		count = 0;
		const QStringList childKeyss = settings.childKeys();

		if (childKeyss.size() > 0)
		{
			do {

				values.clear();
				values = settings.value(QString("parameter%1").arg(count)).toStringList();
				if (values.size() > 0)
				{
					measureObject->presortBasicDowelSet[count] = values[0].toInt();

				}
				count++;
			} while (values.size() > 0);

		}
		settings.endGroup();



		
}

void imageProcessing::WriteBasicParametersPresort()
{
	QStringList values;

	this->basicPresortSettingsPath = referencePath + QString("/%1/Settings/PresortBasicSettings/basic.ini").arg(REF_FOLDER_NAME);


	QSettings settings(this->basicPresortSettingsPath, QSettings::IniFormat);


	QString tmpGroup;
	QStringList list;
	tmpGroup = QString("BasicSettingsDetectWindow");
	settings.beginGroup(tmpGroup);


	for(int i = 0; i < 5; i++)
	{
	
			list << QString("%1").arg(measureObject->presortBasicDetectWindow[i]);


		settings.setValue(QString("parameter%1").arg(i), list);
		list.clear();
	}
	settings.endGroup();

	tmpGroup = QString("BasicSettingsDowelSet");
	settings.beginGroup(tmpGroup);


	for (int i = 0; i < 6; i++)
	{

		list << QString("%1").arg(measureObject->presortBasicDowelSet[i]);


		settings.setValue(QString("parameter%1").arg(i), list);
		list.clear();
	}
	settings.endGroup();
}

void imageProcessing::UpdateBasicSettings()
{
	for (int i = 0; i < 5; i++)
	{
		measureObject->tmpPresortBasicDetectWindow[i] = measureObject->presortBasicDetectWindow[i];
	}
	ui2.lineBasic_0->setText(QString("%1").arg((int)measureObject->tmpPresortBasicDetectWindow[0]));
	ui2.lineBasic_1->setText(QString("%1").arg((int)measureObject->tmpPresortBasicDetectWindow[1]));
	ui2.lineBasic_2->setText(QString("%1").arg((int)measureObject->tmpPresortBasicDetectWindow[2]));
	ui2.lineBasic_3->setText(QString("%1").arg((int)measureObject->tmpPresortBasicDetectWindow[3]));
	ui2.lineBasic_4->setText(QString("%1").arg((float)measureObject->tmpPresortBasicDetectWindow[4]));

	for (int i = 0; i < 6; i++)
	{
		measureObject->tmppresortBasicDowelSet[i] = measureObject->presortBasicDowelSet[i];
	}

	ui2.lineEdit->setText(QString("%1").arg((int)measureObject->tmppresortBasicDowelSet[0]));
	ui2.lineEdit_2->setText(QString("%1").arg((int)measureObject->tmppresortBasicDowelSet[1]));
	ui2.lineEdit_3->setText(QString("%1").arg((int)measureObject->tmppresortBasicDowelSet[2]));
	ui2.lineEdit_4->setText(QString("%1").arg((int)measureObject->tmppresortBasicDowelSet[3]));
	ui2.lineEdit_5->setText(QString("%1").arg((int)measureObject->tmppresortBasicDowelSet[4]));
	ui2.lineEdit_6->setText(QString("%1").arg((int)measureObject->tmppresortBasicDowelSet[5]));

}

void imageProcessing::OnClickedSaveBasicSettings()
{
	measureObject->tmpPresortBasicDetectWindow[0] = ui2.lineBasic_0->text().toInt();
	measureObject->tmpPresortBasicDetectWindow[1] = ui2.lineBasic_1->text().toInt();
	measureObject->tmpPresortBasicDetectWindow[2] = ui2.lineBasic_2->text().toInt();
	measureObject->tmpPresortBasicDetectWindow[3] = ui2.lineBasic_3->text().toInt();
	measureObject->tmpPresortBasicDetectWindow[4] = ui2.lineBasic_4->text().toFloat();


	for (int i = 0; i < 5; i++)
	{
		measureObject->presortBasicDetectWindow[i] = measureObject->tmpPresortBasicDetectWindow[i];
	}

	measureObject->tmppresortBasicDowelSet[0] = ui2.lineEdit->text().toInt();
	measureObject->tmppresortBasicDowelSet[1] = ui2.lineEdit_2->text().toInt();
	measureObject->tmppresortBasicDowelSet[2] = ui2.lineEdit_3->text().toInt();
	measureObject->tmppresortBasicDowelSet[3] = ui2.lineEdit_4->text().toInt();
	measureObject->tmppresortBasicDowelSet[4] = ui2.lineEdit_5->text().toInt();
	measureObject->tmppresortBasicDowelSet[5] = ui2.lineEdit_6->text().toInt();

	for (int i = 0; i < 6; i++)
	{
		measureObject->presortBasicDowelSet[i] = measureObject->tmppresortBasicDowelSet[i];
	}

	WriteBasicParametersPresort();

	measureObject->presortDowelDiameterWindow[presortCalibrationIndexTmp] = ui2.lineEditWidth->text().toInt();
	measureObject->presortDowelTopOffset[presortCalibrationIndexTmp] = ui2.lineEditTopOffset->text().toInt();
	measureObject->presortDowelBottomOffset[presortCalibrationIndexTmp] = ui2.lineEditBottomOffset->text().toInt();
	WriteCalibrationSettingsPresort(presortCalibrationIndexTmp);


	int count = 0;
	int index = 0;

	for (int i = 0; i < advSettingsPresort.size(); i++)
	{
		if (selectedPresortAdvancedSettings.compare(advSettingsPresort[i].name) == 0)
		{
			index = i;
		}
	}
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			tmpAdvancedSettins[i][j] = AdvToleranceLineEdit[i][j]->text().toInt();
			advSettingsPresort[index].parameterValue[0][count] = tmpAdvancedSettins[i][j];
			count++;
		}
	}
	selectedPresortAdvancedSettingsIndex = selectedPresortAdvancedSettingsIndexTmp;
	types[0][currentType]->selectedColorCameraSettings = advSettingsPresort[index].name;
	WriteAdvancedSettingsPresort(index);
	types[0][currentType]->WriteParameters();

}

void imageProcessing::ReadAdvancedSettingsPresort()
{

	int count = 0;
	QDir dir;


	this->advSettingsPresortPath = referencePath + QString("/%1/settings/PresortSettings/").arg(REF_FOLDER_NAME);



	QSettings settings(advSettingsPresortPath, QSettings::IniFormat);

	dir.setPath(advSettingsPresortPath);
	dir.setNameFilters(QStringList() << "*.ini");
	count = dir.count();
	QStringList list = dir.entryList();

	QStringList ls;


	ls.append("AdvSettings");




	//inputs
	for (int j = 0; j < count; j++)
	{
		advSettingsPresort.push_back(AdvancedSettings(advSettingsPresortPath, list[j], ls));
	}
}

void imageProcessing::WriteAdvancedSettingsPresort(int index)
{

	advSettingsPresort[index].WriteParameters("AdvSettings");
}

void imageProcessing::SelectAdvancedPresortSettings()
{
	int index = -1;
	for (int i = 0; i < advSettingsPresort.size(); i++)
	{
		if (types[0][currentType]->selectedColorCameraSettings.compare(advSettingsPresort[i].name) == 0)
		{
			selectedPresortAdvancedSettingsIndex = i;
			index = i;
			break;
		}
	}
	if (index < 0)
	{
		selectedPresortAdvancedSettingsIndex = 0;
	}
}




void imageProcessing::OnClickedHistoryLabel(int index)
{
	int shownDowel = ((lastInHistory)-index + 20) % 20;
	currPiecePresort = shownDowel;
	ClearDrawPresort();

	ShowImagePresort(currPiecePresort);
}

void imageProcessing::OnClickedMeasurePresortDowel()
{
	ClearDrawPresort();
	ProcessingCamera1(presortImage, 2);//za draw uporabimo 2 zato da vem da moram risat na sceno od presort dialog
	ui2.labelProcessingTime->setText(QString("%1").arg(processingTimer[1]->ElapsedTime()));
}

void imageProcessing::OnClickedPresortZoomIn()
{
	ui2.imageView->scale(1.1, 1.1);
}

void imageProcessing::OnClickedPresortZoomOut()
{
	ui2.imageView->scale(0.9, 0.9);
}

void imageProcessing::OnClickedPresortZoomReset()
{
	ui2.imageView->resetTransform();
	ui2.imageView->fitInView(QRectF(0, 0, presortImage->buffer->cols, presortImage->buffer->rows), Qt::KeepAspectRatio);
}

void imageProcessing::OnClickedSelectedCalibrationPresort()
{
	QString  tmp = ui2.comboPresortCalibration->currentText();
	QString  num = QString("%1%2").arg(tmp[0]).arg(tmp[1]);
	int index = num.toInt();
	presortCalibrationIndexTmp = index;
	ui2.lineEditWidth->setText(QString("%1").arg(measureObject->presortDowelDiameterWindow[presortCalibrationIndexTmp]));
	ui2.lineEditTopOffset->setText(QString("%1").arg(measureObject->presortDowelTopOffset[presortCalibrationIndexTmp]));
	ui2.lineEditBottomOffset->setText(QString("%1").arg(measureObject->presortDowelBottomOffset[presortCalibrationIndexTmp]));



}

void imageProcessing::OnClickedSelectedAdvSettingsPresrot()
{
	QString  tmp = ui2.comboAdvancedSettings->currentText();

	int bla = 100;
	int index = -1;

	for (int i = 0; i < advSettingsPresort.size(); i++)
	{
		if (tmp.compare(advSettingsPresort[i].name) == 0)
		{
			selectedPresortAdvancedSettings = tmp;
			index = i;
		}
	}
	//if(index >=  advSettingsPresort.size())
	//	index = 0;
	selectedPresortAdvancedSettingsIndexTmp = index;
	
	int count = 0;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			tmpAdvancedSettins[i][j] = advSettingsPresort[index].parameterValue[0][count];
			count++;


			AdvToleranceLineEdit[i][j]->setText(QString("%1").arg(tmpAdvancedSettins[i][j]));
		}
	}
}

void imageProcessing::ReadCalibrationSettingsPresort()
{

	int count = 0;

	QStringList values;

	this->calibrationSettingsPresortPath = referencePath + QString("/%1/settings/PresortBasicSettings/calibration.ini").arg(REF_FOLDER_NAME);

	if (!QDir(this->calibrationSettingsPresortPath).exists())
		QDir().mkdir(this->calibrationSettingsPresortPath);

	QSettings settings(this->calibrationSettingsPresortPath, QSettings::IniFormat);

	for (int i = 1; i < 20; i++)
	{
		count = 0;
		QString tmpGroup;
		tmpGroup = QString("%1mm").arg(i);
		settings.beginGroup(tmpGroup);
		//inputs
		const QStringList childKeys = settings.childKeys();

		if (childKeys.size() > 0)
		{
			do {

				values.clear();
				values = settings.value(QString("parameter%1").arg(count)).toStringList();
				if (values.size() > 0)
				{
					if (count  == 0)
						measureObject->presortDowelDiameterWindow[i] = values[0].toInt();
					else if (count == 1)
						measureObject->presortDowelTopOffset[i] = values[0].toInt();
					else if (count == 2)
						measureObject->presortDowelBottomOffset[i] = values[0].toInt();
				}

				count++;

			} while (values.size() > 0);
		}
		settings.endGroup();
	}
}

void imageProcessing::WriteCalibrationSettingsPresort(int index)
{

	QSettings settings(calibrationSettingsPresortPath, QSettings::IniFormat);
	QStringList list;
	QString tmpGroup;
	tmpGroup = QString("%1mm").arg(index);
	//settings.remove(tmpGroup);
	settings.beginGroup(tmpGroup);


	int count = 0;

			list << QString("%1").arg(measureObject->presortDowelDiameterWindow[index]);
			settings.setValue(QString("parameter%1").arg(count), list);
			list.clear();
			count++;

			list << QString("%1").arg(measureObject->presortDowelTopOffset[index]);
			settings.setValue(QString("parameter%1").arg(count), list);
			list.clear();
			count++;

			list << QString("%1").arg(measureObject->presortDowelBottomOffset[index]);
			settings.setValue(QString("parameter%1").arg(count), list);
			list.clear();
			count++;


	
		list.clear();
	


	settings.endGroup();

}

void imageProcessing::OnClickedRadioButtonLive()
{
	int live = 0;

	if (ui2.radioHistory->isChecked())
		live = 0;
	else
		live = 1;

	isLivePresort = live;
}

void imageProcessing::OnClickedClose()
{
	int bla = 100;
	timer->stop();
	//closeEvent(NULL);
	presortDialog->close();
}

void imageProcessing::OnClickedCreateNewPresortSettings()
{
	QInputDialog qDialog;
	QInputDialog nameDialog;
	QStringList items;
	QString name;
	for (int i = 0; i < advSettingsPresort.size(); i++)
	{
		items.append(advSettingsPresort[i].name);
	}
	QString tmp;
	qDialog.setOptions(QInputDialog::UseListViewForComboBoxItems);
	qDialog.setComboBoxItems(items);
	qDialog.setLabelText("COPY ADV. SETTINGS:");
	qDialog.setWindowTitle("SELECT ADV. SETTINGS!");
	if (qDialog.exec() == IDOK)
	{
		tmp = qDialog.textValue();

		nameDialog.setWindowTitle("Add new type");
		nameDialog.setLabelText("Insert type name:");

		if (nameDialog.exec() == IDOK)
		{
			name = nameDialog.textValue();
			CopyAdvancedSetting(tmp, name);
		}
	}


}

void imageProcessing::CopyAdvancedSetting(QString existed, QString newName)
{
	for (int i = 0; i < advSettingsPresort.size(); i++)
	{
		if (existed.compare(advSettingsPresort[i].name) == 0)//preverimo katere nastavitve shrani 
		{
			advSettingsPresort.push_back(AdvancedSettings(advSettingsPresort[i]));
			advSettingsPresort.back().name = newName;
			advSettingsPresort.back().fileName = QString("%1.ini").arg(newName);
			ui2.comboAdvancedSettings->addItem(newName);
		}
	}
}

void imageProcessing::DeleteAdvancedSettings(QString selected)
{
	int sel = 0;
	for (int i = 0; i < advSettingsPresort.size(); i++)
	{
		if (selected.compare(advSettingsPresort[i].name) == 0)//poiscemo izbran index 
		{
			sel = i;
			QString path = advSettingsPresort[i].referencePath + advSettingsPresort[i].fileName;
			QFile::remove(path);
			

			advSettingsPresort.erase(advSettingsPresort.begin() + sel);
			ui2.comboAdvancedSettings->removeItem(i);
		}
	}
}

void imageProcessing::OnClickedDeletePresortSettings()
{
		QInputDialog qDialog;
		QStringList items;
		QString name;

		if (advSettingsPresort.size() > 1)
		{
			for (int i = 0; i < advSettingsPresort.size(); i++)
			{
				items.append(advSettingsPresort[i].name);
			}
			QString tmp;
			qDialog.setOptions(QInputDialog::UseListViewForComboBoxItems);
			qDialog.setComboBoxItems(items);
			qDialog.setLabelText("DELETE ADV. SETINGS:");
			qDialog.setWindowTitle("SELECT ADV. SETTINGS TO DELETE!");
			if (qDialog.exec() == IDOK)
			{
				tmp = qDialog.textValue();

				int ret = QMessageBox::warning(this, tr("Warning"),
					tr("DELETE SELECTED ITEM?"),
					QMessageBox::Ok | QMessageBox::Cancel);

				if (ret == QMessageBox::Ok)
				{
					//name = nameDialog.textValue();
					DeleteAdvancedSettings(tmp);
				}
			}
		}
		else
		{
			QMessageBox::warning(this, tr("Warning"),
				tr("1. SETTINGS FOUND!\n" "CANT ERASE THE ONLY SETTING"),
				QMessageBox::Ok);
		}

}


void imageProcessing::ClearDialog()
{
	int camNum = displayWindow;
	int imgNum = displayImage;;

}


void imageProcessing::NastaviJezikImgProc()
{
	if (slovensko)
	{
		this->setWindowTitle("Obdelava slik");
		ui.tabWidget->setTabText(0, "Obdelava slik");
		ui.tabWidget->setTabText(1, "Kalibracija");
		ui.tabWidget->setTabText(2, "Datoteke");

		ui.buttonSaveImage->setText("Shrani sliko");
		ui.buttonLoadImage->setText("Nalozi sliko");

		ui.labelInsertImageIndex->setText("Vnesi stevilko slike:");


		ui.buttonCamUp->setText("KAM+");
		ui.buttonCamDown->setText("KAM-");
		ui.buttonImageUp->setText("SLIKA+");
		ui.buttonImageDown->setText("SLIKA-");

		ui.addParameter->setText("Dodaj parameter");
		ui.removeParameter->setText("Odstrani parameter");

		ui.Cancel->setText("Preklici");
		ui.update->setText("Shrani");
	}
}

bool imageProcessing::eventFilter(QObject *obj, QEvent *event)
{
	uchar b = 0, g = 0, r = 0;
	QPoint point_mouse;

	QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
	QPointF position = mouseEvent->pos();
	QPointF scenePt = ui.imageView->mapToScene(mouseEvent->pos());
	QPointF scenePt2 = ui2.imageView->mapToScene(mouseEvent->pos());
	//int c = presortImage->buffer->channels();
	int x, y;

	if (event->type() == QEvent::MouseMove)
	{
		x = scenePt2.x();
		y = scenePt2.y();

		if (presortDialog->isVisible())
		{
			QString tmp;

			//mouseText->setPlainText(QString("X= %1, Y = %2").arg((int)scenePt2.x()).arg((int)scenePt2.y()));
			if(currPiecePresort > -1)
			{

				if ((x < presortImage->buffer[0].cols) && (y < presortImage->buffer[0].rows) && (x > -1) && (y > -1))
				{

					b = g = r = presortImage->buffer[0].data[presortImage->buffer[0].channels()*(presortImage->buffer[0].cols*y + x)];

					tmp = (QString("X= %1, Y = %2, INT = %3  ").arg((int)scenePt2.x()).arg((int)scenePt2.y()).arg(b));

				}
				else
					tmp = (QString("X= %1, Y = %2").arg((int)scenePt2.x()).arg((int)scenePt2.y()));

			}


			mouseText->show();
			mouseText->setPos(scenePt2);
			mouseText->setHtml(QString("<div style='background-color: #ffff00;'>") + tmp + "</div>");

		}
		else
			mouseText->hide();




		if (this->isVisible())
		{
			x = scenePt.x();
			y = scenePt.y();
			ui.labelX->setText(QString("X = %1").arg(x));
			ui.labelY->setText(QString("Y = %1").arg(y));

			if (currImage->buffer->empty())
				return false;

			if ((x < currImage->buffer[0].cols) && (y < currImage->buffer[0].rows) && (x > -1) && (y > -1))
			{
				if (currImage->buffer->channels() == 1)
				{
					b = g = r = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x)];
				}
				else
				{
					b = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 0];
					g = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 1];
					r = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 2];
				}

				ui.labelG->setText(QString("G = %1").arg(g));
				ui.labelB->setText(QString("B = %1").arg(b));
				ui.labelR->setText(QString("R = %1 ").arg(r));

				return false;
			}
		}

		return true;
	}

	return false;
}

void imageProcessing::keyPressEvent(QKeyEvent * event)
{
	QPolygonF testPolygon;
	QRectF testR;
	switch (event->key())
	{
	case  Qt::Key_Escape:

		ClearDraw();

		break;
	case  Qt::Key_Return:

	

		break;

	
	default:
		break;
	}

}

void imageProcessing::propertyChanged(QtProperty * property)
{
	int bla = 100;
}


void imageProcessing::OnLoadImage()
{
	Mat img;
	QString fileName = QFileDialog::getOpenFileName(this, tr("Load Image"), "C://", "bmp image (*.bmp)");
	String name;

	int channel;
	if (!fileName.isEmpty())
	{
		QMessageBox::information(this, tr("file loaded"), fileName);
		name = fileName.toStdString();

		img = imread(name, -1);
		*currImage->buffer = img;
		ReplaceCurrentBuffer();
	}


	

}

void imageProcessing::OnLoadMultipleImage()
{
	QFileDialog dialog(this);
	Mat img;
	String name;
	dialog.setDirectory(QDir::homePath());
	dialog.setFileMode(QFileDialog::ExistingFiles);
	dialog.setNameFilter(trUtf8("image file(*.bmp)"));
	dialog.setDirectory("C://");
	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();

	if (!fileNames.isEmpty())
	{
		for (int i = 0; i < fileNames.size(); i++)
		{
			name = fileNames[i].toStdString();
			img = imread(name, -1);
			if (displayWindow < cam.size())
			{
				if (i < cam[displayWindow]->image.size())
				{
					cam[displayWindow]->image[i].buffer[0] = img;
				}
			}
			else
			{
				if (i < images.size())
					*images[i]->buffer = img;

			}
		}
		QMessageBox::information(this, QString("file loaded"),QString("%1 files loaded").arg(fileNames.size()));
		ReplaceCurrentBuffer();
	}

}

void imageProcessing::OnSaveImage()
{
	
	//image.fill(Qt::red); // A red rectangle.
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Image "),
		QString(),
		tr("Images (*.bmp)"));

	String name;
	name = fileName.toStdString();
	if(name.size() > 0)
	imwrite(name, *currImage->buffer);

}




void imageProcessing::ZoomOut()
{
	zoomFactor = zoomFactor - 0.1;
	//if (zoomFactor > 0.09)
	//{
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
		ui.imageView->scale(0.9, 0.9);
	//}
	//else
	//{
		//zoomFactor = 0.1;
		//ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
	//}

}

void imageProcessing::ZoomIn()
{

	zoomFactor = zoomFactor + 0.1;

	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
	ui.imageView->scale(1.1, 1.1);

}

void imageProcessing::ZoomReset()
{

	zoomFactor = 1;
	ui.imageView->resetTransform();
	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
	
}


void imageProcessing::ResizeDisplayRect()
{	
	ui.imageView->setGeometry(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
	ui.imageView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	//ui.imageView->setSceneRect(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
}














void imageProcessing::ClearDraw()
{
	//dodamo na draw sliko 
	qDeleteAll(scene->items());
	//qDeleteAll(sceneForMainWindow->items());
	QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));
	pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));
}
void imageProcessing::ClearFunctionList()
{
	for (int i = 0; i < 50; i++) //za izpise kaj vracajo funkcije
	{
		ui.listFunctionsReturn->item(i)->setText("");
	}
}
void imageProcessing::SetCurrentBuffer(int dispWindow, int dispImage)
{
	displayWindow = dispWindow;
	displayImage = dispImage;
	//ReplaceCurrentBuffer();
}

void imageProcessing::ReplaceCurrentBuffer()
{
	//if ((displayImage != prevDisplayImage) || (displayWindow != prevDisplayWindow))
	//{
		if (displayWindow == cam.size())
		{
			currImage = images[displayImage];
		}
		else
		{
			currImage = &cam[displayWindow]->image[displayImage];
		}

	if (displayWindow == cam.size())
	{
		QString niz;
		if (slovensko) niz = QString("SHRANJENE SLIKE");
		else niz = QString("SAVED IMAGES");

		ui.labelLocation->setText(QString("%1: %2").arg(niz).arg(displayImage));
	}
	else
	{
		if (slovensko)  ui.labelLocation->setText(QString("KAM: %1, SLIKA: %2").arg(displayWindow).arg(displayImage));
		else ui.labelLocation->setText(QString("CAM: %1, IMAGE: %2").arg(displayWindow).arg(displayImage));
	}
		

	ui.lineInsertImageIndex->setText(QString("%1").arg(displayImage));

	//if((displayImage != prevDisplayImage) || ( prevDisplayWindow != displayWindow)) // zaradi cudnega pojava na dialogu
	//ResizeDisplayRect();
	
	
	ClearDraw();
	ShowImages();


	prevDisplayImage = displayImage;
	prevDisplayWindow = displayWindow;

	if(displayWindow < cam.size())
	currentStation = 0;



	ui.labelCurrStation->setText(QString("station: %1").arg(currentStation));
}



void imageProcessing::OnPressedImageUp()
{
	ClearDialog();
	displayImage++;
	if (displayWindow == cam.size())
	{
		if (displayImage > images.size()-1)
			displayImage = 0;
	}
	else
	{
		if (displayImage > cam[displayWindow]->num_images - 1)
			displayImage = 0;

	}
	ReplaceCurrentBuffer();


}

void imageProcessing::OnPressedImageDown()
{
	ClearDialog();
	displayImage--;
	if (displayWindow == cam.size())
	{
		if ((displayImage > images.size()) ||(displayImage < 0))
			displayImage = images.size()-1;
	}
	else
	{
		if((displayImage >= cam[displayWindow]->num_images - 1) || (displayImage < 0))
			displayImage = cam[displayWindow]->num_images - 1;

	}

	ReplaceCurrentBuffer();

}

void imageProcessing::OnPressedCamUp()
{	

	ClearDialog();
	displayImage = 0;
	displayWindow++;

	if ((displayWindow < 0) || (displayWindow > cam.size()))
		displayWindow = 0;

	ReplaceCurrentBuffer();

}

void imageProcessing::OnPressedCamDown()
{
	ClearDialog();
	displayImage = 0;
	displayWindow--;

	if ((displayWindow < 0) || (displayWindow > cam.size()))
		displayWindow = cam.size() ;

	ReplaceCurrentBuffer();

}

int imageProcessing::ConvertImageForDisplay(int imageNumber)
{
	Mat test;

	if (currImage->buffer->empty())
	{
		return 0;
	}

	if(currImage->buffer->channels() == 1)
		cvtColor(*currImage->buffer, test, COLOR_GRAY2RGB);
	else
		cvtColor(*currImage->buffer, test, COLOR_BGR2RGB);


	DrawImage = test(Rect(0, 0, test.cols, test.rows));
	
	//QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	
	//showScene->setPixmap(QPixmap::fromImage(qimgOriginal));

		return 1;
}

void imageProcessing::ShowImages()
{
	ui2.imageView->viewport()->installEventFilter(this);
	if (ConvertImageForDisplay(0))
	{
		QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);

		if (!isSceneAdded)
		{
			scene = new QGraphicsScene(this);
			sceneForMainWindow = new QGraphicsScene(this);
			ui.imageView->setScene(scene);
			setMouseTracking(true);
			ui.imageView->viewport()->installEventFilter(this);
			//scene->installEventFilter(this);
			
			pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));
			pixmapVideo->setZValue(-100);
			isSceneAdded = true;
		}

		pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));
		zoomFactor = 1;
		ui.imageView->resetTransform();
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
		ui.imageView->scale(1, 1);
	}
}
void imageProcessing::OnProcessCam0()
{
	currentFunction = 0;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);
	ClearFunctionList();
	int imageNum = 0;
	bool ok;

	

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera0(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera0(displayWindow, imageNum, 1);
		}
	}
	else
	{
		if (images.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera0(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera0(displayWindow, imageNum, 1);
		}
	}


}
void imageProcessing::OnProcessCam1()//uporablejna za iz vrha obdelavo kosa 
{
	int imageNum = 0;
	bool ok;
	ClearDraw();
	currentFunction = 1;
	PopulatePropertyBrowser(currentFunction);
	ClearFunctionList();
	CMemoryBuffer * image;

	if (displayWindow <= cam.size() - 1)
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			//imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			//if (ok)
			//	ProcessingCamera3(displayWindow, imageNum, 1);
			image = &cam[displayWindow]->image[imageNum];
			ProcessingCamera1(image, 1);
		}
		else
		{
			image = &cam[displayWindow]->image[imageNum];
			ProcessingCamera1(image, 1);
		}

	}
	else
	{
		//imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size()- 1, 1, &ok, NULL);

		//if (ok)
		//	ProcessingCamera3(displayWindow, imageNum, 1);
		//image = &cam[displayWindow]->image[displayImage];
		image = images[displayImage];
		ProcessingCamera1(image, 1);
	}

}

void imageProcessing::OnProcessCam2()
{
	currentFunction = 2;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);

	int imageNum = 0;
	bool ok;

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera2(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera2(displayWindow, imageNum, 1);
		}
	}
	else
	{
		ProcessingCamera2(displayWindow, imageNum, 1);

	}
	

}

void imageProcessing::OnProcessCam3()
{
	currentFunction = 3;
	PopulatePropertyBrowser(currentFunction);
	ClearFunctionList();
	ClearDraw();
		//ProcessingCamera3(0, 0, 1);

	CMemoryBuffer* image;
	int imageNum = 0;
	bool ok;

	if (displayWindow <= cam.size()-1)
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			//imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			//if (ok)
			//	ProcessingCamera3(displayWindow, imageNum, 1);
			image = &cam[displayWindow]->image[imageNum];
			ProcessingCamera3(image, 1);
		}
		else
		{
			image = &cam[displayWindow]->image[imageNum];
			ProcessingCamera3(image, 1);
		}
			
	}
	else
	{
		//imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size()- 1, 1, &ok, NULL);

		//if (ok)
		//	ProcessingCamera3(displayWindow, imageNum, 1);
		//image = &cam[displayWindow]->image[displayImage];
		image = images[displayImage];
		ProcessingCamera3(image, 1);
	}


}


void imageProcessing::OnProcessCam4()
{
	currentFunction = 4;
	PopulatePropertyBrowser(currentFunction);

	ProcessingCamera4(displayWindow, displayImage, 1);
}

void imageProcessing::OnDoneEditingLineInsertImageIndex()
{
	int bla = 100;
	QString index = ui.lineInsertImageIndex->text();
	int indexnum = index.toInt();
	
	int size = images.size();
	if (displayWindow == cam.size())
	{
		if (indexnum > size - 1)
			displayImage = size - 1;
		else if (indexnum < 0)
			displayImage = 0;
		else
			displayImage = indexnum;

	}

	else
	{
		if (indexnum > cam[displayWindow]->num_images - 1)
			displayImage = cam[displayWindow]->num_images - 1;
		else if (indexnum < 0)
			displayImage = 0;
		else
			displayImage = indexnum;

	}


	
	ReplaceCurrentBuffer();

	
}





void imageProcessing::onPressedAddParameter()
{
	bool ok;
	int insertNum = 0;
	
	if (currentFunction > -1)
	{
		insertNum = QInputDialog::getInt(this, tr("QInputDialog::setNumParam()"),
			tr("insert num:"), item.size(), 0, item.size(), 1, &ok);
		if (ok)
		{

			QStringList selectableTypes;
			selectableTypes << tr("bool parameter") << tr("Int Parameter") << tr("Double Parameter") << tr("rectangle") <<tr("PointFloat");
			int propType;

			QString parameterType = QInputDialog::getItem(this, tr("parameter Type select"),
				tr("Select:"), selectableTypes, 0, false, &ok);

			if (ok)
			{
				QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
					tr("Parameter Name:"), QLineEdit::Normal, tr("name"), &ok);
				if (ok)
				{
					for (int i = 0; i < 5; i++)
					{
						if (parameterType.compare(selectableTypes[i]) == 0)
						{
							if (i == 0)//bool value
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Bool, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction][insertNum]->boolValue);
								item[insertNum]->setEnabled(true);
							}
							else if (i == 1)//int value
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Int, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->intValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->intMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->intMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);



							}
							else if (i == 2)
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Double, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);

							}
							else if (i == 3)//rectangle
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Rect, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setEnabled(true);

							}
							else if (i == 4)//Point
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Point, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);

							}


						}
					}
					//item.insert(item.begin() + 2, item.back());
					//topItem->addSubProperty(item[insertNum]);
					PopulatePropertyBrowser(currentFunction);
				}
			}
		}
	}
	else
	{
		 QMessageBox::information(this, tr("information"), "No function is selected");
	}
		

}

void imageProcessing::onPressedRemoveParameters()
{
	int deleteNum = 0;
	bool ok;
	if (currentFunction > -1)
	{
		deleteNum = QInputDialog::getInt(this, tr("QInputDialog::setNumParam()"),
			tr("delete param:"), item.size()-1, 0, item.size(), 1, &ok);
		if (ok)
		{
			types[currentStation][currentType]->prop[currentFunction].erase(types[currentStation][currentType]->prop[currentFunction].begin() + deleteNum);
			//item.erase(item.begin() + deleteNum);

		}


		PopulatePropertyBrowser(currentFunction);

	}
	else
	{
		QMessageBox::information(this, tr("information"), "No function is selected");
	}

}

void imageProcessing::onPressedUpdateParameters()
{


	if (currentFunction > -1)
		SaveFunctionParameters(currentType, currentFunction);
		 //SaveFunctionParameters(currentType, currentFunction);
}

void imageProcessing::onPressedSurfaceInspection()
{
	currentFunction = 8;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);
	

	if(displayWindow == cam.size())
	CheckDefects(displayImage, 1, 1);

}

void imageProcessing::OnPressedCreateRubber()
{
	createRubber = 1;

}

void imageProcessing::OnPressedResizeRubber()
{

	gripTopLeft->setVisible(true);
	//gripTopLeft->repaint();
	gripBottomRight->setVisible(true);
	//gripBottomRight->repaint();
	changeRubberBand = 1;
	if (RectItem[selectedRubberRect]->scene() != nullptr)
	{
		scene->removeItem(RectItem[selectedRubberRect]);
	}

	int x = rubberRect[selectedRubberRect].x();
	int y = rubberRect[selectedRubberRect].y();
	int x2 = x + rubberRect[selectedRubberRect].width();
	int y2 = y + rubberRect[selectedRubberRect].height();
	QPointF topLeft = ui.imageView->mapFromScene(QPointF(x, y));
	QPointF bottomR = ui.imageView->mapFromScene(QPointF(x2, y2));

	QPoint left = topLeft.toPoint();
	QPoint right = bottomR.toPoint();
	rubberBand->setGeometry(QRect(left, right));
	rubberBand->show();

}

void imageProcessing::OnPressedSelectRubber()
{
	rubberResizeMode = 1;
}

void imageProcessing::OnPressedTestIntensityFunction()
{
	selectedFunction = 1;
	if (drawRubberFunction == 0)
	{
		RectItem.clear();
		ClearDraw();
		rubberRect.clear();
		rubberRectFunction.clear();
		drawRubberFunction = 1;
		OnPressedCreateRubber();
	}
	else if (drawRubberFunction == 1)
	{
		ClearDraw();
		scene->addRect(rubberRect[0], QPen(Qt::red), QBrush(Qt::NoBrush));
		RectItem[0] = scene->addRect(rubberRect[0], QPen(Qt::yellow), QBrush(Qt::NoBrush));
		currImage->rect = rubberRect[0];
		currImage->CreateIntensity();
		currImage->VerticalIntensity(2);
		scene->addItem(currImage->DrawIntensity(QPen(Qt::yellow)));
		drawRubberFunction = 0;
		//selectedFunction = -1;
	}


}



void imageProcessing::PopulatePropertyBrowser(int function)
{

	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}

	item.clear();

	
	for (int i = 0; i < types[currentStation][currentType]->prop[function].size(); i++)
	{
		if (types[currentStation][currentType]->prop[function][i]->propType == 0)	//bool
		{
			item.push_back(variantManager->addProperty(QVariant::Bool, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->boolValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 1)	//int
		{
			item.push_back(variantManager->addProperty(QVariant::Int, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->intValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->intMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->intMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 2)	//float
		{
			item.push_back(variantManager->addProperty(QVariant::Double, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->doubleValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->doubleMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->doubleMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 3)	//rect
		{
			item.push_back(variantManager->addProperty(QVariant::Rect, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->rectValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 4)	//CPointFloat 
		{
			item.push_back(variantManager->addProperty(QVariant::Point, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->pointValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else
		{

		}
		if ((loginRights == 1)||(loginRights == 2))
			item.back()->setEnabled(true);
		else
			item.back()->setEnabled(false);
	}

}

void imageProcessing::PopulatePropertyBrowserDynamicFunctions(int function)
{
	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}

	item.clear();
	for (int i = 0; i < 30; i++)
	{
		tmpDynamicParameter[i] = 0;
	}
	if (function == 10)//height or width function in two rect
	{
		item.push_back(variantManager->addProperty(QVariant::Rect, QString("AreaRect 1")));
		item.back()->setValue(QRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]));
		item.back()->setEnabled(true);
		topItem->addSubProperty(item.back());
		item.push_back(variantManager->addProperty(QVariant::Rect, QString("AreaRect 2")));
		item.back()->setValue(QRect(tmpDynamicParameter[5], tmpDynamicParameter[6], tmpDynamicParameter[7], tmpDynamicParameter[8]));
		item.back()->setEnabled(true);
		topItem->addSubProperty(item.back());

		QStringList list;
		list.append("HORIZONTAL");
		list.append("VERTICAL");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("distance Direction")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		list.clear();
		list.append("TOP TO BOTTOM");
		list.append("BOTTOM TO TOP");
		list.append("LEFT TO RIGHT");
		list.append("RIGHT TO LEFT");

		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area 1 direction")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());


		list.clear();
		list.append("WHITE TO BLACK");
		list.append("BLACK TO WHITE");

		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area 1 CONDITION")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());
		

		list.clear();
		list.append("TOP TO BOTTOM");
		list.append("BOTTOM TO TOP");
		list.append("LEFT TO RIGHT");
		list.append("RIGHT TO LEFT");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area 2 direction")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());


		list.clear();
		list.append("WHITE TO BLACK");
		list.append("BLACK TO WHITE");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area 2 CONDITION")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		list.clear();
		list.append("AVRAGE");
		list.append("MINIMUM");
		list.append("MAXIMUM");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("TYPE")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		list.clear();

		item.push_back(variantManager->addProperty(QVariant::Int, QString("FILTER SIZE")));
		item.back()->setAttribute(QLatin1String("minimum"), 1);
		item.back()->setEnabled(true);
		item.back()->setValue(1);
		topItem->addSubProperty(item.back());

		item.push_back(variantManager->addProperty(QVariant::Int, QString("THRESHOLD")));
		item.back()->setAttribute(QLatin1String("minimum"), 1);
		item.back()->setAttribute(QLatin1String("maximum"), 255);
		item.back()->setEnabled(true);
		item.back()->setValue(120);
		topItem->addSubProperty(item.back());

		list.clear();
		list.append("ARROW");
		list.append("LINE");
		list.append("NO DRAW");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("DRAW")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		list.clear();
		list.append("FIRST RECT POINT");
		list.append("SECOND RECT POINT");
		item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("DRAW LOCATION")));
		item.back()->setAttribute("enumNames", list);
		item.back()->setEnabled(true);
		item.back()->setValue(0);
		topItem->addSubProperty(item.back());

		item.push_back(variantManager->addProperty(QVariant::Color, QString("Draw Color")));
		

		int value = 65535;
		//QString hexValue = QString::number(value, 16);
		QString text = QString("%1").number(value, 16);
		text.prepend("#00");
		QColor color(text);
		item.back()->setEnabled(true);
		item.back()->setValue(color);
		topItem->addSubProperty(item.back());

	}
	else if (function == 11)
	{
	item.push_back(variantManager->addProperty(QVariant::Rect, QString("AreaRect 1")));
	item.back()->setValue(QRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]));
	item.back()->setEnabled(true);
	topItem->addSubProperty(item.back());

	QStringList list;
	list.clear();
	list.append("TOP TO BOTTOM");
	list.append("BOTTOM TO TOP");
	list.append("LEFT TO RIGHT");
	list.append("RIGHT TO LEFT");
	item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area direction")));
	item.back()->setAttribute("enumNames", list);
	item.back()->setEnabled(true);
	item.back()->setValue(0);
	topItem->addSubProperty(item.back());


	list.clear();
	list.append("WHITE TO BLACK");
	list.append("BLACK TO WHITE");
	item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("Area CONDITION")));
	item.back()->setAttribute("enumNames", list);
	item.back()->setEnabled(true);
	item.back()->setValue(0);
	topItem->addSubProperty(item.back());


	item.push_back(variantManager->addProperty(QVariant::Int, QString("THRESHOLD")));
	item.back()->setAttribute(QLatin1String("minimum"), 1);
	item.back()->setAttribute(QLatin1String("maximum"), 255);
	item.back()->setEnabled(true);
	item.back()->setValue(120);
	topItem->addSubProperty(item.back());


	list.clear();
	list.append("AVRAGE");
	list.append("MAXIMUM");
	item.push_back(variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString("TYPE")));
	item.back()->setAttribute("enumNames", list);
	item.back()->setEnabled(true);
	item.back()->setValue(0);
	topItem->addSubProperty(item.back());

	list.clear();

	item.push_back(variantManager->addProperty(QVariant::Int, QString("FILTER SIZE")));
	item.back()->setAttribute(QLatin1String("minimum"), 1);
	item.back()->setEnabled(true);
	item.back()->setValue(5);
	topItem->addSubProperty(item.back());

	item.push_back(variantManager->addProperty(QVariant::Color, QString("Draw Color")));


	int value = 60000;
	//QString hexValue = QString::number(value, 16);
	QString text = QString("%1").number(value, 16);
	text.prepend("#00");
	QColor color(text);
	item.back()->setEnabled(true);
	item.back()->setValue(color);
	topItem->addSubProperty(item.back());


	}

		/*QtVariantProperty *reportType = variantManager->addProperty(QtVariantPropertyManager::enumTypeId(), "Report Type");
		QStringList types;
		types << "Bug" << "Suggestion" << "To Do";
		reportType->setAttribute("enumNames", types);
		reportType->setValue(1); // "Suggestion"*/

	/*
			item.push_back(variantManager->addProperty(QVariant::Bool, QString("HEIGHT")));
			item.back()->setValue(true);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());

			item.push_back(variantManager->addProperty(QVariant::Int, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->intValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->intMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->intMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());

			item.push_back(variantManager->addProperty(QVariant::Double, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->doubleValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->doubleMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->doubleMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());

			item.push_back(variantManager->addProperty(QVariant::Rect, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->rectValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());*/

}

void imageProcessing::UpdatePropertyBrwserDynamicFunctions(int function)
{
	if (function == 10)
	{
		item[0]->setValue(QRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]));
		item[1]->setValue(QRect(tmpDynamicParameter[5], tmpDynamicParameter[6], tmpDynamicParameter[7], tmpDynamicParameter[8]));
		item[2]->setValue(tmpDynamicParameter[9]);
		item[3]->setValue(tmpDynamicParameter[10]);
		item[4]->setValue(tmpDynamicParameter[11]);
		item[5]->setValue(tmpDynamicParameter[12]);
		item[6]->setValue(tmpDynamicParameter[13]);
		item[7]->setValue(tmpDynamicParameter[14]);
		item[8]->setValue(tmpDynamicParameter[15]);
		item[9]->setValue(tmpDynamicParameter[16]);
		item[10]->setValue(tmpDynamicParameter[17]);
		item[11]->setValue(tmpDynamicParameter[18]);
		item[12]->setValue(tmpDynamicParameter[19]);


		for (int i = 0; i < 30; i++)
		{
			tmpDynamicParameterold[i] = tmpDynamicParameter[i];
		}
	}
	else if (function == 11)
	{
		item[0]->setValue(QRect(tmpDynamicParameter[1], tmpDynamicParameter[2], tmpDynamicParameter[3], tmpDynamicParameter[4]));
		item[1]->setValue(tmpDynamicParameter[5]);
		item[2]->setValue(tmpDynamicParameter[6]);
		item[3]->setValue(tmpDynamicParameter[7]);
		item[4]->setValue(tmpDynamicParameter[8]);
		item[4]->setValue(tmpDynamicParameter[9]);
		item[4]->setValue(tmpDynamicParameter[10]);//color

		for (int i = 0; i < 30; i++)
		{
			tmpDynamicParameterold[i] = tmpDynamicParameter[i];
		}
	}


}

void imageProcessing::ReadPropertyBrowserDynamicFunctions(int function)
{
	QRect tmpRect;

		if (function == 10)
		{
			tmpRect = item[0]->value().toRect();
			if(rubberRect.size() > 0)
			rubberRect[0] = tmpRect;
			tmpDynamicParameter[1] = tmpRect.x();
			tmpDynamicParameter[2] = tmpRect.y();
			tmpDynamicParameter[3] = tmpRect.width();
			tmpDynamicParameter[4] = tmpRect.height();
			tmpRect = item[1]->value().toRect();
			if (rubberRect.size() > 1)
			rubberRect[1] = tmpRect;
			tmpDynamicParameter[5] = tmpRect.x();
			tmpDynamicParameter[6] = tmpRect.y();
			tmpDynamicParameter[7] = tmpRect.width();
			tmpDynamicParameter[8] = tmpRect.height();
			tmpDynamicParameter[9] = item[2]->value().toInt(); // horizotal or verikal
			tmpDynamicParameter[10] = item[3]->value().toInt(); // rect1 direction
			tmpDynamicParameter[11] = item[4]->value().toInt(); //rect1 black to white
			tmpDynamicParameter[12] = item[5]->value().toInt(); //rect2 direction
			tmpDynamicParameter[13] = item[6]->value().toInt(); //rect2 black to white
			tmpDynamicParameter[14] = item[7]->value().toInt(); ////TYPE
			tmpDynamicParameter[15] = item[8]->value().toInt(); //FILTER SIZE
			tmpDynamicParameter[16] = item[9]->value().toInt(); //THRESHOLD
			tmpDynamicParameter[17] = item[10]->value().toInt(); //TYPE OF DRAW
			tmpDynamicParameter[18] = item[11]->value().toInt(); //LOCATION OF DRAW
			QString colorString = item[12]->value().toString(); //DRAW COLOR 
			bool ok;
			colorString = colorString.remove('#');
			int ColorInt = colorString.toInt(&ok, 16);
			tmpDynamicParameter[19] = ColorInt;

			int bla = 100;
		}
		else if (function == 11)
		{
			tmpRect = item[0]->value().toRect();
			if (rubberRect.size() > 0)
				rubberRect[0] = tmpRect;
			tmpDynamicParameter[1] = tmpRect.x();
			tmpDynamicParameter[2] = tmpRect.y();
			tmpDynamicParameter[3] = tmpRect.width();
			tmpDynamicParameter[4] = tmpRect.height();
			tmpDynamicParameter[5] = item[1]->value().toInt(); // rect direction
			tmpDynamicParameter[6] = item[2]->value().toInt(); // black to white
			tmpDynamicParameter[7] = item[3]->value().toInt(); //prag
			tmpDynamicParameter[8] = item[4]->value().toInt(); //tip
			tmpDynamicParameter[9] = item[5]->value().toInt(); //filter size 

			QString colorString = item[6]->value().toString(); //DRAW COLOR 
			bool ok;
			colorString = colorString.remove('#');
			int ColorInt = colorString.toInt(&ok, 16);
			tmpDynamicParameter[10] = ColorInt;


		}
	
}
void imageProcessing::SaveFunctionParameters(int typeNr, int function)
{
	QStringList list;

	int type = -1;
	int intSize;
	if (function > -1)
	{
		for (int i = 0; i < item.size(); i++)
		{
			if (i < 10)
				intSize = 2;
			else if (i < 100)
				intSize = 3;
			type = item[i]->valueType();
			switch (type)
			{
			case QVariant::Bool: //bool
				types[0][typeNr]->prop[function][i]->propType = 0;
				types[0][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[0][typeNr]->prop[function][i]->boolValue = item[i]->value().toBool();
				break;
			case QVariant::Int: //int
				types[0][typeNr]->prop[function][i]->propType = 1;
				types[0][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[0][typeNr]->prop[function][i]->intValue = item[i]->value().toInt();

				break;
			case QVariant::Double: //double
				types[0][typeNr]->prop[function][i]->propType = 2;
				types[0][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[0][typeNr]->prop[function][i]->doubleValue = item[i]->value().toDouble();
				break;
			case QVariant::Rect: //rectangle
				types[0][typeNr]->prop[function][i]->propType = 3;
				types[0][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[0][typeNr]->prop[function][i]->rectValue = item[i]->value().toRect();
				//types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toInt();
				break;
			case QVariant::Point: //point
				types[0][typeNr]->prop[function][i]->propType = 4;
				types[0][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[0][typeNr]->prop[function][i]->pointValue = item[i]->value().toPoint();
				//types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toInt();
				break;
			}
		}

		//Pot do konfugiracijskih datotek


		QString filePath = types[0][typeNr]->typePath + QString("%1.ini").arg(types[0][typeNr]->typeName);

		QFile file(filePath);
		//file.remove();

		QSettings settings(filePath, QSettings::IniFormat);
		settings.beginGroup(QString("Function%1").arg(function));
		const QStringList childKeys = settings.childKeys();
		if (childKeys.size() == 0)
		{

		}

		for (int i = 0; i < types[currentStation][typeNr]->prop[function].size(); i++)
		{
			switch (types[currentStation][typeNr]->prop[function][i]->propType)
			{
			case 0://bool
				list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->boolValue);
				break;
			case 1://integer
				list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intValue)
					<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intMinValue) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intMaxValue);
				break;
			case 2://double
				list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleValue)
					<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleMinValue) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleMaxValue);
				break;

			case 3://rectangle
				list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.x())
					<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.y()) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.width()) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.height());
				break;
			case 4://PointF
				list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->pointValue.x())
					<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->pointValue.y());

				break;


			}
			settings.setValue(QString("parameter%1").arg(i), list);
			list.clear();
		}
		settings.endGroup();
		QMessageBox::information(this, tr("information"), "Parameters saved");

	}
	else
		QMessageBox::information(this, tr("information"), "No function is selected");


}

void imageProcessing::SaveFunctionParametersOnNewType(int typeNr, int function)
{

	QStringList list;
	//Pot do konfugiracijskih datotek



	QString filePath = types[0][typeNr]->typePath + QString("%1.ini").arg(types[0][typeNr]->typeName);
	QSettings settings(filePath, QSettings::IniFormat);
	QStringList childKeys;

	settings.beginGroup(QString("function%1").arg(function));
	childKeys = settings.childKeys();




	for (int i = 0; i < types[0][typeNr]->prop[function].size(); i++)
	{
		switch (types[0][typeNr]->prop[function][i]->propType)
		{
		case 0://bool
			list << QString("%1").arg(types[0][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[0][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[0][typeNr]->prop[function][i]->boolValue);
			break;
		case 1://integer
			list << QString("%1").arg(types[0][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[0][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[0][typeNr]->prop[function][i]->intValue)
				<< QString("%1").arg(types[0][typeNr]->prop[function][i]->intMinValue) << QString("%1").arg(types[0][typeNr]->prop[function][i]->intMaxValue);
			break;
		case 2://double
			list << QString("%1").arg(types[0][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[0][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[0][typeNr]->prop[function][i]->doubleValue)
				<< QString("%1").arg(types[0][typeNr]->prop[function][i]->doubleMinValue) << QString("%1").arg(types[0][typeNr]->prop[function][i]->doubleMaxValue);
			break;

		case 3://rectangle
			list << QString("%1").arg(types[0][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[0][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[0][typeNr]->prop[function][i]->rectValue.x())
				<< QString("%1").arg(types[0][typeNr]->prop[function][i]->rectValue.y()) << QString("%1").arg(types[0][typeNr]->prop[function][i]->rectValue.width()) << QString("%1").arg(types[0][typeNr]->prop[function][i]->rectValue.height());
			break;
		case 4://pointF
			list << QString("%1").arg(types[0][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[0][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[0][typeNr]->prop[function][i]->pointValue.x())
				<< QString("%1").arg(types[0][typeNr]->prop[function][i]->pointValue.y());


		}
		settings.setValue(QString("parameter%1").arg(i), list);
		list.clear();
	}

	settings.endGroup();

}

void imageProcessing::DeleteTypeProperty(QString  typeName)
{


}

void imageProcessing::LoadTypesProperty(int nrType)
{
	QStringList values;

	QString filePath;
	QString dirPath;
	QDir dir;
	QString functionName;
	int functionNr;
	int n = 0;
	int count = 0;



		QString path = types[0][nrType]->typePath + QString("%1.ini").arg(types[0][nrType]->typeName);
		QSettings settings(path, QSettings::IniFormat);


		QStringList childKeys;

		for (int functionNr = 0; functionNr < 20; functionNr++)//stevilo finckij
		{
			n = 0;
			settings.beginGroup(QString("function%1").arg(functionNr));
			childKeys = settings.childKeys();
			//settings.setValue("parameter0", 1);




			do
			{
				values.clear();
				values = settings.value(QString("parameter%1").arg(n)).toStringList();

				if (values.size() > 1)
				{
					types[0][nrType]->prop[functionNr].push_back(new CProperty());

					switch (values[0].toInt())
					{
					case 0://bool
						types[0][nrType]->prop[functionNr].back()->propType = values[0].toInt();
						types[0][nrType]->prop[functionNr].back()->propName = values[1];
						types[0][nrType]->prop[functionNr].back()->boolValue = values[2].toInt();
						break;
					case 1://integer
						types[0][nrType]->prop[functionNr].back()->propType = values[0].toInt();
						types[0][nrType]->prop[functionNr].back()->propName = values[1];
						types[0][nrType]->prop[functionNr].back()->intValue = values[2].toInt();
						types[0][nrType]->prop[functionNr].back()->intMinValue = values[3].toInt();
						types[0][nrType]->prop[functionNr].back()->intMaxValue = values[4].toInt();
						break;
					case 2://double
						types[0][nrType]->prop[functionNr].back()->propType = values[0].toInt();
						types[0][nrType]->prop[functionNr].back()->propName = values[1];
						types[0][nrType]->prop[functionNr].back()->doubleValue = values[2].toDouble();
						types[0][nrType]->prop[functionNr].back()->doubleMinValue = values[3].toDouble();
						types[0][nrType]->prop[functionNr].back()->doubleMaxValue = values[4].toDouble();
						break;
					case 3://rectangle
						types[0][nrType]->prop[functionNr].back()->propType = values[0].toInt();
						types[0][nrType]->prop[functionNr].back()->propName = values[1];
						types[0][nrType]->prop[functionNr].back()->rectValue.setX(values[2].toInt());
						types[0][nrType]->prop[functionNr].back()->rectValue.setY(values[3].toInt());
						types[0][nrType]->prop[functionNr].back()->rectValue.setWidth(values[4].toInt());
						types[0][nrType]->prop[functionNr].back()->rectValue.setHeight(values[5].toInt());

						break;
					case 4://point
						types[0][nrType]->prop[functionNr].back()->propType = values[0].toInt();
						types[0][nrType]->prop[functionNr].back()->propName = values[1];
						types[0][nrType]->prop[functionNr].back()->pointValue.setX(values[2].toInt());
						types[0][nrType]->prop[functionNr].back()->pointValue.setY(values[3].toInt());


						break;
					}
				}
				n++;
			} while (values.size() > 0);
			settings.endGroup();
		}

						
			


		
}


void imageProcessing::OnViewTimer()
{
	if (!presortDialog->isVisible())
		timer->stop();
	int bla = 100;

	if (isLivePresort)
	{
		ClearDrawPresort();
		ProcessingCamera1(&cam[1]->image[0], 2);
		ui2.labelProcessingTime->setText(QString("%1").arg(processingTimer[1]->ElapsedTime()));
	}
	
}


int imageProcessing::ProcessingCamera0(int nrCam, int imageIndex, int draw)
{
	//processingTimer[0]->SetStart();
	//mainScreenDrawTekoci[0].Reset();

	/*if (draw == 0)
	{
		cam[0]->image[0].SaveBuffer(QString("C:\\images\\"), QString("%1.bmp").arg(imageIndex));
		nrSavedImages[0]++;
		if (nrSavedImages[0] > 100)
			nrSavedImages[0] = 0;
	}*/
	Rect rect;

	rect.x = 0;
	rect.y = 0;
	rect.width = 720;
	rect.height = 300;

	Scalar color;



	//cam[0]->image[0].SaveBuffer(QString("C:\\images\\D%1 N%2.bmp").arg(nrCam).arg(imageIndex));
	cam[0]->image[0].buffer[0].copyTo(measureObject->blowImage[nrCam][imageIndex]);


	if (measureObject->dowelGood[nrCam] == 1)
		color = Scalar(0, 255, 0);
	else
		color = Scalar(0, 0, 255);
	rectangle(measureObject->blowImage[nrCam][imageIndex], rect, color, 5);

	QString tmp = QString("NR: :%1").arg(nrCam);

	putText(measureObject->blowImage[nrCam][imageIndex], tmp.toStdString(), Point(500, 100), FONT_HERSHEY_PLAIN, 1, color);


	Point startPoint;
	startPoint.x = 415;
	startPoint.y = 85;
	//ellipse(measureObject->blowImage[nrCam][imageIndex], startPoint, Size(5, 5), 0, 0, 0, color, 5);


	Rect rectTmp;

	rect.x = 0;
	rect.y = 0;
	rect.width = 720;
	rect.height = 540;
	int offsetX = 25;
	int offsetY = 35;

	QRect startRect;
	Rect drawRect;

	if (imageIndex == 0)
	{
		startRect.setCoords(308, 6, 340, 65);
	}
	else
	{

		startRect.setX(measureObject->centerOfG[nrCam][imageIndex - 1].x - offsetX);
		startRect.setY(measureObject->centerOfG[nrCam][imageIndex - 1].y - offsetY);
		startRect.setWidth(offsetX * 2);
		startRect.setHeight(offsetY * 3);

	}



	drawRect.x = startRect.x();
	drawRect.y = startRect.y();
	drawRect.width = startRect.width();
	drawRect.height = startRect.height();

	rectangle(measureObject->blowImage[nrCam][imageIndex], drawRect, color, 2);


	

	int inten = 0;
	int sumX = 0, sumY = 0, counter = 0;

	if ((startRect.x() > 0 && startRect.y() > 0 && startRect.left() > 0) && startRect.bottom() > 0)
	{

		for (int x = startRect.x(); x < startRect.right(); x++)
		{
			for (int y = startRect.y(); y < startRect.bottom(); y++)

			{
				inten = cam[0]->image[0].GetAverageIntensity(x, y);

				if (inten > 100)//upostevaj;
				{
					sumX += x;
					sumY += y;
					counter++;
				}
			}
		}
		if (counter > 0)
		{
			sumX /= counter;
			sumY /= counter;
		}
		else
		{
			sumX = 0;
			sumY = 0;
		}

		measureObject->centerOfG[nrCam][imageIndex] = CPointFloat(sumX, sumY);
		ellipse(measureObject->blowImage[nrCam][imageIndex], Point(sumX, sumY), Size(5, 5), 0, 0, 0, color, 5);
	}
	else
		return 0;
	//rectangle(measureObject->blowImage[nrCam][imageIndex], 40, 40, 200, 200);
	
	/*cam[nrCam]->image[imageIndex].buffer[0].copyTo(drawImageMain[0]);
	mainScreenDrawTekoci[0].AddImage(drawImageMain[0]);

	mainScreenDrawFinal[0].Reset();
	mainScreenDrawFinal[0] = mainScreenDrawTekoci[0];

	cam[nrCam]->imageReady[imageIndex] = 3;
	emit measurePieceSignal(nrCam, imageIndex);
	

	processingTimer[0]->SetStop();
	processingTimer[0]->ElapsedTime();*/
	return 0;
}
int imageProcessing::ProcessingCamera1(int nrCam, int imageIndex, int draw)//postaja 1 izhodna kamera
{
	processingTimer[1]->SetStart();
	mainScreenDrawTekoci[1].Reset();


	if (draw == 0)
	{

	}


	ProcessCameraForBlobs(nrCam, imageIndex, draw);
	
		//Mat tmp;
	//	cam[nrCam]->image[imageIndex].buffer[0].copyTo(tmp);
	///	mainScreenDrawTekoci[1].AddImage(tmp);

	//mainScreenDrawFinal[1].Reset();
//	mainScreenDrawFinal[1] = mainScreenDrawTekoci[1];


	cam[nrCam]->imageReady[imageIndex] = 3;
	//emit measurePieceSignal(nrCam, imageIndex);

	processingTimer[1]->SetStop();
	processingTimer[1]->ElapsedTime();

	return 0;
}



int imageProcessing::ProcessingCamera2(int nrCam, int imageIndex, int draw)
{
	processingTimer[2]->SetStart();
	mainScreenDrawTekoci[1].Reset();


	int deltaX = 57;
	int startPoint = 136;
	QRect startRect;

	startRect.setCoords(startPoint, 780, startPoint + deltaX,950);

	vector <CPointFloat> points[29][2];
	vector <CPointFloat> startStopPoints[2];
	CRectRotated rotatedRect;
	CLine startLine;
	CLine stopLine;

	
	CLine lines[29][2];


	CLine horizontalLine;

	horizontalLine.SetLineNew(0, 865, 2500, 865);
	scene->addItem(horizontalLine.DrawLineNew(QRect(QPoint(0, 0), QPoint(2000, 1500)), QPen(Qt::green)));


	for (int i = 0; i < 29; i++)
	{
		startRect.setX(startPoint);
		startRect.setWidth(deltaX);

		rotatedRect = startRect;

		if (i == 0)
		{
			images[0]->GetFirstEdgePointsVectorRotated(rotatedRect, 2, 30, 1, true, 80, points[i][0], 1, 1);
			startLine.SetLine(points[i][0],20,5);
		}
		else if (i == 28)
		{
			images[0]->GetFirstEdgePointsVectorRotated(rotatedRect, 2, 30, 1, false, 80, points[i][0], 1, 1);
			stopLine.SetLine(points[i][0], 20, 8);
		}
		else
		{
			images[0]->GetDoubleEdgePointsVectorRotated(rotatedRect, 2, 30, 1, false, 80, points[i], 1, 1);
			lines[i][0].SetLine(points[i][0], 20, 8);
			lines[i][1].SetLine(points[i][0], 20, 8);
		}


		if (draw)
		{
			scene->addRect(startRect, QPen(Qt::red), QBrush(Qt::NoBrush));
			for (int j = 0; j < points[i]->size(); j++)
			{
				if (i == 0)
				{
					scene->addItem(points[i][0][j].DrawDot(QPen(Qt::yellow), QBrush(Qt::yellow), 3));
					scene->addItem(startLine.DrawLine(QRect(QPoint(0, 0), QPoint(2000, 1500)), QPen(Qt::yellow)));
				}
				else if (i == 28)
				{
					scene->addItem(points[i][0][j].DrawDot(QPen(Qt::magenta), QBrush(Qt::magenta), 3));
					scene->addItem(stopLine.DrawLine(QRect(QPoint(0, 0), QPoint(2000, 1500)), QPen(Qt::magenta)));
				}
				else
				{
					scene->addItem(points[i][0][j].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
					scene->addItem(points[i][1][j].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
					scene->addItem(lines[i][0].DrawLine(QRect(QPoint(0, 0), QPoint(2000, 1500)), QPen(Qt::blue)));
					scene->addItem(lines[i][1].DrawLine(QRect(QPoint(0, 0), QPoint(2000, 1500)), QPen(Qt::blue)));
				}
			}
		}

		startPoint = startPoint + deltaX;



	}
	float korFac;
	float distanceAll;


	CPointFloat intersectPointStart, intersectPointStop;
	CPointFloat intersectP[29];
	float dist[29];
	float dist2[29];

	intersectPointStop = stopLine.GetIntersectionPoint(horizontalLine);
	scene->addItem(intersectPointStop.DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));

	intersectPointStart = startLine.GetIntersectionPoint(horizontalLine);
	scene->addItem(intersectPointStart.DrawDot(QPen(Qt::green), QBrush(Qt::green), 5));

	distanceAll = startLine.GetDistanceNew(intersectPointStop);


	korFac = 14.4 / distanceAll;

	int bla = 100;
	QGraphicsTextItem *text[29];
	QGraphicsTextItem *text2[29];
	QGraphicsTextItem *textAll;
	QGraphicsTextItem *textMax;
	float max = 0;
	float min = 1000;
	float min2 = 1000;
	float max2 = 0;

	textAll = new QGraphicsTextItem();
	textMax = new QGraphicsTextItem();
	QFont spinFont("MS Shell Dlg 2", 12, QFont::Bold);
	for (int i = 0; i < 29; i++)
	{
		text[i] = new QGraphicsTextItem();
		text2[i] = new QGraphicsTextItem();
		if (i == 0)
		{

		}
		else if (i == 28)
		{

		}
		else
		{
			intersectP[i] = lines[i][1].GetIntersectionPoint(horizontalLine);
			scene->addItem(intersectP[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 5));

			dist[i] = lines[i][0].GetDistanceNew(intersectP[i]);
			dist[i] *= korFac;
			if (dist[i] < min)
			{
				min = dist[i];
			}
			if (dist[i] > max)
			{
				max = dist[i];
			}
			//text[i]->setPlainText(QString("%1").arg(dist[i]));
			text[i]->setPlainText(QString("%1").number(dist[i], 'f', 3));
			text[i]->setZValue(30);
			text[i]->setDefaultTextColor(QColor(255, 255, 0));
			text[i]->setPos(intersectP[i].x - 20, intersectP[i].y - 20);
			scene->addItem(text[i]);
		}

		 if (i == 1)
		{
			dist2[i] = (intersectP[i].x - intersectPointStart.x)* korFac;
			text2[i]->setPlainText(QString("%1").number(dist2[i], 'f', 3));
			text2[i]->setZValue(30);
			text2[i]->setDefaultTextColor(QColor(0, 255, 255));
			text2[i]->setPos(intersectP[i-1].x - 20, intersectP[i-1].y);
			scene->addItem(text2[i]);

			if (dist2[i] < min2)
			{
				min2 = dist2[i];
			}
			if (dist2[i] > max2)
			{
				max2 = dist2[i];
			}
		}
		else if (i == 28)
		{
			dist2[i] = (intersectPointStop.x - intersectP[i-1].x)* korFac;
			text2[i]->setPlainText(QString("%1").number(dist2[i], 'f', 3));
			text2[i]->setZValue(30);
			text2[i]->setDefaultTextColor(QColor(0, 255, 255));
			text2[i]->setPos(intersectP[i-1].x - 20, intersectP[i-1].y );
			scene->addItem(text2[i]);

			if (dist2[i] < min2)
			{
				min2 = dist2[i];
			}
			if (dist2[i] > max2)
			{
				max2 = dist2[i];
			}
		}
		else if(i > 0)
		 {
			 dist2[i] = (intersectP[i].x - intersectP[i - 1].x)* korFac;
			 text2[i]->setPlainText(QString("%1").number(dist2[i], 'f', 3));
			 text2[i]->setZValue(30);
			 text2[i]->setDefaultTextColor(QColor(0, 255, 255));
			 text2[i]->setPos(intersectP[i-1].x - 20, intersectP[i-1].y);
			 scene->addItem(text2[i]);

			 if (dist2[i] < min2)
			 {
				 min2 = dist2[i];
			 }
			 if (dist2[i] > max2)
			 {
				 max2 = dist2[i];
			 }
		 }

	}
	QString tmp;
	tmp = QString("%1").number(min, 'f', 3);
	tmp = "min:" + tmp;
	text[0]->setPlainText(tmp);

	tmp = QString("%1").number(max, 'f', 3);
	tmp = "max:" + tmp;
	text[28]->setPlainText(tmp);

	tmp = QString("%1").number(distanceAll* korFac, 'f', 3);
	tmp = "width:" + tmp;
	textAll->setPlainText(tmp);

	text[0]->setZValue(30);
	text[28]->setZValue(30);
	textAll->setZValue(30);

	text[0]->setDefaultTextColor(QColor(255, 255, 0));
	text[28]->setDefaultTextColor(QColor(255, 255, 0));
	textAll->setDefaultTextColor(QColor(255, 255, 0));



	tmp = QString("%1").number(min2, 'f', 3);
	tmp = "min:" + tmp;
	text2[0]->setPlainText(tmp);

	tmp = QString("%1").number(max2, 'f', 3);
	tmp = "max:" + tmp;
	textMax->setPlainText(tmp);



	text2[0]->setZValue(30);
	textMax->setZValue(30);


	text2[0]->setDefaultTextColor(QColor(255, 255, 0));
	textMax->setDefaultTextColor(QColor(255, 255, 0));




	text[0]->setPos(50,650);
	text[28]->setPos(50,680);
	text2[0]->setPos(50, 740);
	textMax->setPos(50, 770);
	textAll->setPos(50,710);
	scene->addItem(text[0]);
	scene->addItem(text[28]);
	scene->addItem(text2[0]);
	scene->addItem(textMax);
	scene->addItem(textAll);




	//QPixmap pixMap = ui.imageView->grab(QRect(QPoint(0, 0), QPoint(2500, 1500)));
	QPixmap pixMap = ui.imageView->grab();

	
	int hei = pixMap.height();
	//QFile file("yourFile.png");
	QFile file = QString("C:/images/1.JPG");

	file.open(QIODevice::WriteOnly);
	pixMap.save(&file, "PNG");


	pixMap.save(&file, "JPG");
/*
distanceAll
	CLine tmpLine;
	tmpLie.SetLineNew(points[0]);
	lines[0].push_back(tmpLine);
	tmpLine.SetLineNew(points[1]);
	lines[0].push_back(tmpLine);
	scene->addItem(lines[0][0].DrawLineNew(QRect(QPoint(0, 0), QPoint(2000, 1500)), QPen(Qt::green)));
	scene->addItem(lines[0][1].DrawLineNew(QRect(QPoint(0, 0), QPoint(2000, 1500)), QPen(Qt::green)));
	*/
		/*cam[nrCam]->image[imageIndex].SaveBuffer(QString("C:\\images\\izhodnaCAM_dvolepilec\\"), QString("%1.bmp").arg(0));
		nrSavedImages[2]++;
		if (nrSavedImages[2] > 100)
			nrSavedImages[2] = 0;

	Mat tmp;
	cam[nrCam]->image[imageIndex].buffer[0].copyTo(tmp);
	mainScreenDrawTekoci[1].AddImage(tmp);

	mainScreenDrawFinal[1].Reset();
	mainScreenDrawFinal[1] = mainScreenDrawTekoci[1];


	cam[nrCam]->imageReady[imageIndex] = 3;
	emit measurePieceSignal(nrCam, 1);//v primeru dvolepilca nastavimo na sliko 1
	*/
	processingTimer[2]->SetStop();
	processingTimer[2]->ElapsedTime();

	return 0;
}


int imageProcessing::ProcessingCamera3(int nrCam, int imageIndex, int draw)//obdelava iz vrha povrsina.
{
	processingTimer[3]->SetStart();
	//mainScreenDrawTekoci[3].Reset();
	mainScreenDrawTekoci[1].Reset();



	cam[nrCam]->image[imageIndex].SaveBuffer(QString("C:\\images\\PredObdelavo\\ZgorajPov\\"), QString("%1.bmp").arg(nrSavedImages[3]));
	nrSavedImages[3]++;
	if (nrSavedImages[3] > 15)
		nrSavedImages[3] = 0;
	//FindInnerAndOuterRadij(nrCam, imageIndex, draw);
	int  width = cam[nrCam]->image[imageIndex].buffer[0].cols;
	int  height = cam[nrCam]->image[imageIndex].buffer[0].rows;
	//mainScreenDrawFinal[3].Reset();
	//mainScreenDrawFinal[3] = mainScreenDrawTekoci[3];
	QRect area;
	area.setCoords((width / 2 - 5), 50, width / 2 + 5, height - 50);
	

	mainScreenDrawFinal[1].Reset();
	mainScreenDrawFinal[1] = mainScreenDrawTekoci[1];
	emit measurePieceSignal(nrCam, imageIndex);
	processingTimer[3]->SetStop();
	processingTimer[3]->ElapsedTime();

	return 0;
}

int imageProcessing::ProcessingCamera4(int id, int imageIndex, int draw) //nova funkcija za poteg karakteristike  modula X os 
{
	int bla = 100;
	

	return 0;
}

int imageProcessing::ProcessingCamera3(CMemoryBuffer * image, int draw)
{
	processingTimer[3]->SetStart();
	//mainScreenDrawTekoci[3].Reset();
	mainScreenDrawTekoci[1].Reset();
	//mainScreenDrawFinal[1].Reset();
	//mainScreenDrawFinal[1] = mainScreenDrawTekoci[1];
	//presortingCameraCounter++;


	//image->SaveBuffer(QString("C:\\images\\PredObdelavo\\ZgorajPov\\"), QString("%1.bmp").arg(nrSavedImages[3]));
	////nrSavedImages[3]++;
	//if (nrSavedImages[3] > 15)
	//	nrSavedImages[3] = 0;

	int  width = image->buffer->cols;
	int  height = image->buffer->rows;
	//mainScreenDrawFinal[3].Reset();
	//mainScreenDrawFinal[3] = mainScreenDrawTekoci[3];



	//test contours




	QRect area;
	area.setCoords((width / 2 - 5), 50, width / 2 + 5, height - 50);

	image->buffer->copyTo(images[presortingIndex]->buffer[0]);
	presortingIndex++;

	if (presortingIndex > 99)
		presortingIndex = 30;

	mainScreenDrawFinal[1].Reset();
	mainScreenDrawFinal[1] = mainScreenDrawTekoci[1];
	//emit measurePieceSignal(nrCam, imageIndex);
	processingTimer[3]->SetStop();
	processingTimer[3]->ElapsedTime();

	return 0;
}
void imageProcessing::GetPresortParameters(int draw)
{
	if ((draw == 0)|| (draw == 1))//parametri so v zivo 
	{
		measureObject->xStartDetectWindow = measureObject->presortBasicDetectWindow[1];
		measureObject->xStopDetectWindow = measureObject->presortBasicDetectWindow[2];
		measureObject->yMiddleDetectWindow =  measureObject->presortBasicDetectWindow[3];
		measureObject->thresholdDetectWindow = measureObject->presortBasicDetectWindow[0];
		measureObject->korFactorPresort =  measureObject->presortBasicDetectWindow[4];

		measureObject->dowelHeight = measureObject->presortDowelDiameterWindow[measureObject->selectedCalibration];
		measureObject->dowelOffsetTop = measureObject->presortDowelTopOffset[measureObject->selectedCalibration];
		measureObject->dowelOffsetBottom = measureObject->presortDowelBottomOffset[measureObject->selectedCalibration];

		measureObject->xlCenter = measureObject->presortBasicDowelSet[0];
		measureObject->xrCenter=measureObject->presortBasicDowelSet[1];
		measureObject->xlTop=measureObject->presortBasicDowelSet[2];
		measureObject->xrTop = measureObject->presortBasicDowelSet[3];
		measureObject->xlBottom = measureObject->presortBasicDowelSet[4];
		measureObject->xrBottom = measureObject->presortBasicDowelSet[5];

		int count = 0;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				measureObject->presortSettings[i][j] = advSettingsPresort[selectedPresortAdvancedSettingsIndex].parameterValue[0][count];
				count++;
			}
		}

	}
	else if (draw == 2)
	{
		measureObject->xStartDetectWindow = ui2.lineBasic_1->text().toInt();
		measureObject->xStopDetectWindow = ui2.lineBasic_2->text().toInt();
		measureObject->yMiddleDetectWindow = ui2.lineBasic_3->text().toInt();
		measureObject->thresholdDetectWindow = ui2.lineBasic_0->text().toInt();
		measureObject->korFactorPresort = ui2.lineBasic_4->text().toFloat();

		measureObject->dowelHeight = ui2.lineEditWidth->text().toInt();
		measureObject->dowelOffsetTop = ui2.lineEditTopOffset->text().toInt();
		measureObject->dowelOffsetBottom = ui2.lineEditBottomOffset->text().toInt();

		measureObject->xlCenter = ui2.lineEdit->text().toInt();
		measureObject->xrCenter = ui2.lineEdit_2->text().toInt();
		measureObject->xlTop = ui2.lineEdit_3->text().toInt();
		measureObject->xrTop = ui2.lineEdit_4->text().toInt();
		measureObject->xlBottom = ui2.lineEdit_5->text().toInt();
		measureObject->xrBottom = ui2.lineEdit_6->text().toInt();

		int count = 0;

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				measureObject->presortSettings[i][j] = AdvToleranceLineEdit[i][j]->text().toInt();
				count++;
			}
		}
	}
}


int imageProcessing::ProcessingCamera1(CMemoryBuffer * image, int draw)//predsortiranje moznikov
{	processingTimer[1]->SetStart();

	int width = image->buffer[0].rows;
	int height = image->buffer[0].cols;
	Mat realImage;
	int dowelOn = 0;
	QRect detectRect;
	QRect detectleft;
	QRect detectRight;
	image->buffer[0].copyTo(realImage);

	imageCounterPresort++;
	int lastLeft = 0;
	int lastRight = 0;

	GetPresortParameters(draw); //tukaj izberemo parametre glede na to ali je obdelava v zivo ali iz dialoga 

	measureObject->xStartDetectWindow;
	measureObject->xStopDetectWindow;
	measureObject->yMiddleDetectWindow;
	measureObject->thresholdDetectWindow;
	measureObject->korFactorPresort;

	detectRect.setCoords(measureObject->xStartDetectWindow , measureObject->yMiddleDetectWindow -2, measureObject->xStopDetectWindow, measureObject->yMiddleDetectWindow+ 2);
	detectleft = detectRect;
	detectleft.setWidth(detectRect.width() / 2);


	if (draw == 1)
	{
		scene->addRect(detectleft, QPen(Qt::red, 1), QBrush(Qt::NoBrush));
		scene->addRect(detectRect, QPen(Qt::yellow, 1), QBrush(Qt::NoBrush));

	}


	image->rect = detectRect;
	image->CreateIntensity();
	image->VerticalIntensity(2);
	image->rect = detectleft;

	image->DetectTransitionsWhiteToBlack(measureObject->thresholdDetectWindow, 230);

	vector<CPointFloat> leftPoints;
	if (image->detectedPointsLightToDark.size() > 0)
	{
		for (int i = 0; i < image->detectedPointsLightToDark.size(); i++)
		{
			if (draw == 1)
			{
				scene->addItem(image->detectedPointsLightToDark[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
			}
			/*else if (draw == 2)
			{
				scenePresortDialog->addItem(image->detectedPointsLightToDark[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
			}*/
			lastLeft = image->detectedPointsLightToDark[i].x;
			leftPoints.push_back(image->detectedPointsLightToDark[i]);
		}
	}



	QRect dowelRectMiddle;
	QRect dowelRectTop;
	QRect dowelRectBottom;
	//measureObject->dowelHeight = measureObject->presortDowelDiameterWindow[measureObject->selectedCalibration];
	//measureObject->dowelOffsetTop = measureObject->presortDowelTopOffset[measureObject->selectedCalibration];
	

	int dowelHeight = measureObject->dowelHeight;
	int yOffsetTop = measureObject->dowelOffsetTop;
	int yOffsetBottom = measureObject->dowelOffsetBottom;
	int left;
	int right;

	int xlCenter = measureObject->xlCenter; 
	int xrCenter = measureObject->xrCenter;
	int xlWinUp = measureObject->xlTop;
	int xrWinUp = measureObject->xrTop;
	int xlWinDown = measureObject->xlBottom;
	int xrWinDown = measureObject->xrBottom;

	

	//varovalke
	if ((dowelHeight < 0) || (dowelHeight > height / 2))
	{
		dowelHeight = 0;
	}
	if ((yOffsetTop - dowelHeight < 0 ) || (yOffsetTop > height + dowelHeight))
	{
		yOffsetTop = 0;
	}
	if ((yOffsetBottom + dowelHeight < 0) || (yOffsetBottom > height + dowelHeight))
	{
		yOffsetBottom = 0;
	}


	//filter blobs 
	int blobThresholdA0[3] = { 40,50,40 };
	int blobThresholdA1[3] = { 40,50,40 };
	int blobThresholdOffsetA0[3] = { -40,-40,-40 };
	int blobThresholdOffsetA1[3] = { -40,-40,-40 };
	int blobAreaA0[3] = { 5,5,5 };
	int blobWidthA0[3] = { 3,3,3 };
	int blobHeightA0[3] = { 2,2,2 };

	int blobAreaA1[3] = { 5,5,5 };
	int blobWidthA1[3] = { 3,3,3 };
	int blobHeightA1[3] = { 2,2,2 };
	float stDev[3] = { 0,0,0 };
	float avrage[3] = { 0,0,0 };
	int nrContoursA0[3] = { 0,0,0 };
	int nrContoursA1[3] = { 0,0,0 };
	int avrMin[3] = { 50,80,50 };
	int stdMax[3] = { 15,15,15 };
	QColor goodColor = QColor(150, 255, 150);
	QColor badColor = QColor(255, 150, 150);

	for (int i = 0; i < 3; i++)
	{
		avrMin[i] = measureObject->presortSettings[i][0];
		stdMax[i] = measureObject->presortSettings[i][1];
		blobThresholdOffsetA0[i] = measureObject->presortSettings[i][2];
		blobThresholdOffsetA1[i] = measureObject->presortSettings[i][6];
		blobAreaA0[i] = measureObject->presortSettings[i][3];
		blobWidthA0[i] = measureObject->presortSettings[i][4];
		blobHeightA0[i] = measureObject->presortSettings[i][5];

		blobAreaA1[i] = measureObject->presortSettings[i][7];
		blobWidthA1[i] = measureObject->presortSettings[i][8];
		blobHeightA1[i] = measureObject->presortSettings[i][9];

	}




	if (lastLeft > 0)
	{

		realImage.copyTo(images[nrSavedImages[1]]->buffer[0]);
		nrSavedImages[1]++;
		if (nrSavedImages[1] >= 30)
			nrSavedImages[1] = 0;


		detectRight = detectRect;
		//detectRight.setX(detectRect.x() + detectRect.width() / 2);
		detectRight.setX(lastLeft);
		detectRight.setBottomRight(QPoint(detectRect.right(), detectRect.bottom()));
		//detectRight.(detectRect.width() / 2);

		image->rect = detectRight;
		image->DetectTransitionsWhiteToBlackBackWard(measureObject->thresholdDetectWindow, 230);

		if (image->detectedPointsLightToDark.size() > 0)
		{
			for (int i = 0; i < image->detectedPointsLightToDark.size(); i++)
			{
				if (draw == 1)
				{
					scene->addItem(image->detectedPointsLightToDark[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
				}
				/*else if(draw == 2)
				{
					scenePresortDialog->addItem(image->detectedPointsLightToDark[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
				}*/
				lastRight = image->detectedPointsLightToDark[i].x;
			}
		}
		if (draw > 0)
		{
			scene->addRect(detectRight, QPen(Qt::blue, 1), QBrush(Qt::NoBrush));
		}



	}
	if (lastRight > 0)
	{
		int bla = 100;
	}



	int startEndArea = 20;//obmocje za A1 od zacetka in konca merilnega obmocja
	int isGood[5] = {1,1,1,1,1};
	int center = 0;
	int centerImage = detectRect.center().x();
	Mat drawImage;
	int maxWidthA0[3] = { 0,0,0 };
	int maxHeightA0[3] = { 0,0,0 };
	int maxAreaA0[3]= { 0,0,0 };
	int maxWidthA1[3] = { 0,0,0 };
	int maxHeightA1[3] = { 0,0,0 };
	int maxAreaA1[3] = { 0,0,0 };
	float lenght = 0; 


	lenght = (lastRight - lastLeft) * measureObject->korFactorPresort;
	realImage.copyTo(drawImage);//slika za izris na zaslon
	if ((lenght < measureObject->setLenght + 10) && (lenght > 0))
	{
		if ((lastLeft > 0) && ((lastRight < 350) && (lastRight > 0)))//kadar je vsaj eden moznik v obmocju
		{
			center = (lastRight - lastLeft) / 2 + lastLeft;
			//positionForSpeed[positionCounter] = center;
			//realImage.copyTo(images[positionCounter +31]->buffer[0]);
			
			if ((center > centerImage - 30) && (center < centerImage + 30))
			{
				processingTimer[2]->SetStart();
				//mainScreenDrawTekoci[1].Reset();

				presortingCameraCounter++;
				image->buffer->copyTo(images[presortingIndex]->buffer[0]);
				presortingIndex++;

				if (presortingIndex > 99)
					presortingIndex = 30;
				right = lastRight;
				left = lastLeft;
				dowelRectMiddle.setTopLeft(QPoint(left + xlCenter, detectRect.center().y() - dowelHeight / 2));
				dowelRectMiddle.setBottomRight(QPoint(right + xrCenter, detectRect.center().y() + dowelHeight / 2));

				dowelRectTop.setTopLeft(QPoint(left + xlWinUp, detectRect.center().y() - yOffsetTop - dowelHeight / 2));
				dowelRectTop.setBottomRight(QPoint(right + xrWinUp, detectRect.center().y() - yOffsetTop + dowelHeight / 2));

				dowelRectBottom.setTopLeft(QPoint(left + xlWinDown, detectRect.center().y() + yOffsetBottom - dowelHeight / 2));
				dowelRectBottom.setBottomRight(QPoint(right + xrWinDown, detectRect.center().y() + yOffsetBottom + dowelHeight / 2));

				int conus = dowelHeight / 2;
				conus = conus / 2;

				QPolygon middlePolygon, topPolygon, bottomPolygon;
				vector <Point> polygon;
				polygon.resize(8);
				middlePolygon.append(QPoint(dowelRectMiddle.topLeft().x(), dowelRectMiddle.topLeft().y() + conus));
				middlePolygon.append(QPoint(dowelRectMiddle.topLeft().x() + conus, dowelRectMiddle.topLeft().y()));
				middlePolygon.append(QPoint(dowelRectMiddle.topRight().x() - conus, dowelRectMiddle.topRight().y()));
				middlePolygon.append(QPoint(dowelRectMiddle.topRight().x(), dowelRectMiddle.topRight().y() + conus));
				middlePolygon.append(QPoint(dowelRectMiddle.bottomRight().x(), dowelRectMiddle.bottomRight().y() - conus));
				middlePolygon.append(QPoint(dowelRectMiddle.bottomRight().x() - conus, dowelRectMiddle.bottomRight().y()));
				middlePolygon.append(QPoint(dowelRectMiddle.bottomLeft().x() + conus, dowelRectMiddle.bottomLeft().y()));
				middlePolygon.append(QPoint(dowelRectMiddle.bottomLeft().x(), dowelRectMiddle.bottomLeft().y() - conus));

				topPolygon.append(QPoint(dowelRectTop.topLeft().x(), dowelRectTop.topLeft().y() + conus));
				topPolygon.append(QPoint(dowelRectTop.topLeft().x() + conus, dowelRectTop.topLeft().y()));
				topPolygon.append(QPoint(dowelRectTop.topRight().x() - conus, dowelRectTop.topRight().y()));
				topPolygon.append(QPoint(dowelRectTop.topRight().x(), dowelRectTop.topRight().y() + conus));
				topPolygon.append(QPoint(dowelRectTop.bottomRight().x(), dowelRectTop.bottomRight().y() - conus));
				topPolygon.append(QPoint(dowelRectTop.bottomRight().x() - conus, dowelRectTop.bottomRight().y()));
				topPolygon.append(QPoint(dowelRectTop.bottomLeft().x() + conus, dowelRectTop.bottomLeft().y()));
				topPolygon.append(QPoint(dowelRectTop.bottomLeft().x(), dowelRectTop.bottomLeft().y() - conus));

				bottomPolygon.append(QPoint(dowelRectBottom.topLeft().x(), dowelRectBottom.topLeft().y() + conus));
				bottomPolygon.append(QPoint(dowelRectBottom.topLeft().x() + conus, dowelRectBottom.topLeft().y()));
				bottomPolygon.append(QPoint(dowelRectBottom.topRight().x() - conus, dowelRectBottom.topRight().y()));
				bottomPolygon.append(QPoint(dowelRectBottom.topRight().x(), dowelRectBottom.topRight().y() + conus));
				bottomPolygon.append(QPoint(dowelRectBottom.bottomRight().x(), dowelRectBottom.bottomRight().y() - conus));
				bottomPolygon.append(QPoint(dowelRectBottom.bottomRight().x() - conus, dowelRectBottom.bottomRight().y()));
				bottomPolygon.append(QPoint(dowelRectBottom.bottomLeft().x() + conus, dowelRectBottom.bottomLeft().y()));
				bottomPolygon.append(QPoint(dowelRectBottom.bottomLeft().x(), dowelRectBottom.bottomLeft().y() - conus));


				if (draw == 1)
				{
					scene->addRect(dowelRectMiddle, QPen(Qt::green, 1), QBrush(Qt::NoBrush));
					scene->addRect(dowelRectTop, QPen(Qt::red, 1), QBrush(Qt::NoBrush));
					scene->addRect(dowelRectBottom, QPen(Qt::blue, 1), QBrush(Qt::NoBrush));
					scene->addPolygon(middlePolygon, QPen(Qt::yellow), QBrush(Qt::NoBrush));
					scene->addPolygon(topPolygon, QPen(Qt::yellow), QBrush(Qt::NoBrush));
					scene->addPolygon(bottomPolygon, QPen(Qt::yellow), QBrush(Qt::NoBrush));
				}


				int offsetX, offsetY;


				Mat originalImage[3];
				Mat processedImage[3];
				Mat processedImage2[3];
				Rect cropRect[3];
				cropRect[0].x = dowelRectTop.x();
				cropRect[0].y = dowelRectTop.y();
				cropRect[0].width = dowelRectTop.width();
				cropRect[0].height = dowelRectTop.height();

				cropRect[1].x = dowelRectMiddle.x();
				cropRect[1].y = dowelRectMiddle.y();
				cropRect[1].width = dowelRectMiddle.width();
				cropRect[1].height = dowelRectMiddle.height();

				cropRect[2].x = dowelRectBottom.x();
				cropRect[2].y = dowelRectBottom.y();
				cropRect[2].width = dowelRectBottom.width();
				cropRect[2].height = dowelRectBottom.height();


				Point rook_points[8];



				for (int nrRect = 0; nrRect < 3; nrRect++)
				{
					offsetX = cropRect[nrRect].x;
					offsetY = cropRect[nrRect].y;
					for (int i = 0; i < 8; i++)
					{
						if (nrRect == 0)
							polygon[i] = Point(topPolygon[i].x() - offsetX, topPolygon[i].y() - offsetY);
						else if (nrRect == 1)
							polygon[i] = Point(middlePolygon[i].x() - offsetX, middlePolygon[i].y() - offsetY);
						else
							polygon[i] = Point(bottomPolygon[i].x() - offsetX, bottomPolygon[i].y() - offsetY);
					}
					
					for (int i = 0; i < 8; i++)
					{
						rook_points[i] = polygon[i];
					}
					

					const Point* ppt[1] = { rook_points };
					int npt[] = { 8 };

				polylines(drawImage(cropRect[nrRect]), ppt, npt, 1, true, cv::Scalar(255, 0, 0), 1, 8, 0);

				realImage(cropRect[nrRect]).copyTo(originalImage[nrRect]);
					originalImage[nrRect].copyTo(processedImage[nrRect]);
					// Create the mask with the polygon
					Mat mask = cv::Mat::zeros(cropRect[nrRect].height, cropRect[nrRect].width, CV_8U);



					fillPoly(mask, polygon, Scalar(255));

					cvtColor(processedImage[nrRect], processedImage[nrRect], COLOR_BGR2GRAY);
					bitwise_and(processedImage[nrRect], mask, processedImage[nrRect]);

					processedImage[nrRect].copyTo(originalImage[nrRect]);



					int inte;


					processedImage[nrRect].copyTo(processedImage2[nrRect]);
					for (int x = 0; x < processedImage[nrRect].cols; x++)
					{
						for (int y = 0; y < processedImage[nrRect].rows; y++)
						{
							inte = processedImage[nrRect].data[processedImage[nrRect].channels()*(processedImage[nrRect].cols*y + x) + 0];
							if (inte == 0)
							{
								processedImage[nrRect].data[processedImage[nrRect].channels()*(processedImage[nrRect].cols*y + x) + 0] = 255;
								processedImage2[nrRect].data[processedImage[nrRect].channels()*(processedImage[nrRect].cols*y + x) + 0] = 255;
							}
							else
							{

								if ((x > startEndArea) && (x < processedImage[nrRect].cols - startEndArea))
								{
									processedImage2[nrRect].data[processedImage[nrRect].channels()*(processedImage[nrRect].cols*y + x) + 0] = 255;
								}
								//if()
							}

						}
					}
					measureObject->centerPresort = ((right - left) /2 + left);
					if (draw == 1)
					{
						processedImage[nrRect].copyTo(images[47 + nrRect]->buffer[0]);
						processedImage2[nrRect].copyTo(images[50 + nrRect]->buffer[0]);
					}
					Moments m;
					Moments m2;
					Scalar mean;
					Scalar stddev;
					meanStdDev(originalImage[nrRect], mean, stddev, mask = mask);
					avrage[nrRect] = mean[0];
					stDev[nrRect] = stddev[0];
					blobThresholdA0[nrRect] = avrage[nrRect] + blobThresholdOffsetA0[nrRect];
					blobThresholdA1[nrRect] = avrage[nrRect] + blobThresholdOffsetA1[nrRect];
					if (blobThresholdA0[nrRect] < 20)
						blobThresholdA0[nrRect] = 20;
					if (blobThresholdA0[nrRect] > 255)
						blobThresholdA0[nrRect] = 255;
					if (blobThresholdA1[nrRect] < 20)
						blobThresholdA1[nrRect] = 20;
					if (blobThresholdA1[nrRect] > 255)
						blobThresholdA1[nrRect] = 255;
					threshold(processedImage[nrRect], processedImage[nrRect], blobThresholdA0[nrRect], 255, THRESH_BINARY_INV);
					threshold(processedImage2[nrRect], processedImage2[nrRect], blobThresholdA1[nrRect], 255, THRESH_BINARY_INV);

					if (draw == 1)
					{
						processedImage[nrRect].copyTo(images[nrRect + 53]->buffer[0]);
						processedImage2[nrRect].copyTo(images[nrRect + 56]->buffer[0]);
					}

					std::vector<std::vector<cv::Point> > contours;
					std::vector<std::vector<cv::Point> > contours2;


					cv::findContours(processedImage[nrRect], contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
					cv::findContours(processedImage2[nrRect], contours2, RETR_LIST, CHAIN_APPROX_SIMPLE);

					cv::Scalar colors;
					cv::Scalar colors2;
					colors = cv::Scalar(0, 0, 255);
					colors2 = cv::Scalar(0, 255, 0);

					cvtColor(originalImage[nrRect], originalImage[nrRect], COLOR_GRAY2BGR);
					float max = 0;
					float max2 = 0;
					int index = -1;
					int index2 = -1;
					Rect contourB;


					//oblike obmocje 0 obmocje cez cel moznik 
					for (size_t idx = 0; idx < contours.size(); idx++)
					{
						m = moments(contours[idx]);
						contourB = boundingRect(contours[idx]);
						if ((m.m00 > blobAreaA0[nrRect]) && (contourB.width > blobWidthA0[nrRect]) && (contourB.height > blobHeightA0[nrRect]))
						{
							nrContoursA0[nrRect]++;
							if (m.m00 > max)
							{
								max = m.m00;
								index = idx;

								maxWidthA0[nrRect] = contourB.width;
								maxHeightA0[nrRect] = contourB.height;
								maxAreaA0[nrRect] = max;

							}
						}
					}

					for (size_t idx = 0; idx < contours2.size(); idx++)
					{
						m = moments(contours2[idx]);
						contourB = boundingRect(contours2[idx]);

						if (m.m00 > blobAreaA1[nrRect] && contourB.width > blobWidthA1[nrRect] && contourB.height > blobHeightA1[nrRect])
						{
							nrContoursA1[nrRect]++;

							if (m.m00 > max2)
							{
								max2 = m.m00;
								index2 = idx;

								maxWidthA1[nrRect] = contourB.width;
								maxHeightA1[nrRect] = contourB.height;
								maxAreaA1[nrRect] = max2;

							}

						}

					}
					if (draw == 1)
					{
						originalImage[nrRect].copyTo(images[nrRect + 59]->buffer[0]);
					}

					if (index > -1)
						cv::drawContours(drawImage(cv::Rect(cropRect[nrRect].x, cropRect[nrRect].y, originalImage[nrRect].cols, originalImage[nrRect].rows)), contours, index, colors, -1);

					if (index2 > -1)
						cv::drawContours(drawImage(cv::Rect(cropRect[nrRect].x, cropRect[nrRect].y, originalImage[nrRect].cols, originalImage[nrRect].rows)), contours2, index2, colors2, -1);
					//cv::drawContours(newImage(cv::Rect(cropRect[nrRect].x, cropRect[nrRect].y, originalImage[nrRect].cols, originalImage[nrRect].rows)), contours, -1, colors, -1);




					avrage[nrRect] = mean[0];
					stDev[nrRect] = stddev[0];

					for (int m = 0; m < 3; m++)
					{
						measureObject->isGoodPresort[m][nrRect] = 1;

					}
					measureObject->avrIntPresort[nrRect] = 0;
					measureObject->stDevPresort[nrRect] = 0;
					measureObject->nrBlobsPresort[nrRect] = 0;


					measureObject->avrIntPresort[nrRect] = avrage[nrRect];
					measureObject->stDevPresort[nrRect] = stDev[nrRect];
					measureObject->nrBlobsPresort[nrRect] = nrContoursA0[nrRect];

					if (draw == 2)
					{
						presortMeasurementsResult[nrRect][0]->setText(QString("%1").arg(avrage[nrRect]));
						if (avrage[nrRect] > avrMin[nrRect])
						{
							presortMeasurementsResult[nrRect][0]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						}
						else
							presortMeasurementsResult[nrRect][0]->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));
					}
					if (avrage[nrRect] < avrMin[nrRect])
					{
						measureObject->isGoodPresort[0][nrRect] = 0;
						isGood[0] = 0; //int
					}

					if (draw == 2)
					{
						presortMeasurementsResult[nrRect][1]->setText(QString("%1").arg(stDev[nrRect]));
						presortMeasurementsResult[nrRect][2]->setText(QString("%1").arg(blobThresholdA0[nrRect]));
						presortMeasurementsResult[nrRect][6]->setText(QString("%1").arg(blobThresholdA1[nrRect]));
						if (stDev[nrRect] < stdMax[nrRect])
						{
							presortMeasurementsResult[nrRect][1]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						}
						else
							presortMeasurementsResult[nrRect][1]->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));

						presortMeasurementsResult[nrRect][3]->setText(QString("%1").arg(maxAreaA0[nrRect]));
						if (maxAreaA0[nrRect] < blobAreaA0[nrRect])
						{
							presortMeasurementsResult[nrRect][3]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						}
						else
							presortMeasurementsResult[nrRect][3]->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));

						presortMeasurementsResult[nrRect][4]->setText(QString("%1").arg(maxWidthA0[nrRect]));
						if (maxWidthA0[nrRect] < blobWidthA0[nrRect])
						{
							presortMeasurementsResult[nrRect][4]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						}
						else
							presortMeasurementsResult[nrRect][4]->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));

						presortMeasurementsResult[nrRect][5]->setText(QString("%1").arg(maxHeightA0[nrRect]));
						if (maxHeightA0[nrRect] < blobHeightA0[nrRect])
						{
							presortMeasurementsResult[nrRect][5]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						}
						else
							presortMeasurementsResult[nrRect][5]->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));

						presortMeasurementsResult[nrRect][9]->setText(QString("%1").arg(maxHeightA1[nrRect]));
						if (maxHeightA1[nrRect] < blobHeightA1[nrRect])
						{
							presortMeasurementsResult[nrRect][9]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						}
						else
							presortMeasurementsResult[nrRect][9]->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));

						presortMeasurementsResult[nrRect][8]->setText(QString("%1").arg(maxWidthA1[nrRect]));
						if (maxWidthA1[nrRect] < blobWidthA1[nrRect])
						{
							presortMeasurementsResult[nrRect][8]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						}
						else
							presortMeasurementsResult[nrRect][8]->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));

						presortMeasurementsResult[nrRect][7]->setText(QString("%1").arg(maxAreaA1[nrRect]));
						if (maxAreaA1[nrRect] < blobAreaA1[nrRect])
						{
							presortMeasurementsResult[nrRect][7]->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						}
						else
							presortMeasurementsResult[nrRect][7]->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));


						ui2.labelLenght->setText(QString("%1 [mm]").arg(lenght));
						if (lenght < (measureObject->setLenght / 3 * 2))
						{
							ui2.labelLenght->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));
						}
						else
							ui2.labelLenght->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
						

					}

					if (stDev[nrRect] > stdMax[nrRect])
					{
						measureObject->isGoodPresort[1][nrRect] = 0;
						isGood[1] = 0; //int
					}

					if (nrContoursA0[nrRect] > 0)
					{
						measureObject->isGoodPresort[2][nrRect] = 0;
						isGood[2] = 0; //int
					}

					if (nrContoursA1[nrRect] > 0)
					{
						measureObject->isGoodPresort[3][nrRect] = 0;
						isGood[3] = 0; //int
					}

					if (lenght < (measureObject->setLenght/3*2))
					{
						isGood[4] = 0; //int
					}
					measureObject->presortLenght = lenght;

				}


				//measureObject->presortHistory[0] = image;


				//processedImage2[nrRect]
				measureObject->isGoodPresortLenght = 0;
				measureObject->isGoodPresortBlobs = 0;

				int tmpGood = 0;

				if ((isGood[0] == 1) && (isGood[1] == 1) && (isGood[2] == 1) && (isGood[3] == 1) && (isGood[4] == 1))
				{
					measureObject->isGoodPresortLenght = 1;
					measureObject->isGoodPresortBlobs = 1;
					tmpGood = 1;
				}
				if ((isGood[0] == 0) || (isGood[1] == 0) || (isGood[2] == 0)  || (isGood[3] == 0))
				{
					measureObject->isGoodPresortBlobs = 0;
				}
				else
					measureObject->isGoodPresortBlobs = 1;
				if ((isGood[4] == 0))
				{
					measureObject->isGoodPresortLenght = 0;
				}
				else
					measureObject->isGoodPresortLenght = 1;

				if (draw == 0)
				{
					measureObject->currPresortDowel++;
					if (measureObject->currPresortDowel > 19)
						measureObject->currPresortDowel = 0;
					image->buffer->copyTo(*measureObject->presortHistory[measureObject->currPresortDowel].buffer);
					measureObject->presortIsGoodHistory[measureObject->currPresortDowel] = tmpGood;
				}

				Rect drawRect;
				drawRect.x = 0;
				drawRect.y = 0;
				drawRect.width = drawImage.cols;
				drawRect.height = drawImage.rows;

			
				if((measureObject->isGoodPresortLenght == 0) || (measureObject->isGoodPresortBlobs == 0))
				{
					measureObject->isGoodPresortAll = 0;

			
				
					
					drawImage.copyTo(drawImagePresortBad);
					rectangle(drawImagePresortBad, drawRect, Scalar(255, 0, 0), 4);
					updateImagePresortBad = 1;

					presortingCameraCounterBad++;
				}
			
				
				drawImage.copyTo(drawImagePresortAll);
				
				//rectangle(drawImagePresortAll, drawRect, Scalar(255, 0, 0), 4);
				//else
				//	rectangle(drawImagePresortAll, drawRect, Scalar(0, 255, 0), 4);
				updateImagePresortAll = 1;

				int posOld = measureObject->centerPrevImagePresort;
				int curPos = 0;

				
			
				int posNwe = center;

				/*if (posNwe < posOld)//{
				{
					for(int i = 0; i< )
					{
						for(int )
					}

				}*/

				measureObject->presortDowelSpeed = (((posNwe - posOld) * measureObject->korFactorPresort) / 0.0066)/1000;
				if (measureObject->presortDowelSpeed > 5)
 					int bla = 1000;
				if (measureObject->presortDowelSpeed < 0)
					int bla = 1000;
				processingTimer[2]->SetStop();
				measureObject->processingTimePresort = processingTimer[2]->ElapsedTime();
 				emit(measurePieceSignal(1, 0));


			}

			measureObject->centerPrevImagePresort = center;
			/*positionCounter++;
			if (positionCounter > 2)
				positionCounter = 0;*/

		}//konec zanke 
	}
	if (draw == 2)//risemo na zaslon pri izrisu iz presort dialoga
	{
		ui2.labelLenght->setText(QString("%1 mm").arg(lenght));

	
			QImage qimgOriginal((uchar*)drawImage.data, drawImage.cols, drawImage.rows, drawImage.step, QImage::Format_RGB888);
			QPixmap pixmap = QPixmap::fromImage(qimgOriginal);
			scenePresortDialog->addPixmap(pixmap);


			scenePresortDialog->addRect(detectleft, QPen(Qt::red, 1), QBrush(Qt::NoBrush));
			scenePresortDialog->addRect(detectRight, QPen(Qt::blue, 1), QBrush(Qt::NoBrush));


			scenePresortDialog->addItem(CPointFloat(lastLeft, detectleft.center().y()).DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
			scenePresortDialog->addItem(CPointFloat(lastRight, detectleft.center().y()).DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
		
	}


	

	processingTimer[1]->SetStop();
	processingTimer[1]->ElapsedTime();
	return 0;
}

int imageProcessing::CheckDefects(int nrCam, int piece,int draw)
{
	return 0;
}

int imageProcessing::ProcessCameraForBlobs(int nrCam, int index, int draw)
{
	int width = cam[nrCam]->image[index].buffer[0].rows;
	int height = cam[nrCam]->image[index].buffer[0].cols;
	int dowelOn = 0;
	QRect detectRect;
	QRect detectleft;
	QRect detectRight;
	CMemoryBuffer image = cam[nrCam]->image[index];
	cam[nrCam]->image[index].buffer[0].copyTo(image.buffer[0]);
	//image.buffer.Copy
	vector <int> transitionsLeft;
	vector <int> transitionsRight;
	int lastLeft = 0;
	int lastRight = 0;
	if (width > 400)
		int bla = 100;

	detectRect.setCoords(38, 64, 364, 68);
	detectleft = detectRect;
	detectleft.setWidth(detectRect.width() / 2);
	detectRight = detectRect;
	detectRight.setX(detectRect.x() + detectRect.width() / 2);
	detectRight.setWidth(detectRect.width() / 2);
	
	if (draw)
	{
		scene->addRect(detectleft, QPen(Qt::red, 1), QBrush(Qt::NoBrush));
		scene->addRect(detectRight, QPen(Qt::blue, 1), QBrush(Qt::NoBrush));
	}

	//cam[nrCam]->image[index].rect = detectRect;
	//cam[nrCam]->image[index].CreateIntensity();
	//cam[nrCam]->image[index].VerticalIntensity(2);
	//cam[nrCam]->image[index].rect = detectleft;
	image.rect = detectRect;
	image.CreateIntensity();
	image.VerticalIntensity(2);
	image.rect = detectleft;

	image.DetectTransitionsWhiteToBlack(230,230);


		if (image.detectedPointsLightToDark.size() > 0)
		{
			for (int i = 0; i < image.detectedPointsLightToDark.size(); i++)
			{
				if (draw)
				{
					scene->addItem(image.detectedPointsLightToDark[i].DrawDot(QPen(Qt::red), QBrush(Qt::red), 3));
				}
				//transitionsLeft.push_back(cam[nrCam]->image[index].detectedPointsLightToDark[i].x);
				lastLeft = image.detectedPointsLightToDark[i].x;

			}
		}

	image.rect = detectRight;
	image.DetectTransitionsWhiteToBlackBackWard(230, 230);

	if (image.detectedPointsLightToDark.size() > 0)
	{
		for (int i = 0; i < image.detectedPointsLightToDark.size(); i++)
		{
			if (draw)
			{
				scene->addItem(image.detectedPointsLightToDark[i].DrawDot(QPen(Qt::blue), QBrush(Qt::blue), 3));
			}
			//transitionsRight.push_back(cam[nrCam]->image[index].detectedPointsLightToDark[i].x);
			lastRight = image.detectedPointsLightToDark[i].x;
		}
	}

	QRect dowelRectMiddle;
	QRect dowelRectTop;
	QRect dowelRectBottom;


	int dowelHeight = 20;
	int left;
	int right;
	int pixWidth;
	int xlCenter = 5;
	int xrCenter = -5;
	int xlWinUp = 5;
	int xrWinUp = -5;
	int xlWinDown = 5;
	int xrWinDown = -5;
	int yCenter = 2;
	int yWinUp = 0;
	int yWinDown = 6;
	int yOffsetTopBottom =35;

	if (lastLeft > 0) 
	{

		cam[nrCam]->image[index].buffer[0].copyTo(images[nrSavedImages[1]]->buffer[0]);
		nrSavedImages[1]++;
		if (nrSavedImages[1] >= 30)
			nrSavedImages[1] = 0;
	}
	if (lastRight > 0)
	{
		int bla = 100;
	}
	int center = 0;
	int centerImage = detectRect.center().x();
	if ((lastLeft > 0)|| ((lastRight < 350) && (lastRight > 0)))//kadar je vsaj eden moznik v obmocju
	{

		center = (lastRight - lastLeft) / 2 + lastLeft;
		if ((lastLeft > 120) && (lastRight < 380))
		{
			if ((center > centerImage - 25) && (center < centerImage + 25))
			{

				//cam[nrCam]->image[index].buffer[0].copyTo(images[0]->buffer[0]);
				ProcessingCamera3(&image, 0);
				//images[0]->buffer
				//cam[nrCam]->image[index].Copy(*images[0]);

				//cam[nrCam]->image[index].SaveBuffer(QString("C:\\images\\izhodnaCAM\\"), QString("%1.bmp").arg(nrSavedImages[1]));
				//nrSavedImages[1]++;
				//if (nrSavedImages[1] > 100)
				//	nrSavedImages[1] = 0;
			}
		}
		/*left =// transitionsLeft[transitionsLeft.size() - 1];
		right =// transitionsRight[transitionsRight.size() - 1];

		pixWidth = right - left;
		dowelRectMiddle.setTopLeft(QPoint(left + xlCenter, detectRect.center().y() - dowelHeight / 2 + yCenter));
		dowelRectMiddle.setBottomRight(QPoint(right + xrCenter, detectRect.center().y() + dowelHeight / 2 + yCenter));

		dowelRectTop.setTopLeft(QPoint(left + xlWinUp, detectRect.center().y() - yOffsetTopBottom - dowelHeight / 2 + yWinUp));
		dowelRectTop.setBottomRight(QPoint(right + xrWinUp, detectRect.center().y() - yOffsetTopBottom + dowelHeight / 2 + yWinUp));

		dowelRectBottom.setTopLeft(QPoint(left + xlWinDown, detectRect.center().y() + yOffsetTopBottom - dowelHeight / 2 + yWinDown));
		dowelRectBottom.setBottomRight(QPoint(right + xrWinDown, detectRect.center().y() + yOffsetTopBottom + dowelHeight / 2 + yWinDown));

		if (draw)
		{
			scene->addRect(dowelRectMiddle, QPen(Qt::green, 1), QBrush(Qt::NoBrush));
			scene->addRect(dowelRectTop, QPen(Qt::red, 1), QBrush(Qt::NoBrush));
			scene->addRect(dowelRectBottom, QPen(Qt::blue, 1), QBrush(Qt::NoBrush));
		}

		std::vector <cv::Point> centerDowelEndPoints;
		Rect cropRect;
		//centerDowelEndPoints.push_back(Point(dowelRectMiddle.topLeft().x(), dowelRectMiddle.topLeft().y()));
		//centerDowelEndPoints.push_back(Point(dowelRectMiddle.topRight().x(), dowelRectMiddle.topRight().y()));
		//centerDowelEndPoints.push_back(Point(dowelRectMiddle.bottomRight().x(), dowelRectMiddle.bottomRight().y()));
		//centerDowelEndPoints.push_back(Point(dowelRectMiddle.bottomLeft().x(), dowelRectMiddle.bottomLeft().y()));
		cropRect.x = dowelRectMiddle.x();
		cropRect.y = dowelRectMiddle.y();
		cropRect.width = dowelRectMiddle.width();
		cropRect.height = dowelRectMiddle.height();
		int offsetX, offsetY;

		offsetX = cropRect.x;
		offsetY = cropRect.y;
		Mat cropMat ;
		Mat originalImage;
		cam[1]->image[0].buffer[0](cropRect).copyTo(originalImage);
		//imshow("", originalImage);
		Mat mask = cv::Mat::zeros(cropRect.height, cropRect.width, CV_8U);
		//Mat ney;
		cvtColor(originalImage, originalImage, COLOR_BGR2GRAY);
		//cvtColor(originalImage, ney, COLOR_BGR2GRAY);
	

		int conus = dowelHeight / 2;
		 conus = conus / 2;
		
		QPolygon middlePolygon;
		middlePolygon.append(QPoint(dowelRectMiddle.topLeft().x(), dowelRectMiddle.topLeft().y() +conus));
		middlePolygon.append(QPoint(dowelRectMiddle.topLeft().x() + conus, dowelRectMiddle.topLeft().y()));
		middlePolygon.append(QPoint(dowelRectMiddle.topRight().x() - conus, dowelRectMiddle.topRight().y()));
		middlePolygon.append(QPoint(dowelRectMiddle.topRight().x(), dowelRectMiddle.topRight().y() + conus));
		middlePolygon.append(QPoint(dowelRectMiddle.bottomRight().x(), dowelRectMiddle.bottomRight().y() - conus));
		middlePolygon.append(QPoint(dowelRectMiddle.bottomRight().x() - conus, dowelRectMiddle.bottomRight().y() ));

		middlePolygon.append(QPoint(dowelRectMiddle.bottomLeft().x()+conus, dowelRectMiddle.bottomLeft().y() ));
		middlePolygon.append(QPoint(dowelRectMiddle.bottomLeft().x(), dowelRectMiddle.bottomLeft().y()-conus));

		if (draw)
		{
			scene->addPolygon(middlePolygon, QPen(Qt::yellow), QBrush(Qt::NoBrush));
		}

		threshold(originalImage, originalImage, 100, 255, THRESH_BINARY_INV);
		originalImage.copyTo(images[2]->buffer[0]);
		std::vector<std::vector<cv::Point> > contours;
		
		cv::findContours(originalImage, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
		cv::Scalar colors[3];
		colors[0] = cv::Scalar(255, 0, 0);
		colors[1] = cv::Scalar(0, 255, 0);
		colors[2] = cv::Scalar(0, 0, 255);
		cvtColor(originalImage, originalImage, COLOR_GRAY2BGR);
		for (size_t idx = 0; idx < contours.size(); idx++) {
			cv::drawContours(originalImage, contours, idx, colors[idx % 3]);
		}
		originalImage.copyTo(images[3]->buffer[0]);
		*/
	}


	return 0;
}

void imageProcessing::OnSetClassifierTrainImagesPath()
{
	QFileDialog userPathDialog(this);
	userPathDialog.setFileMode(QFileDialog::Directory);
	userPathDialog.setViewMode(QFileDialog::Detail);

	Classifier &currentClassifier = types[currentStation][currentType]->classifier;

	//Pridobimo pot s strani uporabnika
	//Prej je potrebno izbrati za katerega izmed razvščevalnikov nastavljamo pot
	QStringList dirNames;
	if (userPathDialog.exec())
	{
		dirNames = userPathDialog.selectedFiles();
		//Od tu naprej se sedaj lahko pot uprabi za različne namene
		currentClassifier.SetImagesPath(dirNames[0]);
		currentClassifier.LearnClassifier();
	}
}

void imageProcessing::OnSetClassifierTestImagesPath()
{
	QFileDialog userPathDialog(this);
	userPathDialog.setFileMode(QFileDialog::Directory);
	userPathDialog.setViewMode(QFileDialog::Detail);

	Classifier &currentClassifier = types[currentStation][currentType]->classifier;

	//Pridobimo pot s strani uporabnika
	//Prej je potrebno izbrati za katerega izmed razvščevalnikov nastavljamo pot
	QStringList dirNames;
	if (userPathDialog.exec())
	{
		dirNames = userPathDialog.selectedFiles();
		//Od tu naprej se sedaj lahko pot uprabi za različne namene
		currentClassifier.SetImagesPath(dirNames[0], "test");
		currentClassifier.TestClassifier();
	}
}









