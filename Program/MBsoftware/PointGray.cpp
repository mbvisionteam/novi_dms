#include "stdafx.h"
#include "PointGray.h"


#include "FlyCapture2Platform.h"
#include "FlyCapture2Defs.h"

#include "FlyCapture2.h"
#include "Camera.h"
#include "CameraBase.h"
#include <iostream>
#include <sstream>
#include "PointGray.h"
#include <QtConcurrent>

using namespace FlyCapture2;
using namespace std;



bool grabLoop = true;



CPointGray::CPointGray()
{
	cam = NULL;



	cam = new Camera;

}

CPointGray::~CPointGray()
{
	FlyCapture2::Error error;

	TriggerMode triggerMode;
	grabLoop = false;
	//TerminateThread(ThreadGrabImage, 0);

	if (cam != NULL)
	{
		// Turn trigger mode off.
		triggerMode.onOff = false;
		error = cam->SetTriggerMode(&triggerMode);
		if (error != PGRERROR_OK)
		{
			//PrintError(error);
			return;
		}
		Sleep(100);

		// Stop capturing images
		error = cam->StopCapture();
		if (error != PGRERROR_OK)
		{
			//PrintError(error);
			return;
		}
		Sleep(100);

		error = cam->Disconnect();
		if (error != PGRERROR_OK)
		{
			//PrintError(error);
			return;
		}
	}
	//delete cam;

}







int CPointGray::Init(int selectGrabber, QString cameraName, int width, int height, int depth, int offsetX, int offsetY)
{
	return 0;
}
int CPointGray::Init(QString serialNumber, QString VideoFormatS)
{
	Init();
	return 1;
}
int CPointGray::Init()
{



	bufferSize = this->depth * this->width * this->height;
	vendorName = "PointGray";
	InterfaceType ifType;
	unsigned int serialNumberInt = 0;
	//serialNumberInt = atoi((CStringA)serialNumber);

	serialNumberInt = (unsigned int)serialNumber.toInt();
	error = busMgr.GetCameraFromSerialNumber(serialNumberInt, &guid);
	if (error != PGRERROR_OK)
	{
		return 0;

	}



	error = busMgr.GetInterfaceTypeFromGuid(&guid, &ifType);
	if (error != PGRERROR_OK)
	{
		return 0;
	}
	if (cam != NULL)
	{
		delete cam;
		cam = NULL;
	}

	if (ifType == INTERFACE_GIGE)
	{
		//cam = new GigECamera;
	}
	else
	{
		cam = new Camera;
	}


	//Declare a Property struct. 





	error = cam->Connect(&guid);
	if (error != PGRERROR_OK)
	{

		return -1;
	}


	//Nastavitev exposure in gain mora biti za cam->Connect, sicer nima u�inka
	//SetExposureAbsolute(defaultExposure);
	//SetGainAbsolute(defaultGain);

	CameraInfo info;
	//set image RESOLUTION
	cam->GetCameraInfo(&info);
	vendorName = info.vendorName;
	cameraName = info.modelName;
	bool supported;
	const Mode k_fmt7Mode = MODE_0;
	const PixelFormat	k_fmt7PixFmt = PIXEL_FORMAT_MONO8;
	fmt7Info.mode = k_fmt7Mode;
	error = cam->GetFormat7Info(&fmt7Info, &supported);
	QString format;
	if (error != PGRERROR_OK)
	{
		//PrintError(error);
		return -1;
	}
	//preverim width and height ter nastavimo format
	if (width > fmt7Info.maxWidth || height > fmt7Info.maxHeight)
		return -1;
	if (depth == 1)
	{
		const PixelFormat	k_fmt7PixFmt = PIXEL_FORMAT_MONO8;
		format = QString("MONO8");

		formatType = CV_8UC1;
		conversionCode = COLOR_GRAY2RGB;
	}
	if (depth == 3)
	{
		const PixelFormat	k_fmt7PixFmt = PIXEL_FORMAT_BGR;
		format = QString("BGR");
		conversionCode = COLOR_BGR2RGB;
	}
	//preverimo format slike
	if ((k_fmt7PixFmt & fmt7Info.pixelFormatBitField) == 0)
	{
		// Pixel format not supported!
		cout << "Pixel format is not supported" << endl;
		return -1;
	}

	this->videoFormat = QString("%1 x %2 %3").arg(width).arg(height).arg(format);
	fmt7ImageSettings.mode = k_fmt7Mode;
	fmt7ImageSettings.offsetX = offsetX;
	fmt7ImageSettings.offsetY = offsetY;
	fmt7ImageSettings.width = width;
	fmt7ImageSettings.height = height;
	fmt7ImageSettings.pixelFormat = k_fmt7PixFmt;


	Property frmRate;
	frmRate.type = FRAME_RATE;
	frmRate.absValue = FPS;
	error = cam->SetProperty(&frmRate);
	if (error != PGRERROR_OK)
	{
		//PrintError(error);
		return -1;
	}




	bool valid;


	// Validate the settings to make sure that they are valid
	error = cam->ValidateFormat7Settings(
		&fmt7ImageSettings,
		&valid,
		&fmt7PacketInfo);
	if (error != PGRERROR_OK)
	{
		//	PrintError(error);
		return -1;
	}
	error = cam->SetFormat7Configuration(
		&fmt7ImageSettings,
		fmt7PacketInfo.recommendedBytesPerPacket);
	if (error != PGRERROR_OK)
	{
		//PrintError(error);
		return -1;
	}


	if (trigger)
	{
		grabLoop = true;
		triggerMode.mode = 0;
		triggerMode.source = 0;
		triggerMode.parameter = 0;
		triggerMode.onOff = true;
		triggerMode.polarity = 0;
		//cam->SetTriggerMode(&triggerMode);

	}
	else
	{
		// Set camera to trigger mode 0
		triggerMode.onOff = true;
		triggerMode.mode = 0;
		triggerMode.parameter = 0;


		// A source of 7 means software trigger
		triggerMode.source = 7;

	}
	error = cam->SetTriggerMode(&triggerMode);
	if (error != PGRERROR_OK)
	{

		return -1;
	}
	const unsigned int k_softwareTrigger = 0x62C;

	unsigned int regVal = 0;



	// Get the camera configuration
	FC2Config config;

	// Set the grab timeout to 5 seconds
	config.grabTimeout = 1.0/FPS *1000;

	// Set the camera configuration
	error = cam->SetConfiguration(&config);
	if (error != PGRERROR_OK)
	{

		return -1;
	}

	GetGainAbsolute();
	GetExposureAbsolute();

	return 1;

}

int CPointGray::Start(double FPS, int trigger)
{
	return Start();
}

int CPointGray::Start()
{
	// Camera is ready, start capturing images
	error = cam->StartCapture();
	if (error != PGRERROR_OK)
	{

		return -1;
	}
	isLive = 1;
	if (trigger == false)
		GrabImage();

	//frameReadyFreq.SetStart();

	cameraOpened++;
	int ImageSize;
	InitImages(width, height, depth);
	ImageSize = image.size();
	//if (trigger)
		
	//QFuture<void> future = QtConcurrent::run(this, &CPointGray::DoGrabLoop);

	QtConcurrent::run(this, &CPointGray::DoGrabLoop);
	return 1;
}

bool CPointGray::SetGainAbsolute(double value)
{


	prop.type = GAIN;
	//Ensure the property is on. 
	prop.onOff = true;
	//Ensure auto-adjust mode is off. 
	prop.autoManualMode = false;
	//Ensure the property is set up to use absolute value control. 
	prop.absControl = false;
	//Set the absolute value of shutter to 20 ms. 
	//prop.absValue = value;
	prop.valueA = value;
	//Set the property.
	cam->SetProperty(&prop);
	currentGain = value;


	return true;
}


double CPointGray::GetExposureAbsolute()
{
	PropertyInfo propInfo;
	propInfo.type = SHUTTER;
	cam->GetPropertyInfo(&propInfo);
	maxExpo = propInfo.absMax;
	minExpo = propInfo.absMin;

	return 0;
}


int CPointGray::GetGainAbsolute()
{

	PropertyInfo propInfo;
	propInfo.type = GAIN;
	cam->GetPropertyInfo(&propInfo);
	maxGain = propInfo.max;
	minGain = propInfo.min;

	/*Property gain;
	gain.type = GAIN;
	error = cam->GetProperty(&gain);
	maxGain = gain.valueA;
	minGain = gain.valueB;*/
	return 0;
}



bool CPointGray::SaveReferenceSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;
	filePath = settingsPath;


	QSettings settings(filePath, QSettings::IniFormat);

	settings.beginGroup("Settings");
	const QStringList childKeys = settings.childKeys();
	if (childKeys.size() == 0)
	{
		settings.setValue("GAIN0", 0);
		settings.setValue("EXPO0", 0.0);
		settings.setValue("OFFSETX0", 0);
		settings.setValue("OFFSETY0", 0);
		exposure.push_back(0);
		gain.push_back(0);
		savedOffsetX.push_back(0);
		savedOffsetY.push_back(0);
	}
	else
	{

		QString tmp;
		for (int index = 0; index < num_images; index++)
		{
			tmp = QString("EXPO%1").arg(index);
			settings.setValue(tmp, currentExposure);
			tmp = QString("GAIN%1").arg(index);
			settings.setValue(tmp, currentGain);
			tmp = QString("OFFSETX%1").arg(index);
			settings.setValue(tmp, offsetX);
			tmp = QString("OFFSETY%1").arg(index);
			settings.setValue(tmp, offsetY);
		}


	}
	settings.endGroup();

	return 0;


}

bool CPointGray::SaveReferenceSettings(int index)
{
	QString filePath;
	QStringList values;
	QVariant var;
	filePath = settingsPath;


	QSettings settings(filePath, QSettings::IniFormat);

	settings.beginGroup("Settings");
	const QStringList childKeys = settings.childKeys();
	if (childKeys.size() == 0)
	{
		settings.setValue("GAIN0", 0);
		settings.setValue("EXPO0", 0.0);
		settings.setValue("OFFSETX0", 0);
		settings.setValue("OFFSETY0", 0);
		exposure.push_back(0);
		gain.push_back(0);
		savedOffsetX.push_back(0);
		savedOffsetY.push_back(0);
	}
	else
	{

		QString tmp;

		tmp = QString("EXPO%1").arg(index);
		settings.setValue(tmp, currentExposure);
		tmp = QString("GAIN%1").arg(index);
		settings.setValue(tmp, currentGain);
		tmp = QString("OFFSETX%1").arg(index);
		settings.setValue(tmp, offsetX);
		tmp = QString("OFFSETY%1").arg(index);
		settings.setValue(tmp, offsetY);


	}
	settings.endGroup();

	return 0;
}

bool CPointGray::LoadReferenceSettings(int index)
{
	QString filePath;
	QStringList values;
	QVariant var;



	//filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/ImagingSource_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	filePath = settingsPath;



	QSettings settings(filePath, QSettings::IniFormat);

	settings.beginGroup("Settings");
	const QStringList childKeys = settings.childKeys();
	if (childKeys.size() == 0)
	{
		settings.setValue("GAIN0", 0);
		settings.setValue("EXPO0", 0.0);
		settings.setValue("OFFSETX0", 0);
		settings.setValue("OFFSETY0", 0);
		exposure.push_back(0);
		gain.push_back(0);
		savedOffsetX.push_back(0);
		savedOffsetY.push_back(0);

	}
	else
	{

		QString tmp;

		tmp = QString("EXPO%1").arg(index);
		exposure.push_back(settings.value(tmp).toFloat());
		tmp = QString("GAIN%1").arg(index);
		gain.push_back(settings.value(tmp).toInt());
		tmp = QString("OFFSETX%1").arg(index);
		savedOffsetX.push_back(settings.value(tmp).toInt());
		tmp = QString("OFFSETY%1").arg(index);
		savedOffsetY.push_back(settings.value(tmp).toInt());

	}
	settings.endGroup();

	return 0;

}

bool CPointGray::LoadReferenceSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	//filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/ImagingSource_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	filePath = settingsPath;



	QSettings settings(filePath, QSettings::IniFormat);

	settings.beginGroup("Settings");
	const QStringList childKeys = settings.childKeys();
	if (childKeys.size() == 0)
	{
		settings.setValue("GAIN0", 0);
		settings.setValue("EXPO0", 0.0);
		settings.setValue("OFFSETX0", 0);
		settings.setValue("OFFSETY0", 0);
		exposure.push_back(0);
		gain.push_back(0);
		savedOffsetX.push_back(0);
		savedOffsetY.push_back(0);
	}
	else
	{
		for (int i = 0; i < num_images; i++)
		{
			QString tmp;

			tmp = QString("EXPO%1").arg(i);
			exposure.push_back(settings.value(tmp).toFloat());
			tmp = QString("GAIN%1").arg(i);
			gain.push_back(settings.value(tmp).toInt());
			tmp = QString("OFFSETX%1").arg(i);
			savedOffsetX.push_back(settings.value(tmp).toInt());
			tmp = QString("OFFSETY%1").arg(i);
			savedOffsetY.push_back(settings.value(tmp).toInt());
		}
	}
	settings.endGroup();

	return 0;
}




int CPointGray::DoGrabLoop()
{
	const unsigned int k_softwareTrigger = 0x62C;
	const unsigned int k_fireVal = 0x80000000;
	// FlyCapture2::Error error;
	// unsigned char* buff;
	Image image3;
	int ImageSize;
	Mat mat(height, width, formatType);

	while (grabLoop)
	{
		if (enableLive)
		{
			error = cam->WriteRegister(k_softwareTrigger, k_fireVal);//posljemo  softwarski trigger
		}


		error = cam->RetrieveBuffer(&image3);//kadar pride slika
		if (error == PGRERROR_OK)
		{
			if (enableLive == 1)
			{
				imageIndex = 0;
			}
			else if(grabImage )
			{
				grabImage = 0;

			}
			const uint8_t *pImageBuffer = (uint8_t *)image3.GetData();

		

			std::copy(&pImageBuffer[0], &pImageBuffer[0] + bufferSize, mat.data);



			//Za�etek Martinove kode za zrcaljenje: dobro dela v GrabImage, v DoGrabLoop pa se ob�asno prikazujejo napake


		
			if ((this->rotadedHorizontal == false) && (this->rotatedVertical == false))
			{
				
			}
			else if ((this->rotadedHorizontal == true) && (this->rotatedVertical == false))
			{
				flip(mat, mat, 1);
			}
			else if ((this->rotadedHorizontal == false) && (this->rotatedVertical == true))
			{
				flip(mat, mat, 0);
			}
			else if ((this->rotadedHorizontal == true) && (this->rotatedVertical == true))
			{
				flip(mat, mat, -1);
			}

			cvtColor(mat, image[imageIndex].buffer[0], conversionCode);
			//std::copy(&byteData[0], &byteData[0] + bufferSize, &image[imageIndex].buffer->data[0]);
			//image[imageIndex].buffer->put

			imageReady[imageIndex] = 1;
			/*if (!zrcaliOkoliVodoravne && !zrcaliOkoliNavpicne)	//brez zrcaljenja
			{
				std::copy(&pImageBuffer[0], &pImageBuffer[0] + bufferSize, &image[imageIndex].buffer->data[0]);
			}
			else
			{
				for (vrstica = 0; vrstica < this->height; vrstica++)
				{
					if (!zrcaliOkoliVodoravne && zrcaliOkoliNavpicne)	//z zrcaljenjem okoli navpi�ne osi
						std::reverse_copy(&pImageBuffer[0] + vrstica * bytesPerLine, &pImageBuffer[0] + (vrstica + 1)*bytesPerLine - 1, &image[imageIndex].buffer->data[0] + vrstica * bytesPerLine);

					if (zrcaliOkoliVodoravne && !zrcaliOkoliNavpicne)  //z zrcaljenjem okoli vodoravne osi
						std::copy(&pImageBuffer[0] + vrstica * bytesPerLine, &pImageBuffer[0] + (vrstica + 1)*bytesPerLine - 1, &image[imageIndex].buffer->data[0] + (this->height - (vrstica + 1))*bytesPerLine);

					if (zrcaliOkoliVodoravne && zrcaliOkoliNavpicne) //z zrcaljenjem okoli vodoravne in navpi�ne osi																																
				}*/
			//Konec Martinove kode za zrcaljenje
			OnFrameReady();
			frameCounter++;
			imageReady[imageIndex] = 1;
			imageIndex++;
			
			if (imageIndex >= num_images)
				imageIndex = 0;
		

			if (frameReadyFreq.timeCounter >= FPS)
			{
				frameReadyFreq.SetStop();

				frameReadyFreq.CalcFrequency(frameReadyFreq.timeCounter);
				frameReadyFreq.timeCounter = 0;
				frameReadyFreq.SetStart();
			}
			frameReadyFreq.timeCounter++;

			testCounter++;
			testCounter = testCounter & 0xffff;
		}
	}
	return 1;
}

bool CPointGray::SetExposureAbsolute(double value)
{
	//Declare a Property struct. 
	//Define the property to adjust. 


	PropertyInfo propInfo;
	propInfo.type = SHUTTER;
	cam->GetPropertyInfo(&propInfo);
	
	prop.type = SHUTTER;
	
	//Ensure the property is on. 
	prop.onOff = true;
	//Ensure auto-adjust mode is off. 
	prop.autoManualMode = false;
	//Ensure the property is set up to use absolute value control. 
	prop.absControl = true;
	//Set the absolute value of shutter to 20 ms. 

	prop.absValue = (value);

	//Set the property.
	cam->SetProperty(&prop);
	currentExposure = (value);
	return true;
}
void CPointGray::AutoGainOnce()
{

}


void CPointGray::AutoGainContinuous()
{

}


void CPointGray::AutoExposureOnce()
{

}


void CPointGray::AutoExposureContinuous()
{

}


void CPointGray::AutoWhiteBalance()
{

}


bool CPointGray::IsColorCamera()
{
	return 0;
}
int CPointGray::GrabImage()
{
	const unsigned int k_softwareTrigger = 0x62C;
	const unsigned int k_fireVal = 0x80000000;
	FlyCapture2::Error error;
	unsigned char* buff;

	error = cam->WriteRegister(k_softwareTrigger, k_fireVal);
	imageIndex = 0;

	grabImage = 1;
	

	return 0;
}

int CPointGray::GrabImage(int imageNr)
{



	const unsigned int k_softwareTrigger = 0x62C;
	const unsigned int k_fireVal = 0x80000000;
	FlyCapture2::Error error;
	unsigned char* buff;
	imageIndex = imageNr;
	error = cam->WriteRegister(k_softwareTrigger, k_fireVal);
	grabImage = 1;

	return 0;
}


void CPointGray::CloseGrabber()
{

}

bool CPointGray::OnSettingsImage()
{




	return 0;
}


bool CPointGray::IsDeviceValid()
{

	return 0;
}




bool CPointGray::EnableLive()
{

	
	// Set camera to trigger mode 0
	triggerMode.onOff = true;
	triggerMode.mode = 0;
	triggerMode.parameter = 0;


	// A source of 7 means software trigger
	triggerMode.source = 7;


	error = cam->SetTriggerMode(&triggerMode);
	if (error != PGRERROR_OK)
	{

		return false;
	}
	const unsigned int k_softwareTrigger = 0x62C;

	unsigned int regVal = 0;

	do
	{
		error = cam->ReadRegister(k_softwareTrigger, &regVal);
		if (error != PGRERROR_OK)
		{

			return false;
		}

	} while ((regVal >> 31) != 0);

	if (enableLive == 0)
	{
		//grabLoop = true;
		//QtConcurrent::run(this, &CPointGray::DoGrabLoop);
		

		enableLive = 1;
	}
	else
	{
		//grabLoop = false;
		enableLive = false;
		
	}
	return false;
}

bool CPointGray::DisableLive()
{
	if (trigger)
	{
		//grabLoop = true;
		liveImage = false;
	}
	triggerMode.onOff = true;
	triggerMode.mode = 0;
	triggerMode.parameter = 0;


	// A source of 7 means software trigger
	triggerMode.source = 0;


	error = cam->SetTriggerMode(&triggerMode);
	if (error != PGRERROR_OK)
	{

		return false;
	}
	const unsigned int k_softwareTrigger = 0x62C;

	unsigned int regVal = 0;

	do
	{
		error = cam->ReadRegister(k_softwareTrigger, &regVal);
		if (error != PGRERROR_OK)
		{

			return false;
		}

	} while ((regVal >> 31) != 0);


	//AfxBeginThread(ThreadGrabImage, this);
	//emit grabLoopSignal();
	enableLive = false;
	return false;
}

bool CPointGray::SetPartialOffsetX(int xOff)
{
	return false;
}
int	CPointGray::GetPartialOffsetX()
{
	return 0;
}
bool CPointGray::SetPartialOffsetY(int yOff)
{
	return false;
}
int	CPointGray::GetPartialOffsetY()
{
	return 0;
}

void CPointGray::OnFrameReady()
{
	emit showImageSignal(0);
	if( this->realTimeProcessing== 1)
	emit frameReadySignal(id, imageIndex);
}
