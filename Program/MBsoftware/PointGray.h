#pragma once

#include "Camera.h"
#include "FlyCapture2.h"
#include "FlyCapture2Platform.h"
#include "stdafx.h"
#include "FlyCapture2Defs.h"


using namespace FlyCapture2;

class CPointGray :public CCamera
{
	Q_OBJECT
public:
	CPointGray();
	~CPointGray();


public:
	CPointGray* g_view;
	FlyCapture2::Camera* cam;
	Image image2;
	TriggerMode triggerMode;

	PGRGuid guid;
	//Camera cam;
	BusManager busMgr;


	FlyCapture2::Error error;
	Property prop;
	
	Format7Info fmt7Info;
	Format7ImageSettings fmt7ImageSettings;
	Format7PacketInfo fmt7PacketInfo;
	bool liveImage = false;






public:
	virtual int Init(int selectGrabber, QString cameraName, int width, int height, int depth, int offsetX, int offsetY);
	virtual int Init(QString serialNumber, QString VideoFormatS);
	virtual int Init();
	virtual int Start(double FPS, int trigger);
	virtual int Start();

	virtual bool SetGainAbsolute(double value);
	virtual double GetExposureAbsolute();
	virtual int GetGainAbsolute();
	virtual void AutoGainOnce();
	virtual void AutoGainContinuous();
	virtual bool SetExposureAbsolute(double value);
	virtual void AutoExposureOnce();
	virtual void AutoExposureContinuous();
	virtual void AutoWhiteBalance();
	virtual bool IsColorCamera();
	virtual int GrabImage(int imageNr);
	virtual int GrabImage();
	virtual void CloseGrabber(void);
	virtual bool OnSettingsImage();
	virtual bool SaveReferenceSettings();
	virtual bool SaveReferenceSettings(int index);
	virtual bool LoadReferenceSettings();
	virtual bool LoadReferenceSettings(int index);
	virtual bool IsDeviceValid();

	virtual bool EnableLive();
	virtual bool DisableLive();
	virtual bool SetPartialOffsetX(int xOff);
	virtual int GetPartialOffsetX();
	virtual bool SetPartialOffsetY(int yOff);
	virtual int GetPartialOffsetY();
	//static UINT ThreadGrabImage(LPVOID pparam);
	void OnFrameReady();

public slots:
	int DoGrabLoop();


signals:
	void grabLoopSignal();


};


