﻿#pragma once

#include "Camera.h"
#include "MvCameraControl.h"

class Hikrobot :public CCamera
{
public:
	Hikrobot();
	~Hikrobot();

	static Hikrobot*	glob;
	int flipCode;

	/**
	* Preveri vse Hikrobot naprave z doloèenim komunikacijskim vmenikom, ki so na voljo
	* Za določanje željenga vmesnika se uporabla spremeljivka cameraType v razredu CCamera

	* @return: Vrne število naprav na voljo z doloèenim komunikacijskim vmesnikom
	*/

	virtual int Init();
	virtual int Start();
	virtual int StartGrabber();
	virtual int StopGrabber();
	virtual int GrabImage(int imageIndex);
	virtual bool SetGainAbsolute(double gain);
	virtual int GetGainAbsolute();
	virtual void AutoGainOnce();
	virtual void AutoGainContinuous();
	virtual bool SetExposureAbsolute(double exposureTime);
	virtual double GetExposureAbsolute();
	virtual void AutoExposureOnce();
	virtual void AutoExposureContinuous();
	virtual bool EnableLive();
	virtual bool DisableLive();

	virtual void SnapOne();


	int AvailableDevices();
	int ConnectDevice(QString serial);
	bool SaveReferenceSettings();
	bool SaveReferenceSettings(int index);
	bool LoadReferenceSettings(int index);
	bool LoadReferenceSettings();
	bool SetPartialOffsetX(int xOff);
	int GetPartialOffsetX();
	bool SetPartialOffsetY(int yOff);
	int GetPartialOffsetY();

protected:
	void* cameraHandle;
	MV_CC_DEVICE_INFO_LIST availableDevices;
	MV_FRAME_OUT_INFO_EX frameInfo;
private:
	static void __stdcall OnFrameReady(unsigned char * pData, MV_FRAME_OUT_INFO_EX* pFrameInfo, void* pUser);
	//void __stdcall OnFrameReady(unsigned char * pData, MV_FRAME_OUT_INFO_EX* pFrameInfo, void* pUser);
};

