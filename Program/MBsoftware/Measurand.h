﻿#pragma once

#include "stdafx.h"
#include <QObject>





class Measurand : public QObject  //razred ki ga uporabljam za prenos meritev med razredi imageProcessing in MBsoftware
{
	Q_OBJECT

public:
	Measurand(QObject *parent);
	Measurand();
	~Measurand();

	//vmesni podatki
	int currDowelVertical[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT][2];//shranjeni prehodi pred ureditvijo po pozicijah 
	float horizontalTransitionsReal[DMSHISTORYSIZE][MAX_MEASUREMENT][2];//prehodi od horizontalne kamere v mm
	int horizontalMeasurementsOn[DMSHISTORYSIZE][MAX_MEASUREMENT];//meritve ki jih upostevam za horizontalne meritve
	int horizontalTransitions[DMSHISTORYSIZE][MAX_MEASUREMENT][2];//prehodi od horizontalne kamere 
	int horizontalTransitionType[DMSHISTORYSIZE][MAX_MEASUREMENT][2];//tipi prehoda za horizontalno kamereo
	int horizontalData[DMSHISTORYSIZE][MAX_MEASUREMENT][128];//podatki o poziciji od zacetka in konca zdruzeni
	int horizontalDataTypes[DMSHISTORYSIZE][MAX_MEASUREMENT][128];//podatki o poziciji od zacetka in konca zdruzeni
	float horizontalDataZACreal[DMSHISTORYSIZE][MAX_MEASUREMENT];//podatki o poziciji od zacetka 
	float horizontalDataKONreal[DMSHISTORYSIZE][MAX_MEASUREMENT];//podatki o poziciji od konca
	int verticalDataZAC[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT][2]; //podatki o verticalnih prehodih od vseh kamer 
	int verticalDataKON[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT][2]; //podatki o verticalnih prehodih od vseh kamer 
	float  verticalDataZACReal[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT][2]; //podatki o verticalnih prehodih od vseh kamer 
	float verticalDataKONReal[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT][2]; //podatki o verticalnih prehodih od vseh kamer 
	float verticalDataZACDraw[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT][2]; //podatki o verticalnih prehodih od vseh kamer 
	float verticalDataKONDraw[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT][2]; //podatki o verticalnih prehodih od vseh kamer 
	float verticalDataReal[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT][2]; //podatki o verticalnih prehodih od vseh kamer  v mm
	float diameterDataZACReal[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT]; //izracunani premeri v mm s korekcijo
	float diameterDataKONReal[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT]; //izracunani premeri v mm s korekcijo
	int measurementsOnArea[DMSHISTORYSIZE][MAX_MEASUREMENT]; //zaradi prikaza stevilka oznaci v katerem obmocju je meritev



	//meritve kosa 
		//meritve
	int nrMeasurementsArea0[DMSHISTORYSIZE][NR_DMS_CAM]; //stevilo meritev na doloccenem delu moznika
	int nrMeasurementsArea1[DMSHISTORYSIZE][NR_DMS_CAM]; //stevilo meritev na doloccenem delu moznika
	int nrMeasurementsArea2[DMSHISTORYSIZE][NR_DMS_CAM]; //stevilo meritev na doloccenem delu moznika
	int nrMeasurementsArea3[DMSHISTORYSIZE][NR_DMS_CAM]; //stevilo meritev na doloccenem delu moznika
	int nrMeasurementsArea4[DMSHISTORYSIZE][NR_DMS_CAM]; //stevilo meritev na doloccenem delu moznika
	int nrMeasurementsNagative[DMSHISTORYSIZE]; //stevilo meritev na doloccenem delu moznika
	int nrBadMeasurementsDSKAll[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDSKMinus[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDISAll[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDISMinus[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDICAll[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDICMinus[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDIEAll[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDIEMinus[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDKEAll[DMSHISTORYSIZE][NR_DMS_CAM];
	int nrBadMeasurementsDKEMinus[DMSHISTORYSIZE][NR_DMS_CAM];
	float dowelSpeed[DMSHISTORYSIZE];
	float dowelLenght[DMSHISTORYSIZE];
	float mesurementElipse[DMSHISTORYSIZE];
	float measurementDSK[DMSHISTORYSIZE][NR_DMS_CAM]; //meriev premera
	float measurementDIS[DMSHISTORYSIZE][NR_DMS_CAM]; //meriev premera
	float measurementDIC[DMSHISTORYSIZE][NR_DMS_CAM]; //meriev premera
	float measurementDIE[DMSHISTORYSIZE][NR_DMS_CAM]; //meriev premera
	float measurementDKE[DMSHISTORYSIZE][NR_DMS_CAM]; //meriev premera
	QPointF diametersDSK[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];//vse premeri so shranjeni zaradi prikaza in filter ERROR
	QPointF diametersDIS[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];
	QPointF diametersDIC[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];
	QPointF diametersDIE[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];
	QPointF diametersDKE[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];
	int	  diameterDSKGood[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];//spremenljivke premerov ali so meritve v tolerancah
	int	  diameterDISGood[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];//spremenljivke premerov ali so meritve v tolerancah
	int	  diameterDICGood[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];//spremenljivke premerov ali so meritve v tolerancah
	int	  diameterDIEGood[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];//spremenljivke premerov ali so meritve v tolerancah
	int	  diameterDKEGood[DMSHISTORYSIZE][NR_DMS_CAM][MAX_MEASUREMENT];//spremenljivke premerov ali so meritve v tolerancah
	CLine lineKonus[DMSHISTORYSIZE][NR_DMS_CAM][4]; //linije konusa
	CLine lineMiddle[DMSHISTORYSIZE][NR_DMS_CAM][4]; //linije ki sekajo linije konusa
	float measurementAngleAn[DMSHISTORYSIZE][NR_DMS_CAM][4];//racunanje kota med konusom in ravno linijo
	float measurementDistanceDel[DMSHISTORYSIZE][NR_DMS_CAM][4]; //meritve DEL razdaljam med konusom in vrhom mozika
	float measurementKonusLenght[DMSHISTORYSIZE][NR_DMS_CAM][4];//dolzina konusa glede na presecisce priemic 
	float measurementRoughness[DMSHISTORYSIZE][NR_DMS_CAM];//hrapavost zgoraj in spodaj 
	int isGoodArray[DMSHISTORYSIZE][20];//parameter ki ga posredujem za izris moznika na glavnem oknu, da vidim na katerih obmocijih je slab
	float measurementAvrDiameter[DMSHISTORYSIZE];
	int measureAlarm[DMSHISTORYSIZE];
	float speedForPresort[10]; //za testno racunanje hitrosti za predsortiranje
	//INDEX [0]->konusUS
	//INDEX [1]->konusDS
	//INDEX [2]->konusUE
	//INDEX [3]->konusDE

	float dowelMiddleVertical[DMSHISTORYSIZE][NR_DMS_CAM]; //sredina verikalnih kamer

	int dowelGood[DMSHISTORYSIZE];//zgodovina ali je moznik dober
	int dowelStringSize[DMSHISTORYSIZE];
	int nrDowelMeasurementsZAC[DMSHISTORYSIZE];
	int nrDowelMeasurementsKON[DMSHISTORYSIZE];
	int nrDowelMeasurements[DMSHISTORYSIZE];//
	int dowelReady[DMSHISTORYSIZE];//0-15 dow
	int dowelNrXilinx[DMSHISTORYSIZE];
	float processingTime[DMSHISTORYSIZE];
	QByteArray dowelString[DMSHISTORYSIZE];
	QByteArray sendDowelString[DMSHISTORYSIZE];

	

	int currentPiece; //krozimo od 0 do 128 potem lahko pobrisemo zadnji podatek 



	//testna kamera
	Mat blowImage[DMSHISTORYSIZE][20]; //testne slike pihanja moznika 
	int blowImageCounter[DMSHISTORYSIZE];//test
	//int counterImageIndex[DMSHISTORYSIZE];
	int imageIndex[DMSHISTORYSIZE][20];
	int showInHistory[DMSHISTORYSIZE]; //kadar priklicem mozik iz zgodovine, da ne posljem rezulatov xilinx


	//testiranje bedarija
	CPointFloat centerOfG[DMSHISTORYSIZE][20];


	//alarmi
	int dowelAllowedMeasurements[DMSHISTORYSIZE][2];// min in max stevilo meritev za moznik z doloceno hitrostjo 
	QByteArray sendAlarmString[DMSHISTORYSIZE];
	int sendAlarm[DMSHISTORYSIZE];
	int alarmCounter[10];




	//obmocja merjenja
	float measureArea0[2];//konus zacetek
	float measureArea1[2];//zacetek ravnine
	float measureArea2[2]; //preostali del na ravnini moznika


	//sprememljivke ki jih bomo nastavljali iz dialogov
	float setElipsse; //max mera za hrapavost
	float setLenght; //nastavljena dolzina v mm
	float setDiameter; //nastavljen radij 
	float setKonusLenght; //dolzina konusa
	float setAngleMax;
	float setAngleMin;//max in min kot konsa
	float setToleranceAngleMax; //kot za izris toleranc pri pregledu ce so meritve znotraj toeranc
	float setToleranceAngleMin; //kot za izris toleranc pri pregledu ce so meritve znotraj toeranc
	float setDSKDEKoffset;
	float corFactorCam[NR_DMS_CAM];
	int nrEKonusAll;
	int nrEEdgeAll;
	int nrEMiddleAll;
	int nrEKonusMinus;
	int nrEdgeMinus;
	int nrEMiddleMinus;
	int nrELenghtDowel;
	int nrELenghtDowelMinus;
	float deltaErrorX;
	

	float lenghtTolerancePlus;
	float lenghtToleranceMinus;
	float diameterTolerancePlus;
	float diameterToleranceMinus;
	float konusTolerancePlus;
	float konusToleranceMinus;

	int calibrationTransitions[NR_DMS_CAM][2];

	//kalibracijska tabela za 1 do 20 mm sirine moznikov
	int calibrationCenter[20][NR_DMS_CAM];
	float calibrationkorFactor[20][NR_DMS_CAM];
	int selectedCalibration;  //katera stevilka v kalibraciji je izbrana
	float calibrationkorFactorOffset[NR_DMS_CAM];//faktorji ki so zapisani pri offsetu pretvorbe iz centra

	//prikaz slik v zgodovini 
	//QPixmap measurementTablePixmap[DMSHISTORYSIZE];
	//QPixmap dowelPixmap[DMSHISTORYSIZE];
	QImage measurementTableImage[DMSHISTORYSIZE];
	QImage dowelImage[DMSHISTORYSIZE];
	//QTableView tableImage[DMSHISTORYSIZE];
	void ResetMeasurements(int index);
	void ResetBlowImages(int index);


	//predsortiranje
	float presortBasicDetectWindow[5]; //nastavitve za osnovno detekcijo okna 
	float tmpPresortBasicDetectWindow[5]; //nastavitve za osnovno detekcijo okna 

	int presortBasicDowelSet[6]; //nastavitve za osnovno desnol in levo moznikov
	int  tmppresortBasicDowelSet[6]; //nastavitve za osnovno desnol in levo moznikov

	int presortDowelDiameterWindow[20];//okna ki jih nastavimo s kalibracijo glede na debelino moznika
	int presortDowelTopOffset[20]; //okno ki pove kje se nahaja zgornji moznik na ogledalu
	int presortDowelBottomOffset[20]; //okno ki pove kje se nahaja spodnji moznik na ogldealu
	int presortDowelDiameterWindowTmp;//okna ki jih nastavimo s kalibracijo glede na debelino moznika
	int presortDowelTopOffsetTmp; //okno ki pove kje se nahaja zgornji moznik na ogledalu
	int presortDowelBottomOffsetTmp; //okno ki pove kje se nahaja spodnji moznik na ogldealu


	QImage presortDowel[3];

	float presortDowelSpeed;
	float centerPrevImagePresort;
	float processingTimePresort;
	int centerPresort;

	int isGoodPresort[4][3];

	float presortLenght;
	int avrIntPresort[3];
	int stDevPresort[3];
	int nrBlobsPresort[3];
	int isGoodPresortAll;
	int isGoodPresortLenght;
	int isGoodPresortBlobs;

	CMemoryBuffer presortHistory[20];
	int currPresortDowel; //index na katerem se trenutno nahajamo 
	int presortIsGoodHistory[20];

	//predsortiranje nastavitve 
	int xStartDetectWindow;
	int xStopDetectWindow;
	int yMiddleDetectWindow;
	int thresholdDetectWindow;
	float korFactorPresort;
	int dowelHeight;
	int dowelOffsetTop;
	int dowelOffsetBottom;
	int xlTop, xrTop, xlCenter, xrCenter, xlBottom, xrBottom;
	int presortSettings[3][10];


	//nastavitve ventilov 
	int timeToBlow;
	int delayToValve;
	float volumeOfDowel;

	//za zgodovino moznikov
	QString parameterNameHistory[18];
	float toleranceHistoryHIGH[DMSHISTORYSIZE][18];
	float toleranceHistoryLOW[DMSHISTORYSIZE][18];
	int isGoodHistory[DMSHISTORYSIZE][18][NR_DMS_CAM];
	float measureValueHistory[DMSHISTORYSIZE][18][NR_DMS_CAM];
	float widthHistory[10000];
	int historyGaussCounter;
	float lenghtHistory[10000];

	 double widthSumStatistics;
	 double lenghtSumStatistics;
	 double widthSumStatisticsGood;
	 double lenghtSumStatisticsGood;
	 double elipseSumStatistics;

	long long int  statisticsCounter;
	long long int  statisticsCounterGood;



};
