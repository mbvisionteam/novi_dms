#include "stdafx.h"
#include "AdvancedSettings.h"

AdvancedSettings::AdvancedSettings()
{
}

AdvancedSettings::AdvancedSettings(QString referencePath, QString fileName,QStringList groupList)
{
	this->referencePath = referencePath;
	this->fileName = fileName;
	this->name = fileName;
	this->name.chop(4);
	groupName.resize(groupList.size());
	parameterName.resize(groupList.size());
	parameterValue.resize(groupList.size());
	parameterType.resize(groupList.size());

	for (int i = 0; i < groupList.size(); i++)
		groupName[i] = groupList[i];

	ReadParameters();
}

AdvancedSettings::AdvancedSettings(const AdvancedSettings & settings)
{
	this->referencePath = settings.referencePath;
	this->name = settings.name;
	this->fileName = settings.fileName;
	this->groupName = settings.groupName;
	this->parameterName = settings.parameterName;
	this->parameterValue = settings.parameterValue;
	this->parameterType = settings.parameterType;
}

AdvancedSettings::~AdvancedSettings()
{
}

AdvancedSettings AdvancedSettings::operator=(AdvancedSettings settings)
{
	this->referencePath = settings.referencePath;
	this->name = settings.name;
	this->fileName = settings.fileName;
	this->groupName = settings.groupName;
	this->parameterName = settings.parameterName;
	this->parameterValue = settings.parameterValue;
	this->parameterType = settings.parameterType;

	return *this;
}

void AdvancedSettings::ReadParameters()

{
	QStringList values;
	int count = 0;
	int parametersNr = 0;
	QString path = referencePath + QString("%1.ini").arg(name);
	QSettings settings(path, QSettings::IniFormat);


	for (int i = 0; i < groupName.size(); i++)
	{
		count = 0;
		settings.beginGroup(groupName[i]);
		//inputs
		const QStringList childKeys = settings.childKeys();
		//settings.setValue("parameter0", 1);
		if (childKeys.size() > 0)
		{
			do {
				
				values.clear();
				values = settings.value(QString("parameter%1").arg(count)).toStringList();

				if (values.size() > 2)
				{
					for (int k = 0; k < values.size(); k++)
					{
						switch (k)
						{
						case(0):
							parameterValue[i].push_back(values[k].toFloat());
							break;
						case(1):
							parameterName[i].push_back(values[k]);
							break;
						case(2):
							parameterType[i].push_back(values[k].toInt());
							break;
						}


					}

					count++;

				}

			} while (values.size() > 0);
		}
		settings.endGroup();
	}
	//parameterCounter = count;


}

void AdvancedSettings::WriteParameters()
{
}

void AdvancedSettings::WriteParameters(QString groupNames)
{
	QString path = referencePath + QString("%1.ini").arg(name);
	QSettings settings(path, QSettings::IniFormat);
	QStringList list;
	settings.remove(groupNames);
	settings.beginGroup(groupNames);



	QFile file(path);
	for (int i = 0; i < this->groupName.size(); i++)
	{
		if (groupNames.compare(this->groupName[i]) == 0)
		{
			for (int k = 0; k < parameterValue[i].size(); k++)
			{
				list << QString("%1").arg(parameterValue[i][k]) << QString("%1").arg(parameterName[i][k]) << QString("%1").arg(parameterType[i][k]);;
					settings.setValue(QString("parameter%1").arg(k), list);
				list.clear();
			}
		}
	}
	settings.endGroup();
}

void AdvancedSettings::CopyParameters()
{
}

void AdvancedSettings::DeleteParameters()
{
}
