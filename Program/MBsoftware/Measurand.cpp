#include "stdafx.h"
#include "Measurand.h"

Measurand::Measurand(QObject *parent)
	: QObject(parent)
{

}

Measurand::Measurand()
{

	 elipseSumStatistics = 0;
	historyGaussCounter = 0;
	widthSumStatistics = 0;
	lenghtSumStatistics = 0;
	statisticsCounter = 0;
	statisticsCounterGood = 0;
	for (int i = 0; i < 10; i++)
	{
		alarmCounter[i] = 0;
		speedForPresort[i] = 0;
	}
	

	measureArea0[0] = 0.3;
	measureArea0[1] = 2.2;
	measureArea1[0] = 3.40;
	measureArea1[1] = 6.9;
	measureArea2[0] = 7;
	measureArea2[1] = 7;
	setElipsse = 0.5; // max elipsa v mm
	setLenght = 30; //nastavljena dolzina v mm
	 setDiameter = 8; //nastavljen radij 
	 setKonusLenght = 2.2;
	  setAngleMax = 55;
	  setAngleMin = 8;
	  lenghtTolerancePlus = 1;//mm
	  lenghtToleranceMinus = -1;//mm
	  diameterTolerancePlus = 0.25; //mm
	  diameterToleranceMinus = -0.25;
	  konusTolerancePlus = 1.5;
	  konusToleranceMinus = -1.5;
	  //setToleranceAngle = 45;
	 corFactorCam[0] = 0.04341;
	 corFactorCam[1] = 0.02216;
	 corFactorCam[2] = 0.02274;
	 corFactorCam[3] = 0.02068;
	 corFactorCam[4] = 0.0226;
	 corFactorCam[5] = 0.02249;




	  nrEKonusAll = 2;
	  nrEEdgeAll = 2;
	  nrEMiddleAll= 5;
	  nrEKonusMinus = 2;
	  nrEdgeMinus = 2;
	  nrEMiddleMinus = 3;
	  nrELenghtDowel = 3;//plus and minus
	  deltaErrorX = 0.01; //


	  for (int i = 0; i < DMSHISTORYSIZE; i++)
	  {
		  blowImageCounter[i] = 20;

	  }

	  for (int i = 0; i < 20; i++)
		  presortHistory[i].Create(400, 148, 3);

	  currPresortDowel = -1;

}

Measurand::~Measurand()
{
	int bla = 100;

}

void Measurand::ResetMeasurements(int index)
{
	 dowelSpeed[index] =0.0;
	 dowelLenght[index] = 0.0;
	 mesurementElipse[index] = 0.0;
	  dowelGood[index] = 0;//zgodovina ali je moznik dober
	  dowelStringSize[index] = 0;
	  nrDowelMeasurementsZAC[index] = 0;
	  nrDowelMeasurementsKON[index] = 0;
	  nrDowelMeasurements[index] = 0;//
	  dowelReady[index] = 0;//0-15 dow
	  dowelNrXilinx[index] = 0;
	  processingTime[index] = 0.0;
	  dowelString[index] = "";
	  sendDowelString[index] = "";
	  sendAlarm[index] = 0;
	  sendAlarmString[index] = "";

	  measurementAvrDiameter[index] = 0;
	  nrMeasurementsNagative[index] = 0;
	  measureAlarm[index] = 0;
	  
	   blowImageCounter[index] = 20;//test
	   showInHistory[index] = 0; //kadar priklicem mozik iz zgodovine, da ne posljem rezulatov xilinx
	for (int i = 0; i < NR_DMS_CAM; i++)
	{
		dowelMiddleVertical[index][i] = 0.0;
		for (int j = 0; j < MAX_MEASUREMENT; j++)
		{
			for (int k = 0; k < 2; k++)
			{
				 currDowelVertical[index][i][j][k] = 0;//shranjeni prehodi pred ureditvijo po pozicijah 


				  horizontalTransitionsReal[index][j][k] = 0.0;//prehodi od horizontalne kamere v mm
				  horizontalMeasurementsOn[index][j] = -1;
				  horizontalTransitions[index][j][k] = 0;//prehodi od horizontalne kamere 
				  horizontalTransitionType[index][j][k] = 0;//tipi prehoda za horizontalno kamereo
				  verticalDataZAC[index][i][j][k] = 0; //podatki o verticalnih prehodih od vseh kamer 
				  verticalDataKON[index][i][j][k] = 0; //podatki o verticalnih prehodih od vseh kamer 
				   verticalDataZACReal[index][i][j][k] = 0.0; //podatki o verticalnih prehodih od vseh kamer 
				   verticalDataKONReal[index][i][j][k] = 0.0; //podatki o verticalnih prehodih od vseh kamer 
				  verticalDataZACDraw[index][i][j][k] = 0; //podatki o verticalnih prehodih od vseh kamer 
				  verticalDataKONDraw[index][i][j][k] = 0; //podatki o verticalnih prehodih od vseh kamer 
				  verticalDataReal[index][i][j][k] = 0.0; //podatki o verticalnih prehodih od vseh kamer  v mm

			}
			horizontalMeasurementsOn[index][j] = -1;
			measurementsOnArea[index][j] = -1;
			 horizontalDataZACreal[index][j] = -1.0;//podatki o poziciji od zacetka 
			 horizontalDataKONreal[index][j] = -1.0;//podatki o poziciji od konca
			 


			 for (int k = 0; k < 128; k++)
			 {
				  horizontalData[index][j][k] = 0;//podatki o poziciji od zacetka in konca zdruzeni
				  horizontalDataTypes[index][j][k] = 0;//podatki o poziciji od zacetka in konca zdruzeni
			 }
			 diametersDSK[index][i][j]  = QPointF(0.0,0.0);//vse premeri so shranjeni zaradi prikaza in filter ERROR
			 diametersDIS[index][i][j] = QPointF(0.0, 0.0);
			 diametersDIC[index][i][j] = QPointF(0.0, 0.0);
			 diametersDIE[index][i][j] = QPointF(0.0, 0.0);
			 diametersDKE[index][i][j] = QPointF(0.0, 0.0);
			 diameterDataZACReal[index][i][j] = -1; //izracunani premeri v mm s korekcijo
			 diameterDataKONReal[index][i][j] = -1; //izracunani premeri v mm s korekcijo
				diameterDSKGood[index][i][j] = -1;//spremenljivke premerov ali so meritve v tolerancah
				diameterDISGood[index][i][j] = -1;//spremenljivke premerov ali so meritve v tolerancah
				diameterDICGood[index][i][j] = -1;//spremenljivke premerov ali so meritve v tolerancah
				diameterDIEGood[index][i][j] = -1;//spremenljivke premerov ali so meritve v tolerancah
				diameterDKEGood[index][i][j] = -1;//spremenljivke premerov ali so meritve v tolerancah
		}
		nrMeasurementsArea0[index][i] = 0; //stevilo meritev na doloccenem delu moznika
		nrMeasurementsArea1[index][i] = 0; //stevilo meritev na doloccenem delu moznika
		nrMeasurementsArea2[index][i] = 0; //stevilo meritev na doloccenem delu moznika
		nrMeasurementsArea3[index][i] = 0; //stevilo meritev na doloccenem delu moznika
		nrMeasurementsArea4[index][i] = 0; //stevilo meritev na doloccenem delu moznika
		 nrBadMeasurementsDSKAll[index][i] = 0;
		 nrBadMeasurementsDSKMinus[index][i] = 0;
		 nrBadMeasurementsDISAll[index][i] = 0;
		 nrBadMeasurementsDISMinus[index][i] = 0;
		 nrBadMeasurementsDICAll[index][i] = 0;
		 nrBadMeasurementsDICMinus[index][i] = 0;
		 nrBadMeasurementsDIEAll[index][i] = 0;
		 nrBadMeasurementsDIEMinus[index][i] = 0;
		 nrBadMeasurementsDKEAll[index][i] = 0;
		 nrBadMeasurementsDKEMinus[index][i] = 0;
		measurementDSK[index][i] = 0.0; //meriev premera
		measurementDIS[index][i] = 0.0; //meriev premera
		measurementDIC[index][i] = 0.0; //meriev premera
		measurementDIE[index][i] = 0.0; //meriev premera
		measurementDKE[index][i] = 0.0; //meriev premera
		measurementRoughness[index][i] = 0.0;//max hrapavost glede na kamero 

		for (int z = 0; z < 4; z++)
		{
			 lineKonus[index][i][z].SetLine(CPointFloat(0, 0), CPointFloat(0, 0));
			 lineMiddle[index][i][z].SetLine(CPointFloat(0, 0), CPointFloat(0, 0));
			 measurementAngleAn[index][i][z] = 0.0;//racunanje kota med konusom in ravno linijo
			 measurementDistanceDel[index][i][z] = 0.0; //meritve DEL razdaljam med konusom in vrhom mozika
			 measurementKonusLenght[index][i][z] = 0.0;
		}
		


		
	}


	for (int i = 0; i < 20; i++)
	{
		centerOfG[index][i] = CPointFloat(0, 0) ;
		isGoodArray[index][i] = -1;
	}


}

void Measurand::ResetBlowImages(int index)
{

		blowImageCounter[index] = 20;
		for (int i = 0; i < 20; i++)
		{
			blowImage[index][i].zeros(Size(blowImage[index][i].cols, blowImage[index][i].rows), CV_8UC3);
		}
	

}

