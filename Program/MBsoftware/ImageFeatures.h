#pragma once

#include "stdafx.h"
#include <QObject>

enum FeatureTypes
{
	HOG_feature = 0,
	SIFT_feature = 1,
	SURF_feature = 2
};


class ImageFeatures : public QObject
{
	Q_OBJECT;

public:
	ImageFeatures(QObject *parent);

	void GetImageFeatures(int featureType);

	Mat imgFeatures;

private:
	Ptr<cv::HOGDescriptor> hog;
	Ptr<cv::SIFT> sift;
	Ptr<cv::ORB> orb;

	void GetHOGFeatures();
	void GetSIFTFeatures();
	void GetSURFFeatures();
};

