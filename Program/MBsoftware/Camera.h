#pragma once

#include <QWidget>
#include "stdafx.h"

#include "MemoryBuffer.h"
#include "ui_Camera.h"
#include "Timer.h"

//#include "Timer.h"

using namespace std;
using namespace cv;

#define num_buff		1 //stevilo bufferjev v buffercollection - ringu

class CCamera : public QWidget
{
	Q_OBJECT
public:
	CCamera(QWidget *parent = Q_NULLPTR);
	CCamera(const CCamera& camera);// Copy constructor
	~CCamera();
	void closeEvent(QCloseEvent * event);
	int displayisSet;
private:
	Ui::Camera ui;

	bool slovensko;


public slots:
	void Next();
	void Previous();
	void ZoomIn();
	void ZoomOut();
	void ResetZoom();
	void ShowLive();
	void Live();
	void Live(int imageIndex);
	void OnClickedSnapOne();
	void ExposureChanged(double value);
	void GainChanged(int value);
	void XoffsetChanged(int value);
	void YoffsetChanged(int value);
	void SaveSettings();
	void OnSaveImage();
	void SliderExpoChanged(int value);
	void SpinExpoChanged(double value);
	void ImageIndexComboBoxChanged(int value);

signals:
	void frameReadySignal(int id, int imageIndex);//signal za v razred MBVISION OnFrameReady
	void showImageSignal(int imageIndex);



protected:
	bool eventFilter(QObject *obj, QEvent *event);
	void keyPressEvent(QKeyEvent * event);
	void keyReleaseEvent(QKeyEvent * event);



private:
	QGraphicsScene*			sceneVideo;

	void NastaviJezikCam();

	// Attributes
public:
	static int	objectCounter;
	static int	cameraOpened; //koliko kamer je odprtih
	static bool libraryInitialized; //ce je knjiznica inicializirana (mora biti samo enkrat na zacetku)

	int frameReadyCounter; //stevec ki steje kolikokrat je prislo v frameRead zaradi prikaza sliki(ce je FPS vec kot 200 prikazujemo vsako 10 sliko								//biffers

	QGraphicsPixmapItem*	pixmapVideo;

	std::vector<bool>		m_BufferWritten;	// array of flags which buffers have been saved.

	std::vector<CMemoryBuffer>		image;// [num_imagess];
	std::vector<int>				imageReady; // [num_images];
	int						activeImage;  //slika, ki se prikazuje izmed num_images
	int						bufferSize;
	Mat			drawPicture;
	//Mat						currentImage;

	//Focus
	int imageFocus = -1;

	//camera info
	QString cameraIP;
	QString vendorName;
	QString cameraName;
	QString serialNumber;
	QString videoFormat;
	QString customName;
	Timer frameReadyFreq;

	//CAMERA PARAMETERS
	vector<double>	exposure;
	vector<double>  gain;
	vector<double>  savedOffsetX;
	vector<double>  savedOffsetY;
	QString settingsPath;				//poti kjer so shranjen *ini file.
	double	FPS;					 //nastavljen fps
	double	defaultExposure;		//osnovna ekspozicija		
	double	defaultGain;			//osnovno oja�anje
	int		num_images;		//stevilo slik rezerviranih za image
	int		num_capturedImages;  //Dodal Martin: �tevilo zajetih slik (tu ne �tejem umetno generiranih)
	double	currentExposure;
	double	currentGain;
	double maxGain;
	double minGain;
	double minExpo;
	double maxExpo;
	int minOffsetX;
	int minOffsetY;
	int maxOffsetX;
	int maxOffsetY;
	int		height;
	int		width;
	int offsetY;
	int offsetX;
	float zoomFactor;
	int		xOffsetDisp;
	int		yOffsetDisp;
	int		heightDisp;
	int		widthDisp;
	int		depth;
	int		formatType;
	int		matType;
	int		isLive;
	int		isLiveOld;
	int		triggerOutput; //stevilka outputa za triganje
	int		trigger;
	int		id;	//camera id - lahko se uporablja za dolocitev outputa za triganje
	bool		isFrameReady; //vsakic ko pride v frame ready = true
	bool		listener; //flag listener enabled
	bool		grabImage; //zastavica, ce hocemo ze v frame readyu hraniti kopirati sliko v image[imageIndex]
	bool		realTimeProcessing; //if you want that function FrameReady calls OnFrameReady in view class
	int		imageIndex; //index image[] kamor zelimo shraniti sliko
	int		maxGrabbedImages; //stevilo slik, ki jih zajemamo, ko je grabImage = true, potem gre grabImage na false, ce je 0 bo jemal slike dokler se ga rocno ne onemogoci
	bool		rotatedVertical; //get image from camera rotated verticaly
	bool		rotadedHorizontal; //get image from camera rotated horizontaly
							   //spremenljivke za pregledovanje zaklenjenih slik
	int		lockedImagesTable[num_buff]; //tabela zaklenjenih slik - 0: ni zaklenjena > 0: zaklenjena, stevilka pomeni zaporedno stevilko zaklepa
	bool	lockImage;	//zastavica za zaklepanje slike - ce je dvignjena zaklene sliko
	int		lockCounter; //stevec zaklenjenih slik

	bool	isSceneAdded;		//if display scene is added
	int	prevFrameCounter;	//stevilka prejsnjega framea
	int	frameCounter;		//stevilka trenutnega framea
	DWORD		noFrameCounter;		//waiting for new frame counter
	DWORD		triggerCounter;		//waiting for new frame counter
	int		triggerTest;
	bool	grabSucceeded;
	bool	grabberInitialized;
	bool	enableLive;
	int		conversionCode; //code for cenverting picture for displaying
	//Timer	frameReadyFreq;

	int		triggIndex; // index zadnjega triggerja v tabeli
	//	triggerTimer[num_buff];
	int		encoderValues[num_buff];
	int		testCounter;


public:




	void ClearLockingTable();
	//void ShowImg(CDC *pDC, Mat &img, CRect* pRect);
	virtual int EnableStrobe(int polarity);
	virtual int DisableStrobe();

	//imagingSource
	virtual int Init(int selectGrabber, QString cameraName, int VideoFormat, QString VideoFormatS);
	virtual int Init(QString serialNumber, QString VideoFormatS);//imagingSource
	virtual int Init();
	//imagingSource
	virtual int Start(double FPS, bool trigger, bool listener);
	virtual int Start();

	//imagingSource
	virtual int Start(double FPS, bool trigger, bool listener, bool flipV, bool flipH);//imagingSource
	virtual int MemoryUnlock(int imageNr);//imagingSource
	virtual void MemoryUnlockAll();//imagingSource
	virtual int MemoryLock();//imagingSource
	virtual bool SetWhiteBalanceAbsolute(long red, long green, long blue);//imagingSource
	virtual bool SetExposureAbsolute(double dExposure);//imagingSource
	virtual double GetExposureAbsolute();//imagingSource //pointGray
	virtual bool SetGainAbsolute(double dGain);//imagingSource
	virtual int	GetGainAbsolute();//imagingSource //PointGray


	//basler
	virtual int Init(int selectGrabber, QString cameraName, int width, int height, int depth, int offsetX, int offsetY);
	//basler
	virtual int Init(QString cameraName, int width, int height, int depth, int offsetX, int offsetY);
	//basler
	virtual int Start(double FPS, int trigger);
	//basler
	virtual int Start(double FPS, int trigger, bool flipV, bool flipH);
	//basler
	virtual void AutoGainOnce();
	//basler
	virtual void AutoGainContinuous();
	//basler
	virtual void AutoExposureOnce();
	//basler
	virtual void AutoExposureContinuous();
	//basler
	virtual void AutoWhiteBalance();
	//basler
	virtual bool IsColorCamera();

	virtual int GrabImage(int imageNr);
	virtual void CloseGrabber(void);
	virtual bool OnSettingsImage();
	virtual bool SaveReferenceSettings();
	virtual bool SaveReferenceSettings(int);
	virtual bool LoadReferenceSettings();
	virtual bool LoadReferenceSettings(int);
	virtual bool IsDeviceValid();
	//virtual void DisplayData(CDC *pDC, CRect displayRec);
	virtual bool EnableLive();
	virtual bool DisableLive();
	virtual void SetPacketSize(int size);

	virtual bool SetPartialOffsetX(int xOff);
	virtual	int GetPartialOffsetX();
	virtual	bool SetPartialOffsetY(int yOff);
	virtual int GetPartialOffsetY();


	//teledyne dalsa
	virtual int ReadInput();



	int ConvertImageForDisplay(int imageNumber);

	void ShowDialog(int rights);
	void ResizeDisplayRect();
	void ShowImages();
	void ShowImages(int imageIndex);
	void SaveImage(int imageindex, int id);



	int InitImages(int width, int height, int type);
	int InitImages();

	void CopyImage(int sourceIndex, int destIndex);
	void SetDisplay();



};
