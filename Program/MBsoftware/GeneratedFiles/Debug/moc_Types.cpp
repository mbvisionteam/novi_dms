/****************************************************************************
** Meta object code from reading C++ file 'Types.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../Types.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Types.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Types_t {
    QByteArrayData data[13];
    char stringdata0[162];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Types_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Types_t qt_meta_stringdata_Types = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Types"
QT_MOC_LITERAL(1, 6, 9), // "OnConfirm"
QT_MOC_LITERAL(2, 16, 0), // ""
QT_MOC_LITERAL(3, 17, 8), // "OnCancel"
QT_MOC_LITERAL(4, 26, 17), // "OnRemoveParameter"
QT_MOC_LITERAL(5, 44, 5), // "param"
QT_MOC_LITERAL(6, 50, 14), // "OnAddParameter"
QT_MOC_LITERAL(7, 65, 11), // "OnOpenPlans"
QT_MOC_LITERAL(8, 77, 20), // "OnTypeSettingsCancel"
QT_MOC_LITERAL(9, 98, 16), // "OnTypeSettingsOK"
QT_MOC_LITERAL(10, 115, 12), // "OnAddSetting"
QT_MOC_LITERAL(11, 128, 15), // "OnRemoveSetting"
QT_MOC_LITERAL(12, 144, 17) // "OnChangeGroupName"

    },
    "Types\0OnConfirm\0\0OnCancel\0OnRemoveParameter\0"
    "param\0OnAddParameter\0OnOpenPlans\0"
    "OnTypeSettingsCancel\0OnTypeSettingsOK\0"
    "OnAddSetting\0OnRemoveSetting\0"
    "OnChangeGroupName"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Types[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x0a /* Public */,
       3,    0,   65,    2, 0x0a /* Public */,
       4,    1,   66,    2, 0x0a /* Public */,
       6,    0,   69,    2, 0x0a /* Public */,
       7,    0,   70,    2, 0x0a /* Public */,
       8,    0,   71,    2, 0x0a /* Public */,
       9,    0,   72,    2, 0x0a /* Public */,
      10,    0,   73,    2, 0x0a /* Public */,
      11,    0,   74,    2, 0x0a /* Public */,
      12,    0,   75,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int,
    QMetaType::Int,
    QMetaType::Int,

       0        // eod
};

void Types::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Types *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OnConfirm(); break;
        case 1: _t->OnCancel(); break;
        case 2: _t->OnRemoveParameter((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->OnAddParameter(); break;
        case 4: _t->OnOpenPlans(); break;
        case 5: _t->OnTypeSettingsCancel(); break;
        case 6: _t->OnTypeSettingsOK(); break;
        case 7: { int _r = _t->OnAddSetting();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 8: { int _r = _t->OnRemoveSetting();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 9: { int _r = _t->OnChangeGroupName();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Types::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_Types.data,
    qt_meta_data_Types,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Types::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Types::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Types.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Types::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
