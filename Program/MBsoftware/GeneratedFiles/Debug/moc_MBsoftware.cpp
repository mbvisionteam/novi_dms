/****************************************************************************
** Meta object code from reading C++ file 'MBsoftware.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../MBsoftware.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MBsoftware.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MBsoftware_t {
    QByteArrayData data[74];
    char stringdata0[1276];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MBsoftware_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MBsoftware_t qt_meta_stringdata_MBsoftware = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MBsoftware"
QT_MOC_LITERAL(1, 11, 18), // "measurePieceSignal"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 12), // "currentPiece"
QT_MOC_LITERAL(4, 44, 16), // "UpdateDowelImage"
QT_MOC_LITERAL(5, 61, 9), // "currPiece"
QT_MOC_LITERAL(6, 71, 16), // "DrawMeasurements"
QT_MOC_LITERAL(7, 88, 9), // "ViewTimer"
QT_MOC_LITERAL(8, 98, 8), // "GetError"
QT_MOC_LITERAL(9, 107, 12), // "OnFrameReady"
QT_MOC_LITERAL(10, 120, 14), // "OnTcpDataReady"
QT_MOC_LITERAL(11, 135, 4), // "conn"
QT_MOC_LITERAL(12, 140, 5), // "parse"
QT_MOC_LITERAL(13, 146, 11), // "HistoryCall"
QT_MOC_LITERAL(14, 158, 7), // "station"
QT_MOC_LITERAL(15, 166, 22), // "OnClickedShowLptButton"
QT_MOC_LITERAL(16, 189, 23), // "OnClickedShowLptButton0"
QT_MOC_LITERAL(17, 213, 23), // "OnClickedShowLptButton1"
QT_MOC_LITERAL(18, 237, 23), // "OnClickedShowCamButton0"
QT_MOC_LITERAL(19, 261, 23), // "OnClickedShowCamButton1"
QT_MOC_LITERAL(20, 285, 23), // "OnClickedShowUZedButton"
QT_MOC_LITERAL(21, 309, 22), // "OnClickedShowCamButton"
QT_MOC_LITERAL(22, 332, 28), // "OnClickedShowSmartCardButton"
QT_MOC_LITERAL(23, 361, 28), // "OnClickedShowImageProcessing"
QT_MOC_LITERAL(24, 390, 26), // "OnClickedShowDowelSettings"
QT_MOC_LITERAL(25, 417, 27), // "OnClickedShowDMScalibration"
QT_MOC_LITERAL(26, 445, 26), // "OnClickedShowPresortDialog"
QT_MOC_LITERAL(27, 472, 19), // "OnClickedSelectType"
QT_MOC_LITERAL(28, 492, 18), // "OnClickedEditTypes"
QT_MOC_LITERAL(29, 511, 23), // "OnClickedRemoveTypesOld"
QT_MOC_LITERAL(30, 535, 20), // "OnClickedResetGlobal"
QT_MOC_LITERAL(31, 556, 20), // "OnClickedRemoveTypes"
QT_MOC_LITERAL(32, 577, 17), // "OnClickedAddTypes"
QT_MOC_LITERAL(33, 595, 21), // "OnSignalCreateTypeDMS"
QT_MOC_LITERAL(34, 617, 9), // "exsisting"
QT_MOC_LITERAL(35, 627, 7), // "newName"
QT_MOC_LITERAL(36, 635, 8), // "CopyType"
QT_MOC_LITERAL(37, 644, 21), // "OnClickedTypeSettings"
QT_MOC_LITERAL(38, 666, 22), // "OnClickedResetCounters"
QT_MOC_LITERAL(39, 689, 14), // "OnClickedLogin"
QT_MOC_LITERAL(40, 704, 18), // "OnClickedStatusBox"
QT_MOC_LITERAL(41, 723, 18), // "OnClickedSmcButton"
QT_MOC_LITERAL(42, 742, 20), // "OnClickedShowHistory"
QT_MOC_LITERAL(43, 763, 24), // "OnClickedGeneralSettings"
QT_MOC_LITERAL(44, 788, 16), // "OnClickedAboutUs"
QT_MOC_LITERAL(45, 805, 17), // "onClientReadyRead"
QT_MOC_LITERAL(46, 823, 21), // "OnClickedTCPconnction"
QT_MOC_LITERAL(47, 845, 17), // "OnClickedEMGReset"
QT_MOC_LITERAL(48, 863, 13), // "SaveVariables"
QT_MOC_LITERAL(49, 877, 13), // "LoadVariables"
QT_MOC_LITERAL(50, 891, 23), // "SaveVariablesStatistics"
QT_MOC_LITERAL(51, 915, 23), // "LoadVariablesStatistics"
QT_MOC_LITERAL(52, 939, 18), // "CalculateValveTime"
QT_MOC_LITERAL(53, 958, 6), // "AddLog"
QT_MOC_LITERAL(54, 965, 4), // "text"
QT_MOC_LITERAL(55, 970, 4), // "type"
QT_MOC_LITERAL(56, 975, 12), // "EraseLastLOG"
QT_MOC_LITERAL(57, 988, 12), // "MeasurePiece"
QT_MOC_LITERAL(58, 1001, 5), // "index"
QT_MOC_LITERAL(59, 1007, 13), // "UpdateHistory"
QT_MOC_LITERAL(60, 1021, 10), // "SetHistory"
QT_MOC_LITERAL(61, 1032, 14), // "WriteLogToFile"
QT_MOC_LITERAL(62, 1047, 4), // "path"
QT_MOC_LITERAL(63, 1052, 12), // "OnClickedDMS"
QT_MOC_LITERAL(64, 1065, 26), // "OnClickedDMStransitionTest"
QT_MOC_LITERAL(65, 1092, 12), // "SetTolerance"
QT_MOC_LITERAL(66, 1105, 11), // "currentType"
QT_MOC_LITERAL(67, 1117, 31), // "SetToleranceAutoCalibrationMode"
QT_MOC_LITERAL(68, 1149, 21), // "OnSingnalSelectedType"
QT_MOC_LITERAL(69, 1171, 12), // "selectedType"
QT_MOC_LITERAL(70, 1184, 24), // "OnSignalAdvSettingsSaved"
QT_MOC_LITERAL(71, 1209, 8), // "selected"
QT_MOC_LITERAL(72, 1218, 28), // "OnSignalEnterCalibrationMode"
QT_MOC_LITERAL(73, 1247, 28) // "OnSignalLeaveCalibrationMode"

    },
    "MBsoftware\0measurePieceSignal\0\0"
    "currentPiece\0UpdateDowelImage\0currPiece\0"
    "DrawMeasurements\0ViewTimer\0GetError\0"
    "OnFrameReady\0OnTcpDataReady\0conn\0parse\0"
    "HistoryCall\0station\0OnClickedShowLptButton\0"
    "OnClickedShowLptButton0\0OnClickedShowLptButton1\0"
    "OnClickedShowCamButton0\0OnClickedShowCamButton1\0"
    "OnClickedShowUZedButton\0OnClickedShowCamButton\0"
    "OnClickedShowSmartCardButton\0"
    "OnClickedShowImageProcessing\0"
    "OnClickedShowDowelSettings\0"
    "OnClickedShowDMScalibration\0"
    "OnClickedShowPresortDialog\0"
    "OnClickedSelectType\0OnClickedEditTypes\0"
    "OnClickedRemoveTypesOld\0OnClickedResetGlobal\0"
    "OnClickedRemoveTypes\0OnClickedAddTypes\0"
    "OnSignalCreateTypeDMS\0exsisting\0newName\0"
    "CopyType\0OnClickedTypeSettings\0"
    "OnClickedResetCounters\0OnClickedLogin\0"
    "OnClickedStatusBox\0OnClickedSmcButton\0"
    "OnClickedShowHistory\0OnClickedGeneralSettings\0"
    "OnClickedAboutUs\0onClientReadyRead\0"
    "OnClickedTCPconnction\0OnClickedEMGReset\0"
    "SaveVariables\0LoadVariables\0"
    "SaveVariablesStatistics\0LoadVariablesStatistics\0"
    "CalculateValveTime\0AddLog\0text\0type\0"
    "EraseLastLOG\0MeasurePiece\0index\0"
    "UpdateHistory\0SetHistory\0WriteLogToFile\0"
    "path\0OnClickedDMS\0OnClickedDMStransitionTest\0"
    "SetTolerance\0currentType\0"
    "SetToleranceAutoCalibrationMode\0"
    "OnSingnalSelectedType\0selectedType\0"
    "OnSignalAdvSettingsSaved\0selected\0"
    "OnSignalEnterCalibrationMode\0"
    "OnSignalLeaveCalibrationMode"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MBsoftware[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      59,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  309,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,  312,    2, 0x08 /* Private */,
       6,    0,  315,    2, 0x08 /* Private */,
       7,    0,  316,    2, 0x08 /* Private */,
       8,    0,  317,    2, 0x08 /* Private */,
       9,    2,  318,    2, 0x08 /* Private */,
      10,    2,  323,    2, 0x08 /* Private */,
      13,    2,  328,    2, 0x08 /* Private */,
      15,    1,  333,    2, 0x08 /* Private */,
      16,    0,  336,    2, 0x08 /* Private */,
      17,    0,  337,    2, 0x08 /* Private */,
      18,    0,  338,    2, 0x08 /* Private */,
      19,    0,  339,    2, 0x08 /* Private */,
      20,    1,  340,    2, 0x08 /* Private */,
      21,    1,  343,    2, 0x08 /* Private */,
      22,    1,  346,    2, 0x08 /* Private */,
      23,    0,  349,    2, 0x08 /* Private */,
      24,    0,  350,    2, 0x08 /* Private */,
      25,    0,  351,    2, 0x08 /* Private */,
      26,    0,  352,    2, 0x08 /* Private */,
      27,    0,  353,    2, 0x08 /* Private */,
      28,    0,  354,    2, 0x08 /* Private */,
      29,    0,  355,    2, 0x08 /* Private */,
      30,    0,  356,    2, 0x08 /* Private */,
      31,    0,  357,    2, 0x08 /* Private */,
      32,    0,  358,    2, 0x08 /* Private */,
      33,    2,  359,    2, 0x08 /* Private */,
      36,    2,  364,    2, 0x08 /* Private */,
      37,    0,  369,    2, 0x08 /* Private */,
      38,    0,  370,    2, 0x08 /* Private */,
      39,    0,  371,    2, 0x08 /* Private */,
      40,    0,  372,    2, 0x08 /* Private */,
      41,    0,  373,    2, 0x08 /* Private */,
      42,    0,  374,    2, 0x08 /* Private */,
      43,    0,  375,    2, 0x08 /* Private */,
      44,    0,  376,    2, 0x08 /* Private */,
      45,    0,  377,    2, 0x08 /* Private */,
      46,    1,  378,    2, 0x08 /* Private */,
      47,    0,  381,    2, 0x08 /* Private */,
      48,    0,  382,    2, 0x08 /* Private */,
      49,    0,  383,    2, 0x08 /* Private */,
      50,    0,  384,    2, 0x08 /* Private */,
      51,    0,  385,    2, 0x08 /* Private */,
      52,    0,  386,    2, 0x08 /* Private */,
      53,    2,  387,    2, 0x08 /* Private */,
      56,    0,  392,    2, 0x08 /* Private */,
      57,    1,  393,    2, 0x08 /* Private */,
      57,    2,  396,    2, 0x08 /* Private */,
      59,    2,  401,    2, 0x08 /* Private */,
      60,    0,  406,    2, 0x08 /* Private */,
      61,    3,  407,    2, 0x08 /* Private */,
      63,    0,  414,    2, 0x08 /* Private */,
      64,    0,  415,    2, 0x08 /* Private */,
      65,    1,  416,    2, 0x08 /* Private */,
      67,    0,  419,    2, 0x08 /* Private */,
      68,    1,  420,    2, 0x08 /* Private */,
      70,    1,  423,    2, 0x08 /* Private */,
      72,    0,  426,    2, 0x08 /* Private */,
      73,    0,  427,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   11,   12,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   14,    3,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   34,   35,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   34,   35,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   54,   55,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   58,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   14,   58,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   14,   58,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int,   62,   54,   55,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   66,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   69,
    QMetaType::Void, QMetaType::QString,   71,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MBsoftware::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MBsoftware *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->measurePieceSignal((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->UpdateDowelImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->DrawMeasurements(); break;
        case 3: _t->ViewTimer(); break;
        case 4: _t->GetError(); break;
        case 5: _t->OnFrameReady((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->OnTcpDataReady((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->HistoryCall((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: _t->OnClickedShowLptButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->OnClickedShowLptButton0(); break;
        case 10: _t->OnClickedShowLptButton1(); break;
        case 11: _t->OnClickedShowCamButton0(); break;
        case 12: _t->OnClickedShowCamButton1(); break;
        case 13: _t->OnClickedShowUZedButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->OnClickedShowCamButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->OnClickedShowSmartCardButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->OnClickedShowImageProcessing(); break;
        case 17: _t->OnClickedShowDowelSettings(); break;
        case 18: _t->OnClickedShowDMScalibration(); break;
        case 19: _t->OnClickedShowPresortDialog(); break;
        case 20: _t->OnClickedSelectType(); break;
        case 21: _t->OnClickedEditTypes(); break;
        case 22: _t->OnClickedRemoveTypesOld(); break;
        case 23: _t->OnClickedResetGlobal(); break;
        case 24: _t->OnClickedRemoveTypes(); break;
        case 25: _t->OnClickedAddTypes(); break;
        case 26: _t->OnSignalCreateTypeDMS((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 27: _t->CopyType((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 28: _t->OnClickedTypeSettings(); break;
        case 29: _t->OnClickedResetCounters(); break;
        case 30: _t->OnClickedLogin(); break;
        case 31: _t->OnClickedStatusBox(); break;
        case 32: _t->OnClickedSmcButton(); break;
        case 33: _t->OnClickedShowHistory(); break;
        case 34: _t->OnClickedGeneralSettings(); break;
        case 35: _t->OnClickedAboutUs(); break;
        case 36: _t->onClientReadyRead(); break;
        case 37: _t->OnClickedTCPconnction((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 38: _t->OnClickedEMGReset(); break;
        case 39: _t->SaveVariables(); break;
        case 40: _t->LoadVariables(); break;
        case 41: _t->SaveVariablesStatistics(); break;
        case 42: _t->LoadVariablesStatistics(); break;
        case 43: _t->CalculateValveTime(); break;
        case 44: _t->AddLog((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 45: _t->EraseLastLOG(); break;
        case 46: _t->MeasurePiece((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 47: _t->MeasurePiece((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 48: _t->UpdateHistory((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 49: _t->SetHistory(); break;
        case 50: _t->WriteLogToFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 51: _t->OnClickedDMS(); break;
        case 52: _t->OnClickedDMStransitionTest(); break;
        case 53: _t->SetTolerance((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 54: _t->SetToleranceAutoCalibrationMode(); break;
        case 55: _t->OnSingnalSelectedType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 56: _t->OnSignalAdvSettingsSaved((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 57: _t->OnSignalEnterCalibrationMode(); break;
        case 58: _t->OnSignalLeaveCalibrationMode(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MBsoftware::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MBsoftware::measurePieceSignal)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MBsoftware::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MBsoftware.data,
    qt_meta_data_MBsoftware,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MBsoftware::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MBsoftware::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MBsoftware.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MBsoftware::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 59)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 59;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 59)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 59;
    }
    return _id;
}

// SIGNAL 0
void MBsoftware::measurePieceSignal(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
