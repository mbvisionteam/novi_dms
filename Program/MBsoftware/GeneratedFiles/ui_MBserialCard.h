/********************************************************************************
** Form generated from reading UI file 'MBserialCard.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MBSERIALCARD_H
#define UI_MBSERIALCARD_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ControlCard
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBox_2;
    QGroupBox *CommonInputBox;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *groupInput0;
    QGroupBox *groupInput1;
    QGroupBox *groupBox_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QComboBox *comboBox;
    QSlider *horizontalSliderCH1;
    QSpinBox *spinBoxCH1;
    QLabel *label;
    QGroupBox *OutputBox;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupOutput0;
    QGroupBox *groupOutput1;
    QGroupBox *groupBox_4;
    QGroupBox *groupTimers;
    QGridLayout *gridLayout_3;
    QSpinBox *spinCounter2;
    QSpinBox *spinCounter1;
    QCheckBox *checkBoxCounter1;
    QCheckBox *checkBoxCounter3;
    QCheckBox *checkBoxCounter4;
    QSpinBox *spinCounter4;
    QCheckBox *checkBoxCounter2;
    QSpinBox *spinCounter3;
    QLabel *labelCounter1;
    QLabel *labelCounter2;
    QLabel *labelCounter3;
    QLabel *labelCounter4;

    void setupUi(QWidget *ControlCard)
    {
        if (ControlCard->objectName().isEmpty())
            ControlCard->setObjectName(QString::fromUtf8("ControlCard"));
        ControlCard->resize(984, 679);
        gridLayout = new QGridLayout(ControlCard);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox_2 = new QGroupBox(ControlCard);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(0, 100));

        gridLayout->addWidget(groupBox_2, 2, 1, 1, 1);

        CommonInputBox = new QGroupBox(ControlCard);
        CommonInputBox->setObjectName(QString::fromUtf8("CommonInputBox"));
        CommonInputBox->setFlat(true);
        horizontalLayout_2 = new QHBoxLayout(CommonInputBox);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        groupInput0 = new QGroupBox(CommonInputBox);
        groupInput0->setObjectName(QString::fromUtf8("groupInput0"));

        horizontalLayout_2->addWidget(groupInput0);

        groupInput1 = new QGroupBox(CommonInputBox);
        groupInput1->setObjectName(QString::fromUtf8("groupInput1"));

        horizontalLayout_2->addWidget(groupInput1);


        gridLayout->addWidget(CommonInputBox, 3, 2, 1, 2);

        groupBox_3 = new QGroupBox(ControlCard);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(0, 100));

        gridLayout->addWidget(groupBox_3, 2, 2, 1, 1);

        groupBox = new QGroupBox(ControlCard);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(0, 100));
        groupBox->setMaximumSize(QSize(16777215, 180));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        comboBox = new QComboBox(groupBox);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_2->addWidget(comboBox, 2, 2, 1, 1);

        horizontalSliderCH1 = new QSlider(groupBox);
        horizontalSliderCH1->setObjectName(QString::fromUtf8("horizontalSliderCH1"));
        horizontalSliderCH1->setMaximum(4000);
        horizontalSliderCH1->setOrientation(Qt::Horizontal);
        horizontalSliderCH1->setInvertedAppearance(false);
        horizontalSliderCH1->setInvertedControls(false);

        gridLayout_2->addWidget(horizontalSliderCH1, 1, 0, 1, 2);

        spinBoxCH1 = new QSpinBox(groupBox);
        spinBoxCH1->setObjectName(QString::fromUtf8("spinBoxCH1"));
        spinBoxCH1->setMaximum(4000);

        gridLayout_2->addWidget(spinBoxCH1, 1, 2, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 2, 0, 1, 1);


        gridLayout->addWidget(groupBox, 2, 0, 1, 1);

        OutputBox = new QGroupBox(ControlCard);
        OutputBox->setObjectName(QString::fromUtf8("OutputBox"));
        OutputBox->setMaximumSize(QSize(600, 300));
        OutputBox->setFlat(true);
        OutputBox->setCheckable(false);
        horizontalLayout = new QHBoxLayout(OutputBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupOutput0 = new QGroupBox(OutputBox);
        groupOutput0->setObjectName(QString::fromUtf8("groupOutput0"));

        horizontalLayout->addWidget(groupOutput0);

        groupOutput1 = new QGroupBox(OutputBox);
        groupOutput1->setObjectName(QString::fromUtf8("groupOutput1"));

        horizontalLayout->addWidget(groupOutput1);


        gridLayout->addWidget(OutputBox, 3, 0, 1, 2);

        groupBox_4 = new QGroupBox(ControlCard);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setMinimumSize(QSize(0, 100));
        groupBox_4->setMaximumSize(QSize(500, 500));

        gridLayout->addWidget(groupBox_4, 2, 3, 1, 1);

        groupTimers = new QGroupBox(ControlCard);
        groupTimers->setObjectName(QString::fromUtf8("groupTimers"));
        groupTimers->setMinimumSize(QSize(0, 150));
        groupTimers->setMaximumSize(QSize(16777215, 150));
        gridLayout_3 = new QGridLayout(groupTimers);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        spinCounter2 = new QSpinBox(groupTimers);
        spinCounter2->setObjectName(QString::fromUtf8("spinCounter2"));

        gridLayout_3->addWidget(spinCounter2, 1, 1, 1, 1);

        spinCounter1 = new QSpinBox(groupTimers);
        spinCounter1->setObjectName(QString::fromUtf8("spinCounter1"));

        gridLayout_3->addWidget(spinCounter1, 0, 1, 1, 1);

        checkBoxCounter1 = new QCheckBox(groupTimers);
        checkBoxCounter1->setObjectName(QString::fromUtf8("checkBoxCounter1"));

        gridLayout_3->addWidget(checkBoxCounter1, 0, 2, 1, 1);

        checkBoxCounter3 = new QCheckBox(groupTimers);
        checkBoxCounter3->setObjectName(QString::fromUtf8("checkBoxCounter3"));

        gridLayout_3->addWidget(checkBoxCounter3, 2, 2, 1, 1);

        checkBoxCounter4 = new QCheckBox(groupTimers);
        checkBoxCounter4->setObjectName(QString::fromUtf8("checkBoxCounter4"));

        gridLayout_3->addWidget(checkBoxCounter4, 3, 2, 1, 1);

        spinCounter4 = new QSpinBox(groupTimers);
        spinCounter4->setObjectName(QString::fromUtf8("spinCounter4"));

        gridLayout_3->addWidget(spinCounter4, 3, 1, 1, 1);

        checkBoxCounter2 = new QCheckBox(groupTimers);
        checkBoxCounter2->setObjectName(QString::fromUtf8("checkBoxCounter2"));

        gridLayout_3->addWidget(checkBoxCounter2, 1, 2, 1, 1);

        spinCounter3 = new QSpinBox(groupTimers);
        spinCounter3->setObjectName(QString::fromUtf8("spinCounter3"));

        gridLayout_3->addWidget(spinCounter3, 2, 1, 1, 1);

        labelCounter1 = new QLabel(groupTimers);
        labelCounter1->setObjectName(QString::fromUtf8("labelCounter1"));

        gridLayout_3->addWidget(labelCounter1, 0, 0, 1, 1);

        labelCounter2 = new QLabel(groupTimers);
        labelCounter2->setObjectName(QString::fromUtf8("labelCounter2"));

        gridLayout_3->addWidget(labelCounter2, 1, 0, 1, 1);

        labelCounter3 = new QLabel(groupTimers);
        labelCounter3->setObjectName(QString::fromUtf8("labelCounter3"));

        gridLayout_3->addWidget(labelCounter3, 2, 0, 1, 1);

        labelCounter4 = new QLabel(groupTimers);
        labelCounter4->setObjectName(QString::fromUtf8("labelCounter4"));

        gridLayout_3->addWidget(labelCounter4, 3, 0, 1, 1);


        gridLayout->addWidget(groupTimers, 0, 2, 1, 2);


        retranslateUi(ControlCard);

        QMetaObject::connectSlotsByName(ControlCard);
    } // setupUi

    void retranslateUi(QWidget *ControlCard)
    {
        ControlCard->setWindowTitle(QCoreApplication::translate("ControlCard", "Form", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("ControlCard", "Channel 2", nullptr));
        CommonInputBox->setTitle(QCoreApplication::translate("ControlCard", "Input", nullptr));
        groupInput0->setTitle(QCoreApplication::translate("ControlCard", "Input 0-7", nullptr));
        groupInput1->setTitle(QCoreApplication::translate("ControlCard", "Input 8-15", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("ControlCard", "Channel 3", nullptr));
        groupBox->setTitle(QCoreApplication::translate("ControlCard", "Channel1 ", nullptr));
        comboBox->setItemText(0, QCoreApplication::translate("ControlCard", "2x", nullptr));
        comboBox->setItemText(1, QCoreApplication::translate("ControlCard", "2.5x", nullptr));
        comboBox->setItemText(2, QCoreApplication::translate("ControlCard", "3x", nullptr));
        comboBox->setItemText(3, QCoreApplication::translate("ControlCard", "3.5x", nullptr));
        comboBox->setItemText(4, QCoreApplication::translate("ControlCard", "4x", nullptr));

        label->setText(QCoreApplication::translate("ControlCard", "Oja\304\215anje:", nullptr));
        OutputBox->setTitle(QCoreApplication::translate("ControlCard", "Output", nullptr));
        groupOutput0->setTitle(QCoreApplication::translate("ControlCard", "Output 0-7", nullptr));
        groupOutput1->setTitle(QCoreApplication::translate("ControlCard", "Output 8-15", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("ControlCard", "Channel 4", nullptr));
        groupTimers->setTitle(QCoreApplication::translate("ControlCard", "Set counters", nullptr));
        checkBoxCounter1->setText(QCoreApplication::translate("ControlCard", "Enabled", nullptr));
        checkBoxCounter3->setText(QCoreApplication::translate("ControlCard", "Enabled", nullptr));
        checkBoxCounter4->setText(QCoreApplication::translate("ControlCard", "Enabled", nullptr));
        checkBoxCounter2->setText(QCoreApplication::translate("ControlCard", "Enabled", nullptr));
        labelCounter1->setText(QCoreApplication::translate("ControlCard", "Counter1:", nullptr));
        labelCounter2->setText(QCoreApplication::translate("ControlCard", "Counter2:", nullptr));
        labelCounter3->setText(QCoreApplication::translate("ControlCard", "Counter3:", nullptr));
        labelCounter4->setText(QCoreApplication::translate("ControlCard", "Counter4:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ControlCard: public Ui_ControlCard {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MBSERIALCARD_H
