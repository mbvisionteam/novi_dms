/********************************************************************************
** Form generated from reading UI file 'AboutUS.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUTUS_H
#define UI_ABOUTUS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AboutUS
{
public:
    QLabel *label;
    QLabel *label_2;

    void setupUi(QWidget *AboutUS)
    {
        if (AboutUS->objectName().isEmpty())
            AboutUS->setObjectName(QString::fromUtf8("AboutUS"));
        AboutUS->resize(360, 321);
        label = new QLabel(AboutUS);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 341, 131));
        label->setPixmap(QPixmap(QString::fromUtf8("res/MB.bmp")));
        label->setScaledContents(true);
        label_2 = new QLabel(AboutUS);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(30, 150, 251, 191));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(10);
        label_2->setFont(font);

        retranslateUi(AboutUS);

        QMetaObject::connectSlotsByName(AboutUS);
    } // setupUi

    void retranslateUi(QWidget *AboutUS)
    {
        AboutUS->setWindowTitle(QCoreApplication::translate("AboutUS", "AboutUS", nullptr));
        label->setText(QString());
        label_2->setText(QCoreApplication::translate("AboutUS", "MBvision d.o.o.\n"
"Cesta Andreja Bitenca 112\n"
"1000 Ljubljana\n"
"Slovenija\n"
"\n"
"Tel: +386 (0)1 510 89 76\n"
"Fax: +386 (0)1 518 32 41\n"
"E-mail: info@mbvision.si\n"
"Skype: Skype: MBvision\n"
"\n"
"", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AboutUS: public Ui_AboutUS {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUTUS_H
