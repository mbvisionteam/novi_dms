/********************************************************************************
** Form generated from reading UI file 'DMS.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DMS_H
#define UI_DMS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DMS
{
public:
    QPushButton *buttonLive;
    QLabel *labelTakt;
    QLabel *label_2;
    QPushButton *buttonSend;
    QLineEdit *lineSend;
    QPushButton *buttonShowDowel;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QFrame *frame;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBoxGain0;
    QGroupBox *groupBoxOffset0;
    QGroupBox *groupBoxStop0;
    QGroupBox *groupBoxStart0;
    QLabel *label_5;
    QSpacerItem *verticalSpacer_2;
    QGraphicsView *imageView0;
    QFrame *frame_2;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBoxGain1;
    QGroupBox *groupBoxOffset1;
    QGroupBox *groupBoxStop1;
    QGroupBox *groupBoxStart1;
    QGraphicsView *imageView4;
    QLabel *label_7;
    QGraphicsView *imageView3;
    QSpacerItem *verticalSpacer_5;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer;
    QFrame *frame_4;
    QWidget *gridLayoutWidget_5;
    QGridLayout *gridLayout_5;
    QGroupBox *groupBoxGain3;
    QGroupBox *groupBoxOffset3;
    QGroupBox *groupBoxStop3;
    QGroupBox *groupBoxStart3;
    QGraphicsView *imageView5;
    QSpacerItem *verticalSpacer_3;
    QFrame *frame_5;
    QWidget *gridLayoutWidget_6;
    QGridLayout *gridLayout_6;
    QGroupBox *groupBoxGain4;
    QGroupBox *groupBoxOffset4;
    QGroupBox *groupBoxStop4;
    QGroupBox *groupBoxStart4;
    QFrame *frame_3;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBoxGain2;
    QGroupBox *groupBoxOffset2;
    QGroupBox *groupBoxStop2;
    QGroupBox *groupBoxStart2;
    QGraphicsView *imageView1;
    QGraphicsView *imageView2;
    QLabel *label_6;
    QSpacerItem *verticalSpacer;
    QLabel *label_3;
    QLabel *label;
    QSpacerItem *verticalSpacer_4;
    QFrame *frame_6;
    QWidget *gridLayoutWidget_7;
    QGridLayout *gridLayout_7;
    QGroupBox *groupBoxStop5;
    QGroupBox *groupBoxStart5;
    QGroupBox *groupBoxOffset5;
    QGroupBox *groupBoxGain5;

    void setupUi(QWidget *DMS)
    {
        if (DMS->objectName().isEmpty())
            DMS->setObjectName(QString::fromUtf8("DMS"));
        DMS->resize(1920, 1080);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DMS->sizePolicy().hasHeightForWidth());
        DMS->setSizePolicy(sizePolicy);
        DMS->setMinimumSize(QSize(1920, 1080));
        DMS->setMaximumSize(QSize(1920, 1080));
        buttonLive = new QPushButton(DMS);
        buttonLive->setObjectName(QString::fromUtf8("buttonLive"));
        buttonLive->setGeometry(QRect(1750, 20, 161, 61));
        labelTakt = new QLabel(DMS);
        labelTakt->setObjectName(QString::fromUtf8("labelTakt"));
        labelTakt->setGeometry(QRect(1790, 620, 91, 31));
        labelTakt->setFrameShape(QFrame::StyledPanel);
        labelTakt->setFrameShadow(QFrame::Raised);
        label_2 = new QLabel(DMS);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(1750, 270, 151, 21));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        label_2->setFont(font);
        label_2->setFrameShape(QFrame::Box);
        label_2->setFrameShadow(QFrame::Sunken);
        buttonSend = new QPushButton(DMS);
        buttonSend->setObjectName(QString::fromUtf8("buttonSend"));
        buttonSend->setGeometry(QRect(1780, 380, 131, 31));
        buttonSend->setAutoDefault(false);
        lineSend = new QLineEdit(DMS);
        lineSend->setObjectName(QString::fromUtf8("lineSend"));
        lineSend->setGeometry(QRect(1770, 290, 121, 31));
        buttonShowDowel = new QPushButton(DMS);
        buttonShowDowel->setObjectName(QString::fromUtf8("buttonShowDowel"));
        buttonShowDowel->setGeometry(QRect(1770, 440, 131, 31));
        buttonShowDowel->setAutoDefault(false);
        gridLayoutWidget = new QWidget(DMS);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 1731, 971));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(gridLayoutWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(200, 0));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_2 = new QWidget(frame);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(-1, 9, 201, 131));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        groupBoxGain0 = new QGroupBox(gridLayoutWidget_2);
        groupBoxGain0->setObjectName(QString::fromUtf8("groupBoxGain0"));

        gridLayout_2->addWidget(groupBoxGain0, 0, 0, 1, 1);

        groupBoxOffset0 = new QGroupBox(gridLayoutWidget_2);
        groupBoxOffset0->setObjectName(QString::fromUtf8("groupBoxOffset0"));

        gridLayout_2->addWidget(groupBoxOffset0, 0, 1, 1, 1);

        groupBoxStop0 = new QGroupBox(gridLayoutWidget_2);
        groupBoxStop0->setObjectName(QString::fromUtf8("groupBoxStop0"));

        gridLayout_2->addWidget(groupBoxStop0, 1, 1, 1, 1);

        groupBoxStart0 = new QGroupBox(gridLayoutWidget_2);
        groupBoxStart0->setObjectName(QString::fromUtf8("groupBoxStart0"));

        gridLayout_2->addWidget(groupBoxStart0, 1, 0, 1, 1);


        gridLayout->addWidget(frame, 0, 3, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font1;
        font1.setPointSize(20);
        font1.setBold(true);
        font1.setWeight(75);
        label_5->setFont(font1);

        gridLayout->addWidget(label_5, 6, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_2, 3, 1, 1, 1);

        imageView0 = new QGraphicsView(gridLayoutWidget);
        imageView0->setObjectName(QString::fromUtf8("imageView0"));
        imageView0->setMinimumSize(QSize(1280, 140));
        imageView0->setMaximumSize(QSize(16777215, 150));

        gridLayout->addWidget(imageView0, 0, 1, 1, 1);

        frame_2 = new QFrame(gridLayoutWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(200, 0));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_3 = new QWidget(frame_2);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(-1, 9, 201, 131));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        groupBoxGain1 = new QGroupBox(gridLayoutWidget_3);
        groupBoxGain1->setObjectName(QString::fromUtf8("groupBoxGain1"));

        gridLayout_3->addWidget(groupBoxGain1, 0, 0, 1, 1);

        groupBoxOffset1 = new QGroupBox(gridLayoutWidget_3);
        groupBoxOffset1->setObjectName(QString::fromUtf8("groupBoxOffset1"));

        gridLayout_3->addWidget(groupBoxOffset1, 0, 1, 1, 1);

        groupBoxStop1 = new QGroupBox(gridLayoutWidget_3);
        groupBoxStop1->setObjectName(QString::fromUtf8("groupBoxStop1"));

        gridLayout_3->addWidget(groupBoxStop1, 1, 1, 1, 1);

        groupBoxStart1 = new QGroupBox(gridLayoutWidget_3);
        groupBoxStart1->setObjectName(QString::fromUtf8("groupBoxStart1"));

        gridLayout_3->addWidget(groupBoxStart1, 1, 0, 1, 1);


        gridLayout->addWidget(frame_2, 2, 3, 1, 1);

        imageView4 = new QGraphicsView(gridLayoutWidget);
        imageView4->setObjectName(QString::fromUtf8("imageView4"));
        imageView4->setMinimumSize(QSize(1280, 140));
        imageView4->setMaximumSize(QSize(16777215, 150));

        gridLayout->addWidget(imageView4, 8, 1, 1, 1);

        label_7 = new QLabel(gridLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font1);

        gridLayout->addWidget(label_7, 10, 0, 1, 1);

        imageView3 = new QGraphicsView(gridLayoutWidget);
        imageView3->setObjectName(QString::fromUtf8("imageView3"));
        imageView3->setMinimumSize(QSize(1280, 140));
        imageView3->setMaximumSize(QSize(16777215, 150));

        gridLayout->addWidget(imageView3, 6, 1, 1, 1);

        verticalSpacer_5 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_5, 9, 1, 1, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);

        gridLayout->addWidget(label_4, 4, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 2, 1, 1);

        frame_4 = new QFrame(gridLayoutWidget);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setMinimumSize(QSize(200, 0));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_5 = new QWidget(frame_4);
        gridLayoutWidget_5->setObjectName(QString::fromUtf8("gridLayoutWidget_5"));
        gridLayoutWidget_5->setGeometry(QRect(-1, 9, 201, 131));
        gridLayout_5 = new QGridLayout(gridLayoutWidget_5);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        groupBoxGain3 = new QGroupBox(gridLayoutWidget_5);
        groupBoxGain3->setObjectName(QString::fromUtf8("groupBoxGain3"));

        gridLayout_5->addWidget(groupBoxGain3, 0, 0, 1, 1);

        groupBoxOffset3 = new QGroupBox(gridLayoutWidget_5);
        groupBoxOffset3->setObjectName(QString::fromUtf8("groupBoxOffset3"));

        gridLayout_5->addWidget(groupBoxOffset3, 0, 1, 1, 1);

        groupBoxStop3 = new QGroupBox(gridLayoutWidget_5);
        groupBoxStop3->setObjectName(QString::fromUtf8("groupBoxStop3"));

        gridLayout_5->addWidget(groupBoxStop3, 1, 1, 1, 1);

        groupBoxStart3 = new QGroupBox(gridLayoutWidget_5);
        groupBoxStart3->setObjectName(QString::fromUtf8("groupBoxStart3"));

        gridLayout_5->addWidget(groupBoxStart3, 1, 0, 1, 1);


        gridLayout->addWidget(frame_4, 6, 3, 1, 1);

        imageView5 = new QGraphicsView(gridLayoutWidget);
        imageView5->setObjectName(QString::fromUtf8("imageView5"));
        imageView5->setMinimumSize(QSize(1280, 140));
        imageView5->setMaximumSize(QSize(16777215, 150));

        gridLayout->addWidget(imageView5, 10, 1, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_3, 5, 1, 1, 1);

        frame_5 = new QFrame(gridLayoutWidget);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setMinimumSize(QSize(200, 0));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_6 = new QWidget(frame_5);
        gridLayoutWidget_6->setObjectName(QString::fromUtf8("gridLayoutWidget_6"));
        gridLayoutWidget_6->setGeometry(QRect(-1, 9, 201, 131));
        gridLayout_6 = new QGridLayout(gridLayoutWidget_6);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        groupBoxGain4 = new QGroupBox(gridLayoutWidget_6);
        groupBoxGain4->setObjectName(QString::fromUtf8("groupBoxGain4"));

        gridLayout_6->addWidget(groupBoxGain4, 0, 0, 1, 1);

        groupBoxOffset4 = new QGroupBox(gridLayoutWidget_6);
        groupBoxOffset4->setObjectName(QString::fromUtf8("groupBoxOffset4"));

        gridLayout_6->addWidget(groupBoxOffset4, 0, 1, 1, 1);

        groupBoxStop4 = new QGroupBox(gridLayoutWidget_6);
        groupBoxStop4->setObjectName(QString::fromUtf8("groupBoxStop4"));

        gridLayout_6->addWidget(groupBoxStop4, 1, 1, 1, 1);

        groupBoxStart4 = new QGroupBox(gridLayoutWidget_6);
        groupBoxStart4->setObjectName(QString::fromUtf8("groupBoxStart4"));

        gridLayout_6->addWidget(groupBoxStart4, 1, 0, 1, 1);


        gridLayout->addWidget(frame_5, 8, 3, 1, 1);

        frame_3 = new QFrame(gridLayoutWidget);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(200, 0));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_4 = new QWidget(frame_3);
        gridLayoutWidget_4->setObjectName(QString::fromUtf8("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(-1, 9, 201, 131));
        gridLayout_4 = new QGridLayout(gridLayoutWidget_4);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        groupBoxGain2 = new QGroupBox(gridLayoutWidget_4);
        groupBoxGain2->setObjectName(QString::fromUtf8("groupBoxGain2"));

        gridLayout_4->addWidget(groupBoxGain2, 0, 0, 1, 1);

        groupBoxOffset2 = new QGroupBox(gridLayoutWidget_4);
        groupBoxOffset2->setObjectName(QString::fromUtf8("groupBoxOffset2"));

        gridLayout_4->addWidget(groupBoxOffset2, 0, 1, 1, 1);

        groupBoxStop2 = new QGroupBox(gridLayoutWidget_4);
        groupBoxStop2->setObjectName(QString::fromUtf8("groupBoxStop2"));

        gridLayout_4->addWidget(groupBoxStop2, 1, 1, 1, 1);

        groupBoxStart2 = new QGroupBox(gridLayoutWidget_4);
        groupBoxStart2->setObjectName(QString::fromUtf8("groupBoxStart2"));

        gridLayout_4->addWidget(groupBoxStart2, 1, 0, 1, 1);


        gridLayout->addWidget(frame_3, 4, 3, 1, 1);

        imageView1 = new QGraphicsView(gridLayoutWidget);
        imageView1->setObjectName(QString::fromUtf8("imageView1"));
        imageView1->setMinimumSize(QSize(1280, 140));
        imageView1->setMaximumSize(QSize(16777215, 150));

        gridLayout->addWidget(imageView1, 2, 1, 1, 1);

        imageView2 = new QGraphicsView(gridLayoutWidget);
        imageView2->setObjectName(QString::fromUtf8("imageView2"));
        imageView2->setMinimumSize(QSize(1280, 140));
        imageView2->setMaximumSize(QSize(16777215, 150));

        gridLayout->addWidget(imageView2, 4, 1, 1, 1);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font1);

        gridLayout->addWidget(label_6, 8, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer, 1, 1, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font1);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_4, 7, 1, 1, 1);

        frame_6 = new QFrame(gridLayoutWidget);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setMinimumSize(QSize(200, 0));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        gridLayoutWidget_7 = new QWidget(frame_6);
        gridLayoutWidget_7->setObjectName(QString::fromUtf8("gridLayoutWidget_7"));
        gridLayoutWidget_7->setGeometry(QRect(-1, 9, 201, 131));
        gridLayout_7 = new QGridLayout(gridLayoutWidget_7);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_7->setContentsMargins(0, 0, 0, 0);
        groupBoxStop5 = new QGroupBox(gridLayoutWidget_7);
        groupBoxStop5->setObjectName(QString::fromUtf8("groupBoxStop5"));

        gridLayout_7->addWidget(groupBoxStop5, 1, 1, 1, 1);

        groupBoxStart5 = new QGroupBox(gridLayoutWidget_7);
        groupBoxStart5->setObjectName(QString::fromUtf8("groupBoxStart5"));

        gridLayout_7->addWidget(groupBoxStart5, 1, 0, 1, 1);

        groupBoxOffset5 = new QGroupBox(gridLayoutWidget_7);
        groupBoxOffset5->setObjectName(QString::fromUtf8("groupBoxOffset5"));

        gridLayout_7->addWidget(groupBoxOffset5, 0, 1, 1, 1);

        groupBoxGain5 = new QGroupBox(gridLayoutWidget_7);
        groupBoxGain5->setObjectName(QString::fromUtf8("groupBoxGain5"));

        gridLayout_7->addWidget(groupBoxGain5, 0, 0, 1, 1);


        gridLayout->addWidget(frame_6, 10, 3, 1, 1);


        retranslateUi(DMS);

        QMetaObject::connectSlotsByName(DMS);
    } // setupUi

    void retranslateUi(QWidget *DMS)
    {
        DMS->setWindowTitle(QCoreApplication::translate("DMS", "DMS", nullptr));
        buttonLive->setText(QCoreApplication::translate("DMS", "ENABLE LIVE", nullptr));
        labelTakt->setText(QCoreApplication::translate("DMS", "TextLabel", nullptr));
        label_2->setText(QCoreApplication::translate("DMS", "Example: R[0-63]-[REG_VALUE];", nullptr));
        buttonSend->setText(QCoreApplication::translate("DMS", "Send Command", nullptr));
        buttonShowDowel->setText(QCoreApplication::translate("DMS", "Send Command", nullptr));
        groupBoxGain0->setTitle(QCoreApplication::translate("DMS", "GAIN", nullptr));
        groupBoxOffset0->setTitle(QCoreApplication::translate("DMS", "OFFSET", nullptr));
        groupBoxStop0->setTitle(QCoreApplication::translate("DMS", "STOP DETECT", nullptr));
        groupBoxStart0->setTitle(QCoreApplication::translate("DMS", "START DETECT", nullptr));
        label_5->setText(QCoreApplication::translate("DMS", "CAM 3", nullptr));
        groupBoxGain1->setTitle(QCoreApplication::translate("DMS", "GAIN", nullptr));
        groupBoxOffset1->setTitle(QCoreApplication::translate("DMS", "OFFSET", nullptr));
        groupBoxStop1->setTitle(QCoreApplication::translate("DMS", "STOP DETECT", nullptr));
        groupBoxStart1->setTitle(QCoreApplication::translate("DMS", "START DETECT", nullptr));
        label_7->setText(QCoreApplication::translate("DMS", "CAM 5", nullptr));
        label_4->setText(QCoreApplication::translate("DMS", "CAM 2", nullptr));
        groupBoxGain3->setTitle(QCoreApplication::translate("DMS", "GAIN", nullptr));
        groupBoxOffset3->setTitle(QCoreApplication::translate("DMS", "OFFSET", nullptr));
        groupBoxStop3->setTitle(QCoreApplication::translate("DMS", "STOP DETECT", nullptr));
        groupBoxStart3->setTitle(QCoreApplication::translate("DMS", "START DETECT", nullptr));
        groupBoxGain4->setTitle(QCoreApplication::translate("DMS", "GAIN", nullptr));
        groupBoxOffset4->setTitle(QCoreApplication::translate("DMS", "OFFSET", nullptr));
        groupBoxStop4->setTitle(QCoreApplication::translate("DMS", "STOP DETECT", nullptr));
        groupBoxStart4->setTitle(QCoreApplication::translate("DMS", "START DETECT", nullptr));
        groupBoxGain2->setTitle(QCoreApplication::translate("DMS", "GAIN", nullptr));
        groupBoxOffset2->setTitle(QCoreApplication::translate("DMS", "OFFSET", nullptr));
        groupBoxStop2->setTitle(QCoreApplication::translate("DMS", "STOP DETECT", nullptr));
        groupBoxStart2->setTitle(QCoreApplication::translate("DMS", "START DETECT", nullptr));
        label_6->setText(QCoreApplication::translate("DMS", "CAM 4", nullptr));
        label_3->setText(QCoreApplication::translate("DMS", "CAM 1", nullptr));
        label->setText(QCoreApplication::translate("DMS", "CAM 0", nullptr));
        groupBoxStop5->setTitle(QCoreApplication::translate("DMS", "STOP DETECT", nullptr));
        groupBoxStart5->setTitle(QCoreApplication::translate("DMS", "START DETECT", nullptr));
        groupBoxOffset5->setTitle(QCoreApplication::translate("DMS", "OFFSET", nullptr));
        groupBoxGain5->setTitle(QCoreApplication::translate("DMS", "GAIN", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DMS: public Ui_DMS {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DMS_H
