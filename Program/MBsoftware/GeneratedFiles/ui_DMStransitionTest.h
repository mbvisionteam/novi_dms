/********************************************************************************
** Form generated from reading UI file 'DMStransitionTest.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DMSTRANSITIONTEST_H
#define UI_DMSTRANSITIONTEST_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DMSTestWindow
{
public:
    QPushButton *buttonEnableTest;
    QPushButton *buttonGetTimeLine;
    QGraphicsView *timeLineView;
    QFrame *frame;
    QGraphicsView *dowelView;
    QComboBox *comboBoxSelectCamera;
    QPushButton *buttonHistory;
    QPushButton *buttonHistoryUp;
    QPushButton *buttonHistoryLast;
    QFrame *frame_2;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkShowArea;
    QCheckBox *checkShowKonus;
    QCheckBox *checkShowBoarders;
    QCheckBox *checkShowDowel;
    QCheckBox *checkShowMeasurements;
    QFrame *frame_3;
    QGraphicsView *horizontalView;
    QPushButton *buttonHorCamUp;
    QPushButton *buttonHorCamDown;
    QFrame *frame_4;
    QLabel *labelHistoryPieceInHistory;
    QLabel *labelHistoryShown;
    QLabel *labelHistoryShown_4;
    QLabel *labelHistoryShown_3;
    QFrame *frame_5;
    QLabel *labelHistoryIsGood;
    QListWidget *list;
    QFrame *frame_6;
    QGraphicsView *viewDowelBlow;
    QPushButton *buttonFrameUp;
    QPushButton *buttonFrameDown;
    QScrollArea *scrollAreaMeasurements;
    QWidget *scrollAreaWidgetContents;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *layoutMeasurements;
    QGraphicsView *graphicsView;

    void setupUi(QWidget *DMSTestWindow)
    {
        if (DMSTestWindow->objectName().isEmpty())
            DMSTestWindow->setObjectName(QString::fromUtf8("DMSTestWindow"));
        DMSTestWindow->resize(1920, 900);
        buttonEnableTest = new QPushButton(DMSTestWindow);
        buttonEnableTest->setObjectName(QString::fromUtf8("buttonEnableTest"));
        buttonEnableTest->setGeometry(QRect(330, 840, 151, 31));
        buttonGetTimeLine = new QPushButton(DMSTestWindow);
        buttonGetTimeLine->setObjectName(QString::fromUtf8("buttonGetTimeLine"));
        buttonGetTimeLine->setGeometry(QRect(0, 850, 151, 31));
        timeLineView = new QGraphicsView(DMSTestWindow);
        timeLineView->setObjectName(QString::fromUtf8("timeLineView"));
        timeLineView->setGeometry(QRect(130, 850, 191, 31));
        frame = new QFrame(DMSTestWindow);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(590, 10, 941, 511));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        dowelView = new QGraphicsView(frame);
        dowelView->setObjectName(QString::fromUtf8("dowelView"));
        dowelView->setGeometry(QRect(10, 20, 921, 481));
        comboBoxSelectCamera = new QComboBox(DMSTestWindow);
        comboBoxSelectCamera->setObjectName(QString::fromUtf8("comboBoxSelectCamera"));
        comboBoxSelectCamera->setGeometry(QRect(1540, 280, 211, 22));
        buttonHistory = new QPushButton(DMSTestWindow);
        buttonHistory->setObjectName(QString::fromUtf8("buttonHistory"));
        buttonHistory->setGeometry(QRect(1540, 170, 101, 61));
        buttonHistoryUp = new QPushButton(DMSTestWindow);
        buttonHistoryUp->setObjectName(QString::fromUtf8("buttonHistoryUp"));
        buttonHistoryUp->setGeometry(QRect(1650, 170, 101, 61));
        buttonHistoryLast = new QPushButton(DMSTestWindow);
        buttonHistoryLast->setObjectName(QString::fromUtf8("buttonHistoryLast"));
        buttonHistoryLast->setGeometry(QRect(1540, 240, 211, 31));
        frame_2 = new QFrame(DMSTestWindow);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(1540, 350, 211, 171));
        frame_2->setFrameShape(QFrame::Box);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget_2 = new QWidget(frame_2);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 10, 191, 111));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        checkShowArea = new QCheckBox(verticalLayoutWidget_2);
        checkShowArea->setObjectName(QString::fromUtf8("checkShowArea"));

        verticalLayout_2->addWidget(checkShowArea);

        checkShowKonus = new QCheckBox(verticalLayoutWidget_2);
        checkShowKonus->setObjectName(QString::fromUtf8("checkShowKonus"));

        verticalLayout_2->addWidget(checkShowKonus);

        checkShowBoarders = new QCheckBox(verticalLayoutWidget_2);
        checkShowBoarders->setObjectName(QString::fromUtf8("checkShowBoarders"));

        verticalLayout_2->addWidget(checkShowBoarders);

        checkShowDowel = new QCheckBox(verticalLayoutWidget_2);
        checkShowDowel->setObjectName(QString::fromUtf8("checkShowDowel"));

        verticalLayout_2->addWidget(checkShowDowel);

        checkShowMeasurements = new QCheckBox(verticalLayoutWidget_2);
        checkShowMeasurements->setObjectName(QString::fromUtf8("checkShowMeasurements"));

        verticalLayout_2->addWidget(checkShowMeasurements);

        frame_3 = new QFrame(DMSTestWindow);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setGeometry(QRect(590, 530, 941, 131));
        frame_3->setFrameShape(QFrame::Box);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalView = new QGraphicsView(frame_3);
        horizontalView->setObjectName(QString::fromUtf8("horizontalView"));
        horizontalView->setGeometry(QRect(10, 10, 921, 111));
        buttonHorCamUp = new QPushButton(DMSTestWindow);
        buttonHorCamUp->setObjectName(QString::fromUtf8("buttonHorCamUp"));
        buttonHorCamUp->setGeometry(QRect(1650, 530, 101, 51));
        buttonHorCamDown = new QPushButton(DMSTestWindow);
        buttonHorCamDown->setObjectName(QString::fromUtf8("buttonHorCamDown"));
        buttonHorCamDown->setGeometry(QRect(1540, 530, 101, 51));
        frame_4 = new QFrame(DMSTestWindow);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setGeometry(QRect(1540, 10, 211, 91));
        frame_4->setFrameShape(QFrame::Box);
        frame_4->setFrameShadow(QFrame::Raised);
        labelHistoryPieceInHistory = new QLabel(frame_4);
        labelHistoryPieceInHistory->setObjectName(QString::fromUtf8("labelHistoryPieceInHistory"));
        labelHistoryPieceInHistory->setGeometry(QRect(10, 50, 61, 31));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        labelHistoryPieceInHistory->setFont(font);
        labelHistoryPieceInHistory->setFrameShape(QFrame::Box);
        labelHistoryPieceInHistory->setFrameShadow(QFrame::Raised);
        labelHistoryShown = new QLabel(frame_4);
        labelHistoryShown->setObjectName(QString::fromUtf8("labelHistoryShown"));
        labelHistoryShown->setGeometry(QRect(10, 10, 191, 31));
        labelHistoryShown->setFont(font);
        labelHistoryShown->setFrameShape(QFrame::Box);
        labelHistoryShown->setFrameShadow(QFrame::Raised);
        labelHistoryShown_4 = new QLabel(frame_4);
        labelHistoryShown_4->setObjectName(QString::fromUtf8("labelHistoryShown_4"));
        labelHistoryShown_4->setGeometry(QRect(80, 50, 51, 31));
        labelHistoryShown_4->setFont(font);
        labelHistoryShown_4->setFrameShape(QFrame::NoFrame);
        labelHistoryShown_4->setFrameShadow(QFrame::Raised);
        labelHistoryShown_4->setAlignment(Qt::AlignCenter);
        labelHistoryShown_3 = new QLabel(frame_4);
        labelHistoryShown_3->setObjectName(QString::fromUtf8("labelHistoryShown_3"));
        labelHistoryShown_3->setGeometry(QRect(140, 50, 61, 31));
        labelHistoryShown_3->setFont(font);
        labelHistoryShown_3->setFrameShape(QFrame::Box);
        labelHistoryShown_3->setFrameShadow(QFrame::Raised);
        frame_5 = new QFrame(DMSTestWindow);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setGeometry(QRect(1540, 110, 211, 51));
        frame_5->setFrameShape(QFrame::Box);
        frame_5->setFrameShadow(QFrame::Raised);
        labelHistoryIsGood = new QLabel(frame_5);
        labelHistoryIsGood->setObjectName(QString::fromUtf8("labelHistoryIsGood"));
        labelHistoryIsGood->setGeometry(QRect(10, 10, 191, 31));
        labelHistoryIsGood->setFont(font);
        labelHistoryIsGood->setFrameShape(QFrame::Box);
        labelHistoryIsGood->setFrameShadow(QFrame::Raised);
        labelHistoryIsGood->setAlignment(Qt::AlignCenter);
        list = new QListWidget(DMSTestWindow);
        list->setObjectName(QString::fromUtf8("list"));
        list->setGeometry(QRect(590, 670, 221, 221));
        frame_6 = new QFrame(DMSTestWindow);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setGeometry(QRect(1540, 590, 371, 301));
        frame_6->setFrameShape(QFrame::Box);
        frame_6->setFrameShadow(QFrame::Raised);
        viewDowelBlow = new QGraphicsView(frame_6);
        viewDowelBlow->setObjectName(QString::fromUtf8("viewDowelBlow"));
        viewDowelBlow->setGeometry(QRect(10, 10, 351, 281));
        buttonFrameUp = new QPushButton(DMSTestWindow);
        buttonFrameUp->setObjectName(QString::fromUtf8("buttonFrameUp"));
        buttonFrameUp->setGeometry(QRect(1430, 710, 101, 51));
        buttonFrameDown = new QPushButton(DMSTestWindow);
        buttonFrameDown->setObjectName(QString::fromUtf8("buttonFrameDown"));
        buttonFrameDown->setGeometry(QRect(1430, 770, 101, 51));
        scrollAreaMeasurements = new QScrollArea(DMSTestWindow);
        scrollAreaMeasurements->setObjectName(QString::fromUtf8("scrollAreaMeasurements"));
        scrollAreaMeasurements->setGeometry(QRect(820, 670, 591, 221));
        scrollAreaMeasurements->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 589, 219));
        scrollAreaMeasurements->setWidget(scrollAreaWidgetContents);
        verticalLayoutWidget = new QWidget(DMSTestWindow);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 571, 511));
        layoutMeasurements = new QVBoxLayout(verticalLayoutWidget);
        layoutMeasurements->setObjectName(QString::fromUtf8("layoutMeasurements"));
        layoutMeasurements->setContentsMargins(0, 0, 0, 0);
        graphicsView = new QGraphicsView(DMSTestWindow);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        graphicsView->setGeometry(QRect(10, 530, 571, 301));

        retranslateUi(DMSTestWindow);

        QMetaObject::connectSlotsByName(DMSTestWindow);
    } // setupUi

    void retranslateUi(QWidget *DMSTestWindow)
    {
        DMSTestWindow->setWindowTitle(QCoreApplication::translate("DMSTestWindow", "Form", nullptr));
        buttonEnableTest->setText(QCoreApplication::translate("DMSTestWindow", "Send Test Dowel", nullptr));
        buttonGetTimeLine->setText(QCoreApplication::translate("DMSTestWindow", "Get Time Line", nullptr));
        buttonHistory->setText(QCoreApplication::translate("DMSTestWindow", "HISTORY -", nullptr));
        buttonHistoryUp->setText(QCoreApplication::translate("DMSTestWindow", "HISTORY + ", nullptr));
        buttonHistoryLast->setText(QCoreApplication::translate("DMSTestWindow", "SHOW LAST PIECE", nullptr));
        checkShowArea->setText(QCoreApplication::translate("DMSTestWindow", "Show Measurement Areas", nullptr));
        checkShowKonus->setText(QCoreApplication::translate("DMSTestWindow", "Show Conus Lines", nullptr));
        checkShowBoarders->setText(QCoreApplication::translate("DMSTestWindow", "Show Tolerance Area", nullptr));
        checkShowDowel->setText(QCoreApplication::translate("DMSTestWindow", "Show Dowel", nullptr));
        checkShowMeasurements->setText(QCoreApplication::translate("DMSTestWindow", "Show Measurements ", nullptr));
        buttonHorCamUp->setText(QCoreApplication::translate("DMSTestWindow", "Hor. Cam Frame+", nullptr));
        buttonHorCamDown->setText(QCoreApplication::translate("DMSTestWindow", "Hor. Cam Frame-", nullptr));
        labelHistoryPieceInHistory->setText(QCoreApplication::translate("DMSTestWindow", "X", nullptr));
        labelHistoryShown->setText(QCoreApplication::translate("DMSTestWindow", "Dowel Shown:", nullptr));
        labelHistoryShown_4->setText(QCoreApplication::translate("DMSTestWindow", "/", nullptr));
        labelHistoryShown_3->setText(QCoreApplication::translate("DMSTestWindow", "128", nullptr));
        labelHistoryIsGood->setText(QCoreApplication::translate("DMSTestWindow", "IS GOOD", nullptr));
        buttonFrameUp->setText(QCoreApplication::translate("DMSTestWindow", "Frame+", nullptr));
        buttonFrameDown->setText(QCoreApplication::translate("DMSTestWindow", "Frame-", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DMSTestWindow: public Ui_DMSTestWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DMSTRANSITIONTEST_H
