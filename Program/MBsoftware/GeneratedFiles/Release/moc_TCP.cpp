/****************************************************************************
** Meta object code from reading C++ file 'TCP.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../TCP.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TCP.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TCP_t {
    QByteArrayData data[19];
    char stringdata0[248];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TCP_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TCP_t qt_meta_stringdata_TCP = {
    {
QT_MOC_LITERAL(0, 0, 3), // "TCP"
QT_MOC_LITERAL(1, 4, 17), // "connectionTimeout"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 9), // "dataReady"
QT_MOC_LITERAL(4, 33, 10), // "connection"
QT_MOC_LITERAL(5, 44, 12), // "parseCounter"
QT_MOC_LITERAL(6, 57, 10), // "appendText"
QT_MOC_LITERAL(7, 68, 4), // "text"
QT_MOC_LITERAL(8, 73, 15), // "OnNewConnection"
QT_MOC_LITERAL(9, 89, 19), // "OnConnectionTimeout"
QT_MOC_LITERAL(10, 109, 11), // "SendRcvLoop"
QT_MOC_LITERAL(11, 121, 17), // "OnServerReadyRead"
QT_MOC_LITERAL(12, 139, 17), // "OnClientReadyRead"
QT_MOC_LITERAL(13, 157, 20), // "OnClientReadyReadPLC"
QT_MOC_LITERAL(14, 178, 17), // "OnClientConnected"
QT_MOC_LITERAL(15, 196, 20), // "OnClientDisconnected"
QT_MOC_LITERAL(16, 217, 3), // "get"
QT_MOC_LITERAL(17, 221, 13), // "OnClickedSend"
QT_MOC_LITERAL(18, 235, 12) // "OnAppendText"

    },
    "TCP\0connectionTimeout\0\0dataReady\0"
    "connection\0parseCounter\0appendText\0"
    "text\0OnNewConnection\0OnConnectionTimeout\0"
    "SendRcvLoop\0OnServerReadyRead\0"
    "OnClientReadyRead\0OnClientReadyReadPLC\0"
    "OnClientConnected\0OnClientDisconnected\0"
    "get\0OnClickedSend\0OnAppendText"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TCP[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x06 /* Public */,
       3,    2,   85,    2, 0x06 /* Public */,
       6,    1,   90,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   93,    2, 0x0a /* Public */,
       9,    0,   94,    2, 0x0a /* Public */,
      10,    0,   95,    2, 0x0a /* Public */,
      11,    1,   96,    2, 0x0a /* Public */,
      12,    0,   99,    2, 0x0a /* Public */,
      13,    0,  100,    2, 0x0a /* Public */,
      14,    0,  101,    2, 0x0a /* Public */,
      15,    0,  102,    2, 0x0a /* Public */,
      15,    1,  103,    2, 0x0a /* Public */,
      17,    0,  106,    2, 0x0a /* Public */,
      18,    1,  107,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    4,    5,
    QMetaType::Void, QMetaType::QString,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   16,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,

       0        // eod
};

void TCP::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TCP *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connectionTimeout(); break;
        case 1: _t->dataReady((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->appendText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->OnNewConnection(); break;
        case 4: _t->OnConnectionTimeout(); break;
        case 5: _t->SendRcvLoop(); break;
        case 6: _t->OnServerReadyRead((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->OnClientReadyRead(); break;
        case 8: _t->OnClientReadyReadPLC(); break;
        case 9: _t->OnClientConnected(); break;
        case 10: _t->OnClientDisconnected(); break;
        case 11: _t->OnClientDisconnected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->OnClickedSend(); break;
        case 13: _t->OnAppendText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TCP::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCP::connectionTimeout)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TCP::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCP::dataReady)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (TCP::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCP::appendText)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TCP::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_TCP.data,
    qt_meta_data_TCP,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TCP::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TCP::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TCP.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int TCP::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void TCP::connectionTimeout()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void TCP::dataReady(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TCP::appendText(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
