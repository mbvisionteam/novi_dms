/****************************************************************************
** Meta object code from reading C++ file 'dowelSettings.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../dowelSettings.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dowelSettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DowelSettings_t {
    QByteArrayData data[38];
    char stringdata0[652];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DowelSettings_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DowelSettings_t qt_meta_stringdata_DowelSettings = {
    {
QT_MOC_LITERAL(0, 0, 13), // "DowelSettings"
QT_MOC_LITERAL(1, 14, 15), // "newTypeSelected"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 12), // "selectedType"
QT_MOC_LITERAL(4, 44, 21), // "advancedSettingsSaved"
QT_MOC_LITERAL(5, 66, 8), // "selected"
QT_MOC_LITERAL(6, 75, 19), // "createNewTypeSignal"
QT_MOC_LITERAL(7, 95, 7), // "existed"
QT_MOC_LITERAL(8, 103, 7), // "newName"
QT_MOC_LITERAL(9, 111, 10), // "deleteType"
QT_MOC_LITERAL(10, 122, 4), // "name"
QT_MOC_LITERAL(11, 127, 11), // "OnClickedOk"
QT_MOC_LITERAL(12, 139, 15), // "OnClickedCancel"
QT_MOC_LITERAL(13, 155, 17), // "SpinLenghtChanged"
QT_MOC_LITERAL(14, 173, 5), // "value"
QT_MOC_LITERAL(15, 179, 16), // "SpinWidthChanged"
QT_MOC_LITERAL(16, 196, 16), // "SpinKonusChanged"
QT_MOC_LITERAL(17, 213, 20), // "SpinLenghtMaxChanged"
QT_MOC_LITERAL(18, 234, 19), // "SpinWidthMaxChanged"
QT_MOC_LITERAL(19, 254, 19), // "SpinKonusMaxChanged"
QT_MOC_LITERAL(20, 274, 21), // "OnClickRadioWithKonus"
QT_MOC_LITERAL(21, 296, 24), // "OnClickRadioWithoutKonus"
QT_MOC_LITERAL(22, 321, 11), // "TypeChanged"
QT_MOC_LITERAL(23, 333, 17), // "OnClickSelectType"
QT_MOC_LITERAL(24, 351, 29), // "OnClickSelectAdvancedSettings"
QT_MOC_LITERAL(25, 381, 27), // "OnClickSaveAdvancedSettings"
QT_MOC_LITERAL(26, 409, 17), // "SaveChangesToType"
QT_MOC_LITERAL(27, 427, 13), // "IsTypeChanged"
QT_MOC_LITERAL(28, 441, 13), // "OnSaveChanges"
QT_MOC_LITERAL(29, 455, 16), // "CreateAdvDisplay"
QT_MOC_LITERAL(30, 472, 22), // "ReplaceAdvanceSettings"
QT_MOC_LITERAL(31, 495, 15), // "advSettingsName"
QT_MOC_LITERAL(32, 511, 32), // "OnClickCreateNewAdvancedSettings"
QT_MOC_LITERAL(33, 544, 15), // "CopyAdvSettings"
QT_MOC_LITERAL(34, 560, 31), // "OnClickedDeleteAdvancedSettings"
QT_MOC_LITERAL(35, 592, 22), // "DeleteAdvancedSettings"
QT_MOC_LITERAL(36, 615, 19), // "OnClickedDeleteType"
QT_MOC_LITERAL(37, 635, 16) // "OnClickedNewType"

    },
    "DowelSettings\0newTypeSelected\0\0"
    "selectedType\0advancedSettingsSaved\0"
    "selected\0createNewTypeSignal\0existed\0"
    "newName\0deleteType\0name\0OnClickedOk\0"
    "OnClickedCancel\0SpinLenghtChanged\0"
    "value\0SpinWidthChanged\0SpinKonusChanged\0"
    "SpinLenghtMaxChanged\0SpinWidthMaxChanged\0"
    "SpinKonusMaxChanged\0OnClickRadioWithKonus\0"
    "OnClickRadioWithoutKonus\0TypeChanged\0"
    "OnClickSelectType\0OnClickSelectAdvancedSettings\0"
    "OnClickSaveAdvancedSettings\0"
    "SaveChangesToType\0IsTypeChanged\0"
    "OnSaveChanges\0CreateAdvDisplay\0"
    "ReplaceAdvanceSettings\0advSettingsName\0"
    "OnClickCreateNewAdvancedSettings\0"
    "CopyAdvSettings\0OnClickedDeleteAdvancedSettings\0"
    "DeleteAdvancedSettings\0OnClickedDeleteType\0"
    "OnClickedNewType"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DowelSettings[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      29,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  159,    2, 0x06 /* Public */,
       4,    1,  162,    2, 0x06 /* Public */,
       6,    2,  165,    2, 0x06 /* Public */,
       9,    1,  170,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,  173,    2, 0x0a /* Public */,
      12,    0,  174,    2, 0x0a /* Public */,
      13,    1,  175,    2, 0x0a /* Public */,
      15,    1,  178,    2, 0x0a /* Public */,
      16,    1,  181,    2, 0x0a /* Public */,
      17,    0,  184,    2, 0x0a /* Public */,
      18,    0,  185,    2, 0x0a /* Public */,
      19,    0,  186,    2, 0x0a /* Public */,
      20,    0,  187,    2, 0x0a /* Public */,
      21,    0,  188,    2, 0x0a /* Public */,
      22,    0,  189,    2, 0x0a /* Public */,
      23,    0,  190,    2, 0x0a /* Public */,
      24,    0,  191,    2, 0x0a /* Public */,
      25,    0,  192,    2, 0x0a /* Public */,
      26,    0,  193,    2, 0x0a /* Public */,
      27,    0,  194,    2, 0x0a /* Public */,
      28,    0,  195,    2, 0x0a /* Public */,
      29,    0,  196,    2, 0x0a /* Public */,
      30,    1,  197,    2, 0x0a /* Public */,
      32,    0,  200,    2, 0x0a /* Public */,
      33,    2,  201,    2, 0x0a /* Public */,
      34,    0,  206,    2, 0x0a /* Public */,
      35,    1,  207,    2, 0x0a /* Public */,
      36,    0,  210,    2, 0x0a /* Public */,
      37,    0,  211,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    7,    8,
    QMetaType::Void, QMetaType::QString,   10,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   14,
    QMetaType::Void, QMetaType::Double,   14,
    QMetaType::Void, QMetaType::Double,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::Int,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   31,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    7,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void DowelSettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DowelSettings *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newTypeSelected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->advancedSettingsSaved((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->createNewTypeSignal((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->deleteType((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->OnClickedOk(); break;
        case 5: _t->OnClickedCancel(); break;
        case 6: _t->SpinLenghtChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->SpinWidthChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->SpinKonusChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->SpinLenghtMaxChanged(); break;
        case 10: _t->SpinWidthMaxChanged(); break;
        case 11: _t->SpinKonusMaxChanged(); break;
        case 12: _t->OnClickRadioWithKonus(); break;
        case 13: _t->OnClickRadioWithoutKonus(); break;
        case 14: _t->TypeChanged(); break;
        case 15: _t->OnClickSelectType(); break;
        case 16: _t->OnClickSelectAdvancedSettings(); break;
        case 17: _t->OnClickSaveAdvancedSettings(); break;
        case 18: _t->SaveChangesToType(); break;
        case 19: { bool _r = _t->IsTypeChanged();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 20: { int _r = _t->OnSaveChanges();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 21: _t->CreateAdvDisplay(); break;
        case 22: _t->ReplaceAdvanceSettings((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 23: _t->OnClickCreateNewAdvancedSettings(); break;
        case 24: _t->CopyAdvSettings((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 25: _t->OnClickedDeleteAdvancedSettings(); break;
        case 26: _t->DeleteAdvancedSettings((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 27: _t->OnClickedDeleteType(); break;
        case 28: _t->OnClickedNewType(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DowelSettings::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DowelSettings::newTypeSelected)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (DowelSettings::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DowelSettings::advancedSettingsSaved)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (DowelSettings::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DowelSettings::createNewTypeSignal)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (DowelSettings::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DowelSettings::deleteType)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DowelSettings::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_DowelSettings.data,
    qt_meta_data_DowelSettings,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DowelSettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DowelSettings::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DowelSettings.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int DowelSettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 29)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 29;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 29)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 29;
    }
    return _id;
}

// SIGNAL 0
void DowelSettings::newTypeSelected(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DowelSettings::advancedSettingsSaved(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void DowelSettings::createNewTypeSignal(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void DowelSettings::deleteType(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
