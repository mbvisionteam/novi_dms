/****************************************************************************
** Meta object code from reading C++ file 'imageProcessing.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../imageProcessing.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'imageProcessing.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_imageProcessing_t {
    QByteArrayData data[91];
    char stringdata0[1665];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_imageProcessing_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_imageProcessing_t qt_meta_stringdata_imageProcessing = {
    {
QT_MOC_LITERAL(0, 0, 15), // "imageProcessing"
QT_MOC_LITERAL(1, 16, 11), // "imagesReady"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 18), // "measurePieceSignal"
QT_MOC_LITERAL(4, 48, 5), // "nrCam"
QT_MOC_LITERAL(5, 54, 10), // "imageIndex"
QT_MOC_LITERAL(6, 65, 16), // "OnPressedImageUp"
QT_MOC_LITERAL(7, 82, 18), // "OnPressedImageDown"
QT_MOC_LITERAL(8, 101, 14), // "OnPressedCamUp"
QT_MOC_LITERAL(9, 116, 16), // "OnPressedCamDown"
QT_MOC_LITERAL(10, 133, 22), // "ConvertImageForDisplay"
QT_MOC_LITERAL(11, 156, 11), // "imageNumber"
QT_MOC_LITERAL(12, 168, 10), // "ShowImages"
QT_MOC_LITERAL(13, 179, 17), // "ResizeDisplayRect"
QT_MOC_LITERAL(14, 197, 13), // "OnProcessCam0"
QT_MOC_LITERAL(15, 211, 13), // "OnProcessCam1"
QT_MOC_LITERAL(16, 225, 13), // "OnProcessCam2"
QT_MOC_LITERAL(17, 239, 13), // "OnProcessCam3"
QT_MOC_LITERAL(18, 253, 13), // "OnProcessCam4"
QT_MOC_LITERAL(19, 267, 33), // "OnDoneEditingLineInsertImageI..."
QT_MOC_LITERAL(20, 301, 21), // "onPressedAddParameter"
QT_MOC_LITERAL(21, 323, 25), // "onPressedRemoveParameters"
QT_MOC_LITERAL(22, 349, 25), // "onPressedUpdateParameters"
QT_MOC_LITERAL(23, 375, 26), // "onPressedSurfaceInspection"
QT_MOC_LITERAL(24, 402, 21), // "OnPressedCreateRubber"
QT_MOC_LITERAL(25, 424, 21), // "OnPressedResizeRubber"
QT_MOC_LITERAL(26, 446, 21), // "OnPressedSelectRubber"
QT_MOC_LITERAL(27, 468, 30), // "OnPressedTestIntensityFunction"
QT_MOC_LITERAL(28, 499, 11), // "OnViewTimer"
QT_MOC_LITERAL(29, 511, 17), // "ProcessingCamera0"
QT_MOC_LITERAL(30, 529, 2), // "id"
QT_MOC_LITERAL(31, 532, 4), // "draw"
QT_MOC_LITERAL(32, 537, 17), // "ProcessingCamera1"
QT_MOC_LITERAL(33, 555, 20), // "GetPresortParameters"
QT_MOC_LITERAL(34, 576, 17), // "ProcessingCamera2"
QT_MOC_LITERAL(35, 594, 17), // "ProcessingCamera3"
QT_MOC_LITERAL(36, 612, 17), // "ProcessingCamera4"
QT_MOC_LITERAL(37, 630, 14), // "CMemoryBuffer*"
QT_MOC_LITERAL(38, 645, 5), // "image"
QT_MOC_LITERAL(39, 651, 12), // "CheckDefects"
QT_MOC_LITERAL(40, 664, 10), // "indexImage"
QT_MOC_LITERAL(41, 675, 7), // "nrPiece"
QT_MOC_LITERAL(42, 683, 21), // "ProcessCameraForBlobs"
QT_MOC_LITERAL(43, 705, 3), // "cam"
QT_MOC_LITERAL(44, 709, 5), // "index"
QT_MOC_LITERAL(45, 715, 30), // "OnSetClassifierTrainImagesPath"
QT_MOC_LITERAL(46, 746, 29), // "OnSetClassifierTestImagesPath"
QT_MOC_LITERAL(47, 776, 9), // "ClearDraw"
QT_MOC_LITERAL(48, 786, 17), // "ClearFunctionList"
QT_MOC_LITERAL(49, 804, 16), // "SetCurrentBuffer"
QT_MOC_LITERAL(50, 821, 10), // "dispWindow"
QT_MOC_LITERAL(51, 832, 9), // "dispImage"
QT_MOC_LITERAL(52, 842, 7), // "ZoomOut"
QT_MOC_LITERAL(53, 850, 6), // "ZoomIn"
QT_MOC_LITERAL(54, 857, 9), // "ZoomReset"
QT_MOC_LITERAL(55, 867, 11), // "OnLoadImage"
QT_MOC_LITERAL(56, 879, 19), // "OnLoadMultipleImage"
QT_MOC_LITERAL(57, 899, 11), // "OnSaveImage"
QT_MOC_LITERAL(58, 911, 17), // "ShowDialogPresort"
QT_MOC_LITERAL(59, 929, 6), // "rights"
QT_MOC_LITERAL(60, 936, 9), // "currPiece"
QT_MOC_LITERAL(61, 946, 18), // "presortCalibration"
QT_MOC_LITERAL(62, 965, 19), // "UpdateHistoryWindow"
QT_MOC_LITERAL(63, 985, 24), // "CreateScenePresortDialog"
QT_MOC_LITERAL(64, 1010, 16), // "ShowImagePresort"
QT_MOC_LITERAL(65, 1027, 16), // "ClearDrawPresort"
QT_MOC_LITERAL(66, 1044, 26), // "ReadBasicParametersPresort"
QT_MOC_LITERAL(67, 1071, 27), // "WriteBasicParametersPresort"
QT_MOC_LITERAL(68, 1099, 19), // "UpdateBasicSettings"
QT_MOC_LITERAL(69, 1119, 26), // "OnClickedSaveBasicSettings"
QT_MOC_LITERAL(70, 1146, 27), // "ReadAdvancedSettingsPresort"
QT_MOC_LITERAL(71, 1174, 28), // "WriteAdvancedSettingsPresort"
QT_MOC_LITERAL(72, 1203, 29), // "SelectAdvancedPresortSettings"
QT_MOC_LITERAL(73, 1233, 21), // "OnClickedHistoryLabel"
QT_MOC_LITERAL(74, 1255, 28), // "OnClickedMeasurePresortDowel"
QT_MOC_LITERAL(75, 1284, 22), // "OnClickedPresortZoomIn"
QT_MOC_LITERAL(76, 1307, 23), // "OnClickedPresortZoomOut"
QT_MOC_LITERAL(77, 1331, 25), // "OnClickedPresortZoomReset"
QT_MOC_LITERAL(78, 1357, 35), // "OnClickedSelectedCalibrationP..."
QT_MOC_LITERAL(79, 1393, 35), // "OnClickedSelectedAdvSettingsP..."
QT_MOC_LITERAL(80, 1429, 30), // "ReadCalibrationSettingsPresort"
QT_MOC_LITERAL(81, 1460, 31), // "WriteCalibrationSettingsPresort"
QT_MOC_LITERAL(82, 1492, 24), // "OnClickedRadioButtonLive"
QT_MOC_LITERAL(83, 1517, 14), // "OnClickedClose"
QT_MOC_LITERAL(84, 1532, 33), // "OnClickedCreateNewPresortSett..."
QT_MOC_LITERAL(85, 1566, 19), // "CopyAdvancedSetting"
QT_MOC_LITERAL(86, 1586, 7), // "existed"
QT_MOC_LITERAL(87, 1594, 7), // "newName"
QT_MOC_LITERAL(88, 1602, 22), // "DeleteAdvancedSettings"
QT_MOC_LITERAL(89, 1625, 8), // "selected"
QT_MOC_LITERAL(90, 1634, 30) // "OnClickedDeletePresortSettings"

    },
    "imageProcessing\0imagesReady\0\0"
    "measurePieceSignal\0nrCam\0imageIndex\0"
    "OnPressedImageUp\0OnPressedImageDown\0"
    "OnPressedCamUp\0OnPressedCamDown\0"
    "ConvertImageForDisplay\0imageNumber\0"
    "ShowImages\0ResizeDisplayRect\0OnProcessCam0\0"
    "OnProcessCam1\0OnProcessCam2\0OnProcessCam3\0"
    "OnProcessCam4\0OnDoneEditingLineInsertImageIndex\0"
    "onPressedAddParameter\0onPressedRemoveParameters\0"
    "onPressedUpdateParameters\0"
    "onPressedSurfaceInspection\0"
    "OnPressedCreateRubber\0OnPressedResizeRubber\0"
    "OnPressedSelectRubber\0"
    "OnPressedTestIntensityFunction\0"
    "OnViewTimer\0ProcessingCamera0\0id\0draw\0"
    "ProcessingCamera1\0GetPresortParameters\0"
    "ProcessingCamera2\0ProcessingCamera3\0"
    "ProcessingCamera4\0CMemoryBuffer*\0image\0"
    "CheckDefects\0indexImage\0nrPiece\0"
    "ProcessCameraForBlobs\0cam\0index\0"
    "OnSetClassifierTrainImagesPath\0"
    "OnSetClassifierTestImagesPath\0ClearDraw\0"
    "ClearFunctionList\0SetCurrentBuffer\0"
    "dispWindow\0dispImage\0ZoomOut\0ZoomIn\0"
    "ZoomReset\0OnLoadImage\0OnLoadMultipleImage\0"
    "OnSaveImage\0ShowDialogPresort\0rights\0"
    "currPiece\0presortCalibration\0"
    "UpdateHistoryWindow\0CreateScenePresortDialog\0"
    "ShowImagePresort\0ClearDrawPresort\0"
    "ReadBasicParametersPresort\0"
    "WriteBasicParametersPresort\0"
    "UpdateBasicSettings\0OnClickedSaveBasicSettings\0"
    "ReadAdvancedSettingsPresort\0"
    "WriteAdvancedSettingsPresort\0"
    "SelectAdvancedPresortSettings\0"
    "OnClickedHistoryLabel\0"
    "OnClickedMeasurePresortDowel\0"
    "OnClickedPresortZoomIn\0OnClickedPresortZoomOut\0"
    "OnClickedPresortZoomReset\0"
    "OnClickedSelectedCalibrationPresort\0"
    "OnClickedSelectedAdvSettingsPresrot\0"
    "ReadCalibrationSettingsPresort\0"
    "WriteCalibrationSettingsPresort\0"
    "OnClickedRadioButtonLive\0OnClickedClose\0"
    "OnClickedCreateNewPresortSettings\0"
    "CopyAdvancedSetting\0existed\0newName\0"
    "DeleteAdvancedSettings\0selected\0"
    "OnClickedDeletePresortSettings"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_imageProcessing[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      72,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  374,    2, 0x06 /* Public */,
       3,    2,  375,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  380,    2, 0x0a /* Public */,
       7,    0,  381,    2, 0x0a /* Public */,
       8,    0,  382,    2, 0x0a /* Public */,
       9,    0,  383,    2, 0x0a /* Public */,
      10,    1,  384,    2, 0x0a /* Public */,
      12,    0,  387,    2, 0x0a /* Public */,
      13,    0,  388,    2, 0x0a /* Public */,
      14,    0,  389,    2, 0x0a /* Public */,
      15,    0,  390,    2, 0x0a /* Public */,
      16,    0,  391,    2, 0x0a /* Public */,
      17,    0,  392,    2, 0x0a /* Public */,
      18,    0,  393,    2, 0x0a /* Public */,
      19,    0,  394,    2, 0x0a /* Public */,
      20,    0,  395,    2, 0x0a /* Public */,
      21,    0,  396,    2, 0x0a /* Public */,
      22,    0,  397,    2, 0x0a /* Public */,
      23,    0,  398,    2, 0x0a /* Public */,
      24,    0,  399,    2, 0x0a /* Public */,
      25,    0,  400,    2, 0x0a /* Public */,
      26,    0,  401,    2, 0x0a /* Public */,
      27,    0,  402,    2, 0x0a /* Public */,
      28,    0,  403,    2, 0x0a /* Public */,
      29,    3,  404,    2, 0x0a /* Public */,
      32,    3,  411,    2, 0x0a /* Public */,
      33,    1,  418,    2, 0x0a /* Public */,
      34,    3,  421,    2, 0x0a /* Public */,
      35,    3,  428,    2, 0x0a /* Public */,
      36,    3,  435,    2, 0x0a /* Public */,
      35,    2,  442,    2, 0x0a /* Public */,
      32,    2,  447,    2, 0x0a /* Public */,
      39,    3,  452,    2, 0x0a /* Public */,
      42,    3,  459,    2, 0x0a /* Public */,
      45,    0,  466,    2, 0x0a /* Public */,
      46,    0,  467,    2, 0x0a /* Public */,
      47,    0,  468,    2, 0x0a /* Public */,
      48,    0,  469,    2, 0x0a /* Public */,
      49,    2,  470,    2, 0x0a /* Public */,
      52,    0,  475,    2, 0x0a /* Public */,
      53,    0,  476,    2, 0x0a /* Public */,
      54,    0,  477,    2, 0x0a /* Public */,
      55,    0,  478,    2, 0x0a /* Public */,
      56,    0,  479,    2, 0x0a /* Public */,
      57,    0,  480,    2, 0x0a /* Public */,
      58,    3,  481,    2, 0x0a /* Public */,
      62,    0,  488,    2, 0x0a /* Public */,
      63,    0,  489,    2, 0x0a /* Public */,
      64,    1,  490,    2, 0x0a /* Public */,
      65,    0,  493,    2, 0x0a /* Public */,
      66,    0,  494,    2, 0x0a /* Public */,
      67,    0,  495,    2, 0x0a /* Public */,
      68,    0,  496,    2, 0x0a /* Public */,
      69,    0,  497,    2, 0x0a /* Public */,
      70,    0,  498,    2, 0x0a /* Public */,
      71,    1,  499,    2, 0x0a /* Public */,
      72,    0,  502,    2, 0x0a /* Public */,
      73,    1,  503,    2, 0x0a /* Public */,
      74,    0,  506,    2, 0x0a /* Public */,
      75,    0,  507,    2, 0x0a /* Public */,
      76,    0,  508,    2, 0x0a /* Public */,
      77,    0,  509,    2, 0x0a /* Public */,
      78,    0,  510,    2, 0x0a /* Public */,
      79,    0,  511,    2, 0x0a /* Public */,
      80,    0,  512,    2, 0x0a /* Public */,
      81,    1,  513,    2, 0x0a /* Public */,
      82,    0,  516,    2, 0x0a /* Public */,
      83,    0,  517,    2, 0x0a /* Public */,
      84,    0,  518,    2, 0x0a /* Public */,
      85,    2,  519,    2, 0x0a /* Public */,
      88,    1,  524,    2, 0x0a /* Public */,
      90,    0,  527,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    4,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int, QMetaType::Int,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   30,    5,   31,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   30,    5,   31,
    QMetaType::Void, QMetaType::Int,   31,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   30,    5,   31,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   30,    5,   31,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   30,    5,   31,
    QMetaType::Int, 0x80000000 | 37, QMetaType::Int,   38,   31,
    QMetaType::Int, 0x80000000 | 37, QMetaType::Int,   38,   31,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   40,   41,   31,
    QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   43,   44,   31,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   50,   51,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   59,   60,   61,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   44,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   44,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   44,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   44,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   86,   87,
    QMetaType::Void, QMetaType::QString,   89,
    QMetaType::Void,

       0        // eod
};

void imageProcessing::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<imageProcessing *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->imagesReady(); break;
        case 1: _t->measurePieceSignal((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->OnPressedImageUp(); break;
        case 3: _t->OnPressedImageDown(); break;
        case 4: _t->OnPressedCamUp(); break;
        case 5: _t->OnPressedCamDown(); break;
        case 6: { int _r = _t->ConvertImageForDisplay((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 7: _t->ShowImages(); break;
        case 8: _t->ResizeDisplayRect(); break;
        case 9: _t->OnProcessCam0(); break;
        case 10: _t->OnProcessCam1(); break;
        case 11: _t->OnProcessCam2(); break;
        case 12: _t->OnProcessCam3(); break;
        case 13: _t->OnProcessCam4(); break;
        case 14: _t->OnDoneEditingLineInsertImageIndex(); break;
        case 15: _t->onPressedAddParameter(); break;
        case 16: _t->onPressedRemoveParameters(); break;
        case 17: _t->onPressedUpdateParameters(); break;
        case 18: _t->onPressedSurfaceInspection(); break;
        case 19: _t->OnPressedCreateRubber(); break;
        case 20: _t->OnPressedResizeRubber(); break;
        case 21: _t->OnPressedSelectRubber(); break;
        case 22: _t->OnPressedTestIntensityFunction(); break;
        case 23: _t->OnViewTimer(); break;
        case 24: { int _r = _t->ProcessingCamera0((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 25: { int _r = _t->ProcessingCamera1((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 26: _t->GetPresortParameters((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: { int _r = _t->ProcessingCamera2((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 28: { int _r = _t->ProcessingCamera3((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 29: { int _r = _t->ProcessingCamera4((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 30: { int _r = _t->ProcessingCamera3((*reinterpret_cast< CMemoryBuffer*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 31: { int _r = _t->ProcessingCamera1((*reinterpret_cast< CMemoryBuffer*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 32: { int _r = _t->CheckDefects((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 33: { int _r = _t->ProcessCameraForBlobs((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 34: _t->OnSetClassifierTrainImagesPath(); break;
        case 35: _t->OnSetClassifierTestImagesPath(); break;
        case 36: _t->ClearDraw(); break;
        case 37: _t->ClearFunctionList(); break;
        case 38: _t->SetCurrentBuffer((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 39: _t->ZoomOut(); break;
        case 40: _t->ZoomIn(); break;
        case 41: _t->ZoomReset(); break;
        case 42: _t->OnLoadImage(); break;
        case 43: _t->OnLoadMultipleImage(); break;
        case 44: _t->OnSaveImage(); break;
        case 45: _t->ShowDialogPresort((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 46: _t->UpdateHistoryWindow(); break;
        case 47: _t->CreateScenePresortDialog(); break;
        case 48: _t->ShowImagePresort((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 49: _t->ClearDrawPresort(); break;
        case 50: _t->ReadBasicParametersPresort(); break;
        case 51: _t->WriteBasicParametersPresort(); break;
        case 52: _t->UpdateBasicSettings(); break;
        case 53: _t->OnClickedSaveBasicSettings(); break;
        case 54: _t->ReadAdvancedSettingsPresort(); break;
        case 55: _t->WriteAdvancedSettingsPresort((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 56: _t->SelectAdvancedPresortSettings(); break;
        case 57: _t->OnClickedHistoryLabel((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 58: _t->OnClickedMeasurePresortDowel(); break;
        case 59: _t->OnClickedPresortZoomIn(); break;
        case 60: _t->OnClickedPresortZoomOut(); break;
        case 61: _t->OnClickedPresortZoomReset(); break;
        case 62: _t->OnClickedSelectedCalibrationPresort(); break;
        case 63: _t->OnClickedSelectedAdvSettingsPresrot(); break;
        case 64: _t->ReadCalibrationSettingsPresort(); break;
        case 65: _t->WriteCalibrationSettingsPresort((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 66: _t->OnClickedRadioButtonLive(); break;
        case 67: _t->OnClickedClose(); break;
        case 68: _t->OnClickedCreateNewPresortSettings(); break;
        case 69: _t->CopyAdvancedSetting((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 70: _t->DeleteAdvancedSettings((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 71: _t->OnClickedDeletePresortSettings(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (imageProcessing::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&imageProcessing::imagesReady)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (imageProcessing::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&imageProcessing::measurePieceSignal)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject imageProcessing::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_imageProcessing.data,
    qt_meta_data_imageProcessing,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *imageProcessing::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *imageProcessing::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_imageProcessing.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int imageProcessing::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 72)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 72;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 72)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 72;
    }
    return _id;
}

// SIGNAL 0
void imageProcessing::imagesReady()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void imageProcessing::measurePieceSignal(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
