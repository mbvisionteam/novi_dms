/****************************************************************************
** Meta object code from reading C++ file 'DMSudp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../DMSudp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DMSudp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DMSudp_t {
    QByteArrayData data[66];
    char stringdata0[971];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DMSudp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DMSudp_t qt_meta_stringdata_DMSudp = {
    {
QT_MOC_LITERAL(0, 0, 6), // "DMSudp"
QT_MOC_LITERAL(1, 7, 16), // "frameReadySignal"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 2), // "id"
QT_MOC_LITERAL(4, 28, 5), // "image"
QT_MOC_LITERAL(5, 34, 21), // "transitionReadySignal"
QT_MOC_LITERAL(6, 56, 5), // "nrCam"
QT_MOC_LITERAL(7, 62, 9), // "parseData"
QT_MOC_LITERAL(8, 72, 14), // "timeLineSignal"
QT_MOC_LITERAL(9, 87, 13), // "versionSignal"
QT_MOC_LITERAL(10, 101, 7), // "version"
QT_MOC_LITERAL(11, 109, 11), // "OnViewTimer"
QT_MOC_LITERAL(12, 121, 6), // "OnSend"
QT_MOC_LITERAL(13, 128, 13), // "OnClickedLive"
QT_MOC_LITERAL(14, 142, 23), // "OnClickedLiveTranstions"
QT_MOC_LITERAL(15, 166, 20), // "OnClickedGetTimeBase"
QT_MOC_LITERAL(16, 187, 16), // "WriteCamSettings"
QT_MOC_LITERAL(17, 204, 18), // "WriteOneCamSetting"
QT_MOC_LITERAL(18, 223, 5), // "index"
QT_MOC_LITERAL(19, 229, 7), // "setting"
QT_MOC_LITERAL(20, 237, 15), // "ReadCamSettings"
QT_MOC_LITERAL(21, 253, 9), // "ShowImage"
QT_MOC_LITERAL(22, 263, 15), // "ShowTransitions"
QT_MOC_LITERAL(23, 279, 13), // "ShowBlowImage"
QT_MOC_LITERAL(24, 293, 9), // "currPiece"
QT_MOC_LITERAL(25, 303, 5), // "frame"
QT_MOC_LITERAL(26, 309, 12), // "ShowTimeLine"
QT_MOC_LITERAL(27, 322, 18), // "OnToggledCheckBox1"
QT_MOC_LITERAL(28, 341, 18), // "OnToggledCheckBox2"
QT_MOC_LITERAL(29, 360, 18), // "OnToggledCheckBox3"
QT_MOC_LITERAL(30, 379, 18), // "OnToggledCheckBox4"
QT_MOC_LITERAL(31, 398, 18), // "OnToggledCheckBox5"
QT_MOC_LITERAL(32, 417, 27), // "OnClickedNextHorMeasurement"
QT_MOC_LITERAL(33, 445, 27), // "OnClickedPrevHorMeasurement"
QT_MOC_LITERAL(34, 473, 18), // "OnClickedHistoryUp"
QT_MOC_LITERAL(35, 492, 20), // "OnClickedHistoryDown"
QT_MOC_LITERAL(36, 513, 20), // "OnClickedLastScanned"
QT_MOC_LITERAL(37, 534, 16), // "OnClickedFrameUp"
QT_MOC_LITERAL(38, 551, 18), // "OnClickedFrameDown"
QT_MOC_LITERAL(39, 570, 10), // "UpdateList"
QT_MOC_LITERAL(40, 581, 15), // "ReadDowelString"
QT_MOC_LITERAL(41, 597, 7), // "Qstring"
QT_MOC_LITERAL(42, 605, 21), // "CreateDowelFromString"
QT_MOC_LITERAL(43, 627, 12), // "currentPiece"
QT_MOC_LITERAL(44, 640, 12), // "MeasureDowel"
QT_MOC_LITERAL(45, 653, 18), // "MeasureDowelRadius"
QT_MOC_LITERAL(46, 672, 17), // "MeasureDowelConus"
QT_MOC_LITERAL(47, 690, 17), // "OnClientConnected"
QT_MOC_LITERAL(48, 708, 20), // "OnClientDisconnected"
QT_MOC_LITERAL(49, 729, 19), // "OnConnectionTimeout"
QT_MOC_LITERAL(50, 749, 15), // "SpinGainChanged"
QT_MOC_LITERAL(51, 765, 4), // "gain"
QT_MOC_LITERAL(52, 770, 17), // "SpinOffsetChanged"
QT_MOC_LITERAL(53, 788, 6), // "offset"
QT_MOC_LITERAL(54, 795, 23), // "SpinDetectWindowChanged"
QT_MOC_LITERAL(55, 819, 9), // "topBottom"
QT_MOC_LITERAL(56, 829, 5), // "value"
QT_MOC_LITERAL(57, 835, 19), // "SpinGainChangedTest"
QT_MOC_LITERAL(58, 855, 9), // "ShowDowel"
QT_MOC_LITERAL(59, 865, 7), // "nrDowel"
QT_MOC_LITERAL(60, 873, 16), // "ShowMeasurements"
QT_MOC_LITERAL(61, 890, 20), // "ShowHorizontalCamera"
QT_MOC_LITERAL(62, 911, 4), // "curr"
QT_MOC_LITERAL(63, 916, 26), // "OnClickedSelectedDMSCamNew"
QT_MOC_LITERAL(64, 943, 21), // "OnSignalXilinxVersion"
QT_MOC_LITERAL(65, 965, 5) // "array"

    },
    "DMSudp\0frameReadySignal\0\0id\0image\0"
    "transitionReadySignal\0nrCam\0parseData\0"
    "timeLineSignal\0versionSignal\0version\0"
    "OnViewTimer\0OnSend\0OnClickedLive\0"
    "OnClickedLiveTranstions\0OnClickedGetTimeBase\0"
    "WriteCamSettings\0WriteOneCamSetting\0"
    "index\0setting\0ReadCamSettings\0ShowImage\0"
    "ShowTransitions\0ShowBlowImage\0currPiece\0"
    "frame\0ShowTimeLine\0OnToggledCheckBox1\0"
    "OnToggledCheckBox2\0OnToggledCheckBox3\0"
    "OnToggledCheckBox4\0OnToggledCheckBox5\0"
    "OnClickedNextHorMeasurement\0"
    "OnClickedPrevHorMeasurement\0"
    "OnClickedHistoryUp\0OnClickedHistoryDown\0"
    "OnClickedLastScanned\0OnClickedFrameUp\0"
    "OnClickedFrameDown\0UpdateList\0"
    "ReadDowelString\0Qstring\0CreateDowelFromString\0"
    "currentPiece\0MeasureDowel\0MeasureDowelRadius\0"
    "MeasureDowelConus\0OnClientConnected\0"
    "OnClientDisconnected\0OnConnectionTimeout\0"
    "SpinGainChanged\0gain\0SpinOffsetChanged\0"
    "offset\0SpinDetectWindowChanged\0topBottom\0"
    "value\0SpinGainChangedTest\0ShowDowel\0"
    "nrDowel\0ShowMeasurements\0ShowHorizontalCamera\0"
    "curr\0OnClickedSelectedDMSCamNew\0"
    "OnSignalXilinxVersion\0array"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DMSudp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      46,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  244,    2, 0x06 /* Public */,
       5,    2,  249,    2, 0x06 /* Public */,
       8,    1,  254,    2, 0x06 /* Public */,
       9,    1,  257,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,  260,    2, 0x08 /* Private */,
      12,    0,  261,    2, 0x08 /* Private */,
      13,    0,  262,    2, 0x08 /* Private */,
      14,    0,  263,    2, 0x08 /* Private */,
      15,    0,  264,    2, 0x08 /* Private */,
      16,    0,  265,    2, 0x08 /* Private */,
      17,    2,  266,    2, 0x08 /* Private */,
      20,    0,  271,    2, 0x08 /* Private */,
      21,    2,  272,    2, 0x08 /* Private */,
      22,    2,  277,    2, 0x08 /* Private */,
      23,    2,  282,    2, 0x08 /* Private */,
      26,    1,  287,    2, 0x08 /* Private */,
      27,    1,  290,    2, 0x08 /* Private */,
      28,    1,  293,    2, 0x08 /* Private */,
      29,    1,  296,    2, 0x08 /* Private */,
      30,    1,  299,    2, 0x08 /* Private */,
      31,    1,  302,    2, 0x08 /* Private */,
      32,    0,  305,    2, 0x08 /* Private */,
      33,    0,  306,    2, 0x08 /* Private */,
      34,    0,  307,    2, 0x08 /* Private */,
      35,    0,  308,    2, 0x08 /* Private */,
      36,    0,  309,    2, 0x08 /* Private */,
      37,    0,  310,    2, 0x08 /* Private */,
      38,    0,  311,    2, 0x08 /* Private */,
      39,    0,  312,    2, 0x08 /* Private */,
      40,    1,  313,    2, 0x0a /* Public */,
      42,    1,  316,    2, 0x0a /* Public */,
      44,    1,  319,    2, 0x0a /* Public */,
      45,    1,  322,    2, 0x0a /* Public */,
      46,    1,  325,    2, 0x0a /* Public */,
      47,    0,  328,    2, 0x0a /* Public */,
      48,    0,  329,    2, 0x0a /* Public */,
      49,    0,  330,    2, 0x0a /* Public */,
      50,    2,  331,    2, 0x0a /* Public */,
      52,    2,  336,    2, 0x0a /* Public */,
      54,    3,  341,    2, 0x0a /* Public */,
      57,    1,  348,    2, 0x0a /* Public */,
      58,    1,  351,    2, 0x0a /* Public */,
      60,    1,  354,    2, 0x0a /* Public */,
      61,    2,  357,    2, 0x0a /* Public */,
      63,    0,  362,    2, 0x0a /* Public */,
      64,    1,  363,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::QByteArray,    3,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::QByteArray,    6,    7,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void, QMetaType::QByteArray,   10,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   18,   19,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QByteArray,    2,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::QByteArray,    2,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   24,   25,
    QMetaType::Void, QMetaType::QByteArray,    4,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   41,
    QMetaType::Void, QMetaType::Int,   43,
    QMetaType::Void, QMetaType::Int,   43,
    QMetaType::Void, QMetaType::Int,   43,
    QMetaType::Void, QMetaType::Int,   43,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   18,   51,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   18,   53,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   18,   55,   56,
    QMetaType::Void, QMetaType::Int,   51,
    QMetaType::Void, QMetaType::Int,   59,
    QMetaType::Void, QMetaType::Int,   59,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   62,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   65,

       0        // eod
};

void DMSudp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DMSudp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->frameReadySignal((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QByteArray(*)>(_a[2]))); break;
        case 1: _t->transitionReadySignal((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QByteArray(*)>(_a[2]))); break;
        case 2: _t->timeLineSignal((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 3: _t->versionSignal((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 4: _t->OnViewTimer(); break;
        case 5: _t->OnSend(); break;
        case 6: _t->OnClickedLive(); break;
        case 7: _t->OnClickedLiveTranstions(); break;
        case 8: _t->OnClickedGetTimeBase(); break;
        case 9: _t->WriteCamSettings(); break;
        case 10: _t->WriteOneCamSetting((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 11: _t->ReadCamSettings(); break;
        case 12: _t->ShowImage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QByteArray(*)>(_a[2]))); break;
        case 13: _t->ShowTransitions((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QByteArray(*)>(_a[2]))); break;
        case 14: _t->ShowBlowImage((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 15: _t->ShowTimeLine((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 16: _t->OnToggledCheckBox1((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->OnToggledCheckBox2((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->OnToggledCheckBox3((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->OnToggledCheckBox4((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->OnToggledCheckBox5((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->OnClickedNextHorMeasurement(); break;
        case 22: _t->OnClickedPrevHorMeasurement(); break;
        case 23: _t->OnClickedHistoryUp(); break;
        case 24: _t->OnClickedHistoryDown(); break;
        case 25: _t->OnClickedLastScanned(); break;
        case 26: _t->OnClickedFrameUp(); break;
        case 27: _t->OnClickedFrameDown(); break;
        case 28: _t->UpdateList(); break;
        case 29: _t->ReadDowelString((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 30: _t->CreateDowelFromString((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 31: _t->MeasureDowel((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 32: _t->MeasureDowelRadius((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 33: _t->MeasureDowelConus((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 34: _t->OnClientConnected(); break;
        case 35: _t->OnClientDisconnected(); break;
        case 36: _t->OnConnectionTimeout(); break;
        case 37: _t->SpinGainChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 38: _t->SpinOffsetChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 39: _t->SpinDetectWindowChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 40: _t->SpinGainChangedTest((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 41: _t->ShowDowel((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 42: _t->ShowMeasurements((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 43: _t->ShowHorizontalCamera((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 44: _t->OnClickedSelectedDMSCamNew(); break;
        case 45: _t->OnSignalXilinxVersion((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DMSudp::*)(int , QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DMSudp::frameReadySignal)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (DMSudp::*)(int , QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DMSudp::transitionReadySignal)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (DMSudp::*)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DMSudp::timeLineSignal)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (DMSudp::*)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DMSudp::versionSignal)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DMSudp::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_DMSudp.data,
    qt_meta_data_DMSudp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DMSudp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DMSudp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DMSudp.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int DMSudp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 46)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 46;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 46)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 46;
    }
    return _id;
}

// SIGNAL 0
void DMSudp::frameReadySignal(int _t1, QByteArray _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DMSudp::transitionReadySignal(int _t1, QByteArray _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void DMSudp::timeLineSignal(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void DMSudp::versionSignal(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
