/********************************************************************************
** Form generated from reading UI file 'EMG.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EMG_H
#define UI_EMG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EMGui
{
public:
    QPushButton *buttonReset;
    QLabel *label_2;
    QLabel *labelText;
    QLabel *label;

    void setupUi(QWidget *EMGui)
    {
        if (EMGui->objectName().isEmpty())
            EMGui->setObjectName(QString::fromUtf8("EMGui"));
        EMGui->resize(402, 503);
        buttonReset = new QPushButton(EMGui);
        buttonReset->setObjectName(QString::fromUtf8("buttonReset"));
        buttonReset->setGeometry(QRect(10, 390, 381, 71));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        buttonReset->setFont(font);
        buttonReset->setFlat(false);
        label_2 = new QLabel(EMGui);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 20, 381, 31));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(36);
        font1.setBold(false);
        font1.setWeight(50);
        label_2->setFont(font1);
        label_2->setStyleSheet(QString::fromUtf8("color: rgb(255, 0, 0);"));
        label_2->setAlignment(Qt::AlignCenter);
        labelText = new QLabel(EMGui);
        labelText->setObjectName(QString::fromUtf8("labelText"));
        labelText->setGeometry(QRect(10, 90, 381, 41));
        labelText->setFont(font);
        labelText->setStyleSheet(QString::fromUtf8("color: rgb(255, 0, 0);"));
        labelText->setAlignment(Qt::AlignCenter);
        label = new QLabel(EMGui);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(50, 120, 281, 241));
        label->setPixmap(QPixmap(QString::fromUtf8("res/EMG1.png")));

        retranslateUi(EMGui);

        buttonReset->setDefault(true);


        QMetaObject::connectSlotsByName(EMGui);
    } // setupUi

    void retranslateUi(QWidget *EMGui)
    {
        EMGui->setWindowTitle(QCoreApplication::translate("EMGui", "Form", nullptr));
        buttonReset->setText(QCoreApplication::translate("EMGui", "RESET", nullptr));
        label_2->setText(QCoreApplication::translate("EMGui", "EMG STOP!!!", nullptr));
        labelText->setText(QCoreApplication::translate("EMGui", "RELEASE EMG!", nullptr));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class EMGui: public Ui_EMGui {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EMG_H
