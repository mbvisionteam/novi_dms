/********************************************************************************
** Form generated from reading UI file 'DMSPresortDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DMSPRESORTDIALOG_H
#define UI_DMSPRESORTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DMSpresortDialog
{
public:
    QScrollArea *scrollAreaHistory;
    QWidget *scrollAreaWidgetContents;
    QPushButton *buttonClose;
    QGraphicsView *imageView;
    QPushButton *buttonMeasure;
    QPushButton *buttonZoomOut;
    QPushButton *buttonZoomIn;
    QPushButton *buttonZoomReset;
    QGroupBox *groupBox;
    QFormLayout *formLayout_2;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout;
    QLabel *label_4;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit_4;
    QLabel *label_5;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_6;
    QLabel *label_7;
    QLabel *label_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QLabel *label_8;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLineEdit *lineBasic_0;
    QLabel *label_16;
    QLabel *label_14;
    QLabel *label_13;
    QLabel *label_15;
    QLabel *label_17;
    QLineEdit *lineBasic_1;
    QLineEdit *lineBasic_2;
    QLineEdit *lineBasic_3;
    QLineEdit *lineBasic_4;
    QPushButton *buttonSave;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_3;
    QComboBox *comboPresortCalibration;
    QLabel *label_12;
    QLineEdit *lineEditWidth;
    QLabel *label_10;
    QLineEdit *lineEditTopOffset;
    QLabel *label_11;
    QLineEdit *lineEditBottomOffset;
    QRadioButton *radioLive;
    QRadioButton *radioHistory;
    QGroupBox *groupBox_5;
    QComboBox *comboAdvancedSettings;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridAdvanceSettings;
    QLabel *label_44;
    QLabel *label_23;
    QLabel *label_42;
    QLabel *label_36;
    QLabel *label_46;
    QLabel *label_24;
    QLabel *label_37;
    QLabel *label_40;
    QLabel *label_35;
    QLabel *label_38;
    QLabel *label_39;
    QLabel *label_43;
    QLabel *label_41;
    QLabel *label_45;
    QPushButton *buttonCreateNewPresortSettings;
    QPushButton *buttonDeletePresortSettings;
    QGroupBox *groupBox_6;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayoutResult;
    QLabel *label_22;
    QLabel *label_47;
    QLabel *label_27;
    QLabel *label_33;
    QLabel *label_31;
    QLabel *label_26;
    QLabel *label_25;
    QLabel *label_50;
    QLabel *label_51;
    QLabel *label_32;
    QLabel *label_29;
    QLabel *label_21;
    QLabel *label_28;
    QLabel *label_48;
    QLabel *labelProcessingTime;
    QLabel *label_34;
    QLabel *labelLenght;
    QLabel *label_49;

    void setupUi(QWidget *DMSpresortDialog)
    {
        if (DMSpresortDialog->objectName().isEmpty())
            DMSpresortDialog->setObjectName(QString::fromUtf8("DMSpresortDialog"));
        DMSpresortDialog->resize(1531, 862);
        scrollAreaHistory = new QScrollArea(DMSpresortDialog);
        scrollAreaHistory->setObjectName(QString::fromUtf8("scrollAreaHistory"));
        scrollAreaHistory->setGeometry(QRect(10, 10, 1511, 141));
        scrollAreaHistory->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 1509, 139));
        scrollAreaHistory->setWidget(scrollAreaWidgetContents);
        buttonClose = new QPushButton(DMSpresortDialog);
        buttonClose->setObjectName(QString::fromUtf8("buttonClose"));
        buttonClose->setGeometry(QRect(1450, 270, 75, 23));
        imageView = new QGraphicsView(DMSpresortDialog);
        imageView->setObjectName(QString::fromUtf8("imageView"));
        imageView->setGeometry(QRect(550, 160, 671, 271));
        imageView->viewport()->setProperty("cursor", QVariant(QCursor(Qt::CrossCursor)));
        imageView->setMouseTracking(true);
        buttonMeasure = new QPushButton(DMSpresortDialog);
        buttonMeasure->setObjectName(QString::fromUtf8("buttonMeasure"));
        buttonMeasure->setGeometry(QRect(370, 170, 151, 81));
        buttonZoomOut = new QPushButton(DMSpresortDialog);
        buttonZoomOut->setObjectName(QString::fromUtf8("buttonZoomOut"));
        buttonZoomOut->setGeometry(QRect(450, 360, 75, 23));
        buttonZoomIn = new QPushButton(DMSpresortDialog);
        buttonZoomIn->setObjectName(QString::fromUtf8("buttonZoomIn"));
        buttonZoomIn->setGeometry(QRect(450, 390, 75, 23));
        buttonZoomReset = new QPushButton(DMSpresortDialog);
        buttonZoomReset->setObjectName(QString::fromUtf8("buttonZoomReset"));
        buttonZoomReset->setGeometry(QRect(450, 420, 75, 23));
        groupBox = new QGroupBox(DMSpresortDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 160, 311, 541));
        QFont font;
        font.setFamily(QString::fromUtf8("Times New Roman"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        groupBox->setFont(font);
        formLayout_2 = new QFormLayout(groupBox);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(0, 300));
        gridLayout = new QGridLayout(groupBox_3);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_4 = new QLabel(groupBox_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 2, 0, 1, 1);

        lineEdit_5 = new QLineEdit(groupBox_3);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
        lineEdit_5->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit_5, 4, 1, 1, 1);

        lineEdit_4 = new QLineEdit(groupBox_3);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit_4, 3, 1, 1, 1);

        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1);

        lineEdit_3 = new QLineEdit(groupBox_3);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit_3, 2, 1, 1, 1);

        lineEdit_2 = new QLineEdit(groupBox_3);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit_2, 1, 1, 1, 1);

        lineEdit_6 = new QLineEdit(groupBox_3);
        lineEdit_6->setObjectName(QString::fromUtf8("lineEdit_6"));
        lineEdit_6->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit_6, 5, 1, 1, 1);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 4, 0, 1, 1);

        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        lineEdit = new QLineEdit(groupBox_3);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit, 0, 1, 1, 1);

        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 5, 0, 1, 1);


        formLayout_2->setWidget(1, QFormLayout::SpanningRole, groupBox_3);

        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(0, 200));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lineBasic_0 = new QLineEdit(groupBox_2);
        lineBasic_0->setObjectName(QString::fromUtf8("lineBasic_0"));
        lineBasic_0->setMaximumSize(QSize(80, 16777215));

        gridLayout_2->addWidget(lineBasic_0, 0, 1, 1, 1);

        label_16 = new QLabel(groupBox_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_2->addWidget(label_16, 2, 0, 1, 1);

        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_2->addWidget(label_14, 1, 0, 1, 1);

        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_2->addWidget(label_13, 0, 0, 1, 1);

        label_15 = new QLabel(groupBox_2);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_2->addWidget(label_15, 3, 0, 1, 1);

        label_17 = new QLabel(groupBox_2);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout_2->addWidget(label_17, 4, 0, 1, 1);

        lineBasic_1 = new QLineEdit(groupBox_2);
        lineBasic_1->setObjectName(QString::fromUtf8("lineBasic_1"));
        lineBasic_1->setMaximumSize(QSize(80, 16777215));

        gridLayout_2->addWidget(lineBasic_1, 1, 1, 1, 1);

        lineBasic_2 = new QLineEdit(groupBox_2);
        lineBasic_2->setObjectName(QString::fromUtf8("lineBasic_2"));
        lineBasic_2->setMaximumSize(QSize(80, 16777215));

        gridLayout_2->addWidget(lineBasic_2, 2, 1, 1, 1);

        lineBasic_3 = new QLineEdit(groupBox_2);
        lineBasic_3->setObjectName(QString::fromUtf8("lineBasic_3"));
        lineBasic_3->setMaximumSize(QSize(80, 16777215));

        gridLayout_2->addWidget(lineBasic_3, 3, 1, 1, 1);

        lineBasic_4 = new QLineEdit(groupBox_2);
        lineBasic_4->setObjectName(QString::fromUtf8("lineBasic_4"));
        lineBasic_4->setMaximumSize(QSize(80, 16777215));

        gridLayout_2->addWidget(lineBasic_4, 4, 1, 1, 1);


        formLayout_2->setWidget(0, QFormLayout::SpanningRole, groupBox_2);

        buttonSave = new QPushButton(DMSpresortDialog);
        buttonSave->setObjectName(QString::fromUtf8("buttonSave"));
        buttonSave->setGeometry(QRect(370, 260, 151, 81));
        groupBox_4 = new QGroupBox(DMSpresortDialog);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(10, 710, 311, 157));
        groupBox_4->setFont(font);
        gridLayout_3 = new QGridLayout(groupBox_4);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        comboPresortCalibration = new QComboBox(groupBox_4);
        comboPresortCalibration->setObjectName(QString::fromUtf8("comboPresortCalibration"));

        gridLayout_3->addWidget(comboPresortCalibration, 0, 0, 1, 2);

        label_12 = new QLabel(groupBox_4);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_3->addWidget(label_12, 1, 0, 1, 1);

        lineEditWidth = new QLineEdit(groupBox_4);
        lineEditWidth->setObjectName(QString::fromUtf8("lineEditWidth"));
        lineEditWidth->setMaximumSize(QSize(80, 16777215));

        gridLayout_3->addWidget(lineEditWidth, 1, 1, 1, 1);

        label_10 = new QLabel(groupBox_4);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_3->addWidget(label_10, 2, 0, 1, 1);

        lineEditTopOffset = new QLineEdit(groupBox_4);
        lineEditTopOffset->setObjectName(QString::fromUtf8("lineEditTopOffset"));
        lineEditTopOffset->setMaximumSize(QSize(80, 16777215));

        gridLayout_3->addWidget(lineEditTopOffset, 2, 1, 1, 1);

        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_3->addWidget(label_11, 3, 0, 1, 1);

        lineEditBottomOffset = new QLineEdit(groupBox_4);
        lineEditBottomOffset->setObjectName(QString::fromUtf8("lineEditBottomOffset"));
        lineEditBottomOffset->setMaximumSize(QSize(80, 16777215));

        gridLayout_3->addWidget(lineEditBottomOffset, 3, 1, 1, 1);

        radioLive = new QRadioButton(DMSpresortDialog);
        radioLive->setObjectName(QString::fromUtf8("radioLive"));
        radioLive->setGeometry(QRect(560, 430, 101, 17));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Times New Roman"));
        font1.setPointSize(12);
        radioLive->setFont(font1);
        radioHistory = new QRadioButton(DMSpresortDialog);
        radioHistory->setObjectName(QString::fromUtf8("radioHistory"));
        radioHistory->setGeometry(QRect(660, 430, 121, 17));
        radioHistory->setFont(font1);
        groupBox_5 = new QGroupBox(DMSpresortDialog);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(400, 460, 541, 391));
        groupBox_5->setFont(font);
        comboAdvancedSettings = new QComboBox(groupBox_5);
        comboAdvancedSettings->setObjectName(QString::fromUtf8("comboAdvancedSettings"));
        comboAdvancedSettings->setGeometry(QRect(10, 30, 241, 31));
        gridLayoutWidget_2 = new QWidget(groupBox_5);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 80, 521, 297));
        gridAdvanceSettings = new QGridLayout(gridLayoutWidget_2);
        gridAdvanceSettings->setObjectName(QString::fromUtf8("gridAdvanceSettings"));
        gridAdvanceSettings->setContentsMargins(0, 0, 0, 0);
        label_44 = new QLabel(gridLayoutWidget_2);
        label_44->setObjectName(QString::fromUtf8("label_44"));

        gridAdvanceSettings->addWidget(label_44, 6, 0, 1, 1);

        label_23 = new QLabel(gridLayoutWidget_2);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setMaximumSize(QSize(80, 16777215));

        gridAdvanceSettings->addWidget(label_23, 0, 2, 1, 1);

        label_42 = new QLabel(gridLayoutWidget_2);
        label_42->setObjectName(QString::fromUtf8("label_42"));

        gridAdvanceSettings->addWidget(label_42, 7, 0, 1, 1);

        label_36 = new QLabel(gridLayoutWidget_2);
        label_36->setObjectName(QString::fromUtf8("label_36"));

        gridAdvanceSettings->addWidget(label_36, 0, 0, 1, 1);

        label_46 = new QLabel(gridLayoutWidget_2);
        label_46->setObjectName(QString::fromUtf8("label_46"));

        gridAdvanceSettings->addWidget(label_46, 9, 0, 1, 1);

        label_24 = new QLabel(gridLayoutWidget_2);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setMaximumSize(QSize(80, 16777215));

        gridAdvanceSettings->addWidget(label_24, 0, 1, 1, 1);

        label_37 = new QLabel(gridLayoutWidget_2);
        label_37->setObjectName(QString::fromUtf8("label_37"));

        gridAdvanceSettings->addWidget(label_37, 1, 0, 1, 1);

        label_40 = new QLabel(gridLayoutWidget_2);
        label_40->setObjectName(QString::fromUtf8("label_40"));

        gridAdvanceSettings->addWidget(label_40, 4, 0, 1, 1);

        label_35 = new QLabel(gridLayoutWidget_2);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setMaximumSize(QSize(80, 16777215));

        gridAdvanceSettings->addWidget(label_35, 0, 3, 1, 1);

        label_38 = new QLabel(gridLayoutWidget_2);
        label_38->setObjectName(QString::fromUtf8("label_38"));

        gridAdvanceSettings->addWidget(label_38, 2, 0, 1, 1);

        label_39 = new QLabel(gridLayoutWidget_2);
        label_39->setObjectName(QString::fromUtf8("label_39"));

        gridAdvanceSettings->addWidget(label_39, 3, 0, 1, 1);

        label_43 = new QLabel(gridLayoutWidget_2);
        label_43->setObjectName(QString::fromUtf8("label_43"));

        gridAdvanceSettings->addWidget(label_43, 5, 0, 1, 1);

        label_41 = new QLabel(gridLayoutWidget_2);
        label_41->setObjectName(QString::fromUtf8("label_41"));

        gridAdvanceSettings->addWidget(label_41, 8, 0, 1, 1);

        label_45 = new QLabel(gridLayoutWidget_2);
        label_45->setObjectName(QString::fromUtf8("label_45"));

        gridAdvanceSettings->addWidget(label_45, 10, 0, 1, 1);

        buttonCreateNewPresortSettings = new QPushButton(groupBox_5);
        buttonCreateNewPresortSettings->setObjectName(QString::fromUtf8("buttonCreateNewPresortSettings"));
        buttonCreateNewPresortSettings->setGeometry(QRect(280, 30, 121, 31));
        buttonDeletePresortSettings = new QPushButton(groupBox_5);
        buttonDeletePresortSettings->setObjectName(QString::fromUtf8("buttonDeletePresortSettings"));
        buttonDeletePresortSettings->setGeometry(QRect(410, 30, 121, 31));
        groupBox_6 = new QGroupBox(DMSpresortDialog);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(1120, 430, 391, 431));
        groupBox_6->setFont(font);
        gridLayoutWidget = new QWidget(groupBox_6);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(30, 40, 349, 321));
        gridLayoutResult = new QGridLayout(gridLayoutWidget);
        gridLayoutResult->setObjectName(QString::fromUtf8("gridLayoutResult"));
        gridLayoutResult->setContentsMargins(0, 0, 0, 0);
        label_22 = new QLabel(gridLayoutWidget);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        gridLayoutResult->addWidget(label_22, 0, 2, 1, 1);

        label_47 = new QLabel(gridLayoutWidget);
        label_47->setObjectName(QString::fromUtf8("label_47"));

        gridLayoutResult->addWidget(label_47, 7, 0, 1, 1);

        label_27 = new QLabel(gridLayoutWidget);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        gridLayoutResult->addWidget(label_27, 1, 0, 1, 1);

        label_33 = new QLabel(gridLayoutWidget);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        gridLayoutResult->addWidget(label_33, 6, 0, 1, 1);

        label_31 = new QLabel(gridLayoutWidget);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        gridLayoutResult->addWidget(label_31, 4, 0, 1, 1);

        label_26 = new QLabel(gridLayoutWidget);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        gridLayoutResult->addWidget(label_26, 0, 0, 1, 1);

        label_25 = new QLabel(gridLayoutWidget);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        gridLayoutResult->addWidget(label_25, 0, 3, 1, 1);

        label_50 = new QLabel(gridLayoutWidget);
        label_50->setObjectName(QString::fromUtf8("label_50"));

        gridLayoutResult->addWidget(label_50, 9, 0, 1, 1);

        label_51 = new QLabel(gridLayoutWidget);
        label_51->setObjectName(QString::fromUtf8("label_51"));

        gridLayoutResult->addWidget(label_51, 10, 0, 1, 1);

        label_32 = new QLabel(gridLayoutWidget);
        label_32->setObjectName(QString::fromUtf8("label_32"));

        gridLayoutResult->addWidget(label_32, 5, 0, 1, 1);

        label_29 = new QLabel(gridLayoutWidget);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        gridLayoutResult->addWidget(label_29, 3, 0, 1, 1);

        label_21 = new QLabel(gridLayoutWidget);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        gridLayoutResult->addWidget(label_21, 0, 1, 1, 1);

        label_28 = new QLabel(gridLayoutWidget);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        gridLayoutResult->addWidget(label_28, 2, 0, 1, 1);

        label_48 = new QLabel(gridLayoutWidget);
        label_48->setObjectName(QString::fromUtf8("label_48"));

        gridLayoutResult->addWidget(label_48, 8, 0, 1, 1);

        labelProcessingTime = new QLabel(groupBox_6);
        labelProcessingTime->setObjectName(QString::fromUtf8("labelProcessingTime"));
        labelProcessingTime->setGeometry(QRect(270, 390, 101, 41));
        labelProcessingTime->setFont(font1);
        label_34 = new QLabel(groupBox_6);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setGeometry(QRect(40, 400, 131, 19));
        labelLenght = new QLabel(groupBox_6);
        labelLenght->setObjectName(QString::fromUtf8("labelLenght"));
        labelLenght->setGeometry(QRect(270, 370, 101, 41));
        labelLenght->setFont(font1);
        label_49 = new QLabel(groupBox_6);
        label_49->setObjectName(QString::fromUtf8("label_49"));
        label_49->setGeometry(QRect(40, 380, 131, 19));

        retranslateUi(DMSpresortDialog);

        QMetaObject::connectSlotsByName(DMSpresortDialog);
    } // setupUi

    void retranslateUi(QWidget *DMSpresortDialog)
    {
        DMSpresortDialog->setWindowTitle(QCoreApplication::translate("DMSpresortDialog", "Presort Dialog", nullptr));
        buttonClose->setText(QCoreApplication::translate("DMSpresortDialog", "Close", nullptr));
        buttonMeasure->setText(QCoreApplication::translate("DMSpresortDialog", "Measure Current Dowel", nullptr));
        buttonZoomOut->setText(QCoreApplication::translate("DMSpresortDialog", "ZoomOut", nullptr));
        buttonZoomIn->setText(QCoreApplication::translate("DMSpresortDialog", "ZoomIn", nullptr));
        buttonZoomReset->setText(QCoreApplication::translate("DMSpresortDialog", "ZoomReset", nullptr));
        groupBox->setTitle(QCoreApplication::translate("DMSpresortDialog", "Dowel Basic Settings", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("DMSpresortDialog", "Dowel Set window", nullptr));
        label_4->setText(QCoreApplication::translate("DMSpresortDialog", " XlWinTop", nullptr));
        label_5->setText(QCoreApplication::translate("DMSpresortDialog", "XrWinTop", nullptr));
        label_7->setText(QCoreApplication::translate("DMSpresortDialog", " XlWinBottom", nullptr));
        label_2->setText(QCoreApplication::translate("DMSpresortDialog", " XrCenter", nullptr));
        label->setText(QCoreApplication::translate("DMSpresortDialog", " XlCenter", nullptr));
        label_8->setText(QCoreApplication::translate("DMSpresortDialog", " XrWinBottom", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("DMSpresortDialog", "Detect Window", nullptr));
        label_16->setText(QCoreApplication::translate("DMSpresortDialog", "X Stop", nullptr));
        label_14->setText(QCoreApplication::translate("DMSpresortDialog", "X Start", nullptr));
        label_13->setText(QCoreApplication::translate("DMSpresortDialog", "Start Detect Threshold", nullptr));
        label_15->setText(QCoreApplication::translate("DMSpresortDialog", "Y Start", nullptr));
        label_17->setText(QCoreApplication::translate("DMSpresortDialog", "Cam Factor mm/pix", nullptr));
        buttonSave->setText(QCoreApplication::translate("DMSpresortDialog", "Save", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("DMSpresortDialog", "Presort Calibration Settings", nullptr));
        label_12->setText(QCoreApplication::translate("DMSpresortDialog", "Width ", nullptr));
        label_10->setText(QCoreApplication::translate("DMSpresortDialog", "YWinTopOffset", nullptr));
        label_11->setText(QCoreApplication::translate("DMSpresortDialog", "YWinBottomOffset", nullptr));
        radioLive->setText(QCoreApplication::translate("DMSpresortDialog", "Live Image", nullptr));
        radioHistory->setText(QCoreApplication::translate("DMSpresortDialog", "Load  History", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("DMSpresortDialog", "Advanced Settings", nullptr));
        label_44->setText(QCoreApplication::translate("DMSpresortDialog", "MAX DEFECT HEIGHT A0", nullptr));
        label_23->setText(QCoreApplication::translate("DMSpresortDialog", "CENTER", nullptr));
        label_42->setText(QCoreApplication::translate("DMSpresortDialog", "OFFSET THRESHOLD A1", nullptr));
        label_36->setText(QString());
        label_46->setText(QCoreApplication::translate("DMSpresortDialog", "MAX DEFECT width A1", nullptr));
        label_24->setText(QCoreApplication::translate("DMSpresortDialog", "TOP", nullptr));
        label_37->setText(QCoreApplication::translate("DMSpresortDialog", "MIN AVR.INT", nullptr));
        label_40->setText(QCoreApplication::translate("DMSpresortDialog", "MAX DEFECT AREA A0", nullptr));
        label_35->setText(QCoreApplication::translate("DMSpresortDialog", "BOTTOM", nullptr));
        label_38->setText(QCoreApplication::translate("DMSpresortDialog", "MAX.STD", nullptr));
        label_39->setText(QCoreApplication::translate("DMSpresortDialog", "OFFSET THRESHOLD A0", nullptr));
        label_43->setText(QCoreApplication::translate("DMSpresortDialog", "MAX DEFECT WIDTH A0", nullptr));
        label_41->setText(QCoreApplication::translate("DMSpresortDialog", "MAX DEFECT AREA A1", nullptr));
        label_45->setText(QCoreApplication::translate("DMSpresortDialog", "MAX DEFECT height A1", nullptr));
        buttonCreateNewPresortSettings->setText(QCoreApplication::translate("DMSpresortDialog", "Create NEW", nullptr));
        buttonDeletePresortSettings->setText(QCoreApplication::translate("DMSpresortDialog", "Delete", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("DMSpresortDialog", "Measure Results:", nullptr));
        label_22->setText(QCoreApplication::translate("DMSpresortDialog", "CENTER", nullptr));
        label_47->setText(QCoreApplication::translate("DMSpresortDialog", "T.Offset A1", nullptr));
        label_27->setText(QCoreApplication::translate("DMSpresortDialog", "AVR.INT", nullptr));
        label_33->setText(QCoreApplication::translate("DMSpresortDialog", "Height A0", nullptr));
        label_31->setText(QCoreApplication::translate("DMSpresortDialog", "Area A0", nullptr));
        label_26->setText(QString());
        label_25->setText(QCoreApplication::translate("DMSpresortDialog", "BOTTOM", nullptr));
        label_50->setText(QCoreApplication::translate("DMSpresortDialog", "Width A1", nullptr));
        label_51->setText(QCoreApplication::translate("DMSpresortDialog", "Height A1", nullptr));
        label_32->setText(QCoreApplication::translate("DMSpresortDialog", "Width A0", nullptr));
        label_29->setText(QCoreApplication::translate("DMSpresortDialog", "T.OFFSET A0", nullptr));
        label_21->setText(QCoreApplication::translate("DMSpresortDialog", "TOP", nullptr));
        label_28->setText(QCoreApplication::translate("DMSpresortDialog", "ST.DEV", nullptr));
        label_48->setText(QCoreApplication::translate("DMSpresortDialog", "Area A1", nullptr));
        labelProcessingTime->setText(QCoreApplication::translate("DMSpresortDialog", "TextLabel", nullptr));
        label_34->setText(QCoreApplication::translate("DMSpresortDialog", "ProcessingTime:", nullptr));
        labelLenght->setText(QCoreApplication::translate("DMSpresortDialog", "TextLabel", nullptr));
        label_49->setText(QCoreApplication::translate("DMSpresortDialog", "Lenght:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DMSpresortDialog: public Ui_DMSpresortDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DMSPRESORTDIALOG_H
