/********************************************************************************
** Form generated from reading UI file 'Camera.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERA_H
#define UI_CAMERA_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Camera
{
public:
    QGroupBox *groupBox;
    QPushButton *buttonLive;
    QGroupBox *groupBoxIntensity;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelin;
    QLabel *labelB;
    QLabel *labelG;
    QLabel *labelR;
    QGroupBox *groupBoxCoordinates;
    QHBoxLayout *horizontalLayout;
    QLabel *labelX;
    QLabel *labelY;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QSlider *sliderExpo;
    QSpinBox *spinGain;
    QSlider *sliderGain;
    QLabel *labelExpo;
    QLabel *labelGain;
    QDoubleSpinBox *spinExpo;
    QLabel *labelXoffset;
    QLabel *labelYoffset;
    QSpinBox *SpinOffsetX;
    QSpinBox *spinOffsetY;
    QSlider *sliderOffsetX;
    QSlider *sliderOffsetY;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout;
    QLabel *labelCustomName;
    QLabel *labelManufacturer;
    QLabel *labelCameraModel;
    QLabel *labelSerialNum;
    QLabel *labelFormat;
    QGroupBox *groupBox_4;
    QFormLayout *formLayout;
    QLabel *labelWidth;
    QLabel *labelFPS;
    QLabel *labelHeight;
    QLabel *labelResFPS;
    QPushButton *buttonSnap;
    QGroupBox *groupBox_5;
    QFormLayout *formLayout_2;
    QPushButton *buttonZoomIn;
    QPushButton *buttonZoomOut;
    QPushButton *buttonReset;
    QLabel *labelZoom;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_2;
    QComboBox *comboBoxImageIndex;
    QPushButton *buttonSaveSettings;
    QPushButton *buttonSaveImage;
    QLabel *labelConnected;
    QLabel *labelFocusMeasure;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_2;
    QGraphicsView *imageView;

    void setupUi(QWidget *Camera)
    {
        if (Camera->objectName().isEmpty())
            Camera->setObjectName(QString::fromUtf8("Camera"));
        Camera->resize(1440, 920);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Camera->sizePolicy().hasHeightForWidth());
        Camera->setSizePolicy(sizePolicy);
        Camera->setMinimumSize(QSize(1440, 920));
        Camera->setMaximumSize(QSize(1440, 920));
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../../Users/MBvision/.designer/backup/MBicon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Camera->setWindowIcon(icon);
        groupBox = new QGroupBox(Camera);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(9, 11, 1422, 280));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMinimumSize(QSize(640, 280));
        groupBox->setMaximumSize(QSize(1920, 280));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(12);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        groupBox->setFont(font);
        groupBox->setCursor(QCursor(Qt::ArrowCursor));
        groupBox->setMouseTracking(true);
        groupBox->setTabletTracking(true);
        groupBox->setFlat(false);
        groupBox->setCheckable(false);
        buttonLive = new QPushButton(groupBox);
        buttonLive->setObjectName(QString::fromUtf8("buttonLive"));
        buttonLive->setGeometry(QRect(360, 190, 200, 40));
        buttonLive->setMinimumSize(QSize(200, 40));
        buttonLive->setMaximumSize(QSize(100, 40));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(16);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        buttonLive->setFont(font1);
        buttonLive->setAutoFillBackground(true);
        buttonLive->setStyleSheet(QString::fromUtf8(""));
        buttonLive->setCheckable(false);
        buttonLive->setAutoDefault(false);
        buttonLive->setFlat(false);
        groupBoxIntensity = new QGroupBox(groupBox);
        groupBoxIntensity->setObjectName(QString::fromUtf8("groupBoxIntensity"));
        groupBoxIntensity->setGeometry(QRect(776, 182, 401, 61));
        groupBoxIntensity->setMinimumSize(QSize(0, 60));
        horizontalLayout_2 = new QHBoxLayout(groupBoxIntensity);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        labelin = new QLabel(groupBoxIntensity);
        labelin->setObjectName(QString::fromUtf8("labelin"));
        labelin->setMinimumSize(QSize(60, 0));
        labelin->setFont(font);
        labelin->setFrameShape(QFrame::Box);
        labelin->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(labelin);

        labelB = new QLabel(groupBoxIntensity);
        labelB->setObjectName(QString::fromUtf8("labelB"));
        labelB->setMinimumSize(QSize(60, 0));
        labelB->setFont(font);
        labelB->setFrameShape(QFrame::Box);
        labelB->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(labelB);

        labelG = new QLabel(groupBoxIntensity);
        labelG->setObjectName(QString::fromUtf8("labelG"));
        labelG->setMinimumSize(QSize(60, 0));
        labelG->setMaximumSize(QSize(200, 16777215));
        labelG->setFont(font);
        labelG->setFrameShape(QFrame::Box);
        labelG->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(labelG);

        labelR = new QLabel(groupBoxIntensity);
        labelR->setObjectName(QString::fromUtf8("labelR"));
        labelR->setMinimumSize(QSize(60, 0));
        labelR->setFont(font);
        labelR->setFrameShape(QFrame::Box);
        labelR->setFrameShadow(QFrame::Raised);

        horizontalLayout_2->addWidget(labelR);

        groupBoxCoordinates = new QGroupBox(groupBox);
        groupBoxCoordinates->setObjectName(QString::fromUtf8("groupBoxCoordinates"));
        groupBoxCoordinates->setGeometry(QRect(1183, 182, 226, 60));
        groupBoxCoordinates->setMinimumSize(QSize(0, 60));
        groupBoxCoordinates->setMaximumSize(QSize(16777215, 60));
        horizontalLayout = new QHBoxLayout(groupBoxCoordinates);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        labelX = new QLabel(groupBoxCoordinates);
        labelX->setObjectName(QString::fromUtf8("labelX"));
        labelX->setMinimumSize(QSize(80, 0));
        labelX->setMaximumSize(QSize(200, 16777215));
        labelX->setFont(font);
        labelX->setFrameShape(QFrame::Box);
        labelX->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelX);

        labelY = new QLabel(groupBoxCoordinates);
        labelY->setObjectName(QString::fromUtf8("labelY"));
        labelY->setMinimumSize(QSize(80, 0));
        labelY->setMaximumSize(QSize(200, 16777215));
        labelY->setFont(font);
        labelY->setFrameShape(QFrame::Box);
        labelY->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelY);

        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(360, 10, 581, 171));
        groupBox_2->setFont(font);
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(10, 10, 10, 10);
        sliderExpo = new QSlider(groupBox_2);
        sliderExpo->setObjectName(QString::fromUtf8("sliderExpo"));
        sizePolicy1.setHeightForWidth(sliderExpo->sizePolicy().hasHeightForWidth());
        sliderExpo->setSizePolicy(sizePolicy1);
        sliderExpo->setMinimumSize(QSize(350, 25));
        sliderExpo->setMaximumSize(QSize(250, 30));
        sliderExpo->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderExpo, 1, 1, 1, 1);

        spinGain = new QSpinBox(groupBox_2);
        spinGain->setObjectName(QString::fromUtf8("spinGain"));
        spinGain->setMaximumSize(QSize(100, 16777215));

        gridLayout->addWidget(spinGain, 0, 2, 1, 1);

        sliderGain = new QSlider(groupBox_2);
        sliderGain->setObjectName(QString::fromUtf8("sliderGain"));
        sizePolicy1.setHeightForWidth(sliderGain->sizePolicy().hasHeightForWidth());
        sliderGain->setSizePolicy(sizePolicy1);
        sliderGain->setMinimumSize(QSize(350, 25));
        sliderGain->setMaximumSize(QSize(250, 30));
        sliderGain->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderGain, 0, 1, 1, 1);

        labelExpo = new QLabel(groupBox_2);
        labelExpo->setObjectName(QString::fromUtf8("labelExpo"));
        labelExpo->setMaximumSize(QSize(80, 16777215));
        labelExpo->setFont(font);

        gridLayout->addWidget(labelExpo, 1, 0, 1, 1);

        labelGain = new QLabel(groupBox_2);
        labelGain->setObjectName(QString::fromUtf8("labelGain"));
        labelGain->setMaximumSize(QSize(80, 16777215));
        labelGain->setFont(font);

        gridLayout->addWidget(labelGain, 0, 0, 1, 1);

        spinExpo = new QDoubleSpinBox(groupBox_2);
        spinExpo->setObjectName(QString::fromUtf8("spinExpo"));
        spinExpo->setMaximumSize(QSize(100, 16777215));

        gridLayout->addWidget(spinExpo, 1, 2, 1, 1);

        labelXoffset = new QLabel(groupBox_2);
        labelXoffset->setObjectName(QString::fromUtf8("labelXoffset"));
        labelXoffset->setMaximumSize(QSize(80, 16777215));
        labelXoffset->setFont(font);

        gridLayout->addWidget(labelXoffset, 2, 0, 1, 1);

        labelYoffset = new QLabel(groupBox_2);
        labelYoffset->setObjectName(QString::fromUtf8("labelYoffset"));
        labelYoffset->setMaximumSize(QSize(80, 16777215));
        labelYoffset->setFont(font);

        gridLayout->addWidget(labelYoffset, 3, 0, 1, 1);

        SpinOffsetX = new QSpinBox(groupBox_2);
        SpinOffsetX->setObjectName(QString::fromUtf8("SpinOffsetX"));
        SpinOffsetX->setMaximumSize(QSize(100, 16777215));

        gridLayout->addWidget(SpinOffsetX, 2, 2, 1, 1);

        spinOffsetY = new QSpinBox(groupBox_2);
        spinOffsetY->setObjectName(QString::fromUtf8("spinOffsetY"));
        spinOffsetY->setMaximumSize(QSize(100, 16777215));

        gridLayout->addWidget(spinOffsetY, 3, 2, 1, 1);

        sliderOffsetX = new QSlider(groupBox_2);
        sliderOffsetX->setObjectName(QString::fromUtf8("sliderOffsetX"));
        sliderOffsetX->setMinimumSize(QSize(350, 25));
        sliderOffsetX->setMaximumSize(QSize(250, 16777215));
        sliderOffsetX->setSingleStep(4);
        sliderOffsetX->setOrientation(Qt::Horizontal);
        sliderOffsetX->setTickInterval(4);

        gridLayout->addWidget(sliderOffsetX, 2, 1, 1, 1);

        sliderOffsetY = new QSlider(groupBox_2);
        sliderOffsetY->setObjectName(QString::fromUtf8("sliderOffsetY"));
        sliderOffsetY->setMinimumSize(QSize(350, 25));
        sliderOffsetY->setMaximumSize(QSize(350, 16777215));
        sliderOffsetY->setSingleStep(14);
        sliderOffsetY->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderOffsetY, 3, 1, 1, 1);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 10, 341, 261));
        groupBox_3->setFont(font);
        verticalLayout = new QVBoxLayout(groupBox_3);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        labelCustomName = new QLabel(groupBox_3);
        labelCustomName->setObjectName(QString::fromUtf8("labelCustomName"));
        labelCustomName->setMinimumSize(QSize(0, 25));
        labelCustomName->setMaximumSize(QSize(16777215, 20));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Calibri"));
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        labelCustomName->setFont(font2);
        labelCustomName->setFrameShape(QFrame::Box);
        labelCustomName->setFrameShadow(QFrame::Raised);

        verticalLayout->addWidget(labelCustomName);

        labelManufacturer = new QLabel(groupBox_3);
        labelManufacturer->setObjectName(QString::fromUtf8("labelManufacturer"));
        labelManufacturer->setMinimumSize(QSize(0, 25));
        labelManufacturer->setMaximumSize(QSize(16777215, 20));
        labelManufacturer->setFont(font2);
        labelManufacturer->setFrameShape(QFrame::Box);
        labelManufacturer->setFrameShadow(QFrame::Raised);

        verticalLayout->addWidget(labelManufacturer);

        labelCameraModel = new QLabel(groupBox_3);
        labelCameraModel->setObjectName(QString::fromUtf8("labelCameraModel"));
        labelCameraModel->setMinimumSize(QSize(0, 25));
        labelCameraModel->setMaximumSize(QSize(16777215, 20));
        labelCameraModel->setFont(font2);
        labelCameraModel->setFrameShape(QFrame::Box);
        labelCameraModel->setFrameShadow(QFrame::Raised);

        verticalLayout->addWidget(labelCameraModel);

        labelSerialNum = new QLabel(groupBox_3);
        labelSerialNum->setObjectName(QString::fromUtf8("labelSerialNum"));
        labelSerialNum->setMinimumSize(QSize(0, 25));
        labelSerialNum->setMaximumSize(QSize(16777215, 20));
        labelSerialNum->setFont(font2);
        labelSerialNum->setFrameShape(QFrame::Box);
        labelSerialNum->setFrameShadow(QFrame::Raised);

        verticalLayout->addWidget(labelSerialNum);

        labelFormat = new QLabel(groupBox_3);
        labelFormat->setObjectName(QString::fromUtf8("labelFormat"));
        labelFormat->setMinimumSize(QSize(0, 25));
        labelFormat->setMaximumSize(QSize(16777215, 20));
        labelFormat->setFont(font2);
        labelFormat->setFrameShape(QFrame::Box);
        labelFormat->setFrameShadow(QFrame::Raised);

        verticalLayout->addWidget(labelFormat);

        groupBox_4 = new QGroupBox(groupBox_3);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        QFont font3;
        font3.setPointSize(10);
        font3.setBold(true);
        font3.setItalic(true);
        font3.setWeight(75);
        groupBox_4->setFont(font3);
        formLayout = new QFormLayout(groupBox_4);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        labelWidth = new QLabel(groupBox_4);
        labelWidth->setObjectName(QString::fromUtf8("labelWidth"));
        labelWidth->setMinimumSize(QSize(140, 25));
        labelWidth->setMaximumSize(QSize(16777215, 20));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Calibri"));
        font4.setPointSize(10);
        font4.setBold(true);
        font4.setItalic(true);
        font4.setWeight(75);
        labelWidth->setFont(font4);
        labelWidth->setFrameShape(QFrame::Box);
        labelWidth->setFrameShadow(QFrame::Raised);

        formLayout->setWidget(0, QFormLayout::LabelRole, labelWidth);

        labelFPS = new QLabel(groupBox_4);
        labelFPS->setObjectName(QString::fromUtf8("labelFPS"));
        labelFPS->setMinimumSize(QSize(140, 25));
        labelFPS->setMaximumSize(QSize(16777215, 20));
        labelFPS->setFont(font4);
        labelFPS->setFrameShape(QFrame::Box);
        labelFPS->setFrameShadow(QFrame::Raised);

        formLayout->setWidget(0, QFormLayout::FieldRole, labelFPS);

        labelHeight = new QLabel(groupBox_4);
        labelHeight->setObjectName(QString::fromUtf8("labelHeight"));
        labelHeight->setMinimumSize(QSize(140, 25));
        labelHeight->setMaximumSize(QSize(16777215, 20));
        labelHeight->setFont(font4);
        labelHeight->setFrameShape(QFrame::Box);
        labelHeight->setFrameShadow(QFrame::Raised);

        formLayout->setWidget(1, QFormLayout::LabelRole, labelHeight);

        labelResFPS = new QLabel(groupBox_4);
        labelResFPS->setObjectName(QString::fromUtf8("labelResFPS"));
        labelResFPS->setMinimumSize(QSize(140, 25));
        labelResFPS->setMaximumSize(QSize(16777215, 20));
        labelResFPS->setFont(font4);
        labelResFPS->setFrameShape(QFrame::Box);
        labelResFPS->setFrameShadow(QFrame::Raised);

        formLayout->setWidget(1, QFormLayout::FieldRole, labelResFPS);


        verticalLayout->addWidget(groupBox_4);

        buttonSnap = new QPushButton(groupBox);
        buttonSnap->setObjectName(QString::fromUtf8("buttonSnap"));
        buttonSnap->setGeometry(QRect(570, 190, 200, 40));
        buttonSnap->setMinimumSize(QSize(200, 40));
        buttonSnap->setMaximumSize(QSize(100, 40));
        buttonSnap->setFont(font1);
        buttonSnap->setAutoFillBackground(true);
        buttonSnap->setStyleSheet(QString::fromUtf8(""));
        buttonSnap->setCheckable(false);
        buttonSnap->setAutoDefault(false);
        buttonSnap->setFlat(false);
        groupBox_5 = new QGroupBox(groupBox);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(1210, 90, 201, 91));
        groupBox_5->setFont(font);
        formLayout_2 = new QFormLayout(groupBox_5);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        buttonZoomIn = new QPushButton(groupBox_5);
        buttonZoomIn->setObjectName(QString::fromUtf8("buttonZoomIn"));
        buttonZoomIn->setMinimumSize(QSize(50, 0));
        buttonZoomIn->setMaximumSize(QSize(100, 16777215));
        buttonZoomIn->setFont(font4);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, buttonZoomIn);

        buttonZoomOut = new QPushButton(groupBox_5);
        buttonZoomOut->setObjectName(QString::fromUtf8("buttonZoomOut"));
        buttonZoomOut->setMinimumSize(QSize(50, 0));
        buttonZoomOut->setMaximumSize(QSize(100, 16777215));
        buttonZoomOut->setFont(font4);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, buttonZoomOut);

        buttonReset = new QPushButton(groupBox_5);
        buttonReset->setObjectName(QString::fromUtf8("buttonReset"));
        buttonReset->setMinimumSize(QSize(50, 0));
        buttonReset->setMaximumSize(QSize(100, 16777215));
        buttonReset->setFont(font4);

        formLayout_2->setWidget(1, QFormLayout::LabelRole, buttonReset);

        labelZoom = new QLabel(groupBox_5);
        labelZoom->setObjectName(QString::fromUtf8("labelZoom"));
        labelZoom->setMinimumSize(QSize(50, 0));
        labelZoom->setMaximumSize(QSize(100, 16777215));
        labelZoom->setFont(font);
        labelZoom->setFrameShape(QFrame::Box);
        labelZoom->setFrameShadow(QFrame::Raised);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, labelZoom);

        groupBox_6 = new QGroupBox(groupBox);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(950, 30, 231, 141));
        gridLayout_2 = new QGridLayout(groupBox_6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        comboBoxImageIndex = new QComboBox(groupBox_6);
        comboBoxImageIndex->setObjectName(QString::fromUtf8("comboBoxImageIndex"));
        comboBoxImageIndex->setMinimumSize(QSize(0, 30));

        gridLayout_2->addWidget(comboBoxImageIndex, 0, 0, 1, 1);

        buttonSaveSettings = new QPushButton(groupBox_6);
        buttonSaveSettings->setObjectName(QString::fromUtf8("buttonSaveSettings"));
        buttonSaveSettings->setMinimumSize(QSize(0, 30));
        buttonSaveSettings->setFont(font4);
        buttonSaveSettings->setAutoFillBackground(false);

        gridLayout_2->addWidget(buttonSaveSettings, 0, 1, 1, 1);

        buttonSaveImage = new QPushButton(groupBox_6);
        buttonSaveImage->setObjectName(QString::fromUtf8("buttonSaveImage"));
        buttonSaveImage->setMinimumSize(QSize(0, 30));
        buttonSaveImage->setFont(font4);
        buttonSaveImage->setCheckable(false);
        buttonSaveImage->setChecked(false);
        buttonSaveImage->setAutoRepeat(false);
        buttonSaveImage->setAutoExclusive(false);
        buttonSaveImage->setAutoDefault(false);
        buttonSaveImage->setFlat(false);

        gridLayout_2->addWidget(buttonSaveImage, 1, 1, 1, 1);

        labelConnected = new QLabel(groupBox);
        labelConnected->setObjectName(QString::fromUtf8("labelConnected"));
        labelConnected->setGeometry(QRect(1210, 10, 201, 31));
        labelConnected->setFrameShape(QFrame::Panel);
        labelFocusMeasure = new QLabel(groupBox);
        labelFocusMeasure->setObjectName(QString::fromUtf8("labelFocusMeasure"));
        labelFocusMeasure->setGeometry(QRect(1210, 50, 201, 31));
        labelFocusMeasure->setFrameShape(QFrame::Box);
        labelFocusMeasure->setFrameShadow(QFrame::Sunken);
        labelFocusMeasure->setLineWidth(2);
        verticalLayoutWidget = new QWidget(Camera);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 300, 1421, 611));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetFixedSize);
        verticalLayout_2->setContentsMargins(10, 10, 10, 10);
        imageView = new QGraphicsView(verticalLayoutWidget);
        imageView->setObjectName(QString::fromUtf8("imageView"));
        sizePolicy1.setHeightForWidth(imageView->sizePolicy().hasHeightForWidth());
        imageView->setSizePolicy(sizePolicy1);
        imageView->setMinimumSize(QSize(1400, 500));
        imageView->setMaximumSize(QSize(1400, 580));
        imageView->viewport()->setProperty("cursor", QVariant(QCursor(Qt::CrossCursor)));
        imageView->setMouseTracking(true);
        imageView->setTabletTracking(true);

        verticalLayout_2->addWidget(imageView);


        retranslateUi(Camera);

        buttonLive->setDefault(false);
        buttonSnap->setDefault(false);
        buttonSaveImage->setDefault(false);


        QMetaObject::connectSlotsByName(Camera);
    } // setupUi

    void retranslateUi(QWidget *Camera)
    {
        Camera->setWindowTitle(QCoreApplication::translate("Camera", "Form", nullptr));
        groupBox->setTitle(QString());
        buttonLive->setText(QCoreApplication::translate("Camera", "Enable Live", nullptr));
        groupBoxIntensity->setTitle(QCoreApplication::translate("Camera", "Intensity:", nullptr));
        labelin->setText(QString());
        labelB->setText(QString());
        labelG->setText(QString());
        labelR->setText(QString());
        groupBoxCoordinates->setTitle(QCoreApplication::translate("Camera", "Coordinates:", nullptr));
        labelX->setText(QString());
        labelY->setText(QString());
        groupBox_2->setTitle(QCoreApplication::translate("Camera", "Settings:", nullptr));
        labelExpo->setText(QCoreApplication::translate("Camera", "Expositure:", nullptr));
        labelGain->setText(QCoreApplication::translate("Camera", "Gain:", nullptr));
        labelXoffset->setText(QCoreApplication::translate("Camera", "X offest:", nullptr));
        labelYoffset->setText(QCoreApplication::translate("Camera", "Y offset:", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("Camera", "Propertys:", nullptr));
        labelCustomName->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        labelManufacturer->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        labelCameraModel->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        labelSerialNum->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        labelFormat->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("Camera", "Image:", nullptr));
        labelWidth->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        labelFPS->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        labelHeight->setText(QCoreApplication::translate("Camera", "blabablla", nullptr));
        labelResFPS->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        buttonSnap->setText(QCoreApplication::translate("Camera", "SNAP ONE", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("Camera", "Zoom:", nullptr));
        buttonZoomIn->setText(QCoreApplication::translate("Camera", "Zoom In + ", nullptr));
        buttonZoomOut->setText(QCoreApplication::translate("Camera", "ZoomOut - ", nullptr));
        buttonReset->setText(QCoreApplication::translate("Camera", "Reset Zoom", nullptr));
        labelZoom->setText(QString());
        groupBox_6->setTitle(QCoreApplication::translate("Camera", "Image settings", nullptr));
        buttonSaveSettings->setText(QCoreApplication::translate("Camera", "SaveSettings", nullptr));
        buttonSaveImage->setText(QCoreApplication::translate("Camera", "SaveImage", nullptr));
        labelConnected->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
        labelFocusMeasure->setText(QCoreApplication::translate("Camera", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Camera: public Ui_Camera {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERA_H
