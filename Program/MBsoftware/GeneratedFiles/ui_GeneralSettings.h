/********************************************************************************
** Form generated from reading UI file 'GeneralSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GENERALSETTINGS_H
#define UI_GENERALSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GeneralSettings
{
public:
    QDialogButtonBox *buttonBox;
    QGroupBox *groupBox_5;
    QComboBox *comboBox;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineNrDowelsAvtoStop;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_4;
    QCheckBox *checkPresortStopEnable;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_12;
    QLineEdit *editBlowTimeX;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_10;
    QLineEdit *editBlowOffsetX;
    QComboBox *comboPresortMode;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_16;
    QLineEdit *editPresortStopNrPieces;
    QGroupBox *groupBox_9;
    QGridLayout *gridLayout_3;
    QCheckBox *checkPresortSensorEnable;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *editBlowTimeY;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_8;
    QLineEdit *editBlowOffsetY;
    QGroupBox *groupBox_10;
    QComboBox *comboRotatorSpeed;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_9;
    QLineEdit *editElevatorStart;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_13;
    QLineEdit *ediElevatorStop;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_14;
    QLineEdit *editDowelStuckBlink;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_15;
    QLineEdit *editDowelStuckStop;
    QGroupBox *groupBox_11;
    QGridLayout *gridLayout;
    QLabel *labelTimeBlow;
    QLineEdit *editTimeBlow;
    QLabel *label_3;
    QLineEdit *editOffsetToBlow;
    QGroupBox *groupBox_12;
    QGridLayout *gridLayout_2;
    QLineEdit *editCleanPeriode;
    QLabel *label_6;
    QLabel *label_5;
    QCheckBox *checkStartClean;
    QLabel *labelTimeBlow_2;
    QLineEdit *editCleanValve2Offset;
    QLabel *label_4;
    QLineEdit *editCleanValve1Offset;
    QCheckBox *checkCleanEnable;
    QLineEdit *editCleanValve1Duration;
    QLabel *label_7;
    QLineEdit *editCleanValve2Duration;

    void setupUi(QWidget *GeneralSettings)
    {
        if (GeneralSettings->objectName().isEmpty())
            GeneralSettings->setObjectName(QString::fromUtf8("GeneralSettings"));
        GeneralSettings->resize(1009, 568);
        buttonBox = new QDialogButtonBox(GeneralSettings);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(680, 530, 301, 31));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(buttonBox->sizePolicy().hasHeightForWidth());
        buttonBox->setSizePolicy(sizePolicy);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        groupBox_5 = new QGroupBox(GeneralSettings);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 10, 341, 521));
        QFont font;
        font.setFamily(QString::fromUtf8("Times New Roman"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        groupBox_5->setFont(font);
        comboBox = new QComboBox(groupBox_5);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(10, 30, 301, 31));
        horizontalLayoutWidget = new QWidget(groupBox_5);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 80, 301, 31));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Times New Roman"));
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setWeight(50);
        label->setFont(font1);

        horizontalLayout->addWidget(label);

        lineNrDowelsAvtoStop = new QLineEdit(horizontalLayoutWidget);
        lineNrDowelsAvtoStop->setObjectName(QString::fromUtf8("lineNrDowelsAvtoStop"));
        lineNrDowelsAvtoStop->setMinimumSize(QSize(80, 0));
        lineNrDowelsAvtoStop->setMaximumSize(QSize(80, 40));

        horizontalLayout->addWidget(lineNrDowelsAvtoStop);

        groupBox_7 = new QGroupBox(groupBox_5);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setGeometry(QRect(0, 133, 321, 201));
        groupBox_7->setFont(font);
        gridLayout_4 = new QGridLayout(groupBox_7);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        checkPresortStopEnable = new QCheckBox(groupBox_7);
        checkPresortStopEnable->setObjectName(QString::fromUtf8("checkPresortStopEnable"));
        checkPresortStopEnable->setFont(font);

        gridLayout_4->addWidget(checkPresortStopEnable, 3, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_12 = new QLabel(groupBox_7);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setFont(font1);

        horizontalLayout_4->addWidget(label_12);

        editBlowTimeX = new QLineEdit(groupBox_7);
        editBlowTimeX->setObjectName(QString::fromUtf8("editBlowTimeX"));
        editBlowTimeX->setMinimumSize(QSize(80, 0));
        editBlowTimeX->setMaximumSize(QSize(80, 40));

        horizontalLayout_4->addWidget(editBlowTimeX);


        gridLayout_4->addLayout(horizontalLayout_4, 1, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_10 = new QLabel(groupBox_7);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font1);

        horizontalLayout_5->addWidget(label_10);

        editBlowOffsetX = new QLineEdit(groupBox_7);
        editBlowOffsetX->setObjectName(QString::fromUtf8("editBlowOffsetX"));
        editBlowOffsetX->setMinimumSize(QSize(80, 0));
        editBlowOffsetX->setMaximumSize(QSize(80, 40));

        horizontalLayout_5->addWidget(editBlowOffsetX);


        gridLayout_4->addLayout(horizontalLayout_5, 2, 0, 1, 1);

        comboPresortMode = new QComboBox(groupBox_7);
        comboPresortMode->setObjectName(QString::fromUtf8("comboPresortMode"));

        gridLayout_4->addWidget(comboPresortMode, 0, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_16 = new QLabel(groupBox_7);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font1);

        horizontalLayout_6->addWidget(label_16);

        editPresortStopNrPieces = new QLineEdit(groupBox_7);
        editPresortStopNrPieces->setObjectName(QString::fromUtf8("editPresortStopNrPieces"));
        editPresortStopNrPieces->setMinimumSize(QSize(80, 0));
        editPresortStopNrPieces->setMaximumSize(QSize(80, 40));

        horizontalLayout_6->addWidget(editPresortStopNrPieces);


        gridLayout_4->addLayout(horizontalLayout_6, 4, 0, 1, 1);

        groupBox_9 = new QGroupBox(groupBox_5);
        groupBox_9->setObjectName(QString::fromUtf8("groupBox_9"));
        groupBox_9->setGeometry(QRect(10, 370, 311, 131));
        groupBox_9->setFont(font);
        gridLayout_3 = new QGridLayout(groupBox_9);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        checkPresortSensorEnable = new QCheckBox(groupBox_9);
        checkPresortSensorEnable->setObjectName(QString::fromUtf8("checkPresortSensorEnable"));
        checkPresortSensorEnable->setFont(font);

        gridLayout_3->addWidget(checkPresortSensorEnable, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_9);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font1);

        horizontalLayout_2->addWidget(label_2);

        editBlowTimeY = new QLineEdit(groupBox_9);
        editBlowTimeY->setObjectName(QString::fromUtf8("editBlowTimeY"));
        editBlowTimeY->setMinimumSize(QSize(80, 0));
        editBlowTimeY->setMaximumSize(QSize(80, 40));

        horizontalLayout_2->addWidget(editBlowTimeY);


        gridLayout_3->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_8 = new QLabel(groupBox_9);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font1);

        horizontalLayout_3->addWidget(label_8);

        editBlowOffsetY = new QLineEdit(groupBox_9);
        editBlowOffsetY->setObjectName(QString::fromUtf8("editBlowOffsetY"));
        editBlowOffsetY->setMinimumSize(QSize(80, 0));
        editBlowOffsetY->setMaximumSize(QSize(80, 40));

        horizontalLayout_3->addWidget(editBlowOffsetY);


        gridLayout_3->addLayout(horizontalLayout_3, 2, 0, 1, 1);

        groupBox_10 = new QGroupBox(GeneralSettings);
        groupBox_10->setObjectName(QString::fromUtf8("groupBox_10"));
        groupBox_10->setGeometry(QRect(360, 340, 601, 161));
        groupBox_10->setFont(font);
        comboRotatorSpeed = new QComboBox(groupBox_10);
        comboRotatorSpeed->setObjectName(QString::fromUtf8("comboRotatorSpeed"));
        comboRotatorSpeed->setGeometry(QRect(330, 20, 261, 31));
        verticalLayoutWidget = new QWidget(groupBox_10);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 20, 311, 128));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_9 = new QLabel(verticalLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font1);

        horizontalLayout_7->addWidget(label_9);

        editElevatorStart = new QLineEdit(verticalLayoutWidget);
        editElevatorStart->setObjectName(QString::fromUtf8("editElevatorStart"));
        editElevatorStart->setMinimumSize(QSize(80, 0));
        editElevatorStart->setMaximumSize(QSize(80, 40));

        horizontalLayout_7->addWidget(editElevatorStart);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_13 = new QLabel(verticalLayoutWidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setFont(font1);

        horizontalLayout_8->addWidget(label_13);

        ediElevatorStop = new QLineEdit(verticalLayoutWidget);
        ediElevatorStop->setObjectName(QString::fromUtf8("ediElevatorStop"));
        ediElevatorStop->setMinimumSize(QSize(80, 0));
        ediElevatorStop->setMaximumSize(QSize(80, 40));

        horizontalLayout_8->addWidget(ediElevatorStop);


        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_14 = new QLabel(verticalLayoutWidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font1);

        horizontalLayout_9->addWidget(label_14);

        editDowelStuckBlink = new QLineEdit(verticalLayoutWidget);
        editDowelStuckBlink->setObjectName(QString::fromUtf8("editDowelStuckBlink"));
        editDowelStuckBlink->setMinimumSize(QSize(80, 0));
        editDowelStuckBlink->setMaximumSize(QSize(80, 40));

        horizontalLayout_9->addWidget(editDowelStuckBlink);


        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_15 = new QLabel(verticalLayoutWidget);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font1);

        horizontalLayout_10->addWidget(label_15);

        editDowelStuckStop = new QLineEdit(verticalLayoutWidget);
        editDowelStuckStop->setObjectName(QString::fromUtf8("editDowelStuckStop"));
        editDowelStuckStop->setMinimumSize(QSize(80, 0));
        editDowelStuckStop->setMaximumSize(QSize(80, 40));

        horizontalLayout_10->addWidget(editDowelStuckStop);


        verticalLayout->addLayout(horizontalLayout_10);

        groupBox_11 = new QGroupBox(GeneralSettings);
        groupBox_11->setObjectName(QString::fromUtf8("groupBox_11"));
        groupBox_11->setGeometry(QRect(360, 40, 601, 95));
        groupBox_11->setFont(font);
        gridLayout = new QGridLayout(groupBox_11);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        labelTimeBlow = new QLabel(groupBox_11);
        labelTimeBlow->setObjectName(QString::fromUtf8("labelTimeBlow"));
        labelTimeBlow->setFont(font1);

        gridLayout->addWidget(labelTimeBlow, 0, 0, 1, 1);

        editTimeBlow = new QLineEdit(groupBox_11);
        editTimeBlow->setObjectName(QString::fromUtf8("editTimeBlow"));
        editTimeBlow->setMinimumSize(QSize(80, 0));
        editTimeBlow->setMaximumSize(QSize(80, 40));

        gridLayout->addWidget(editTimeBlow, 0, 1, 1, 1);

        label_3 = new QLabel(groupBox_11);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        editOffsetToBlow = new QLineEdit(groupBox_11);
        editOffsetToBlow->setObjectName(QString::fromUtf8("editOffsetToBlow"));
        editOffsetToBlow->setMinimumSize(QSize(80, 0));
        editOffsetToBlow->setMaximumSize(QSize(80, 40));

        gridLayout->addWidget(editOffsetToBlow, 1, 1, 1, 1);

        groupBox_12 = new QGroupBox(GeneralSettings);
        groupBox_12->setObjectName(QString::fromUtf8("groupBox_12"));
        groupBox_12->setGeometry(QRect(360, 140, 601, 191));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Times New Roman"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setItalic(false);
        font2.setWeight(75);
        groupBox_12->setFont(font2);
        gridLayout_2 = new QGridLayout(groupBox_12);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        editCleanPeriode = new QLineEdit(groupBox_12);
        editCleanPeriode->setObjectName(QString::fromUtf8("editCleanPeriode"));
        editCleanPeriode->setMinimumSize(QSize(80, 0));
        editCleanPeriode->setMaximumSize(QSize(80, 40));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Times New Roman"));
        font3.setPointSize(12);
        font3.setBold(false);
        font3.setItalic(false);
        font3.setWeight(50);
        editCleanPeriode->setFont(font3);

        gridLayout_2->addWidget(editCleanPeriode, 3, 1, 1, 1);

        label_6 = new QLabel(groupBox_12);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMaximumSize(QSize(16777215, 40));
        label_6->setFont(font3);

        gridLayout_2->addWidget(label_6, 4, 2, 1, 1);

        label_5 = new QLabel(groupBox_12);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMaximumSize(QSize(16777215, 40));
        label_5->setFont(font3);

        gridLayout_2->addWidget(label_5, 5, 0, 1, 1);

        checkStartClean = new QCheckBox(groupBox_12);
        checkStartClean->setObjectName(QString::fromUtf8("checkStartClean"));
        checkStartClean->setMaximumSize(QSize(16777215, 40));
        checkStartClean->setFont(font3);

        gridLayout_2->addWidget(checkStartClean, 1, 0, 1, 1);

        labelTimeBlow_2 = new QLabel(groupBox_12);
        labelTimeBlow_2->setObjectName(QString::fromUtf8("labelTimeBlow_2"));
        labelTimeBlow_2->setMaximumSize(QSize(16777215, 40));
        labelTimeBlow_2->setFont(font3);

        gridLayout_2->addWidget(labelTimeBlow_2, 3, 0, 1, 1);

        editCleanValve2Offset = new QLineEdit(groupBox_12);
        editCleanValve2Offset->setObjectName(QString::fromUtf8("editCleanValve2Offset"));
        editCleanValve2Offset->setMinimumSize(QSize(80, 0));
        editCleanValve2Offset->setMaximumSize(QSize(80, 40));
        editCleanValve2Offset->setFont(font3);

        gridLayout_2->addWidget(editCleanValve2Offset, 5, 1, 1, 1);

        label_4 = new QLabel(groupBox_12);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMaximumSize(QSize(16777215, 40));
        label_4->setFont(font3);

        gridLayout_2->addWidget(label_4, 4, 0, 1, 1);

        editCleanValve1Offset = new QLineEdit(groupBox_12);
        editCleanValve1Offset->setObjectName(QString::fromUtf8("editCleanValve1Offset"));
        editCleanValve1Offset->setMinimumSize(QSize(80, 0));
        editCleanValve1Offset->setMaximumSize(QSize(80, 40));
        editCleanValve1Offset->setFont(font3);

        gridLayout_2->addWidget(editCleanValve1Offset, 4, 1, 1, 1);

        checkCleanEnable = new QCheckBox(groupBox_12);
        checkCleanEnable->setObjectName(QString::fromUtf8("checkCleanEnable"));
        checkCleanEnable->setMaximumSize(QSize(16777215, 40));
        checkCleanEnable->setFont(font3);

        gridLayout_2->addWidget(checkCleanEnable, 2, 0, 1, 1);

        editCleanValve1Duration = new QLineEdit(groupBox_12);
        editCleanValve1Duration->setObjectName(QString::fromUtf8("editCleanValve1Duration"));
        editCleanValve1Duration->setMinimumSize(QSize(80, 0));
        editCleanValve1Duration->setMaximumSize(QSize(80, 40));
        editCleanValve1Duration->setFont(font3);

        gridLayout_2->addWidget(editCleanValve1Duration, 4, 3, 1, 1);

        label_7 = new QLabel(groupBox_12);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMaximumSize(QSize(16777215, 40));
        label_7->setFont(font3);

        gridLayout_2->addWidget(label_7, 5, 2, 1, 1);

        editCleanValve2Duration = new QLineEdit(groupBox_12);
        editCleanValve2Duration->setObjectName(QString::fromUtf8("editCleanValve2Duration"));
        editCleanValve2Duration->setMinimumSize(QSize(80, 0));
        editCleanValve2Duration->setMaximumSize(QSize(80, 40));
        editCleanValve2Duration->setFont(font3);

        gridLayout_2->addWidget(editCleanValve2Duration, 5, 3, 1, 1);


        retranslateUi(GeneralSettings);

        QMetaObject::connectSlotsByName(GeneralSettings);
    } // setupUi

    void retranslateUi(QWidget *GeneralSettings)
    {
        GeneralSettings->setWindowTitle(QCoreApplication::translate("GeneralSettings", "GeneralSettings", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("GeneralSettings", "Working Mode", nullptr));
        label->setText(QCoreApplication::translate("GeneralSettings", "Set nr. dowels  to AutoStop:", nullptr));
        groupBox_7->setTitle(QCoreApplication::translate("GeneralSettings", " Presorting Camera  Mode", nullptr));
        checkPresortStopEnable->setText(QCoreApplication::translate("GeneralSettings", "Device stop Enable", nullptr));
        label_12->setText(QCoreApplication::translate("GeneralSettings", "Presorting valve (X) blow time [ms]", nullptr));
        label_10->setText(QCoreApplication::translate("GeneralSettings", "Presorting valve (X) offset [mm]", nullptr));
        label_16->setText(QCoreApplication::translate("GeneralSettings", "Stop Device after nr. Bad pieces:", nullptr));
        groupBox_9->setTitle(QCoreApplication::translate("GeneralSettings", "Presorting Sensor", nullptr));
        checkPresortSensorEnable->setText(QCoreApplication::translate("GeneralSettings", "Presorting sensor Enable", nullptr));
        label_2->setText(QCoreApplication::translate("GeneralSettings", "Presorting valve(Y)blow time [ms]", nullptr));
        label_8->setText(QCoreApplication::translate("GeneralSettings", "Presorting valve (Y) offset [mm]", nullptr));
        groupBox_10->setTitle(QCoreApplication::translate("GeneralSettings", "Rotator Settings, Elevator Settings", nullptr));
        label_9->setText(QCoreApplication::translate("GeneralSettings", "Elevator start time [ms]:", nullptr));
        label_13->setText(QCoreApplication::translate("GeneralSettings", "Elevator stop tims [ms]:", nullptr));
        label_14->setText(QCoreApplication::translate("GeneralSettings", "Dowel stuck: Start blinking [s]:", nullptr));
        label_15->setText(QCoreApplication::translate("GeneralSettings", "Dowel stuck.Stop mashine time [s]:", nullptr));
        groupBox_11->setTitle(QCoreApplication::translate("GeneralSettings", "Automatic Blow Valve Settings", nullptr));
        labelTimeBlow->setText(QCoreApplication::translate("GeneralSettings", "Valve Blow Time factor [ms]:", nullptr));
        label_3->setText(QCoreApplication::translate("GeneralSettings", "Offset to Valve [mm]:", nullptr));
        groupBox_12->setTitle(QCoreApplication::translate("GeneralSettings", "Cleaning Settings", nullptr));
        label_6->setText(QCoreApplication::translate("GeneralSettings", "Clean mirror ON duration [ms]", nullptr));
        label_5->setText(QCoreApplication::translate("GeneralSettings", "Time to start  clean DMS light[ms]", nullptr));
        checkStartClean->setText(QCoreApplication::translate("GeneralSettings", "Start Button Cleaning Enable", nullptr));
        labelTimeBlow_2->setText(QCoreApplication::translate("GeneralSettings", "Clean periode [min]", nullptr));
        label_4->setText(QCoreApplication::translate("GeneralSettings", "Time to start  clean  mirror [ms]", nullptr));
        checkCleanEnable->setText(QCoreApplication::translate("GeneralSettings", "Cleaning enable", nullptr));
        label_7->setText(QCoreApplication::translate("GeneralSettings", "Clean light ON duration [ms]", nullptr));
    } // retranslateUi

};

namespace Ui {
    class GeneralSettings: public Ui_GeneralSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GENERALSETTINGS_H
