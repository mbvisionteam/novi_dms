/********************************************************************************
** Form generated from reading UI file 'DMScalibration.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DMSCALIBRATION_H
#define UI_DMSCALIBRATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DMSCalibrationUI
{
public:
    QPushButton *buttonAutoCalibration;
    QPushButton *buttonClose;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_17;
    QLabel *labelCen3;
    QLabel *labelCen1;
    QLabel *labelDix2;
    QLabel *labelCen4;
    QLabel *labelCen5;
    QLabel *label_20;
    QLabel *label_13;
    QLabel *label_16;
    QLabel *label_18;
    QLabel *labelCen2;
    QLabel *label_15;
    QLabel *labelDix3;
    QLabel *labelDix5;
    QLabel *label_23;
    QLabel *label_21;
    QLabel *labelDix4;
    QLabel *labelDia1;
    QLabel *labelDix1;
    QLabel *label_22;
    QLabel *labelDia2;
    QLabel *labelDia3;
    QLabel *labelDia4;
    QLabel *labelDia5;
    QLabel *labelDiaC1;
    QLabel *labelDiaC2;
    QLabel *labelDiaC3;
    QLabel *labelDiaC4;
    QLabel *labelDiaC5;
    QLabel *label_14;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEditKorFactor2;
    QLabel *label_27;
    QLabel *label_26;
    QLabel *label_28;
    QLineEdit *lineEditCenter3;
    QLineEdit *lineEditCenter4;
    QLabel *label_25;
    QLineEdit *lineEditCenter2;
    QLabel *label_19;
    QLineEdit *lineEditCenter5;
    QLabel *label_29;
    QLineEdit *lineEditKorFactor5;
    QLineEdit *lineEditKorFactor4;
    QLabel *label_24;
    QLineEdit *lineEditKorFactor3;
    QLineEdit *lineEditCenter1;
    QLineEdit *lineEditKorFactor1;
    QPushButton *buttonReadRefCenter;
    QPushButton *buttonCalcKorFactor;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_3;
    QLabel *labelRefDiameter;
    QLabel *labelRefLenght;
    QLabel *label_32;
    QLabel *label_33;
    QPushButton *buttonSave;
    QPushButton *buttonEnterRefDowel;
    QLabel *labelRefDiameter_2;
    QLabel *labelSelectedCalibration;
    QWidget *gridLayoutWidget_4;
    QGridLayout *gridLayout_4;
    QLineEdit *lineEditKorFactor0;
    QLabel *label_31;
    QLabel *label_30;
    QPushButton *buttonCalcKorFactorWidth;
    QWidget *gridLayoutWidget_5;
    QGridLayout *gridLayout_5;
    QLabel *label_35;
    QLabel *labelWidthPix;
    QLabel *label_34;
    QLabel *labelWidthReal;
    QLabel *label_36;

    void setupUi(QWidget *DMSCalibrationUI)
    {
        if (DMSCalibrationUI->objectName().isEmpty())
            DMSCalibrationUI->setObjectName(QString::fromUtf8("DMSCalibrationUI"));
        DMSCalibrationUI->resize(1198, 687);
        buttonAutoCalibration = new QPushButton(DMSCalibrationUI);
        buttonAutoCalibration->setObjectName(QString::fromUtf8("buttonAutoCalibration"));
        buttonAutoCalibration->setGeometry(QRect(210, 300, 391, 51));
        buttonClose = new QPushButton(DMSCalibrationUI);
        buttonClose->setObjectName(QString::fromUtf8("buttonClose"));
        buttonClose->setGeometry(QRect(1060, 630, 121, 51));
        gridLayoutWidget = new QWidget(DMSCalibrationUI);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 370, 671, 142));
        QFont font;
        font.setFamily(QString::fromUtf8("Times New Roman"));
        font.setPointSize(12);
        gridLayoutWidget->setFont(font);
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_17 = new QLabel(gridLayoutWidget);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Times New Roman"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        label_17->setFont(font1);
        label_17->setFrameShape(QFrame::WinPanel);
        label_17->setFrameShadow(QFrame::Sunken);
        label_17->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_17, 0, 4, 1, 1);

        labelCen3 = new QLabel(gridLayoutWidget);
        labelCen3->setObjectName(QString::fromUtf8("labelCen3"));
        labelCen3->setFont(font);
        labelCen3->setFrameShape(QFrame::WinPanel);
        labelCen3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelCen3, 2, 3, 1, 1);

        labelCen1 = new QLabel(gridLayoutWidget);
        labelCen1->setObjectName(QString::fromUtf8("labelCen1"));
        labelCen1->setFont(font);
        labelCen1->setFrameShape(QFrame::WinPanel);
        labelCen1->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelCen1, 2, 1, 1, 1);

        labelDix2 = new QLabel(gridLayoutWidget);
        labelDix2->setObjectName(QString::fromUtf8("labelDix2"));
        labelDix2->setFont(font);
        labelDix2->setFrameShape(QFrame::WinPanel);
        labelDix2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDix2, 1, 2, 1, 1);

        labelCen4 = new QLabel(gridLayoutWidget);
        labelCen4->setObjectName(QString::fromUtf8("labelCen4"));
        labelCen4->setFont(font);
        labelCen4->setFrameShape(QFrame::WinPanel);
        labelCen4->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelCen4, 2, 4, 1, 1);

        labelCen5 = new QLabel(gridLayoutWidget);
        labelCen5->setObjectName(QString::fromUtf8("labelCen5"));
        labelCen5->setFont(font);
        labelCen5->setFrameShape(QFrame::WinPanel);
        labelCen5->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelCen5, 2, 5, 1, 1);

        label_20 = new QLabel(gridLayoutWidget);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setFont(font1);
        label_20->setFrameShape(QFrame::WinPanel);
        label_20->setFrameShadow(QFrame::Sunken);
        label_20->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_20, 2, 0, 1, 1);

        label_13 = new QLabel(gridLayoutWidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setFont(font1);
        label_13->setFrameShape(QFrame::WinPanel);
        label_13->setFrameShadow(QFrame::Sunken);
        label_13->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_13, 1, 0, 1, 1);

        label_16 = new QLabel(gridLayoutWidget);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font1);
        label_16->setFrameShape(QFrame::WinPanel);
        label_16->setFrameShadow(QFrame::Sunken);
        label_16->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_16, 0, 3, 1, 1);

        label_18 = new QLabel(gridLayoutWidget);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setFont(font1);
        label_18->setFrameShape(QFrame::WinPanel);
        label_18->setFrameShadow(QFrame::Sunken);
        label_18->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_18, 0, 5, 1, 1);

        labelCen2 = new QLabel(gridLayoutWidget);
        labelCen2->setObjectName(QString::fromUtf8("labelCen2"));
        labelCen2->setFont(font);
        labelCen2->setFrameShape(QFrame::WinPanel);
        labelCen2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelCen2, 2, 2, 1, 1);

        label_15 = new QLabel(gridLayoutWidget);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font1);
        label_15->setFrameShape(QFrame::WinPanel);
        label_15->setFrameShadow(QFrame::Sunken);
        label_15->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_15, 0, 2, 1, 1);

        labelDix3 = new QLabel(gridLayoutWidget);
        labelDix3->setObjectName(QString::fromUtf8("labelDix3"));
        labelDix3->setFont(font);
        labelDix3->setFrameShape(QFrame::WinPanel);
        labelDix3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDix3, 1, 3, 1, 1);

        labelDix5 = new QLabel(gridLayoutWidget);
        labelDix5->setObjectName(QString::fromUtf8("labelDix5"));
        labelDix5->setFont(font);
        labelDix5->setFrameShape(QFrame::WinPanel);
        labelDix5->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDix5, 1, 5, 1, 1);

        label_23 = new QLabel(gridLayoutWidget);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setFont(font);
        label_23->setFrameShape(QFrame::WinPanel);
        label_23->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label_23, 0, 0, 1, 1);

        label_21 = new QLabel(gridLayoutWidget);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setFont(font1);
        label_21->setFrameShape(QFrame::WinPanel);
        label_21->setFrameShadow(QFrame::Sunken);
        label_21->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_21, 3, 0, 1, 1);

        labelDix4 = new QLabel(gridLayoutWidget);
        labelDix4->setObjectName(QString::fromUtf8("labelDix4"));
        labelDix4->setFont(font);
        labelDix4->setFrameShape(QFrame::WinPanel);
        labelDix4->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDix4, 1, 4, 1, 1);

        labelDia1 = new QLabel(gridLayoutWidget);
        labelDia1->setObjectName(QString::fromUtf8("labelDia1"));
        labelDia1->setFont(font);
        labelDia1->setFrameShape(QFrame::WinPanel);
        labelDia1->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDia1, 3, 1, 1, 1);

        labelDix1 = new QLabel(gridLayoutWidget);
        labelDix1->setObjectName(QString::fromUtf8("labelDix1"));
        labelDix1->setFont(font);
        labelDix1->setFrameShape(QFrame::WinPanel);
        labelDix1->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDix1, 1, 1, 1, 1);

        label_22 = new QLabel(gridLayoutWidget);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setFont(font1);
        label_22->setFrameShape(QFrame::WinPanel);
        label_22->setFrameShadow(QFrame::Sunken);
        label_22->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_22, 4, 0, 1, 1);

        labelDia2 = new QLabel(gridLayoutWidget);
        labelDia2->setObjectName(QString::fromUtf8("labelDia2"));
        labelDia2->setFont(font);
        labelDia2->setFrameShape(QFrame::WinPanel);
        labelDia2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDia2, 3, 2, 1, 1);

        labelDia3 = new QLabel(gridLayoutWidget);
        labelDia3->setObjectName(QString::fromUtf8("labelDia3"));
        labelDia3->setFont(font);
        labelDia3->setFrameShape(QFrame::WinPanel);
        labelDia3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDia3, 3, 3, 1, 1);

        labelDia4 = new QLabel(gridLayoutWidget);
        labelDia4->setObjectName(QString::fromUtf8("labelDia4"));
        labelDia4->setFont(font);
        labelDia4->setFrameShape(QFrame::WinPanel);
        labelDia4->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDia4, 3, 4, 1, 1);

        labelDia5 = new QLabel(gridLayoutWidget);
        labelDia5->setObjectName(QString::fromUtf8("labelDia5"));
        labelDia5->setFont(font);
        labelDia5->setFrameShape(QFrame::WinPanel);
        labelDia5->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDia5, 3, 5, 1, 1);

        labelDiaC1 = new QLabel(gridLayoutWidget);
        labelDiaC1->setObjectName(QString::fromUtf8("labelDiaC1"));
        labelDiaC1->setFont(font);
        labelDiaC1->setFrameShape(QFrame::WinPanel);
        labelDiaC1->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDiaC1, 4, 1, 1, 1);

        labelDiaC2 = new QLabel(gridLayoutWidget);
        labelDiaC2->setObjectName(QString::fromUtf8("labelDiaC2"));
        labelDiaC2->setFont(font);
        labelDiaC2->setFrameShape(QFrame::WinPanel);
        labelDiaC2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDiaC2, 4, 2, 1, 1);

        labelDiaC3 = new QLabel(gridLayoutWidget);
        labelDiaC3->setObjectName(QString::fromUtf8("labelDiaC3"));
        labelDiaC3->setFont(font);
        labelDiaC3->setFrameShape(QFrame::WinPanel);
        labelDiaC3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDiaC3, 4, 3, 1, 1);

        labelDiaC4 = new QLabel(gridLayoutWidget);
        labelDiaC4->setObjectName(QString::fromUtf8("labelDiaC4"));
        labelDiaC4->setFont(font);
        labelDiaC4->setFrameShape(QFrame::WinPanel);
        labelDiaC4->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDiaC4, 4, 4, 1, 1);

        labelDiaC5 = new QLabel(gridLayoutWidget);
        labelDiaC5->setObjectName(QString::fromUtf8("labelDiaC5"));
        labelDiaC5->setFont(font);
        labelDiaC5->setFrameShape(QFrame::WinPanel);
        labelDiaC5->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelDiaC5, 4, 5, 1, 1);

        label_14 = new QLabel(gridLayoutWidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font1);
        label_14->setFrameShape(QFrame::WinPanel);
        label_14->setFrameShadow(QFrame::Sunken);
        label_14->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_14, 0, 1, 1, 1);

        gridLayoutWidget_2 = new QWidget(DMSCalibrationUI);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(10, 170, 806, 121));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        lineEditKorFactor2 = new QLineEdit(gridLayoutWidget_2);
        lineEditKorFactor2->setObjectName(QString::fromUtf8("lineEditKorFactor2"));
        lineEditKorFactor2->setFont(font);

        gridLayout_2->addWidget(lineEditKorFactor2, 6, 2, 1, 1);

        label_27 = new QLabel(gridLayoutWidget_2);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setMinimumSize(QSize(107, 0));
        label_27->setMaximumSize(QSize(107, 24));
        label_27->setFont(font1);
        label_27->setFrameShape(QFrame::WinPanel);
        label_27->setFrameShadow(QFrame::Sunken);
        label_27->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_27, 0, 5, 1, 1);

        label_26 = new QLabel(gridLayoutWidget_2);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setMinimumSize(QSize(107, 0));
        label_26->setMaximumSize(QSize(107, 24));
        label_26->setFont(font1);
        label_26->setFrameShape(QFrame::WinPanel);
        label_26->setFrameShadow(QFrame::Sunken);
        label_26->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_26, 0, 4, 1, 1);

        label_28 = new QLabel(gridLayoutWidget_2);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setMinimumSize(QSize(107, 0));
        label_28->setMaximumSize(QSize(107, 24));
        label_28->setFont(font1);
        label_28->setFrameShape(QFrame::WinPanel);
        label_28->setFrameShadow(QFrame::Sunken);
        label_28->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_28, 5, 0, 1, 1);

        lineEditCenter3 = new QLineEdit(gridLayoutWidget_2);
        lineEditCenter3->setObjectName(QString::fromUtf8("lineEditCenter3"));
        lineEditCenter3->setFont(font);

        gridLayout_2->addWidget(lineEditCenter3, 5, 3, 1, 1);

        lineEditCenter4 = new QLineEdit(gridLayoutWidget_2);
        lineEditCenter4->setObjectName(QString::fromUtf8("lineEditCenter4"));
        lineEditCenter4->setFont(font);

        gridLayout_2->addWidget(lineEditCenter4, 5, 4, 1, 1);

        label_25 = new QLabel(gridLayoutWidget_2);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setMinimumSize(QSize(107, 0));
        label_25->setMaximumSize(QSize(107, 24));
        label_25->setFont(font1);
        label_25->setFrameShape(QFrame::WinPanel);
        label_25->setFrameShadow(QFrame::Sunken);
        label_25->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_25, 0, 3, 1, 1);

        lineEditCenter2 = new QLineEdit(gridLayoutWidget_2);
        lineEditCenter2->setObjectName(QString::fromUtf8("lineEditCenter2"));
        lineEditCenter2->setFont(font);

        gridLayout_2->addWidget(lineEditCenter2, 5, 2, 1, 1);

        label_19 = new QLabel(gridLayoutWidget_2);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(107, 0));
        label_19->setMaximumSize(QSize(107, 24));
        label_19->setFont(font1);
        label_19->setFrameShape(QFrame::WinPanel);
        label_19->setFrameShadow(QFrame::Sunken);
        label_19->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_19, 0, 1, 1, 1);

        lineEditCenter5 = new QLineEdit(gridLayoutWidget_2);
        lineEditCenter5->setObjectName(QString::fromUtf8("lineEditCenter5"));
        lineEditCenter5->setFont(font);

        gridLayout_2->addWidget(lineEditCenter5, 5, 5, 1, 1);

        label_29 = new QLabel(gridLayoutWidget_2);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setMinimumSize(QSize(107, 0));
        label_29->setMaximumSize(QSize(107, 24));
        label_29->setFont(font1);
        label_29->setFrameShape(QFrame::WinPanel);
        label_29->setFrameShadow(QFrame::Sunken);
        label_29->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_29, 6, 0, 1, 1);

        lineEditKorFactor5 = new QLineEdit(gridLayoutWidget_2);
        lineEditKorFactor5->setObjectName(QString::fromUtf8("lineEditKorFactor5"));
        lineEditKorFactor5->setFont(font);

        gridLayout_2->addWidget(lineEditKorFactor5, 6, 5, 1, 1);

        lineEditKorFactor4 = new QLineEdit(gridLayoutWidget_2);
        lineEditKorFactor4->setObjectName(QString::fromUtf8("lineEditKorFactor4"));
        lineEditKorFactor4->setFont(font);

        gridLayout_2->addWidget(lineEditKorFactor4, 6, 4, 1, 1);

        label_24 = new QLabel(gridLayoutWidget_2);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setMinimumSize(QSize(107, 0));
        label_24->setMaximumSize(QSize(107, 24));
        label_24->setFont(font1);
        label_24->setFrameShape(QFrame::WinPanel);
        label_24->setFrameShadow(QFrame::Sunken);
        label_24->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_24, 0, 2, 1, 1);

        lineEditKorFactor3 = new QLineEdit(gridLayoutWidget_2);
        lineEditKorFactor3->setObjectName(QString::fromUtf8("lineEditKorFactor3"));
        lineEditKorFactor3->setFont(font);

        gridLayout_2->addWidget(lineEditKorFactor3, 6, 3, 1, 1);

        lineEditCenter1 = new QLineEdit(gridLayoutWidget_2);
        lineEditCenter1->setObjectName(QString::fromUtf8("lineEditCenter1"));
        lineEditCenter1->setFont(font);

        gridLayout_2->addWidget(lineEditCenter1, 5, 1, 1, 1);

        lineEditKorFactor1 = new QLineEdit(gridLayoutWidget_2);
        lineEditKorFactor1->setObjectName(QString::fromUtf8("lineEditKorFactor1"));
        lineEditKorFactor1->setFont(font);

        gridLayout_2->addWidget(lineEditKorFactor1, 6, 1, 1, 1);

        buttonReadRefCenter = new QPushButton(gridLayoutWidget_2);
        buttonReadRefCenter->setObjectName(QString::fromUtf8("buttonReadRefCenter"));
        buttonReadRefCenter->setFont(font1);

        gridLayout_2->addWidget(buttonReadRefCenter, 5, 6, 1, 1);

        buttonCalcKorFactor = new QPushButton(gridLayoutWidget_2);
        buttonCalcKorFactor->setObjectName(QString::fromUtf8("buttonCalcKorFactor"));
        buttonCalcKorFactor->setFont(font1);

        gridLayout_2->addWidget(buttonCalcKorFactor, 6, 6, 1, 1);

        gridLayoutWidget_3 = new QWidget(DMSCalibrationUI);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(151, 9, 231, 91));
        gridLayout_3 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        labelRefDiameter = new QLabel(gridLayoutWidget_3);
        labelRefDiameter->setObjectName(QString::fromUtf8("labelRefDiameter"));
        labelRefDiameter->setMinimumSize(QSize(107, 0));
        labelRefDiameter->setMaximumSize(QSize(107, 24));
        labelRefDiameter->setFont(font1);
        labelRefDiameter->setFrameShape(QFrame::WinPanel);
        labelRefDiameter->setFrameShadow(QFrame::Sunken);
        labelRefDiameter->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(labelRefDiameter, 1, 0, 1, 1);

        labelRefLenght = new QLabel(gridLayoutWidget_3);
        labelRefLenght->setObjectName(QString::fromUtf8("labelRefLenght"));
        labelRefLenght->setMinimumSize(QSize(107, 0));
        labelRefLenght->setMaximumSize(QSize(107, 24));
        labelRefLenght->setFont(font1);
        labelRefLenght->setFrameShape(QFrame::WinPanel);
        labelRefLenght->setFrameShadow(QFrame::Sunken);
        labelRefLenght->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(labelRefLenght, 1, 1, 1, 1);

        label_32 = new QLabel(gridLayoutWidget_3);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setMinimumSize(QSize(107, 0));
        label_32->setMaximumSize(QSize(107, 24));
        label_32->setFont(font1);
        label_32->setFrameShape(QFrame::WinPanel);
        label_32->setFrameShadow(QFrame::Sunken);
        label_32->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_32, 0, 0, 1, 1);

        label_33 = new QLabel(gridLayoutWidget_3);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setMinimumSize(QSize(107, 0));
        label_33->setMaximumSize(QSize(107, 24));
        label_33->setFont(font1);
        label_33->setFrameShape(QFrame::WinPanel);
        label_33->setFrameShadow(QFrame::Sunken);
        label_33->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_33, 0, 1, 1, 1);

        buttonSave = new QPushButton(DMSCalibrationUI);
        buttonSave->setObjectName(QString::fromUtf8("buttonSave"));
        buttonSave->setGeometry(QRect(690, 300, 121, 51));
        buttonEnterRefDowel = new QPushButton(DMSCalibrationUI);
        buttonEnterRefDowel->setObjectName(QString::fromUtf8("buttonEnterRefDowel"));
        buttonEnterRefDowel->setGeometry(QRect(20, 20, 121, 71));
        labelRefDiameter_2 = new QLabel(DMSCalibrationUI);
        labelRefDiameter_2->setObjectName(QString::fromUtf8("labelRefDiameter_2"));
        labelRefDiameter_2->setGeometry(QRect(450, 20, 180, 24));
        labelRefDiameter_2->setMinimumSize(QSize(180, 0));
        labelRefDiameter_2->setMaximumSize(QSize(107, 24));
        labelRefDiameter_2->setFont(font1);
        labelRefDiameter_2->setFrameShape(QFrame::WinPanel);
        labelRefDiameter_2->setFrameShadow(QFrame::Sunken);
        labelRefDiameter_2->setAlignment(Qt::AlignCenter);
        labelSelectedCalibration = new QLabel(DMSCalibrationUI);
        labelSelectedCalibration->setObjectName(QString::fromUtf8("labelSelectedCalibration"));
        labelSelectedCalibration->setGeometry(QRect(640, 20, 107, 24));
        labelSelectedCalibration->setMinimumSize(QSize(107, 0));
        labelSelectedCalibration->setMaximumSize(QSize(107, 24));
        labelSelectedCalibration->setFont(font1);
        labelSelectedCalibration->setFrameShape(QFrame::WinPanel);
        labelSelectedCalibration->setFrameShadow(QFrame::Sunken);
        labelSelectedCalibration->setAlignment(Qt::AlignCenter);
        gridLayoutWidget_4 = new QWidget(DMSCalibrationUI);
        gridLayoutWidget_4->setObjectName(QString::fromUtf8("gridLayoutWidget_4"));
        gridLayoutWidget_4->setGeometry(QRect(830, 170, 346, 121));
        gridLayout_4 = new QGridLayout(gridLayoutWidget_4);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        lineEditKorFactor0 = new QLineEdit(gridLayoutWidget_4);
        lineEditKorFactor0->setObjectName(QString::fromUtf8("lineEditKorFactor0"));
        lineEditKorFactor0->setFont(font);

        gridLayout_4->addWidget(lineEditKorFactor0, 1, 1, 1, 1);

        label_31 = new QLabel(gridLayoutWidget_4);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setMinimumSize(QSize(107, 0));
        label_31->setMaximumSize(QSize(107, 24));
        label_31->setFont(font1);
        label_31->setFrameShape(QFrame::WinPanel);
        label_31->setFrameShadow(QFrame::Sunken);
        label_31->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_31, 0, 1, 1, 1);

        label_30 = new QLabel(gridLayoutWidget_4);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setMinimumSize(QSize(107, 0));
        label_30->setMaximumSize(QSize(107, 24));
        label_30->setFont(font1);
        label_30->setFrameShape(QFrame::WinPanel);
        label_30->setFrameShadow(QFrame::Sunken);
        label_30->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_30, 1, 0, 1, 1);

        buttonCalcKorFactorWidth = new QPushButton(gridLayoutWidget_4);
        buttonCalcKorFactorWidth->setObjectName(QString::fromUtf8("buttonCalcKorFactorWidth"));
        buttonCalcKorFactorWidth->setFont(font1);

        gridLayout_4->addWidget(buttonCalcKorFactorWidth, 1, 2, 1, 1);

        gridLayoutWidget_5 = new QWidget(DMSCalibrationUI);
        gridLayoutWidget_5->setObjectName(QString::fromUtf8("gridLayoutWidget_5"));
        gridLayoutWidget_5->setGeometry(QRect(830, 370, 361, 141));
        gridLayout_5 = new QGridLayout(gridLayoutWidget_5);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        label_35 = new QLabel(gridLayoutWidget_5);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setMinimumSize(QSize(107, 0));
        label_35->setMaximumSize(QSize(107, 24));
        label_35->setFont(font1);
        label_35->setFrameShape(QFrame::WinPanel);
        label_35->setFrameShadow(QFrame::Sunken);
        label_35->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(label_35, 0, 1, 1, 1);

        labelWidthPix = new QLabel(gridLayoutWidget_5);
        labelWidthPix->setObjectName(QString::fromUtf8("labelWidthPix"));
        labelWidthPix->setMinimumSize(QSize(107, 0));
        labelWidthPix->setMaximumSize(QSize(107, 24));
        labelWidthPix->setFont(font1);
        labelWidthPix->setFrameShape(QFrame::WinPanel);
        labelWidthPix->setFrameShadow(QFrame::Sunken);
        labelWidthPix->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(labelWidthPix, 1, 1, 1, 1);

        label_34 = new QLabel(gridLayoutWidget_5);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setMinimumSize(QSize(107, 0));
        label_34->setMaximumSize(QSize(107, 24));
        label_34->setFont(font1);
        label_34->setFrameShape(QFrame::WinPanel);
        label_34->setFrameShadow(QFrame::Sunken);
        label_34->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(label_34, 1, 0, 1, 1);

        labelWidthReal = new QLabel(gridLayoutWidget_5);
        labelWidthReal->setObjectName(QString::fromUtf8("labelWidthReal"));
        labelWidthReal->setMinimumSize(QSize(107, 0));
        labelWidthReal->setMaximumSize(QSize(107, 24));
        labelWidthReal->setFont(font1);
        labelWidthReal->setFrameShape(QFrame::WinPanel);
        labelWidthReal->setFrameShadow(QFrame::Sunken);
        labelWidthReal->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(labelWidthReal, 2, 1, 1, 1);

        label_36 = new QLabel(gridLayoutWidget_5);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        label_36->setMinimumSize(QSize(107, 0));
        label_36->setMaximumSize(QSize(107, 24));
        label_36->setFont(font1);
        label_36->setFrameShape(QFrame::WinPanel);
        label_36->setFrameShadow(QFrame::Sunken);
        label_36->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(label_36, 2, 0, 1, 1);


        retranslateUi(DMSCalibrationUI);

        QMetaObject::connectSlotsByName(DMSCalibrationUI);
    } // setupUi

    void retranslateUi(QWidget *DMSCalibrationUI)
    {
        DMSCalibrationUI->setWindowTitle(QCoreApplication::translate("DMSCalibrationUI", "Form", nullptr));
        buttonAutoCalibration->setText(QCoreApplication::translate("DMSCalibrationUI", "Automatic Calibration", nullptr));
        buttonClose->setText(QCoreApplication::translate("DMSCalibrationUI", "CLOSE", nullptr));
        label_17->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM4", nullptr));
        labelCen3->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelCen1->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDix2->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelCen4->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelCen5->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        label_20->setText(QCoreApplication::translate("DMSCalibrationUI", "Cen", nullptr));
        label_13->setText(QCoreApplication::translate("DMSCalibrationUI", "DiX", nullptr));
        label_16->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM3", nullptr));
        label_18->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM5", nullptr));
        labelCen2->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        label_15->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM2", nullptr));
        labelDix3->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDix5->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        label_23->setText(QString());
        label_21->setText(QCoreApplication::translate("DMSCalibrationUI", "DiA ", nullptr));
        labelDix4->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDia1->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDix1->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        label_22->setText(QCoreApplication::translate("DMSCalibrationUI", "DiA Corr:", nullptr));
        labelDia2->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDia3->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDia4->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDia5->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDiaC1->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDiaC2->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDiaC3->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDiaC4->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        labelDiaC5->setText(QCoreApplication::translate("DMSCalibrationUI", "TextLabel", nullptr));
        label_14->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM1", nullptr));
        label_27->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM5", nullptr));
        label_26->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM4", nullptr));
        label_28->setText(QCoreApplication::translate("DMSCalibrationUI", "Ref Center", nullptr));
        label_25->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM3", nullptr));
        label_19->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM1", nullptr));
        label_29->setText(QCoreApplication::translate("DMSCalibrationUI", "Kor.Factor", nullptr));
        label_24->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM2", nullptr));
        buttonReadRefCenter->setText(QCoreApplication::translate("DMSCalibrationUI", "Read Ref Center", nullptr));
        buttonCalcKorFactor->setText(QCoreApplication::translate("DMSCalibrationUI", "Calculate Factor", nullptr));
        labelRefDiameter->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM1", nullptr));
        labelRefLenght->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM1", nullptr));
        label_32->setText(QCoreApplication::translate("DMSCalibrationUI", "Diameter", nullptr));
        label_33->setText(QCoreApplication::translate("DMSCalibrationUI", "Lenght", nullptr));
        buttonSave->setText(QCoreApplication::translate("DMSCalibrationUI", "Save Correction", nullptr));
        buttonEnterRefDowel->setText(QCoreApplication::translate("DMSCalibrationUI", "Enter Reference Dowel", nullptr));
        labelRefDiameter_2->setText(QCoreApplication::translate("DMSCalibrationUI", "Selected Callibration:", nullptr));
        labelSelectedCalibration->setText(QCoreApplication::translate("DMSCalibrationUI", "Diameter", nullptr));
        lineEditKorFactor0->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM0", nullptr));
        label_31->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM0", nullptr));
        label_30->setText(QCoreApplication::translate("DMSCalibrationUI", "KorFactor", nullptr));
        buttonCalcKorFactorWidth->setText(QCoreApplication::translate("DMSCalibrationUI", "Calculate Factor", nullptr));
        label_35->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM0", nullptr));
        labelWidthPix->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM0", nullptr));
        label_34->setText(QCoreApplication::translate("DMSCalibrationUI", "Width", nullptr));
        labelWidthReal->setText(QCoreApplication::translate("DMSCalibrationUI", "CAM0", nullptr));
        label_36->setText(QCoreApplication::translate("DMSCalibrationUI", "WidthReal", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DMSCalibrationUI: public Ui_DMSCalibrationUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DMSCALIBRATION_H
