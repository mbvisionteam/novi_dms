/********************************************************************************
** Form generated from reading UI file 'MBsoftware.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MBSOFTWARE_H
#define UI_MBSOFTWARE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MBsoftwareClass
{
public:
    QWidget *centralWidget;
    QFrame *frameStatus;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout;
    QFrame *frameDeviceStatus;
    QLabel *labelDeviceStatus;
    QListWidget *listWidgetStatus;
    QLabel *labelLogin;
    QLabel *labelStatusNaprave;
    QLabel *labelStatus;
    QLabel *labelStatusNavijalka;
    QLabel *labelKoracno;
    QLabel *labelTip;
    QTabWidget *MainMenuBar;
    QWidget *tabLogin;
    QHBoxLayout *horizontalLayout_4;
    QToolButton *buttonLogin;
    QSpacerItem *horizontalSpacer_2;
    QWidget *tabTypes;
    QHBoxLayout *horizontalLayout_3;
    QToolButton *buttonDowelSelect;
    QToolButton *buttonSetTolerance;
    QToolButton *buttonTypeSelect;
    QToolButton *buttonAddType;
    QToolButton *buttonRemoveType;
    QToolButton *buttonTypeSettings;
    QToolButton *buttonResetCounters;
    QSpacerItem *horizontalSpacer_3;
    QWidget *tabCameras;
    QWidget *tabSignals;
    QWidget *tabImageProcessing;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *buttonImageProcessing;
    QToolButton *buttonDMStransitionTest;
    QToolButton *buttonDMS;
    QSpacerItem *horizontalSpacer;
    QWidget *tabStatistcs;
    QHBoxLayout *horizontalLayout_5;
    QToolButton *buttonStatusBox;
    QSpacerItem *horizontalSpacer_4;
    QToolButton *buttonAboutUs;
    QLabel *labelLogo;
    QFrame *frameMeasurements;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *layoutMeasurement;
    QGraphicsView *imageViewMainWindow_0;
    QComboBox *comboBoxSelectCamera;
    QListWidget *statusList;
    QFrame *statisticsFrame;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *labelGoodProcent;
    QLabel *label_3;
    QLabel *labelTotal;
    QLabel *labelGood_4;
    QLabel *labelBadProcent;
    QLabel *label_10;
    QLabel *labelBAD_2;
    QLabel *label_9;
    QLabel *label_11;
    QLabel *labelValveGood;
    QLabel *labelGood_3;
    QLabel *label_2;
    QLabel *labelTest;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *layoutTest;
    QGroupBox *groupBox_2;
    QGraphicsView *imageViewPresortLive;
    QGraphicsView *imageViewPresort;
    QLabel *labelPresortDowel2;
    QLabel *labelPresortDowel0;
    QLabel *labelPresortDowel1;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *layoutPresortTable;
    QListWidget *presortList;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label;
    QFrame *frame_2;
    QGridLayout *gridLayout_2;
    QFrame *frame_4;
    QGridLayout *gridLayout_4;
    QLabel *label_12;
    QLabel *label_14;
    QLabel *label_16;
    QLabel *label_7;
    QLabel *label_6;
    QLabel *label_13;
    QLabel *label_4;
    QFrame *frame_5;
    QGridLayout *gridLayout_3;
    QLabel *label_39;
    QLabel *label_25;
    QLabel *label_27;
    QLabel *label_19;
    QLabel *label_24;
    QLabel *label_28;
    QLabel *label_40;
    QLabel *label_36;
    QLabel *label_35;
    QLabel *label_34;
    QLabel *label_33;
    QLabel *label_32;
    QLabel *label_41;
    QLabel *label_37;
    QLabel *label_38;
    QLabel *label_18;
    QLabel *label_26;
    QLabel *label_29;
    QLabel *label_31;
    QLabel *label_23;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_30;
    QLabel *label_42;
    QLabel *label_43;
    QLabel *label_44;
    QLabel *label_46;
    QLabel *label_47;
    QFrame *frame_3;
    QGridLayout *gridLayout_7;
    QFrame *frame_7;
    QGridLayout *gridLayout_8;
    QLabel *label_8;
    QLabel *labelBAD;
    QLabel *label_106;
    QLabel *label_108;
    QLabel *label_105;
    QLabel *label_15;
    QLabel *label_109;
    QLabel *labelGood;
    QLabel *label_5;
    QLabel *label_111;
    QLabel *label_113;
    QFrame *frame_6;
    QGridLayout *gridLayout_6;
    QLabel *label_76;
    QLabel *label_78;
    QLabel *label_90;
    QLabel *label_80;
    QLabel *label_94;
    QLabel *label_79;
    QLabel *label_96;
    QLabel *label_91;
    QLabel *label_95;
    QLabel *label_81;
    QLabel *label_82;
    QFrame *frame_8;
    QGridLayout *gridLayout_9;
    QLabel *label_103;
    QLabel *labelBAD_3;
    QLabel *label_110;
    QLabel *label_114;
    QLabel *label_115;
    QLabel *label_104;
    QLabel *label_116;
    QLabel *labelGood_2;
    QLabel *label_117;
    QLabel *label_118;
    QLabel *label_119;
    QLabel *label_17;
    QLabel *label_20;
    QLabel *label_107;
    QLabel *label_112;

    void setupUi(QMainWindow *MBsoftwareClass)
    {
        if (MBsoftwareClass->objectName().isEmpty())
            MBsoftwareClass->setObjectName(QString::fromUtf8("MBsoftwareClass"));
        MBsoftwareClass->setWindowModality(Qt::NonModal);
        MBsoftwareClass->resize(1920, 1080);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MBsoftwareClass->sizePolicy().hasHeightForWidth());
        MBsoftwareClass->setSizePolicy(sizePolicy);
        MBsoftwareClass->setMinimumSize(QSize(1920, 1080));
        MBsoftwareClass->setMaximumSize(QSize(16777215, 16777215));
        MBsoftwareClass->setCursor(QCursor(Qt::ArrowCursor));
        MBsoftwareClass->setAutoFillBackground(false);
        MBsoftwareClass->setStyleSheet(QString::fromUtf8(""));
        MBsoftwareClass->setToolButtonStyle(Qt::ToolButtonIconOnly);
        MBsoftwareClass->setTabShape(QTabWidget::Triangular);
        MBsoftwareClass->setDockNestingEnabled(false);
        centralWidget = new QWidget(MBsoftwareClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        centralWidget->setMinimumSize(QSize(1280, 860));
        centralWidget->setMaximumSize(QSize(1920, 1300));
        frameStatus = new QFrame(centralWidget);
        frameStatus->setObjectName(QString::fromUtf8("frameStatus"));
        frameStatus->setGeometry(QRect(1620, 310, 300, 561));
        frameStatus->setMinimumSize(QSize(300, 0));
        frameStatus->setMaximumSize(QSize(300, 16777215));
        frameStatus->setFrameShape(QFrame::Box);
        frameStatus->setFrameShadow(QFrame::Raised);
        frameStatus->setLineWidth(4);
        verticalLayoutWidget_2 = new QWidget(frameStatus);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 10, 281, 546));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        frameDeviceStatus = new QFrame(verticalLayoutWidget_2);
        frameDeviceStatus->setObjectName(QString::fromUtf8("frameDeviceStatus"));
        frameDeviceStatus->setMinimumSize(QSize(0, 250));
        frameDeviceStatus->setMaximumSize(QSize(16777215, 250));
        frameDeviceStatus->setFrameShape(QFrame::StyledPanel);
        frameDeviceStatus->setFrameShadow(QFrame::Plain);
        frameDeviceStatus->setLineWidth(1);
        labelDeviceStatus = new QLabel(frameDeviceStatus);
        labelDeviceStatus->setObjectName(QString::fromUtf8("labelDeviceStatus"));
        labelDeviceStatus->setGeometry(QRect(10, 10, 141, 20));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        labelDeviceStatus->setFont(font);
        listWidgetStatus = new QListWidget(frameDeviceStatus);
        listWidgetStatus->setObjectName(QString::fromUtf8("listWidgetStatus"));
        listWidgetStatus->setGeometry(QRect(10, 30, 261, 211));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setBold(true);
        font1.setWeight(75);
        listWidgetStatus->setFont(font1);

        verticalLayout->addWidget(frameDeviceStatus);

        labelLogin = new QLabel(verticalLayoutWidget_2);
        labelLogin->setObjectName(QString::fromUtf8("labelLogin"));
        labelLogin->setMinimumSize(QSize(200, 43));
        labelLogin->setMaximumSize(QSize(16777215, 43));
        labelLogin->setFont(font);
        labelLogin->setStyleSheet(QString::fromUtf8(""));
        labelLogin->setFrameShape(QFrame::Panel);
        labelLogin->setFrameShadow(QFrame::Sunken);
        labelLogin->setLineWidth(3);

        verticalLayout->addWidget(labelLogin);

        labelStatusNaprave = new QLabel(verticalLayoutWidget_2);
        labelStatusNaprave->setObjectName(QString::fromUtf8("labelStatusNaprave"));
        labelStatusNaprave->setMinimumSize(QSize(200, 43));
        labelStatusNaprave->setMaximumSize(QSize(16777215, 43));
        labelStatusNaprave->setFont(font);
        labelStatusNaprave->setStyleSheet(QString::fromUtf8(""));
        labelStatusNaprave->setFrameShape(QFrame::Panel);
        labelStatusNaprave->setFrameShadow(QFrame::Sunken);
        labelStatusNaprave->setLineWidth(3);

        verticalLayout->addWidget(labelStatusNaprave);

        labelStatus = new QLabel(verticalLayoutWidget_2);
        labelStatus->setObjectName(QString::fromUtf8("labelStatus"));
        labelStatus->setMinimumSize(QSize(200, 43));
        labelStatus->setMaximumSize(QSize(16777215, 43));
        labelStatus->setFont(font);
        labelStatus->setStyleSheet(QString::fromUtf8(""));
        labelStatus->setFrameShape(QFrame::Panel);
        labelStatus->setFrameShadow(QFrame::Sunken);
        labelStatus->setLineWidth(3);

        verticalLayout->addWidget(labelStatus);

        labelStatusNavijalka = new QLabel(verticalLayoutWidget_2);
        labelStatusNavijalka->setObjectName(QString::fromUtf8("labelStatusNavijalka"));
        labelStatusNavijalka->setMinimumSize(QSize(200, 43));
        labelStatusNavijalka->setMaximumSize(QSize(16777215, 43));
        labelStatusNavijalka->setFont(font);
        labelStatusNavijalka->setStyleSheet(QString::fromUtf8(""));
        labelStatusNavijalka->setFrameShape(QFrame::Panel);
        labelStatusNavijalka->setFrameShadow(QFrame::Sunken);
        labelStatusNavijalka->setLineWidth(3);

        verticalLayout->addWidget(labelStatusNavijalka);

        labelKoracno = new QLabel(verticalLayoutWidget_2);
        labelKoracno->setObjectName(QString::fromUtf8("labelKoracno"));
        labelKoracno->setMinimumSize(QSize(200, 43));
        labelKoracno->setMaximumSize(QSize(16777215, 43));
        labelKoracno->setFont(font);
        labelKoracno->setStyleSheet(QString::fromUtf8(""));
        labelKoracno->setFrameShape(QFrame::Panel);
        labelKoracno->setFrameShadow(QFrame::Sunken);
        labelKoracno->setLineWidth(3);

        verticalLayout->addWidget(labelKoracno);

        labelTip = new QLabel(verticalLayoutWidget_2);
        labelTip->setObjectName(QString::fromUtf8("labelTip"));
        labelTip->setMinimumSize(QSize(200, 43));
        labelTip->setMaximumSize(QSize(16777215, 43));
        labelTip->setFont(font);
        labelTip->setStyleSheet(QString::fromUtf8(""));
        labelTip->setFrameShape(QFrame::Panel);
        labelTip->setFrameShadow(QFrame::Sunken);
        labelTip->setLineWidth(3);

        verticalLayout->addWidget(labelTip);

        MainMenuBar = new QTabWidget(centralWidget);
        MainMenuBar->setObjectName(QString::fromUtf8("MainMenuBar"));
        MainMenuBar->setGeometry(QRect(9, 9, 641, 150));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(MainMenuBar->sizePolicy().hasHeightForWidth());
        MainMenuBar->setSizePolicy(sizePolicy2);
        MainMenuBar->setMinimumSize(QSize(640, 120));
        MainMenuBar->setMaximumSize(QSize(16777215, 150));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Comic Sans MS"));
        font2.setPointSize(10);
        MainMenuBar->setFont(font2);
        MainMenuBar->setTabPosition(QTabWidget::North);
        MainMenuBar->setTabShape(QTabWidget::Triangular);
        MainMenuBar->setMovable(true);
        MainMenuBar->setTabBarAutoHide(true);
        tabLogin = new QWidget();
        tabLogin->setObjectName(QString::fromUtf8("tabLogin"));
        horizontalLayout_4 = new QHBoxLayout(tabLogin);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        buttonLogin = new QToolButton(tabLogin);
        buttonLogin->setObjectName(QString::fromUtf8("buttonLogin"));
        buttonLogin->setMinimumSize(QSize(100, 100));
        buttonLogin->setFocusPolicy(Qt::ClickFocus);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../../../Users/Ales/.designer/backup/res/LoginBig.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonLogin->setIcon(icon);
        buttonLogin->setIconSize(QSize(80, 80));
        buttonLogin->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonLogin->setAutoRaise(true);

        horizontalLayout_4->addWidget(buttonLogin);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        MainMenuBar->addTab(tabLogin, QString());
        tabTypes = new QWidget();
        tabTypes->setObjectName(QString::fromUtf8("tabTypes"));
        horizontalLayout_3 = new QHBoxLayout(tabTypes);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        buttonDowelSelect = new QToolButton(tabTypes);
        buttonDowelSelect->setObjectName(QString::fromUtf8("buttonDowelSelect"));
        buttonDowelSelect->setMinimumSize(QSize(100, 100));
        buttonDowelSelect->setFont(font2);
        buttonDowelSelect->setFocusPolicy(Qt::ClickFocus);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/MyFiles/res/ParametersEditBig.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonDowelSelect->setIcon(icon1);
        buttonDowelSelect->setIconSize(QSize(60, 60));
        buttonDowelSelect->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonDowelSelect->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonDowelSelect);

        buttonSetTolerance = new QToolButton(tabTypes);
        buttonSetTolerance->setObjectName(QString::fromUtf8("buttonSetTolerance"));
        buttonSetTolerance->setMinimumSize(QSize(100, 100));
        buttonSetTolerance->setFont(font2);
        buttonSetTolerance->setFocusPolicy(Qt::ClickFocus);
        buttonSetTolerance->setIcon(icon1);
        buttonSetTolerance->setIconSize(QSize(60, 60));
        buttonSetTolerance->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonSetTolerance->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonSetTolerance);

        buttonTypeSelect = new QToolButton(tabTypes);
        buttonTypeSelect->setObjectName(QString::fromUtf8("buttonTypeSelect"));
        buttonTypeSelect->setMinimumSize(QSize(100, 100));
        buttonTypeSelect->setFont(font2);
        buttonTypeSelect->setFocusPolicy(Qt::ClickFocus);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/MyFiles/res/TypesLarge.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonTypeSelect->setIcon(icon2);
        buttonTypeSelect->setIconSize(QSize(70, 70));
        buttonTypeSelect->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonTypeSelect->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonTypeSelect);

        buttonAddType = new QToolButton(tabTypes);
        buttonAddType->setObjectName(QString::fromUtf8("buttonAddType"));
        buttonAddType->setMinimumSize(QSize(100, 100));
        buttonAddType->setFont(font2);
        buttonAddType->setFocusPolicy(Qt::ClickFocus);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/MyFiles/res/TypeNewIco.ico"), QSize(), QIcon::Normal, QIcon::Off);
        buttonAddType->setIcon(icon3);
        buttonAddType->setIconSize(QSize(60, 60));
        buttonAddType->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonAddType->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonAddType);

        buttonRemoveType = new QToolButton(tabTypes);
        buttonRemoveType->setObjectName(QString::fromUtf8("buttonRemoveType"));
        buttonRemoveType->setMinimumSize(QSize(100, 100));
        buttonRemoveType->setFont(font2);
        buttonRemoveType->setFocusPolicy(Qt::ClickFocus);
        buttonRemoveType->setAcceptDrops(false);
        buttonRemoveType->setAutoFillBackground(false);
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/MyFiles/res/X.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonRemoveType->setIcon(icon4);
        buttonRemoveType->setIconSize(QSize(60, 60));
        buttonRemoveType->setCheckable(false);
        buttonRemoveType->setPopupMode(QToolButton::DelayedPopup);
        buttonRemoveType->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonRemoveType->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonRemoveType);

        buttonTypeSettings = new QToolButton(tabTypes);
        buttonTypeSettings->setObjectName(QString::fromUtf8("buttonTypeSettings"));
        buttonTypeSettings->setMinimumSize(QSize(100, 100));
        buttonTypeSettings->setFont(font2);
        buttonTypeSettings->setFocusPolicy(Qt::ClickFocus);
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/MyFiles/res/ParametersBig.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonTypeSettings->setIcon(icon5);
        buttonTypeSettings->setIconSize(QSize(60, 60));
        buttonTypeSettings->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonTypeSettings->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonTypeSettings);

        buttonResetCounters = new QToolButton(tabTypes);
        buttonResetCounters->setObjectName(QString::fromUtf8("buttonResetCounters"));
        buttonResetCounters->setMinimumSize(QSize(100, 100));
        buttonResetCounters->setFont(font2);
        buttonResetCounters->setFocusPolicy(Qt::ClickFocus);
        buttonResetCounters->setAcceptDrops(false);
        buttonResetCounters->setAutoFillBackground(false);
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/MyFiles/res/resetButton.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonResetCounters->setIcon(icon6);
        buttonResetCounters->setIconSize(QSize(60, 60));
        buttonResetCounters->setCheckable(false);
        buttonResetCounters->setPopupMode(QToolButton::DelayedPopup);
        buttonResetCounters->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonResetCounters->setAutoRaise(true);

        horizontalLayout_3->addWidget(buttonResetCounters);

        horizontalSpacer_3 = new QSpacerItem(830, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        MainMenuBar->addTab(tabTypes, QString());
        tabCameras = new QWidget();
        tabCameras->setObjectName(QString::fromUtf8("tabCameras"));
        MainMenuBar->addTab(tabCameras, QString());
        tabSignals = new QWidget();
        tabSignals->setObjectName(QString::fromUtf8("tabSignals"));
        MainMenuBar->addTab(tabSignals, QString());
        tabImageProcessing = new QWidget();
        tabImageProcessing->setObjectName(QString::fromUtf8("tabImageProcessing"));
        tabImageProcessing->setAcceptDrops(false);
        horizontalLayout = new QHBoxLayout(tabImageProcessing);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox = new QGroupBox(tabImageProcessing);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Comic Sans MS"));
        font3.setPointSize(10);
        font3.setBold(true);
        font3.setWeight(75);
        groupBox->setFont(font3);
        groupBox->setAutoFillBackground(false);
        groupBox->setTitle(QString::fromUtf8(""));
        horizontalLayout_2 = new QHBoxLayout(groupBox);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        buttonImageProcessing = new QToolButton(groupBox);
        buttonImageProcessing->setObjectName(QString::fromUtf8("buttonImageProcessing"));
        buttonImageProcessing->setMinimumSize(QSize(100, 100));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/MyFiles/res/imageProcessing.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonImageProcessing->setIcon(icon7);
        buttonImageProcessing->setIconSize(QSize(60, 45));
        buttonImageProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonImageProcessing->setAutoRaise(true);

        horizontalLayout_2->addWidget(buttonImageProcessing);

        buttonDMStransitionTest = new QToolButton(groupBox);
        buttonDMStransitionTest->setObjectName(QString::fromUtf8("buttonDMStransitionTest"));
        buttonDMStransitionTest->setMinimumSize(QSize(100, 100));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/MBsoftware/res/imageProcessing.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonDMStransitionTest->setIcon(icon8);
        buttonDMStransitionTest->setIconSize(QSize(60, 45));
        buttonDMStransitionTest->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonDMStransitionTest->setAutoRaise(true);

        horizontalLayout_2->addWidget(buttonDMStransitionTest);

        buttonDMS = new QToolButton(groupBox);
        buttonDMS->setObjectName(QString::fromUtf8("buttonDMS"));
        buttonDMS->setMinimumSize(QSize(100, 100));
        buttonDMS->setIcon(icon8);
        buttonDMS->setIconSize(QSize(60, 45));
        buttonDMS->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonDMS->setAutoRaise(true);

        horizontalLayout_2->addWidget(buttonDMS);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        horizontalLayout->addWidget(groupBox);

        MainMenuBar->addTab(tabImageProcessing, QString());
        tabStatistcs = new QWidget();
        tabStatistcs->setObjectName(QString::fromUtf8("tabStatistcs"));
        horizontalLayout_5 = new QHBoxLayout(tabStatistcs);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        buttonStatusBox = new QToolButton(tabStatistcs);
        buttonStatusBox->setObjectName(QString::fromUtf8("buttonStatusBox"));
        buttonStatusBox->setMinimumSize(QSize(100, 100));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8("../../../../Users/Ales/.designer/backup/res/status.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonStatusBox->setIcon(icon9);
        buttonStatusBox->setIconSize(QSize(70, 80));
        buttonStatusBox->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonStatusBox->setAutoRaise(true);

        horizontalLayout_5->addWidget(buttonStatusBox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);

        buttonAboutUs = new QToolButton(tabStatistcs);
        buttonAboutUs->setObjectName(QString::fromUtf8("buttonAboutUs"));
        buttonAboutUs->setMinimumSize(QSize(100, 100));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/MyFiles/res/about.bmp"), QSize(), QIcon::Normal, QIcon::Off);
        buttonAboutUs->setIcon(icon10);
        buttonAboutUs->setIconSize(QSize(70, 80));
        buttonAboutUs->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonAboutUs->setAutoRaise(true);

        horizontalLayout_5->addWidget(buttonAboutUs);

        MainMenuBar->addTab(tabStatistcs, QString());
        labelLogo = new QLabel(centralWidget);
        labelLogo->setObjectName(QString::fromUtf8("labelLogo"));
        labelLogo->setGeometry(QRect(1610, 970, 301, 101));
        labelLogo->setPixmap(QPixmap(QString::fromUtf8(":/MyFiles/res/MB.bmp")));
        labelLogo->setScaledContents(true);
        frameMeasurements = new QFrame(centralWidget);
        frameMeasurements->setObjectName(QString::fromUtf8("frameMeasurements"));
        frameMeasurements->setGeometry(QRect(10, 170, 641, 541));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(frameMeasurements->sizePolicy().hasHeightForWidth());
        frameMeasurements->setSizePolicy(sizePolicy3);
        frameMeasurements->setMinimumSize(QSize(400, 100));
        frameMeasurements->setMaximumSize(QSize(1000, 600));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Calibri"));
        font4.setPointSize(14);
        font4.setBold(true);
        font4.setWeight(75);
        frameMeasurements->setFont(font4);
        frameMeasurements->setFrameShape(QFrame::Box);
        frameMeasurements->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget = new QWidget(frameMeasurements);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(9, 9, 621, 521));
        layoutMeasurement = new QVBoxLayout(verticalLayoutWidget);
        layoutMeasurement->setSpacing(6);
        layoutMeasurement->setContentsMargins(11, 11, 11, 11);
        layoutMeasurement->setObjectName(QString::fromUtf8("layoutMeasurement"));
        layoutMeasurement->setContentsMargins(0, 0, 0, 0);
        imageViewMainWindow_0 = new QGraphicsView(centralWidget);
        imageViewMainWindow_0->setObjectName(QString::fromUtf8("imageViewMainWindow_0"));
        imageViewMainWindow_0->setGeometry(QRect(10, 720, 641, 311));
        imageViewMainWindow_0->setMinimumSize(QSize(0, 0));
        imageViewMainWindow_0->setFrameShape(QFrame::WinPanel);
        imageViewMainWindow_0->setFrameShadow(QFrame::Sunken);
        comboBoxSelectCamera = new QComboBox(centralWidget);
        comboBoxSelectCamera->setObjectName(QString::fromUtf8("comboBoxSelectCamera"));
        comboBoxSelectCamera->setGeometry(QRect(1360, 680, 201, 22));
        statusList = new QListWidget(centralWidget);
        statusList->setObjectName(QString::fromUtf8("statusList"));
        statusList->setGeometry(QRect(1260, 720, 311, 141));
        statisticsFrame = new QFrame(centralWidget);
        statisticsFrame->setObjectName(QString::fromUtf8("statisticsFrame"));
        statisticsFrame->setGeometry(QRect(700, 680, 461, 211));
        statisticsFrame->setFrameShape(QFrame::Box);
        statisticsFrame->setFrameShadow(QFrame::Sunken);
        gridLayoutWidget = new QWidget(statisticsFrame);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 40, 441, 161));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        labelGoodProcent = new QLabel(gridLayoutWidget);
        labelGoodProcent->setObjectName(QString::fromUtf8("labelGoodProcent"));
        labelGoodProcent->setFont(font);
        labelGoodProcent->setStyleSheet(QString::fromUtf8(""));
        labelGoodProcent->setFrameShape(QFrame::Panel);
        labelGoodProcent->setFrameShadow(QFrame::Sunken);
        labelGoodProcent->setLineWidth(3);
        labelGoodProcent->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelGoodProcent, 0, 2, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(180, 0));
        label_3->setFont(font);
        label_3->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 150);\n"
""));
        label_3->setFrameShape(QFrame::Panel);
        label_3->setFrameShadow(QFrame::Sunken);
        label_3->setLineWidth(3);

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        labelTotal = new QLabel(gridLayoutWidget);
        labelTotal->setObjectName(QString::fromUtf8("labelTotal"));
        labelTotal->setFont(font);
        labelTotal->setFrameShape(QFrame::Panel);
        labelTotal->setFrameShadow(QFrame::Sunken);
        labelTotal->setLineWidth(3);
        labelTotal->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelTotal, 3, 1, 1, 2);

        labelGood_4 = new QLabel(gridLayoutWidget);
        labelGood_4->setObjectName(QString::fromUtf8("labelGood_4"));
        labelGood_4->setFont(font);
        labelGood_4->setStyleSheet(QString::fromUtf8(""));
        labelGood_4->setFrameShape(QFrame::Panel);
        labelGood_4->setFrameShadow(QFrame::Sunken);
        labelGood_4->setLineWidth(3);
        labelGood_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelGood_4, 0, 1, 1, 1);

        labelBadProcent = new QLabel(gridLayoutWidget);
        labelBadProcent->setObjectName(QString::fromUtf8("labelBadProcent"));
        labelBadProcent->setFont(font);
        labelBadProcent->setFrameShape(QFrame::Panel);
        labelBadProcent->setFrameShadow(QFrame::Sunken);
        labelBadProcent->setLineWidth(3);
        labelBadProcent->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelBadProcent, 1, 2, 1, 1);

        label_10 = new QLabel(gridLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(180, 0));
        label_10->setFont(font);
        label_10->setFrameShape(QFrame::Panel);
        label_10->setFrameShadow(QFrame::Sunken);
        label_10->setLineWidth(3);

        gridLayout->addWidget(label_10, 3, 0, 1, 1);

        labelBAD_2 = new QLabel(gridLayoutWidget);
        labelBAD_2->setObjectName(QString::fromUtf8("labelBAD_2"));
        labelBAD_2->setFont(font);
        labelBAD_2->setFrameShape(QFrame::Panel);
        labelBAD_2->setFrameShadow(QFrame::Sunken);
        labelBAD_2->setLineWidth(3);
        labelBAD_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelBAD_2, 1, 1, 1, 1);

        label_9 = new QLabel(gridLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(180, 0));
        label_9->setFont(font);
        label_9->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        label_9->setFrameShape(QFrame::Panel);
        label_9->setFrameShadow(QFrame::Sunken);
        label_9->setLineWidth(3);

        gridLayout->addWidget(label_9, 0, 0, 1, 1);

        label_11 = new QLabel(gridLayoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(180, 0));
        label_11->setFont(font);
        label_11->setStyleSheet(QString::fromUtf8("background-color: rgb(150, 255, 150);\n"
""));
        label_11->setFrameShape(QFrame::Panel);
        label_11->setFrameShadow(QFrame::Sunken);
        label_11->setLineWidth(3);

        gridLayout->addWidget(label_11, 2, 0, 1, 1);

        labelValveGood = new QLabel(gridLayoutWidget);
        labelValveGood->setObjectName(QString::fromUtf8("labelValveGood"));
        labelValveGood->setFont(font);
        labelValveGood->setStyleSheet(QString::fromUtf8(""));
        labelValveGood->setFrameShape(QFrame::Panel);
        labelValveGood->setFrameShadow(QFrame::Sunken);
        labelValveGood->setLineWidth(3);
        labelValveGood->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelValveGood, 2, 1, 1, 1);

        labelGood_3 = new QLabel(gridLayoutWidget);
        labelGood_3->setObjectName(QString::fromUtf8("labelGood_3"));
        labelGood_3->setFont(font);
        labelGood_3->setStyleSheet(QString::fromUtf8(""));
        labelGood_3->setFrameShape(QFrame::Panel);
        labelGood_3->setFrameShadow(QFrame::Sunken);
        labelGood_3->setLineWidth(3);
        labelGood_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelGood_3, 2, 2, 1, 1);

        label_2 = new QLabel(statisticsFrame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 0, 351, 41));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Calibri"));
        font5.setPointSize(16);
        font5.setBold(true);
        font5.setItalic(true);
        font5.setWeight(75);
        label_2->setFont(font5);
        label_2->setFrameShape(QFrame::NoFrame);
        label_2->setFrameShadow(QFrame::Sunken);
        labelTest = new QLabel(centralWidget);
        labelTest->setObjectName(QString::fromUtf8("labelTest"));
        labelTest->setGeometry(QRect(830, 950, 271, 111));
        labelTest->setFrameShape(QFrame::Box);
        verticalLayoutWidget_3 = new QWidget(centralWidget);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(1150, 900, 391, 131));
        layoutTest = new QVBoxLayout(verticalLayoutWidget_3);
        layoutTest->setSpacing(6);
        layoutTest->setContentsMargins(11, 11, 11, 11);
        layoutTest->setObjectName(QString::fromUtf8("layoutTest"));
        layoutTest->setContentsMargins(0, 0, 0, 0);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(660, 280, 911, 391));
        groupBox_2->setFont(font4);
        groupBox_2->setFlat(false);
        groupBox_2->setCheckable(false);
        imageViewPresortLive = new QGraphicsView(groupBox_2);
        imageViewPresortLive->setObjectName(QString::fromUtf8("imageViewPresortLive"));
        imageViewPresortLive->setGeometry(QRect(10, 30, 311, 211));
        imageViewPresortLive->setMinimumSize(QSize(0, 0));
        imageViewPresortLive->setFrameShape(QFrame::WinPanel);
        imageViewPresortLive->setFrameShadow(QFrame::Sunken);
        imageViewPresort = new QGraphicsView(groupBox_2);
        imageViewPresort->setObjectName(QString::fromUtf8("imageViewPresort"));
        imageViewPresort->setGeometry(QRect(320, 30, 331, 211));
        imageViewPresort->setMinimumSize(QSize(0, 0));
        imageViewPresort->setFrameShape(QFrame::WinPanel);
        imageViewPresort->setFrameShadow(QFrame::Sunken);
        labelPresortDowel2 = new QLabel(groupBox_2);
        labelPresortDowel2->setObjectName(QString::fromUtf8("labelPresortDowel2"));
        labelPresortDowel2->setGeometry(QRect(660, 130, 241, 41));
        labelPresortDowel2->setFrameShape(QFrame::WinPanel);
        labelPresortDowel2->setFrameShadow(QFrame::Sunken);
        labelPresortDowel0 = new QLabel(groupBox_2);
        labelPresortDowel0->setObjectName(QString::fromUtf8("labelPresortDowel0"));
        labelPresortDowel0->setGeometry(QRect(660, 30, 241, 41));
        labelPresortDowel0->setFrameShape(QFrame::WinPanel);
        labelPresortDowel0->setFrameShadow(QFrame::Sunken);
        labelPresortDowel1 = new QLabel(groupBox_2);
        labelPresortDowel1->setObjectName(QString::fromUtf8("labelPresortDowel1"));
        labelPresortDowel1->setGeometry(QRect(660, 80, 241, 41));
        labelPresortDowel1->setFrameShape(QFrame::WinPanel);
        labelPresortDowel1->setFrameShadow(QFrame::Sunken);
        verticalLayoutWidget_4 = new QWidget(groupBox_2);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(10, 250, 641, 131));
        layoutPresortTable = new QVBoxLayout(verticalLayoutWidget_4);
        layoutPresortTable->setSpacing(6);
        layoutPresortTable->setContentsMargins(11, 11, 11, 11);
        layoutPresortTable->setObjectName(QString::fromUtf8("layoutPresortTable"));
        layoutPresortTable->setContentsMargins(0, 0, 0, 0);
        presortList = new QListWidget(groupBox_2);
        presortList->setObjectName(QString::fromUtf8("presortList"));
        presortList->setGeometry(QRect(660, 250, 241, 131));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(660, 10, 1251, 41));
        frame->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame->setFrameShape(QFrame::Panel);
        frame->setFrameShadow(QFrame::Sunken);
        horizontalLayout_6 = new QHBoxLayout(frame);
        horizontalLayout_6->setSpacing(3);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(3, 3, 3, 3);
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Calibri"));
        font6.setPointSize(24);
        font6.setBold(true);
        font6.setWeight(75);
        label->setFont(font6);
        label->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label);

        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(660, 61, 1251, 101));
        frame_2->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame_2->setFrameShape(QFrame::Panel);
        frame_2->setFrameShadow(QFrame::Sunken);
        gridLayout_2 = new QGridLayout(frame_2);
        gridLayout_2->setSpacing(3);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(3, 3, 3, 3);
        frame_4 = new QFrame(frame_2);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setMinimumSize(QSize(200, 0));
        frame_4->setMaximumSize(QSize(400, 16777215));
        frame_4->setFrameShape(QFrame::Box);
        frame_4->setFrameShadow(QFrame::Sunken);
        frame_4->setLineWidth(2);
        gridLayout_4 = new QGridLayout(frame_4);
        gridLayout_4->setSpacing(3);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(3, 3, 3, 3);
        label_12 = new QLabel(frame_4);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(80, 10));
        label_12->setMaximumSize(QSize(80, 16777215));
        label_12->setFont(font4);
        label_12->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_12->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_12, 2, 1, 1, 1);

        label_14 = new QLabel(frame_4);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(180, 10));
        label_14->setMaximumSize(QSize(200, 16777215));
        label_14->setFont(font4);
        label_14->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_14->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_14, 0, 2, 1, 1);

        label_16 = new QLabel(frame_4);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(180, 10));
        label_16->setMaximumSize(QSize(200, 16777215));
        label_16->setFont(font4);
        label_16->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_16->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_16, 1, 2, 1, 1);

        label_7 = new QLabel(frame_4);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(80, 10));
        label_7->setMaximumSize(QSize(80, 16777215));
        label_7->setFont(font4);
        label_7->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_7->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_7, 1, 1, 1, 1);

        label_6 = new QLabel(frame_4);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(80, 10));
        label_6->setMaximumSize(QSize(80, 16777215));
        label_6->setFont(font4);
        label_6->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_6, 0, 1, 1, 1);

        label_13 = new QLabel(frame_4);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(180, 10));
        label_13->setMaximumSize(QSize(80, 16777215));
        label_13->setFont(font4);
        label_13->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_13->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_13, 2, 2, 1, 1);

        label_4 = new QLabel(frame_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(100, 30));
        label_4->setMaximumSize(QSize(100, 16777215));
        label_4->setFont(font);
        label_4->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_4->addWidget(label_4, 1, 0, 1, 1);


        gridLayout_2->addWidget(frame_4, 2, 2, 1, 1);

        frame_5 = new QFrame(frame_2);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setFrameShape(QFrame::Box);
        frame_5->setFrameShadow(QFrame::Sunken);
        frame_5->setLineWidth(2);
        gridLayout_3 = new QGridLayout(frame_5);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setHorizontalSpacing(3);
        gridLayout_3->setVerticalSpacing(4);
        gridLayout_3->setContentsMargins(3, 3, 3, 3);
        label_39 = new QLabel(frame_5);
        label_39->setObjectName(QString::fromUtf8("label_39"));
        label_39->setMinimumSize(QSize(110, 25));
        label_39->setMaximumSize(QSize(110, 16777215));
        label_39->setFont(font4);
        label_39->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_39->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_39, 2, 7, 1, 1);

        label_25 = new QLabel(frame_5);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setMinimumSize(QSize(50, 25));
        label_25->setMaximumSize(QSize(50, 16777215));
        label_25->setFont(font4);
        label_25->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_25->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_25, 0, 2, 1, 1);

        label_27 = new QLabel(frame_5);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setMinimumSize(QSize(40, 25));
        label_27->setMaximumSize(QSize(30, 16777215));
        label_27->setFont(font4);
        label_27->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_27->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_27, 3, 3, 1, 1);

        label_19 = new QLabel(frame_5);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(110, 25));
        label_19->setMaximumSize(QSize(110, 16777215));
        label_19->setFont(font4);
        label_19->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_19->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_19, 2, 1, 1, 1);

        label_24 = new QLabel(frame_5);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setMinimumSize(QSize(50, 25));
        label_24->setMaximumSize(QSize(50, 16777215));
        label_24->setFont(font4);
        label_24->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_24->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_24, 3, 2, 1, 1);

        label_28 = new QLabel(frame_5);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setMinimumSize(QSize(40, 25));
        label_28->setMaximumSize(QSize(30, 16777215));
        label_28->setFont(font4);
        label_28->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_28->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_28, 2, 3, 1, 1);

        label_40 = new QLabel(frame_5);
        label_40->setObjectName(QString::fromUtf8("label_40"));
        label_40->setMinimumSize(QSize(110, 25));
        label_40->setMaximumSize(QSize(110, 16777215));
        label_40->setFont(font4);
        label_40->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_40->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_40, 3, 7, 1, 1);

        label_36 = new QLabel(frame_5);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        label_36->setMinimumSize(QSize(40, 25));
        label_36->setMaximumSize(QSize(30, 16777215));
        label_36->setFont(font4);
        label_36->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_36->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_36, 2, 6, 1, 1);

        label_35 = new QLabel(frame_5);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setMinimumSize(QSize(40, 25));
        label_35->setMaximumSize(QSize(30, 16777215));
        label_35->setFont(font4);
        label_35->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_35->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_35, 0, 6, 1, 1);

        label_34 = new QLabel(frame_5);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setMinimumSize(QSize(50, 25));
        label_34->setMaximumSize(QSize(50, 16777215));
        label_34->setFont(font4);
        label_34->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_34->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_34, 3, 5, 1, 1);

        label_33 = new QLabel(frame_5);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setMinimumSize(QSize(50, 25));
        label_33->setMaximumSize(QSize(50, 16777215));
        label_33->setFont(font4);
        label_33->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_33->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_33, 2, 5, 1, 1);

        label_32 = new QLabel(frame_5);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setMinimumSize(QSize(50, 25));
        label_32->setMaximumSize(QSize(50, 16777215));
        label_32->setFont(font4);
        label_32->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_32->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_32, 0, 5, 1, 1);

        label_41 = new QLabel(frame_5);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setMinimumSize(QSize(50, 25));
        label_41->setMaximumSize(QSize(50, 16777215));
        label_41->setFont(font4);
        label_41->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_41->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_41, 0, 8, 1, 1);

        label_37 = new QLabel(frame_5);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setMinimumSize(QSize(40, 25));
        label_37->setMaximumSize(QSize(30, 16777215));
        label_37->setFont(font4);
        label_37->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_37->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_37, 3, 6, 1, 1);

        label_38 = new QLabel(frame_5);
        label_38->setObjectName(QString::fromUtf8("label_38"));
        label_38->setMinimumSize(QSize(110, 25));
        label_38->setMaximumSize(QSize(110, 16777215));
        label_38->setFont(font4);
        label_38->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_38->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_38, 0, 7, 1, 1);

        label_18 = new QLabel(frame_5);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setMinimumSize(QSize(100, 30));
        label_18->setMaximumSize(QSize(100, 16777215));
        label_18->setFont(font);
        label_18->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_18->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_18, 2, 0, 1, 1);

        label_26 = new QLabel(frame_5);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setMinimumSize(QSize(40, 25));
        label_26->setMaximumSize(QSize(30, 16777215));
        label_26->setFont(font4);
        label_26->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_26->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_26, 0, 3, 1, 1);

        label_29 = new QLabel(frame_5);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setMinimumSize(QSize(110, 25));
        label_29->setMaximumSize(QSize(110, 16777215));
        label_29->setFont(font4);
        label_29->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_29->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_29, 0, 4, 1, 1);

        label_31 = new QLabel(frame_5);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setMinimumSize(QSize(110, 25));
        label_31->setMaximumSize(QSize(110, 16777215));
        label_31->setFont(font4);
        label_31->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_31->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_31, 3, 4, 1, 1);

        label_23 = new QLabel(frame_5);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setMinimumSize(QSize(50, 25));
        label_23->setMaximumSize(QSize(50, 16777215));
        label_23->setFont(font4);
        label_23->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_23->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_23, 2, 2, 1, 1);

        label_21 = new QLabel(frame_5);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(110, 25));
        label_21->setMaximumSize(QSize(110, 16777215));
        label_21->setFont(font4);
        label_21->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_21->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_21, 0, 1, 1, 1);

        label_22 = new QLabel(frame_5);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setMinimumSize(QSize(110, 25));
        label_22->setMaximumSize(QSize(110, 16777215));
        label_22->setFont(font4);
        label_22->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_22->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_22, 3, 1, 1, 1);

        label_30 = new QLabel(frame_5);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setMinimumSize(QSize(110, 25));
        label_30->setMaximumSize(QSize(110, 16777215));
        label_30->setFont(font4);
        label_30->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_30->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_30, 2, 4, 1, 1);

        label_42 = new QLabel(frame_5);
        label_42->setObjectName(QString::fromUtf8("label_42"));
        label_42->setMinimumSize(QSize(40, 25));
        label_42->setMaximumSize(QSize(30, 16777215));
        label_42->setFont(font4);
        label_42->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_42->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_42, 0, 9, 1, 1);

        label_43 = new QLabel(frame_5);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setMinimumSize(QSize(50, 25));
        label_43->setMaximumSize(QSize(50, 16777215));
        label_43->setFont(font4);
        label_43->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_43->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_43, 2, 8, 1, 1);

        label_44 = new QLabel(frame_5);
        label_44->setObjectName(QString::fromUtf8("label_44"));
        label_44->setMinimumSize(QSize(50, 25));
        label_44->setMaximumSize(QSize(50, 16777215));
        label_44->setFont(font4);
        label_44->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_44->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_44, 3, 8, 1, 1);

        label_46 = new QLabel(frame_5);
        label_46->setObjectName(QString::fromUtf8("label_46"));
        label_46->setMinimumSize(QSize(40, 25));
        label_46->setMaximumSize(QSize(30, 16777215));
        label_46->setFont(font4);
        label_46->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_46->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_46, 2, 9, 1, 1);

        label_47 = new QLabel(frame_5);
        label_47->setObjectName(QString::fromUtf8("label_47"));
        label_47->setMinimumSize(QSize(40, 25));
        label_47->setMaximumSize(QSize(30, 16777215));
        label_47->setFont(font4);
        label_47->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_47->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_47, 3, 9, 1, 1);


        gridLayout_2->addWidget(frame_5, 2, 3, 1, 1);

        frame_3 = new QFrame(centralWidget);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setGeometry(QRect(660, 170, 1251, 101));
        frame_3->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame_3->setFrameShape(QFrame::Panel);
        frame_3->setFrameShadow(QFrame::Sunken);
        gridLayout_7 = new QGridLayout(frame_3);
        gridLayout_7->setSpacing(3);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_7->setContentsMargins(3, 3, 3, 3);
        frame_7 = new QFrame(frame_3);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        frame_7->setMinimumSize(QSize(200, 0));
        frame_7->setMaximumSize(QSize(400, 16777215));
        frame_7->setFrameShape(QFrame::Box);
        frame_7->setFrameShadow(QFrame::Sunken);
        frame_7->setLineWidth(2);
        gridLayout_8 = new QGridLayout(frame_7);
        gridLayout_8->setSpacing(3);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(3, 3, 3, 3);
        label_8 = new QLabel(frame_7);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(80, 10));
        label_8->setMaximumSize(QSize(80, 16777215));
        label_8->setFont(font4);
        label_8->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_8->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_8, 1, 1, 1, 1);

        labelBAD = new QLabel(frame_7);
        labelBAD->setObjectName(QString::fromUtf8("labelBAD"));
        labelBAD->setMinimumSize(QSize(100, 10));
        labelBAD->setMaximumSize(QSize(100, 16777215));
        labelBAD->setFont(font4);
        labelBAD->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelBAD->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(labelBAD, 1, 2, 1, 1);

        label_106 = new QLabel(frame_7);
        label_106->setObjectName(QString::fromUtf8("label_106"));
        label_106->setMinimumSize(QSize(100, 10));
        label_106->setMaximumSize(QSize(100, 16777215));
        label_106->setFont(font4);
        label_106->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_106->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_106, 2, 2, 1, 1);

        label_108 = new QLabel(frame_7);
        label_108->setObjectName(QString::fromUtf8("label_108"));
        label_108->setMinimumSize(QSize(3, 10));
        label_108->setMaximumSize(QSize(30, 16777215));
        label_108->setFont(font4);
        label_108->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_108->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_108, 0, 3, 1, 1);

        label_105 = new QLabel(frame_7);
        label_105->setObjectName(QString::fromUtf8("label_105"));
        label_105->setMinimumSize(QSize(80, 10));
        label_105->setMaximumSize(QSize(80, 16777215));
        label_105->setFont(font4);
        label_105->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_105->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_105, 0, 1, 1, 1);

        label_15 = new QLabel(frame_7);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(80, 10));
        label_15->setMaximumSize(QSize(80, 16777215));
        label_15->setFont(font4);
        label_15->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_15->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_15, 2, 1, 1, 1);

        label_109 = new QLabel(frame_7);
        label_109->setObjectName(QString::fromUtf8("label_109"));
        label_109->setMinimumSize(QSize(3, 10));
        label_109->setMaximumSize(QSize(30, 16777215));
        label_109->setFont(font4);
        label_109->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_109->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_109, 1, 3, 1, 1);

        labelGood = new QLabel(frame_7);
        labelGood->setObjectName(QString::fromUtf8("labelGood"));
        labelGood->setMinimumSize(QSize(100, 10));
        labelGood->setMaximumSize(QSize(100, 16777215));
        labelGood->setFont(font4);
        labelGood->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelGood->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(labelGood, 0, 2, 1, 1);

        label_5 = new QLabel(frame_7);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(100, 30));
        label_5->setMaximumSize(QSize(100, 16777215));
        label_5->setFont(font);
        label_5->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_5->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_5, 1, 0, 1, 1);

        label_111 = new QLabel(frame_7);
        label_111->setObjectName(QString::fromUtf8("label_111"));
        label_111->setMinimumSize(QSize(40, 10));
        label_111->setMaximumSize(QSize(40, 16777215));
        label_111->setFont(font4);
        label_111->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_111->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_111, 0, 4, 1, 1);

        label_113 = new QLabel(frame_7);
        label_113->setObjectName(QString::fromUtf8("label_113"));
        label_113->setMinimumSize(QSize(40, 10));
        label_113->setMaximumSize(QSize(40, 16777215));
        label_113->setFont(font4);
        label_113->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_113->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_113, 1, 4, 1, 1);


        gridLayout_7->addWidget(frame_7, 0, 0, 1, 1);

        frame_6 = new QFrame(frame_3);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setFrameShape(QFrame::Box);
        frame_6->setFrameShadow(QFrame::Sunken);
        frame_6->setLineWidth(2);
        gridLayout_6 = new QGridLayout(frame_6);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setHorizontalSpacing(3);
        gridLayout_6->setVerticalSpacing(4);
        gridLayout_6->setContentsMargins(3, 3, 3, 3);
        label_76 = new QLabel(frame_6);
        label_76->setObjectName(QString::fromUtf8("label_76"));
        label_76->setMinimumSize(QSize(50, 25));
        label_76->setMaximumSize(QSize(50, 16777215));
        label_76->setFont(font4);
        label_76->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_76->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_76, 0, 2, 1, 1);

        label_78 = new QLabel(frame_6);
        label_78->setObjectName(QString::fromUtf8("label_78"));
        label_78->setMinimumSize(QSize(90, 25));
        label_78->setMaximumSize(QSize(90, 16777215));
        label_78->setFont(font4);
        label_78->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_78->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_78, 2, 1, 1, 1);

        label_90 = new QLabel(frame_6);
        label_90->setObjectName(QString::fromUtf8("label_90"));
        label_90->setMinimumSize(QSize(100, 30));
        label_90->setMaximumSize(QSize(100, 16777215));
        label_90->setFont(font);
        label_90->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_90->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_90, 2, 0, 1, 1);

        label_80 = new QLabel(frame_6);
        label_80->setObjectName(QString::fromUtf8("label_80"));
        label_80->setMinimumSize(QSize(90, 25));
        label_80->setMaximumSize(QSize(90, 16777215));
        label_80->setFont(font4);
        label_80->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_80->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_80, 2, 3, 1, 1);

        label_94 = new QLabel(frame_6);
        label_94->setObjectName(QString::fromUtf8("label_94"));
        label_94->setMinimumSize(QSize(50, 25));
        label_94->setMaximumSize(QSize(50, 16777215));
        label_94->setFont(font4);
        label_94->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_94->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_94, 2, 2, 1, 1);

        label_79 = new QLabel(frame_6);
        label_79->setObjectName(QString::fromUtf8("label_79"));
        label_79->setMinimumSize(QSize(50, 25));
        label_79->setMaximumSize(QSize(50, 16777215));
        label_79->setFont(font4);
        label_79->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_79->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_79, 3, 2, 1, 1);

        label_96 = new QLabel(frame_6);
        label_96->setObjectName(QString::fromUtf8("label_96"));
        label_96->setMinimumSize(QSize(90, 25));
        label_96->setMaximumSize(QSize(90, 16777215));
        label_96->setFont(font4);
        label_96->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_96->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_96, 3, 1, 1, 1);

        label_91 = new QLabel(frame_6);
        label_91->setObjectName(QString::fromUtf8("label_91"));
        label_91->setMinimumSize(QSize(90, 25));
        label_91->setMaximumSize(QSize(90, 16777215));
        label_91->setFont(font4);
        label_91->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_91->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_91, 0, 3, 1, 1);

        label_95 = new QLabel(frame_6);
        label_95->setObjectName(QString::fromUtf8("label_95"));
        label_95->setMinimumSize(QSize(90, 25));
        label_95->setMaximumSize(QSize(90, 16777215));
        label_95->setFont(font4);
        label_95->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_95->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_95, 0, 1, 1, 1);

        label_81 = new QLabel(frame_6);
        label_81->setObjectName(QString::fromUtf8("label_81"));
        label_81->setMinimumSize(QSize(50, 25));
        label_81->setMaximumSize(QSize(50, 16777215));
        label_81->setFont(font4);
        label_81->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_81->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_81, 0, 4, 1, 1);

        label_82 = new QLabel(frame_6);
        label_82->setObjectName(QString::fromUtf8("label_82"));
        label_82->setMinimumSize(QSize(50, 25));
        label_82->setMaximumSize(QSize(50, 16777215));
        label_82->setFont(font4);
        label_82->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_82->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_82, 2, 4, 1, 1);


        gridLayout_7->addWidget(frame_6, 0, 1, 1, 1);

        frame_8 = new QFrame(frame_3);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        frame_8->setMinimumSize(QSize(200, 0));
        frame_8->setMaximumSize(QSize(400, 16777215));
        frame_8->setFrameShape(QFrame::Box);
        frame_8->setFrameShadow(QFrame::Sunken);
        frame_8->setLineWidth(2);
        gridLayout_9 = new QGridLayout(frame_8);
        gridLayout_9->setSpacing(3);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        gridLayout_9->setContentsMargins(3, 3, 3, 3);
        label_103 = new QLabel(frame_8);
        label_103->setObjectName(QString::fromUtf8("label_103"));
        label_103->setMinimumSize(QSize(80, 10));
        label_103->setMaximumSize(QSize(80, 16777215));
        label_103->setFont(font4);
        label_103->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_103->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_103, 1, 1, 1, 1);

        labelBAD_3 = new QLabel(frame_8);
        labelBAD_3->setObjectName(QString::fromUtf8("labelBAD_3"));
        labelBAD_3->setMinimumSize(QSize(100, 10));
        labelBAD_3->setMaximumSize(QSize(100, 16777215));
        labelBAD_3->setFont(font4);
        labelBAD_3->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelBAD_3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(labelBAD_3, 1, 2, 1, 1);

        label_110 = new QLabel(frame_8);
        label_110->setObjectName(QString::fromUtf8("label_110"));
        label_110->setMinimumSize(QSize(100, 10));
        label_110->setMaximumSize(QSize(100, 16777215));
        label_110->setFont(font4);
        label_110->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_110->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_110, 2, 2, 1, 1);

        label_114 = new QLabel(frame_8);
        label_114->setObjectName(QString::fromUtf8("label_114"));
        label_114->setMinimumSize(QSize(3, 10));
        label_114->setMaximumSize(QSize(30, 16777215));
        label_114->setFont(font4);
        label_114->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_114->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_114, 0, 3, 1, 1);

        label_115 = new QLabel(frame_8);
        label_115->setObjectName(QString::fromUtf8("label_115"));
        label_115->setMinimumSize(QSize(80, 10));
        label_115->setMaximumSize(QSize(80, 16777215));
        label_115->setFont(font4);
        label_115->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_115->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_115, 0, 1, 1, 1);

        label_104 = new QLabel(frame_8);
        label_104->setObjectName(QString::fromUtf8("label_104"));
        label_104->setMinimumSize(QSize(80, 10));
        label_104->setMaximumSize(QSize(80, 16777215));
        label_104->setFont(font4);
        label_104->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_104->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_104, 2, 1, 1, 1);

        label_116 = new QLabel(frame_8);
        label_116->setObjectName(QString::fromUtf8("label_116"));
        label_116->setMinimumSize(QSize(3, 10));
        label_116->setMaximumSize(QSize(30, 16777215));
        label_116->setFont(font4);
        label_116->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_116->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_116, 1, 3, 1, 1);

        labelGood_2 = new QLabel(frame_8);
        labelGood_2->setObjectName(QString::fromUtf8("labelGood_2"));
        labelGood_2->setMinimumSize(QSize(100, 10));
        labelGood_2->setMaximumSize(QSize(100, 16777215));
        labelGood_2->setFont(font4);
        labelGood_2->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelGood_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(labelGood_2, 0, 2, 1, 1);

        label_117 = new QLabel(frame_8);
        label_117->setObjectName(QString::fromUtf8("label_117"));
        label_117->setMinimumSize(QSize(200, 30));
        label_117->setMaximumSize(QSize(200, 16777215));
        label_117->setFont(font);
        label_117->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_117->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_117, 1, 0, 1, 1);

        label_118 = new QLabel(frame_8);
        label_118->setObjectName(QString::fromUtf8("label_118"));
        label_118->setMinimumSize(QSize(40, 10));
        label_118->setMaximumSize(QSize(40, 16777215));
        label_118->setFont(font4);
        label_118->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_118->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_118, 0, 4, 1, 1);

        label_119 = new QLabel(frame_8);
        label_119->setObjectName(QString::fromUtf8("label_119"));
        label_119->setMinimumSize(QSize(40, 10));
        label_119->setMaximumSize(QSize(40, 16777215));
        label_119->setFont(font4);
        label_119->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_119->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_119, 1, 4, 1, 1);


        gridLayout_7->addWidget(frame_8, 0, 2, 1, 1);

        label_17 = new QLabel(centralWidget);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(1480, 770, 100, 31));
        label_17->setMinimumSize(QSize(100, 10));
        label_17->setMaximumSize(QSize(200, 16777215));
        label_17->setFont(font4);
        label_17->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_17->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_20 = new QLabel(centralWidget);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(1730, 780, 200, 79));
        label_20->setMinimumSize(QSize(100, 10));
        label_20->setMaximumSize(QSize(200, 16777215));
        label_20->setFont(font4);
        label_20->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_20->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_107 = new QLabel(centralWidget);
        label_107->setObjectName(QString::fromUtf8("label_107"));
        label_107->setGeometry(QRect(1270, 650, 80, 22));
        label_107->setMinimumSize(QSize(80, 10));
        label_107->setMaximumSize(QSize(80, 16777215));
        label_107->setFont(font4);
        label_107->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_107->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_112 = new QLabel(centralWidget);
        label_112->setObjectName(QString::fromUtf8("label_112"));
        label_112->setGeometry(QRect(1490, 690, 40, 21));
        label_112->setMinimumSize(QSize(40, 10));
        label_112->setMaximumSize(QSize(40, 16777215));
        label_112->setFont(font4);
        label_112->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_112->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        MBsoftwareClass->setCentralWidget(centralWidget);

        retranslateUi(MBsoftwareClass);

        MainMenuBar->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MBsoftwareClass);
    } // setupUi

    void retranslateUi(QMainWindow *MBsoftwareClass)
    {
        MBsoftwareClass->setWindowTitle(QCoreApplication::translate("MBsoftwareClass", "MBsoftware", nullptr));
        labelDeviceStatus->setText(QCoreApplication::translate("MBsoftwareClass", "DEVICE STATUS:", nullptr));
        labelLogin->setText(QString());
        labelStatusNaprave->setText(QString());
        labelStatus->setText(QString());
        labelStatusNavijalka->setText(QString());
        labelKoracno->setText(QString());
        labelTip->setText(QString());
        buttonLogin->setText(QCoreApplication::translate("MBsoftwareClass", "Login", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabLogin), QCoreApplication::translate("MBsoftwareClass", "Login", nullptr));
        buttonDowelSelect->setText(QCoreApplication::translate("MBsoftwareClass", "Type Select", nullptr));
        buttonSetTolerance->setText(QCoreApplication::translate("MBsoftwareClass", "Edit tolerance", nullptr));
        buttonTypeSelect->setText(QCoreApplication::translate("MBsoftwareClass", "Type select", nullptr));
        buttonAddType->setText(QCoreApplication::translate("MBsoftwareClass", "Add type", nullptr));
        buttonRemoveType->setText(QCoreApplication::translate("MBsoftwareClass", "Remove type", nullptr));
        buttonTypeSettings->setText(QCoreApplication::translate("MBsoftwareClass", "Type Settings", nullptr));
        buttonResetCounters->setText(QCoreApplication::translate("MBsoftwareClass", "Reset Counters", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabTypes), QCoreApplication::translate("MBsoftwareClass", "Types", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabCameras), QCoreApplication::translate("MBsoftwareClass", "Cameras", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabSignals), QCoreApplication::translate("MBsoftwareClass", "I/O signals", nullptr));
        buttonImageProcessing->setText(QCoreApplication::translate("MBsoftwareClass", "ImageProcessing Dialog", nullptr));
        buttonDMStransitionTest->setText(QCoreApplication::translate("MBsoftwareClass", "DMS TEST", nullptr));
        buttonDMS->setText(QCoreApplication::translate("MBsoftwareClass", "DMS", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabImageProcessing), QCoreApplication::translate("MBsoftwareClass", "ImageProcessing", nullptr));
        buttonStatusBox->setText(QCoreApplication::translate("MBsoftwareClass", "Status window", nullptr));
        buttonAboutUs->setText(QCoreApplication::translate("MBsoftwareClass", "About Us", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabStatistcs), QCoreApplication::translate("MBsoftwareClass", "Statistics && Status", nullptr));
        labelLogo->setText(QString());
        labelGoodProcent->setText(QString());
        label_3->setText(QCoreApplication::translate("MBsoftwareClass", "BAD:", nullptr));
        labelTotal->setText(QString());
        labelGood_4->setText(QString());
        labelBadProcent->setText(QString());
        label_10->setText(QCoreApplication::translate("MBsoftwareClass", "TOTAL:", nullptr));
        labelBAD_2->setText(QString());
        label_9->setText(QCoreApplication::translate("MBsoftwareClass", "GOOD:", nullptr));
        label_11->setText(QCoreApplication::translate("MBsoftwareClass", "VALVE OPEN:", nullptr));
        labelValveGood->setText(QString());
        labelGood_3->setText(QString());
        label_2->setText(QCoreApplication::translate("MBsoftwareClass", "COUNTERS:", nullptr));
        labelTest->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MBsoftwareClass", "Presort Control", nullptr));
        labelPresortDowel2->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelPresortDowel0->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        labelPresortDowel1->setText(QCoreApplication::translate("MBsoftwareClass", "TextLabel", nullptr));
        label->setText(QCoreApplication::translate("MBsoftwareClass", "DMS5: XXX", nullptr));
        label_12->setText(QCoreApplication::translate("MBsoftwareClass", "MODE:", nullptr));
        label_14->setText(QCoreApplication::translate("MBsoftwareClass", "DOWEL8x30", nullptr));
        label_16->setText(QCoreApplication::translate("MBsoftwareClass", "NO", nullptr));
        label_7->setText(QCoreApplication::translate("MBsoftwareClass", "Konus:", nullptr));
        label_6->setText(QCoreApplication::translate("MBsoftwareClass", "Type:", nullptr));
        label_13->setText(QCoreApplication::translate("MBsoftwareClass", "S0", nullptr));
        label_4->setText(QCoreApplication::translate("MBsoftwareClass", "Settings:", nullptr));
        label_39->setText(QCoreApplication::translate("MBsoftwareClass", "Width Max:", nullptr));
        label_25->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_27->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_19->setText(QCoreApplication::translate("MBsoftwareClass", "Lenght MAX:", nullptr));
        label_24->setText(QCoreApplication::translate("MBsoftwareClass", "29.00", nullptr));
        label_28->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_40->setText(QCoreApplication::translate("MBsoftwareClass", "Width Max:", nullptr));
        label_36->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_35->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_34->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_33->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_32->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_41->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_37->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_38->setText(QCoreApplication::translate("MBsoftwareClass", "Konus:", nullptr));
        label_18->setText(QCoreApplication::translate("MBsoftwareClass", "Tolerance:", nullptr));
        label_26->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_29->setText(QCoreApplication::translate("MBsoftwareClass", "Width:", nullptr));
        label_31->setText(QCoreApplication::translate("MBsoftwareClass", "Width Min:", nullptr));
        label_23->setText(QCoreApplication::translate("MBsoftwareClass", "31.00", nullptr));
        label_21->setText(QCoreApplication::translate("MBsoftwareClass", "Lenght:", nullptr));
        label_22->setText(QCoreApplication::translate("MBsoftwareClass", "Lenght Min:", nullptr));
        label_30->setText(QCoreApplication::translate("MBsoftwareClass", "Width Max:", nullptr));
        label_42->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_43->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_44->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_46->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_47->setText(QCoreApplication::translate("MBsoftwareClass", "mm", nullptr));
        label_8->setText(QCoreApplication::translate("MBsoftwareClass", "Bad:", nullptr));
        labelBAD->setText(QCoreApplication::translate("MBsoftwareClass", "11222222", nullptr));
        label_106->setText(QCoreApplication::translate("MBsoftwareClass", "11222222", nullptr));
        label_108->setText(QCoreApplication::translate("MBsoftwareClass", "112", nullptr));
        label_105->setText(QCoreApplication::translate("MBsoftwareClass", "Good:", nullptr));
        label_15->setText(QCoreApplication::translate("MBsoftwareClass", "Valve:", nullptr));
        label_109->setText(QCoreApplication::translate("MBsoftwareClass", "112", nullptr));
        labelGood->setText(QCoreApplication::translate("MBsoftwareClass", "11222222", nullptr));
        label_5->setText(QCoreApplication::translate("MBsoftwareClass", "Counters:", nullptr));
        label_111->setText(QCoreApplication::translate("MBsoftwareClass", "%", nullptr));
        label_113->setText(QCoreApplication::translate("MBsoftwareClass", "%", nullptr));
        label_76->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_78->setText(QCoreApplication::translate("MBsoftwareClass", "Bad/min", nullptr));
        label_90->setText(QCoreApplication::translate("MBsoftwareClass", "Timers:", nullptr));
        label_80->setText(QCoreApplication::translate("MBsoftwareClass", "N:", nullptr));
        label_94->setText(QCoreApplication::translate("MBsoftwareClass", "31.00", nullptr));
        label_79->setText(QCoreApplication::translate("MBsoftwareClass", "29.00", nullptr));
        label_96->setText(QCoreApplication::translate("MBsoftwareClass", "ALL/min", nullptr));
        label_91->setText(QCoreApplication::translate("MBsoftwareClass", "min AVG:", nullptr));
        label_95->setText(QCoreApplication::translate("MBsoftwareClass", "Good/min", nullptr));
        label_81->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_82->setText(QCoreApplication::translate("MBsoftwareClass", "30.00", nullptr));
        label_103->setText(QCoreApplication::translate("MBsoftwareClass", "Bad:", nullptr));
        labelBAD_3->setText(QCoreApplication::translate("MBsoftwareClass", "11222222", nullptr));
        label_110->setText(QCoreApplication::translate("MBsoftwareClass", "11222222", nullptr));
        label_114->setText(QCoreApplication::translate("MBsoftwareClass", "112", nullptr));
        label_115->setText(QCoreApplication::translate("MBsoftwareClass", "Good:", nullptr));
        label_104->setText(QCoreApplication::translate("MBsoftwareClass", "Valve:", nullptr));
        label_116->setText(QCoreApplication::translate("MBsoftwareClass", "112", nullptr));
        labelGood_2->setText(QCoreApplication::translate("MBsoftwareClass", "11222222", nullptr));
        label_117->setText(QCoreApplication::translate("MBsoftwareClass", "Counters:", nullptr));
        label_118->setText(QCoreApplication::translate("MBsoftwareClass", "%", nullptr));
        label_119->setText(QCoreApplication::translate("MBsoftwareClass", "%", nullptr));
        label_17->setText(QCoreApplication::translate("MBsoftwareClass", "Type:", nullptr));
        label_20->setText(QCoreApplication::translate("MBsoftwareClass", "Type:", nullptr));
        label_107->setText(QCoreApplication::translate("MBsoftwareClass", "Valve:", nullptr));
        label_112->setText(QCoreApplication::translate("MBsoftwareClass", "112", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MBsoftwareClass: public Ui_MBsoftwareClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MBSOFTWARE_H
