/********************************************************************************
** Form generated from reading UI file 'test.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEST_H
#define UI_TEST_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TypeSettings
{
public:
    QRadioButton *radioButton;
    QCheckBox *checkBox0;
    QGroupBox *groupBox0;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *group0Layout;
    QPushButton *pushButton;

    void setupUi(QWidget *TypeSettings)
    {
        if (TypeSettings->objectName().isEmpty())
            TypeSettings->setObjectName(QString::fromUtf8("TypeSettings"));
        TypeSettings->resize(1053, 653);
        radioButton = new QRadioButton(TypeSettings);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));
        radioButton->setGeometry(QRect(60, 120, 82, 17));
        checkBox0 = new QCheckBox(TypeSettings);
        checkBox0->setObjectName(QString::fromUtf8("checkBox0"));
        checkBox0->setGeometry(QRect(420, 70, 70, 17));
        groupBox0 = new QGroupBox(TypeSettings);
        groupBox0->setObjectName(QString::fromUtf8("groupBox0"));
        groupBox0->setGeometry(QRect(20, 210, 281, 421));
        verticalLayout_2 = new QVBoxLayout(groupBox0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        group0Layout = new QVBoxLayout();
        group0Layout->setObjectName(QString::fromUtf8("group0Layout"));

        verticalLayout_2->addLayout(group0Layout);

        pushButton = new QPushButton(TypeSettings);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(570, 180, 75, 23));

        retranslateUi(TypeSettings);

        QMetaObject::connectSlotsByName(TypeSettings);
    } // setupUi

    void retranslateUi(QWidget *TypeSettings)
    {
        TypeSettings->setWindowTitle(QCoreApplication::translate("TypeSettings", "Form", nullptr));
        radioButton->setText(QCoreApplication::translate("TypeSettings", "RadioButton", nullptr));
        checkBox0->setText(QCoreApplication::translate("TypeSettings", "CheckBox", nullptr));
        groupBox0->setTitle(QCoreApplication::translate("TypeSettings", "GroupBox", nullptr));
        pushButton->setText(QCoreApplication::translate("TypeSettings", "PushButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TypeSettings: public Ui_TypeSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEST_H
