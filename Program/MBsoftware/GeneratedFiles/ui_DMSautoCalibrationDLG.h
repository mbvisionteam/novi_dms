/********************************************************************************
** Form generated from reading UI file 'DMSautoCalibrationDLG.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DMSAUTOCALIBRATIONDLG_H
#define UI_DMSAUTOCALIBRATIONDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DMSautoCalibration
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *layoutMeasurements;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *buttonSave;
    QPushButton *buttonCancel;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_4;
    QLabel *label_6;
    QLabel *label_2;
    QLabel *label_8;
    QLabel *label;
    QLabel *label_5;
    QLabel *label_3;
    QLabel *label_9;
    QLabel *labelOldCorr1;
    QLabel *labelOldCorr2;
    QLabel *labelOldCorr3;
    QLabel *labelOldCorr4;
    QLabel *labelOldCorr5;
    QLabel *labelOldCorr0;
    QLabel *labelNewCorr1;
    QLabel *labelNewCorr3;
    QLabel *labelNewCorr4;
    QLabel *labelNewCorr5;
    QLabel *labelNewCorr0;
    QLabel *labelNewCorr2;

    void setupUi(QWidget *DMSautoCalibration)
    {
        if (DMSautoCalibration->objectName().isEmpty())
            DMSautoCalibration->setObjectName(QString::fromUtf8("DMSautoCalibration"));
        DMSautoCalibration->resize(581, 511);
        DMSautoCalibration->setFocusPolicy(Qt::StrongFocus);
        verticalLayoutWidget = new QWidget(DMSautoCalibration);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 60, 561, 241));
        layoutMeasurements = new QVBoxLayout(verticalLayoutWidget);
        layoutMeasurements->setObjectName(QString::fromUtf8("layoutMeasurements"));
        layoutMeasurements->setContentsMargins(0, 0, 0, 0);
        horizontalLayoutWidget = new QWidget(DMSautoCalibration);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(150, 440, 311, 52));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        buttonSave = new QPushButton(horizontalLayoutWidget);
        buttonSave->setObjectName(QString::fromUtf8("buttonSave"));
        buttonSave->setMinimumSize(QSize(0, 50));

        horizontalLayout->addWidget(buttonSave);

        buttonCancel = new QPushButton(horizontalLayoutWidget);
        buttonCancel->setObjectName(QString::fromUtf8("buttonCancel"));
        buttonCancel->setMinimumSize(QSize(0, 50));

        horizontalLayout->addWidget(buttonCancel);

        gridLayoutWidget = new QWidget(DMSautoCalibration);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 310, 561, 101));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font;
        font.setFamily(QString::fromUtf8("Times New Roman"));
        font.setPointSize(12);
        label_4->setFont(font);
        label_4->setFrameShape(QFrame::Box);
        label_4->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label_4, 0, 4, 1, 1);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font);
        label_6->setFrameShape(QFrame::Box);
        label_6->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label_6, 0, 6, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);
        label_2->setFrameShape(QFrame::Box);
        label_2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label_2, 0, 2, 1, 1);

        label_8 = new QLabel(gridLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font);
        label_8->setFrameShape(QFrame::Box);
        label_8->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label_8, 1, 0, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);
        label->setFrameShape(QFrame::Box);
        label->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label, 0, 1, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);
        label_5->setFrameShape(QFrame::Box);
        label_5->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label_5, 0, 5, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);
        label_3->setFrameShape(QFrame::Box);
        label_3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label_3, 0, 3, 1, 1);

        label_9 = new QLabel(gridLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font);
        label_9->setFrameShape(QFrame::Box);
        label_9->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(label_9, 2, 0, 1, 1);

        labelOldCorr1 = new QLabel(gridLayoutWidget);
        labelOldCorr1->setObjectName(QString::fromUtf8("labelOldCorr1"));
        labelOldCorr1->setFont(font);
        labelOldCorr1->setFrameShape(QFrame::Box);
        labelOldCorr1->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelOldCorr1, 1, 1, 1, 1);

        labelOldCorr2 = new QLabel(gridLayoutWidget);
        labelOldCorr2->setObjectName(QString::fromUtf8("labelOldCorr2"));
        labelOldCorr2->setFont(font);
        labelOldCorr2->setFrameShape(QFrame::Box);
        labelOldCorr2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelOldCorr2, 1, 2, 1, 1);

        labelOldCorr3 = new QLabel(gridLayoutWidget);
        labelOldCorr3->setObjectName(QString::fromUtf8("labelOldCorr3"));
        labelOldCorr3->setFont(font);
        labelOldCorr3->setFrameShape(QFrame::Box);
        labelOldCorr3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelOldCorr3, 1, 3, 1, 1);

        labelOldCorr4 = new QLabel(gridLayoutWidget);
        labelOldCorr4->setObjectName(QString::fromUtf8("labelOldCorr4"));
        labelOldCorr4->setFont(font);
        labelOldCorr4->setFrameShape(QFrame::Box);
        labelOldCorr4->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelOldCorr4, 1, 4, 1, 1);

        labelOldCorr5 = new QLabel(gridLayoutWidget);
        labelOldCorr5->setObjectName(QString::fromUtf8("labelOldCorr5"));
        labelOldCorr5->setFont(font);
        labelOldCorr5->setFrameShape(QFrame::Box);
        labelOldCorr5->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelOldCorr5, 1, 5, 1, 1);

        labelOldCorr0 = new QLabel(gridLayoutWidget);
        labelOldCorr0->setObjectName(QString::fromUtf8("labelOldCorr0"));
        labelOldCorr0->setFont(font);
        labelOldCorr0->setFrameShape(QFrame::Box);
        labelOldCorr0->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelOldCorr0, 1, 6, 1, 1);

        labelNewCorr1 = new QLabel(gridLayoutWidget);
        labelNewCorr1->setObjectName(QString::fromUtf8("labelNewCorr1"));
        labelNewCorr1->setFont(font);
        labelNewCorr1->setFrameShape(QFrame::Box);
        labelNewCorr1->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelNewCorr1, 2, 1, 1, 1);

        labelNewCorr3 = new QLabel(gridLayoutWidget);
        labelNewCorr3->setObjectName(QString::fromUtf8("labelNewCorr3"));
        labelNewCorr3->setFont(font);
        labelNewCorr3->setFrameShape(QFrame::Box);
        labelNewCorr3->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelNewCorr3, 2, 3, 1, 1);

        labelNewCorr4 = new QLabel(gridLayoutWidget);
        labelNewCorr4->setObjectName(QString::fromUtf8("labelNewCorr4"));
        labelNewCorr4->setFont(font);
        labelNewCorr4->setFrameShape(QFrame::Box);
        labelNewCorr4->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelNewCorr4, 2, 4, 1, 1);

        labelNewCorr5 = new QLabel(gridLayoutWidget);
        labelNewCorr5->setObjectName(QString::fromUtf8("labelNewCorr5"));
        labelNewCorr5->setFont(font);
        labelNewCorr5->setFrameShape(QFrame::Box);
        labelNewCorr5->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelNewCorr5, 2, 5, 1, 1);

        labelNewCorr0 = new QLabel(gridLayoutWidget);
        labelNewCorr0->setObjectName(QString::fromUtf8("labelNewCorr0"));
        labelNewCorr0->setFont(font);
        labelNewCorr0->setFrameShape(QFrame::Box);
        labelNewCorr0->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelNewCorr0, 2, 6, 1, 1);

        labelNewCorr2 = new QLabel(gridLayoutWidget);
        labelNewCorr2->setObjectName(QString::fromUtf8("labelNewCorr2"));
        labelNewCorr2->setFont(font);
        labelNewCorr2->setFrameShape(QFrame::Box);
        labelNewCorr2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(labelNewCorr2, 2, 2, 1, 1);


        retranslateUi(DMSautoCalibration);

        QMetaObject::connectSlotsByName(DMSautoCalibration);
    } // setupUi

    void retranslateUi(QWidget *DMSautoCalibration)
    {
        DMSautoCalibration->setWindowTitle(QCoreApplication::translate("DMSautoCalibration", "Form", nullptr));
        buttonSave->setText(QCoreApplication::translate("DMSautoCalibration", "SAVE", nullptr));
        buttonCancel->setText(QCoreApplication::translate("DMSautoCalibration", "Cancel", nullptr));
        label_4->setText(QCoreApplication::translate("DMSautoCalibration", "CAM4", nullptr));
        label_6->setText(QCoreApplication::translate("DMSautoCalibration", "CAM0", nullptr));
        label_2->setText(QCoreApplication::translate("DMSautoCalibration", "CAM2", nullptr));
        label_8->setText(QCoreApplication::translate("DMSautoCalibration", "Old Correction", nullptr));
        label->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        label_5->setText(QCoreApplication::translate("DMSautoCalibration", "CAM5", nullptr));
        label_3->setText(QCoreApplication::translate("DMSautoCalibration", "CAM3", nullptr));
        label_9->setText(QCoreApplication::translate("DMSautoCalibration", "New Correction", nullptr));
        labelOldCorr1->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelOldCorr2->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelOldCorr3->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelOldCorr4->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelOldCorr5->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelOldCorr0->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelNewCorr1->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelNewCorr3->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelNewCorr4->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelNewCorr5->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelNewCorr0->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
        labelNewCorr2->setText(QCoreApplication::translate("DMSautoCalibration", "CAM1", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DMSautoCalibration: public Ui_DMSautoCalibration {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DMSAUTOCALIBRATIONDLG_H
