/********************************************************************************
** Form generated from reading UI file 'DowelSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOWELSETTINGS_H
#define UI_DOWELSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_dowelSettings
{
public:
    QComboBox *comboBoxSelectType;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QGridLayout *gridLayout_2;
    QLabel *label_2;
    QDoubleSpinBox *spinBoxLenght;
    QLabel *label;
    QDoubleSpinBox *spinBoxLenghtMax;
    QSpacerItem *horizontalSpacer;
    QFrame *frame_2;
    QGridLayout *gridLayout_3;
    QLabel *label_6;
    QDoubleSpinBox *spinBoxWidthMax;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_4;
    QDoubleSpinBox *spinBoxWidth;
    QFrame *frame_3;
    QGridLayout *gridLayout_4;
    QDoubleSpinBox *spinBoxKonus;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_3;
    QDoubleSpinBox *spinBoxKonusMax;
    QLabel *label_9;
    QWidget *layoutWidget;
    QVBoxLayout *vboxLayout;
    QPushButton *buttonOk;
    QSpacerItem *spacerItem;
    QPushButton *buttonCancel;
    QGroupBox *groupBox_3;
    QTabWidget *tabWidget;
    QWidget *tabAdvTolerance;
    QGridLayout *gridLayout;
    QLabel *label_14;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_13;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_18;
    QLabel *label_16;
    QLineEdit *lineEdit10;
    QLineEdit *lineEdit14;
    QLineEdit *lineEdit11;
    QLineEdit *lineEdit12;
    QLineEdit *lineEdit13;
    QLineEdit *lineEdit15;
    QLineEdit *lineEdit16;
    QLabel *label_20;
    QLabel *label_21;
    QLabel *label_19;
    QWidget *tabErrorMeasuremnts;
    QGridLayout *gridLayout_5;
    QLabel *label_27;
    QLabel *label_22;
    QLineEdit *lineEdit20;
    QLabel *label_24;
    QLabel *label_25;
    QLineEdit *lineEdit23;
    QSpacerItem *horizontalSpacer_6;
    QLabel *label_23;
    QLineEdit *lineEdit21;
    QLineEdit *lineEdit22;
    QSpacerItem *verticalSpacer_2;
    QLineEdit *lineEdit24;
    QLabel *label_28;
    QLabel *label_29;
    QLabel *label_30;
    QLineEdit *lineEdit25;
    QLineEdit *lineEdit26;
    QLineEdit *lineEdit27;
    QLineEdit *lineEdit28;
    QLabel *label_26;
    QWidget *tabAdvToleranceSpiral;
    QGridLayout *gridLayout_7;
    QGridLayout *gridLayout_6;
    QLineEdit *lineEdit30;
    QSpacerItem *horizontalSpacer_7;
    QLabel *label_33;
    QLabel *label_31;
    QLineEdit *lineEdit33;
    QLabel *label_34;
    QLineEdit *lineEdit35;
    QLabel *label_36;
    QSpacerItem *verticalSpacer_3;
    QLabel *label_35;
    QLabel *label_37;
    QLineEdit *lineEdit31;
    QLineEdit *lineEdit36;
    QLabel *label_32;
    QLineEdit *lineEdit32;
    QLineEdit *lineEdit34;
    QSpacerItem *horizontalSpacer_8;
    QComboBox *comboBoxSelectAdvancedSettings;
    QLabel *label_11;
    QGroupBox *groupBox;
    QRadioButton *radioWithKonus;
    QRadioButton *radioWithoutKonus;
    QPushButton *buttonSaveAdvanced;
    QPushButton *buttonCreateNewAdvancedSettings;
    QPushButton *buttonDeleteAdvancedSetttings;
    QLabel *label_10;
    QPushButton *buttonCreateType;
    QPushButton *buttonButtonRemoveType;

    void setupUi(QWidget *dowelSettings)
    {
        if (dowelSettings->objectName().isEmpty())
            dowelSettings->setObjectName(QString::fromUtf8("dowelSettings"));
        dowelSettings->resize(1085, 772);
        dowelSettings->setFocusPolicy(Qt::StrongFocus);
        comboBoxSelectType = new QComboBox(dowelSettings);
        comboBoxSelectType->setObjectName(QString::fromUtf8("comboBoxSelectType"));
        comboBoxSelectType->setEnabled(true);
        comboBoxSelectType->setGeometry(QRect(470, 20, 311, 31));
        QFont font;
        font.setFamily(QString::fromUtf8("Times New Roman"));
        font.setPointSize(14);
        font.setBold(false);
        font.setWeight(50);
        comboBoxSelectType->setFont(font);
        comboBoxSelectType->setFocusPolicy(Qt::StrongFocus);
        comboBoxSelectType->setAcceptDrops(false);
        comboBoxSelectType->setAutoFillBackground(true);
        comboBoxSelectType->setEditable(false);
        comboBoxSelectType->setDuplicatesEnabled(false);
        comboBoxSelectType->setFrame(true);
        groupBox_2 = new QGroupBox(dowelSettings);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 10, 311, 291));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Times New Roman"));
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        groupBox_2->setFont(font1);
        verticalLayout = new QVBoxLayout(groupBox_2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame = new QFrame(groupBox_2);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::Panel);
        frame->setFrameShadow(QFrame::Sunken);
        gridLayout_2 = new QGridLayout(frame);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMaximumSize(QSize(70, 16777215));
        label_2->setFont(font);

        gridLayout_2->addWidget(label_2, 0, 4, 1, 1);

        spinBoxLenght = new QDoubleSpinBox(frame);
        spinBoxLenght->setObjectName(QString::fromUtf8("spinBoxLenght"));
        spinBoxLenght->setFont(font);

        gridLayout_2->addWidget(spinBoxLenght, 0, 2, 1, 1);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMaximumSize(QSize(70, 16777215));
        label->setFont(font);

        gridLayout_2->addWidget(label, 0, 1, 1, 1);

        spinBoxLenghtMax = new QDoubleSpinBox(frame);
        spinBoxLenghtMax->setObjectName(QString::fromUtf8("spinBoxLenghtMax"));
        spinBoxLenghtMax->setFont(font);
        spinBoxLenghtMax->setMaximum(12.000000000000000);
        spinBoxLenghtMax->setSingleStep(0.100000000000000);

        gridLayout_2->addWidget(spinBoxLenghtMax, 0, 5, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 6, 1, 1);


        verticalLayout->addWidget(frame);

        frame_2 = new QFrame(groupBox_2);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setFrameShape(QFrame::Panel);
        frame_2->setFrameShadow(QFrame::Sunken);
        gridLayout_3 = new QGridLayout(frame_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_6 = new QLabel(frame_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMaximumSize(QSize(70, 16777215));
        label_6->setFont(font);

        gridLayout_3->addWidget(label_6, 0, 2, 1, 1);

        spinBoxWidthMax = new QDoubleSpinBox(frame_2);
        spinBoxWidthMax->setObjectName(QString::fromUtf8("spinBoxWidthMax"));
        spinBoxWidthMax->setFont(font);
        spinBoxWidthMax->setMaximum(12.000000000000000);
        spinBoxWidthMax->setSingleStep(0.100000000000000);

        gridLayout_3->addWidget(spinBoxWidthMax, 0, 3, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_2, 0, 4, 1, 1);

        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMaximumSize(QSize(70, 16777215));
        label_4->setFont(font);

        gridLayout_3->addWidget(label_4, 0, 0, 1, 1);

        spinBoxWidth = new QDoubleSpinBox(frame_2);
        spinBoxWidth->setObjectName(QString::fromUtf8("spinBoxWidth"));
        spinBoxWidth->setFont(font);

        gridLayout_3->addWidget(spinBoxWidth, 0, 1, 1, 1);


        verticalLayout->addWidget(frame_2);

        frame_3 = new QFrame(groupBox_2);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setFrameShape(QFrame::Panel);
        frame_3->setFrameShadow(QFrame::Sunken);
        gridLayout_4 = new QGridLayout(frame_3);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        spinBoxKonus = new QDoubleSpinBox(frame_3);
        spinBoxKonus->setObjectName(QString::fromUtf8("spinBoxKonus"));
        spinBoxKonus->setFont(font);

        gridLayout_4->addWidget(spinBoxKonus, 0, 1, 1, 1);

        label_7 = new QLabel(frame_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMaximumSize(QSize(70, 16777215));
        label_7->setFont(font);

        gridLayout_4->addWidget(label_7, 0, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_3, 0, 4, 1, 1);

        spinBoxKonusMax = new QDoubleSpinBox(frame_3);
        spinBoxKonusMax->setObjectName(QString::fromUtf8("spinBoxKonusMax"));
        spinBoxKonusMax->setFont(font);
        spinBoxKonusMax->setMaximum(12.000000000000000);
        spinBoxKonusMax->setSingleStep(0.100000000000000);

        gridLayout_4->addWidget(spinBoxKonusMax, 0, 3, 1, 1);

        label_9 = new QLabel(frame_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMaximumSize(QSize(70, 16777215));
        label_9->setFont(font);

        gridLayout_4->addWidget(label_9, 0, 2, 1, 1);


        verticalLayout->addWidget(frame_3);

        layoutWidget = new QWidget(dowelSettings);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(790, 20, 281, 93));
        vboxLayout = new QVBoxLayout(layoutWidget);
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout->setContentsMargins(0, 0, 0, 0);
        buttonOk = new QPushButton(layoutWidget);
        buttonOk->setObjectName(QString::fromUtf8("buttonOk"));
        buttonOk->setMinimumSize(QSize(100, 50));
        buttonOk->setFont(font);

        vboxLayout->addWidget(buttonOk);

        spacerItem = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacerItem);

        buttonCancel = new QPushButton(layoutWidget);
        buttonCancel->setObjectName(QString::fromUtf8("buttonCancel"));
        buttonCancel->setFont(font);

        vboxLayout->addWidget(buttonCancel);

        groupBox_3 = new QGroupBox(dowelSettings);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 310, 1051, 411));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Calibri"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        groupBox_3->setFont(font2);
        tabWidget = new QTabWidget(groupBox_3);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(13, 24, 751, 271));
        tabWidget->setFont(font);
        tabWidget->setDocumentMode(true);
        tabWidget->setTabsClosable(false);
        tabWidget->setTabBarAutoHide(false);
        tabAdvTolerance = new QWidget();
        tabAdvTolerance->setObjectName(QString::fromUtf8("tabAdvTolerance"));
        gridLayout = new QGridLayout(tabAdvTolerance);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_14 = new QLabel(tabAdvTolerance);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(150, 30));

        gridLayout->addWidget(label_14, 1, 0, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_5, 0, 2, 1, 1);

        label_13 = new QLabel(tabAdvTolerance);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(150, 30));

        gridLayout->addWidget(label_13, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 5, 0, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_4, 0, 5, 1, 1);

        label_18 = new QLabel(tabAdvTolerance);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setMinimumSize(QSize(150, 30));

        gridLayout->addWidget(label_18, 3, 0, 1, 1);

        label_16 = new QLabel(tabAdvTolerance);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(150, 30));

        gridLayout->addWidget(label_16, 2, 0, 1, 1);

        lineEdit10 = new QLineEdit(tabAdvTolerance);
        lineEdit10->setObjectName(QString::fromUtf8("lineEdit10"));
        lineEdit10->setMinimumSize(QSize(0, 30));
        lineEdit10->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit10, 0, 1, 1, 1);

        lineEdit14 = new QLineEdit(tabAdvTolerance);
        lineEdit14->setObjectName(QString::fromUtf8("lineEdit14"));
        lineEdit14->setMinimumSize(QSize(0, 30));
        lineEdit14->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit14, 0, 4, 1, 1);

        lineEdit11 = new QLineEdit(tabAdvTolerance);
        lineEdit11->setObjectName(QString::fromUtf8("lineEdit11"));
        lineEdit11->setMinimumSize(QSize(0, 30));
        lineEdit11->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit11, 1, 1, 1, 1);

        lineEdit12 = new QLineEdit(tabAdvTolerance);
        lineEdit12->setObjectName(QString::fromUtf8("lineEdit12"));
        lineEdit12->setMinimumSize(QSize(0, 30));
        lineEdit12->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit12, 2, 1, 1, 1);

        lineEdit13 = new QLineEdit(tabAdvTolerance);
        lineEdit13->setObjectName(QString::fromUtf8("lineEdit13"));
        lineEdit13->setMinimumSize(QSize(0, 30));
        lineEdit13->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit13, 3, 1, 1, 1);

        lineEdit15 = new QLineEdit(tabAdvTolerance);
        lineEdit15->setObjectName(QString::fromUtf8("lineEdit15"));
        lineEdit15->setMinimumSize(QSize(0, 30));
        lineEdit15->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit15, 1, 4, 1, 1);

        lineEdit16 = new QLineEdit(tabAdvTolerance);
        lineEdit16->setObjectName(QString::fromUtf8("lineEdit16"));
        lineEdit16->setMinimumSize(QSize(0, 30));
        lineEdit16->setMaximumSize(QSize(80, 16777215));

        gridLayout->addWidget(lineEdit16, 2, 4, 1, 1);

        label_20 = new QLabel(tabAdvTolerance);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMinimumSize(QSize(150, 30));

        gridLayout->addWidget(label_20, 1, 3, 1, 1);

        label_21 = new QLabel(tabAdvTolerance);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(150, 30));

        gridLayout->addWidget(label_21, 2, 3, 1, 1);

        label_19 = new QLabel(tabAdvTolerance);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(150, 30));

        gridLayout->addWidget(label_19, 0, 3, 1, 1);

        tabWidget->addTab(tabAdvTolerance, QString());
        tabErrorMeasuremnts = new QWidget();
        tabErrorMeasuremnts->setObjectName(QString::fromUtf8("tabErrorMeasuremnts"));
        gridLayout_5 = new QGridLayout(tabErrorMeasuremnts);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_27 = new QLabel(tabErrorMeasuremnts);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_27, 0, 3, 1, 1);

        label_22 = new QLabel(tabErrorMeasuremnts);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_22, 0, 0, 1, 1);

        lineEdit20 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit20->setObjectName(QString::fromUtf8("lineEdit20"));
        lineEdit20->setMinimumSize(QSize(0, 30));
        lineEdit20->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit20, 0, 1, 1, 1);

        label_24 = new QLabel(tabErrorMeasuremnts);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_24, 3, 0, 1, 1);

        label_25 = new QLabel(tabErrorMeasuremnts);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_25, 4, 0, 1, 1);

        lineEdit23 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit23->setObjectName(QString::fromUtf8("lineEdit23"));
        lineEdit23->setMinimumSize(QSize(0, 30));
        lineEdit23->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit23, 4, 1, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_6, 0, 2, 1, 1);

        label_23 = new QLabel(tabErrorMeasuremnts);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_23, 1, 0, 1, 1);

        lineEdit21 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit21->setObjectName(QString::fromUtf8("lineEdit21"));
        lineEdit21->setMinimumSize(QSize(0, 30));
        lineEdit21->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit21, 1, 1, 1, 1);

        lineEdit22 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit22->setObjectName(QString::fromUtf8("lineEdit22"));
        lineEdit22->setMinimumSize(QSize(0, 30));
        lineEdit22->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit22, 3, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_5->addItem(verticalSpacer_2, 6, 1, 1, 1);

        lineEdit24 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit24->setObjectName(QString::fromUtf8("lineEdit24"));
        lineEdit24->setMinimumSize(QSize(0, 30));
        lineEdit24->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit24, 0, 4, 1, 1);

        label_28 = new QLabel(tabErrorMeasuremnts);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_28, 1, 3, 1, 1);

        label_29 = new QLabel(tabErrorMeasuremnts);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_29, 3, 3, 1, 1);

        label_30 = new QLabel(tabErrorMeasuremnts);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_30, 4, 3, 1, 1);

        lineEdit25 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit25->setObjectName(QString::fromUtf8("lineEdit25"));
        lineEdit25->setMinimumSize(QSize(0, 30));
        lineEdit25->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit25, 1, 4, 1, 1);

        lineEdit26 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit26->setObjectName(QString::fromUtf8("lineEdit26"));
        lineEdit26->setMinimumSize(QSize(0, 30));
        lineEdit26->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit26, 3, 4, 1, 1);

        lineEdit27 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit27->setObjectName(QString::fromUtf8("lineEdit27"));
        lineEdit27->setMinimumSize(QSize(0, 30));
        lineEdit27->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit27, 4, 4, 1, 1);

        lineEdit28 = new QLineEdit(tabErrorMeasuremnts);
        lineEdit28->setObjectName(QString::fromUtf8("lineEdit28"));
        lineEdit28->setMinimumSize(QSize(0, 30));
        lineEdit28->setMaximumSize(QSize(80, 16777215));

        gridLayout_5->addWidget(lineEdit28, 5, 4, 1, 1);

        label_26 = new QLabel(tabErrorMeasuremnts);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setMinimumSize(QSize(150, 30));

        gridLayout_5->addWidget(label_26, 5, 3, 1, 1);

        tabWidget->addTab(tabErrorMeasuremnts, QString());
        tabAdvToleranceSpiral = new QWidget();
        tabAdvToleranceSpiral->setObjectName(QString::fromUtf8("tabAdvToleranceSpiral"));
        gridLayout_7 = new QGridLayout(tabAdvToleranceSpiral);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_6 = new QGridLayout();
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        lineEdit30 = new QLineEdit(tabAdvToleranceSpiral);
        lineEdit30->setObjectName(QString::fromUtf8("lineEdit30"));
        lineEdit30->setMinimumSize(QSize(0, 30));
        lineEdit30->setMaximumSize(QSize(80, 16777215));

        gridLayout_6->addWidget(lineEdit30, 0, 1, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_7, 0, 2, 1, 1);

        label_33 = new QLabel(tabAdvToleranceSpiral);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setMinimumSize(QSize(150, 30));

        gridLayout_6->addWidget(label_33, 2, 0, 1, 1);

        label_31 = new QLabel(tabAdvToleranceSpiral);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setMinimumSize(QSize(150, 30));

        gridLayout_6->addWidget(label_31, 2, 3, 1, 1);

        lineEdit33 = new QLineEdit(tabAdvToleranceSpiral);
        lineEdit33->setObjectName(QString::fromUtf8("lineEdit33"));
        lineEdit33->setMinimumSize(QSize(0, 30));
        lineEdit33->setMaximumSize(QSize(80, 16777215));

        gridLayout_6->addWidget(lineEdit33, 3, 1, 1, 1);

        label_34 = new QLabel(tabAdvToleranceSpiral);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setMinimumSize(QSize(150, 30));

        gridLayout_6->addWidget(label_34, 0, 3, 1, 1);

        lineEdit35 = new QLineEdit(tabAdvToleranceSpiral);
        lineEdit35->setObjectName(QString::fromUtf8("lineEdit35"));
        lineEdit35->setMinimumSize(QSize(0, 30));
        lineEdit35->setMaximumSize(QSize(80, 16777215));

        gridLayout_6->addWidget(lineEdit35, 1, 4, 1, 1);

        label_36 = new QLabel(tabAdvToleranceSpiral);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        label_36->setMinimumSize(QSize(150, 30));

        gridLayout_6->addWidget(label_36, 3, 0, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer_3, 4, 0, 1, 1);

        label_35 = new QLabel(tabAdvToleranceSpiral);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setMinimumSize(QSize(150, 30));

        gridLayout_6->addWidget(label_35, 0, 0, 1, 1);

        label_37 = new QLabel(tabAdvToleranceSpiral);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setMinimumSize(QSize(150, 30));

        gridLayout_6->addWidget(label_37, 1, 0, 1, 1);

        lineEdit31 = new QLineEdit(tabAdvToleranceSpiral);
        lineEdit31->setObjectName(QString::fromUtf8("lineEdit31"));
        lineEdit31->setMinimumSize(QSize(0, 30));
        lineEdit31->setMaximumSize(QSize(80, 16777215));

        gridLayout_6->addWidget(lineEdit31, 1, 1, 1, 1);

        lineEdit36 = new QLineEdit(tabAdvToleranceSpiral);
        lineEdit36->setObjectName(QString::fromUtf8("lineEdit36"));
        lineEdit36->setMinimumSize(QSize(0, 30));
        lineEdit36->setMaximumSize(QSize(80, 16777215));

        gridLayout_6->addWidget(lineEdit36, 2, 4, 1, 1);

        label_32 = new QLabel(tabAdvToleranceSpiral);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setMinimumSize(QSize(150, 30));

        gridLayout_6->addWidget(label_32, 1, 3, 1, 1);

        lineEdit32 = new QLineEdit(tabAdvToleranceSpiral);
        lineEdit32->setObjectName(QString::fromUtf8("lineEdit32"));
        lineEdit32->setMinimumSize(QSize(0, 30));
        lineEdit32->setMaximumSize(QSize(80, 16777215));

        gridLayout_6->addWidget(lineEdit32, 2, 1, 1, 1);

        lineEdit34 = new QLineEdit(tabAdvToleranceSpiral);
        lineEdit34->setObjectName(QString::fromUtf8("lineEdit34"));
        lineEdit34->setMinimumSize(QSize(0, 30));
        lineEdit34->setMaximumSize(QSize(80, 16777215));

        gridLayout_6->addWidget(lineEdit34, 0, 4, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_8, 0, 5, 1, 1);


        gridLayout_7->addLayout(gridLayout_6, 0, 0, 1, 1);

        tabWidget->addTab(tabAdvToleranceSpiral, QString());
        comboBoxSelectAdvancedSettings = new QComboBox(groupBox_3);
        comboBoxSelectAdvancedSettings->setObjectName(QString::fromUtf8("comboBoxSelectAdvancedSettings"));
        comboBoxSelectAdvancedSettings->setEnabled(true);
        comboBoxSelectAdvancedSettings->setGeometry(QRect(770, 90, 221, 21));
        comboBoxSelectAdvancedSettings->setFont(font);
        comboBoxSelectAdvancedSettings->setFocusPolicy(Qt::StrongFocus);
        comboBoxSelectAdvancedSettings->setAcceptDrops(false);
        comboBoxSelectAdvancedSettings->setAutoFillBackground(true);
        comboBoxSelectAdvancedSettings->setEditable(false);
        comboBoxSelectAdvancedSettings->setDuplicatesEnabled(false);
        comboBoxSelectAdvancedSettings->setFrame(true);
        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(770, 60, 250, 26));
        label_11->setMinimumSize(QSize(250, 0));
        label_11->setMaximumSize(QSize(150, 16777215));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Calibri"));
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setWeight(75);
        label_11->setFont(font3);
        groupBox = new QGroupBox(groupBox_3);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(770, 220, 211, 101));
        groupBox->setFont(font);
        radioWithKonus = new QRadioButton(groupBox);
        radioWithKonus->setObjectName(QString::fromUtf8("radioWithKonus"));
        radioWithKonus->setGeometry(QRect(10, 30, 181, 20));
        radioWithKonus->setFont(font);
        radioWithoutKonus = new QRadioButton(groupBox);
        radioWithoutKonus->setObjectName(QString::fromUtf8("radioWithoutKonus"));
        radioWithoutKonus->setGeometry(QRect(10, 60, 191, 20));
        radioWithoutKonus->setFont(font);
        buttonSaveAdvanced = new QPushButton(groupBox_3);
        buttonSaveAdvanced->setObjectName(QString::fromUtf8("buttonSaveAdvanced"));
        buttonSaveAdvanced->setGeometry(QRect(768, 340, 261, 50));
        buttonSaveAdvanced->setMinimumSize(QSize(100, 50));
        buttonSaveAdvanced->setFont(font);
        buttonCreateNewAdvancedSettings = new QPushButton(groupBox_3);
        buttonCreateNewAdvancedSettings->setObjectName(QString::fromUtf8("buttonCreateNewAdvancedSettings"));
        buttonCreateNewAdvancedSettings->setGeometry(QRect(770, 120, 221, 31));
        buttonCreateNewAdvancedSettings->setFont(font);
        buttonDeleteAdvancedSetttings = new QPushButton(groupBox_3);
        buttonDeleteAdvancedSetttings->setObjectName(QString::fromUtf8("buttonDeleteAdvancedSetttings"));
        buttonDeleteAdvancedSetttings->setGeometry(QRect(770, 150, 221, 31));
        buttonDeleteAdvancedSetttings->setFont(font);
        label_10 = new QLabel(dowelSettings);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(340, 20, 150, 26));
        label_10->setMinimumSize(QSize(150, 0));
        label_10->setMaximumSize(QSize(150, 16777215));
        label_10->setFont(font);
        buttonCreateType = new QPushButton(dowelSettings);
        buttonCreateType->setObjectName(QString::fromUtf8("buttonCreateType"));
        buttonCreateType->setGeometry(QRect(870, 250, 201, 31));
        buttonCreateType->setMinimumSize(QSize(100, 20));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Times New Roman"));
        font4.setPointSize(14);
        buttonCreateType->setFont(font4);
        buttonButtonRemoveType = new QPushButton(dowelSettings);
        buttonButtonRemoveType->setObjectName(QString::fromUtf8("buttonButtonRemoveType"));
        buttonButtonRemoveType->setGeometry(QRect(870, 280, 201, 31));
        buttonButtonRemoveType->setMinimumSize(QSize(100, 20));
        buttonButtonRemoveType->setFont(font4);

        retranslateUi(dowelSettings);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(dowelSettings);
    } // setupUi

    void retranslateUi(QWidget *dowelSettings)
    {
        dowelSettings->setWindowTitle(QCoreApplication::translate("dowelSettings", "Form", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("dowelSettings", "Tolerance", nullptr));
        label_2->setText(QCoreApplication::translate("dowelSettings", "+/-", nullptr));
        label->setText(QCoreApplication::translate("dowelSettings", "Lenght:", nullptr));
        label_6->setText(QCoreApplication::translate("dowelSettings", "+/-", nullptr));
        label_4->setText(QCoreApplication::translate("dowelSettings", "Width:", nullptr));
        label_7->setText(QCoreApplication::translate("dowelSettings", "Konus:", nullptr));
        label_9->setText(QCoreApplication::translate("dowelSettings", "+/-", nullptr));
        buttonOk->setText(QCoreApplication::translate("dowelSettings", "SELECT", nullptr));
        buttonCancel->setText(QCoreApplication::translate("dowelSettings", "Cancel", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("dowelSettings", "Advanced Settings", nullptr));
        label_14->setText(QCoreApplication::translate("dowelSettings", "MAX. ANGLE [\302\260]:", nullptr));
        label_13->setText(QCoreApplication::translate("dowelSettings", "MIN. ANGLE [\302\260]:", nullptr));
        label_18->setText(QCoreApplication::translate("dowelSettings", "MAX DELTA KONUS:", nullptr));
        label_16->setText(QCoreApplication::translate("dowelSettings", "MIN.DELTA KONUS:", nullptr));
        label_20->setText(QCoreApplication::translate("dowelSettings", "MAX ELIPSE:", nullptr));
        label_21->setText(QCoreApplication::translate("dowelSettings", "OFFSET DSK, DEK:", nullptr));
        label_19->setText(QCoreApplication::translate("dowelSettings", "MAX. ROUGHNESS:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabAdvTolerance), QCoreApplication::translate("dowelSettings", "Advanced tolerance Settings", nullptr));
        label_27->setText(QCoreApplication::translate("dowelSettings", "Max low Error  konus:", nullptr));
        label_22->setText(QCoreApplication::translate("dowelSettings", "Max NR.ERROR konus:", nullptr));
        label_24->setText(QCoreApplication::translate("dowelSettings", "Max.NR.ERROR width", nullptr));
        label_25->setText(QCoreApplication::translate("dowelSettings", "Max.NR.ERROR lenght", nullptr));
        label_23->setText(QCoreApplication::translate("dowelSettings", "Max. NR.ERROR konus EDGE:", nullptr));
        label_28->setText(QCoreApplication::translate("dowelSettings", "Max low Error  konus EDGE:", nullptr));
        label_29->setText(QCoreApplication::translate("dowelSettings", "Max low Error  width:", nullptr));
        label_30->setText(QCoreApplication::translate("dowelSettings", "Max low Error  lenght:", nullptr));
        label_26->setText(QCoreApplication::translate("dowelSettings", "Delta ERROR[mm]:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabErrorMeasuremnts), QCoreApplication::translate("dowelSettings", "Error Measurements Settings", nullptr));
        label_33->setText(QCoreApplication::translate("dowelSettings", "MIN.DELTA KONUS:", nullptr));
        label_31->setText(QCoreApplication::translate("dowelSettings", "OFFSET DSK, DEK:", nullptr));
        label_34->setText(QCoreApplication::translate("dowelSettings", "MAX. ROUGHNESS:", nullptr));
        label_36->setText(QCoreApplication::translate("dowelSettings", "MAX DELTA KONUS:", nullptr));
        label_35->setText(QCoreApplication::translate("dowelSettings", "MIN. ANGLE [\302\260]:", nullptr));
        label_37->setText(QCoreApplication::translate("dowelSettings", "MAX. ANGLE [\302\260]:", nullptr));
        label_32->setText(QCoreApplication::translate("dowelSettings", "MAX ELIPSE:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabAdvToleranceSpiral), QCoreApplication::translate("dowelSettings", "Advanced tolerance Settings Spiral", nullptr));
        label_11->setText(QCoreApplication::translate("dowelSettings", "Selected Advanced Settings:", nullptr));
        groupBox->setTitle(QCoreApplication::translate("dowelSettings", "Dowel Type", nullptr));
        radioWithKonus->setText(QCoreApplication::translate("dowelSettings", "Dowel with konus", nullptr));
        radioWithoutKonus->setText(QCoreApplication::translate("dowelSettings", "Dowel without konus", nullptr));
        buttonSaveAdvanced->setText(QCoreApplication::translate("dowelSettings", "SAVE Advanced Settings", nullptr));
        buttonCreateNewAdvancedSettings->setText(QCoreApplication::translate("dowelSettings", "Create new Advanced Settings", nullptr));
        buttonDeleteAdvancedSetttings->setText(QCoreApplication::translate("dowelSettings", "Delete Advanced Settings", nullptr));
        label_10->setText(QCoreApplication::translate("dowelSettings", "Selected Dowel:", nullptr));
        buttonCreateType->setText(QCoreApplication::translate("dowelSettings", "Create new Dowel", nullptr));
        buttonButtonRemoveType->setText(QCoreApplication::translate("dowelSettings", "Remove Type", nullptr));
    } // retranslateUi

};

namespace Ui {
    class dowelSettings: public Ui_dowelSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOWELSETTINGS_H
