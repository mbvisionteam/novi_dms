/********************************************************************************
** Form generated from reading UI file 'XilinxZynq.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_XILINXZYNQ_H
#define UI_XILINXZYNQ_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_XilinxForm
{
public:
    QGroupBox *readLowGroup;
    QVBoxLayout *verticalLayout_2;
    QScrollArea *scrollAreaReadReg;
    QWidget *scrollAreaWidgetContents;
    QPushButton *buttonSend;
    QLineEdit *lineSend;
    QGroupBox *readHighGroup;
    QVBoxLayout *verticalLayout;
    QScrollArea *scrollAreaWriteReg;
    QWidget *scrollAreaWidgetContents_2;
    QLabel *label_2;
    QTabWidget *tabWidget;
    QWidget *inputP1Tab;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayoutInput;
    QWidget *outputP2Tab;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayoutOutput;
    QWidget *tabP3;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayoutIOP3;
    QWidget *tabP8;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayoutIOP8;
    QWidget *tabP9;
    QWidget *verticalLayoutWidget_6;
    QVBoxLayout *verticalLayoutInputP9;
    QGroupBox *groupBox;
    QFrame *frame;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *layoutLight;
    QFrame *frame_2;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *layoutCamera;
    QGroupBox *groupBox_2;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *LabelText;
    QLabel *labelConnected;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLabel *labelReadSpeed;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QLabel *labelcurrVersion;

    void setupUi(QWidget *XilinxForm)
    {
        if (XilinxForm->objectName().isEmpty())
            XilinxForm->setObjectName(QString::fromUtf8("XilinxForm"));
        XilinxForm->resize(964, 885);
        readLowGroup = new QGroupBox(XilinxForm);
        readLowGroup->setObjectName(QString::fromUtf8("readLowGroup"));
        readLowGroup->setGeometry(QRect(10, 20, 201, 681));
        verticalLayout_2 = new QVBoxLayout(readLowGroup);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        scrollAreaReadReg = new QScrollArea(readLowGroup);
        scrollAreaReadReg->setObjectName(QString::fromUtf8("scrollAreaReadReg"));
        scrollAreaReadReg->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 179, 646));
        scrollAreaReadReg->setWidget(scrollAreaWidgetContents);

        verticalLayout_2->addWidget(scrollAreaReadReg);

        buttonSend = new QPushButton(XilinxForm);
        buttonSend->setObjectName(QString::fromUtf8("buttonSend"));
        buttonSend->setGeometry(QRect(240, 830, 131, 31));
        buttonSend->setAutoDefault(false);
        lineSend = new QLineEdit(XilinxForm);
        lineSend->setObjectName(QString::fromUtf8("lineSend"));
        lineSend->setGeometry(QRect(10, 830, 221, 31));
        readHighGroup = new QGroupBox(XilinxForm);
        readHighGroup->setObjectName(QString::fromUtf8("readHighGroup"));
        readHighGroup->setGeometry(QRect(220, 30, 181, 681));
        verticalLayout = new QVBoxLayout(readHighGroup);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        scrollAreaWriteReg = new QScrollArea(readHighGroup);
        scrollAreaWriteReg->setObjectName(QString::fromUtf8("scrollAreaWriteReg"));
        scrollAreaWriteReg->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 159, 646));
        scrollAreaWriteReg->setWidget(scrollAreaWidgetContents_2);

        verticalLayout->addWidget(scrollAreaWriteReg);

        label_2 = new QLabel(XilinxForm);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 800, 221, 21));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        label_2->setFont(font);
        label_2->setFrameShape(QFrame::Box);
        label_2->setFrameShadow(QFrame::Sunken);
        tabWidget = new QTabWidget(XilinxForm);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(410, 190, 341, 341));
        inputP1Tab = new QWidget();
        inputP1Tab->setObjectName(QString::fromUtf8("inputP1Tab"));
        verticalLayoutWidget = new QWidget(inputP1Tab);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 241, 301));
        verticalLayoutInput = new QVBoxLayout(verticalLayoutWidget);
        verticalLayoutInput->setObjectName(QString::fromUtf8("verticalLayoutInput"));
        verticalLayoutInput->setContentsMargins(0, 0, 0, 0);
        tabWidget->addTab(inputP1Tab, QString());
        outputP2Tab = new QWidget();
        outputP2Tab->setObjectName(QString::fromUtf8("outputP2Tab"));
        verticalLayoutWidget_2 = new QWidget(outputP2Tab);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 10, 241, 301));
        verticalLayoutOutput = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayoutOutput->setObjectName(QString::fromUtf8("verticalLayoutOutput"));
        verticalLayoutOutput->setContentsMargins(0, 0, 0, 0);
        tabWidget->addTab(outputP2Tab, QString());
        tabP3 = new QWidget();
        tabP3->setObjectName(QString::fromUtf8("tabP3"));
        verticalLayoutWidget_4 = new QWidget(tabP3);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(10, 10, 241, 301));
        verticalLayoutIOP3 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayoutIOP3->setObjectName(QString::fromUtf8("verticalLayoutIOP3"));
        verticalLayoutIOP3->setContentsMargins(0, 0, 0, 0);
        tabWidget->addTab(tabP3, QString());
        tabP8 = new QWidget();
        tabP8->setObjectName(QString::fromUtf8("tabP8"));
        verticalLayoutWidget_5 = new QWidget(tabP8);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(10, 10, 251, 301));
        verticalLayoutIOP8 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayoutIOP8->setObjectName(QString::fromUtf8("verticalLayoutIOP8"));
        verticalLayoutIOP8->setContentsMargins(0, 0, 0, 0);
        tabWidget->addTab(tabP8, QString());
        tabP9 = new QWidget();
        tabP9->setObjectName(QString::fromUtf8("tabP9"));
        verticalLayoutWidget_6 = new QWidget(tabP9);
        verticalLayoutWidget_6->setObjectName(QString::fromUtf8("verticalLayoutWidget_6"));
        verticalLayoutWidget_6->setGeometry(QRect(10, 10, 251, 301));
        verticalLayoutInputP9 = new QVBoxLayout(verticalLayoutWidget_6);
        verticalLayoutInputP9->setObjectName(QString::fromUtf8("verticalLayoutInputP9"));
        verticalLayoutInputP9->setContentsMargins(0, 0, 0, 0);
        tabWidget->addTab(tabP9, QString());
        groupBox = new QGroupBox(XilinxForm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(410, 550, 521, 301));
        frame = new QFrame(groupBox);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(10, 30, 501, 81));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Sunken);
        horizontalLayoutWidget = new QWidget(frame);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 10, 481, 61));
        layoutLight = new QHBoxLayout(horizontalLayoutWidget);
        layoutLight->setObjectName(QString::fromUtf8("layoutLight"));
        layoutLight->setContentsMargins(0, 0, 0, 0);
        frame_2 = new QFrame(groupBox);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(10, 120, 501, 141));
        frame_2->setFrameShape(QFrame::Box);
        frame_2->setFrameShadow(QFrame::Sunken);
        horizontalLayoutWidget_2 = new QWidget(frame_2);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 10, 481, 121));
        layoutCamera = new QHBoxLayout(horizontalLayoutWidget_2);
        layoutCamera->setObjectName(QString::fromUtf8("layoutCamera"));
        layoutCamera->setContentsMargins(0, 0, 0, 0);
        groupBox_2 = new QGroupBox(XilinxForm);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(690, 30, 241, 141));
        verticalLayoutWidget_3 = new QWidget(groupBox_2);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(30, 20, 201, 111));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        LabelText = new QLabel(verticalLayoutWidget_3);
        LabelText->setObjectName(QString::fromUtf8("LabelText"));
        LabelText->setFrameShape(QFrame::NoFrame);
        LabelText->setFrameShadow(QFrame::Plain);

        horizontalLayout_2->addWidget(LabelText);

        labelConnected = new QLabel(verticalLayoutWidget_3);
        labelConnected->setObjectName(QString::fromUtf8("labelConnected"));
        labelConnected->setFrameShape(QFrame::Panel);
        labelConnected->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(labelConnected);


        verticalLayout_3->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(verticalLayoutWidget_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout->addWidget(label_3);

        labelReadSpeed = new QLabel(verticalLayoutWidget_3);
        labelReadSpeed->setObjectName(QString::fromUtf8("labelReadSpeed"));
        labelReadSpeed->setFrameShape(QFrame::Panel);
        labelReadSpeed->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(labelReadSpeed);


        verticalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_4 = new QLabel(verticalLayoutWidget_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMaximumSize(QSize(16772, 16777215));

        horizontalLayout_3->addWidget(label_4);

        labelcurrVersion = new QLabel(verticalLayoutWidget_3);
        labelcurrVersion->setObjectName(QString::fromUtf8("labelcurrVersion"));
        labelcurrVersion->setMaximumSize(QSize(16772, 16777215));
        labelcurrVersion->setFrameShape(QFrame::Panel);
        labelcurrVersion->setFrameShadow(QFrame::Sunken);

        horizontalLayout_3->addWidget(labelcurrVersion);


        verticalLayout_3->addLayout(horizontalLayout_3);


        retranslateUi(XilinxForm);

        tabWidget->setCurrentIndex(4);


        QMetaObject::connectSlotsByName(XilinxForm);
    } // setupUi

    void retranslateUi(QWidget *XilinxForm)
    {
        XilinxForm->setWindowTitle(QCoreApplication::translate("XilinxForm", "Form", nullptr));
        readLowGroup->setTitle(QCoreApplication::translate("XilinxForm", "Read Registers 0-63", nullptr));
        buttonSend->setText(QCoreApplication::translate("XilinxForm", "Send Command", nullptr));
        readHighGroup->setTitle(QCoreApplication::translate("XilinxForm", "Write Registers 0-63", nullptr));
        label_2->setText(QCoreApplication::translate("XilinxForm", "Example: R[0-63]-[REG_VALUE];", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(inputP1Tab), QCoreApplication::translate("XilinxForm", "INPUT P1", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(outputP2Tab), QCoreApplication::translate("XilinxForm", "OUTPUT P2", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabP3), QCoreApplication::translate("XilinxForm", "I/O P3", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabP8), QCoreApplication::translate("XilinxForm", "I/O P8", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabP9), QCoreApplication::translate("XilinxForm", "INPUT P9", nullptr));
        groupBox->setTitle(QCoreApplication::translate("XilinxForm", "Camera && Light ", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("XilinxForm", "Status:", nullptr));
        LabelText->setText(QCoreApplication::translate("XilinxForm", "Connection Status:", nullptr));
        labelConnected->setText(QCoreApplication::translate("XilinxForm", "TextLabel", nullptr));
        label_3->setText(QCoreApplication::translate("XilinxForm", "Recive Speed:", nullptr));
        labelReadSpeed->setText(QCoreApplication::translate("XilinxForm", "TextLabel", nullptr));
        label_4->setText(QCoreApplication::translate("XilinxForm", "Version:", nullptr));
        labelcurrVersion->setText(QCoreApplication::translate("XilinxForm", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class XilinxForm: public Ui_XilinxForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_XILINXZYNQ_H
