/********************************************************************************
** Form generated from reading UI file 'MBsoftware1.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MBSOFTWARE1_H
#define UI_MBSOFTWARE1_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MBsoftwareClass1
{
public:
    QAction *buttonAboutUs;
    QAction *buttonDMS;
    QAction *buttonDMStransitionTest;
    QAction *buttonLogin;
    QAction *buttonImageProcessing;
    QAction *buttonResetCounters;
    QAction *buttonStatusBox;
    QAction *buttonDowelSelect;
    QAction *buttonCalibrationSettings;
    QAction *buttonPresortSettings;
    QAction *buttonGeneralSettings;
    QAction *buttonResetAll;
    QAction *actionLPT_0;
    QAction *actionLPT_1;
    QAction *actionArea_Camera_0;
    QAction *actionArea_Camera_1;
    QWidget *centralwidget;
    QFrame *frameMeasurements;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *layoutMeasurement;
    QGraphicsView *imageViewMainWindow_0;
    QFrame *frameStatus;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout;
    QLabel *labelTimeAndDate;
    QFrame *frameDeviceStatus;
    QLabel *labelDeviceStatus;
    QListWidget *listWidgetStatus;
    QLabel *labelLogin;
    QLabel *labelStatusNaprave;
    QLabel *labelDMSStatus;
    QLabel *labelRotator;
    QLabel *labelElevator;
    QLabel *labelRotatorSpeed;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_3;
    QFrame *frame_7;
    QGridLayout *gridLayout_8;
    QLabel *labelGoodProc;
    QLabel *label_105;
    QLabel *labelGood;
    QLabel *label_15;
    QLabel *labelBad;
    QLabel *labelValve;
    QLabel *label_8;
    QLabel *labelBadProc;
    QSpacerItem *horizontalSpacer;
    QFrame *frame_6;
    QGridLayout *gridLayout_6;
    QLabel *labelAllMin;
    QLabel *labelGoodMin;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_78;
    QLabel *label_95;
    QSpacerItem *horizontalSpacer_3;
    QLabel *labelBadMin;
    QLabel *label_96;
    QLabel *label_97;
    QLabel *labelAllHour;
    QFrame *frame_8;
    QGridLayout *gridLayout_9;
    QLabel *labelInBOX;
    QLabel *label_99;
    QLabel *label_98;
    QLabel *label_100;
    QLabel *labelInBoxMax;
    QLabel *labelNrBox;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_4;
    QFrame *frame_5;
    QHBoxLayout *horizontalLayout_2;
    QGridLayout *gridLayout_2;
    QLabel *LabelKonusTol;
    QLabel *label_38;
    QLabel *LabelLenght;
    QLabel *labelKonus;
    QLabel *label_44;
    QLabel *labelWidth;
    QLabel *label_43;
    QLabel *LabelLenghtTol;
    QLabel *label_30;
    QLabel *LabelWidthTol;
    QLabel *label_31;
    QLabel *label_39;
    QFrame *frame_4;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout;
    QLabel *labelKonusOn;
    QLabel *label_21;
    QLabel *label_16;
    QLabel *labelWorkingMode;
    QLabel *labelAdvanceSettings;
    QLabel *label_7;
    QLabel *label_19;
    QLabel *labelSelectedType;
    QFrame *frame_9;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout_3;
    QLabel *labelwWorkingmodePresort;
    QLabel *label_23;
    QLabel *label_22;
    QLabel *labelWorkingModePresortSensor;
    QLabel *label_26;
    QLabel *labelPresortSettings;
    QTabWidget *tabWidget;
    QWidget *tabStatistics;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_2;
    QCustomPlot *plotLenght;
    QCustomPlot *plotWidth;
    QCustomPlot *plotWidthMax;
    QCustomPlot *plotWidthMin;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QCustomPlot *plotGauss1;
    QCustomPlot *plotGauss2;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_10;
    QGridLayout *gridLayout_7;
    QLabel *label_5;
    QLabel *label_10;
    QLabel *label_6;
    QLabel *label_14;
    QLabel *label;
    QLabel *labelAlarm2;
    QLabel *label_11;
    QLabel *labelWidthStatisticsGood;
    QLabel *labelAlarm3;
    QLabel *label_3;
    QLabel *labelElipseStatistics;
    QLabel *labelLenghtStatisticsGood;
    QLabel *labelLenghtStatistics;
    QLabel *label_12;
    QLabel *labelWidthStatistics;
    QLabel *labelAlarm1;
    QLabel *label_27;
    QLabel *labelAlarm4;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_11;
    QGridLayout *gridLayout_13;
    QLabel *label_13;
    QLabel *labelBadStat_3;
    QLabel *labelBadStat_4;
    QLabel *labelBadStat_6;
    QLabel *labelBadStat_8;
    QLabel *labelBadStat_5;
    QLabel *labelBadStat_7;
    QLabel *label_2;
    QLabel *labelBadStat;
    QLabel *label_9;
    QLabel *label_18;
    QLabel *label_4;
    QLabel *label_17;
    QLabel *label_20;
    QLabel *labelBadStat_2;
    QLabel *label_24;
    QWidget *tabService;
    QListWidget *statusList;
    QGroupBox *groupBox_5;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_11;
    QCheckBox *checkInput0;
    QLabel *labelCheckInput0;
    QHBoxLayout *horizontalLayout_12;
    QCheckBox *checkInput1;
    QLabel *labelCheckInput1;
    QHBoxLayout *horizontalLayout_13;
    QCheckBox *checkInput2;
    QLabel *labelCheckInput2;
    QHBoxLayout *horizontalLayout_14;
    QCheckBox *checkInput3;
    QLabel *labelCheckInput3;
    QHBoxLayout *horizontalLayout_15;
    QCheckBox *checkInput4;
    QLabel *labelCheckInput4;
    QHBoxLayout *horizontalLayout_16;
    QCheckBox *checkInput5;
    QLabel *labelCheckInput5;
    QHBoxLayout *horizontalLayout_17;
    QCheckBox *checkInput6;
    QLabel *labelCheckInput6;
    QHBoxLayout *horizontalLayout_18;
    QCheckBox *checkInput7;
    QLabel *labelCheckInput7;
    QGroupBox *groupBox_6;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *checkOutput0;
    QCheckBox *checkOutput1;
    QCheckBox *checkOutput2;
    QCheckBox *checkOutput3;
    QCheckBox *checkOutput4;
    QCheckBox *checkOutput5;
    QCheckBox *checkOutput6;
    QCheckBox *checkOutput7;
    QCheckBox *checkOutput8;
    QCheckBox *checkOutput9;
    QCheckBox *checkOutput10;
    QCheckBox *checkOutput11;
    QCheckBox *checkOutput12;
    QCheckBox *checkOutput13;
    QCheckBox *checkOutput14;
    QCheckBox *checkOutput15;
    QGroupBox *groupBox_7;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_28;
    QLabel *labelTrans0_0;
    QLabel *labelTrans0_1;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_32;
    QLabel *labelTrans1_0;
    QLabel *labelTrans1_1;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_34;
    QLabel *labelTrans2_0;
    QLabel *labelTrans2_1;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_36;
    QLabel *labelTrans3_0;
    QLabel *labelTrans3_1;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_40;
    QLabel *labelTrans4_0;
    QLabel *labelTrans4_1;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_42;
    QLabel *labelTrans5_0;
    QLabel *labelTrans5_1;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_5;
    QFrame *FrameStatus;
    QGridLayout *gridLayout_12;
    QLabel *labelStatusRect11;
    QLabel *labelStatusRect;
    QFrame *frame_10;
    QHBoxLayout *horizontalLayout_9;
    QFrame *frame_11;
    QLabel *labelLogo;
    QLabel *label_25;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout_14;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_7;
    QGraphicsView *imageViewPresort;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_6;
    QGraphicsView *imageViewPresortLive;
    QGridLayout *gridLayout_15;
    QSpacerItem *horizontalSpacer_4;
    QLabel *labelBadpresortColor;
    QLabel *labelLenghtStatistics_2;
    QLabel *labelLenghtStatistics_4;
    QLabel *labelBadpresortSmall;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *spacer;
    QLabel *labelLenghtStatistics_7;
    QLabel *labelBadpresortSensor;
    QMenuBar *menubar;
    QMenu *menuAboutUs;
    QMenu *menuSignals;
    QMenu *menuLogin;
    QMenu *menuTypes;
    QMenu *menuSettings;
    QMenu *menuHistory;

    void setupUi(QMainWindow *MBsoftwareClass1)
    {
        if (MBsoftwareClass1->objectName().isEmpty())
            MBsoftwareClass1->setObjectName(QString::fromUtf8("MBsoftwareClass1"));
        MBsoftwareClass1->resize(1920, 1066);
        MBsoftwareClass1->setMinimumSize(QSize(1920, 108));
        MBsoftwareClass1->setMaximumSize(QSize(1920, 16777215));
        QFont font;
        font.setFamily(QString::fromUtf8("Times New Roman"));
        font.setPointSize(18);
        MBsoftwareClass1->setFont(font);
        MBsoftwareClass1->setFocusPolicy(Qt::NoFocus);
        MBsoftwareClass1->setAcceptDrops(true);
        buttonAboutUs = new QAction(MBsoftwareClass1);
        buttonAboutUs->setObjectName(QString::fromUtf8("buttonAboutUs"));
        buttonDMS = new QAction(MBsoftwareClass1);
        buttonDMS->setObjectName(QString::fromUtf8("buttonDMS"));
        buttonDMStransitionTest = new QAction(MBsoftwareClass1);
        buttonDMStransitionTest->setObjectName(QString::fromUtf8("buttonDMStransitionTest"));
        buttonLogin = new QAction(MBsoftwareClass1);
        buttonLogin->setObjectName(QString::fromUtf8("buttonLogin"));
        buttonImageProcessing = new QAction(MBsoftwareClass1);
        buttonImageProcessing->setObjectName(QString::fromUtf8("buttonImageProcessing"));
        buttonImageProcessing->setEnabled(false);
        buttonImageProcessing->setVisible(false);
        buttonImageProcessing->setIconVisibleInMenu(true);
        buttonResetCounters = new QAction(MBsoftwareClass1);
        buttonResetCounters->setObjectName(QString::fromUtf8("buttonResetCounters"));
        buttonStatusBox = new QAction(MBsoftwareClass1);
        buttonStatusBox->setObjectName(QString::fromUtf8("buttonStatusBox"));
        buttonDowelSelect = new QAction(MBsoftwareClass1);
        buttonDowelSelect->setObjectName(QString::fromUtf8("buttonDowelSelect"));
        buttonCalibrationSettings = new QAction(MBsoftwareClass1);
        buttonCalibrationSettings->setObjectName(QString::fromUtf8("buttonCalibrationSettings"));
        buttonPresortSettings = new QAction(MBsoftwareClass1);
        buttonPresortSettings->setObjectName(QString::fromUtf8("buttonPresortSettings"));
        buttonGeneralSettings = new QAction(MBsoftwareClass1);
        buttonGeneralSettings->setObjectName(QString::fromUtf8("buttonGeneralSettings"));
        buttonResetAll = new QAction(MBsoftwareClass1);
        buttonResetAll->setObjectName(QString::fromUtf8("buttonResetAll"));
        actionLPT_0 = new QAction(MBsoftwareClass1);
        actionLPT_0->setObjectName(QString::fromUtf8("actionLPT_0"));
        actionLPT_1 = new QAction(MBsoftwareClass1);
        actionLPT_1->setObjectName(QString::fromUtf8("actionLPT_1"));
        actionArea_Camera_0 = new QAction(MBsoftwareClass1);
        actionArea_Camera_0->setObjectName(QString::fromUtf8("actionArea_Camera_0"));
        actionArea_Camera_1 = new QAction(MBsoftwareClass1);
        actionArea_Camera_1->setObjectName(QString::fromUtf8("actionArea_Camera_1"));
        centralwidget = new QWidget(MBsoftwareClass1);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        frameMeasurements = new QFrame(centralwidget);
        frameMeasurements->setObjectName(QString::fromUtf8("frameMeasurements"));
        frameMeasurements->setGeometry(QRect(10, 140, 641, 571));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frameMeasurements->sizePolicy().hasHeightForWidth());
        frameMeasurements->setSizePolicy(sizePolicy);
        frameMeasurements->setMinimumSize(QSize(400, 100));
        frameMeasurements->setMaximumSize(QSize(1000, 600));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        frameMeasurements->setFont(font1);
        frameMeasurements->setFrameShape(QFrame::Box);
        frameMeasurements->setFrameShadow(QFrame::Raised);
        verticalLayoutWidget = new QWidget(frameMeasurements);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 621, 551));
        layoutMeasurement = new QVBoxLayout(verticalLayoutWidget);
        layoutMeasurement->setObjectName(QString::fromUtf8("layoutMeasurement"));
        layoutMeasurement->setContentsMargins(0, 0, 0, 0);
        imageViewMainWindow_0 = new QGraphicsView(centralwidget);
        imageViewMainWindow_0->setObjectName(QString::fromUtf8("imageViewMainWindow_0"));
        imageViewMainWindow_0->setGeometry(QRect(10, 720, 641, 311));
        imageViewMainWindow_0->setMinimumSize(QSize(0, 0));
        imageViewMainWindow_0->setFrameShape(QFrame::WinPanel);
        imageViewMainWindow_0->setFrameShadow(QFrame::Sunken);
        frameStatus = new QFrame(centralwidget);
        frameStatus->setObjectName(QString::fromUtf8("frameStatus"));
        frameStatus->setGeometry(QRect(1610, 140, 300, 571));
        frameStatus->setMinimumSize(QSize(300, 0));
        frameStatus->setMaximumSize(QSize(300, 16777215));
        frameStatus->setFrameShape(QFrame::Box);
        frameStatus->setFrameShadow(QFrame::Raised);
        frameStatus->setLineWidth(4);
        verticalLayoutWidget_2 = new QWidget(frameStatus);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 10, 281, 551));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        labelTimeAndDate = new QLabel(verticalLayoutWidget_2);
        labelTimeAndDate->setObjectName(QString::fromUtf8("labelTimeAndDate"));
        labelTimeAndDate->setMinimumSize(QSize(200, 35));
        labelTimeAndDate->setMaximumSize(QSize(16777215, 43));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Times New Roman"));
        font2.setPointSize(16);
        font2.setBold(false);
        font2.setWeight(50);
        labelTimeAndDate->setFont(font2);
        labelTimeAndDate->setStyleSheet(QString::fromUtf8(""));
        labelTimeAndDate->setFrameShape(QFrame::Panel);
        labelTimeAndDate->setFrameShadow(QFrame::Sunken);
        labelTimeAndDate->setLineWidth(3);
        labelTimeAndDate->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(labelTimeAndDate);

        frameDeviceStatus = new QFrame(verticalLayoutWidget_2);
        frameDeviceStatus->setObjectName(QString::fromUtf8("frameDeviceStatus"));
        frameDeviceStatus->setMinimumSize(QSize(0, 250));
        frameDeviceStatus->setMaximumSize(QSize(16777215, 250));
        frameDeviceStatus->setFrameShape(QFrame::StyledPanel);
        frameDeviceStatus->setFrameShadow(QFrame::Plain);
        frameDeviceStatus->setLineWidth(1);
        labelDeviceStatus = new QLabel(frameDeviceStatus);
        labelDeviceStatus->setObjectName(QString::fromUtf8("labelDeviceStatus"));
        labelDeviceStatus->setGeometry(QRect(10, 10, 201, 20));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Times New Roman"));
        font3.setPointSize(16);
        font3.setBold(true);
        font3.setWeight(75);
        labelDeviceStatus->setFont(font3);
        listWidgetStatus = new QListWidget(frameDeviceStatus);
        listWidgetStatus->setObjectName(QString::fromUtf8("listWidgetStatus"));
        listWidgetStatus->setGeometry(QRect(10, 30, 261, 211));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Times New Roman"));
        font4.setPointSize(11);
        font4.setBold(false);
        font4.setWeight(50);
        listWidgetStatus->setFont(font4);

        verticalLayout->addWidget(frameDeviceStatus);

        labelLogin = new QLabel(verticalLayoutWidget_2);
        labelLogin->setObjectName(QString::fromUtf8("labelLogin"));
        labelLogin->setMinimumSize(QSize(200, 35));
        labelLogin->setMaximumSize(QSize(16777215, 43));
        labelLogin->setFont(font3);
        labelLogin->setStyleSheet(QString::fromUtf8(""));
        labelLogin->setFrameShape(QFrame::Panel);
        labelLogin->setFrameShadow(QFrame::Sunken);
        labelLogin->setLineWidth(3);

        verticalLayout->addWidget(labelLogin);

        labelStatusNaprave = new QLabel(verticalLayoutWidget_2);
        labelStatusNaprave->setObjectName(QString::fromUtf8("labelStatusNaprave"));
        labelStatusNaprave->setMinimumSize(QSize(200, 35));
        labelStatusNaprave->setMaximumSize(QSize(16777215, 43));
        labelStatusNaprave->setFont(font3);
        labelStatusNaprave->setStyleSheet(QString::fromUtf8(""));
        labelStatusNaprave->setFrameShape(QFrame::Panel);
        labelStatusNaprave->setFrameShadow(QFrame::Sunken);
        labelStatusNaprave->setLineWidth(3);

        verticalLayout->addWidget(labelStatusNaprave);

        labelDMSStatus = new QLabel(verticalLayoutWidget_2);
        labelDMSStatus->setObjectName(QString::fromUtf8("labelDMSStatus"));
        labelDMSStatus->setMinimumSize(QSize(200, 35));
        labelDMSStatus->setMaximumSize(QSize(16777215, 43));
        labelDMSStatus->setFont(font3);
        labelDMSStatus->setStyleSheet(QString::fromUtf8(""));
        labelDMSStatus->setFrameShape(QFrame::Panel);
        labelDMSStatus->setFrameShadow(QFrame::Sunken);
        labelDMSStatus->setLineWidth(3);

        verticalLayout->addWidget(labelDMSStatus);

        labelRotator = new QLabel(verticalLayoutWidget_2);
        labelRotator->setObjectName(QString::fromUtf8("labelRotator"));
        labelRotator->setMinimumSize(QSize(200, 35));
        labelRotator->setMaximumSize(QSize(16777215, 43));
        labelRotator->setFont(font3);
        labelRotator->setStyleSheet(QString::fromUtf8(""));
        labelRotator->setFrameShape(QFrame::Panel);
        labelRotator->setFrameShadow(QFrame::Sunken);
        labelRotator->setLineWidth(3);

        verticalLayout->addWidget(labelRotator);

        labelElevator = new QLabel(verticalLayoutWidget_2);
        labelElevator->setObjectName(QString::fromUtf8("labelElevator"));
        labelElevator->setMinimumSize(QSize(200, 35));
        labelElevator->setMaximumSize(QSize(16777215, 43));
        labelElevator->setFont(font3);
        labelElevator->setStyleSheet(QString::fromUtf8(""));
        labelElevator->setFrameShape(QFrame::Panel);
        labelElevator->setFrameShadow(QFrame::Sunken);
        labelElevator->setLineWidth(3);

        verticalLayout->addWidget(labelElevator);

        labelRotatorSpeed = new QLabel(verticalLayoutWidget_2);
        labelRotatorSpeed->setObjectName(QString::fromUtf8("labelRotatorSpeed"));
        labelRotatorSpeed->setMinimumSize(QSize(200, 35));
        labelRotatorSpeed->setMaximumSize(QSize(16777215, 43));
        labelRotatorSpeed->setFont(font3);
        labelRotatorSpeed->setStyleSheet(QString::fromUtf8(""));
        labelRotatorSpeed->setFrameShape(QFrame::Panel);
        labelRotatorSpeed->setFrameShadow(QFrame::Sunken);
        labelRotatorSpeed->setLineWidth(3);

        verticalLayout->addWidget(labelRotatorSpeed);

        frame_3 = new QFrame(centralwidget);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setGeometry(QRect(650, 140, 951, 151));
        frame_3->setFont(font);
        frame_3->setStyleSheet(QString::fromUtf8("\n"
"/*background-color: rgb(0, 137, 207);*/"));
        frame_3->setFrameShape(QFrame::Panel);
        frame_3->setFrameShadow(QFrame::Sunken);
        horizontalLayout_3 = new QHBoxLayout(frame_3);
        horizontalLayout_3->setSpacing(3);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(3, 3, 3, 3);
        frame_7 = new QFrame(frame_3);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        frame_7->setMinimumSize(QSize(350, 126));
        frame_7->setMaximumSize(QSize(450, 16777215));
        frame_7->setFont(font);
        frame_7->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame_7->setFrameShape(QFrame::Box);
        frame_7->setFrameShadow(QFrame::Sunken);
        frame_7->setLineWidth(2);
        gridLayout_8 = new QGridLayout(frame_7);
        gridLayout_8->setSpacing(3);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(3, 3, 3, 3);
        labelGoodProc = new QLabel(frame_7);
        labelGoodProc->setObjectName(QString::fromUtf8("labelGoodProc"));
        labelGoodProc->setMinimumSize(QSize(120, 10));
        labelGoodProc->setMaximumSize(QSize(90, 16777215));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Times New Roman"));
        font5.setPointSize(24);
        font5.setBold(false);
        font5.setWeight(50);
        labelGoodProc->setFont(font5);
        labelGoodProc->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelGoodProc->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_8->addWidget(labelGoodProc, 0, 4, 1, 1);

        label_105 = new QLabel(frame_7);
        label_105->setObjectName(QString::fromUtf8("label_105"));
        label_105->setMinimumSize(QSize(80, 10));
        label_105->setMaximumSize(QSize(80, 16777215));
        label_105->setFont(font5);
        label_105->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_105->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_105, 0, 0, 1, 1);

        labelGood = new QLabel(frame_7);
        labelGood->setObjectName(QString::fromUtf8("labelGood"));
        labelGood->setMinimumSize(QSize(140, 10));
        labelGood->setMaximumSize(QSize(130, 16777215));
        labelGood->setFont(font5);
        labelGood->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelGood->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_8->addWidget(labelGood, 0, 1, 1, 1);

        label_15 = new QLabel(frame_7);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(80, 10));
        label_15->setMaximumSize(QSize(80, 16777215));
        label_15->setFont(font5);
        label_15->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_15->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_15, 2, 0, 1, 1);

        labelBad = new QLabel(frame_7);
        labelBad->setObjectName(QString::fromUtf8("labelBad"));
        labelBad->setMinimumSize(QSize(140, 10));
        labelBad->setMaximumSize(QSize(130, 16777215));
        labelBad->setFont(font5);
        labelBad->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelBad->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_8->addWidget(labelBad, 1, 1, 1, 1);

        labelValve = new QLabel(frame_7);
        labelValve->setObjectName(QString::fromUtf8("labelValve"));
        labelValve->setMinimumSize(QSize(140, 10));
        labelValve->setMaximumSize(QSize(130, 16777215));
        labelValve->setFont(font5);
        labelValve->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelValve->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_8->addWidget(labelValve, 2, 1, 1, 1);

        label_8 = new QLabel(frame_7);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(80, 10));
        label_8->setMaximumSize(QSize(80, 16777215));
        label_8->setFont(font5);
        label_8->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_8->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_8->addWidget(label_8, 1, 0, 1, 1);

        labelBadProc = new QLabel(frame_7);
        labelBadProc->setObjectName(QString::fromUtf8("labelBadProc"));
        labelBadProc->setMinimumSize(QSize(120, 10));
        labelBadProc->setMaximumSize(QSize(90, 16777215));
        labelBadProc->setFont(font5);
        labelBadProc->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelBadProc->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_8->addWidget(labelBadProc, 1, 4, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_8->addItem(horizontalSpacer, 0, 3, 1, 1);


        horizontalLayout_3->addWidget(frame_7);

        frame_6 = new QFrame(frame_3);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setMinimumSize(QSize(0, 126));
        frame_6->setMaximumSize(QSize(250, 16777215));
        frame_6->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame_6->setFrameShape(QFrame::Box);
        frame_6->setFrameShadow(QFrame::Sunken);
        frame_6->setLineWidth(2);
        gridLayout_6 = new QGridLayout(frame_6);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setHorizontalSpacing(3);
        gridLayout_6->setVerticalSpacing(4);
        gridLayout_6->setContentsMargins(3, 3, 3, 3);
        labelAllMin = new QLabel(frame_6);
        labelAllMin->setObjectName(QString::fromUtf8("labelAllMin"));
        labelAllMin->setMinimumSize(QSize(70, 25));
        labelAllMin->setMaximumSize(QSize(70, 25));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Times New Roman"));
        font6.setPointSize(20);
        font6.setBold(false);
        font6.setWeight(50);
        labelAllMin->setFont(font6);
        labelAllMin->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelAllMin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelAllMin, 3, 2, 1, 1);

        labelGoodMin = new QLabel(frame_6);
        labelGoodMin->setObjectName(QString::fromUtf8("labelGoodMin"));
        labelGoodMin->setMinimumSize(QSize(70, 25));
        labelGoodMin->setMaximumSize(QSize(70, 25));
        labelGoodMin->setFont(font6);
        labelGoodMin->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelGoodMin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelGoodMin, 0, 2, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_2, 0, 1, 1, 1);

        label_78 = new QLabel(frame_6);
        label_78->setObjectName(QString::fromUtf8("label_78"));
        label_78->setMinimumSize(QSize(140, 25));
        label_78->setMaximumSize(QSize(90, 25));
        label_78->setFont(font6);
        label_78->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_78->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_78, 2, 0, 1, 1);

        label_95 = new QLabel(frame_6);
        label_95->setObjectName(QString::fromUtf8("label_95"));
        label_95->setMinimumSize(QSize(140, 25));
        label_95->setMaximumSize(QSize(90, 25));
        label_95->setFont(font6);
        label_95->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_95->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_95, 0, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_3, 2, 3, 1, 1);

        labelBadMin = new QLabel(frame_6);
        labelBadMin->setObjectName(QString::fromUtf8("labelBadMin"));
        labelBadMin->setMinimumSize(QSize(70, 25));
        labelBadMin->setMaximumSize(QSize(70, 25));
        labelBadMin->setFont(font6);
        labelBadMin->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelBadMin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelBadMin, 2, 2, 1, 1);

        label_96 = new QLabel(frame_6);
        label_96->setObjectName(QString::fromUtf8("label_96"));
        label_96->setMinimumSize(QSize(140, 25));
        label_96->setMaximumSize(QSize(90, 25));
        label_96->setFont(font6);
        label_96->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_96->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_96, 3, 0, 1, 1);

        label_97 = new QLabel(frame_6);
        label_97->setObjectName(QString::fromUtf8("label_97"));
        label_97->setMinimumSize(QSize(140, 25));
        label_97->setMaximumSize(QSize(90, 25));
        label_97->setFont(font6);
        label_97->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_97->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_6->addWidget(label_97, 4, 0, 1, 1);

        labelAllHour = new QLabel(frame_6);
        labelAllHour->setObjectName(QString::fromUtf8("labelAllHour"));
        labelAllHour->setMinimumSize(QSize(70, 25));
        labelAllHour->setMaximumSize(QSize(70, 25));
        labelAllHour->setFont(font6);
        labelAllHour->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelAllHour->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(labelAllHour, 4, 2, 1, 1);


        horizontalLayout_3->addWidget(frame_6);

        frame_8 = new QFrame(frame_3);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        frame_8->setMinimumSize(QSize(250, 126));
        frame_8->setMaximumSize(QSize(150, 16777215));
        QFont font7;
        font7.setFamily(QString::fromUtf8("Terminal"));
        font7.setPointSize(20);
        font7.setBold(false);
        font7.setWeight(50);
        frame_8->setFont(font7);
        frame_8->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame_8->setFrameShape(QFrame::Box);
        frame_8->setFrameShadow(QFrame::Sunken);
        frame_8->setLineWidth(2);
        gridLayout_9 = new QGridLayout(frame_8);
        gridLayout_9->setSpacing(3);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        gridLayout_9->setContentsMargins(3, 3, 3, 3);
        labelInBOX = new QLabel(frame_8);
        labelInBOX->setObjectName(QString::fromUtf8("labelInBOX"));
        labelInBOX->setMinimumSize(QSize(70, 25));
        labelInBOX->setMaximumSize(QSize(70, 25));
        QFont font8;
        font8.setFamily(QString::fromUtf8("Times New Roman"));
        font8.setPointSize(18);
        font8.setBold(false);
        font8.setWeight(50);
        labelInBOX->setFont(font8);
        labelInBOX->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelInBOX->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_9->addWidget(labelInBOX, 0, 1, 1, 1);

        label_99 = new QLabel(frame_8);
        label_99->setObjectName(QString::fromUtf8("label_99"));
        label_99->setMinimumSize(QSize(140, 25));
        label_99->setMaximumSize(QSize(90, 25));
        label_99->setFont(font8);
        label_99->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_99->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_99, 1, 0, 1, 1);

        label_98 = new QLabel(frame_8);
        label_98->setObjectName(QString::fromUtf8("label_98"));
        label_98->setMinimumSize(QSize(140, 25));
        label_98->setMaximumSize(QSize(90, 25));
        label_98->setFont(font8);
        label_98->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_98->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_98, 0, 0, 1, 1);

        label_100 = new QLabel(frame_8);
        label_100->setObjectName(QString::fromUtf8("label_100"));
        label_100->setMinimumSize(QSize(140, 25));
        label_100->setMaximumSize(QSize(90, 25));
        label_100->setFont(font8);
        label_100->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_100->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_9->addWidget(label_100, 2, 0, 1, 1);

        labelInBoxMax = new QLabel(frame_8);
        labelInBoxMax->setObjectName(QString::fromUtf8("labelInBoxMax"));
        labelInBoxMax->setMinimumSize(QSize(70, 25));
        labelInBoxMax->setMaximumSize(QSize(70, 25));
        labelInBoxMax->setFont(font8);
        labelInBoxMax->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelInBoxMax->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_9->addWidget(labelInBoxMax, 1, 1, 1, 1);

        labelNrBox = new QLabel(frame_8);
        labelNrBox->setObjectName(QString::fromUtf8("labelNrBox"));
        labelNrBox->setMinimumSize(QSize(70, 25));
        labelNrBox->setMaximumSize(QSize(70, 25));
        labelNrBox->setFont(font8);
        labelNrBox->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelNrBox->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_9->addWidget(labelNrBox, 2, 1, 1, 1);


        horizontalLayout_3->addWidget(frame_8);

        frame_2 = new QFrame(centralwidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(300, 10, 1611, 131));
        frame_2->setStyleSheet(QString::fromUtf8("\n"
"/*background-color: rgb(0, 137, 207);*/"));
        frame_2->setFrameShape(QFrame::Panel);
        frame_2->setFrameShadow(QFrame::Sunken);
        horizontalLayout_4 = new QHBoxLayout(frame_2);
        horizontalLayout_4->setSpacing(3);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(3, 3, 3, 3);
        frame_5 = new QFrame(frame_2);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setMinimumSize(QSize(600, 0));
        frame_5->setMaximumSize(QSize(800, 16777215));
        frame_5->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame_5->setFrameShape(QFrame::Box);
        frame_5->setFrameShadow(QFrame::Sunken);
        frame_5->setLineWidth(2);
        horizontalLayout_2 = new QHBoxLayout(frame_5);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(3, 3, 3, 3);
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        LabelKonusTol = new QLabel(frame_5);
        LabelKonusTol->setObjectName(QString::fromUtf8("LabelKonusTol"));
        LabelKonusTol->setMinimumSize(QSize(120, 25));
        LabelKonusTol->setMaximumSize(QSize(50, 16777215));
        LabelKonusTol->setFont(font8);
        LabelKonusTol->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        LabelKonusTol->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(LabelKonusTol, 1, 6, 1, 1);

        label_38 = new QLabel(frame_5);
        label_38->setObjectName(QString::fromUtf8("label_38"));
        label_38->setMinimumSize(QSize(100, 25));
        label_38->setMaximumSize(QSize(110, 16777215));
        label_38->setFont(font8);
        label_38->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_38->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_38, 0, 5, 1, 1);

        LabelLenght = new QLabel(frame_5);
        LabelLenght->setObjectName(QString::fromUtf8("LabelLenght"));
        LabelLenght->setMinimumSize(QSize(140, 25));
        LabelLenght->setMaximumSize(QSize(50, 16777215));
        LabelLenght->setFont(font8);
        LabelLenght->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        LabelLenght->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(LabelLenght, 0, 1, 1, 1);

        labelKonus = new QLabel(frame_5);
        labelKonus->setObjectName(QString::fromUtf8("labelKonus"));
        labelKonus->setMinimumSize(QSize(140, 25));
        labelKonus->setMaximumSize(QSize(50, 16777215));
        labelKonus->setFont(font8);
        labelKonus->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelKonus->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelKonus, 0, 6, 1, 1);

        label_44 = new QLabel(frame_5);
        label_44->setObjectName(QString::fromUtf8("label_44"));
        label_44->setMinimumSize(QSize(100, 25));
        label_44->setMaximumSize(QSize(85, 16777215));
        label_44->setFont(font8);
        label_44->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_44->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_44, 1, 0, 1, 1);

        labelWidth = new QLabel(frame_5);
        labelWidth->setObjectName(QString::fromUtf8("labelWidth"));
        labelWidth->setMinimumSize(QSize(140, 25));
        labelWidth->setMaximumSize(QSize(50, 16777215));
        labelWidth->setFont(font8);
        labelWidth->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelWidth->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelWidth, 0, 4, 1, 1);

        label_43 = new QLabel(frame_5);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setMinimumSize(QSize(100, 25));
        label_43->setMaximumSize(QSize(85, 16777215));
        label_43->setFont(font8);
        label_43->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_43->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_43, 0, 0, 1, 1);

        LabelLenghtTol = new QLabel(frame_5);
        LabelLenghtTol->setObjectName(QString::fromUtf8("LabelLenghtTol"));
        LabelLenghtTol->setMinimumSize(QSize(120, 25));
        LabelLenghtTol->setMaximumSize(QSize(50, 16777215));
        LabelLenghtTol->setFont(font8);
        LabelLenghtTol->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        LabelLenghtTol->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(LabelLenghtTol, 1, 1, 1, 1);

        label_30 = new QLabel(frame_5);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setMinimumSize(QSize(100, 25));
        label_30->setMaximumSize(QSize(110, 16777215));
        label_30->setFont(font8);
        label_30->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_30->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_30, 0, 3, 1, 1);

        LabelWidthTol = new QLabel(frame_5);
        LabelWidthTol->setObjectName(QString::fromUtf8("LabelWidthTol"));
        LabelWidthTol->setMinimumSize(QSize(120, 25));
        LabelWidthTol->setMaximumSize(QSize(50, 16777215));
        LabelWidthTol->setFont(font8);
        LabelWidthTol->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        LabelWidthTol->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(LabelWidthTol, 1, 4, 1, 1);

        label_31 = new QLabel(frame_5);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setMinimumSize(QSize(100, 25));
        label_31->setMaximumSize(QSize(110, 16777215));
        label_31->setFont(font8);
        label_31->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_31->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_31, 1, 3, 1, 1);

        label_39 = new QLabel(frame_5);
        label_39->setObjectName(QString::fromUtf8("label_39"));
        label_39->setMinimumSize(QSize(100, 25));
        label_39->setMaximumSize(QSize(110, 16777215));
        label_39->setFont(font8);
        label_39->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_39->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_39, 1, 5, 1, 1);


        horizontalLayout_2->addLayout(gridLayout_2);


        horizontalLayout_4->addWidget(frame_5);

        frame_4 = new QFrame(frame_2);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setMinimumSize(QSize(200, 0));
        frame_4->setMaximumSize(QSize(400, 16777215));
        frame_4->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame_4->setFrameShape(QFrame::Box);
        frame_4->setFrameShadow(QFrame::Sunken);
        frame_4->setLineWidth(2);
        gridLayout_4 = new QGridLayout(frame_4);
        gridLayout_4->setSpacing(3);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(3, 3, 3, 3);
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        labelKonusOn = new QLabel(frame_4);
        labelKonusOn->setObjectName(QString::fromUtf8("labelKonusOn"));
        labelKonusOn->setMinimumSize(QSize(180, 10));
        labelKonusOn->setMaximumSize(QSize(150, 16777215));
        QFont font9;
        font9.setFamily(QString::fromUtf8("Calibri"));
        font9.setPointSize(18);
        font9.setBold(false);
        font9.setWeight(50);
        labelKonusOn->setFont(font9);
        labelKonusOn->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelKonusOn->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(labelKonusOn, 4, 1, 1, 1);

        label_21 = new QLabel(frame_4);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(180, 10));
        label_21->setMaximumSize(QSize(80, 16777215));
        label_21->setFont(font8);
        label_21->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_21->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_21, 1, 0, 1, 1);

        label_16 = new QLabel(frame_4);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(150, 10));
        label_16->setMaximumSize(QSize(120, 16777215));
        label_16->setFont(font8);
        label_16->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_16->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_16, 4, 0, 1, 1);

        labelWorkingMode = new QLabel(frame_4);
        labelWorkingMode->setObjectName(QString::fromUtf8("labelWorkingMode"));
        labelWorkingMode->setMinimumSize(QSize(200, 10));
        labelWorkingMode->setMaximumSize(QSize(150, 16777215));
        labelWorkingMode->setFont(font8);
        labelWorkingMode->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelWorkingMode->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(labelWorkingMode, 1, 1, 1, 1);

        labelAdvanceSettings = new QLabel(frame_4);
        labelAdvanceSettings->setObjectName(QString::fromUtf8("labelAdvanceSettings"));
        labelAdvanceSettings->setMinimumSize(QSize(180, 10));
        labelAdvanceSettings->setMaximumSize(QSize(150, 16777215));
        labelAdvanceSettings->setFont(font8);
        labelAdvanceSettings->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelAdvanceSettings->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(labelAdvanceSettings, 3, 1, 1, 1);

        label_7 = new QLabel(frame_4);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(150, 10));
        label_7->setMaximumSize(QSize(80, 16777215));
        label_7->setFont(font8);
        label_7->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_7->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_7, 3, 0, 1, 1);

        label_19 = new QLabel(frame_4);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(80, 10));
        label_19->setMaximumSize(QSize(80, 16777215));
        label_19->setFont(font8);
        label_19->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_19->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(label_19, 2, 0, 1, 1);

        labelSelectedType = new QLabel(frame_4);
        labelSelectedType->setObjectName(QString::fromUtf8("labelSelectedType"));
        labelSelectedType->setMinimumSize(QSize(200, 10));
        labelSelectedType->setMaximumSize(QSize(150, 16777215));
        labelSelectedType->setFont(font8);
        labelSelectedType->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelSelectedType->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(labelSelectedType, 2, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout, 1, 0, 1, 1);


        horizontalLayout_4->addWidget(frame_4);

        frame_9 = new QFrame(frame_2);
        frame_9->setObjectName(QString::fromUtf8("frame_9"));
        frame_9->setMinimumSize(QSize(200, 0));
        frame_9->setMaximumSize(QSize(400, 16777215));
        frame_9->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        frame_9->setFrameShape(QFrame::Box);
        frame_9->setFrameShadow(QFrame::Sunken);
        frame_9->setLineWidth(2);
        gridLayout_5 = new QGridLayout(frame_9);
        gridLayout_5->setSpacing(3);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(3, 3, 3, 3);
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        labelwWorkingmodePresort = new QLabel(frame_9);
        labelwWorkingmodePresort->setObjectName(QString::fromUtf8("labelwWorkingmodePresort"));
        labelwWorkingmodePresort->setMinimumSize(QSize(200, 10));
        labelwWorkingmodePresort->setMaximumSize(QSize(150, 16777215));
        labelwWorkingmodePresort->setFont(font8);
        labelwWorkingmodePresort->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelwWorkingmodePresort->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelwWorkingmodePresort, 1, 1, 1, 1);

        label_23 = new QLabel(frame_9);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setMinimumSize(QSize(180, 10));
        label_23->setMaximumSize(QSize(80, 16777215));
        label_23->setFont(font8);
        label_23->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_23->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_23, 1, 0, 1, 1);

        label_22 = new QLabel(frame_9);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setMinimumSize(QSize(180, 10));
        label_22->setMaximumSize(QSize(80, 16777215));
        label_22->setFont(font8);
        label_22->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_22->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_22, 4, 0, 1, 1);

        labelWorkingModePresortSensor = new QLabel(frame_9);
        labelWorkingModePresortSensor->setObjectName(QString::fromUtf8("labelWorkingModePresortSensor"));
        labelWorkingModePresortSensor->setMinimumSize(QSize(200, 10));
        labelWorkingModePresortSensor->setMaximumSize(QSize(150, 16777215));
        labelWorkingModePresortSensor->setFont(font8);
        labelWorkingModePresortSensor->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelWorkingModePresortSensor->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelWorkingModePresortSensor, 4, 1, 1, 1);

        label_26 = new QLabel(frame_9);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setMinimumSize(QSize(180, 10));
        label_26->setMaximumSize(QSize(80, 16777215));
        label_26->setFont(font8);
        label_26->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_26->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_26, 2, 0, 1, 1);

        labelPresortSettings = new QLabel(frame_9);
        labelPresortSettings->setObjectName(QString::fromUtf8("labelPresortSettings"));
        labelPresortSettings->setMinimumSize(QSize(200, 10));
        labelPresortSettings->setMaximumSize(QSize(150, 16777215));
        labelPresortSettings->setFont(font8);
        labelPresortSettings->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelPresortSettings->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelPresortSettings, 2, 1, 1, 1);


        gridLayout_5->addLayout(gridLayout_3, 1, 0, 1, 1);


        horizontalLayout_4->addWidget(frame_9);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(660, 590, 941, 441));
        QFont font10;
        font10.setFamily(QString::fromUtf8("Times New Roman"));
        font10.setPointSize(10);
        tabWidget->setFont(font10);
        tabStatistics = new QWidget();
        tabStatistics->setObjectName(QString::fromUtf8("tabStatistics"));
        verticalLayoutWidget_3 = new QWidget(tabStatistics);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(9, 0, 451, 391));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        plotLenght = new QCustomPlot(verticalLayoutWidget_3);
        plotLenght->setObjectName(QString::fromUtf8("plotLenght"));

        verticalLayout_2->addWidget(plotLenght);

        plotWidth = new QCustomPlot(verticalLayoutWidget_3);
        plotWidth->setObjectName(QString::fromUtf8("plotWidth"));

        verticalLayout_2->addWidget(plotWidth);

        plotWidthMax = new QCustomPlot(verticalLayoutWidget_3);
        plotWidthMax->setObjectName(QString::fromUtf8("plotWidthMax"));

        verticalLayout_2->addWidget(plotWidthMax);

        plotWidthMin = new QCustomPlot(verticalLayoutWidget_3);
        plotWidthMin->setObjectName(QString::fromUtf8("plotWidthMin"));

        verticalLayout_2->addWidget(plotWidthMin);

        horizontalLayoutWidget = new QWidget(tabStatistics);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(470, 0, 451, 91));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        plotGauss1 = new QCustomPlot(horizontalLayoutWidget);
        plotGauss1->setObjectName(QString::fromUtf8("plotGauss1"));

        horizontalLayout->addWidget(plotGauss1);

        plotGauss2 = new QCustomPlot(horizontalLayoutWidget);
        plotGauss2->setObjectName(QString::fromUtf8("plotGauss2"));

        horizontalLayout->addWidget(plotGauss2);

        groupBox_2 = new QGroupBox(tabStatistics);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(470, 100, 221, 301));
        QFont font11;
        font11.setPointSize(12);
        groupBox_2->setFont(font11);
        gridLayout_10 = new QGridLayout(groupBox_2);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        gridLayout_7 = new QGridLayout();
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font12;
        font12.setFamily(QString::fromUtf8("Times New Roman"));
        font12.setPointSize(12);
        label_5->setFont(font12);
        label_5->setFrameShape(QFrame::WinPanel);
        label_5->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label_5, 3, 0, 1, 1);

        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(135, 29));
        label_10->setFont(font12);
        label_10->setFrameShape(QFrame::WinPanel);
        label_10->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label_10, 6, 0, 1, 1);

        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font12);
        label_6->setFrameShape(QFrame::WinPanel);
        label_6->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label_6, 4, 0, 1, 1);

        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font12);
        label_14->setFrameShape(QFrame::WinPanel);
        label_14->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label_14, 5, 0, 1, 1);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font12);
        label->setFrameShape(QFrame::WinPanel);
        label->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label, 0, 0, 1, 1);

        labelAlarm2 = new QLabel(groupBox_2);
        labelAlarm2->setObjectName(QString::fromUtf8("labelAlarm2"));
        labelAlarm2->setMinimumSize(QSize(0, 29));
        labelAlarm2->setMaximumSize(QSize(80, 16777215));
        labelAlarm2->setFont(font12);
        labelAlarm2->setFrameShape(QFrame::WinPanel);
        labelAlarm2->setFrameShadow(QFrame::Sunken);
        labelAlarm2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelAlarm2, 7, 1, 1, 1);

        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(135, 29));
        label_11->setFont(font12);
        label_11->setFrameShape(QFrame::WinPanel);
        label_11->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label_11, 7, 0, 1, 1);

        labelWidthStatisticsGood = new QLabel(groupBox_2);
        labelWidthStatisticsGood->setObjectName(QString::fromUtf8("labelWidthStatisticsGood"));
        labelWidthStatisticsGood->setMaximumSize(QSize(80, 16777215));
        labelWidthStatisticsGood->setFont(font12);
        labelWidthStatisticsGood->setFrameShape(QFrame::WinPanel);
        labelWidthStatisticsGood->setFrameShadow(QFrame::Sunken);
        labelWidthStatisticsGood->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelWidthStatisticsGood, 4, 1, 1, 1);

        labelAlarm3 = new QLabel(groupBox_2);
        labelAlarm3->setObjectName(QString::fromUtf8("labelAlarm3"));
        labelAlarm3->setMinimumSize(QSize(0, 29));
        labelAlarm3->setMaximumSize(QSize(80, 16777215));
        labelAlarm3->setFont(font12);
        labelAlarm3->setFrameShape(QFrame::WinPanel);
        labelAlarm3->setFrameShadow(QFrame::Sunken);
        labelAlarm3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelAlarm3, 8, 1, 1, 1);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font12);
        label_3->setFrameShape(QFrame::WinPanel);
        label_3->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label_3, 2, 0, 1, 1);

        labelElipseStatistics = new QLabel(groupBox_2);
        labelElipseStatistics->setObjectName(QString::fromUtf8("labelElipseStatistics"));
        labelElipseStatistics->setMaximumSize(QSize(80, 16777215));
        labelElipseStatistics->setFont(font12);
        labelElipseStatistics->setFrameShape(QFrame::WinPanel);
        labelElipseStatistics->setFrameShadow(QFrame::Sunken);
        labelElipseStatistics->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelElipseStatistics, 5, 1, 1, 1);

        labelLenghtStatisticsGood = new QLabel(groupBox_2);
        labelLenghtStatisticsGood->setObjectName(QString::fromUtf8("labelLenghtStatisticsGood"));
        labelLenghtStatisticsGood->setMaximumSize(QSize(80, 16777215));
        labelLenghtStatisticsGood->setFont(font12);
        labelLenghtStatisticsGood->setFrameShape(QFrame::WinPanel);
        labelLenghtStatisticsGood->setFrameShadow(QFrame::Sunken);
        labelLenghtStatisticsGood->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelLenghtStatisticsGood, 3, 1, 1, 1);

        labelLenghtStatistics = new QLabel(groupBox_2);
        labelLenghtStatistics->setObjectName(QString::fromUtf8("labelLenghtStatistics"));
        labelLenghtStatistics->setMaximumSize(QSize(80, 16777215));
        labelLenghtStatistics->setFont(font12);
        labelLenghtStatistics->setFrameShape(QFrame::WinPanel);
        labelLenghtStatistics->setFrameShadow(QFrame::Sunken);
        labelLenghtStatistics->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelLenghtStatistics, 0, 1, 1, 1);

        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(135, 29));
        label_12->setFont(font12);
        label_12->setFrameShape(QFrame::WinPanel);
        label_12->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label_12, 8, 0, 1, 1);

        labelWidthStatistics = new QLabel(groupBox_2);
        labelWidthStatistics->setObjectName(QString::fromUtf8("labelWidthStatistics"));
        labelWidthStatistics->setMaximumSize(QSize(80, 16777215));
        labelWidthStatistics->setFont(font12);
        labelWidthStatistics->setFrameShape(QFrame::WinPanel);
        labelWidthStatistics->setFrameShadow(QFrame::Sunken);
        labelWidthStatistics->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelWidthStatistics, 2, 1, 1, 1);

        labelAlarm1 = new QLabel(groupBox_2);
        labelAlarm1->setObjectName(QString::fromUtf8("labelAlarm1"));
        labelAlarm1->setMinimumSize(QSize(0, 29));
        labelAlarm1->setMaximumSize(QSize(80, 16777215));
        labelAlarm1->setFont(font12);
        labelAlarm1->setFrameShape(QFrame::WinPanel);
        labelAlarm1->setFrameShadow(QFrame::Sunken);
        labelAlarm1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelAlarm1, 6, 1, 1, 1);

        label_27 = new QLabel(groupBox_2);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setMinimumSize(QSize(135, 29));
        label_27->setFont(font12);
        label_27->setFrameShape(QFrame::WinPanel);
        label_27->setFrameShadow(QFrame::Sunken);

        gridLayout_7->addWidget(label_27, 9, 0, 1, 1);

        labelAlarm4 = new QLabel(groupBox_2);
        labelAlarm4->setObjectName(QString::fromUtf8("labelAlarm4"));
        labelAlarm4->setMinimumSize(QSize(0, 29));
        labelAlarm4->setMaximumSize(QSize(80, 16777215));
        labelAlarm4->setFont(font12);
        labelAlarm4->setFrameShape(QFrame::WinPanel);
        labelAlarm4->setFrameShadow(QFrame::Sunken);
        labelAlarm4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(labelAlarm4, 9, 1, 1, 1);


        gridLayout_10->addLayout(gridLayout_7, 0, 0, 1, 1);

        groupBox_4 = new QGroupBox(tabStatistics);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(690, 100, 231, 301));
        groupBox_4->setFont(font11);
        gridLayout_11 = new QGridLayout(groupBox_4);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        gridLayout_13 = new QGridLayout();
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        label_13 = new QLabel(groupBox_4);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(80, 0));
        label_13->setMaximumSize(QSize(80, 16777215));
        label_13->setFont(font12);
        label_13->setFrameShape(QFrame::WinPanel);
        label_13->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(label_13, 3, 0, 1, 1);

        labelBadStat_3 = new QLabel(groupBox_4);
        labelBadStat_3->setObjectName(QString::fromUtf8("labelBadStat_3"));
        labelBadStat_3->setMinimumSize(QSize(120, 0));
        labelBadStat_3->setMaximumSize(QSize(80, 16777215));
        labelBadStat_3->setFont(font12);
        labelBadStat_3->setFrameShape(QFrame::WinPanel);
        labelBadStat_3->setFrameShadow(QFrame::Sunken);
        labelBadStat_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_13->addWidget(labelBadStat_3, 2, 1, 1, 1);

        labelBadStat_4 = new QLabel(groupBox_4);
        labelBadStat_4->setObjectName(QString::fromUtf8("labelBadStat_4"));
        labelBadStat_4->setMinimumSize(QSize(120, 0));
        labelBadStat_4->setMaximumSize(QSize(80, 16777215));
        labelBadStat_4->setFont(font12);
        labelBadStat_4->setFrameShape(QFrame::WinPanel);
        labelBadStat_4->setFrameShadow(QFrame::Sunken);
        labelBadStat_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_13->addWidget(labelBadStat_4, 3, 1, 1, 1);

        labelBadStat_6 = new QLabel(groupBox_4);
        labelBadStat_6->setObjectName(QString::fromUtf8("labelBadStat_6"));
        labelBadStat_6->setMinimumSize(QSize(120, 0));
        labelBadStat_6->setMaximumSize(QSize(80, 16777215));
        labelBadStat_6->setFont(font12);
        labelBadStat_6->setFrameShape(QFrame::WinPanel);
        labelBadStat_6->setFrameShadow(QFrame::Sunken);
        labelBadStat_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_13->addWidget(labelBadStat_6, 5, 1, 1, 1);

        labelBadStat_8 = new QLabel(groupBox_4);
        labelBadStat_8->setObjectName(QString::fromUtf8("labelBadStat_8"));
        labelBadStat_8->setMinimumSize(QSize(120, 0));
        labelBadStat_8->setMaximumSize(QSize(80, 16777215));
        labelBadStat_8->setFont(font12);
        labelBadStat_8->setFrameShape(QFrame::WinPanel);
        labelBadStat_8->setFrameShadow(QFrame::Sunken);
        labelBadStat_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_13->addWidget(labelBadStat_8, 7, 1, 1, 1);

        labelBadStat_5 = new QLabel(groupBox_4);
        labelBadStat_5->setObjectName(QString::fromUtf8("labelBadStat_5"));
        labelBadStat_5->setMinimumSize(QSize(120, 0));
        labelBadStat_5->setMaximumSize(QSize(80, 16777215));
        labelBadStat_5->setFont(font12);
        labelBadStat_5->setFrameShape(QFrame::WinPanel);
        labelBadStat_5->setFrameShadow(QFrame::Sunken);
        labelBadStat_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_13->addWidget(labelBadStat_5, 4, 1, 1, 1);

        labelBadStat_7 = new QLabel(groupBox_4);
        labelBadStat_7->setObjectName(QString::fromUtf8("labelBadStat_7"));
        labelBadStat_7->setMinimumSize(QSize(120, 0));
        labelBadStat_7->setMaximumSize(QSize(80, 16777215));
        labelBadStat_7->setFont(font12);
        labelBadStat_7->setFrameShape(QFrame::WinPanel);
        labelBadStat_7->setFrameShadow(QFrame::Sunken);
        labelBadStat_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_13->addWidget(labelBadStat_7, 6, 1, 1, 1);

        label_2 = new QLabel(groupBox_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(80, 0));
        label_2->setMaximumSize(QSize(80, 16777215));
        label_2->setFont(font12);
        label_2->setFrameShape(QFrame::WinPanel);
        label_2->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(label_2, 0, 0, 1, 1);

        labelBadStat = new QLabel(groupBox_4);
        labelBadStat->setObjectName(QString::fromUtf8("labelBadStat"));
        labelBadStat->setMinimumSize(QSize(120, 0));
        labelBadStat->setMaximumSize(QSize(80, 16777215));
        labelBadStat->setFont(font12);
        labelBadStat->setFrameShape(QFrame::WinPanel);
        labelBadStat->setFrameShadow(QFrame::Sunken);
        labelBadStat->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_13->addWidget(labelBadStat, 0, 1, 1, 1);

        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(80, 0));
        label_9->setMaximumSize(QSize(80, 16777215));
        label_9->setFont(font12);
        label_9->setFrameShape(QFrame::WinPanel);
        label_9->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(label_9, 2, 0, 1, 1);

        label_18 = new QLabel(groupBox_4);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setMinimumSize(QSize(80, 0));
        label_18->setMaximumSize(QSize(80, 16777215));
        label_18->setFont(font12);
        label_18->setFrameShape(QFrame::WinPanel);
        label_18->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(label_18, 5, 0, 1, 1);

        label_4 = new QLabel(groupBox_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(80, 0));
        label_4->setMaximumSize(QSize(80, 16777215));
        label_4->setFont(font12);
        label_4->setFrameShape(QFrame::WinPanel);
        label_4->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(label_4, 1, 0, 1, 1);

        label_17 = new QLabel(groupBox_4);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setMinimumSize(QSize(80, 0));
        label_17->setMaximumSize(QSize(80, 16777215));
        label_17->setFont(font12);
        label_17->setFrameShape(QFrame::WinPanel);
        label_17->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(label_17, 4, 0, 1, 1);

        label_20 = new QLabel(groupBox_4);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMinimumSize(QSize(80, 0));
        label_20->setMaximumSize(QSize(80, 16777215));
        label_20->setFont(font12);
        label_20->setFrameShape(QFrame::WinPanel);
        label_20->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(label_20, 6, 0, 1, 1);

        labelBadStat_2 = new QLabel(groupBox_4);
        labelBadStat_2->setObjectName(QString::fromUtf8("labelBadStat_2"));
        labelBadStat_2->setMinimumSize(QSize(120, 0));
        labelBadStat_2->setMaximumSize(QSize(80, 16777215));
        labelBadStat_2->setFont(font12);
        labelBadStat_2->setFrameShape(QFrame::WinPanel);
        labelBadStat_2->setFrameShadow(QFrame::Sunken);
        labelBadStat_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_13->addWidget(labelBadStat_2, 1, 1, 1, 1);

        label_24 = new QLabel(groupBox_4);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setMinimumSize(QSize(80, 0));
        label_24->setMaximumSize(QSize(80, 16777215));
        label_24->setFont(font12);
        label_24->setFrameShape(QFrame::WinPanel);
        label_24->setFrameShadow(QFrame::Sunken);

        gridLayout_13->addWidget(label_24, 7, 0, 1, 1);


        gridLayout_11->addLayout(gridLayout_13, 0, 0, 1, 1);

        tabWidget->addTab(tabStatistics, QString());
        tabService = new QWidget();
        tabService->setObjectName(QString::fromUtf8("tabService"));
        statusList = new QListWidget(tabService);
        statusList->setObjectName(QString::fromUtf8("statusList"));
        statusList->setGeometry(QRect(510, 220, 411, 181));
        groupBox_5 = new QGroupBox(tabService);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 10, 271, 401));
        verticalLayoutWidget_4 = new QWidget(groupBox_5);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(10, 30, 251, 212));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        checkInput0 = new QCheckBox(verticalLayoutWidget_4);
        checkInput0->setObjectName(QString::fromUtf8("checkInput0"));

        horizontalLayout_11->addWidget(checkInput0);

        labelCheckInput0 = new QLabel(verticalLayoutWidget_4);
        labelCheckInput0->setObjectName(QString::fromUtf8("labelCheckInput0"));
        labelCheckInput0->setMaximumSize(QSize(100, 16777215));
        labelCheckInput0->setFrameShape(QFrame::Box);
        labelCheckInput0->setFrameShadow(QFrame::Sunken);

        horizontalLayout_11->addWidget(labelCheckInput0);


        verticalLayout_3->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        checkInput1 = new QCheckBox(verticalLayoutWidget_4);
        checkInput1->setObjectName(QString::fromUtf8("checkInput1"));

        horizontalLayout_12->addWidget(checkInput1);

        labelCheckInput1 = new QLabel(verticalLayoutWidget_4);
        labelCheckInput1->setObjectName(QString::fromUtf8("labelCheckInput1"));
        labelCheckInput1->setMaximumSize(QSize(100, 16777215));
        labelCheckInput1->setFrameShape(QFrame::Box);
        labelCheckInput1->setFrameShadow(QFrame::Sunken);

        horizontalLayout_12->addWidget(labelCheckInput1);


        verticalLayout_3->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        checkInput2 = new QCheckBox(verticalLayoutWidget_4);
        checkInput2->setObjectName(QString::fromUtf8("checkInput2"));

        horizontalLayout_13->addWidget(checkInput2);

        labelCheckInput2 = new QLabel(verticalLayoutWidget_4);
        labelCheckInput2->setObjectName(QString::fromUtf8("labelCheckInput2"));
        labelCheckInput2->setMaximumSize(QSize(100, 16777215));
        labelCheckInput2->setFrameShape(QFrame::Box);
        labelCheckInput2->setFrameShadow(QFrame::Sunken);

        horizontalLayout_13->addWidget(labelCheckInput2);


        verticalLayout_3->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        checkInput3 = new QCheckBox(verticalLayoutWidget_4);
        checkInput3->setObjectName(QString::fromUtf8("checkInput3"));

        horizontalLayout_14->addWidget(checkInput3);

        labelCheckInput3 = new QLabel(verticalLayoutWidget_4);
        labelCheckInput3->setObjectName(QString::fromUtf8("labelCheckInput3"));
        labelCheckInput3->setMaximumSize(QSize(100, 16777215));
        labelCheckInput3->setFrameShape(QFrame::Box);
        labelCheckInput3->setFrameShadow(QFrame::Sunken);

        horizontalLayout_14->addWidget(labelCheckInput3);


        verticalLayout_3->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        checkInput4 = new QCheckBox(verticalLayoutWidget_4);
        checkInput4->setObjectName(QString::fromUtf8("checkInput4"));

        horizontalLayout_15->addWidget(checkInput4);

        labelCheckInput4 = new QLabel(verticalLayoutWidget_4);
        labelCheckInput4->setObjectName(QString::fromUtf8("labelCheckInput4"));
        labelCheckInput4->setMaximumSize(QSize(100, 16777215));
        labelCheckInput4->setFrameShape(QFrame::Box);
        labelCheckInput4->setFrameShadow(QFrame::Sunken);

        horizontalLayout_15->addWidget(labelCheckInput4);


        verticalLayout_3->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        checkInput5 = new QCheckBox(verticalLayoutWidget_4);
        checkInput5->setObjectName(QString::fromUtf8("checkInput5"));

        horizontalLayout_16->addWidget(checkInput5);

        labelCheckInput5 = new QLabel(verticalLayoutWidget_4);
        labelCheckInput5->setObjectName(QString::fromUtf8("labelCheckInput5"));
        labelCheckInput5->setMaximumSize(QSize(100, 16777215));
        labelCheckInput5->setFrameShape(QFrame::Box);
        labelCheckInput5->setFrameShadow(QFrame::Sunken);

        horizontalLayout_16->addWidget(labelCheckInput5);


        verticalLayout_3->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        checkInput6 = new QCheckBox(verticalLayoutWidget_4);
        checkInput6->setObjectName(QString::fromUtf8("checkInput6"));

        horizontalLayout_17->addWidget(checkInput6);

        labelCheckInput6 = new QLabel(verticalLayoutWidget_4);
        labelCheckInput6->setObjectName(QString::fromUtf8("labelCheckInput6"));
        labelCheckInput6->setMaximumSize(QSize(100, 16777215));
        labelCheckInput6->setFrameShape(QFrame::Box);
        labelCheckInput6->setFrameShadow(QFrame::Sunken);

        horizontalLayout_17->addWidget(labelCheckInput6);


        verticalLayout_3->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        checkInput7 = new QCheckBox(verticalLayoutWidget_4);
        checkInput7->setObjectName(QString::fromUtf8("checkInput7"));

        horizontalLayout_18->addWidget(checkInput7);

        labelCheckInput7 = new QLabel(verticalLayoutWidget_4);
        labelCheckInput7->setObjectName(QString::fromUtf8("labelCheckInput7"));
        labelCheckInput7->setMaximumSize(QSize(100, 16777215));
        labelCheckInput7->setFrameShape(QFrame::Box);
        labelCheckInput7->setFrameShadow(QFrame::Sunken);

        horizontalLayout_18->addWidget(labelCheckInput7);


        verticalLayout_3->addLayout(horizontalLayout_18);

        groupBox_6 = new QGroupBox(tabService);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(290, 10, 211, 401));
        verticalLayoutWidget_5 = new QWidget(groupBox_6);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(10, 20, 191, 396));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        checkOutput0 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput0->setObjectName(QString::fromUtf8("checkOutput0"));

        verticalLayout_4->addWidget(checkOutput0);

        checkOutput1 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput1->setObjectName(QString::fromUtf8("checkOutput1"));

        verticalLayout_4->addWidget(checkOutput1);

        checkOutput2 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput2->setObjectName(QString::fromUtf8("checkOutput2"));

        verticalLayout_4->addWidget(checkOutput2);

        checkOutput3 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput3->setObjectName(QString::fromUtf8("checkOutput3"));

        verticalLayout_4->addWidget(checkOutput3);

        checkOutput4 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput4->setObjectName(QString::fromUtf8("checkOutput4"));

        verticalLayout_4->addWidget(checkOutput4);

        checkOutput5 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput5->setObjectName(QString::fromUtf8("checkOutput5"));

        verticalLayout_4->addWidget(checkOutput5);

        checkOutput6 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput6->setObjectName(QString::fromUtf8("checkOutput6"));

        verticalLayout_4->addWidget(checkOutput6);

        checkOutput7 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput7->setObjectName(QString::fromUtf8("checkOutput7"));

        verticalLayout_4->addWidget(checkOutput7);

        checkOutput8 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput8->setObjectName(QString::fromUtf8("checkOutput8"));

        verticalLayout_4->addWidget(checkOutput8);

        checkOutput9 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput9->setObjectName(QString::fromUtf8("checkOutput9"));

        verticalLayout_4->addWidget(checkOutput9);

        checkOutput10 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput10->setObjectName(QString::fromUtf8("checkOutput10"));

        verticalLayout_4->addWidget(checkOutput10);

        checkOutput11 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput11->setObjectName(QString::fromUtf8("checkOutput11"));

        verticalLayout_4->addWidget(checkOutput11);

        checkOutput12 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput12->setObjectName(QString::fromUtf8("checkOutput12"));

        verticalLayout_4->addWidget(checkOutput12);

        checkOutput13 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput13->setObjectName(QString::fromUtf8("checkOutput13"));

        verticalLayout_4->addWidget(checkOutput13);

        checkOutput14 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput14->setObjectName(QString::fromUtf8("checkOutput14"));

        verticalLayout_4->addWidget(checkOutput14);

        checkOutput15 = new QCheckBox(verticalLayoutWidget_5);
        checkOutput15->setObjectName(QString::fromUtf8("checkOutput15"));

        verticalLayout_4->addWidget(checkOutput15);

        groupBox_7 = new QGroupBox(tabService);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setGeometry(QRect(510, 10, 411, 201));
        verticalLayout_5 = new QVBoxLayout(groupBox_7);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        label_28 = new QLabel(groupBox_7);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setMaximumSize(QSize(70, 16777215));
        QFont font13;
        font13.setPointSize(14);
        label_28->setFont(font13);

        horizontalLayout_19->addWidget(label_28);

        labelTrans0_0 = new QLabel(groupBox_7);
        labelTrans0_0->setObjectName(QString::fromUtf8("labelTrans0_0"));
        labelTrans0_0->setFont(font11);
        labelTrans0_0->setFrameShape(QFrame::Box);
        labelTrans0_0->setFrameShadow(QFrame::Sunken);

        horizontalLayout_19->addWidget(labelTrans0_0);

        labelTrans0_1 = new QLabel(groupBox_7);
        labelTrans0_1->setObjectName(QString::fromUtf8("labelTrans0_1"));
        labelTrans0_1->setFont(font11);
        labelTrans0_1->setFrameShape(QFrame::Box);
        labelTrans0_1->setFrameShadow(QFrame::Sunken);

        horizontalLayout_19->addWidget(labelTrans0_1);


        verticalLayout_5->addLayout(horizontalLayout_19);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        label_32 = new QLabel(groupBox_7);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setMaximumSize(QSize(70, 16777215));
        label_32->setFont(font13);

        horizontalLayout_20->addWidget(label_32);

        labelTrans1_0 = new QLabel(groupBox_7);
        labelTrans1_0->setObjectName(QString::fromUtf8("labelTrans1_0"));
        labelTrans1_0->setFont(font11);
        labelTrans1_0->setFrameShape(QFrame::Box);
        labelTrans1_0->setFrameShadow(QFrame::Sunken);

        horizontalLayout_20->addWidget(labelTrans1_0);

        labelTrans1_1 = new QLabel(groupBox_7);
        labelTrans1_1->setObjectName(QString::fromUtf8("labelTrans1_1"));
        labelTrans1_1->setFont(font11);
        labelTrans1_1->setFrameShape(QFrame::Box);
        labelTrans1_1->setFrameShadow(QFrame::Sunken);

        horizontalLayout_20->addWidget(labelTrans1_1);


        verticalLayout_5->addLayout(horizontalLayout_20);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        label_34 = new QLabel(groupBox_7);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setMaximumSize(QSize(70, 16777215));
        label_34->setFont(font13);

        horizontalLayout_21->addWidget(label_34);

        labelTrans2_0 = new QLabel(groupBox_7);
        labelTrans2_0->setObjectName(QString::fromUtf8("labelTrans2_0"));
        labelTrans2_0->setFont(font11);
        labelTrans2_0->setFrameShape(QFrame::Box);
        labelTrans2_0->setFrameShadow(QFrame::Sunken);

        horizontalLayout_21->addWidget(labelTrans2_0);

        labelTrans2_1 = new QLabel(groupBox_7);
        labelTrans2_1->setObjectName(QString::fromUtf8("labelTrans2_1"));
        labelTrans2_1->setFont(font11);
        labelTrans2_1->setFrameShape(QFrame::Box);
        labelTrans2_1->setFrameShadow(QFrame::Sunken);

        horizontalLayout_21->addWidget(labelTrans2_1);


        verticalLayout_5->addLayout(horizontalLayout_21);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        label_36 = new QLabel(groupBox_7);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        label_36->setMaximumSize(QSize(70, 16777215));
        label_36->setFont(font13);

        horizontalLayout_22->addWidget(label_36);

        labelTrans3_0 = new QLabel(groupBox_7);
        labelTrans3_0->setObjectName(QString::fromUtf8("labelTrans3_0"));
        labelTrans3_0->setFont(font11);
        labelTrans3_0->setFrameShape(QFrame::Box);
        labelTrans3_0->setFrameShadow(QFrame::Sunken);

        horizontalLayout_22->addWidget(labelTrans3_0);

        labelTrans3_1 = new QLabel(groupBox_7);
        labelTrans3_1->setObjectName(QString::fromUtf8("labelTrans3_1"));
        labelTrans3_1->setFont(font11);
        labelTrans3_1->setFrameShape(QFrame::Box);
        labelTrans3_1->setFrameShadow(QFrame::Sunken);

        horizontalLayout_22->addWidget(labelTrans3_1);


        verticalLayout_5->addLayout(horizontalLayout_22);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        label_40 = new QLabel(groupBox_7);
        label_40->setObjectName(QString::fromUtf8("label_40"));
        label_40->setMaximumSize(QSize(70, 16777215));
        label_40->setFont(font13);

        horizontalLayout_23->addWidget(label_40);

        labelTrans4_0 = new QLabel(groupBox_7);
        labelTrans4_0->setObjectName(QString::fromUtf8("labelTrans4_0"));
        labelTrans4_0->setFont(font11);
        labelTrans4_0->setFrameShape(QFrame::Box);
        labelTrans4_0->setFrameShadow(QFrame::Sunken);

        horizontalLayout_23->addWidget(labelTrans4_0);

        labelTrans4_1 = new QLabel(groupBox_7);
        labelTrans4_1->setObjectName(QString::fromUtf8("labelTrans4_1"));
        labelTrans4_1->setFont(font11);
        labelTrans4_1->setFrameShape(QFrame::Box);
        labelTrans4_1->setFrameShadow(QFrame::Sunken);

        horizontalLayout_23->addWidget(labelTrans4_1);


        verticalLayout_5->addLayout(horizontalLayout_23);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        label_42 = new QLabel(groupBox_7);
        label_42->setObjectName(QString::fromUtf8("label_42"));
        label_42->setMaximumSize(QSize(70, 16777215));
        label_42->setFont(font13);

        horizontalLayout_24->addWidget(label_42);

        labelTrans5_0 = new QLabel(groupBox_7);
        labelTrans5_0->setObjectName(QString::fromUtf8("labelTrans5_0"));
        labelTrans5_0->setFont(font11);
        labelTrans5_0->setFrameShape(QFrame::Box);
        labelTrans5_0->setFrameShadow(QFrame::Sunken);

        horizontalLayout_24->addWidget(labelTrans5_0);

        labelTrans5_1 = new QLabel(groupBox_7);
        labelTrans5_1->setObjectName(QString::fromUtf8("labelTrans5_1"));
        labelTrans5_1->setFont(font11);
        labelTrans5_1->setFrameShape(QFrame::Box);
        labelTrans5_1->setFrameShadow(QFrame::Sunken);

        horizontalLayout_24->addWidget(labelTrans5_1);


        verticalLayout_5->addLayout(horizontalLayout_24);

        tabWidget->addTab(tabService, QString());
        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(650, 290, 951, 71));
        QFont font14;
        font14.setFamily(QString::fromUtf8("Times New Roman"));
        font14.setPointSize(48);
        frame->setFont(font14);
        frame->setStyleSheet(QString::fromUtf8(""));
        frame->setFrameShape(QFrame::Panel);
        frame->setFrameShadow(QFrame::Sunken);
        horizontalLayout_5 = new QHBoxLayout(frame);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(3, 3, 3, 3);
        FrameStatus = new QFrame(frame);
        FrameStatus->setObjectName(QString::fromUtf8("FrameStatus"));
        FrameStatus->setMaximumSize(QSize(10000, 16777215));
        FrameStatus->setAutoFillBackground(false);
        FrameStatus->setStyleSheet(QString::fromUtf8("\n"
"background-color: rgb(0, 137, 207);"));
        FrameStatus->setFrameShape(QFrame::Box);
        FrameStatus->setFrameShadow(QFrame::Sunken);
        gridLayout_12 = new QGridLayout(FrameStatus);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        labelStatusRect11 = new QLabel(FrameStatus);
        labelStatusRect11->setObjectName(QString::fromUtf8("labelStatusRect11"));
        labelStatusRect11->setMinimumSize(QSize(120, 10));
        labelStatusRect11->setMaximumSize(QSize(200, 16777215));
        labelStatusRect11->setFont(font6);
        labelStatusRect11->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        labelStatusRect11->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_12->addWidget(labelStatusRect11, 0, 0, 1, 1);

        labelStatusRect = new QLabel(FrameStatus);
        labelStatusRect->setObjectName(QString::fromUtf8("labelStatusRect"));
        labelStatusRect->setMinimumSize(QSize(120, 10));
        labelStatusRect->setMaximumSize(QSize(10000, 16777215));
        QFont font15;
        font15.setFamily(QString::fromUtf8("Times New Roman"));
        font15.setPointSize(44);
        font15.setBold(true);
        font15.setWeight(75);
        labelStatusRect->setFont(font15);
        labelStatusRect->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"border-color: rgb(255, 255, 255);\n"
"outline: rgb(255,0,0);"));
        labelStatusRect->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_12->addWidget(labelStatusRect, 0, 1, 1, 1);


        horizontalLayout_5->addWidget(FrameStatus);

        frame_10 = new QFrame(centralwidget);
        frame_10->setObjectName(QString::fromUtf8("frame_10"));
        frame_10->setGeometry(QRect(10, 10, 291, 131));
        frame_10->setStyleSheet(QString::fromUtf8("\n"
"/*background-color: rgb(0, 137, 207);*/"));
        frame_10->setFrameShape(QFrame::Box);
        frame_10->setFrameShadow(QFrame::Sunken);
        horizontalLayout_9 = new QHBoxLayout(frame_10);
        horizontalLayout_9->setSpacing(3);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(3, 3, 3, 3);
        frame_11 = new QFrame(frame_10);
        frame_11->setObjectName(QString::fromUtf8("frame_11"));
        frame_11->setLayoutDirection(Qt::LeftToRight);
        frame_11->setAutoFillBackground(false);
        frame_11->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 137, 207);"));
        frame_11->setFrameShape(QFrame::Box);
        frame_11->setFrameShadow(QFrame::Sunken);
        labelLogo = new QLabel(frame_11);
        labelLogo->setObjectName(QString::fromUtf8("labelLogo"));
        labelLogo->setGeometry(QRect(0, -10, 281, 100));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(labelLogo->sizePolicy().hasHeightForWidth());
        labelLogo->setSizePolicy(sizePolicy1);
        labelLogo->setMinimumSize(QSize(150, 50));
        labelLogo->setMaximumSize(QSize(300, 100));
        labelLogo->setPixmap(QPixmap(QString::fromUtf8(":/MyFiles/res/MB.bmp")));
        labelLogo->setScaledContents(true);
        labelLogo->setAlignment(Qt::AlignCenter);
        label_25 = new QLabel(frame_11);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setGeometry(QRect(10, 65, 261, 51));
        QFont font16;
        font16.setFamily(QString::fromUtf8("Times New Roman"));
        font16.setPointSize(20);
        label_25->setFont(font16);
        label_25->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_25->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(frame_11);

        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(650, 360, 951, 231));
        gridLayout_14 = new QGridLayout(gridLayoutWidget);
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        gridLayout_14->setContentsMargins(0, 0, 0, 0);
        groupBox_3 = new QGroupBox(gridLayoutWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(0, 50));
        groupBox_3->setMaximumSize(QSize(16777215, 200));
        groupBox_3->setFont(font11);
        horizontalLayout_7 = new QHBoxLayout(groupBox_3);
        horizontalLayout_7->setSpacing(3);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(3, 0, 3, 3);
        imageViewPresort = new QGraphicsView(groupBox_3);
        imageViewPresort->setObjectName(QString::fromUtf8("imageViewPresort"));
        imageViewPresort->setMinimumSize(QSize(0, 0));
        imageViewPresort->setFrameShape(QFrame::WinPanel);
        imageViewPresort->setFrameShadow(QFrame::Sunken);

        horizontalLayout_7->addWidget(imageViewPresort);


        gridLayout_14->addWidget(groupBox_3, 0, 2, 1, 1);

        groupBox = new QGroupBox(gridLayoutWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMaximumSize(QSize(16777215, 200));
        groupBox->setFont(font11);
        horizontalLayout_6 = new QHBoxLayout(groupBox);
        horizontalLayout_6->setSpacing(3);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(3, 0, 3, 3);
        imageViewPresortLive = new QGraphicsView(groupBox);
        imageViewPresortLive->setObjectName(QString::fromUtf8("imageViewPresortLive"));
        imageViewPresortLive->setMinimumSize(QSize(0, 0));
        imageViewPresortLive->setFrameShape(QFrame::WinPanel);
        imageViewPresortLive->setFrameShadow(QFrame::Sunken);

        horizontalLayout_6->addWidget(imageViewPresortLive);


        gridLayout_14->addWidget(groupBox, 0, 0, 1, 1);

        gridLayout_15 = new QGridLayout();
        gridLayout_15->setObjectName(QString::fromUtf8("gridLayout_15"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_15->addItem(horizontalSpacer_4, 0, 2, 1, 1);

        labelBadpresortColor = new QLabel(gridLayoutWidget);
        labelBadpresortColor->setObjectName(QString::fromUtf8("labelBadpresortColor"));
        labelBadpresortColor->setMinimumSize(QSize(120, 0));
        labelBadpresortColor->setMaximumSize(QSize(120, 16777215));
        labelBadpresortColor->setFont(font12);
        labelBadpresortColor->setFrameShape(QFrame::WinPanel);
        labelBadpresortColor->setFrameShadow(QFrame::Sunken);
        labelBadpresortColor->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_15->addWidget(labelBadpresortColor, 0, 1, 1, 1);

        labelLenghtStatistics_2 = new QLabel(gridLayoutWidget);
        labelLenghtStatistics_2->setObjectName(QString::fromUtf8("labelLenghtStatistics_2"));
        labelLenghtStatistics_2->setMaximumSize(QSize(80, 16777215));
        labelLenghtStatistics_2->setFont(font12);
        labelLenghtStatistics_2->setFrameShape(QFrame::WinPanel);
        labelLenghtStatistics_2->setFrameShadow(QFrame::Sunken);
        labelLenghtStatistics_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_15->addWidget(labelLenghtStatistics_2, 0, 0, 1, 1);

        labelLenghtStatistics_4 = new QLabel(gridLayoutWidget);
        labelLenghtStatistics_4->setObjectName(QString::fromUtf8("labelLenghtStatistics_4"));
        labelLenghtStatistics_4->setMaximumSize(QSize(80, 16777215));
        labelLenghtStatistics_4->setFont(font12);
        labelLenghtStatistics_4->setFrameShape(QFrame::WinPanel);
        labelLenghtStatistics_4->setFrameShadow(QFrame::Sunken);
        labelLenghtStatistics_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_15->addWidget(labelLenghtStatistics_4, 0, 3, 1, 1);

        labelBadpresortSmall = new QLabel(gridLayoutWidget);
        labelBadpresortSmall->setObjectName(QString::fromUtf8("labelBadpresortSmall"));
        labelBadpresortSmall->setMinimumSize(QSize(120, 0));
        labelBadpresortSmall->setMaximumSize(QSize(120, 16777215));
        labelBadpresortSmall->setFont(font12);
        labelBadpresortSmall->setFrameShape(QFrame::WinPanel);
        labelBadpresortSmall->setFrameShadow(QFrame::Sunken);
        labelBadpresortSmall->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_15->addWidget(labelBadpresortSmall, 0, 4, 1, 1);


        gridLayout_14->addLayout(gridLayout_15, 1, 2, 1, 1);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(spacer);

        labelLenghtStatistics_7 = new QLabel(gridLayoutWidget);
        labelLenghtStatistics_7->setObjectName(QString::fromUtf8("labelLenghtStatistics_7"));
        labelLenghtStatistics_7->setMinimumSize(QSize(140, 0));
        labelLenghtStatistics_7->setMaximumSize(QSize(80, 16777215));
        labelLenghtStatistics_7->setFont(font12);
        labelLenghtStatistics_7->setFrameShape(QFrame::WinPanel);
        labelLenghtStatistics_7->setFrameShadow(QFrame::Sunken);
        labelLenghtStatistics_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_10->addWidget(labelLenghtStatistics_7);

        labelBadpresortSensor = new QLabel(gridLayoutWidget);
        labelBadpresortSensor->setObjectName(QString::fromUtf8("labelBadpresortSensor"));
        labelBadpresortSensor->setMinimumSize(QSize(120, 0));
        labelBadpresortSensor->setMaximumSize(QSize(120, 16777215));
        labelBadpresortSensor->setFont(font12);
        labelBadpresortSensor->setFrameShape(QFrame::WinPanel);
        labelBadpresortSensor->setFrameShadow(QFrame::Sunken);
        labelBadpresortSensor->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_10->addWidget(labelBadpresortSensor);


        gridLayout_14->addLayout(horizontalLayout_10, 1, 0, 1, 1);

        MBsoftwareClass1->setCentralWidget(centralwidget);
        frameMeasurements->raise();
        imageViewMainWindow_0->raise();
        frameStatus->raise();
        frame_3->raise();
        frame_2->raise();
        frame->raise();
        frame_10->raise();
        gridLayoutWidget->raise();
        tabWidget->raise();
        menubar = new QMenuBar(MBsoftwareClass1);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1920, 27));
        QFont font17;
        font17.setFamily(QString::fromUtf8("Times New Roman"));
        font17.setPointSize(14);
        font17.setBold(false);
        font17.setWeight(50);
        menubar->setFont(font17);
        menubar->setMouseTracking(true);
        menubar->setTabletTracking(false);
        menubar->setFocusPolicy(Qt::NoFocus);
        menubar->setContextMenuPolicy(Qt::DefaultContextMenu);
        menubar->setAcceptDrops(true);
        menubar->setAutoFillBackground(false);
        menubar->setDefaultUp(false);
        menubar->setNativeMenuBar(true);
        menuAboutUs = new QMenu(menubar);
        menuAboutUs->setObjectName(QString::fromUtf8("menuAboutUs"));
        menuSignals = new QMenu(menubar);
        menuSignals->setObjectName(QString::fromUtf8("menuSignals"));
        menuLogin = new QMenu(menubar);
        menuLogin->setObjectName(QString::fromUtf8("menuLogin"));
        menuLogin->setFont(font11);
        menuTypes = new QMenu(menubar);
        menuTypes->setObjectName(QString::fromUtf8("menuTypes"));
        menuTypes->setFont(font12);
        menuSettings = new QMenu(menubar);
        menuSettings->setObjectName(QString::fromUtf8("menuSettings"));
        menuHistory = new QMenu(menubar);
        menuHistory->setObjectName(QString::fromUtf8("menuHistory"));
        MBsoftwareClass1->setMenuBar(menubar);

        menubar->addAction(menuLogin->menuAction());
        menubar->addAction(menuTypes->menuAction());
        menubar->addAction(menuSettings->menuAction());
        menubar->addAction(menuSignals->menuAction());
        menubar->addAction(menuHistory->menuAction());
        menubar->addAction(menuAboutUs->menuAction());
        menuAboutUs->addAction(buttonAboutUs);
        menuSignals->addAction(buttonDMS);
        menuSignals->addAction(buttonStatusBox);
        menuSignals->addAction(buttonImageProcessing);
        menuSignals->addSeparator();
        menuSignals->addAction(actionLPT_0);
        menuSignals->addAction(actionLPT_1);
        menuSignals->addSeparator();
        menuSignals->addAction(actionArea_Camera_0);
        menuSignals->addAction(actionArea_Camera_1);
        menuLogin->addAction(buttonLogin);
        menuTypes->addAction(buttonDowelSelect);
        menuTypes->addAction(buttonGeneralSettings);
        menuTypes->addAction(buttonPresortSettings);
        menuTypes->addAction(buttonCalibrationSettings);
        menuSettings->addAction(buttonResetCounters);
        menuSettings->addAction(buttonResetAll);
        menuHistory->addAction(buttonDMStransitionTest);

        retranslateUi(MBsoftwareClass1);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MBsoftwareClass1);
    } // setupUi

    void retranslateUi(QMainWindow *MBsoftwareClass1)
    {
        MBsoftwareClass1->setWindowTitle(QCoreApplication::translate("MBsoftwareClass1", "MainWindow", nullptr));
        buttonAboutUs->setText(QCoreApplication::translate("MBsoftwareClass1", "Card", nullptr));
        buttonDMS->setText(QCoreApplication::translate("MBsoftwareClass1", "Linea Camera Settings", nullptr));
        buttonDMStransitionTest->setText(QCoreApplication::translate("MBsoftwareClass1", "DMS HISTORY  DIALOG", nullptr));
        buttonLogin->setText(QCoreApplication::translate("MBsoftwareClass1", "buttonLogin", nullptr));
        buttonImageProcessing->setText(QCoreApplication::translate("MBsoftwareClass1", "buttonImageProcessing", nullptr));
        buttonResetCounters->setText(QCoreApplication::translate("MBsoftwareClass1", "RESET Counters AND Statistics", nullptr));
        buttonStatusBox->setText(QCoreApplication::translate("MBsoftwareClass1", "status box DIALOG", nullptr));
        buttonDowelSelect->setText(QCoreApplication::translate("MBsoftwareClass1", "Set DOWEL", nullptr));
        buttonCalibrationSettings->setText(QCoreApplication::translate("MBsoftwareClass1", "Calibration Dialog", nullptr));
        buttonPresortSettings->setText(QCoreApplication::translate("MBsoftwareClass1", "Presort Settings DIALOG", nullptr));
        buttonGeneralSettings->setText(QCoreApplication::translate("MBsoftwareClass1", "General Settings DIALOG", nullptr));
        buttonResetAll->setText(QCoreApplication::translate("MBsoftwareClass1", "RESET  ALL", nullptr));
        actionLPT_0->setText(QCoreApplication::translate("MBsoftwareClass1", "LPT 0", nullptr));
        actionLPT_1->setText(QCoreApplication::translate("MBsoftwareClass1", "LPT 1", nullptr));
        actionArea_Camera_0->setText(QCoreApplication::translate("MBsoftwareClass1", "Area Camera 0", nullptr));
        actionArea_Camera_1->setText(QCoreApplication::translate("MBsoftwareClass1", "Area Camera 1", nullptr));
        labelTimeAndDate->setText(QString());
        labelDeviceStatus->setText(QCoreApplication::translate("MBsoftwareClass1", "DEVICE STATUS:", nullptr));
        labelLogin->setText(QString());
        labelStatusNaprave->setText(QString());
        labelDMSStatus->setText(QString());
        labelRotator->setText(QString());
        labelElevator->setText(QString());
        labelRotatorSpeed->setText(QString());
        labelGoodProc->setText(QCoreApplication::translate("MBsoftwareClass1", "112 %", nullptr));
        label_105->setText(QCoreApplication::translate("MBsoftwareClass1", "Good:", nullptr));
        labelGood->setText(QCoreApplication::translate("MBsoftwareClass1", "11222222", nullptr));
        label_15->setText(QCoreApplication::translate("MBsoftwareClass1", "Valve:", nullptr));
        labelBad->setText(QCoreApplication::translate("MBsoftwareClass1", "11222222", nullptr));
        labelValve->setText(QCoreApplication::translate("MBsoftwareClass1", "11222222", nullptr));
        label_8->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad:", nullptr));
        labelBadProc->setText(QCoreApplication::translate("MBsoftwareClass1", "112 %", nullptr));
        labelAllMin->setText(QCoreApplication::translate("MBsoftwareClass1", "700", nullptr));
        labelGoodMin->setText(QCoreApplication::translate("MBsoftwareClass1", "700", nullptr));
        label_78->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad/min:", nullptr));
        label_95->setText(QCoreApplication::translate("MBsoftwareClass1", "Good/min:", nullptr));
        labelBadMin->setText(QCoreApplication::translate("MBsoftwareClass1", "700", nullptr));
        label_96->setText(QCoreApplication::translate("MBsoftwareClass1", "ALL/min:", nullptr));
        label_97->setText(QCoreApplication::translate("MBsoftwareClass1", "ALL/Hour:", nullptr));
        labelAllHour->setText(QCoreApplication::translate("MBsoftwareClass1", "700", nullptr));
        labelInBOX->setText(QCoreApplication::translate("MBsoftwareClass1", "700", nullptr));
        label_99->setText(QCoreApplication::translate("MBsoftwareClass1", "In Box MAX:", nullptr));
        label_98->setText(QCoreApplication::translate("MBsoftwareClass1", "In Box:", nullptr));
        label_100->setText(QCoreApplication::translate("MBsoftwareClass1", "Box NO.:", nullptr));
        labelInBoxMax->setText(QCoreApplication::translate("MBsoftwareClass1", "700", nullptr));
        labelNrBox->setText(QCoreApplication::translate("MBsoftwareClass1", "700", nullptr));
        LabelKonusTol->setText(QCoreApplication::translate("MBsoftwareClass1", "+/-0.25", nullptr));
        label_38->setText(QCoreApplication::translate("MBsoftwareClass1", "Konus:", nullptr));
        LabelLenght->setText(QCoreApplication::translate("MBsoftwareClass1", "30.00 mm", nullptr));
        labelKonus->setText(QCoreApplication::translate("MBsoftwareClass1", "30.00 mm", nullptr));
        label_44->setText(QCoreApplication::translate("MBsoftwareClass1", " L. Tol:", nullptr));
        labelWidth->setText(QCoreApplication::translate("MBsoftwareClass1", "30.00 mm", nullptr));
        label_43->setText(QCoreApplication::translate("MBsoftwareClass1", " Lenght:", nullptr));
        LabelLenghtTol->setText(QCoreApplication::translate("MBsoftwareClass1", "+/-0.25", nullptr));
        label_30->setText(QCoreApplication::translate("MBsoftwareClass1", "Width:", nullptr));
        LabelWidthTol->setText(QCoreApplication::translate("MBsoftwareClass1", "+/-0.25", nullptr));
        label_31->setText(QCoreApplication::translate("MBsoftwareClass1", "W. Tol:", nullptr));
        label_39->setText(QCoreApplication::translate("MBsoftwareClass1", "K.Tol:", nullptr));
        labelKonusOn->setText(QString());
        label_21->setText(QCoreApplication::translate("MBsoftwareClass1", "Working  Mode:", nullptr));
        label_16->setText(QCoreApplication::translate("MBsoftwareClass1", "Dowel Type:", nullptr));
        labelWorkingMode->setText(QCoreApplication::translate("MBsoftwareClass1", "NonStop", nullptr));
        labelAdvanceSettings->setText(QString());
        label_7->setText(QCoreApplication::translate("MBsoftwareClass1", "Adv.Settings:", nullptr));
        label_19->setText(QCoreApplication::translate("MBsoftwareClass1", "Type:", nullptr));
        labelSelectedType->setText(QCoreApplication::translate("MBsoftwareClass1", "Type", nullptr));
        labelwWorkingmodePresort->setText(QCoreApplication::translate("MBsoftwareClass1", "COLOR", nullptr));
        label_23->setText(QCoreApplication::translate("MBsoftwareClass1", "Presort. Camera:", nullptr));
        label_22->setText(QCoreApplication::translate("MBsoftwareClass1", "Presort. Sensor:", nullptr));
        labelWorkingModePresortSensor->setText(QCoreApplication::translate("MBsoftwareClass1", "OFF", nullptr));
        label_26->setText(QCoreApplication::translate("MBsoftwareClass1", "Presort Settings:", nullptr));
        labelPresortSettings->setText(QCoreApplication::translate("MBsoftwareClass1", "Standard", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MBsoftwareClass1", "statistics", nullptr));
        label_5->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr.len.Good:", nullptr));
        label_10->setText(QCoreApplication::translate("MBsoftwareClass1", "Alarm1 Count:", nullptr));
        label_6->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr.Wid.Good:", nullptr));
        label_14->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr.  Elipse:", nullptr));
        label->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. LENGHT:", nullptr));
        labelAlarm2->setText(QCoreApplication::translate("MBsoftwareClass1", "labelWidthStatistics", nullptr));
        label_11->setText(QCoreApplication::translate("MBsoftwareClass1", "Alarm2 Count:", nullptr));
        labelWidthStatisticsGood->setText(QCoreApplication::translate("MBsoftwareClass1", "labelWidthStatistics", nullptr));
        labelAlarm3->setText(QCoreApplication::translate("MBsoftwareClass1", "labelWidthStatistics", nullptr));
        label_3->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. WIDTH:", nullptr));
        labelElipseStatistics->setText(QCoreApplication::translate("MBsoftwareClass1", "labelWidthStatistics", nullptr));
        labelLenghtStatisticsGood->setText(QCoreApplication::translate("MBsoftwareClass1", "labelWidthStatistics", nullptr));
        labelLenghtStatistics->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        label_12->setText(QCoreApplication::translate("MBsoftwareClass1", "Alarm3 Count:", nullptr));
        labelWidthStatistics->setText(QCoreApplication::translate("MBsoftwareClass1", "labelWidthStatistics", nullptr));
        labelAlarm1->setText(QCoreApplication::translate("MBsoftwareClass1", "labelWidthStatistics", nullptr));
        label_27->setText(QCoreApplication::translate("MBsoftwareClass1", "Alarm4 Count:", nullptr));
        labelAlarm4->setText(QCoreApplication::translate("MBsoftwareClass1", "labelWidthStatistics", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Statistics bad", nullptr));
        label_13->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Konus:", nullptr));
        labelBadStat_3->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        labelBadStat_4->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        labelBadStat_6->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        labelBadStat_8->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        labelBadStat_5->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        labelBadStat_7->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        label_2->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Lenght:", nullptr));
        labelBadStat->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        label_9->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Angle:", nullptr));
        label_18->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Rough:", nullptr));
        label_4->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Width:", nullptr));
        label_17->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Elipse:", nullptr));
        label_20->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Error:", nullptr));
        labelBadStat_2->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        label_24->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Color:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabStatistics), QCoreApplication::translate("MBsoftwareClass1", "Statistics", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Input Signals", nullptr));
        checkInput0->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        labelCheckInput0->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        checkInput1->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        labelCheckInput1->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        checkInput2->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        labelCheckInput2->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        checkInput3->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        labelCheckInput3->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        checkInput4->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        labelCheckInput4->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        checkInput5->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        labelCheckInput5->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        checkInput6->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        labelCheckInput6->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        checkInput7->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        labelCheckInput7->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Output Signals", nullptr));
        checkOutput0->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput1->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput2->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput3->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput4->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput5->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput6->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput7->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput8->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput9->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput10->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput11->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput12->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput13->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput14->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        checkOutput15->setText(QCoreApplication::translate("MBsoftwareClass1", "CheckBox", nullptr));
        groupBox_7->setTitle(QCoreApplication::translate("MBsoftwareClass1", "DMS Cam Transitions", nullptr));
        label_28->setText(QCoreApplication::translate("MBsoftwareClass1", "CAM 0:", nullptr));
        labelTrans0_0->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        labelTrans0_1->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        label_32->setText(QCoreApplication::translate("MBsoftwareClass1", "CAM 1:", nullptr));
        labelTrans1_0->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        labelTrans1_1->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        label_34->setText(QCoreApplication::translate("MBsoftwareClass1", "CAM 2:", nullptr));
        labelTrans2_0->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        labelTrans2_1->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        label_36->setText(QCoreApplication::translate("MBsoftwareClass1", "CAM 3:", nullptr));
        labelTrans3_0->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        labelTrans3_1->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        label_40->setText(QCoreApplication::translate("MBsoftwareClass1", "CAM 4:", nullptr));
        labelTrans4_0->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        labelTrans4_1->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        label_42->setText(QCoreApplication::translate("MBsoftwareClass1", "CAM 5:", nullptr));
        labelTrans5_0->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        labelTrans5_1->setText(QCoreApplication::translate("MBsoftwareClass1", "TextLabel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabService), QCoreApplication::translate("MBsoftwareClass1", "Service Tab", nullptr));
        labelStatusRect11->setText(QCoreApplication::translate("MBsoftwareClass1", "DMS STATUS:", nullptr));
        labelStatusRect->setText(QCoreApplication::translate("MBsoftwareClass1", "11222222", nullptr));
        labelLogo->setText(QString());
        label_25->setText(QCoreApplication::translate("MBsoftwareClass1", "DMS-7 Prototip", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Show Dowel Bad", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Show Dowel ALL", nullptr));
        labelBadpresortColor->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        labelLenghtStatistics_2->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad Color:", nullptr));
        labelLenghtStatistics_4->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad short:", nullptr));
        labelBadpresortSmall->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        labelLenghtStatistics_7->setText(QCoreApplication::translate("MBsoftwareClass1", "Bad presort sensor:", nullptr));
        labelBadpresortSensor->setText(QCoreApplication::translate("MBsoftwareClass1", "Avr. Width", nullptr));
        menuAboutUs->setTitle(QCoreApplication::translate("MBsoftwareClass1", "AboutUs", nullptr));
        menuSignals->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Signals", nullptr));
        menuLogin->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Login", nullptr));
        menuTypes->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Dowel Settings", nullptr));
        menuSettings->setTitle(QCoreApplication::translate("MBsoftwareClass1", "Reset", nullptr));
        menuHistory->setTitle(QCoreApplication::translate("MBsoftwareClass1", "History", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MBsoftwareClass1: public Ui_MBsoftwareClass1 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MBSOFTWARE1_H
