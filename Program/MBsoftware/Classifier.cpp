#include "stdafx.h"
#include "Classifier.h"

Classifier::Classifier(QObject *parent)
	: QObject(parent)
{
	this->classifier = SVM::create();
}

Classifier::Classifier()
{
	this->classifier = SVM::create();
}

Classifier::~Classifier()
{
}

void Classifier::SetImagesPath(QString dirPath, QString mode)
{
	if (mode == "train")
	{
		this->imagesDir = dirPath;
	}
	
	else
	{
		this->testImagesDir = dirPath;
	}
}

void Classifier::GetTrainImages()
{
	QDir dir(this->imagesDir);

	//I��emo samo direktorije
	dir.setFilter(QDir::Filter::AllDirs | QDir::Filter::NoDotAndDotDot);
	QStringList dirs = dir.entryList();

	//Ime direkotrija mora biti tvorjeno iz ene samo �tevke
	QRegExp re("^[0-9]$");
	QString path;

	trainImages[0].clear();
	trainImages[1].clear();

	for (int i = 0; i < dirs.size(); ++i)
	{
		if (re.exactMatch(dirs[i]))
		{
			//Shranimo celotno pot do slike ter ime kon�nega direktorija, ki predstavlja oznako slike
			path = this->imagesDir + "//" + dirs[i];
			QDir imagesDir(path);

			QStringList nameFilter;
			nameFilter << "*.png" << "*bmp";
			imagesDir.setNameFilters(nameFilter);//O��emo samo slike z bmp kon�nico

			QStringList images = imagesDir.entryList();
			QString imagesPath;

			for (int j = 0; j < images.size(); ++j)
			{
				imagesPath = path + "//" + images[j];
				this->trainImages[0].push_back(imagesPath);
				this->trainImages[1].push_back(dirs[i]);
			}
		}

		else
		{
			qDebug()<<"Classifier image directory has an invalid name";
		}
	}
}

void Classifier::GetTestImages()
{
	QDir dir(this->testImagesDir);

	//I��emo samo direktorije
	dir.setFilter(QDir::Filter::AllDirs | QDir::Filter::NoDotAndDotDot);
	QStringList dirs = dir.entryList();

	//Ime direkotrija mora biti tvorjeno iz ene samo �tevke
	QRegExp re("^[0-9]$");
	QString path;

	testImages[0].clear();
	trainImages[1].clear();

	for (int i = 0; i < dirs.size(); ++i)
	{
		if (re.exactMatch(dirs[i]))
		{
			//Shranimo celotno pot do slike ter ime kon�nega direktorija, ki predstavlja oznako slike
			path = this->testImagesDir + "//" + dirs[i];
			QDir imagesDir(path);

			QStringList nameFilter;
			nameFilter << "*.png" << "*.bmp";
			imagesDir.setNameFilters(nameFilter);//O��emo samo slike z bmp kon�nico

			QStringList images = imagesDir.entryList();
			QString imagesPath;

			for (int j = 0; j < images.size(); ++j)
			{
				imagesPath = path + "//" + images[j];
				this->testImages[0].push_back(imagesPath);
				this->testImages[1].push_back(dirs[i]);
			}
		}

		else
		{
			qDebug() << "Classifier image directory has an invalid name";
		}
	}
}

void Classifier::PrepareTrainData()
{
	HOGDescriptor currentHOG = HOGDescriptor(cv::Size(128,128), cv::Size(16,16), cv::Size(2,2), cv::Size(8,8), 9);
	vector<float> descriptorsVector;
	vector<float> labelsVector;
	vector<vector<float>> descriptorMatrix;
	vector<cv::Point> locs;

	//�tevilo vseh slik
	int numOfImages = trainImages[0].size();

	//Zanka, ki za vsako sliko izra�una HOG deskriptor ter ga doda k u�nim podatkom
	for (int i = 0; i < numOfImages; ++i)
	{
		Mat currImg = imread(trainImages[0][i].toStdString());

		//HOG deskriptor zahteva sivo sliko
		if (currImg.channels() == 3 && preSett.imageDepth == 1)
			cvtColor(currImg, currImg, COLOR_BGR2GRAY);

		//Podatki
		resize(currImg, currImg, preSett.imageSize);
		currentHOG.compute(currImg, descriptorsVector, cv::Size(16, 16), cv::Size(0, 0), locs);
		descriptorMatrix.push_back(descriptorsVector);
		//Oznake
		float label = trainImages[1][i].toFloat();
		labelsVector.push_back(label);
	}

	//Deskriptorje zapakiramo v primerno obliko
	Mat trainingMatrix = Mat(descriptorMatrix.size(), descriptorMatrix[0].size(), CV_32FC1);
	Mat labels = Mat(labelsVector.size(), 1, CV_32SC1);

	for (int i = 0; i < labels.rows; ++i)
	{
		labels.at<float>(i, 0) = labelsVector[i];
	}

	for (int i = 0; i < trainingMatrix.rows; ++i)
	{
		for (int j = 0; j < trainingMatrix.cols; ++j)
		{
			trainingMatrix.at<float>(i, j) = descriptorMatrix[i][j];
		}
	}

	this->trainingData = trainingData->create(trainingMatrix, ml::SampleTypes::ROW_SAMPLE, labels);
	//this->trainingData->setTrainTestSplitRatio(0.8);
}

void Classifier::PrepareTestData()
{
	HOGDescriptor currentHOG = HOGDescriptor(cv::Size(128, 128), cv::Size(16, 16), cv::Size(2, 2), cv::Size(8, 8), 9);
	vector<float> descriptorsVector;
	vector<float> labelsVector;
	vector<vector<float>> descriptorMatrix;
	vector<cv::Point> locs;

	//�tevilo vseh slik
	int numOfImages = testImages[0].size();

	//Zanka, ki za vsako sliko izra�una HOG deskriptor ter ga doda k u�nim podatkom
	for (int i = 0; i < numOfImages; ++i)
	{
		Mat currImg = imread(testImages[0][i].toStdString());

		//HOG deskriptor zahteva sivo sliko
		if (currImg.channels() == 3 && preSett.imageDepth == 1)
			cvtColor(currImg, currImg, COLOR_BGR2GRAY);

		//Podatki
		resize(currImg, currImg, preSett.imageSize);
		currentHOG.compute(currImg, descriptorsVector, cv::Size(16, 16), cv::Size(0, 0), locs);
		descriptorMatrix.push_back(descriptorsVector);
		//Oznake
		float label = testImages[1][i].toFloat();
		labelsVector.push_back(label);
	}

	//Deskriptorje zapakiramo v primerno obliko
	Mat testMatrix = Mat(descriptorMatrix.size(), descriptorMatrix[0].size(), CV_32FC1);
	Mat labels = Mat(labelsVector.size(), 1, CV_32S);

	for (int i = 0; i < labels.rows; ++i)
	{
		labels.at<float>(i, 0) = labelsVector[i];
	}

	for (int i = 0; i < testMatrix.rows; ++i)
	{
		for (int j = 0; j < testMatrix.cols; ++j)
		{
			testMatrix.at<float>(i, j) = descriptorMatrix[i][j];
		}
	}

	this->testData = testData->create(testMatrix, ml::SampleTypes::ROW_SAMPLE, labels);
	//this->trainingData->setTrainTestSplitRatio(0.8);
}

void Classifier::GetHOGFromImages()
{
}

void Classifier::GetTrainingFeatures(int featureType)
{

}

bool Classifier::LearnClassifier()
{
	this->GetTrainImages();
	this->PrepareTrainData();

	classifier->setType(SVM::C_SVC);
	classifier->setKernel(SVM::RBF);
	//classifier->setDegree(3);
	//classifier->setGamma(1);
	//classifier->setCoef0(0);
	classifier->setC(0.2);
	//classifier->setNu(0.5);
	//classifier->setP(0);
	classifier->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 100, 1e-6));
	classifier->trainAuto(trainingData);

	return false;
}

void Classifier::TestClassifier()
{
	this->GetTestImages();
	this->PrepareTestData();

	Mat testSamples = this->testData->getTrainSamples(ml::SampleTypes::ROW_SAMPLE, false, false);
	std::vector<float> responses = testData->getResponses();

	std::vector<float> prediction;
	this->classifier->predict(testSamples, prediction);

	int hits = 0;
	int misses = 0;
	
	for (int i = 0; i < prediction.size(); ++i)
	{
		if (prediction[i] == responses[i])
		{
			hits++;
		}

		else
		{
			misses++;
		}
	}

	float hitPrec = hits / (float)prediction.size();
	float missPrec = misses / (float)prediction.size();

	int bla = 0;
	
}
