#pragma once

class Timer
{
public:
	Timer();
	~Timer();

public:
	LARGE_INTEGER start;
	LARGE_INTEGER stop;
	int timeCounter;
	double frequency;
	double elapsed;
	LARGE_INTEGER PROC_FREQUENCY;
	double PROC_FREQUENCY_GHZ;

public:
	void Init();
	double ElapsedTime(); //elapsed time in mili seconds
	double CalcFrequency(int counter);
	void SetStart(void);
	void SetStop(void);

};