#pragma once


#ifndef  DMSstat_H_
#define  DMSstat_H_
#endif

class CDMSstat
{
public: // konstruktor
	CDMSstat();

public: // destruktor
	~CDMSstat();

private:
	static QMutex _mutexBackupBox;
	static QMutex _mutexPermanentCounters;

	static int _saveTimePeriod;  //casovna perioda (v sekundah) za avtomaticno shranjevanje statistike
	static QDateTime _lastSaveTime;

	static QDateTime _lastBackupBoxTime;

	static QDateTime _lastStartDateTime;
	static QDateTime _beforeLastStopDateTime;
	static QDateTime _lastStopDateTime;



	//zabelezeni stevci ob startu oziroma ustavitvi naprave
	static int _startGood;
	static int _startBad;
	static int _startValve;	//stevilo pihov dobrih moznikov ob zagonu naprave
	static int _stopGood;
	static int _stopBad;
	static int _stopValve;	//stevilo pihov dobrih moznikov ob ustavitvi naprave
	static bool _napravaOn;  //ima vrednost true med klicem SetStart in SetStop

	//Spremenljivke za statistiko skatel
	static float _boxSetMass;  //predpisana masa skatle z dobrimi mozniki: ima pomen le, kadar masa stevilo moznikov v skatli ni predpisano
	static double _boxSumLength;  
	static double _boxSumWidth;
	
	static const bool _MirovFormat = true;

	static const int _savePermanentCountersPeriod = 10;  //casovna perioda za shranjevanje main counters
	static unsigned long long int _permanentCounterGood;  //stalni stevec dobrih
	static unsigned long long int _permanentCounterBad;  //stalni stevec slabih
	static unsigned long long int _permanentCounterValve;  //stalni stevec pihanih: lahko je manjse od _permanentCounterGood, ker naprava ne pihne dobrega moznika, kadar se dotika slabega
	static QDateTime _lastPermanentCountersSaveTime;

	static QString _countersPath;  //tu shranjujemo stalne stevce ter backup trenutnega stanja skatle



private:
	static QString PodaljsajNiz(QString niz);

	static QString PodaljsajNiz(QString niz, int length);

	static QString DMSstatDolociDirPath(int vrstaStatistike);
	static QString DMSstatDolociFileName(int vrstaStatistike, int fileNumber, QDate* datum);
	static QString DMSstatDolociPot(int vrstaStatistike, int fileNumber, QDate* datum);

	static int FindFileNumber(int vrstaStatistike, QDate* datum);  //poisce zadnji obstojeci file number, ce je ta prevelik pa gre na naslednjo stevilko

	static char* ToChar(QString operater);

	static void SaveTime(int dowelGood, int  dowelBad, int valve, float length, float tolLen, float width, float tolWidth, float konus, float tolKon, QString operater);

	static void SaveBox(int boxNumber, float setMass, float mass, int dowelIn, float setLength, float setLengthTol, float setWidth, float setWidthTol, float boxAvgLength, float boxAvgWidth, int badDowels, int goodDowels, QString operater);

	static void SaveStop(QTime* startTime, int startGood, int startBad, int startValve, QTime* stopTime, int stopGood, int stopBad, int stopValve, int timeOff, int timeOn, int stopError);

	static vector<QString> VrniNaslovnoVrstico(int vrstaStatistike);

	static QString VrniNaslovnoVrsticoMiro(int vrstaStatistike);

	static bool SaveStat(int vrstaStatistike, vector<QString> novaVrstica, QDate* datum);  //shranjuje podatke v txt datoteke po Martinovem formatu: podatki loceni s presledki

	static bool SaveStatMiro(int vrstaStatistike, QString novaVrstica, QDate* datum);  //shranjuje podatke v txt datoteke tocno po Mirovem formatu (podatki loceni s tabulatorju)

	static void ResetCurrentBox();

	//Ob zagonu programa (klic setStart) nalozi spremenljivke skatle iz backup datoteke v spremenljivke skatle: pomembno v primeru sesutja programa
	//funkcija se klice ob vsakem SetStart
	static void RestoreBoxVariables();

	//BackupVariables je namenjena shranjevanju spremenljivk skatle na disk, da se vsebina skatle ne pozabi v primeru zaprtja ali sesutja programa
	//Shranjevanje se izvaja na vsakih nekaj sekund (ob klicu CheckBackupVar v timerju), toda samo, kadar naprava deluje (med klicem SetStart in SetStop). Poklice se tudi ob vsakem SetStop in ResetCurrentBox
	static void BackupVariables();

	static void SavePermanentCounters();
	static void LoadPermanentCounters();

public:
	static void Init();

	static bool SetTimePeriod(int period); //nastavi casovno periodo (v sekundah) za avtomaticno shranjevanje statistike. Priporocena vrednost okoli 5 sekund. Se shrani v datoteko.
	static bool CheckSaveTime(int dowelGood, int dowelBad, int valve, float length, float tolLen, float width, float tolWidth, float konus, float tolKon, QString operater);  //klicati v timerju: funkcija sama preveri, ce je minilo dovolj casa za naslednje shranjevanje
	
	static void ResetBoxNumber(); //Izprazni skatlo (ResetCurrentBox). Klicati samo, kadar uporabnik resetira stevilo skatel. Kadar je skatla polna, FullBox sama klice ResetCurrentBox.

	static bool DowelToBox(float length, float width, bool isGood);  //klicati takoj, ko se kos ovrednosti kot dober/slab, �e pred pihanjem

	static bool BlowDowel();  //klicati ob pihanju


	//Ales naj klice FullBox v ViewTimer, kadar bo skatla polna
	static bool FullBox(int boxNumber, float setMass, float mass, int boxDowelIn, float setLength, float setLengthTol, float setWidth, float setWidthTol, int boxBadDowels, int boxGoodDowels, QString operater);

	static bool CheckBackupVar();  //klicati v timerju: preveri, ce je cas za varnostno kopijo spremenljivk skatle (BackupVariables)

	static void SetStart(int startGood, int startBad, int startValve);  //klicati ob startu naprave za zabelezenje casa zagona naprave in stanja stevcev ter vklop varnostnega shranjevanja z BackupVariables
	static void SetStop(int stopGood, int stopBad, int stopValve, int stopError);  //klicati ob ustavitvi naprave  za zabelezenje casa zagona naprave in stanja stevcev ter izklop varnostnega shranjevanja z BackupVariables

	static bool CheckSavePermanentCounters();  //klicati v timerju: preveri, ce je cas za varnostno kopijo stalnih stevcev

	static int GetPermanentGood();  //vrne stalni stevec dobrih kosov

	static int GetPermanentBad();  //vrne stalni stevec slabih kosov

	static int GetPermanentValve(); //vrne stalni stevec pihanih kosov
};
