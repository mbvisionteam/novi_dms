#pragma once
#include "stdafx.h"
#include "ui_DMScalibration.h"
#include "Measurand.h"
#include "ui_DMSautoCalibrationDLG.h"

class DMScalibration : public QWidget
{
	Q_OBJECT

public:
	DMScalibration(QString filePath);
	~DMScalibration();
	QWidget	* autoCalibrationDialog;
	void closeEvent(QCloseEvent * event);
	void OnShowDialog(int rights, int calibration, float diameter, float lenght);

	static Measurand*						measureObject;


	Ui::DMSCalibrationUI ui;
	Ui::DMSautoCalibration ui2;
	void ConnectMeasurand(Measurand * objects);

	QTimer * OnTimer;
	QString referencePath;
	QString filePath;
	int dix[6];
	int cen[6];
	int selectedCalibration; //selected calibration index za tabelo kalibracij
	float refDowelDiameter;
	float refDowelLenght;
	float newAutoCalibrationFactors[6];


	QTableView  *parametersTable;
	QStandardItemModel* modelTable;
	vector<vector<QStandardItem*>>  standardTableItem;

	void CreateAutoCalibrationTable();
	void UpdateAutoCalibrationTable(int index);
	void CalculateNewCalibrationFactor();



	float autoCalibrationWidth[5][5];
	float autoCalibrationLenght[5];
	int autoCalibrationIndexCounter = 0;

private slots:
	void ViewTimer(); //timer za izris zaslona
	void ReadCalibrationSettings();
	void WriteCalibrationSettings(int index);
	void OnClickedEnterRefDowel(void);
	void SelectedReference(int index);
	void OnClickedSaveCorrection();
	void OnClickedClosedDialog();
	void OnClickedGetRefCenters();
	void OnClickedCalculateKorFactors();
	void OnClickedCalculateKorFactorWidth();
	void OnClickedAutoCalibrationMode();
	void OnClickedSaveAutoCalibration();
	void OnClickedCancelAutoCalibration();



 signals:
	void enterAutoCalibration();
	void leaveAutoCalibration();
};

